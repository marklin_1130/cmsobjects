package test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import digipages.dev.utility.BookUniIdUpdater;
import model.*;


import static org.boon.Boon.*;


public class TestMemberBookBUIDS {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}



	public void testUpdateMBUIDs() {
		try {
			
			BookUniIdUpdater.update(em);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	
	@Test
	public void testSearchByUuid() {
		try {
			
			Member m = Member.getRandomMember(em);
			for (MemberBook mb:m.getMemberBooks())
			{
				MemberBookFinder finder = new MemberBookFinder(mb.getBookUniId());
				finder.find(em, m);
				System.out.println("find MemberBook " + finder.getMemberBook().getId()
						+ " by member=" + m.getId() + " uni_id=" + mb.getBookUniId());
			}
			
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
