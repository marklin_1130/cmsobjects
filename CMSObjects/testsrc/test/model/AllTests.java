package test.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BookVersionTest.class, FixCoverURLTest.class, TestItemChilds.class, TestMemberBookBUIDS.class,
		TestMemberBookReadList.class })
public class AllTests {

}
