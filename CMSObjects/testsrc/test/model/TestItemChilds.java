package test.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Item;
import model.MediabookChapter;


public class TestItemChilds {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testItemSetChilds() {
		try {
			em.getTransaction().begin();
				Item i = (Item) em.find(Item.class,"0010243690");
				List<String> childs = new ArrayList<String>();
				childs.add("1234");
				childs.add("5678");
				i.setChilds(childs);
//				em.persist(i);
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@Test
	public void testMediabookChapter() {
		try {
			em.getTransaction().begin();
			
			
			MediabookChapter chpater = new MediabookChapter();
			chpater.setCat("e07");
//			chpater.setId(publisher_name) --自增流水號-也用來轉檔辨識用
//			chpater.setItemId(post.getItem());
//			chpater.setCat(post.getCat());
//			chpater.setChapterFile(mediaBookChapterPostData.getChapter_file()); // 只有檔名,之後轉檔需要自行組合路徑+檔名
////			chpater.setChapterInfo(null);
////			chpater.setChapterLength(publisher_name)
//			chpater.setChapterName(mediaBookChapterPostData.getChapter_name());
//			chpater.setChapterNo(mediaBookChapterPostData.getChapter_no());
////			chpater.setChapterSize(mediaBookChapterPostData.get)
//			chpater.setCreateTime(dataDate);
//			chpater.setDeleted(false);
////			chpater.setFileLocation(mediaBookChapterPostData);
//			chpater.setFormat(chpater.getChapterFile().substring(chpater.getChapterFile().indexOf(".") + 1));
//			chpater.setIspreview(mediaBookChapterPostData.getIs_preview());
//			chpater.setIsScript(mediaBookChapterPostData.getIs_script());
//			chpater.setIsSubtitle(mediaBookChapterPostData.getIs_subtitle());
//			chpater.setIsTrial(mediaBookChapterPostData.getIs_preview().equalsIgnoreCase("Y") ? true : false);
//			chpater.setLastUpdated(dataDate);
//			chpater.setOperator("mediabookAPI");
//			chpater.setPreviewContent(StringUtils.trimToEmpty(mediaBookChapterPostData.getPreview_content()));
////			chpater.setPreviewInfo(StringUtils.EMPTY);
//			chpater.setPreviewType(mediaBookChapterPostData.getPreview_type());
//			chpater.setResponseMsg(publisher_name)
			
				em.persist(chpater);
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
