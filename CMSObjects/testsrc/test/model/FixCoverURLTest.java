package test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.*;


import static org.boon.Boon.*;


public class FixCoverURLTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testItemSetChilds() {
		try {
			em.getTransaction().begin();
			List<Item> items=em.createNamedQuery("Item.findAll", Item.class).getResultList();
			for (Item tmpItem:items)
			{
				ItemInfo info =tmpItem.getInfo();
				if (! (info instanceof BaseBookInfo))
				{
					continue;
				}
				BaseBookInfo binfo = (BaseBookInfo)info;
				String origCover = binfo.getEfile_cover_url();
				if (null==origCover)
					continue;
				if (!origCover.startsWith("http://s3public.staging.books.com.tw"))
					continue;
				String newCover = removeOldStr(binfo.getEfile_cover_url());
				binfo.setEfile_cover_url(newCover);
			}
			
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	static public String removeOldStr(String orig)
	{
		if (orig.startsWith("http://s3public.staging.books.com.tw"))
		{
			return orig.replace("http://s3public.staging.books.com.tw/cover/"
					, "");
		}
		
		return orig;
	}
}
