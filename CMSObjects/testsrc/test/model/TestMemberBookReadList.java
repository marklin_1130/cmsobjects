package test.model;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import model.*;

public class TestMemberBookReadList {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private Member m;
	private MemberBook mb;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		
		do {
			m = Member.getRandomMember(em);
			List<MemberBook> books = m.getMemberBooks();
			if (books.size()>0)
			mb=books.get(0);
		}while (mb==null);
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testSetReadList() {
		assertNotNull(mb.getReadlistIdnames());
	}
	
	@Test
	public void setWriteReadList()
	{
		em.getEntityManagerFactory().getCache().evictAll();
		em.getTransaction().begin();
		mb.addToReadList("custom1");
		mb.addToReadList("custom2");
		mb.addToReadList("custom1");
		mb.addToReadList("trial");
		
		System.out.println("Result:"+mb.getReadlistIdnames().toString());
		em.getTransaction().commit();
		em.getEntityManagerFactory().getCache().evictAll();
	}

}
