package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import model.BookVersion;

public class BookVersionTest {

	@Test
	public void testBookVersion() {
		try{
			BookVersion bv = new BookVersion("V001.0002");
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	@Test
	public void testNewer() {
		BookVersion v1 = new BookVersion("V001.0001");
		BookVersion v2 = new BookVersion("V001.0002");
		BookVersion v3 = new BookVersion("V002.0001");
		assertTrue(v2.newer(v1));
		assertTrue(v3.newer(v2));
		assertTrue(v3.newer(v1));
	}

	@Test
	public void testSameMajorVersion() {
		BookVersion v1 = new BookVersion("V001.0001");
		BookVersion v2 = new BookVersion("V001.0002");
		assertTrue(v1.sameMajorVersion(v2));
	}

	@Test
	public void testToString() {
		try {
			BookVersion v1 = new BookVersion("V001.0001");
			assertEquals(v1.toString(),"V001.0001");
		}catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	@Test
	public void testUpgradeVersion() {
		BookVersion v1 = new BookVersion("V001.0002");
		assertEquals(v1.upgradeVersion(true).toString(),"V002.0001");
		assertEquals(v1.upgradeVersion(false).toString(),"V002.0002");
	}

}
