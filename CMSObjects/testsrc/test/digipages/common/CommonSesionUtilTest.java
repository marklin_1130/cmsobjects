package test.digipages.common;

import static org.junit.Assert.*;

import org.junit.Test;

import digipages.common.CommonSesionUtil;

public class CommonSesionUtilTest {

	@Test
	public void testGetSuperDomain() {
		assertEquals(".digipage.info", CommonSesionUtil.getSuperDomain("appapi.booksdev.digipage.info"));
		assertEquals(".books.com.tw", CommonSesionUtil.getSuperDomain("appapi.booksdev.books.com.tw"));
	}

}
