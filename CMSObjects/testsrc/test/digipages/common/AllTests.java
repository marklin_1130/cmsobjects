package test.digipages.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CommonSesionUtilTest.class, CommonUtilTest.class })
public class AllTests {

}
