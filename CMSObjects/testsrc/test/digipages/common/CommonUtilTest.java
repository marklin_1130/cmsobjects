package test.digipages.common;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;

import org.junit.Test;

import digipages.common.CommonUtil;

public class CommonUtilTest {

	@Test
	public void testParseDateTime() {
		try {
			OffsetDateTime dateTime=CommonUtil.parseDateTime("2015-07-06T15:41:20+08:00");
			LocalDateTime localDateTime= dateTime.toLocalDateTime();
			java.sql.Timestamp ts=java.sql.Timestamp.valueOf(localDateTime);
			
			System.out.println(ts);
			
		}catch (Exception ex)
		{
			fail();
		}
	}
	@Test
	public void getTailSplitName()
	{
		String result=CommonUtil.getTailSplitName("https://docs.google.com/spreadsheets/d/");
		assertEquals(result, "d");
		result=CommonUtil.getTailSplitName("/d/bb");
		assertEquals(result, "bb");
	}

}
