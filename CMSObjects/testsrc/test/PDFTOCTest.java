package test;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.text.PDFTextStripper;

import digipages.BookConvert.FlattenPDF.PageDimension;
import digipages.BookConvert.utility.CommonUtil;

public class PDFTOCTest {

    private static List<RenderedImage> getImagesFromResources(PDResources resources) throws IOException {
        List<RenderedImage> images = new ArrayList<>();

        for (COSName xObjectName : resources.getXObjectNames()) {
            PDXObject xObject = resources.getXObject(xObjectName);

            if (xObject instanceof PDFormXObject) {
                images.addAll(getImagesFromResources(((PDFormXObject) xObject).getResources()));
            } else if (xObject instanceof PDImageXObject) {
                images.add(((PDImageXObject) xObject).getImage());
            }
        }

        return images;
    }

    private static void addToc(PDDocument document, ArrayList<ArrayList<String>> navMap, PDOutlineItem outline) throws IOException {

        String test ="abc";
        
        System.out.println(test.replaceAll("\u0007", ""));
        System.out.println(test.replaceAll("", ""));
        System.out.println(test.replaceAll("\\x07", ""));
        
        try {
            
        while (outline != null) {
            ArrayList<String> navChild2Point = new ArrayList<String>();
            PDPage childPage = outline.findDestinationPage(document);
            int childPageCount = getPdfPage(document, childPage);
            System.out.println(" Child:" + outline.getTitle() + ",pageNum:" + childPageCount);
            navChild2Point.add(outline.getTitle().replaceAll("\u0007", ""));
            navChild2Point.add(outline.getTitle().replaceAll("", ""));
            navChild2Point.add(outline.getTitle().replaceAll("\\x07", ""));
            
            
            if (childPageCount > 0) {
                navChild2Point.add("page_" + CommonUtil.paddingLeft(String.valueOf(childPageCount), 4, '0') + ".xhtml");
            } else {
                navChild2Point.add("");
            }
            navMap.add(navChild2Point);
            addToc(document, navMap, outline.getFirstChild());
            outline = outline.getNextSibling();
        }
        } catch (Exception e) { 
            e.printStackTrace();
        }

    }
    
    private void testCutPDFrange() throws Exception {
        String Ranges = "1-8";

        File pdfFile = new File("D:/tmp/0702-69/9573234327_0504.pdf");

        // 69 判斷無效頁
        PDDocument sourceDocument = PDDocument.load(pdfFile);
        String[] range = Ranges.split(";");
        int invaildPageCount = 0;
        int lastPageCount = 0;
        for (int i = 0; i < range.length; i++) {
            if (range[i].contains("-")) {
                int start = Integer.valueOf((range[i].split("-"))[0]);
                int end = Integer.valueOf((range[i].split("-"))[1]);

                for (int j = start; j < (end + 1); j++) {
                    PDPage tempPage = sourceDocument.getPage(j-1);
                    boolean isHaveImage = false;
                    boolean isHaveText = false;
                    PDResources pdResources = tempPage.getResources();
                    if (pdResources != null) {
                        List<RenderedImage> images = getImagesFromResources(pdResources);
                        if (images.size() > 0) {
                            isHaveImage = true;
//                            break;
                        }
                    }
                    PDFTextStripper reader = new PDFTextStripper();
                    reader.setStartPage(j-1);
                    reader.setEndPage(j-1);
                    String pageText = reader.getText(sourceDocument);
                    isHaveText = StringUtils.trimToEmpty(pageText).length() > 0;

                    if (!isHaveImage && !isHaveText) {
                        System.out.println("無效頁" + (j-1) + ": isHaveImage:" + isHaveImage + ",isHaveText:" + isHaveText);
                        invaildPageCount++;
                    }

                    lastPageCount = j-1;
                }
            }
        }
        System.out.println("無效頁數:" + invaildPageCount);
        System.out.println("最後頁數:" + lastPageCount);
    }
    
    public static void main(String[] args) {

        try {

            
//            2021-04-08T12:00:00+08:00
            
//            String startDate = "2021/06/11 00:00:00";
            
            
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss+8:00");
            
            
            
//            System.out.println(startDate.substring(0,10).replace("/", "-")+"T15:00:00+8:00");
            

            File pdfFile = new File("C:/tmp/E050044619.pdf");

            //test  dimension
            
            PDDocument pdf = PDDocument.load(pdfFile);
            String pdfSrcPath = pdfFile.getAbsolutePath();
            String targetJpgPath = pdfSrcPath.replaceAll("\\.pdf$", "") + ".jpg";
            PDPage page = pdf.getPage(0);
            pdf.close();
            // below 2 parameter deter the size/detail of pdf
            PageDimension dimension = new PageDimension(page, 2);
            
            System.out.println(dimension.getBigWidth());
            System.out.println(dimension.getBigHeight());
            
            //test  dimension
            
            
//            PDDocument sourceDocument = PDDocument.load(pdfFile);

//            String cmd = "python C:/pdfminer-master/tools/dumppdf.py -T " + pdfFile.getAbsolutePath();
//            System.out.println("Starting dumpPDF: cmd=" + cmd);
            // Run PDFminer
//            Process p = Runtime.getRuntime().exec(cmd);
            // System.out.println(IOUtils.toString(p.getErrorStream()));
//            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));

            String line = "";
            ArrayList<ArrayList<String>> navMap = parsePDFTocUseingPDFBOX(pdfFile);
            // new ArrayList<>();
            // while ((line = reader.readLine()) != null) {
            // if (line.contains("<outline level")) {
            // ArrayList<String> navPoint = new ArrayList<String>();
            // String title;
            // Integer pageno;
            //
            // String temp = line.substring(line.indexOf("title=\"") +
            // ("title=\"").length());
            // title = temp.substring(0, temp.indexOf("\""));
            // navPoint.add(title);
            // System.out.println(title);
            // while (!(line = reader.readLine()).contains("</outline>")) {
            // if (line.contains("<pageno>")) {
            // temp = line.substring(line.indexOf("<pageno>") + ("<pageno>").length());
            // pageno = Integer.parseInt(temp.substring(0, temp.indexOf("<")));
            // String pageNo = CommonUtil.paddingLeft(String.valueOf(pageno + 1), 4, '0');
            // String pageHtml = "page_" + pageNo + ".xhtml";
            // navPoint.add(pageHtml);
            // System.out.println(pageHtml);
            // }
            // }
            //
            // // if (navPoint.size() < 2) {
            // // throw new IllegalArgumentException("PDF lacks page info
            // // in TOC index.");
            // // }
            // navMap.add(navPoint);
            //
            // }
            // }

            String outline = "";
            outline += "<?xml version='1.0'?>\r\n" + "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:epub='http://www.idpf.org/2007/ops' xml:lang='en' lang='en'>\r\n" + "   <head>\r\n"
                    + "       <meta charset='utf-8' />\r\n" + "       <title>" + "TEST" + "</title>\r\n" + "   </head>\r\n" + "   <body>";
            // <navMap>
            outline += "  <nav epub:type='toc'>\r\n" + "           <h1>Table of Contents</h1>";
            for (ArrayList<String> navPoint : navMap) {
                outline += "           <ol>\r\n" + "                <li>\r\n" + "                    <a href='" + (navPoint.size() > 1 ? navPoint.get(1) : "") + "'>" + navPoint.get(0) + "</a>\r\n"
                        + "                   <ol>\r\n" + "                   </ol>\r\n" + "                </li> \r\n" + "            </ol>";
            }
            outline += "       </nav>\r\n" + "   </body>\r\n" + "</html>";
            System.out.println(outline);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static ArrayList<ArrayList<String>> parsePDFTocUseingPDFBOX(File file) throws IOException {
        ArrayList<ArrayList<String>> navMap = new ArrayList<>();
        try (PDDocument document = PDDocument.load(file)) {

            PDDocumentCatalog catalog = document.getDocumentCatalog();
            PDDocumentOutline outline = catalog.getDocumentOutline();
            PDOutlineItem item = outline.getFirstChild();
            
            addToc(document, navMap, outline.getFirstChild());
            
//            if (outline != null) {
//                while (item != null) {
//                    ArrayList<String> navPoint = new ArrayList<String>();
//                    PDOutlineItem child = item.getFirstChild();
//                    PDPage pdPage = item.findDestinationPage(document);
//                    int pageNo = getPdfPage(document, pdPage);
//                    String pageHtml = "page_" + CommonUtil.paddingLeft(String.valueOf(pageNo), 4, '0') + ".xhtml";
//                    navPoint.add(item.getTitle());
//                    if (pageNo > 0) {
//                        navPoint.add(pageHtml);
//                    }
//                    navMap.add(navPoint);
//                    while (child != null) {
//                        ArrayList<String> navChildPoint = new ArrayList<String>();
//                        PDPage childTilepdPage = child.findDestinationPage(document);
//                        int chilePage = getPdfPage(document, childTilepdPage);
//                        navChildPoint.add(child.getTitle());
//                        String pageChildHtml = "page_" + CommonUtil.paddingLeft(String.valueOf(chilePage), 4, '0') + ".xhtml";
//
//                        if (chilePage > 0) {
//                            navChildPoint.add(pageChildHtml);
//                        }
//                        navMap.add(navChildPoint);
//                        PDOutlineItem child2 = child.getFirstChild();
//                        while (child2 != null) {
//                            ArrayList<String> navChild2Point = new ArrayList<String>();
//                            PDPage child2TilepdPage = child2.findDestinationPage(document);
//                            int child2Page = getPdfPage(document, child2TilepdPage);
//                            navChild2Point.add(child2.getTitle());
//                            if (child2Page > 0) {
//                                navChild2Point.add("page_" + CommonUtil.paddingLeft(String.valueOf(child2Page), 4, '0') + ".xhtml");
//                            }
//                            navMap.add(navChild2Point);
//                            child2 = child2.getNextSibling();
//                        }
//                        child = child.getNextSibling();
//                    }
//                    item = item.getNextSibling();
//                }
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return navMap;

    }

    private static int getPdfPage(PDDocument document, PDPage page) {
        int pageNum = 0;
        for (int i = 0; i < document.getNumberOfPages(); i++) {
            if (document.getPage(i).equals(page)) {
                return i + 1;
            }
        }
        return pageNum;
    }
}
