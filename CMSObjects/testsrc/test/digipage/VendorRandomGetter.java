package test.digipage;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Publisher;
import model.Vendor;

public class VendorRandomGetter {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testFindPublisherBybid() {
		try {
			Vendor v = Vendor.getRandomVendor(em);
			System.out.println("V" + v.toJsonString());
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
