package test.digipage;


import static org.junit.Assert.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.boon.json.JsonFactory;
import static org.boon.Boon.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Member;
import model.MemberBook;


public class TestJSonSerialize {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJsonSerialize() {
		try {
			UpdateStatusData usd = new UpdateStatusData();
			List<records> rds = new ArrayList<records>();
			rds.add(new records("1234"));
			rds.add(new records("5678"));
			usd.setRecords(rds);
			
			String json=toJson(usd);
			Object object=JsonFactory.create().fromJson(json);
			printObject("",object);
			
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	private void printObject(String head,Object object) {
		
		String xhead=head + ",";
		if (object instanceof Map)
		{
			Map <String,Object> map = (Map<String,Object>) object;
			if (hasChild(map)){
				for (String key:map.keySet())
				{
					printObject(xhead+key,map.get(key));
				}
			}else
			{
				for (String key:map.keySet()){
					System.out.println(xhead+key+"," + map.get(key));
				}
			}
		}else if (object instanceof List)
		{
			List <Object> list = (List<Object>) object;
			for (Object obj:list)
			{
				System.out.println(xhead +"," + obj);
			}
		}
			
		else
		{
			System.out.println("Not Excepted Object" + object);
		}
	}

	private boolean hasChild(Map<String, Object> map) {
		for (Object o:map.values())
		{
			if (o instanceof Map)
				return true;
			if (o instanceof Array)
				return true;
			if (o instanceof List)
				return true;
				
		}
		return false;
	}
	
	public class UpdateStatusData {
		List<records> records;
		String server_time;
		String updated_time;
		
		public List<records> getRecords() {
			return records;
		}
		public void setRecords(List<records> records) {
			this.records = records;
		}
		public String getServer_time() {
			return server_time;
		}
		public void setServer_time(String server_time) {
			this.server_time = server_time;
		}
		public String getUpdated_time() {
			return updated_time;
		}
		public void setUpdated_time(String updated_time) {
			this.updated_time = updated_time;
		}
	}
	
	public class records {
		String id;
		String last_updated_time;

		public records(String id)
		{
			this.id=id;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getLast_updated_time() {
			return last_updated_time;
		}
		public void setLast_updated_time(String last_updated_time) {
			this.last_updated_time = last_updated_time;
		}
	}

}
