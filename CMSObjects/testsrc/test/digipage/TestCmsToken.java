package test.digipage;

import static org.junit.Assert.*;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import digipages.exceptions.ServerException;
import model.CmsToken;
import model.Member;
import model.MemberDevice;
import model.Publisher;
import model.Vendor;

public class TestCmsToken {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testclearWebTokens() {
		try {
			
			Member m=generateMemberAndDevices();
			em.getTransaction().begin();
			MemberDevice.clearWebDevices(em, m);
			CmsToken.clearWebTokens(em, m);
			em.getTransaction().commit();
			List<CmsToken> tokens = CmsToken.listTokenByMember(em, m.getId());
			assertEquals(tokens.size(), 2);
			assertEquals(m.getMemberDevices().size(),2);
			
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	private Member generateMemberAndDevices() throws ServerException {
		Member m = new Member();
		MemberDevice md= new MemberDevice();
		md.setDeviceId(UUID.randomUUID().toString());
		md.setOsType("web");
		md.setLastOnlineIp("127.0.0.1");
		m.addMemberDevice(md);
		
		md = new MemberDevice();
		md.setDeviceId(UUID.randomUUID().toString());
		md.setOsType("mobile_web");
		md.setLastOnlineIp("127.0.0.1");
		m.addMemberDevice(md);
		
		md = new MemberDevice();
		md.setDeviceId(UUID.randomUUID().toString());
		md.setOsType("ios");
		md.setLastOnlineIp("127.0.0.1");
		m.addMemberDevice(md);
		
		md = new MemberDevice();
		md.setDeviceId(UUID.randomUUID().toString());
		md.setOsType("android");
		md.setLastOnlineIp("127.0.0.1");
		m.addMemberDevice(md);
		
		
		
		em.getTransaction().begin();
		em.persist(m);
		em.getTransaction().commit();
		
		em.getTransaction().begin();
		for (MemberDevice tmpMd:m.getMemberDevices())
		{
			CmsToken ct = new CmsToken();
			ct.setDeviceId(tmpMd.getId());
			ct.setMemberId(m.getId());
			ct.setClientIp(tmpMd.getLastOnlineIp());
			em.persist(ct);
		}
		em.getTransaction().commit();
		return m;
	}

}
