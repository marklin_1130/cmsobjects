package test.digipage.pgconverter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.common.CommonUtil;
import model.ActivityLog;
import model.BookRecords;
import model.Member;
import model.MemberBook;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGGenericJsonContentConverter {
	private static String TAG = "TestPGGenericJsonContentConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static Long pk ;
	
	String usualuni_id1 = "123456789";
	String usualuni_id2 = "987654321";
	
	String unusualuni_id1 = "2&(%UR^GF()252";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		deleteTestItem();
		em.close();
	}

	public void dBCreate() {
		try {
			Member m = Member.getRandomMember(em);
			ActivityLog activityLog = new ActivityLog();
			List<BookRecords> book_records = new ArrayList<BookRecords>() ;
			BookRecords records_data = new BookRecords();
			BookRecords records_data2 = new BookRecords();
			em.getTransaction().begin();
			records_data.setBook_uni_id(usualuni_id1);
			records_data.setCurrent_page("5");
			records_data.setIp("169.254.40.234");
			records_data.setVersion("002.0001");
			book_records.add(records_data);
			records_data2.setBook_uni_id("415005458");
			records_data2.setCurrent_page("1");
			records_data2.setIp("169.254.40.211");
			records_data2.setVersion("1.0001");
			book_records.add(records_data2);
			
			activityLog.setMemberId(m.getId());
			activityLog.setLogType("LogMemberActivity");
			activityLog.setLastUpdated(CommonUtil.zonedTime());
			activityLog.setActivityContent(book_records);
			
			em.persist(activityLog);
			em.getTransaction().commit();
			pk=activityLog.getId();
			
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" dBCreate() fail !");
		}
	}
	
	@Test
	public void testAFromDBDBFind() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			ActivityLog testitem=em.find(ActivityLog.class, pk);
			List<BookRecords> book_records = (List<BookRecords>) testitem.getActivityContent();
			BookRecords records_data = book_records.get(0);
			assertEquals(usualuni_id1, records_data.getBook_uni_id());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	@Test
	public void testBToDBUpdate() {
		try {
			//write
			updateBookRecord(usualuni_id2);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			ActivityLog testitem=em.find(ActivityLog.class, pk);
			List<BookRecords> book_records = (List<BookRecords>) testitem.getActivityContent();
			BookRecords records_data = book_records.get(0);
			assertEquals(usualuni_id2, records_data.getBook_uni_id());		
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testCIllegalToDBUpdate() {
		try {
			//write
			updateBookRecord(unusualuni_id1);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			ActivityLog testitem=em.find(ActivityLog.class, pk);
			List<BookRecords> book_records = (List<BookRecords>) testitem.getActivityContent();
			BookRecords records_data = book_records.get(0);
			assertEquals(unusualuni_id1, records_data.getBook_uni_id());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testIllegalToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testDNullToDBUpdate() {
		try {
			//write
			updateBookRecord(null);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			ActivityLog testitem=em.find(ActivityLog.class, pk);
			List<BookRecords> book_records = (List<BookRecords>) testitem.getActivityContent();
			BookRecords records_data = book_records.get(0);
			assertEquals(null, records_data);
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testNullToDBUpdate() fail !");
		}
	}
	
	public void deleteTestItem() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			ActivityLog testitem=em.find(ActivityLog.class, pk);
			em.getTransaction().begin();
			em.remove(testitem);
			em.getTransaction().commit();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testDeleteTestItem() fail !");
		}
	}
	
	public void updateBookRecord(String uni_id){
		em.getEntityManagerFactory().getCache().evictAll(); 
		ActivityLog testitem=em.find(ActivityLog.class, pk);
		List<BookRecords> book_records = (List<BookRecords>) testitem.getActivityContent();
		BookRecords records_data = book_records.get(0);
		em.getTransaction().begin();
		if(uni_id == null){
			book_records.set(0,null);
		}else{
			records_data.setBook_uni_id(uni_id);
		}		
		em.persist(testitem);
		em.getTransaction().commit();
	}
	

}
