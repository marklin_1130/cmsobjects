package test.digipage.pgconverter;

import static org.junit.Assert.*;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import model.Member;
import model.MemberBook;
import model.MemberBookNote;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGuuidConverter {
	private static String TAG = "TestPGuuidConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	
	private static final UUID usualuuid = UUID.randomUUID();//fromString("ea1fd2ad-15d8-acc6-6716-7efc94a4d967");
	private static final UUID usualuuid2 = UUID.randomUUID();//.fromString("5d98ce6d-9d3b-4610-bf19-73e9db0bc5d8");
	
	//UUID unusualuuid1 = UUID.fromString("$F%*@!VU&adgE#(GU()#$455xcbs55");

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		deleteTestItem();
		em.close();
	}

	public void dBCreate() {
		try {
			Member m = Member.getRandomMember(em);
			MemberBook mb=m.getMemberBooks().get(0);
			em.getTransaction().begin();
			MemberBookNote mbn = new MemberBookNote();
			mbn.setUuid(usualuuid);
			mb.addMemberBookNote(mbn);
			em.getTransaction().commit();
			pk = mbn.getId();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" dBCreate() fail !");
		}
	}
	
	@Test
	public void testAFromDBDBFind() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = MemberBookNote.getNoteByUUid(em,usualuuid.toString());
			assertEquals(usualuuid,mbn.getUuid());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	@Test
	public void testBToDBUpdate() {
		try {
			//write
			updateUUID(usualuuid2,usualuuid);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = MemberBookNote.getNoteByUUid(em,usualuuid2.toString());
			assertEquals(usualuuid2,mbn.getUuid());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBUpdate() fail !");
		}
	}
	/*
	@Test
	public void testCIllegalToDBUpdate() {
		try {
			//write
			updateUUID(UUID.fromString("$F%*@!VU&adgE#(GU()#$455xcbs55"),usualuuid2);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = MemberBookNote.getNoteByUUid(em,unusualuuid1.toString());
			if(mbn == null){
				fail(TAG+" testFromDBDBFind() fail !");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testIllegalToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testDNullToDBUpdate() {
		try {
			//write
			updateUUID(null,usualuuid2);
			//read		
			em.getEntityManagerFactory().getCache().evictAll();
			MemberBookNote mbn = MemberBookNote.getNoteByUUid(em,null);
			assertEquals(null,mbn.getUuid());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testNullToDBUpdate() fail !");
		}
	}
	*/
	public void deleteTestItem() {
		try {
			em.getEntityManagerFactory().getCache().evictAll();
			MemberBookNote mbn = em.find(MemberBookNote.class, pk);
			em.getTransaction().begin();
			em.remove(mbn);
			em.getTransaction().commit();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" deleteTestItem() fail !");
		}
	}
	
	public void updateUUID(UUID newuuid,UUID olduuid){
		em.getEntityManagerFactory().getCache().evictAll();
		MemberBookNote mbn = MemberBookNote.getNoteByUUid(em,olduuid.toString());
		em.getTransaction().begin();
		mbn.setUuid(newuuid);
		em.getTransaction().commit();
	}

}
