package test.digipage.pgconverter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import model.Grafinfo;
import model.Member;
import model.MemberBook;
import model.MemberBookNote;
import model.NoteContent;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGNoteConverter {
	private static String TAG = "TestPGNoteConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	
	UUID uuid = UUID.randomUUID();
	
	String usualcfi1 = "9999";
	String usualcfi2 = "cfi1234";
	
	String unusualcfi1 = "$F%*@!VU&E#(GU()#$455";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	public void dBCreate() {
		try {
			Member m = Member.getRandomMember(em);
			MemberBook mb=m.getMemberBooks().get(0);
			em.getTransaction().begin();
			MemberBookNote mbn = new MemberBookNote();
			mbn.setUuid(uuid);
			NoteContent note = new NoteContent();
			note.setAuthor_nick("author");
			note.setCfi(usualcfi1);
			note.setChapter("1234");
			note.setColor("XXXX");
			List <Grafinfo> graffiti = new ArrayList<>();
			Grafinfo gi = new Grafinfo();
			gi.setPage("page");
			graffiti.add(gi);
			note.setGraffiti(graffiti);
			mbn.setNoteContent(note);
			mb.addMemberBookNote(mbn);
			em.getTransaction().commit();
			pk = mbn.getId();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" dBCreate() fail !");
		}
	}
	
	@Test
	public void testAFromDBDBFind() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = em.find(MemberBookNote.class, pk);
			NoteContent note = mbn.getNoteContent();
			assertEquals(usualcfi1, note.getCfi());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	@Test
	public void testBToDBUpdate() {
		try {
			//Write
			updateCfi(usualcfi2);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = em.find(MemberBookNote.class, pk);
			NoteContent note = mbn.getNoteContent();
			assertEquals(usualcfi2, note.getCfi());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testCIllegalToDBUpdate() {
		try {
			//Write
			updateCfi(unusualcfi1);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = em.find(MemberBookNote.class, pk);
			NoteContent note = mbn.getNoteContent();
			assertEquals(unusualcfi1, note.getCfi());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testIllegalToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testDNullToDBUpdate() {
		try {
			//Write
			updateCfi(null);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = em.find(MemberBookNote.class, pk);
			NoteContent note = mbn.getNoteContent();
			assertEquals(null, note.getCfi());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testNullToDBUpdate() fail !");
		}
	}
	
	public void deleteTestItem() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBookNote mbn = em.find(MemberBookNote.class, pk);
			em.getTransaction().begin();
			em.remove(mbn);
			em.getTransaction().commit();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" deleteTestItem() fail !");
		}
	}
	
	public void updateCfi(String cfi){
		em.getEntityManagerFactory().getCache().evictAll(); 
		MemberBookNote mbn = em.find(MemberBookNote.class, pk);
		NoteContent note = new NoteContent();
		em.getTransaction().begin();
		note.setCfi(cfi);
		mbn.setNoteContent(note);
		em.getTransaction().commit();
	}

}
