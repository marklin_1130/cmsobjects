package test.digipage.pgconverter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import model.MemberBook;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGArrayConverter {
	private static String TAG = "TestPGArrayConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	
	//Test value
	String usualName1 = "all";
	String usualName2 = "book";
	String usualName3 = "magazine";

	String unusualName1 = "g477bhnvdl^(*)($%#tg423j1mg";
	String unusualName2 = "#$T!@GYVFH(*&)$RBfgj00fnwk";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		deleteTestItem();
		em.close();
	}

	public void dBCreate() {
		try {
			insertData(usualName1,usualName2);
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBCreate() fail !");
		}
	}
	
	@Test
	public void testBFromDBDBFind() {
		try {
			List<String> set = new ArrayList<String>();
			set.add(usualName1);
			set.add(usualName2);
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBook testitem=em.find(MemberBook.class, pk);
			assertEquals(set, testitem.getReadlistIdnames());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	
	
	public void deleteTestItem() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBook testitem=em.find(MemberBook.class, pk);
			em.getTransaction().begin();
			em.remove(testitem);
			em.getTransaction().commit();			 
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testDeleteTestItem() fail !");
		}
	}
	
	public void insertData(String name1,String name2){
		em.getTransaction().begin();
		MemberBook mb = new MemberBook();
		mb.setBookUniId("XD1234");
		mb.addToReadList("all");
		em.persist(mb);
		em.getTransaction().commit();
		pk=mb.getId();
	}
	

}
