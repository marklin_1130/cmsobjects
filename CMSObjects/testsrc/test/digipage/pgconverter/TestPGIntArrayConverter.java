package test.digipage.pgconverter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import model.MemberBook;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGIntArrayConverter {
	private static String TAG = "TestPGIntArrayConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	
	int usualid1 = 123 ;
	int usualid2 = 456 ;
	int usualid3 = 789 ;
	int unusualid1 = -21^128784 ;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		deleteTestItem();
		em.close();
	}

	public void dBCreate() {
		try {
			em.getTransaction().begin();
			MemberBook mb = new MemberBook();
			List<Integer> set = new ArrayList<Integer>();
			set.add(usualid1);
			set.add(usualid2);
			mb.setDeviceId(set);
			mb.setBookUniId("Test1234");
			em.persist(mb);
			em.getTransaction().commit();
			pk=mb.getId();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBCreate() fail !");
		}
	}
	
	@Test
	public void testAFromDBDBFind() {
		try {
			List<Integer> set = new ArrayList<Integer>();
			set.add(usualid1);
			set.add(usualid2);
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBook testitem=em.find(MemberBook.class, pk);
			assertEquals(set, testitem.getDeviceId());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	@Test
	public void testBToDBUpdate() {
		try {
			//write
			List<Integer> set = new ArrayList<Integer>();
			set.add(usualid1);
			set.add(usualid3);
			updateDeviceId(set);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBook testitem=em.find(MemberBook.class, pk);
			assertEquals(set, testitem.getDeviceId());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testCIllegalToDBUpdate() {
		try {
			//write
			List<Integer> set = new ArrayList<Integer>();
			set.add(usualid1);
			set.add(unusualid1);
			updateDeviceId(set);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBook testitem=em.find(MemberBook.class, pk);
			assertEquals(set, testitem.getDeviceId());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testIllegalToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testDNullToDBUpdate() {
		try {
			//write
			updateDeviceId(null);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberBook testitem=em.find(MemberBook.class, pk);
			assertEquals(null, testitem.getDeviceId());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testNullToDBUpdate() fail !");
		}
	}
	
	public void deleteTestItem() {
		try {
			em.getEntityManagerFactory().getCache().evictAll();
			MemberBook testitem=em.find(MemberBook.class, pk);
			em.getTransaction().begin();
			em.remove(testitem);
			em.getTransaction().commit();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" deleteTestItem() fail !");
		}
	}
	
	public void updateDeviceId(List<Integer> set){
		em.getEntityManagerFactory().getCache().evictAll(); 
		MemberBook testitem=em.find(MemberBook.class, pk);
		em.getTransaction().begin();
		testitem.setDeviceId(set);
		em.persist(testitem);
		em.getTransaction().commit();
	}

}
