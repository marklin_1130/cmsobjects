package test.digipage.pgconverter;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.kohsuke.randname.RandomNameGenerator;

import model.BookInfo;
import model.Item;
import model.ItemInfo;
import model.Publisher;
import model.Vendor;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGItemInfoConverter {
	private static String TAG = "TestPGItemInfoConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static String pk = "";
	
	private static RandomNameGenerator rname = new RandomNameGenerator();
	private static Random rnd = new Random();
	private static List<Publisher> publishers = new ArrayList<>();
	private static List<Vendor> vendors = new ArrayList<>();
	
	String usuaisbn = "123456789";
	String unusualisbn = "$%&*GFV_)^**_%#FVJH*(&";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		deleteTestItem();
		em.close();
	}

	
	public void dBCreate() {
		try {
			em.getTransaction().begin();
			Item item = new Item();
			BookInfo bi = new BookInfo();
			bi.setItem("E050000033");
			bi.setC_title("圖解第一次投資原物料就上手");
			bi.setO_title("");
			bi.setType("book");
			bi.setIsbn("9789866434723");
			bi.setPublisher_id("easy");
			bi.setRank("1");
			bi.setPublish_date("2015/01/17");
			bi.setAuthor("陳育珩");
			bi.setO_author("");
			bi.setTranslator("");
			bi.setEditor("");
			bi.setIllustrator("");
			bi.setEdition("初版");
			bi.setForeword("");
			bi.setToc("<strong>第一篇 認識原物料</strong><br />什麼是原物料？<br />原物料的供需市場<br />為什麼原物料很重要？<br />全球重要原物料的分布<br />為何原物料產量與價格會變動？<br />為什麼要投資原物料？<br />原物料的投資工具<br />如何才能第一次投資原物料就上手？<br /><br /><strong>第二篇 認識原物料市場</strong><br />什麼是原物料市場？<br />原物料市場的分類<br />全球原物料市場的主要供給國家<br />全球原物料市場的供應大廠<br />全球農產品的主要供應商<br />全球金屬的主要供應商<br />全球能源的主要供應商<br />影響供給面價格的主要原因<br />全球原物料的主要需求國家<br />全球原物料市場的需求大廠<br />全球農產品的主要需求廠商<br />全球金屬的主要需求廠商<br />全球能源的主要需求廠商<br />影響消費面價格的主要因素<br />全球主要原物料交易市場<br />農產品的主要交易市場<br />金屬的主要交易市場<br />能源的主要交易市場<br /><br /><strong>第三篇 了解原物料走勢的規律</strong><br />影響原物料行情的關鍵因素<br />如何掌握農產品的價格趨勢？<br />");
			bi.setIntro("");
			bi.setEfile_cover_url("http://www.books.com.tw/img/001/066/21/0010662158.jpg");
			bi.setShare_flag("N");
			bi.setBook_format("0");
			bi.setPage_direction(0);
			bi.setLanguage("zh-tw");

			item.setInfo(bi);

			Publisher p = new Publisher();
			p.setBId("pubid_" + rname.next());
			p.setName("pname_" + rname.next());
			em.persist(p);
			publishers.add(p);

			Vendor v = new Vendor();
			v.setBId("VendBid_" + rname.next());
			v.setName("vname_" + rname.next());
			em.persist(v);
			vendors.add(v);

			item.setId("item_" + rname.next());
			item.setItemType("Book");
			item.setOperator(rname.next());
			Publisher curPublisher = publishers.get(rnd.nextInt(publishers.size()));
			item.setPublisher(curPublisher);
			Vendor curVendor = vendors.get(rnd.nextInt(vendors.size()));
			item.setVendor(curVendor);
			java.sql.Timestamp ts = java.sql.Timestamp.valueOf(LocalDateTime.now());
			item.setLastUpdated(ts);

			em.persist(item);
			em.getTransaction().commit();
			pk = item.getId();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" dBCreate() fail !");
		}
	}
	
	@Test
	public void testAFromDBFind() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			Item item = em.find(Item.class, pk);
			ItemInfo info = item.getInfo();
			BookInfo bi = (BookInfo) info;
			assertEquals("9789866434723", bi.getIsbn());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	@Test
	public void testBToDBUpdate() {
		try {
			//write
			updateIsbnDB(usuaisbn);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			Item item = em.find(Item.class, pk);
			ItemInfo info = item.getInfo();
			BookInfo bi = (BookInfo) info;
			assertEquals(usuaisbn, bi.getIsbn());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testCIllegalToDBUpdate() {
		try {
			//write
			updateIsbnDB(unusualisbn);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			Item item = em.find(Item.class, pk);
			ItemInfo info = item.getInfo();
			BookInfo bi = (BookInfo) info;
			assertEquals(unusualisbn, bi.getIsbn());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testIllegalToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testDNullToDBUpdate() {
		try {
			//write
			updateIsbnDB(null);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			Item item = em.find(Item.class, pk);
			ItemInfo info = item.getInfo();
			BookInfo bi = (BookInfo) info;
			assertEquals(null, bi);
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testNullToDBUpdate() fail !");
		}
	}
	
	
	public void deleteTestItem() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			Item item = em.find(Item.class, pk);
			em.getTransaction().begin();
			em.remove(item);
			em.getTransaction().commit();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" deleteTestItem() fail !");
		}
	}
	
	public void updateIsbnDB(String isbn){
		em.getEntityManagerFactory().getCache().evictAll(); 
		Item item = em.find(Item.class, pk);
		em.getTransaction().begin();
		ItemInfo info = item.getInfo();
		BookInfo bi = null;
		if(isbn != null){			
			bi = (BookInfo) info;
			bi.setIsbn(isbn);
		}
		
		item.setInfo(bi);
		em.persist(item);
		em.getTransaction().commit();
	}

}
