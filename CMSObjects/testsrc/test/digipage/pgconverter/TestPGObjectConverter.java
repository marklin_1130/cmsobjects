package test.digipage.pgconverter;

import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGObjectConverter {
	//none used
	private static String TAG = "TestPGObjectConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		deleteTestItem();
		em.close();
	}

	public void dBCreate() {
		try {

		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" dBCreate() fail !");
		}
	}

	@Test
	public void testAFromDBFind() {
		try {
			
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	@Test
	public void testBToDBUpdate() {
		try {
			//write

			//read
			
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBUpdate() fail !");
		}
	}	
	
	@Test
	public void testCIllegalToDBUpdate() {
		try {
			//write

			//read
			
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testIllegalToDBUpdate() fail !");
		}
	}
	
	@Test
	public void testDNullToDBUpdate() {
		try {
			//write

			//read
			
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testNullToDBUpdate() fail !");
		}
	}
	
	public void deleteTestItem() {
		try {
			
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testDeleteTestItem() fail !");
		}
	}


}
