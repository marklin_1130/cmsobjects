package test.digipage.pgconverter;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import model.Member;
import model.MemberDevice;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestPGcidrConverter {
	private static String TAG = "TestPGcidrConverter";
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	
	//Test value
	String usualip1 = "10.82.211.34";
	String usualip2 = "10.82.211.11";
	
	String unusualip1 = "10.fs.35.%";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		dBCreate();
	}

	@After
	public void tearDown() throws Exception {
		deleteTestItem();
		em.close();
	}

	public void dBCreate() {
		try {
			insertData(usualip1);
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBCreate() fail !");
		}
	}
	
	@Test
	public void testAFromDBDBFind() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberDevice item=em.find(MemberDevice.class, pk);
			assertEquals(usualip1, item.getLastOnlineIp());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testFromDBDBFind() fail !");
		}
	}
	
	@Test
	public void testBToDBUpdate() {
		try {
			//write
			updateDate(usualip2);			
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberDevice readitem=em.find(MemberDevice.class, pk);
			assertEquals(usualip2, readitem.getLastOnlineIp());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testToDBUpdate() fail !");
		}
	}
	/*
	@Test
	public void testCIllegalToDB(){
		try{
			updateDate(unusualip1);
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberDevice readitem=em.find(MemberDevice.class, pk);
			assertEquals(unusualip1, readitem.getLastOnlineIp());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testIllegalToDB() fail !");
		}
		
	}*/
	
	@Test
	public void testDNullToDB() {
		try {
			updateDate(null);			
			//read
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberDevice readitem=em.find(MemberDevice.class, pk);
			//assertEquals(null,readitem.getLastOnlineIp());
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testNullToDB() fail !");
		}
	}
	
	public void deleteTestItem() {
		try {
			em.getEntityManagerFactory().getCache().evictAll(); 
			MemberDevice testitem=em.find(MemberDevice.class, pk);
			em.getTransaction().begin();
			em.remove(testitem);
			em.getTransaction().commit();
		}catch(Exception ex){
			ex.printStackTrace();
			fail(TAG+" testDeleteTestItem() fail !");
		}
	}
	
	public void insertData(String ip1){
		em.getTransaction().begin();
		Member m = Member.getRandomMember(em);
		MemberDevice md = new MemberDevice();
		md.setMember(m);
		md.setLastOnlineIp(ip1);
		em.persist(md);
		em.getTransaction().commit();
		pk=md.getId();
		System.out.println("pk ="+pk);
	}
	
	public void updateDate(String ip1){
		em.getEntityManagerFactory().getCache().evictAll(); 
		MemberDevice testitem=em.find(MemberDevice.class, pk);
		em.getTransaction().begin();
		testitem.setLastOnlineIp(ip1);
		em.persist(testitem);
		em.getTransaction().commit();
	}

}
