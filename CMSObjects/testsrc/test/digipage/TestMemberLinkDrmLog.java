package test.digipage;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.boon.Boon.*;

import model.Member;
import model.MemberBook;
import model.MemberDrmLog;


public class TestMemberLinkDrmLog {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testMemberBookLinkDrmLog() {
		try {
			
			MemberBook mb =em.find(MemberBook.class, 180576L);
			if (mb==null)
			{
				fail();
			}
			
			Member m=mb.getMember();
			List<MemberDrmLog> drmLogs=m.getMemberDrmLogs();
			em.getTransaction().begin();
			for (MemberDrmLog mdl:drmLogs)
			{
				if (!mdl.getItem().equals(mb.getItem()))
					continue;

				em.persist(mb);
			}
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
