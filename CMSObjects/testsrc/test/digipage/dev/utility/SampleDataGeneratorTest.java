package test.digipage.dev.utility;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import digipages.dev.utility.SampleDataGenerator;

public class SampleDataGeneratorTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	public void testGenerateMemberDataString() {
		try {
			
			SampleDataGenerator sg = new SampleDataGenerator(em);
			sg.generateMemberData("chlang");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	public void testCloneSampleBook()
	{
		try {
			SampleDataGenerator sg = new SampleDataGenerator(em);
			em.getTransaction().begin();
			sg.genBooksFromSample(5);
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testCreateBenchgMembers()
	{
		try {
			SampleDataGenerator sg = new SampleDataGenerator(em);
			sg.genBenchMembers(10,10,5);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
