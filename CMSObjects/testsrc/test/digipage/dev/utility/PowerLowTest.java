package test.digipage.dev.utility;

import static org.junit.Assert.*;

import org.junit.Test;

import digipages.dev.utility.PowerLaw;

public class PowerLowTest {

	@Test
	public void test() {
		PowerLaw p = new PowerLaw();
		int size =400;
		int max = 0;
		int sum = 0;

		for (int i=0; i< 100; i++){
			int tmp=p.zipf(size);
			if (max <tmp)
				max = tmp;
			sum+=tmp;
		}
		System.out.println("Max:"+max);
		System.out.println("Avg:" + sum/100);
	}

}
