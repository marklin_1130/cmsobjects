package test.digipage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;

import digipages.BookConvert.TimeoutMonitor.TimeoutMonitorHandler;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.GenericLogRecord;
import digipages.BookConvert.utility.ResponseData;

public class TmpTest4ResponseData {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJsonResponseData() {
		try {
			
			Gson gson=new Gson();
			String responseDataString="{\"book_file_id\":\"222859\",\"call_type\":\"4\",\"count_TimeoutMointor\":0,\"efile_nofixed_name\":\"\",\"efile_url\":\"s3private.booksdev.digipage.info/src/6ce7020b-613a-4b0c-bb5e-683b74284c0c/\",\"forceCheck\":false,\"format\":\"reflowable\",\"isTrial\":true,\"item\":\"6ce7020b-613a-4b0c-bb5e-683b74284c0c\",\"preview_content\":\"背離親緣上long_paragraph_tooMany.epub\",\"preview_type\":\"3\",\"request_time\":\"2017-01-17T18:25:52+08:00\",\"return_file_num\":1,\"status\":\"A\",\"version\":\"V001.0001\"}";
			ResponseData responseData = gson.fromJson(responseDataString, 
					ResponseData.class);
//			System.out.println(responseData.toString());
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testJsonGenericLogRecord(){
		try {
			String src="{\"level\":\"Info\",\"message\":\"ResponseData:{\\\"item\\\":\\\"6ce7020b-613a-4b0c-bb5e-683b74284c0c\\\",\\\"call_type\\\":\\\"4\\\",\\\"processing_id\\\":\\\"222859\\\",\\\"file_url\\\":\\\"6028A7/222859\\\",\\\"return_file_num\\\":1,\\\"r_data\\\":{\\\"result\\\":true,\\\"size\\\":\\\"1994971\\\",\\\"version\\\":\\\"V001.0001\\\",\\\"error_message\\\":\\\"digipages.BookConvert.BookMain.EpubBook:Paragraph is too big. size\\\\u003d696 file: back1st-6.xhtml\\\\r\\\\nContent:\\\\r\\\\n結束早療之後，教育身心障礙兒的\\\\r\\\\n\\\\r\\\\ndigipages.BookConvert.BookMain.EpubBook:Paragraph is too big. size\\\\u003d789 file: back1st-5.xhtml\\\\r\\\\nContent:\\\\r\\\\n威廉．黑爾是侏儒，也是第一位以\\\\r\\\\nParagraph is too big. size\\\\u003d654 file: back1st-5.xhtml\\\\r\\\\nContent:\\\\r\\\\n美國小個子組織裡有些人認為，侏\\\\r\\\\n\\\\r\\\\ndigipages.BookConvert.BookMain.EpubBook:Paragraph is too big. size\\\\u003d746 file: back1st-7.xhtml\\\\r\\\\nContent:\\\\r\\\\n一般認為自閉症是一種廣泛性的失\\\\r\\\\nParagraph is too big. size\\\\u003d680 file: back1st-7.xhtml\\\\r\\\\nContent:\\\\r\\\\n某些常見的病症可能由單一異常的\\\\r\\\\nParagraph is too big. size\\\\u003d667 file: back1st-7.xhtml\\\\r\\\\nContent:\\\\r\\\\n這類行為或概念療法只做過最低限\\\\r\\\\nParagraph is too big. size\\\\u003d739 file: back1st-7.xhtml\\\\r\\\\nContent:\\\\r\\\\n談到這，就要提到神經多元運動，\\\\r\\\\nParagraph is too big. size\\\\u003d681 file: back1st-7.xhtml\\\\r\\\\nContent:\\\\r\\\\n墨菲爾斯有個自閉兒，她寫道：「\\\\r\\\\nParagraph is too big. size\\\\u003d949 file: back1st-7.xhtml\\\\r\\\\nContent:\\\\r\\\\n一九九七年，六歲的查爾斯安東尼\\\\r\\\\n\\\\r\\\\ndigipages.BookConvert.BookMain.EpubBook:Paragraph is too big. size\\\\u003d880 file: back1st-4.xhtml\\\\r\\\\nContent:\\\\r\\\\n保羅在聖經的〈羅馬書〉中稱：「\\\\r\\\\nParagraph is too big. size\\\\u003d705 file: back1st-4.xhtml\\\\r\\\\nContent:\\\\r\\\\n亞里斯多德認為「生來便不具某種\\\\r\\\\nParagraph is too big. size\\\\u003d643 file: back1st-4.xhtml\\\\r\\\\nContent:\\\\r\\\\n口語教育與手語教育孰優孰劣，各\\\\r\\\\n\\\\r\\\\ndigipages.BookConvert.BookMain.EpubBook:Warning: Big paragraph Summary, over 625 chars, total count\\\\u003d12\\\\r\\\\n\\\\r\\\\n[checkConsistency]:Start checkConsistency()\\\\r\\\\n[checkConsistency]:result: true\\\\r\\\\nEnd checkConsistency() spend 3616ms\\\"}}\\r\\nURL:https://bookapi.booksdev.digipage.info/V1.4/CMSAPIBook/BookProcessResult\\r\\nResponseCode:200\\r\\nResponse:%7B%22item%22%3A%226ce7020b-613a-4b0c-bb5e-683b74284c0c%22%2C%22call_type%22%3A%224%22%2C%22processing_id%22%3A%22222859%22%2C%22file_url%22%3A%226028A7%2F222859%22%2C%22return_file_num%22%3A1%2C%22r_data%22%3A%7B%22result%22%3Atrue%2C%22size%22%3A%221994971%22%2C%22version%22%3A%22V001.0001%22%2C%22error_message%22%3A%22digipages.BookConvert.BookMain.EpubBook%3AParagraph+is+too+big.+size%5Cu003d696+file%3A+back1st-6.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E7%B5%90%E6%9D%9F%E6%97%A9%E7%99%82%E4%B9%8B%E5%BE%8C%EF%BC%8C%E6%95%99%E8%82%B2%E8%BA%AB%E5%BF%83%E9%9A%9C%E7%A4%99%E5%85%92%E7%9A%84%5Cr%5Cn%5Cr%5Cndigipages.BookConvert.BookMain.EpubBook%3AParagraph+is+too+big.+size%5Cu003d789+file%3A+back1st-5.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E5%A8%81%E5%BB%89%EF%BC%8E%E9%BB%91%E7%88%BE%E6%98%AF%E4%BE%8F%E5%84%92%EF%BC%8C%E4%B9%9F%E6%98%AF%E7%AC%AC%E4%B8%80%E4%BD%8D%E4%BB%A5%5Cr%5CnParagraph+is+too+big.+size%5Cu003d654+file%3A+back1st-5.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E7%BE%8E%E5%9C%8B%E5%B0%8F%E5%80%8B%E5%AD%90%E7%B5%84%E7%B9%94%E8%A3%A1%E6%9C%89%E4%BA%9B%E4%BA%BA%E8%AA%8D%E7%82%BA%EF%BC%8C%E4%BE%8F%5Cr%5Cn%5Cr%5Cndigipages.BookConvert.BookMain.EpubBook%3AParagraph+is+too+big.+size%5Cu003d746+file%3A+back1st-7.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E4%B8%80%E8%88%AC%E8%AA%8D%E7%82%BA%E8%87%AA%E9%96%89%E7%97%87%E6%98%AF%E4%B8%80%E7%A8%AE%E5%BB%A3%E6%B3%9B%E6%80%A7%E7%9A%84%E5%A4%B1%5Cr%5CnParagraph+is+too+big.+size%5Cu003d680+file%3A+back1st-7.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E6%9F%90%E4%BA%9B%E5%B8%B8%E8%A6%8B%E7%9A%84%E7%97%85%E7%97%87%E5%8F%AF%E8%83%BD%E7%94%B1%E5%96%AE%E4%B8%80%E7%95%B0%E5%B8%B8%E7%9A%84%5Cr%5CnParagraph+is+too+big.+size%5Cu003d667+file%3A+back1st-7.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E9%80%99%E9%A1%9E%E8%A1%8C%E7%82%BA%E6%88%96%E6%A6%82%E5%BF%B5%E7%99%82%E6%B3%95%E5%8F%AA%E5%81%9A%E9%81%8E%E6%9C%80%E4%BD%8E%E9%99%90%5Cr%5CnParagraph+is+too+big.+size%5Cu003d739+file%3A+back1st-7.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E8%AB%87%E5%88%B0%E9%80%99%EF%BC%8C%E5%B0%B1%E8%A6%81%E6%8F%90%E5%88%B0%E7%A5%9E%E7%B6%93%E5%A4%9A%E5%85%83%E9%81%8B%E5%8B%95%EF%BC%8C%5Cr%5CnParagraph+is+too+big.+size%5Cu003d681+file%3A+back1st-7.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E5%A2%A8%E8%8F%B2%E7%88%BE%E6%96%AF%E6%9C%89%E5%80%8B%E8%87%AA%E9%96%89%E5%85%92%EF%BC%8C%E5%A5%B9%E5%AF%AB%E9%81%93%EF%BC%9A%E3%80%8C%5Cr%5CnParagraph+is+too+big.+size%5Cu003d949+file%3A+back1st-7.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E4%B8%80%E4%B9%9D%E4%B9%9D%E4%B8%83%E5%B9%B4%EF%BC%8C%E5%85%AD%E6%AD%B2%E7%9A%84%E6%9F%A5%E7%88%BE%E6%96%AF%E5%AE%89%E6%9D%B1%E5%B0%BC%5Cr%5Cn%5Cr%5Cndigipages.BookConvert.BookMain.EpubBook%3AParagraph+is+too+big.+size%5Cu003d880+file%3A+back1st-4.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E4%BF%9D%E7%BE%85%E5%9C%A8%E8%81%96%E7%B6%93%E7%9A%84%E3%80%88%E7%BE%85%E9%A6%AC%E6%9B%B8%E3%80%89%E4%B8%AD%E7%A8%B1%EF%BC%9A%E3%80%8C%5Cr%5CnParagraph+is+too+big.+size%5Cu003d705+file%3A+back1st-4.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E4%BA%9E%E9%87%8C%E6%96%AF%E5%A4%9A%E5%BE%B7%E8%AA%8D%E7%82%BA%E3%80%8C%E7%94%9F%E4%BE%86%E4%BE%BF%E4%B8%8D%E5%85%B7%E6%9F%90%E7%A8%AE%5Cr%5CnParagraph+is+too+big.+size%5Cu003d643+file%3A+back1st-4.xhtml%5Cr%5CnContent%3A%5Cr%5Cn%E5%8F%A3%E8%AA%9E%E6%95%99%E8%82%B2%E8%88%87%E6%89%8B%E8%AA%9E%E6%95%99%E8%82%B2%E5%AD%B0%E5%84%AA%E5%AD%B0%E5%8A%A3%EF%BC%8C%E5%90%84%5Cr%5Cn%5Cr%5Cndigipages.BookConvert.BookMain.EpubBook%3AWarning%3A+Big+paragraph+Summary%2C+over+625+chars%2C+total+count%5Cu003d12%5Cr%5Cn%5Cr%5Cn%5BcheckConsistency%5D%3AStart+checkConsistency%28%29%5Cr%5Cn%5BcheckConsistency%5D%3Aresult%3A+true%5Cr%5CnEnd+checkConsistency%28%29+spend+3616ms%22%7D%7D\\r\\nefileProcessResult url\\u003dhttp://localhost:8080/V1.4/CMSAPIBook/fakeAccepterGet Request from Client:\\\\A\\r\\nEnd NotifyAPI spend 2134ms\",\"source\":\"digipages.BookConvert.NotifyAPI.NotifyAPIHandler\"}";
			String x=TimeoutMonitorHandler.extractResponseStr(src);
			Gson gson=new Gson();
			ResponseData r=gson.fromJson(x, ResponseData.class);
			System.out.println(r.toString());
			
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
