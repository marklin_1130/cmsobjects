package test.digipage;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.boon.Boon.*;

import model.Member;
import model.MemberBook;
import model.MemberDrmLog;


public class TestDrmLogSelect {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testAddDRMLogUnique() {
		try {
			List <MemberDrmLog> mds = em.createNativeQuery("select * from member_drm_log WHERE member_id = 3598 OFFSET 0 LIMIT 100",MemberDrmLog.class).getResultList();
			for (MemberDrmLog md:mds)
			{
				System.out.println(md.getId() + md.getItem().getInfo().getName());
			}
						
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
