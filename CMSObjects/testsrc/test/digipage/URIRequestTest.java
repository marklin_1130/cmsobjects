package test.digipage;

import static org.junit.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class URIRequestTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		HttpUriRequest booksAuth;
		try {
			booksAuth = RequestBuilder.get()
					.setUri(new URI("http://www.google.com.tw/"))
					.addParameter("client_id", "1234")
					.addParameter("client_secret", "5678")
					.addParameter("code", "910123")
					.addParameter("redirect_uri", "aabbaab").build();
			System.out.println(booksAuth.getRequestLine().toString());
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
		}
	}

}
