package test.digipage;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.BookInfo;
import model.Item;
import model.ItemInfo;
import model.MemberDevice;

public class TestPGCidrConverter {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}
	

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testToDBConverter() {
		try {
			em.getTransaction().begin();
			MemberDevice md = new MemberDevice();
			md.setLastOnlineIp("192.168.53.109");
			em.persist(md);
			em.getTransaction().commit();
			pk=md.getId();
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testFromDBConverter() {
		try {
			MemberDevice item=em.find(MemberDevice.class, pk);
			assertEquals("192.168.53.109", item.getLastOnlineIp());
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
