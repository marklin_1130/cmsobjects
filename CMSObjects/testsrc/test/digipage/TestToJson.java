package test.digipage;

import static org.junit.Assert.*;

import org.boon.json.JsonFactory;
import org.boon.json.ObjectMapper;
import org.junit.Test;

import model.BookInfo;
import static org.boon.json.JsonFactory.toJson;

public class TestToJson {

	@Test
	public void test() {
		
		ObjectMapper mapper = JsonFactory.createUseAnnotations( true );
		
		BookInfo bi = new BookInfo();
		bi.setType("Book");
		bi.setBook_format("222222");
		String json = mapper.toJson(bi);
		boolean hasBookmark = json.contains("type");
		assertTrue(hasBookmark);
	}

}
