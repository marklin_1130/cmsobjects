package test.digipage;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.BookInfo;
import model.Item;
import model.ItemInfo;
import model.Member;
import model.MemberDevice;

public class TestUdtAutoGenerator {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static long pk;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}
	

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testToDBConverter() {
		try {
			em.getTransaction().begin();
			Member m = new Member();
			m.setName("123");
			m.setNick("hello");
			em.persist(m);
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	

}
