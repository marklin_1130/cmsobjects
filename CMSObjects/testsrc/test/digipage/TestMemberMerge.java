package test.digipage;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import digipages.dev.utility.SampleDataGenerator;
import digipages.exceptions.ServerException;

import static org.boon.Boon.*;

import model.Member;
import model.MemberBook;
import model.MemberDrmLog;


public class TestMemberMerge {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testMemberMerge() {
		try {

			// init
			em.getTransaction().begin();
			Member guestMember = createMemberWithBookNote();
			Member realMember = createMemberWithBookNote();
			em.persist(guestMember);
			em.persist(realMember);
			em.getTransaction().commit();
			
			em.refresh(guestMember);
			em.refresh(realMember);
			// count total memberBookNote:
			int totalNoteCnt=countNoteCnt(guestMember)+countNoteCnt(realMember);
			em.detach(guestMember);
			em.detach(realMember);
			
			
			
			// real thing:
			guestMember = em.find(Member.class, guestMember.getId());
			realMember = em.find(Member.class, realMember.getId());
			em.getTransaction().begin();
			System.out.println("m1Json:" + toJson(guestMember));
			System.out.println("m2Json:" + toJson(realMember));
			realMember.mergeFrom(em,guestMember);
			System.out.println("Merge Finished:\n");
			System.out.println("m1Json:" + toJson(guestMember));
			System.out.println("m2Json:" + toJson(realMember));
			em.persist(realMember);
			em.getTransaction().commit();
			
			
			// reload
			em.refresh(realMember);
			em.detach(realMember);
			realMember = em.find(Member.class, realMember.getId());
			
			int bookNoteCnt=countNoteCnt(realMember);
			System.out.println("Excepting:" + totalNoteCnt + "\t got:"+ bookNoteCnt);
			
			if (bookNoteCnt!=totalNoteCnt)
			{
				fail();
			}
			
			em.getTransaction().begin();
			em.remove(realMember);
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	private int countNoteCnt(Member m2) {
		int bookNoteCnt=0;
		for (MemberBook mb:m2.getMemberBooks())
		{
			bookNoteCnt+=mb.getMemberBookNotes().size();
		}

		return bookNoteCnt;
	}

	private Member createMemberWithBookNote() throws ServerException {
		SampleDataGenerator gen = new SampleDataGenerator(em);
		SampleDataGenerator.setMemberBookCnt(2);
		SampleDataGenerator.setMemberBookNoteCnt(2);
		Member m =gen.generateBenchMember();
		return m;
	}

}
