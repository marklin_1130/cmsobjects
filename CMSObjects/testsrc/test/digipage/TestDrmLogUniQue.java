package test.digipage;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Member;
import model.MemberBook;
import model.MemberDrmLog;


public class TestDrmLogUniQue {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testAddDRMLogUnique() {
		try {
			Member m = Member.getRandomMember(em);
			MemberDrmLog drmLog=new MemberDrmLog();
			
			MemberBook mb=m.getMemberBooks().get(0);
			drmLog.setItem(mb.getItem());
			drmLog.setReadDays(0);
			drmLog.setTransactionId("1112");
			drmLog.setType(1);
			
			em.getTransaction().begin();
			m.addMemberDrmLog(drmLog);
			em.getTransaction().commit();
			
			MemberDrmLog drmLog2 = new MemberDrmLog();
//			mb = m.getMemberBooks().get(1);
			drmLog2.setItem(mb.getItem());
			drmLog2.setReadDays(0);
			drmLog2.setTransactionId("1112");
			drmLog2.setType(1);
			
			em.getTransaction().begin();
			m.addMemberDrmLog(drmLog2);
			em.getTransaction().commit();
			
			
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
