package test.digipage;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ PublisherTest.class, TestCmsToken.class, TestDrmLogSelect.class, TestDrmLogUniQue.class,
		TestGetGuestMemberByDeviceId.class, TestItemInfo.class, TestJSonSerialize.class, TestMemberLinkDrmLog.class,
		TestMemberMerge.class, TestOrderRandomPerformance.class, TestPGCidrConverter.class, TestPGJsonConverter.class,
		TestPublisherByIDBenchmark.class, TestRegexReplace.class, TestSort.class, TestToJson.class,
		TestUdtAutoGenerator.class, URIRequestTest.class, VendorRandomGetter.class })
public class AllTests {

}
