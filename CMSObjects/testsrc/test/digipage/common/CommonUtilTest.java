package test.digipage.common;

import static org.junit.Assert.*;

import org.junit.Test;

import digipages.common.CommonUtil;

public class CommonUtilTest {

	@Test
	public void testGetRequestHost() {
		String result=CommonUtil.getRequestHost("http://viewer.booksdev.digipage.info/viewer/index.html?readlist=magazine");
		assertEquals("http://viewer.booksdev.digipage.info", result);
	}

}
