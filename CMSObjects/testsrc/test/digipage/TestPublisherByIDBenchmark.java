package test.digipage;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Publisher;
import model.Member;


public class TestPublisherByIDBenchmark {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static Connection connection = null;
	private static int cnt=1000;
	private static Random rand = new Random();
	
	private static String [] bids={"easy"
			,"catchon"
			,"psp"
			,"water"
			,"emodern"
			,"hsiangshu"
			,"owl"
			,"business"
			,"myhouse"
			,"mook"
			,"bookcul"
			,"trendy"
			,"cite1"
			,"vf"
			,"apexcite"
			,"magicbase"
			,"popo1"
			,"rye"
			,"pcuser"
			,"smallwheat"
			,"spring"
			,"ryenews"
			,"face"
			,"marcopolo"
			,"mcommon2"
			,"common"
			,"mcommon11"
			,"mcommon8"
			};

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		
		Class.forName("org.postgresql.Driver");
		connection = DriverManager.getConnection(
		   "jdbc:postgresql://postgres/booksdb","booksAPI", "BenQ1234");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
		connection.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	/** 
	 * according to test, benchmark bottleneck = random, jpa overhead from jdbc is extreme small.
	 * jpa: 502ms, jdbc 476ms, native sql 517 ms (all are slow) 
	 */
	@Test
	public void testJPAfindByBid() {
		try {
			long startTime = System.currentTimeMillis();
			List <Publisher> Publishers= new ArrayList<>();
			for (int i=0; i< cnt; i++)
			{
				Publisher bf = Publisher.findPublisherBybid(em,getRandombid());
				Publishers.add(bf);
			}
			long duration = (System.currentTimeMillis()-startTime);
			System.out.println("TotalTime in find by bid JPA:" + duration);
			System.out.println("Avg process time in jpa:" + duration/(double)cnt);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	
	private String getRandombid() {
		
		return bids[rand.nextInt(bids.length)];
	}

	@Test 
	public void testJDBCfindByBid()
	{
		
		try {
			PreparedStatement psd = connection.prepareStatement("select * from publisher where b_id=? limit 1");
			long startTime = System.currentTimeMillis();
			List <Publisher> Publishers= new ArrayList<>();
			for (int i=0; i< cnt; i++)
			{
				psd.setString(1,getRandombid());
				ResultSet rs = psd.executeQuery();
				while (rs.next())
				{
					Publisher bf = Publisher.fromResultSet(rs);
					Publishers.add(bf);
				}
			}
			long duration = (System.currentTimeMillis()-startTime);
			System.out.println("TotalTime in find by bid jdbc:" + duration);
			System.out.println("Avg process time in jdbc:" + duration/(double)cnt);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
