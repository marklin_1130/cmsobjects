package test.digipage;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Member;


public class TestGetGuestMemberByDeviceId {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testGetGuestByDeviceId() {
		try {
			Member guest=Member.getGuestByDeviceId(em, "DF9AAC24-9E31-4684-B2A0-91AD32448BE3");
			List <Member> normalMembers= Member.listNormalReaderMembers(em);
			System.out.println("cnt = " + normalMembers.size());
			System.out.println(guest.getBmemberId() + ":" + guest.getId());
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
