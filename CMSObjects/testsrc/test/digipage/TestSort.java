package test.digipage;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import model.*;
public class TestSort {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		List <MemberBookNote> booknotes = new ArrayList<>();
		
			for (int i=0; i< 10; i++){
				booknotes.add(newMemberBookNote(i));
				System.out.println(i);
			}
			Collections.sort(booknotes,new Comparator<MemberBookNote>() {
	
				@Override
				public int compare(MemberBookNote o1, MemberBookNote o2) {
					
					return -(int)(o1.getLikeCnt()-o2.getLikeCnt());
				}
			});
		
		System.out.println("=================");
		for (int i=0;i<10;i++)
		{
			MemberBookNote bn = booknotes.get(i);
			System.out.println(bn.getLikeCnt());
		}
	}

	private MemberBookNote newMemberBookNote(int i) {
		MemberBookNote bn = new MemberBookNote();
		bn.setLikeCnt((long)i);
		return bn;
	}

}
