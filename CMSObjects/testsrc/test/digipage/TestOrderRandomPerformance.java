package test.digipage;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.BookFile;
import model.Member;


public class TestOrderRandomPerformance {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static Connection connection = null;
	private static int cnt=10;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		
		Class.forName("org.postgresql.Driver");
		connection = DriverManager.getConnection(
		   "jdbc:postgresql://postgres/booksdb","booksAPI", "BenQ1234");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
		connection.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	/** 
	 * according to test, benchmark bottleneck = random, jpa overhead from jdbc is extreme small.
	 * jpa: 502ms, jdbc 476ms, native sql 517 ms (all are slow) 
	 */
	@Test
	public void testJPARandom() {
		try {
			long startTime = System.currentTimeMillis();
			List <BookFile> bookFiles= new ArrayList<>();
			for (int i=0; i< cnt; i++)
			{
				BookFile bf = BookFile.getRandomBookFile(em);
				bookFiles.add(bf);
			}
			long duration = (System.currentTimeMillis()-startTime);
			System.out.println("TotalTime in random JPA:" + duration);
			System.out.println("Avg process time in jpa:" + duration/(double)cnt);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	
	@Test 
	public void testJDBCRandom()
	{
		
		try {
			PreparedStatement psd = connection.prepareStatement("select * from book_file order by random() limit 1");
			long startTime = System.currentTimeMillis();
			List <BookFile> bookFiles= new ArrayList<>();
			for (int i=0; i< cnt; i++)
			{
				ResultSet rs = psd.executeQuery();
				while (rs.next())
				{
					BookFile bf = BookFile.fromResultSet(rs);
					bookFiles.add(bf);
				}
			}
			long duration = (System.currentTimeMillis()-startTime);
			System.out.println("TotalTime in random jdbc:" + duration);
			System.out.println("Avg process time in jdbc:" + duration/(double)cnt);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
