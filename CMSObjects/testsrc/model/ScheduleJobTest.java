package model;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

public class ScheduleJobTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
		emf.close();
	}

	@Test
	public void testListTimeoutRetryJobs() {
		try {
			List <ScheduleJob> jobs=ScheduleJob.listTimeoutRetryJobs(em);
			Gson gson =new Gson();
			for (Object job:jobs)
			{
				System.out.println("job:" + gson.toJson(job));
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
