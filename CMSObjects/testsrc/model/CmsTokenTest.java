package model;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import digipages.exceptions.ServerException;

public class CmsTokenTest {

	CmsToken token = new CmsToken();
	
	@Before
	public void setUp() throws Exception {
		token.setClientIp("12345");
		token.setDeviceId(99999L);
		token.setDeviceStr("99123801");
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDecryString() {
		try {
			String encStr=URLEncoder.encode("95dc13AHDWw7HAMn0qM34qmU2E2jfRyapr3HEM1xtZPuucMksW3RWkH3RavXxl0yO6UXT5feKuamXBSJIr1MCQw8vlQToBad2hyG9F3On4oqhWDBmCfrVzwOiLVE2uSILrxmWlPlcxUS8QvSEyi0fFUPyKbMAvGTJQNz1toGIzfSVMQWzGymCC9T1Xk0zzVWKyOYDFIR5386iU/2hK6q+xXgz+xQ==", "UTF-8");
			CmsToken token2 = CmsToken.fromString(encStr);
			
			System.out.println(ReflectionToStringBuilder.reflectionToString(token2, ToStringStyle.SHORT_PREFIX_STYLE));
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
			
	@Test
	public void testFromString() {
		try {
			String encStr=token.toEncStr();
			CmsToken token2 = CmsToken.fromString(encStr);
			if (!token2.getClientIp().equals("12345"))
				fail();
			if (token2.getDeviceId()!=99999L)
				fail();
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | UnsupportedEncodingException | ServerException e) {

			e.printStackTrace();
			fail();
		}
		
	}

	@Test
	public void testToEncStr() {
		try {
			String x=token.toEncStr();
			System.out.println("enced=" + x);
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
