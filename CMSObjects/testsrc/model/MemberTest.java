package model;

import static org.junit.Assert.*;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.boon.Boon.*;

import digipages.exceptions.ServerException;

public class MemberTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();

	}

	@After
	public void tearDown() throws Exception {
		em.close();
		emf.close();
	}

//	@Test
	public void testMergeFrom() {
		try{
			em.getTransaction().begin();
			Member tmpGuest=createGuestMemberWitBook("");
			em.persist(tmpGuest);
			em.remove(tmpGuest);
			Member mergeParent=em.find(Member.class, 43729L); // super big member
			mergeParent.mergeFrom(em, tmpGuest);
			em.getTransaction().commit();
			System.out.println(toJson(mergeParent));
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testFindById()
	{
		// examine query string
		try {
			Member realMember = Member.getMemberByBMemberId(em, "", "");
			if (realMember!=null)
			{
				fail();
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

	private Member createGuestMemberWitBook(String string) throws ServerException {
		Member m = new Member();
		m.setMemberType("guest");
		m.setName("tmpGuest");
		MemberBook mb = new MemberBook();
		BookFile bf = em.find(BookFile.class, 221825);
		mb.updateByBookFile(bf);
		MemberBookNote mbn = new MemberBookNote();
		mbn.setUuid(UUID.randomUUID());
		NoteContent noteContent = new NoteContent();
		mbn.setNoteContent(noteContent);
		mbn.setNote("1234");
		mbn.setText("12354");
		mbn.setType("bookmark");
		mbn.setVersion("V001.0001");
		mb.addMemberBookNote(mbn);
		
		m.addMemberBook(mb);
		
		return m;
	}

}
