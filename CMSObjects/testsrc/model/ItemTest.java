package model;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ItemTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	private static EntityManagerFactory emf;
	private static EntityManager em;

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();

	}

	@After
	public void tearDown() throws Exception {
		em.close();
		emf.close();
	}

//	@Test
	public void testGetAvailTrialBookCnt() {
		Item item=em.find(Item.class, "0000000220");
		if (4!=item.getAvailTrialBookCnt(em))
		{
			fail();
		}
		item=em.find(Item.class, "0111");
		if (3!=item.getAvailTrialBookCnt(em))
		{
			fail();
		}
		
		item=em.find(Item.class, "004");
		if (0!=item.getAvailTrialBookCnt(em)){
			fail();
		}
				
		
	}
	@Test
	public void testNewestTrialVersion()
	{
		try {
			Item item=em.find(Item.class, "0000000213");
			String x=item.genNewTrialVersion(em, true);
			System.out.println("get new Version of trial:" + x);
			
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testNewestNormalVersion()
	{
		try {
			Item item=em.find(Item.class, "0000000213");
			String x=item.genNewNormalVersion(em, false);
			System.out.println("get new Version of Normal:" + x);
			
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
