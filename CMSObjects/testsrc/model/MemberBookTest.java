package model;

import static org.boon.Boon.toJson;
import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class MemberBookTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();

	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
	public void testRemoveReadList() {
		try {
			em.getTransaction().begin();
			MemberBook.removeReadList(em, 3607l, "book");
			em.getTransaction().commit();
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}
//	@Test
	public void testListMemberUnread()
	{
		try{
			Member member = Member.getMemberByBMemberId(em, "chlang", Member.MEMBERTYPE_NORMALREADER);
			int total = 
					((Number) em.createNamedQuery("MemberBook.countAllUnreadBookByBooksMemberId").setParameter("memberId", member.getId()).getSingleResult()).intValue();
			assertEquals(1,total);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
