package digipages.CommonJobHandler;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.common.DPLogger;
import model.ScheduleJob;

/**
 * notice: this test case can be only used for http client test, must start a web server for it.
 * (disabled by default)
 * @author Eric.CL.Chou
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JobHandlerRESTClientTest {
	private static final DPLogger logger =DPLogger.getLogger(JobStatusReporter.class.getName());
	private static List<ScheduleJob> currentJobs=new ArrayList<>();
	
	@Before
	public void setUp() throws Exception {
//		JobStatusReporter.setBaseURI("http://localhost:8080/CMSAPIBook/");
	}

	@After
	public void tearDown() throws Exception {
	}




	@Test
	public void testAddJobs() {
		try {
			List <ScheduleJob> jobs = genJobs();
			String response;
			response = JobStatusReporter.addJobs(jobs);
			System.out.println("job added: "+response);
		} catch (IOException e) {
			logger.error("",e);
			fail();
		}
	}

	
	@Test
	public void testGetJobs() {
		String uuid = UUID.randomUUID().toString();
		try {
			List <ScheduleJob> jobs = new ArrayList<>();
			do {
				jobs=JobStatusReporter.getJobs(uuid, 2, 1);
				currentJobs.addAll(jobs);
			} while (jobs.size()>0);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Connection error");
		}
	}
	
	
	@Test
	public void testUpdateJobs() {
		try {
			for (ScheduleJob tmpJob:currentJobs)
			{
				tmpJob.setStatus(9);
				tmpJob.setLastUpdated(new Date());
			}
			
			String response=JobStatusReporter.updateJobs(currentJobs);
			System.out.println("Finish jobs: " + response);
			
		} catch (IOException e) {
			e.printStackTrace();
			fail("Connection error");
		}
	}

	private List<ScheduleJob> genJobs() {
		List<ScheduleJob> jobs = new ArrayList<>();
		for (int i=1;i<5;i++){
			ScheduleJob j = new ScheduleJob();
			j.setJobName("TestPostJobs");
			j.setJobParam("Ec2 Test Jobs"+i);
			j.setJobRunner(2);
			jobs.add(j);
		}
		return jobs;
	}

}
