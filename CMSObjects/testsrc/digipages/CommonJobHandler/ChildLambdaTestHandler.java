package digipages.CommonJobHandler;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class ChildLambdaTestHandler implements CommonJobHandlerInterface{

	private static ChildLambdaTestHandler myInstance= new ChildLambdaTestHandler();
	private static DPLogger logger = DPLogger.getLogger(ChildLambdaTestHandler.class.getName());

	@Override
	public String mainHandler(Properties config,ScheduleJob job) throws ServerException {
		try {
			Thread.sleep(2000);
			logger.debug("Child processed  "+ job.getJobName() + " Runner=" + job.getJobRunner());
		} catch (Exception e)
		{
			logger.error("fail to Sleep ?!",e);
			throw new ServerException("id_err_999","UnExcepted Error:" + e.getMessage());
		}
		return "Child processed  "+ job.getJobName() + " Runner=" + job.getJobRunner();
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory em) {
		// since I have no need to use em.
	}


	
	public static ChildLambdaTestHandler getInstance()
	{
		return myInstance;
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return null;
	}

	public static RunMode getInitRunMode() {
		return RunMode.LambdaMode;
	}


}
