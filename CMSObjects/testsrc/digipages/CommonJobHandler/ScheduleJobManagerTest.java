package digipages.CommonJobHandler;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ScheduleJobManagerTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
		emf.close();
	}

	@Test
	public void testUpdateTimeoutStatus() {
		try {
			em.getTransaction().begin();
			ScheduleJobManager.updateTimeoutStatus(em);
			em.getTransaction().commit();
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
