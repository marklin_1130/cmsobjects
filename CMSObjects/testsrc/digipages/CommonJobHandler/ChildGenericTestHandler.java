package digipages.CommonJobHandler;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import com.google.gson.Gson;

import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class ChildGenericTestHandler implements CommonJobHandlerInterface{

	private DPLogger logger= DPLogger.getLogger(ChildGenericTestHandler.class.getName());
	private static ChildGenericTestHandler myInstance=new ChildGenericTestHandler(); 

	@Override
	/**
	 * auto fall back to EC2 Runner
	 */
	public String mainHandler(Properties config,ScheduleJob job) throws ServerException, JobRetryException {
		Gson gson = new Gson();
		String parameterStr = gson.toJson(job.getJobParam(Object.class));
		
		int processDelay = 2;
		if (parameterStr.contains("delay"))
		{
			processDelay=10;
		}
		
		if (job.getJobRunner().equals(RunMode.LambdaMode.getValue()))
		{
			throw new JobRetryException("Lambda fail and re-schedule");
		}
		
		
		try {
			Thread.sleep(processDelay*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new ServerException("id_err_999", e.getMessage());
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("Child processed  ").append(job.getJobName())
		.append(" Runner=").append(job.getJobRunner())
		.append(" Parameter=").append( parameterStr);
		logger.debug(sb.toString());

		return "Finish with result: " + sb.toString();
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// not used.
	}


	public static ChildGenericTestHandler getInstance()
	{
		return myInstance;
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		
		return RunMode.ExtEc2Mode;
	}

	public static RunMode getInitRunMode() {
		
		return RunMode.LambdaMode;
	}

}
