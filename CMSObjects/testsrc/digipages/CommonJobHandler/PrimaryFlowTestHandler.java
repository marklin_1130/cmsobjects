package digipages.CommonJobHandler;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.ScheduleJobParam;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class PrimaryFlowTestHandler implements CommonJobHandlerInterface{

	private static CommonJobHandlerInterface myInstance=new PrimaryFlowTestHandler();
	private EntityManagerFactory emf;
	
	public PrimaryFlowTestHandler() {
	}


	@Override
	public String mainHandler(Properties config,ScheduleJob job) throws ServerException {

		
		try {
			ScheduleJob jobB = new ScheduleJob();
			jobB.setJobGroup("JobGroupB");
			jobB.setJobName(ChildLocalTestHandler.class.getName());
			jobB.setJobRunner(RunMode.LocalMode.getValue());
			jobB.setJobParam("LocalRunner2");
			
			ChildLocalTestHandler.getInstance().setEntityManagerFactory(emf);
			ScheduleJobManager.addJob(jobB);
			
			ScheduleJob jobB2 = new ScheduleJob();
			jobB2.setJobGroup("JobGroupB");
			jobB2.setJobName(ChildLocalTestHandler.class.getName());
			jobB2.setJobRunner(RunMode.LocalMode.getValue());
			jobB2.setJobParam("LocalRunner3");
			jobB2.setShouldStartTime(CommonUtil.nextNMinutes(1));
			ScheduleJobParam x = job.getJobParam(ScheduleJobParam.class);
			System.out.println("Job runned with Parameter " + x);
			// same name handler, don't have 2nd one. or you will hit wall.
	//		ChildLocalTestHandler handlerB2 = new ChildLocalTestHandler(logger,config);
			ScheduleJobManager.addJob(jobB2);
			
			
			System.out.println("finish main job" + job.getId());
			
			ScheduleJobManager.finishJob(job);
			return "finish main job" + job.getId();
		} catch (Exception e) {
			
			throw new ServerException("id_err_999",e.getMessage());
		}
	}


	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf=emf;
		
	}


	
	public static CommonJobHandlerInterface getInstance() {
		return myInstance;
	}



	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {

		return RunMode.LocalMode;
	}


	
	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
	





}
