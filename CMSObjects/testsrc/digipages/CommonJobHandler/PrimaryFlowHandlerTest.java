package digipages.CommonJobHandler;

import static org.junit.Assert.fail;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.JobRecoverInternal;
import digipages.common.ScheduleJobParam;
import model.ScheduleJob;



public class PrimaryFlowHandlerTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	static BlockingQueue<ScheduleJob> jobQueue = new ArrayBlockingQueue<>(100);
	static LocalRunner localRunner ;
	static Thread localRunnerThread;

	private Scheduler scheduler;
	private SchedulerFactory schedulerFactory;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();

		// register handlers

		ScheduleJobManager.setEntityManagerFactory(emf);
		localRunner =  new LocalRunner(jobQueue,emf,null); 
		localRunnerThread = new Thread(localRunner);
		localRunnerThread.start();
		Ec2WorkerManager.setEntityManagerFactory(emf);
		
		System.out.println("LocalRunner Started...");
		
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		localRunner.finish();
		localRunnerThread.interrupt();
//		localRunnerThread.join();
		em.close();
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		quartzJob();
	}

	@After
	public void tearDown() throws Exception {
		shutdownQuartz();
	}

	private void shutdownQuartz() {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		
	}

	//	@Test
	public void testPrimaryDirectRunner() {
		try {
			ScheduleJob mainDirectjob = new ScheduleJob();
			mainDirectjob.setJobParam("PrimaryJobController");
			mainDirectjob.setJobName(mainDirectjob.getClass().getName());
			mainDirectjob.setJobGroup("testGroup");
			mainDirectjob.setJobRunner(RunMode.DirectRunMode.getValue());
			PrimaryFlowTestHandler.getInstance();
			ScheduleJobManager.addJob(mainDirectjob);
			Thread.sleep(800);
			
			
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testPrimaryLocalRunner() {
		try {
			// wait for local runner start.
			Thread.sleep(1000);
			ScheduleJob mainLocaljob = new ScheduleJob();
			
			ScheduleJobParam jobParams = new ScheduleJobParam();
			jobParams.setMember_id(1234L);
			jobParams.setItem_id("item_id_1234");
			mainLocaljob.setJobParam(jobParams);
			mainLocaljob.setJobName(PrimaryFlowTestHandler.class.getName());
			mainLocaljob.setJobGroup("testGroup");
			mainLocaljob.setJobRunner(RunMode.LocalMode.getValue());
			ScheduleJobManager.addJob(mainLocaljob);
			
			Thread.sleep(90000);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	public void quartzJob() throws Exception {
		try {
			
			Properties props = new Properties();
			props.setProperty(StdSchedulerFactory.PROP_SCHED_SKIP_UPDATE_CHECK,"true");
			schedulerFactory= new StdSchedulerFactory(props);
			scheduler = schedulerFactory.getScheduler();
			// start the scheduler
			scheduler.start();
			
			// every 30 seconds.
			// initiate jobRecover
			JobDetail jobRecover = JobBuilder
					.newJob(JobRecoverInternal.class)
					.withIdentity("jobRecover", "scheduleJobGroup")
					.build();
			jobRecover.getJobDataMap().put("postgres", emf.createEntityManager());
			jobRecover.getJobDataMap().put("JobQueue", jobQueue);

			// initiate jobRecoverTrigger
			SimpleTrigger jobRecoverTrigger = TriggerBuilder.newTrigger()
					.withIdentity(TriggerKey.triggerKey("jobRecoverTrigger", "scheduleJobGroup"))
					.withSchedule(SimpleScheduleBuilder
							.simpleSchedule()
							.withIntervalInSeconds(5)
							.repeatForever())
					.startAt(DateBuilder.futureDate(randInt(1, 5), 
							DateBuilder.IntervalUnit.SECOND))
					.build();

			scheduler.scheduleJob(jobRecover, jobRecoverTrigger);
		} catch (SchedulerException se) {
			System.err.println(se.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int randInt(int min, int max) {
		// Usually this can be a field rather than a method variable
		Random rand = new Random();

		// nextInt is normally exclusive of the top value, so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

}
