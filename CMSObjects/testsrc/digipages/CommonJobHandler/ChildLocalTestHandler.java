package digipages.CommonJobHandler;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import com.google.gson.Gson;

import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class ChildLocalTestHandler implements CommonJobHandlerInterface{

	
	private static ChildLocalTestHandler myInstance=new ChildLocalTestHandler();
	private static DPLogger logger = DPLogger.getLogger(ChildLocalTestHandler.class.getName());
	private EntityManagerFactory emf;

	public ChildLocalTestHandler()
	{
		LocalRunner.registerHandler(this);
	}

	@Override
	public String mainHandler(Properties config,ScheduleJob job) throws ServerException{
		try {
			Gson gson=new Gson();
			Thread.sleep(5000);
			String message ="Child processed,   "+ job.getJobName() + 
					" Runner=" + job.getJobRunner() +
					" Param="+ gson.toJson(job.getJobParam(Object.class)) + 
					" EntityManagerFactory = " + emf;
			logger.debug(message);
			return message;
		
		}catch (Exception e) {
			logger.error("fail to sleep ?!",e);
			throw new ServerException("id_err_999",e.getMessage());
		}
		
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf=emf;
		
	}



	public static ChildLocalTestHandler getInstance() {
		return myInstance;
	}

	
	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	
	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}



}
