package digipages.CommonJobHandler;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.common.CommonUtil;
import digipages.exceptions.ServerException;
import model.ScheduleJob;
import model.WorkerStatus;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Ec2WorkerManagerTest {
	private EntityManagerFactory emf;
	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		
		Ec2WorkerManager.setInDebug(true);
		Ec2WorkerManager.setEntityManagerFactory(emf);
		ScheduleJobManager.setEntityManagerFactory(emf);
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		Ec2WorkerManager.setInDebug(false);
		clearEc2Workers();
		clearWaitingJobs();
		emf.close();
	}

	private void clearWaitingJobs() {
		EntityManager em=emf.createEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("delete from schedule_job where job_name like 'Ec2WorkerManagerTest%'").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	private void clearEc2Workers() {
		EntityManager em=emf.createEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("delete from worker_status").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	@Test
	public void testAdjustWorkers_init() {
		try {
			// case 1: starting with no Ec2
			
			addWaitingJobs(0);
			Ec2WorkerManager.adjustWorkers();
			assertEquals(0,Ec2WorkerManager.totalEc2Count());
			
		} catch(Exception e)
		{
			fail();
		}
	}
	
	@Test
	public void testAdjustWorkers_maxWorker() {
		try {

			adjustTotalEc2(10);
			addWaitingJobs(150);
			Ec2WorkerManager.adjustWorkers();
			assertEquals(10,Ec2WorkerManager.totalEc2Count());
			
			
		} catch(Exception e)
		{
			fail();
		}
	}
	
	@Test
	public void testAdjustWorkers_needExpand() {
		try {
			// case 1: starting with many
			
			adjustTotalEc2(1);
			addWaitingJobs(100);
			adjustIdleEc2(0);
			Ec2WorkerManager.adjustWorkers();
			assertEquals(2,Ec2WorkerManager.totalEc2Count());
			
			
		} catch(Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testAdjustWorkers_haveIdle() {
		try {
			// case 1: starting with many
			adjustTotalEc2(1);
			addWaitingJobs(100);
			adjustIdleEc2(1);
			Ec2WorkerManager.adjustWorkers();
			assertEquals(Ec2WorkerManager.totalEc2Count(),1);
			
			
		} catch(Exception e)
		{
			fail();
		}
	}
	
	@Test
	public void testAdjustWorkers_notExceedRatio() {
		try {
			adjustTotalEc2(1);
			addWaitingJobs(3);
			adjustIdleEc2(0);
			Ec2WorkerManager.adjustWorkers();
			assertEquals(Ec2WorkerManager.totalEc2Count(),1);
			
			
		} catch(Exception e)
		{
			fail();
		}
	}
	@Test
	public void testAdjustWorkers_ExceedRatio() {
		try {
			adjustTotalEc2(1);
			addWaitingJobs(6);
			adjustIdleEc2(0);
			Ec2WorkerManager.adjustWorkers();
			assertEquals(2,Ec2WorkerManager.totalEc2Count());
			
			
		} catch(Exception e)
		{
			fail();
		}
	}
	@Test
	public void testAdjustWorkers_ExceedRatio2() {
		try {
			adjustTotalEc2(1);
			addWaitingJobs(6);
			adjustIdleEc2(1);
			Ec2WorkerManager.adjustWorkers();
			assertEquals(1,Ec2WorkerManager.totalEc2Count());
			
			
		} catch(Exception e)
		{
			fail();
		}
	}
	
	@Test
	public void testZZStartEc2Instance(){
		Ec2WorkerManager.setInDebug(false);
		InputStream inputStream = Ec2WorkerManager.class.getClassLoader().getResourceAsStream("config.properties");
		try {
			String workerScript = loadWorkerScript();
			Properties config = new Properties();
			config.load(inputStream);
			Ec2WorkerManager.setConfig(config,workerScript,null);
			Ec2WorkerManager.startNewEc2();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		Ec2WorkerManager.setInDebug(true);
		
	}

	private String loadWorkerScript() throws IOException {
		String ret = "";
		InputStream in = Ec2WorkerManagerTest.class.getClassLoader().getResourceAsStream("WorkerInitScript.sh");
		ret = CommonUtil.inputStreamToString(in);
		return ret;
	}

	private void adjustTotalEc2(int ec2Cnt)
	{
		int cnt=Ec2WorkerManager.totalEc2Count();
		int startCnt = ec2Cnt-cnt;
		if (startCnt>0)
		{
			addIdleEc2(startCnt);
			
		}
	}

	//
	private void addIdleEc2(int startCnt) {
		EntityManager em= emf.createEntityManager();
		em.getTransaction().begin();
		for (int i=0; i< startCnt;i++)
		{
			WorkerStatus ws = new WorkerStatus();
			ws.setInstanceId(UUID.randomUUID().toString());
			ws.setStatus(1);
			ws.setThreadId(i);
			em.persist(ws);
		}
		em.close();
		em.getTransaction().commit();
	}

	private void adjustIdleEc2(int idleEc2Cnt)
	{
		EntityManager em= emf.createEntityManager();
		em.getTransaction().begin();
		List <WorkerStatus> workers =WorkerStatus.listAvailWorkers(em);
		// doing reduce idle
		if (workers.size()>idleEc2Cnt)
		{
			for (int i=0; i< workers.size()-idleEc2Cnt;i++)
			{
				workers.get(i).setStatus(2);
			}
		}
		em.getTransaction().commit();
		
		if (workers.size()<idleEc2Cnt)
		{
			addIdleEc2(idleEc2Cnt-workers.size());
		}
		
		
	}

	private void addWaitingJobs(int waitJobs) throws ServerException {
		List <ScheduleJob> jobs = new ArrayList<>();
		for (int i=0; i< waitJobs;i++){
			ScheduleJob sj = new ScheduleJob();
			sj.setJobRunner(RunMode.ExtEc2Mode.getValue());
			sj.setJobName("Ec2WorkerManagerTest-"+i);
			sj.setJobGroup("Ec2WorkerManagerTest");
			sj.setJobParam(new ArrayList<String>());
			sj.setShouldStartTime(new Date());
			sj.setStatus(0);
			jobs.add(sj);
		}
		ScheduleJobManager.addJobs(jobs);
	}
	
	

}
