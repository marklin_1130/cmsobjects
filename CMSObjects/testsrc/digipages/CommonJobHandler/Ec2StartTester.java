package digipages.CommonJobHandler;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.common.CommonUtil;
import digipages.exceptions.ServerException;
import model.ScheduleJob;
import model.WorkerStatus;

/**
 * Only for test aws parameter only.
 * @author Eric.CL.Chou
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Ec2StartTester {
	private EntityManagerFactory emf;
	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		
		Ec2WorkerManager.setInDebug(true);
		Ec2WorkerManager.setEntityManagerFactory(emf);
		ScheduleJobManager.setEntityManagerFactory(emf);
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		Ec2WorkerManager.setInDebug(false);
		clearEc2Workers();
		clearWaitingJobs();
		emf.close();
	}

	private void clearWaitingJobs() {
		EntityManager em=emf.createEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("delete from schedule_job where job_name like 'Ec2WorkerManagerTest%'").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	private void clearEc2Workers() {
		EntityManager em=emf.createEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("delete from worker_status").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	@Test
	public void testZZStartEc2Instance(){
		Ec2WorkerManager.setInDebug(false);
		InputStream inputStream = Ec2WorkerManager.class.getClassLoader()
				.getResourceAsStream("config.properties");
		
		try {
			String workerScript = loadWorkerScript();
			Properties config = new Properties();
			config.load(inputStream);
			config.setProperty("InstanceType", "t2.small");
			config.setProperty("EcWorker2ImgId", "ami-0a0aa16b");
			config.setProperty("EC2S3Profile", "arn:aws:iam::458137374613:instance-profile/RoleConverter");
			Ec2WorkerManager.setConfig(config,workerScript,null);
			Ec2WorkerManager.startNewEc2();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		Ec2WorkerManager.setInDebug(true);
		
	}

	private String loadWorkerScript() throws IOException {
		String ret = "";
		InputStream in = Ec2StartTester.class.getClassLoader().getResourceAsStream("WorkerInitScript.sh");
		ret = CommonUtil.inputStreamToString(in);
		return ret;
	}
	
	

}
