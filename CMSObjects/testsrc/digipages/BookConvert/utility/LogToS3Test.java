package digipages.BookConvert.utility;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

public class LogToS3Test {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testWarn() {
		try {
			LogToS3 logger = new LogToS3();
			logger.warn("Test Message");
			Gson gson = new Gson();
			String outx = gson.toJson(logger);
			System.out.println(outx);
			if (!outx.contains("Test Message"))
			{
				fail();
			}
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
