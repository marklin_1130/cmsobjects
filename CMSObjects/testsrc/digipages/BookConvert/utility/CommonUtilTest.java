package digipages.BookConvert.utility;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CommonUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStripTextFromPartialHtml() {
		try {
			// special character
			String testStr="　　★打造少女漫畫經典《焚月》《藍雪花》等作品，國民漫畫小天后林青慧浪漫改編格林童話！<br />\r\n" + 
					"　　☆加入超逗趣元素，一本獻上兩個精采故事！有黃金單身漢國王VS動不動發飆的爆走女孩（森林奇緣），以及超級自戀潔癖王子VS嫁不出去天兵公主（青蛙王子）！再加上小影老師特別撰寫，對於原版格林童話的Q版可愛感言～<br />\r\n" + 
					"　　★書內收入於全台第一少女漫畫月刊《夢夢》連載時，大受好評的各式唯美彩圖，總共8P彩頁精采呈現!!<br />\r\n" + 
					"<br />\r\n" + 
					"<strong>　　森林奇緣Brother and Sister</strong><br />\r\n" + 
					"　　相依為命的姊弟安琳與約瑟，因為被身為女巫的壞&a 心後母詛咒，害得弟弟約瑟變成了一隻小鹿；幸好，他們在森林裡邂逅國王‧貝里斯，並且被帶回王宮安頓。但是，眼紅安琳即將變成王妃的後母，這次卻變成安琳的樣子混入王宮!?貝里斯能識破這個詭計嗎&hellip;？<br />\r\n" + 
					"<br />\r\n" + 
					"<strong>　　青蛙王子The Frog Prince</strong><br />\r\n" + 
					"不容許有一點瑕疵的完美主義者‧修迪爾王子，因為譏笑矮人族被變成青蛙，必須有人愛上並親吻他，才能恢復人型；就在他外出尋找解咒方法時，遇到煩惱自己嫁不出去的貝莉莎公主。修迪爾答應要訓練貝莉莎成為完美的公主，但條件是貝莉莎必須成為他的「僕人」!?<br />\r\n" + 
					"<br />\r\n" + 
					"　　CS漫畫之星試閱：www.comicstar.com.tw/comic.php?cid=459<br />\r\n" + 
					"<br />\r\n" + 
					"<strong>★讀者好評再推薦★</strong><br />\r\n" + 
					"<br />\r\n" + 
					"　　「很有趣，可以從頭笑到尾XD ──台北市‧拉M」<br />\r\n" + 
					"　　「真的是一部很溫馨的作品呢！看著這系列的作品會讓我覺得自己就是裡面的女主角，心頭暖暖的。 ──台中市‧雪姬」<br />\r\n" + 
					"　　「小影老師您的作品畫得真好，每次看到妳筆下的男女主角深情相望時，我的心也怦然心動～ ──雲林縣‧哈密瓜」<br />\r\n" + 
					"　　「小影老師的『童話DOREMI』系列顛覆一般人對童話故事的刻板印象，內容十分有趣，非常喜愛！ ──宜蘭縣‧小魚兒」<br />\r\n" + 
					"　　「一開始是被小影老師畫的古裝吸引，後來看了『童話DOREMI』，真的覺得老師畫的西式禮服也好美喔!! ──台中市‧珊珊」<br />";
			String target=CommonUtil.stripTextFromPartialHtml(testStr);
			if (target.contains("<"))
			{
				fail("still not success in striping ");
			}
			if (!target.contains("可以從頭笑到尾"))
			{
				fail("strip too much !!");
			}
			
			// wrong attribute quote
			testStr="><P align=center><STRONG><FONT color=#ff0000>　　※2012漫畫博覽會首賣※</FONT></STRONG></P>\r\n" + 
					"<P>　　「耀你是那種嘴巴否認，心裡卻很想的類型呢！」</P>\r\n" + 
					"<P>　　順利找到四個寶藏後，第五個寶藏的提示居然是「你知道的」！<BR>　　依照前面尋找的經驗，小樂斷定這個寶藏一定跟耀有關！<BR>　　而外婆跟耀之間，又會存在著怎樣的回憶呢──？</P>\r\n" + 
					"<P><STRONG>作者簡介</STRONG></P>\r\n" + 
					"<P><STRONG>林亭葳</STRONG></P>\r\n" + 
					"<P>　　● 2010年2月，榮獲第二屆尖端少女漫畫新人獎冠軍<BR>　　● 延續獲獎作品設定，第二、三篇故事陸續刊登於《甜芯》月刊，人氣絕佳！<BR>　　● 2011年8月出版首本單行本《夢境地~Dream Land~》，並於漫博會舉辦簽名會，反應熱烈！<BR>　　● 2012年以《夢境地~Dream Land~》獲得第五屆國際漫畫賞入選<BR>　　● 最新作品「樂園的寶藏」，現正於《夢夢》月刊持續連載中，其幽默風趣的表現，深得讀者喜愛！目前已出版1~2集。</P>";
			target=CommonUtil.stripTextFromPartialHtml(testStr);
			if (target.contains("<"))
			{
				fail("still not success in striping ");
			}
			if (!target.contains("順利找到四個寶藏後"))
			{
				fail("strip too much !!");
			}
			
			// unbalanced String
			testStr = "<p>　　本書收錄林王民萱(小威老師)在夢夢月刊上連載的「蝴蝶愛看書」與甜芯月刊上連載的「傲龍戲鳳妙情緣」兩部作品，喜歡林王民萱(小威老師)作品的妳，絕對不能錯過她的第一本漫畫單行本哦！";
			target=CommonUtil.stripTextFromPartialHtml(testStr);
			if (target.contains("<"))
			{
				fail("still not success in striping ");
			}
			if (!target.contains("「蝴蝶愛看書」與甜芯月刊上連載"))
			{
				fail("strip too much !!");
			}
			
			// and symbol & in text 
			testStr = "    本書  收錄林王民萱&(小威老師)在夢夢月刊上連載的「蝴蝶愛看書」與甜芯月刊上連載的「傲龍戲鳳妙情緣」兩部作品，喜歡林王民萱(小威老師)作品的妳，絕對不能錯過她的第一本漫畫單行本哦！";
			target=CommonUtil.stripTextFromPartialHtml(testStr);
			if (target.contains("<"))
			{
				fail("still not success in striping ");
			}
			if (!target.contains("「蝴蝶愛看書」與甜芯月刊上連載"))
			{
				fail("strip too much !!");
			}
			
			
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

	
}
