package digipages.BookConvert.TocAndOpf;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import digipages.BookConvert.FlattenPDF.PageDimension;

public class TocAndOpfHandlerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			File pdfFile = new File("D:/tmp/E060000037_1_RenderGS.pdf");
			PDDocument document = PDDocument.load(pdfFile);
		     PDPage page = document.getPage(3);
			
			PageDimension dimension = new PageDimension(page, 2);
			
			
			int totalPage = document.getNumberOfPages();
			System.out.println("pages = "+totalPage); 
			document.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			fail("Exception");
		}
	}

}
