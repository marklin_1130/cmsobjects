package digipages.BookConvert.FlattenPDF;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PageDimensionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPageDimension() {
		try {
			
			PDPage page = new PDPage();
			page.setMediaBox(new PDRectangle(900,1024));
			page.setBleedBox(new PDRectangle(986,654));
			
			PageDimension dimension= new PageDimension(page, 2);
			
			assertTrue(2048<=dimension.getBigHeight());

			dimension= new PageDimension(page, 4);
			assertTrue(4096<=dimension.getBigHeight());
		}catch (Exception e)
		{
			fail();
		}
	}
	
	@Test
	
	public void testPDFDimensions() {
		try{
		File pdfFile = new File("/tmp/ALL+No.132.pdf");
		PDDocument pdf = PDDocument.load(pdfFile);
		for (int i=0; i< pdf.getNumberOfPages();i++)
		{
			PDPage page = pdf.getPage(i);
			System.out.println("Page:" +i);
			System.out.print(dumpPageDimension(page));
			PageDimension dimension = new PageDimension(page, 2);
			System.out.println(dimension.getSmallWidth() + ":" + dimension.getSmallHeight());
			System.out.println(dimension.getSmallX() + ":" + dimension.getSmallY() + "\n");
			
		}
		pdf.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

	
	private String dumpPageDimension(PDPage page) {
		
		String mediaBox = "MediaBox=" + dumpDimension(page.getMediaBox());
		String artBox = "ArtBox  =" +dumpDimension(page.getArtBox());
		String bBox="Bbox    ="+dumpDimension(page.getBBox());
		String bleedBox="BleedBox="+dumpDimension(page.getBleedBox());
		String cropBox="CropBox ="+dumpDimension(page.getCropBox());
		String trimBox="TrimBox ="+dumpDimension(page.getTrimBox());
				
		return 
			mediaBox + "\n" 
			+ artBox + "\n" 
			+ bBox + "\n" 
			+ bleedBox + "\n" 
			+ cropBox + "\n" 
			+ trimBox + "\n"
			;
	}

	private String dumpDimension(PDRectangle dimension) {
		String ret= 
				String.format("%3.2f",dimension.getWidth()) + "," +
				String.format("%3.2f",dimension.getHeight()) + ": " +
				String.format("%3.2f",dimension.getLowerLeftX()) + "," +
				String.format("%3.2f",dimension.getLowerLeftY()) + "," +
				String.format("%3.2f",dimension.getUpperRightX()) + "," +
				String.format("%3.2f",dimension.getUpperRightY()) ;
		return ret;
	}
}
