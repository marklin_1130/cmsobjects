package digipages.BookConvert.FlattenPDF;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.RequestData;
import model.ScheduleJob;

public class FlattenPDFHandlerTest {
	static Gson gson = new Gson();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// only works on member local env
//	@Test
	public void testMainHandler() {
		FlattenPDFHandler handler = new FlattenPDFHandler();
		try {
			// S3 file will replace original location. 
//			copyS3FileToTarget();
			Properties conf = buildConf();
			ScheduleJob job = buildJob();
			handler.mainHandler(conf, job);
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}
	
//	@Test
	public void testPDFDimensionCheck()
	{
		try {
			File f = new File("D:\\Downloads\\testE050003413.pdf");
			chkPdfDimensionDist(f);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	/**
     * check PdfSize for consistant.
	 * @throws IOException 
     * @throws Exception
     */
    private static void chkPdfDimensionDist(File pdfFile) 
    {
    	long startTime = System.currentTimeMillis();
    	
    	try {
    		PDRectangle prevSize=null;
	    	PDDocument sourceDocument = PDDocument.load(pdfFile);
			int totalPage= sourceDocument.getNumberOfPages();
			boolean fail=false;
			StringBuilder sb = new StringBuilder();
			for (int i=0;i< totalPage; i++)
			{
				PDPage tempPage = sourceDocument.getPage(i);
				PDRectangle curSize = tempPage.getMediaBox();
				if (diffTooMuch(prevSize, curSize))
				{
					sb.append("PDF page size mismatch , ") 
					.append(pdfFile.getAbsolutePath())
					.append(" page:").append(i+1)
					.append(" prevSize:").append(prevSize)
					.append(" curSize:").append(curSize)
					.append("\r\n");
					fail=true;
				}
				prevSize=curSize;
			}
			sourceDocument.close();
			System.out.println(sb);
    	} catch (Exception ex)
    	{
    		ex.printStackTrace();
    	} 

    }
    
    /**
     * torlance 5%
     * @param prevSize
     * @param curSize
     * @return
     */
	private static boolean diffTooMuch(PDRectangle prevSize, PDRectangle curSize) {
		if (prevSize==null || curSize==null)
			return false;
		float curHeight=curSize.getHeight();
		if (Math.abs(prevSize.getHeight() - curSize.getHeight())/curHeight>0.05)
		{
			return true;
		}

		float curWidth = curSize.getWidth();
		if (Math.abs(prevSize.getWidth() - curSize.getWidth())/curWidth >0.05)
		{
			return true;
		}
		
		return false;
	}

	private ScheduleJob buildJob() {
		ScheduleJob job = new ScheduleJob();
		RequestData rq = new RequestData();
		rq.setIsTrial(true);
		rq.setItem("E91029123"); 
		rq.setBook_file_id("123123"); 
		
		DataMessage dmsg = new DataMessage();
		dmsg.setRequestData(gson.toJson(rq));
		dmsg.setTargetBucket("books-private"); 
		dmsg.setFolder("tmp/FlattenTest");
		dmsg.setBookFileName("RQ7057-001_丹麥女孩.pdf");
		
		job.setJobParam(dmsg);
		return job;
	}

	private Properties buildConf() throws FileNotFoundException, IOException {
		Properties conf = new Properties();
		conf.load(new FileReader(new File("D:\\DevEnv\\GitHub\\GitProj_CMSObjects\\CMSObjects\\testsrc\\digipages\\BookConvert\\FlattenPDF\\config.properties")));
		return conf;
	}
}
