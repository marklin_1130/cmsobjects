package digipages.BookConvert.BookMain.EPubUtil;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SpineTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			File tmpFile = new File("/tmp/content.opf");
			Spine spine = new Spine("",tmpFile);
			String direction= spine.getPageDirection();
			System.out.println("Direction=" + direction);
		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
