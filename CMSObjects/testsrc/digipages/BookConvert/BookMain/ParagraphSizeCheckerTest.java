package digipages.BookConvert.BookMain;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import digipages.BookConvert.utility.Utility;

public class ParagraphSizeCheckerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
//	public void testCheckBigParagraph() {
//		fail("Not yet implemented");
//	}

//	@Test
	public void testIsVerticalHtml() {
		
		try {
			assertTrue(ParagraphSizeChecker.isVerticalHtml(new File("resource/TestData/epub/ParagraphSizeCheckerTest/EBOLA.xhtml")));
			assertFalse(ParagraphSizeChecker.isVerticalHtml(new File("resource/TestData/epub/ParagraphSizeCheckerTest/cover.xhtml")));
			assertTrue(ParagraphSizeChecker.
					isVerticalHtml(new File("resource/TestData/epub/ParagraphSizeCheckerTest/item/xhtml/pf-001.xhtml")));

		} catch (IOException e) {
			
			e.printStackTrace();
			fail();
		}
	}
	@Test
	public void testRecursiveCSS(){
		try {
			String path = "resource/TestData/epub/RecursiveCSS/item/xhtml/";
			List<File> fileList = Utility.listAllFile(new File(path));
			ParagraphSizeChecker checker = new ParagraphSizeChecker(625, 20);
			for (File htmls:fileList)
			{
				checker.updateFile(htmls, "rtl");
			}
			
			if (checker.getSize()<=0)
			{
				fail();
			}
				
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

}
