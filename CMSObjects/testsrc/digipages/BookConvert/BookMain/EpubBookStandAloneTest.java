package digipages.BookConvert.BookMain;

import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EpubBookStandAloneTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testRegCssFilter() {
	    
	    try {
	        
	        String input = "h6{ /*圖片/表格說明*/ \r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "margin-bottom:4px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "margin-top:10px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    font-style:italic\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    font-weight:normal\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    font-size:14px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    line-height:21px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    white-space:normal\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    word-spacing:1px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    letter-spacing:0\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    text-indent:0\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    text-align:left\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    text-decoration:none\r\n" + 
	                "}\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "p{ /*內文*/ \r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "margin-bottom:5px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "margin-top:11px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    font-style:normal\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    font-weight:normal\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    font-size:16px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    line-height:24px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    white-space:normal\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    word-spacing:1px\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    letter-spacing:0\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    text-indent:0\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    text-align:left\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "\r\n" + 
	                "    text-decoration:none\r\n" + 
	                "}";
        
	    Pattern patternFindP = Pattern.compile("(p.?\\{([\\s\\S]+?)\\})");
	    
	    
	    Pattern patternFindFontSize = Pattern.compile("(font-size:(.+)px|pt)");
	    
	    
	    
	    Matcher matcher = patternFindP.matcher(input);
	    
	    
	    if(matcher.find()) {
	        
	        String finder = matcher.group(1);
	        
	        System.out.println(finder);
	        
	        Matcher matcherFontSize = patternFindFontSize.matcher(finder);
	        
	        if(matcherFontSize.find()) {
	            
	            String errorCss = matcherFontSize.group(1);
	            System.out.println(errorCss);
	        }
	        
	        
	    }
	    
        } catch (Exception e) {
            e.printStackTrace();
        }
	    
	    
	    
    }

    @Test
    public void testAllowFname() {
        String name = "14_Cabrals_Fleet.jpg";
        boolean fileNameOk = name.matches("[\\w_\\d\\/\\.\\-\\\\]*");
        assertTrue(fileNameOk);
    }

}
