package digipages.BookConvert.BookMain;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import digipages.BookConvert.utility.LogToS3;

public class EpubBookTest {

	private String rootFileDir;
	private String rootFileName;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test
	public void testCheckBigParagraph() {
		try {
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        dbFactory.setNamespaceAware(true);
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			EpubBook.dBuilder = dBuilder;
					
			Properties properties = buildProperties();
			LogToS3 ConvertingLogger= new LogToS3(null, null, null);
			String itemId="E05123223456";
			File epubFolder=new File("/tmp/test1/");
			EpubBookStandAlone.unZipEpub("/tmp/9789866179990.epub", epubFolder.getAbsolutePath(), ConvertingLogger);
			findRootFile(epubFolder.getAbsolutePath()+"/");
			String opfPath = rootFileDir+ "/" + rootFileName;
			EpubBookStandAlone.checkConsistency(itemId, opfPath, epubFolder, ConvertingLogger, properties,null);
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
		
	}
	
    private void findRootFile(String epubFolderPath) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
        String containerPath = epubFolderPath + "META-INF/container.xml";
        InputStream objectData = new FileInputStream(containerPath);        
        Document doc = dBuilder.parse(objectData);
        Node rootfile = doc.getElementsByTagName("rootfile").item(0);
        NamedNodeMap map = rootfile.getAttributes();
        Node full_path = map.getNamedItem("full-path");
        
        rootFileDir = epubFolderPath + full_path.getNodeValue().substring(0, full_path.getNodeValue().lastIndexOf("/") + 1);
        rootFileName = full_path.getNodeValue().substring(full_path.getNodeValue().lastIndexOf("/") + 1);
                
        objectData.close();
    }

	private Properties buildProperties() {
		Properties prop = new Properties();
		prop.setProperty("allowFixedLayout", "false");
		prop.setProperty("EpubCheckMaxParagraph", "625");
		prop.setProperty("ConsistencyCheck_is_opened", "YES");
		return prop;
	}
	

}