package digipages.BookConvert.BookMain.htmlCss;

import static org.junit.Assert.*;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import digipages.BookConvert.BookMain.EPubUtil.CheckHtml;
import digipages.BookConvert.BookMain.EPubUtil.EPubBookData;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.LogToS3;

public class CheckHtmlTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
    @Test
    public void testCheckFontSizeHtml() {
        try {
            Pattern pattern = Pattern.compile("(font-size:(.+)(px|pt|PT|PX))");
            String styleLine = "java -jar whitesource-fs-agent-1.9.0.jar -c whitesource-fs-agent-ios.config -d D:\\BOOKS_WORK\\GIT\\Ebook_APP\\boos_app_ios\n" + 
                    "\n" + 
                    "java -jar whitesource-fs-agent-1.9.0.jar -c whitesource-fs-agent-android.config -d D:\\BOOKS_WORK\\GIT\\Ebook_APP\\books_app_android\n" + 
                    "\n" + 
                    "\n" + 
                    "<html xml:lang=\"zh-TW\" xmlns=\"http://www.w3.org/1999/xhtml\">\n" + 
                    " <head>\n" + 
                    "  <meta content=\"text/xhtml+xml; charset=UTF-8\" http-equiv=\"Content-Type\">\n" + 
                    "  <title>chapter0</title>\n" + 
                    "  <link href=\"css/style.css\" rel=\"stylesheet\" type=\"text/css\">\n" + 
                    " </head>\n" + 
                    " <body class=\"pagesize\" style=\"\">\n" + 
                    "  <p><span style=\"font-size: 24px;\"><strong>序 </strong></span> 林宛俞</p>\n" + 
                    "  <p> </p>\n" + 
                    "  <p> </p>\n" + 
                    "  <p>安安啊！各位親愛的兄弟姊妹們大家好啊！又到了要寫序的時間嚕！</p>\n" + 
                    "  <p>說到寫序啊！宛俞姊姊已經寫了好多本書，但還是沒有一個具體的感覺可以描述序這個東西。</p>\n" + 
                    "  <p>你們說，那可以聊聊這本書，或是可以聊聊宛俞姊姊最近的近況啊！</p>\n" + 
                    "  <p>宛俞姊姊最近的近況喔！就是手機不見了=。=</p>\n" + 
                    "  <p>人家那支志玲代言的手機寶寶真是超好用的，本來宛俞姊姊想說就給它用到壞掉再換就好了，自從手機出現後，宛俞姊姊也只有用過三支手機。</p>\n" + 
                    "  <p>什麼？太少喔？還好啦！因為第一支手機是我家小弟給我的，那時候還是很可怕的「黑色大石頭」，用它打人可能那個人還會昏倒，又重又可怕，不過很神奇的是，不用再找公共電話，方便極了。</p>\n" + 
                    "  <p>第二支手機就是前幾天失蹤的志玲寶寶，不要問我怎樣失蹤的，如果知道的話，就不叫失、蹤了！</p>\n" + 
                    "  <p>手機失蹤讓宛俞姊姊心很痛，因為那支手機是宛俞姊姊第一次花錢買的，更棒的是，它有照相的功能……好，我知道現在手機基本上都有照相的功能，但是在N年前，有照相手機可是很棒的呢！記得那支手機花了我一萬八，現在卻不見了！</p>\n" + 
                    "  <p>不但手機不見了，連裡面的朋友也都不見了，嗚嗚……所以宛俞姊姊的好朋友要是沒收到來八卦的電話，別怪我啊！</p>\n" + 
                    "  <p>不過沒手機還真是不方便，看來要是我回到古代的話，第一件事情就是被廁所嚇死，再來就是被那種聯絡對方要幾天幾夜的騎馬方式或是飛鴿傳信的方法給急到死。</p>\n" + 
                    "  <p>所以，活在現代真是方便，而活在台灣更是全世界最幸福的，因為有哪個國家像台灣這樣，小小的，卻方便到不行？</p>\n" + 
                    "  <p>以後福爾摩沙可以改成──超級方便的國家。</p>\n" + 
                    "  <p>不同意？</p>\n" + 
                    "  <p>出去看看你家外面是不是有超商的閃亮超牌在黑夜中閃耀著？</p>\n" + 
                    "  <p>哈！這就對了，我愛台灣。 </p>\n" + 
                    " </body>\n" + 
                    "</html>";
            if (StringUtils.isNotBlank(styleLine)) {
                Matcher matcher = pattern.matcher(styleLine);
                if (matcher.find()) {
                    System.out.println(matcher.group(1));
                }
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

	@Test
	public void testCheckMarginHtml() {
		try {
			LogToS3 checkResultLog = new LogToS3();
			File f = new File("resource/TestData/epub/ParagraphSizeCheckerTest/EBOLA.xhtml");
			CheckHtml.checkCssHtml("resource/TestData/epub/ParagraphSizeCheckerTest/",f, checkResultLog, null,"");
			if (!checkResultLog.dumpMessage().contains("Css margin Error"))
				fail("Couldn't find Error");
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testCheckMarginHtmlPass() {
		try {
			LogToS3 checkResultLog = new LogToS3();
			File f = new File("resource/TestData/epub/ParagraphSizeCheckerTest/cover.xhtml");
			CheckHtml.checkCssHtml("resource/TestData/epub/ParagraphSizeCheckerTest/",f, checkResultLog, null,"");
			if (checkResultLog.dumpMessage().contains("Css margin Error"))
				fail("False Positive, code is clean");
		}catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testCheckCover()
	{
		try {
			LogToS3 checkResultLog = new LogToS3();
			EPubBookData epubData = new EPubBookData("resource/TestData/epub/RecursiveCSS/");
			boolean result=CheckHtml.checkEPubCover(epubData, checkResultLog);
			assertTrue(result);
			
		}catch (Exception e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testCheckCoverFail()
	{
		try {
			LogToS3 checkResultLog = new LogToS3();
			EPubBookData epubData = new EPubBookData("resource/TestData/epub/MultipleCoverImg/");
			boolean result=CheckHtml.checkEPubCover(epubData, checkResultLog);
			assertTrue(result);
			
		}catch (Exception e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	

}
