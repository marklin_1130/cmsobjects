package digipages.snps;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.lang3.StringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;

import model.MemberDevice;
import model.MessageQueue;

public class SNPSTest {
	
//	c4cccfdd272d42cdb418049e2ad8f268 電子書-UAT
//	e700570b4d924f43bbfffb03fe6b8a10 電子書-UAT
	
    private static EntityManagerFactory emf;
    private static EntityManager em;
    private String snpsaccount = "ebookreader";
    private String snpspw = "3x3fmf8s";
    private String snpsDeviceUpdate = "http://10.38.8.61:8080/SNPSApi/services/rs/internal/device/update";
    private String snpsBoardcast = "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/boardcast";
    private String snpsBoardcastPayload = "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/boardcast/payload";
    private String snpsPublish = "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/customer";
    private String snpsSimple = "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/simple";
    private String snpsAppKey = "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/customer";
//    private String snpsDeviceUpdate = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/device/update";
//    private String snpsBoardcast = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/boardcast";
//    private String snpsBoardcastPayload = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/boardcast/payload";
//    private String snpsPublish = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/customer";
//    private String snpsSimple = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/simple";
//    private String snpsAppKey = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/customer";
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        emf = Persistence.createEntityManagerFactory("CMSObjects");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        emf.close();
    }

    @Test
    public void testVPCIP() {

        String testIp = "113.196.251.31";

        // ACCESS
        String[] ips = "".split(",");
        List<String> accessIp = Arrays.asList(ips);

        if (accessIp.contains(testIp)) {
            System.out.println("PASS1");
        }

        // VPC
        String[] vpcips = "172.31,112.121.96,112.121.97,113.196.250,113.196.251,".split(",");

        for (String vpc : vpcips) {
            if (testIp.startsWith(vpc)) {
                System.out.println("PASS2");
            }
        }

    }

    @Test
    public void testSendSNSByToken() {

        // ios: c4cccfdd272d42cdb418049e2ad8f268
        // andorid: e700570b4d924f43bbfffb03fe6b8a10

        SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
        snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
        SNPSPublishSimple simple = new SNPSPublishSimple(); 
        simple.setAppKey("ddafbffd3e5445bc86cd1a5e46475481");
        simple.setDeviceToken("c9vwG_G6taQ:APA91bH3zKy8XBDF5TH1jfv4siciEnhegivFUiWb4tUaO7gHz42DE7Trm_XQolUxHtd3QhqrHcDTOCjQiowj_ZX-HL88FXHWUXq7iuUuR1LZWRN7A1Oe3dwvIAfcRGpPKnMoK05gMzFJ");
        simple.setMessage("PROD SNS TEST");
        simple.setBadge("1");
        snpsServiceImpl.sendSNSByToken(simple);

    }

    @Test
    public void testSNSDeviceUpdate() {

        // ios: c4cccfdd272d42cdb418049e2ad8f268
        // andorid: e700570b4d924f43bbfffb03fe6b8a10
        // ios: 427c8bc041164a2295e3b11dd58acc81
        // andorid: ddafbffd3e5445bc86cd1a5e46475481

        SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
        snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
        SNPSDeviceUpdate aSNPSDeviceUpdate = new SNPSDeviceUpdate();
        aSNPSDeviceUpdate.setAppKey("ddafbffd3e5445bc86cd1a5e46475481");
        aSNPSDeviceUpdate.setCid("enjoyto");
        aSNPSDeviceUpdate.setDeviceToken("c9vwG_G6taQ:APA91bH3zKy8XBDF5TH1jfv4siciEnhegivFUiWb4tUaO7gHz42DE7Trm_XQolUxHtd3QhqrHcDTOCjQiowj_ZX-HL88FXHWUXq7iuUuR1LZWRN7A1Oe3dwvIAfcRGpPKnMoK05gMzFJ");
        aSNPSDeviceUpdate.setEnable("1");
        aSNPSDeviceUpdate.setUserCanceled("0");
        snpsServiceImpl.DeviceUpdate(aSNPSDeviceUpdate);

    }

    @Test
    public void testImportDeviceToSNS1() {
        try {

            // ios: c4cccfdd272d42cdb418049e2ad8f268
            // andorid: e700570b4d924f43bbfffb03fe6b8a10
            em = emf.createEntityManager();
            List<MemberDevice> memberDevices = em.createNamedQuery("MemberDevice.findByToken", MemberDevice.class).getResultList();

            System.out.println("data loaded");
            for (MemberDevice memberDevice : memberDevices) {

                if (memberDevice.getStatus() == 2) {
                    SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
                    snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
                    SNPSDeviceUpdate aSNPSDeviceUpdate = new SNPSDeviceUpdate();
                    if ("ios".equalsIgnoreCase(memberDevice.getOsType())) {
                        aSNPSDeviceUpdate.setAppKey("427c8bc041164a2295e3b11dd58acc81");
                    } else {
                        aSNPSDeviceUpdate.setAppKey("ddafbffd3e5445bc86cd1a5e46475481");
                    }
                    aSNPSDeviceUpdate.setCid(memberDevice.getMember().getBmemberId());
                    aSNPSDeviceUpdate.setDeviceToken(memberDevice.getDeviceToken());
                    aSNPSDeviceUpdate.setEnable("1");
                    aSNPSDeviceUpdate.setUserCanceled("0");
                    snpsServiceImpl.DeviceUpdate(aSNPSDeviceUpdate);
                    System.out.println(new Gson().toJson(aSNPSDeviceUpdate));
                }
            }

            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSNS2() {
        try {

            SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
            snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
            SNPSPublishBoardcast aSNPSPublishBoardcast = new SNPSPublishBoardcast();
            aSNPSPublishBoardcast.setAppKey("c4cccfdd272d42cdb418049e2ad8f268");
            aSNPSPublishBoardcast.setBadge("1");
            aSNPSPublishBoardcast.setMessage("測試test。●");
            Object resp = snpsServiceImpl.boardCast(aSNPSPublishBoardcast);
            System.out.println(new Gson().toJson(resp));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Test
    public void testSNSPayloadAndroid() {
        try {

            SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
            snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
            MessageQueue mq = new MessageQueue();
            mq.setMessageId("999");
            mq.setContent("2020/01/14 : 訊息推播測試payload Android");
            mq.setOsType("android");
            
            SNPSPublishBoardcastPayload androidSNPSPublishBoardcast = new SNPSPublishBoardcastPayload();
            if ("android".equalsIgnoreCase(mq.getOsType())) {
                androidSNPSPublishBoardcast.setAppKey("e700570b4d924f43bbfffb03fe6b8a10");
                String payloadAndroid = "{\"data\":{\"message_id\":\""+mq.getMessageId()+"\",\"message_title\":\""+StringUtils.EMPTY+"\",\"message_content\":\""+mq.getContent()+"\"}}";
                androidSNPSPublishBoardcast.setPayload(payloadAndroid);
            }
            if ("ios".equalsIgnoreCase(mq.getOsType())) {
                androidSNPSPublishBoardcast.setAppKey("c4cccfdd272d42cdb418049e2ad8f268");
                String payloadIOS = "{\"aps\":{\"alert\":{\"body\":\""+mq.getContent()+"\"},\"sound\":\"default\",\"datainfo\":{\"message_id\":\""+mq.getMessageId()+"\"}}}";
                androidSNPSPublishBoardcast.setPayload(payloadIOS);
            }
            
            Object resp = snpsServiceImpl.boardcastPayload(androidSNPSPublishBoardcast);
            System.out.println(new Gson().toJson(resp));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Test
    public void testSNSPayloadIOS() {
        try {

            SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
            snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
            MessageQueue mq = new MessageQueue();
            mq.setMessageId("999");
            mq.setContent("2020/01/14 : 訊息推播測試payload IOS");
            mq.setOsType("ios");
            
            SNPSPublishBoardcastPayload androidSNPSPublishBoardcast = new SNPSPublishBoardcastPayload();
            if ("android".equalsIgnoreCase(mq.getOsType())) {
                androidSNPSPublishBoardcast.setAppKey("e700570b4d924f43bbfffb03fe6b8a10");
                String payloadAndroid = "{\"data\":{\"message_id\":\""+mq.getMessageId()+"\",\"message_title\":\""+StringUtils.EMPTY+"\",\"message_content\":\""+mq.getContent()+"\"}}";
                androidSNPSPublishBoardcast.setPayload(payloadAndroid);
            }
            if ("ios".equalsIgnoreCase(mq.getOsType())) {
                androidSNPSPublishBoardcast.setAppKey("c4cccfdd272d42cdb418049e2ad8f268");
                String payloadIOS = "{\"aps\":{\"alert\":{\"body\":\""+mq.getContent()+"\"},\"sound\":\"default\",\"datainfo\":{\"message_id\":\""+mq.getMessageId()+"\"}}}";
                androidSNPSPublishBoardcast.setPayload(payloadIOS);
            }
            
            
            Object resp = snpsServiceImpl.boardcastPayload(androidSNPSPublishBoardcast);
            System.out.println(new Gson().toJson(resp));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
