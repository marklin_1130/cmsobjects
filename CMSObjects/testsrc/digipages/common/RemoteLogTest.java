package digipages.common;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RemoteLogTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLogErr() {
		try {
			RemoteLog.logErr("1234");
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

}
