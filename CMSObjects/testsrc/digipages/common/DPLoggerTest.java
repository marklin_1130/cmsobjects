package digipages.common;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DPLoggerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testGetLoggerString() {
		try {
		DPLogger logger = DPLogger.getLogger(this.getClass().getName());
		logger.debug("Test Logger Message");
		logger.error("Test Error Message");
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}


}
