package digipages.common;

import static org.junit.Assert.fail;

import java.util.Date;

import org.junit.Test;

public class AESUtilsTest {

    @Test
    public void testEncryptDecrypte() {
        try{
            String data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><DRM><DRMType>1</DRMType><DownloadLimit><StartTime>2017-02-14T09:37:55+08:00</StartTime><EndTime>2099-12-31T23:59:59+08:00</EndTime></DownloadLimit><License><LicenseType>1</LicenseType><LicenseGroup></LicenseGroup><Authorized>Y</Authorized></License><Item>E050003972</Item><Member>milkpeanut</Member><ReadLimit><StartTime>2017-02-15T07:37:55+08:00</StartTime><EndTime>2099-12-31T23:59:59+08:00</EndTime><Duration>0</Duration><GracePeriod>0</GracePeriod><ChapterPrice></ChapterPrice><ChapterLimit>ALL</ChapterLimit></ReadLimit><FunctionLimit><SocialShare>Y</SocialShare><MakePublic>Y</MakePublic><Like>Y</Like><Annotation>Y</Annotation><Bookmark>Y</Bookmark><Note>Y</Note></FunctionLimit><Checksum>FAC7A13E1661B03DFF7911CF518132F815D36DC13540A58F5B200B38257563EC</Checksum><AuthID>713215</AuthID></DRM>";
            String encrypt = AESUtils.encryptAES(data);
            String decrypt = AESUtils.decryptAES(encrypt);
            System.out.println(encrypt);
            System.out.println(decrypt);
        } catch (Exception ex)
        {
            fail("ConvertFail");
        }
    }
    
    @Test
    public void testGetversionEncryptDecrypte() {
        try{
            String data = "Version: "+"1.3.0687"+"|"+AESUtils.encryptAES(CommonUtil.fromDateToString(new Date()));
            System.out.println(data);
            String decrypt = AESUtils.decryptAES(data.split("\\|")[1]);
            System.out.println(decrypt);
        } catch (Exception ex)
        {
            ex.printStackTrace();
            fail("ConvertFail");
        }
    }
    
}
