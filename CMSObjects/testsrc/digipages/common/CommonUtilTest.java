package digipages.common;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class CommonUtilTest {

	@Test
	public void testGetCurLocalDateTimeStr() {
		try{
			System.out.println(CommonUtil.getCurLocalDateTimeStr(null));
			System.out.println(CommonUtil.getCurLocalDateTimeStr("UTC+8"));
			System.out.println(CommonUtil.getCurLocalDateTimeStr("Asia/Taipei"));
		} catch (Exception ex)
		{
			fail("ConvertFail");
		}
	}
	
	@Test
	public void testNormalizeDateTimeStr() {
		try{
			System.out.println(CommonUtil.normalizeDateTime(null));
			System.out.println(CommonUtil.normalizeDateTime(""));
			System.out.println(CommonUtil.normalizeDateTime("9999/12/31 00:00:00"));
			System.out.println(CommonUtil.normalizeDateTime("9999/11/30 23:59:59"));
			System.out.println(CommonUtil.normalizeDateTime("9999/12/31"));
			System.out.println(CommonUtil.normalizeDateTime("2099/12/31 23:59:59"));
			System.out.println(CommonUtil.normalizeDateTime("9999/11/30 23:59:59"));
			System.out.println(CommonUtil.normalizeDateTime("2099/12/31 23:59:00"));
			System.out.println(CommonUtil.normalizeDateTime("9999/12/31 23:59:59"));
			System.out.println(CommonUtil.normalizeDateTime("9999/12/31 00:00:00"));
			
		} catch (Exception ex)
		{
			fail("ConvertFail");
		}
	}
	
	@Test
	public void testDeviceNameGen()
	{
		List <String> names = new ArrayList<String>();
		names.add("aaa");
		names.add("aaa#2");
		names.add("bbb#rita's 5s");
		for (int i=0; i< 10;  i++)
		if (CommonUtil.hasDuplicate(names, "aaa"))
		{
			names.add(CommonUtil.genNewName(names,"aaa"));
		}
		
		for (String tmpName:names)
		{
			System.out.println(tmpName);
		}
		
	}
	
	@Test
	public void testZoneDateToSybaseStr()
	{
		String orig = CommonUtil.zonedTime();
		long startTime = System.currentTimeMillis();
		for (int i=0; i<1000;i++){
			String target=CommonUtil.zoneDateToSybaseStr(orig);
		}
		
		System.out.println("converted SybaseStr Total Time= "+ (System.currentTimeMillis()-startTime));
		
		
	}
	@Test
	public void testGenCrossDomainHost()
	{
		String x = CommonUtil.genCrossDomainHost("https://viewer.booksdev.digipage.info");
		if (!x.contains("*"))
			fail("no * generated");
		if (!x.contains("https://"))
			fail("header https fail");
		if (!x.endsWith(".info"))
			fail("tail fail: info " + x);
		x = CommonUtil.genCrossDomainHost("http://viewer.booksdev.digipage.info");
		if (!x.contains("*"))
			fail("no * generated");
		if (!x.contains("http://"))
			fail("header http fail");
		if (!x.endsWith(".info"))
			fail("tail fail: info" + x);
		x = CommonUtil.genCrossDomainHost("https://viewer-auth.books.com.tw");
		if (!x.contains("*"))
			fail("no * generated");
		if (!x.contains("http://"))
			fail("header http fail");
		if (!x.endsWith(".tw"))
			fail("tail fail: tw" + x);
	}

}
