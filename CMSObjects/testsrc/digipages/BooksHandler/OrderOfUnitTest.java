package digipages.BooksHandler;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrderOfUnitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("SetupBefore Class");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("TearDownAfter Class");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("\tSetup");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("\tTearDown");
	}

	@Test
	public void testA() {
		System.out.println("\t\tTestA");
		// fail("Not yet implemented");
	}

	@Test
	public void testB() {
		System.out.println("\t\tTestB");
		// fail("Not yet implemented");
	}

	@Test
	public void testC() {
		System.out.println("\t\tTestC");
		// fail("Not yet implemented");
	}
}