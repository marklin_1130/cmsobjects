package digipages.BooksHandler;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.common.DRMLogicUtility;
import digipages.dto.MemberDrmLogDTO;
import digipages.exceptions.ServerException;
import model.BookFile;
import model.Item;
import model.Member;
import model.MemberBook;
import model.MemberBookDrmMapping;
import model.MemberDrmLog;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MemberDrmHandlerFix {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private Member member;
	private Item itemTrial;
	private Item itemWithChilds;
	private Item itemNormal;
	private static String currentTestName;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		// SampleDataGenerator gen = new SampleDataGenerator(em);
		// em.getTransaction().begin();
		//
		// member=gen.generateBenchMember(false);
		//
		// itemTrial =gen.genSampleTrialItem();
		// em.persist(itemTrial);
		//
		// itemNormal = gen.genSampleItem();
		// em.persist(itemNormal);
		//
		// itemWithChilds = gen.genSampleBookWithChilds(em);
		// em.persist(itemWithChilds);
		//
		// em.getTransaction().commit();
	}

	@After
	public void tearDown() throws Exception {
		// if (!em.getTransaction().isActive()) {
		// em.getTransaction().begin();
		// }
		// member=em.merge(member);
		// em.remove(member);
		// itemTrial = em.merge(itemTrial);
		// em.remove(itemTrial);
		// if(!"testRescanDRMWithModifyItemChilds".equalsIgnoreCase(currentTestName)){
		// itemWithChilds = em.merge(itemWithChilds);
		// }
		// em.remove(itemWithChilds);
		// em.getTransaction().commit();
		em.close();
	}


	@Test
	public void testdelmaping() {
		
//	    delete from message_queue where id in (372,371,370)

//	    delete from message_queue where id in (375,374,373,377)
	    
		em.getTransaction().begin();
		javax.persistence.Query del = em.createNativeQuery("update message_queue set owner_id = null where id = 369");
		
//		javax.persistence.Query del2 = em.createNativeQuery("delete from member_book where member_id = 97087");
		
//		javax.persistence.Query del3 = em.createNativeQuery("delete from member_drm_log where member_id = 97087");
		
		
		del.executeUpdate();
//		del2.executeUpdate();
//		del3.executeUpdate();
				
		em.getTransaction().commit();		
	}
	
	

	@Test
	public void test001maping() {

		try {

			List<Member> members = (List<Member>) em.createNativeQuery(
					"select * from member where bmember_id in (select bmember_id from member where 1=1 group by bmember_id ,member_type  having count (*) > 1 and bmember_id <> '' and member_type = 'NormalReader' ) order by bmember_id",
					Member.class).getResultList();

			Map<String, List<Member>> membersMap = new HashMap<String, List<Member>>();
			for (Member member : members) {
				String bemmebrIdf = member.getBmemberId();
				if (membersMap.get(bemmebrIdf) == null) {
					List<Member> list = new ArrayList<Member>();
					list.add(member);
					membersMap.put(bemmebrIdf, list);
				} else {
					membersMap.get(bemmebrIdf).add(member);
				}

			}
//			 
//
//			activity_log
//
//			cms_token
//
//			like_hotbook_feedback
//
//			member_book
//
//			member_book_drm_mapping
//
//			###member_book_note
//
//			member_device
//
//			member_drm_log
//
//			member_read_list
//
//			message
//
//			password_session
//
//			setting
//			

			for (Entry<String, List<Member>> entry : membersMap.entrySet()) {
				em.getTransaction().begin();
				System.out.println(entry.getKey());

				List<Member> membersList = entry.getValue();

				Comparator<Member> memberIdcom = (Object1, Object2) -> Object1.getId() < Object2.getId() ? -1 : 1;
				Member memberMin = membersList.stream().sorted(memberIdcom).findFirst().get();

				Long memberMinid = memberMin.getId();
				
				String maxId = "";
				
				for (Member member : membersList) {
					if(member.getId()!=memberMinid) {
//						maxId = maxId + String.valueOf(member.getId())+",";
						
						 javax.persistence.Query q8 = em.createNativeQuery("delete from member where id = "+member.getId()+"");
						 q8.executeUpdate();
						 
//						 
//						for (MemberBook mb1 : member.getMemberBooks()) {
//							
//							javax.persistence.Query existBook = em.createNativeQuery("select  *  from member_book  where item_id = '"+mb1.getItem().getId()+"'  and member_id = "+memberMin.getId() ,MemberBook.class);
//							if(existBook.getResultList().size()<1) {
//								
//								javax.persistence.Query q000 = em.createNativeQuery("update member_book_drm_mapping set member_id = "+memberMinid+" where member_book_id ="+mb1.getId()+" and member_id= "+member.getId()+"");
//								 q000.executeUpdate();
//								 javax.persistence.Query q0 = em.createNativeQuery("update member_book set member_id = "+memberMinid+" where id = "+mb1.getId()+" and member_id = "+member.getId()+"");
//								 q0.executeUpdate();
//								 javax.persistence.Query q00= em.createNativeQuery("update member_drm_log set member_id = "+memberMinid+" where item_id='"+mb1.getItem().getId()+"' and member_id = "+member.getId()+"");
//								 q00.executeUpdate();
//								 
//							}
//							
//						}
//						
//						
//					 javax.persistence.Query q1 = em.createNativeQuery("update activity_log set member_id = "+memberMinid+" where member_id = "+member.getId()+"");
//					 q1.executeUpdate();
//					 
//					 javax.persistence.Query q2 = em.createNativeQuery("update cms_token set member_id = "+memberMinid+" where member_id = "+member.getId()+"");
//					 q2.executeUpdate();
//					 
//					 javax.persistence.Query q3 = em.createNativeQuery("update like_hotbook_feedback set member_id = "+memberMinid+" where member_id = "+member.getId()+"");
//					 q3.executeUpdate();
//					 
//					 javax.persistence.Query q4 = em.createNativeQuery("update member_read_list set member_id = "+memberMinid+" where member_id = "+member.getId()+"");
//					 q4.executeUpdate();
//					 
//					 javax.persistence.Query q5 = em.createNativeQuery("update password_session set member_id = "+memberMinid+" where member_id = "+member.getId()+"");
//					 q5.executeUpdate();
//					 
//					 javax.persistence.Query q6 = em.createNativeQuery("update message set member_id = "+memberMinid+" where member_id = "+member.getId()+"");
//					 q6.executeUpdate();
//					 
//					 javax.persistence.Query q7 = em.createNativeQuery("update setting set member_id = "+memberMinid+" where member_id = "+member.getId()+"");
//					 q7.executeUpdate();
//					 
//					}
					}
				}
				
//				maxId = maxId.substring(0, maxId.length()-1);

//				for (Member member : membersList) {
//					for (MemberBook mb : member.getMemberBooks()) {
//						
//						mb.setMember(memberMin);
//						em.persist(mb);
//					}
//					
//					for (MemberDevice md : member.getMemberDevices()) {
//						md.setMember(memberMin);
//						em.persist(md);
//					}
//					
//					
//					for (MemberDrmLog mdl : member.getMemberDrmLogs()) {
//						
//						for (MemberBookDrmMapping mp : mdl.getMemberBookDrmMappings()) {
//							mp.setMemberId(memberMinid);
//							em.persist(mp);
//						}
//						
//						mdl.setMember(memberMin);
//						em.persist(mdl);
//					}
//				}
				 
				em.getTransaction().commit();
				

			}

//			MemberBookDrmMapping mmm = em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class)
//					.setParameter("id", 55936L).getSingleResult();
//			mmm.setMemberBook(MemberBook.findByBookFileId(em, 101679L, 24349L));
//
//			em.persist(mmm);
//			em.getTransaction().commit();

		} catch (Exception e) {e.printStackTrace();
			em.getTransaction().rollback();
			e.printStackTrace();
		}

	}

	@Test
	public void test000Item() {
		// 測是效能用
		String resp = "";
		try {
			int count = 0;
			int count2 = 0;
			int count3 = 0;
			int count4 = 0;
			String line;
			String line2;
			try (InputStream fis = new FileInputStream("D:/0906ALL.TXT");
					InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
					BufferedReader br = new BufferedReader(isr);

					InputStream fis2 = new FileInputStream("D:/allCMSitem0906.csv");
					InputStreamReader isr2 = new InputStreamReader(fis2, Charset.forName("UTF-8"));
					BufferedReader br2 = new BufferedReader(isr2);) {

				List<Item> items = new ArrayList<Item>();
				// List<String> itemsStr = new ArrayList<String>();
				// List<String> itemsOrgStr = new ArrayList<String>();

				Set<String> itemsStr = new HashSet<String>();
				Set<String> itemsOrgStr = new HashSet<String>();

				while ((line2 = br2.readLine()) != null) {
					// Deal with the line
					// Check existence of item.

					// MemberDrmLog.getMemberDrmLogByTransactionId(em, line2);

					String item_id = line2;
					itemsStr.add(line2);
					// Item item = em.find(Item.class, item_id);
					// items.add(item);
				}

				// List<String> lines1 = new ArrayList<String>();
				//
				// Path file1 = Paths.get("D:/insert0906.txt");
				//
				List<String> lines2 = new ArrayList<String>();
				Path file2 = Paths.get("D:/item0906_3.txt");

				//
				// List<String> lines3 = new ArrayList<String>();
				// Path file3 = Paths.get("D:/3.txt");
				// Files.write(file3, lines3, Charset.forName("UTF-8"));

				while ((line = br.readLine()) != null) {

					itemsOrgStr.add(StringUtils.trimToEmpty(line));
					// Deal with the line

					// Check existence of item.

					// Item item = em.find(Item.class, item_id);
					//
					// if(item == null) {
					//// System.out.println("item isnull"+item_id);
					// count3++;
					// }else {
					//
					// BookFile bf = BookFile.findLastVersionByItemId(em,item.getId());
					//
					// if(bf==null) {
					// count4++;
					//// System.out.println("item "+item_id+"bookfile null");
					// }else {
					//
					// if(bf.getStatus()==8) {
					// count2++;
					//// System.out.println("item "+item_id+"status = 8"+"bookFileId"+bf.getId());
					// lines2.add(item_id+","+bf.getStatus()+","+bf.getFormat()+","+item.getcTitle());
					// lines1.add("update book_file set status = 9 , operator = 'sys' where 1=1 and
					// id = "+bf.getId()+"; -- "+item_id +" "+item.getcTitle());
					// }
					//
					// if(bf.getStatus()==9) {
					// count++;
					// }
					// }
					//
					// }

				}

				for (String itemid : itemsStr) {

					if (itemsOrgStr.contains(StringUtils.trimToEmpty(itemid))) {
					} else {
						Item item = em.find(Item.class, itemid);
						BookFile bf = BookFile.findLastVersionByItemId(em, item.getId());
						if (bf.getStatus() == 9) {
							System.out.println(itemid + "," + bf.getStatus() + "," + bf.getFormat() + ",op:"
									+ bf.getOperator() + "," + item.getcTitle());
							lines2.add(itemid + "," + bf.getStatus() + "," + bf.getFormat() + ",op:" + bf.getOperator()
									+ "," + item.getcTitle());
						}
					}
				}

				// Files.write(file1, lines1, Charset.forName("UTF-8"),
				// StandardOpenOption.APPEND);
				Files.write(file2, lines2, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
				//
				// System.out.println("item same"+count);
				// System.out.println("item error"+count2);
				// System.out.println("item null"+count3);
				// System.out.println("item book file null"+count4);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void test000AddDRM() {
		// 測是效能用
		String resp = "";
		try {

			// List<String> bmemberId = new ArrayList<String>();
			// bmemberId.add("nil62611");
			// bmemberId.add("akiyao");
			// bmemberId.add("qjsmpyk");
			// bmemberId.add("mike1105");
			// bmemberId.add("pieces1017");
			// bmemberId.add("huamee");
			// bmemberId.add("testgood");
			// bmemberId.add("testgood1");
			// bmemberId.add("benqprovider1");
			// bmemberId.add("benqprovider2");
			// bmemberId.add("errbook");
			// bmemberId.add("benquser2");
			// bmemberId.add("benquser1");
			// bmemberId.add("maisonchen");
			// bmemberId.add("catherine112");
			// bmemberId.add("ritayang112");
			// bmemberId.add("chrispdm");
			// bmemberId.add("money0530");
			// bmemberId.add("gelatoni530");
			// bmemberId.add("cwc1223");
			// bmemberId.add("a1b2c3d4e5x");
			// bmemberId.add("nt0930799968");
			// bmemberId.add("mike1105");
			// bmemberId.add("allenbooks");
			// bmemberId.add("sumk");
			// bmemberId.add("aimvyy44");
			// bmemberId.add("tq0551");
			// bmemberId.add("john70480");
			// bmemberId.add("monkey0102");
			// bmemberId.add("milkpeanut");
			// bmemberId.add("t221109365");
			// bmemberId.add("ekekmm");
			// bmemberId.add("bluesad");
			// bmemberId.add("680612");
			// bmemberId.add("yuchin6");
			// bmemberId.add("iris0208");
			// bmemberId.add("maisiejung");
			// bmemberId.add("heloise7233");
			// bmemberId.add("mayding");
			// bmemberId.add("blueshuyi");
			// bmemberId.add("a864629943");
			// bmemberId.add("Lina1002");
			// bmemberId.add("manaju4133");
			// bmemberId.add("phyllisbooks");
			//
			Set<String> drmids = new HashSet<String>();
			try (InputStream fis2 = new FileInputStream("D:/ddd.txt");
					InputStreamReader isr2 = new InputStreamReader(fis2, Charset.forName("UTF-8"));
					BufferedReader br2 = new BufferedReader(isr2);) {

				List<Item> items = new ArrayList<Item>();
				// List<String> itemsStr = new ArrayList<String>();
				// List<String> itemsOrgStr = new ArrayList<String>();

				Set<String> itemsOrgStr = new HashSet<String>();
				String line2 = "";
				while ((line2 = br2.readLine()) != null) {
					// Deal with the line
					// Check existence of item.
					String item_id = line2;
					drmids.add(line2);
					// Item item = em.find(Item.class, item_id);
					// items.add(item);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			List<MemberDrmLog> drms = new ArrayList<MemberDrmLog>();
			for (String drmid : drmids) {
				drms.add(MemberDrmLog.getMemberDrmLogById(em, Long.valueOf(drmid)));

			}

			for (MemberDrmLog memberDrmLog : drms) {

				List<MemberBookDrmMapping> mapp = memberDrmLog.getMemberBookDrmMappings();

				if (mapp.isEmpty()) {

					try {
						MemberDrmHandler handler = new MemberDrmHandler();
						MemberDrmLogDTO dto = new MemberDrmLogDTO();
						dto.setBooksMemberId(memberDrmLog.getMember().getBmemberId());
						// 新增一般書籍授權,檢查書籍授權資料已新增
						dto.setViewerBaseURI("http://123/");
						dto.setReadDays("100");
						dto.setSessionToken("");
						dto.setItemId(memberDrmLog.getItem().getId());
						dto.setReadExpireTime(memberDrmLog.getReadExpireTime());
						dto.setDownloadExpireTime(memberDrmLog.getDownloadExpireTime());
						dto.setTransactionId(memberDrmLog.getTransactionId());
						dto.setType(String.valueOf(memberDrmLog.getType()));
						dto.setSubmitId(memberDrmLog.getSubmitId());
						resp = handler.addDRM(em, dto);
						// resp = handler.addTrial(em, "4",memberId,"E050011286","");
						System.out.println(memberDrmLog.getItem().getId() + " " + resp);

					} catch (ServerException e) {
						System.out.println("finally: " + e.getError_message());
					}

				}

				// else {
				// for (MemberBookDrmMapping memberBookDrmMapping : mapp) {

				// if(memberBookDrmMapping.getMemberBook()==null) {
				// em.getTransaction().begin();
				// em.remove(memberBookDrmMapping);
				// em.getTransaction().commit();
				// }

				// }

				// try {
				// MemberDrmHandler handler = new MemberDrmHandler();
				// MemberDrmLogDTO dto = new MemberDrmLogDTO();
				// dto.setBooksMemberId(memberDrmLog.getMember().getBmemberId());
				// //新增一般書籍授權,檢查書籍授權資料已新增
				// dto.setViewerBaseURI("http://123/");
				// dto.setReadDays("100");
				// dto.setSessionToken("");
				// dto.setItemId(memberDrmLog.getItem().getId());
				// dto.setReadExpireTime(memberDrmLog.getReadExpireTime());
				// dto.setDownloadExpireTime(memberDrmLog.getDownloadExpireTime());
				// dto.setTransactionId(memberDrmLog.getTransactionId());
				// dto.setType(String.valueOf(memberDrmLog.getType()));
				// dto.setSubmitId(memberDrmLog.getSubmitId());
				// resp = handler.addDRM(em, dto);
				//// resp = handler.addTrial(em, "4",memberId,"E050011286","");
				// System.out.println(memberDrmLog.getItem().getId()+" "+resp);
				//
				// } catch (ServerException e) {
				// System.out.println("finally: " + e.getError_message());
				// }

				// }

			}

			// dto.setItemId("E050004354");
			// dto.setReadExpireTime("2019/08/09 19:00:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004357");
			// dto.setReadExpireTime("2019/08/09 19:30:00");
			// drms.add(dto);
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004356");
			// dto.setReadExpireTime("2019/08/07 18:00:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050005679");
			// dto.setReadExpireTime("2019/08/08 10:00:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004612");
			// dto.setReadExpireTime("2017/08/08 10:30:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004613");
			// dto.setReadExpireTime("2017/08/08 11:00:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004614");
			// dto.setReadExpireTime("2017/08/08 11:30:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004615");
			// dto.setReadExpireTime("2017/08/08 12:00:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004616");
			// dto.setReadExpireTime("2017/08/08 14:00:00");
			// drms.add(dto);
			//
			// dto = new MemberDrmLogDTO();
			// dto.setItemId("E050004617");
			// dto.setReadExpireTime("2017/08/08 15:00:00");
			// drms.add(dto);

			// MemberDrmHandler handler = new MemberDrmHandler();
			// for (String memberId : bmemberId) {
			// for (MemberDrmLogDTO memberDrmLogDTO : drms) {
			// try {
			// memberDrmLogDTO.setBooksMemberId(memberId);
			// //新增一般書籍授權,檢查書籍授權資料已新增
			// memberDrmLogDTO.setViewerBaseURI("http://123/");
			// memberDrmLogDTO.setReadDays("999");
			// memberDrmLogDTO.setSessionToken("");
			// memberDrmLogDTO.setDownloadExpireTime("2017/10/10 12:00:00");
			// memberDrmLogDTO.setTransactionId("20170905016294");
			// memberDrmLogDTO.setType("1");
			// memberDrmLogDTO.setSubmitId("20170905016294");
			// resp = handler.addDRM(em, memberDrmLogDTO);
			//// resp = handler.addTrial(em, "4",memberId,"E050011286","");
			// System.out.println(memberId+" "+resp);
			//
			// } catch (ServerException e) {
			// System.out.println("finally: " + e.getError_message());
			// }
			// }
			// }

		} catch (Exception e) {

			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test001AddModifyDelDRM() {
		String resp = "";
		try {
			MemberDrmHandler handler = new MemberDrmHandler();
			MemberDrmLogDTO dto = new MemberDrmLogDTO();
			dto.setViewerBaseURI("http://123/");
			dto.setItemId(itemNormal.getId());
			dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
			dto.setReadDays("10");
			dto.setSessionToken("");
			dto.setTransactionId(UUID.randomUUID().toString());
			dto.setBooksMemberId(member.getBmemberId());
			dto.setType("1");

			// 新增一般書籍授權,檢查書籍授權資料已新增
			resp = handler.addDRM(em, dto);
			System.err.println("addDRM: " + resp);
			em.refresh(member);
			MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
			assertNotNull("書籍未新增", targetBook);
			Assert.assertTrue(targetBook.getMemberBookDrmMappings().size() > 0);
			Assert.assertTrue(targetBook.getMemberDrmLogs().size() > 0);

			MemberBookDrmMapping memberBookDrmMapping = targetBook.getMemberBookDrmMappings().get(0);
			Long memberBookDrmMappingId = memberBookDrmMapping.getId();
			MemberDrmLog memberDrmLog = targetBook.getMemberDrmLogs().get(0);
			Long memberDrmLogId = memberDrmLog.getId();

			// 修改一般書籍授權,檢查書籍授權資料已修改
			String modifyDownloadExpireTime = "2999/10/10 12:00:00.00";
			String modifyReadStartTime = "2017/01/10 12:00:00.00";
			String modifyReadExpireTime = "2017/02/10 12:00:00.00";
			dto.setReadDays("5");
			dto.setType("3");// 免費領取
			dto.setDownloadExpireTime(modifyDownloadExpireTime);
			dto.setReadStartTime(modifyReadStartTime);
			dto.setReadExpireTime(modifyReadExpireTime);

			resp = handler.modifyDRM(em, dto);
			System.err.println("modifyDRM: " + resp);
			em.refresh(member);
			em.refresh(memberDrmLog);
			Assert.assertTrue("修改DRM ReadDay 失敗", memberDrmLog.getReadDays() == 5);
			Assert.assertTrue("修改DRM Type 失敗", memberDrmLog.getType() == 3);
			Assert.assertTrue("修改DRM DownloadExpireTime 失敗",
					memberDrmLog.getDownloadExpireTime().equalsIgnoreCase(modifyDownloadExpireTime));
			Assert.assertTrue("修改DRM ReadStartTime 失敗",
					memberDrmLog.getReadStartTime().equalsIgnoreCase(modifyReadStartTime));
			Assert.assertTrue("修改DRM ReadExpireTime 失敗",
					memberDrmLog.getReadExpireTime().equalsIgnoreCase(modifyReadExpireTime));

			// 刪除一般書籍授權,檢查書籍授權資料已刪除
			resp = handler.delDRM(em, dto);
			System.err.println("delDRM: " + resp);
			em.refresh(member);
			targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
			Assert.assertTrue(targetBook.isDeleted());
			Assert.assertTrue(em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class)
					.setParameter("id", memberDrmLogId).getResultList().size() == 0);
			Assert.assertTrue(em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class)
					.setParameter("id", memberBookDrmMappingId).getResultList().size() == 0);

		} catch (ServerException e) {
			System.out.println("finally: " + e.getError_message());
		} catch (Exception e) {

			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test00dDelDRM() {
		String resp = "";
		try {
			MemberDrmHandler handler = new MemberDrmHandler();
			MemberDrmLogDTO dto = new MemberDrmLogDTO();
			dto.setViewerBaseURI("http://123/");
			dto.setItemId("E050005379");
			dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
			dto.setReadDays("10");
			dto.setSessionToken("");
			dto.setSubmitId("");
			dto.setTransactionId("20170712014679");
			dto.setBooksMemberId("nil62611");
			dto.setType("1");

			// 刪除一般書籍授權,檢查書籍授權資料已刪除
			resp = handler.delDRM(em, dto);
			System.out.println("delDRM: " + resp);

		} catch (ServerException e) {
			System.out.println("finally: " + e.getError_message());
		} catch (Exception e) {

			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test002AddTrialDRM() {
		String resp = "";
		try {
			MemberDrmHandler handler = new MemberDrmHandler();
			MemberDrmLogDTO dto = new MemberDrmLogDTO();
			dto.setViewerBaseURI("http://123/");
			dto.setItemId(itemTrial.getId());
			dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
			dto.setReadDays("10");
			dto.setSessionToken("");
			dto.setTransactionId(UUID.randomUUID().toString());
			dto.setBooksMemberId(member.getBmemberId());
			dto.setType("4");

			// 新增試閱書籍,檢查書籍已新增
			resp = handler.addTrial(em, dto.getType(), String.valueOf(member.getId()), dto.getItemId(),
					dto.getViewerBaseURI());

			System.err.println("finally: " + resp);
			em.refresh(member);
			MemberBook targetBook = member.getMemberTrialBookByItemId(em, dto.getItemId());
			assertNotNull(targetBook);

		} catch (ServerException e) {
			System.out.println("finally: " + e.getError_message());
		} catch (Exception e) {

			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test003AddModifyActiveAndInactiveDRM() {
		try {
			String transactionId = UUID.randomUUID().toString();
			MemberDrmHandler handler = new MemberDrmHandler();
			MemberDrmLogDTO dto = new MemberDrmLogDTO();
			dto.setBooksMemberId(member.getBmemberId());
			dto.setViewerBaseURI("http://123/");
			dto.setItemId(itemNormal.getId());
			dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
			dto.setReadDays("10");
			dto.setSessionToken("");
			dto.setTransactionId(transactionId);
			dto.setType("1");
			handler.addDRM(em, dto);

			// 新增一般書籍授權,檢查書籍授權資料已新增
			String resp = handler.addDRM(em, dto);
			System.err.println("addDRM: " + resp);
			em.refresh(member);
			MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
			assertNotNull("書籍未新增", targetBook);
			Assert.assertTrue(targetBook.getMemberBookDrmMappings().size() > 0);
			Assert.assertTrue(targetBook.getMemberDrmLogs().size() > 0);

			// 設定資料供後續檢查
			MemberBookDrmMapping memberBookDrmMapping = targetBook.getMemberBookDrmMappings().get(0);
			Long memberBookDrmMappingId = memberBookDrmMapping.getId();
			MemberDrmLog memberDrmLog = targetBook.getMemberDrmLogs().get(0);
			Long memberDrmLogId = memberDrmLog.getId();

			// 刪除一般書籍授權,檢查書籍授權資料已刪除
			resp = handler.delDRM(em, dto);
			System.err.println("delDRM: " + resp);
			em.refresh(member);
			targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
			Assert.assertTrue(targetBook.isDeleted());
			Assert.assertTrue(em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class)
					.setParameter("id", memberDrmLogId).getResultList().size() == 0);
			Assert.assertTrue(em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class)
					.setParameter("id", memberBookDrmMappingId).getResultList().size() == 0);

			// 刪除後重新啟用
			dto.setStatus("1");
			handler.modifyDRM(em, dto);
			targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
			Assert.assertTrue("重新啟用書籍未新增MemberBook",
					"update".equalsIgnoreCase(targetBook.getAction()) && targetBook.isDeleted() == false);
			Assert.assertTrue("重新啟用書籍未新增MemberDrmLog", em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class)
					.setParameter("id", memberDrmLogId).getResultList().size() == 1);
			Assert.assertTrue("重新啟用書籍未新增MemberBookDrmMapping",
					memberDrmLog.getMemberBookDrmMappings().get(0).getMemberBook().getId().equals(targetBook.getId()));

		} catch (ServerException e) {
			System.out.println("finally: " + e.getError_message());
		} catch (Exception e) {

			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test004AddDRMWithChilds() {
		try {
			MemberDrmHandler handler = new MemberDrmHandler();
			MemberDrmLogDTO dto = new MemberDrmLogDTO();
			dto.setBooksMemberId(member.getBmemberId());
			dto.setViewerBaseURI("http://123/");
			dto.setItemId(itemWithChilds.getId());
			dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
			dto.setReadDays("10");
			dto.setSessionToken("");
			dto.setTransactionId(UUID.randomUUID().toString());
			dto.setType("1");

			// 新增一般套書書籍授權,檢查書籍授權資料已新增
			String resp = handler.addDRM(em, dto);
			em.refresh(member);
			int serialSize = itemWithChilds.getChilds().size();
			List<Long> memberBookDrmMappingIds = new ArrayList<Long>();
			List<Long> memberBookIds = new ArrayList<Long>();
			Long memberDrmLogId = 0L;
			MemberDrmLog memberDrmLog = null;
			System.err.println("finally: " + resp);
			for (String itemId : itemWithChilds.getChilds()) {
				MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemId);
				assertNotNull("新增子書未新增", targetBook);
				Assert.assertTrue("新增套書,子書不存在對應DRM", targetBook.getMemberDrmLogs().size() > 0);
				Assert.assertTrue("新增套書,子書不存在對應關係", targetBook.getMemberBookDrmMappings().size() > 0);
				for (MemberBookDrmMapping mapping : targetBook.getMemberBookDrmMappings()) {
					memberBookDrmMappingIds.add(mapping.getId());
				}
				if (memberDrmLog == null) {
					memberDrmLog = targetBook.getMemberDrmLogs().get(0);
					memberDrmLogId = memberDrmLog.getId();
				}
			}

			List<MemberBook> memberSerialBooks = memberDrmLog.getMemberBooks();
			for (MemberBook book : memberSerialBooks) {
				memberBookIds.add(book.getId());
			}

			Assert.assertTrue("新增套書後MemberBook 數量不一致", memberSerialBooks.size() == serialSize);
			Assert.assertTrue("新增套書後MemberBookDrmMappingIds數量不一致", memberBookDrmMappingIds.size() == serialSize);

			// 刪除一般套書書籍授權,檢查書籍授權資料已刪除
			handler.delDRM(em, dto);
			em.refresh(member);
			Assert.assertTrue("刪除後MemberDrmLog未刪除", em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class)
					.setParameter("id", memberDrmLogId).getResultList().size() == 0);

			for (Long mappingId : memberBookDrmMappingIds) {
				Assert.assertTrue("刪除後MemberBookDrmMapping未刪除",
						em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class)
								.setParameter("id", mappingId).getResultList().size() == 0);
			}

			for (Long bookId : memberBookIds) {
				Assert.assertTrue("刪除後MemberBook未刪除", member.getMemberBookByMBId(bookId).getDeleted());
			}

		} catch (ServerException e) {
			e.printStackTrace();
			System.out.println("finally: " + e.getError_message());
			fail();
		} catch (Exception e) {

			e.printStackTrace();
			fail();
		}

	}

	// 測試更新套書(異動套書內容)
	@Test
	public void test005RescanDRMWithModifyItemChilds() {
		currentTestName = "testRescanDRMWithModifyItemChilds";
		try {
			MemberDrmHandler handler = new MemberDrmHandler();
			MemberDrmLogDTO dto = new MemberDrmLogDTO();
			dto.setBooksMemberId(member.getBmemberId());
			dto.setViewerBaseURI("http://123/");
			dto.setItemId(itemWithChilds.getId());
			dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
			dto.setReadDays("10");
			dto.setSessionToken("");
			dto.setTransactionId(UUID.randomUUID().toString());
			dto.setType("1");

			// add first
			// 新增一般套書書籍授權,檢查書籍授權資料已新增
			String resp = handler.addDRM(em, dto);
			em.refresh(member);
			int serialSize = itemWithChilds.getChilds().size();
			List<Long> memberBookDrmMappingIds = new ArrayList<Long>();
			List<Long> memberBookIds = new ArrayList<Long>();
			Long memberDrmLogId = 0L;
			MemberDrmLog memberDrmLog = null;
			System.err.println("finally: " + resp);
			for (String itemId : itemWithChilds.getChilds()) {
				MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemId);
				assertNotNull("新增子書未新增", targetBook);
				Assert.assertTrue("新增套書,子書不存在對應DRM", targetBook.getMemberDrmLogs().size() > 0);
				Assert.assertTrue("新增套書,子書不存在對應關係", targetBook.getMemberBookDrmMappings().size() > 0);
				for (MemberBookDrmMapping mapping : targetBook.getMemberBookDrmMappings()) {
					memberBookDrmMappingIds.add(mapping.getId());
				}
				if (memberDrmLog == null) {
					memberDrmLog = targetBook.getMemberDrmLogs().get(0);
					memberDrmLogId = memberDrmLog.getId();
				}
			}

			List<MemberBook> memberSerialBooks = memberDrmLog.getMemberBooks();
			for (MemberBook book : memberSerialBooks) {
				memberBookIds.add(book.getId());
			}
			Assert.assertTrue("新增套書後MemberBook 數量不一致", memberSerialBooks.size() == serialSize);
			Assert.assertTrue("新增套書後MemberBookDrmMappingIds數量不一致", memberBookDrmMappingIds.size() == serialSize);

			// 異動套書內容
			memberBookDrmMappingIds.clear();

			List<String> childsList = itemWithChilds.getChilds();
			String removedItemChild = childsList.get(0);
			String addedItemChild = itemNormal.getId();
			List<String> afterRescanChildsList = new ArrayList<String>();
			Long afterRescanMemberDrmLogId = 0L;
			MemberDrmLog afterScanMemberDrmLog = null;
			childsList.remove(0);
			childsList.add(addedItemChild);
			itemWithChilds.setChilds(childsList);
			em.getTransaction().begin();
			em.persist(itemWithChilds);
			em.getTransaction().commit();
			em.refresh(itemWithChilds);
			RescanDRMHandler rescanDRMHandler = new RescanDRMHandler();
			rescanDRMHandler.rescanMemberDRM(em, itemWithChilds.getId(), member.getId());
			em.refresh(member);
			memberBookIds.clear();
			// removedItemChildBook 的對應
			// ADD CHILD ITEM 的對應
			for (String itemId : itemWithChilds.getChilds()) {
				MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemId);
				memberBookIds.add(targetBook.getId());
				assertNotNull("異動套書子書未新增", targetBook);
				Assert.assertTrue("異動新增套書,子書不存在對應DRM", targetBook.getMemberDrmLogs().size() > 0);
				Assert.assertTrue("異動新增套書,子書不存在對應關係", targetBook.getMemberBookDrmMappings().size() > 0);
				for (MemberBookDrmMapping mapping : targetBook.getMemberBookDrmMappings()) {
					afterRescanChildsList.add(mapping.getMemberBook().getItem().getId());
				}
				if (afterScanMemberDrmLog == null) {
					afterScanMemberDrmLog = targetBook.getMemberDrmLogs().get(0);
					afterRescanMemberDrmLogId = memberDrmLog.getId();
				}
			}

			MemberBook markAsDeletedMemberBook = member.getMemberNormalBookByItemId(em, removedItemChild);
			Assert.assertTrue("異動套書後舊子書未刪除", markAsDeletedMemberBook.getDeleted());
			MemberBook newAddedMemberBook = member.getMemberNormalBookByItemId(em, addedItemChild);
			Assert.assertTrue("異動套書後新子書未新增", newAddedMemberBook != null);
			Assert.assertTrue("異動套書子書內容不一致", childsList.containsAll(afterRescanChildsList));
			Assert.assertTrue("異動套書刪除子書 MemberBookDrmMapping 未刪除",
					markAsDeletedMemberBook.getMemberBookDrmMappings().size() == 0);
			Assert.assertTrue("異動套書DRM不一致", afterRescanMemberDrmLogId.equals(memberDrmLogId));

			// 刪除一般套書書籍授權,檢查書籍授權資料已刪除
			handler.delDRM(em, dto);
			em.refresh(member);
			Assert.assertTrue("刪除後MemberDrmLog未刪除", em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class)
					.setParameter("id", memberDrmLogId).getResultList().size() == 0);

			for (Long mappingId : memberBookDrmMappingIds) {
				Assert.assertTrue("刪除後MemberBookDrmMapping未刪除",
						em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class)
								.setParameter("id", String.valueOf(mappingId)).getResultList().size() == 0);
			}

			for (Long bookId : memberBookIds) {
				Assert.assertTrue("刪除後MemberBook未刪除", member.getMemberBookByMBId(bookId).getDeleted());
			}

		} catch (ServerException e) {
			e.printStackTrace();
			System.out.println("finally: " + e.getError_message());
			fail();
		} catch (Exception e) {

			e.printStackTrace();
			fail();
		}

	}

	public void testDrmMappingOperation() {

		try {
			long drmLogId = 713274;
			String itemId = "E050000948";
			Long memberId = 53126L;

			MemberBook memebrBook1 = MemberBook.findNormalByItemId(em, memberId, itemId);

			List<MemberDrmLog> drms = memebrBook1.getMemberDrmLogs();

			System.out.println(drms.get(0));

			for (MemberDrmLog memberDrmLog : drms) {
				System.out.println(memberDrmLog.getItem());
			}

			MemberDrmLog memberDrmLog = MemberDrmLog.getMemberDrmLogById(em, drmLogId);
			// List<MemberBookDrmMapping> MemberBookDrmMappings =
			// memberDrmLog.getMemberBookDrmMappings();
			List<MemberBook> MemberBooks = memberDrmLog.getMemberBooks();

			System.out.println(memberDrmLog);

			// System.out.println(MemberBookDrmMappings.get(0));

			System.out.println(MemberBooks.get(0));

			System.out.println("done");

		} catch (Exception e) {
			e.printStackTrace();
		}
		// List<MemberBookDrmMapping> mbdm =
		// em.createNamedQuery(arg0)
	}

	public void testGetLongestMemberDrmLog() {

		try {
			em.getTransaction().begin();
			long drmLogId = 713274;
			String itemId = "E050000939";
			Long memberId = 53126L;

			MemberBook memebrBook = MemberBook.findNormalByItemId(em, memberId, itemId);
			MemberDrmLog memberLog1 = DRMLogicUtility.getLongestMemberDrmLog(memebrBook.getMemberDrmLogs());
			MemberDrmLog memberLog2 = DRMLogicUtility
					.getLongestMemberDrmLogWithMapping(memebrBook.getMemberBookDrmMappings());

			org.junit.Assert.assertNotNull(memberLog1);
			org.junit.Assert.assertNotNull(memberLog2);

			List<MemberBook> list = memberLog1.getMemberBooks();
			System.out.println(list.get(0));

			org.junit.Assert.assertNotNull(list);

			em.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	public void testDrmOperation() {

		String itemId = "E050000968";

		try {

			String storedProcedureName = "booksapi.serial_book_operation";
			em.getTransaction().begin();
			StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcedureName);
			query.registerStoredProcedureParameter("in_item_id", String.class, ParameterMode.IN);
			query.setParameter("in_item_id", itemId);
			query.execute();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	public void testMemberDrmOperation() {

		String itemId = "E050000968";
		int memberId = 53126;

		try {
			String storedProcedureName = "serial_book_member_operation";
			em.getTransaction().begin();
			StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcedureName);
			query.registerStoredProcedureParameter("in_item_id", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("in_member_id", Integer.class, ParameterMode.IN);
			query.setParameter("in_item_id", itemId);
			query.setParameter("in_member_id", memberId);
			query.execute();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	// private MemberDrmLog insertDummyMemberDRMLog(String memberId, String itemId,
	// String transactionId) throws Exception {
	// MemberDrmLog drmObject = new MemberDrmLog();
	// try {
	// em.getTransaction().begin();
	// Item item = em.find(Item.class, itemId);
	// drmObject.setId(80000L);
	// drmObject.setItem(item);
	// drmObject.setDownloadExpireTime("2999/10/10 12:00:00");
	// drmObject.setReadDays(Integer.valueOf(999));
	// drmObject.setReadExpireTime("2017/09/10 12:00:00");
	// drmObject.setType(3);
	// drmObject.setTransactionId(transactionId);
	// drmObject.setStatus(1);
	// drmObject.setLastUpdated(CommonUtil.zonedTime());
	// Member member = Member.getMemberByBMemberId(em, memberId,
	// Member.MEMBERTYPE_NORMALREADER);
	// drmObject.setMember(member);
	// em.persist(drmObject);
	// em.getTransaction().commit();
	// } catch (Exception e) {
	// throw e;
	// }
	//
	// return drmObject;
	// }

	// 同樣的 TransactionId 不可重複新增書籍授權
	// @Test
	// public void testDelAddDRM() {
	// try {
	// MemberDrmHandler handler = new MemberDrmHandler();
	// MemberDrmLogDTO dto = new MemberDrmLogDTO();
	// dto.setBooksMemberId(member.getBmemberId());
	// dto.setViewerBaseURI("http://123/");
	// dto.setItemId(itemWithChilds.getId());
	// dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
	// dto.setReadDays("10");
	// dto.setSessionToken("");
	// dto.setTransactionId(UUID.randomUUID().toString());
	// if (itemWithChilds.getBookFiles().get(0).getIsTrial())
	// dto.setType("4");
	// else
	// dto.setType("1");
	// String resp = handler.addDRM(em,dto);
	// // delete
	// resp = handler.delDRM(em,dto);
	// // tmp try re-add
	// resp = handler.addDRM(em,dto);
	// System.err.println("finally: " + resp);
	// boolean isTrial=dto.getType().equals("4");
	//
	// MemberBook targetBook= isTrial ?
	// member.getMemberTrialBookByItemId(em,itemTrial.getId())
	// : member.getMemberNormalBookByItemId(em,itemTrial.getId());
	//
	// assertNotNull(targetBook);
	//
	// }catch (ServerException e)
	// {
	// System.out.println("finally: " + e.getError_message());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// fail();
	// }
	// }

	// @Test
	// public void testDupAddDRM() {
	// String resp = "";
	// try {
	// MemberDrmHandler handler = new MemberDrmHandler();
	// MemberDrmLogDTO dto = new MemberDrmLogDTO();
	// dto.setViewerBaseURI("http://123/");
	// dto.setItemId(itemNormal.getId());
	// dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
	// dto.setReadDays("10");
	// dto.setSessionToken("");
	// dto.setTransactionId(UUID.randomUUID().toString());
	// dto.setBooksMemberId(member.getBmemberId());
	//
	//// if (itemTrial.getBookFiles().get(0).getIsTrial()){
	//// dto.setType("4");
	//// resp = handler.addTrial(em, dto.getType(), dto.getBooksMemberId(),
	// dto.getItemId(), dto.getViewerBaseURI());
	//// }
	//// else{
	// dto.setType("1");
	// resp = handler.addDRM(em,dto);
	//// }
	//
	// // tmp try re-add
	// try {
	// resp = handler.addDRM(em,dto);
	// System.err.println("finally: " + resp);
	// fail();
	// } catch (Exception e) {
	// System.out.println(e.getMessage());
	// }
	// em.refresh(member);
	//
	// MemberBook targetBook=
	// member.getMemberNormalBookByItemId(em,itemNormal.getId());
	//
	// assertNotNull("新增試閱書不存在",targetBook);
	//
	// }catch (ServerException e)
	// {
	// System.out.println("finally: " + e.getError_message());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// fail();
	// }
	// }

	// 套書的試閱只有一本 ,待確認
	// @Test
	// public void testAddTrialDRMWithChilds() {
	// try {
	// MemberDrmHandler handler = new MemberDrmHandler();
	// MemberDrmLogDTO dto = new MemberDrmLogDTO();
	// dto.setBooksMemberId(member.getBmemberId());
	// dto.setViewerBaseURI("http://123/");
	// dto.setItemId(itemWithChilds.getId());
	// dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
	// dto.setReadDays("10");
	// dto.setSessionToken("");
	// dto.setTransactionId(UUID.randomUUID().toString());
	// dto.setType("4");
	// String resp = handler.addDRM(em,dto);
	//
	// // tmp try re-add
	//// resp = handler.addDRM(em);
	// System.err.println("finally2: " + resp);
	// if (!resp.contains("http"))
	// fail();
	//
	// MemberBook
	// targetBook=member.getMemberTrialBookByItemId(em,itemWithChilds.getId());
	// assertNotNull(targetBook);
	//// assertEquals(member.getMemberBooks().size(),5);
	//
	// }catch (ServerException e)
	// {
	// System.out.println("finally: " + e.getError_message());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// fail();
	// }
	// }

	// @Test
	// public void testDelWithChildDRM() {
	// try {
	// String transactionId = UUID.randomUUID().toString();
	// MemberDrmHandler handler = new MemberDrmHandler();
	// MemberDrmLogDTO dto = new MemberDrmLogDTO();
	// dto.setBooksMemberId(member.getBmemberId());
	// dto.setViewerBaseURI("http://123/");
	// dto.setItemId(itemWithChilds.getId());
	// dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
	// dto.setReadDays("10");
	// dto.setSessionToken("");
	// dto.setTransactionId(transactionId);
	// dto.setType("1");
	// handler.addDRM(em,dto);
	//
	// String resp2 = handler.delDRM(em,dto);
	// System.out.println("Del finally: " + resp2);
	// for (MemberBook m:member.getMemberBooks())
	// {
	// assertEquals("del",m.getAction());
	// }
	//
	// }catch (ServerException e)
	// {
	// System.out.println("finally: " + e.getError_message());
	// if (e.getError_code().endsWith("999"))
	// {
	// e.printStackTrace();
	// fail();
	// }
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// fail();
	// }
	// }

	// @Test
	// public void testDelDRM() {
	// try {
	// String transactionId = UUID.randomUUID().toString();
	// MemberDrmHandler handler = new MemberDrmHandler();
	// MemberDrmLogDTO dto = new MemberDrmLogDTO();
	// dto.setBooksMemberId(member.getBmemberId());
	// dto.setViewerBaseURI("http://123/");
	// dto.setItemId(itemNormal.getId());
	// dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
	// dto.setReadDays("10");
	// dto.setSessionToken("");
	// dto.setTransactionId(transactionId);
	// dto.setType("1");
	// handler.addDRM(em,dto);
	//
	// String resp2 = handler.delDRM(em,dto);
	// System.out.println("Del finally: " + resp2);
	// for (MemberBook m:member.getMemberBooks())
	// {
	// assertEquals("del",m.getAction());
	// }
	//
	// }catch (ServerException e)
	// {
	// System.out.println("finally: " + e.getError_message());
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// fail();
	// }
	// }

}
