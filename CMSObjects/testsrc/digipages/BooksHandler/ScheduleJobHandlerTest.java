package digipages.BooksHandler;

import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.BookConvert.lossless.LosslessCompressionHandler;
import digipages.CommonJobHandler.Ec2WorkerManager;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.Item;
import model.Member;
import model.ScheduleJob;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScheduleJobHandlerTest {
	private static DPLogger logger = DPLogger.getLogger(ScheduleJobHandlerTest.class.getName());
	private static EntityManagerFactory emf;
	private static EntityManager em;
	static BlockingQueue<ScheduleJob> jobQueue = new ArrayBlockingQueue<>(100);
	private static List<ScheduleJob> jobs;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();
		ScheduleJobManager.setEntityManagerFactory(emf);
		Ec2WorkerManager.setEntityManagerFactory(emf);
		LocalRunner lr = new LocalRunner(jobQueue, emf,null);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// try to remove all my tests records.
		em.getTransaction().begin();
		em.createNativeQuery("delete from schedule_job where job_name='TestJobName' ;").executeUpdate();
		em.getTransaction().commit();
		em.close();
		emf.close();
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAAddJobs() {
		try {
			
			
			List <ScheduleJob> jobs=new ArrayList<>();
			for (int i=0; i< 100;i++){
				ScheduleJob tmpJob = createTmpJob(i);
				jobs.add(tmpJob);
			}
			ScheduleJobManager.addJobs(jobs);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testAddLosslessCompressionHandler() {
		try {
			
			ScheduleJob sj = new ScheduleJob();
			sj.setJobParam("{\r\n" + 
					"        \"targetBucket\": \"s3public-auth.books.com.tw\",\r\n" + 
					"        \"folder\": \"trial-converted/7ED9E7/286393\",\r\n" + 
					"        \"bookFileName\": \"E060002456.pdf\",\r\n" + 
					"        \"requestData\": \"{\\\"author\\\":\\\"茉莉戈波提爾曼寧\\\",\\\"book_file_id\\\":\\\"286393\\\",\\\"c_title\\\":\\\"E060002456\\\",\\\"o_title\\\":\\\"upload-test\\\",\\\"call_type\\\":\\\"4\\\",\\\"count_TimeoutMointor\\\":0,\\\"efile_nofixed_name\\\":\\\"E060002456.pdf\\\",\\\"efile_url\\\":\\\"auth-aws-ebook/tmp/\\\",\\\"forceCheck\\\":false,\\\"format\\\":\\\"pdf\\\",\\\"intro\\\":\\\"簡介\\\",\\\"isbn\\\":\\\"9789579684255\\\",\\\"isTrial\\\":true,\\\"item\\\":\\\"91296787-5e5f-474b-ae7e-14a474a0c45a\\\",\\\"preview_content\\\":\\\"10\\\",\\\"preview_type\\\":\\\"1\\\",\\\"publish_date\\\":\\\"2015/5/18\\\",\\\"publisher_name\\\":\\\"貓頭鷹\\\",\\\"request_time\\\":\\\"2020-06-15T12:34:07 08:00\\\",\\\"return_file_num\\\":1,\\\"status\\\":\\\"A\\\",\\\"version\\\":\\\"V001.0001\\\",\\\"pageDirection\\\":\\\"ltr\\\",\\\"type\\\":\\\"book\\\"}\",\r\n" + 
					"        \"convertPage\": 0,\r\n" + 
					"        \"size\": \"6836972\",\r\n" + 
					"        \"result\": true,\r\n" + 
					"        \"errorMessage\": \"\",\r\n" + 
					"        \"logFolder\": \"log-space/trial-converted/91296787-5e5f-474b-ae7e-14a474a0c45a/286393\",\r\n" + 
					"        \"totalPage\": 0,\r\n" + 
					"        \"thumbnails\": [],\r\n" + 
					"        \"bookUniId\": \"91296787-5e5f-474b-ae7e-14a474a0c45a_fixedlayout_trial\",\r\n" + 
					"        \"bookFileId\": 0,\r\n" + 
					"        \"cmsToken\": \"1cee3c3fg3BHXLQJqfewG/XYerJbz7EI951ZXWu/NDoWIwMtFrL5/ziOvW+vj6sQfzlaWOSeCCgGE50ndSzJSzdQXjGe6VQN0tKuCRmq7O+k3x+xlr/6n9zjE2FpUe/iKEURiDJ0h+y+dDG30=\",\r\n" + 
					"        \"pageWait\": 0,\r\n" + 
					"        \"losslessNotify\": false\r\n" + 
					"    }");
			sj.setJobGroup("286393");
			sj.setJobName(LosslessCompressionHandler.class.getName());
			sj.setJobRunner(2);
			ScheduleJobManager.addJob(sj);
			
		} catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	private ScheduleJob createTmpJob(int i) {
		ScheduleJob sj = new ScheduleJob();
		sj.setJobGroup("TestGroup");
		sj.setJobName("TestJobName");
		sj.setJobRunner(1); // for lambda
		sj.setPriority(500);
		sj.setShouldStartTime(new Date());
		sj.setStatus(0);
		sj.setRetryCnt(0);
		
		sj.setJobParam(i);
		return sj;
	}

	@Test
	public void testBTakeJobs() {

		UUID uuid = UUID.randomUUID();
		try {
			ScheduleJob jobParam = new ScheduleJob();
			
			jobParam.setUuid(uuid);
			jobParam.setJobName("TestJobName");
			jobParam.setJobGroup("TestGroup");
			jobParam.setJobRunner(1);
			jobs=ScheduleJobManager.takeJobs(jobParam,10);
		}catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	@Test
	public void testCFinishJobs() {

//		List <Long> finishJobIds = listJobIds(jobs);
		
		for (ScheduleJob tmpJob:jobs)
		{
			tmpJob.setStatus(9);
			tmpJob.setResultStr("Success");
			tmpJob.setLastUpdated(new Date());
		}
		
		try {
			ScheduleJobManager.finishJobs(jobs);
		}catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	@Test
	public void testDRefreshJobs() throws ServerException
	{
		// add jobs
		
		List <ScheduleJob> jobs=new ArrayList<>();
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(0);
			jobs.add(tmpJob);
		}
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(1);
			jobs.add(tmpJob);
		}
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(2);
			jobs.add(tmpJob);
		}
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(2);
			tmpJob.setRetryCnt(0);
			jobs.add(tmpJob);
		}
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(2);
			tmpJob.setRetryCnt(1);
			jobs.add(tmpJob);
		}
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(2);
			tmpJob.setRetryCnt(2);
			jobs.add(tmpJob);
		}
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(2);
			tmpJob.setRetryCnt(3);
			jobs.add(tmpJob);
		}
		for (int i=0; i< 10;i++){
			ScheduleJob tmpJob = createTmpJob(i);
			tmpJob.setShouldFinishTime(new Date()); // now
			tmpJob.setStatus(1); // processing
			tmpJob.setJobRunner(2);
			tmpJob.setRetryCnt(4);
			jobs.add(tmpJob);
		}


		
		ScheduleJobManager.addJobs(jobs);
		
		Object o=em.createNativeQuery("select count(*) from schedule_job where status=1 and should_finish_time < now() and retry_cnt<3;").getSingleResult();
		long i = (Long) o;
		if (i<=0)
			fail("No Record Crated");
		
		ScheduleJobManager.refreshJobs();
		o=em.createNativeQuery("select count(*) from schedule_job where status=1 and should_finish_time < now() and retry_cnt<3;").getSingleResult();
		i = (Long) o;
		if (i>0)
			fail("Still Records not refreshed.");
		
		o=em.createNativeQuery("select count(*) from schedule_job where status=0 and retry_cnt<3;").getSingleResult();
		i = (Long)o;
		if (i<30)
			fail("Recovered jobs count mismatch.");

		o=em.createNativeQuery("select count(*) from schedule_job where status=-1 and retry_cnt>=3;").getSingleResult();
		i = (Long)o;
		if (i<30)
			fail("Recovered jobs count mismatch.");
		
		
	}


}
