package digipages.BooksHandler;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.common.DRMLogicUtility;
import digipages.dto.MemberDrmLogDTO;
import digipages.exceptions.ServerException;
import model.BookFile;
import model.Item;
import model.Member;
import model.MemberBook;
import model.MemberBookDrmMapping;
import model.MemberDrmLog;
import model.MemberReadList;

@FixMethodOrder(MethodSorters.NAME_ASCENDING) 
public class MemberDrmHandlerTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private Member member;
	private Item itemTrial;
	private Item itemWithChilds;
	private Item itemNormal;
	private static String currentTestName; 

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
//		SampleDataGenerator gen = new SampleDataGenerator(em);
//		em.getTransaction().begin();
//
//		member=gen.generateBenchMember(false);
//		
//		itemTrial =gen.genSampleTrialItem();
//		em.persist(itemTrial);
//		
//		itemNormal = gen.genSampleItem();
//		em.persist(itemNormal);
//		
//		itemWithChilds = gen.genSampleBookWithChilds(em);
//		em.persist(itemWithChilds);
//		
//		em.getTransaction().commit();
	}

	@After
	public void tearDown() throws Exception {
//	    if (!em.getTransaction().isActive()) {
//	        em.getTransaction().begin();    
//	    }
//		member=em.merge(member);
//		em.remove(member);
//		itemTrial = em.merge(itemTrial);
//		em.remove(itemTrial);
//		if(!"testRescanDRMWithModifyItemChilds".equalsIgnoreCase(currentTestName)){
//		    itemWithChilds = em.merge(itemWithChilds);
//		}
//		em.remove(itemWithChilds);
//		em.getTransaction().commit();
		em.close();
	}
	
	@Test
    public void test000AddMember() {
		em.getTransaction().begin();
		try {
		em.persist(Member.createNewMember("testDup" , "testDup","NormalReader" ));
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		em.getTransaction().commit();
		
	}

	//更新書單
	   @Test
	      public void testAllMemberAddReadList() {
	          try {
	             
	              List<Member>  memebrList =  em.createNativeQuery("select * from member where member_type <> 'Guest' and bmember_id not like 'bench%' and bmember_id is not null ",Member.class).getResultList();
	              
	              for (Member member : memebrList) {
	                  if(!member.getMemberType().equalsIgnoreCase("Guest") && !member.getBmemberId().contains("bench") && member.getBmemberId()!=null) {
    	                  em.getTransaction().begin();
    	                  MemberReadList listMediabook = new MemberReadList("mediabook","影音書",member);
    	                  em.persist(listMediabook);
    	                  member.getMemberReadLists().add(listMediabook);
    	                  
    	                  MemberReadList listAudiobook = new MemberReadList("audiobook","有聲書",member);
    	                  em.persist(listAudiobook);
    	                  member.getMemberReadLists().add(listAudiobook);
    	                  
    	                  System.out.println(member.getBmemberId());
    	                  em.getTransaction().commit();
	                  }
                }
	              
	          } catch (Exception e) {
	              
	              e.printStackTrace();
	              fail();
	          }
	      }
	
	@Test
	  public void testDelDRM() {
	      try {
	          String transactionId = UUID.randomUUID().toString();
	          MemberDrmHandler handler = new MemberDrmHandler();
	          MemberDrmLogDTO dto = new MemberDrmLogDTO();
	          dto.setBooksMemberId("enjoyto");
	          dto.setViewerBaseURI("http://123/");
	          dto.setItemId("E071908862");
	          dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
	          dto.setReadDays("100");
	          dto.setSessionToken("");
	          dto.setTransactionId("E079999996");
	          dto.setType("1");
//	          handler.addDRM(em,dto);
	          
	          String resp2 = handler.delDRM(em,dto);
	          System.out.println("Del finally: " + resp2);
//	          for (MemberBook m:member.getMemberBooks())
//	          {
//	              assertEquals("del",m.getAction());
//	          }
	          
	      }catch (ServerException e)
	      {
	          System.out.println("finally: " + e.getError_message());
	      } catch (Exception e) {
	          
	          e.printStackTrace();
	          fail();
	      }
	  }
	
	@Test
    public void testJust() {
        //測是效能用
        String resp = "";
        try {
//            select * from book_file bf where item_id = 'E050000124'

            BookFile bf = BookFile.findLastVersionByItemId(em, "E050000124");
            //-2143351901
            bf.setSize(4775807L);
            em.persist(bf);
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
            
    }
	@Test
    public void test000AddDRM() {
    	//測是效能用
        String resp = "";
        try {
//            select * from book_file bf where item_id = 'E050000124'

//            File afile =new File("D:\\cmsdrm1101.txt");
//            FileInputStream fis = new FileInputStream(afile);
//            
//            //Construct BufferedReader from InputStreamReader
//            BufferedReader br = new BufferedReader(new InputStreamReader(fis,"UTF8"));
//            MemberDrmHandler handler = new MemberDrmHandler();
//            String line = null;
//            while ((line = br.readLine()) != null) {
////                cms_transaction_id,id,item,type,download_expire_time,read_days,read_expire_time,submit_id,notify_mail_type,status
////                20171101008439,chengsh3737,E050016850,1,9999/12/31 00:00:00,,,20171101008439,A,0
//                
//                String[] data = line.split(",");
//                MemberDrmLogDTO dto = new MemberDrmLogDTO();
//                String txid = UUID.randomUUID().toString();
//                dto.setBooksMemberId(data[1]);
//                //新增一般書籍授權,檢查書籍授權資料已新增
//                dto.setViewerBaseURI("http://123/");
//                dto.setReadDays(data[5]);
//                dto.setSessionToken("");
//                dto.setDownloadExpireTime("2099/12/31 23:59:59");
//                dto.setTransactionId(data[0]);
//                dto.setType(data[3]);
//                dto.setSubmitId(data[7]);
//                dto.setItemId(data[2]);
//                dto.setReadExpireTime(data[6]);
//                resp = handler.addDRM(em, dto);
//                System.out.println(data[1]+" "+resp);
//                
//            }
//         
//            br.close();
            
//            
//            
//        	
            List<String> bmemberId = new ArrayList<String>();
//            bmemberId.add("E050027147");
//            bmemberId.add("sumk");
//            bmemberId.add("paulchen");
//            bmemberId.add("t221109365");
//            bmemberId.add("spalding");
//            bmemberId.add("john70480");
//            bmemberId.add("enjoyto");
            bmemberId.add("nt0930799968");
//            bmemberId.add("cjwong");
//            bmemberId.add("qjsmpyk");
//            bmemberId.add("pieces1017");
//            bmemberId.add("genius05");
//            bmemberId.add("morph66");
//            bmemberId.add("cindy520240");
//            bmemberId.add("kwteen1004");
//            bmemberId.add("testgood");
//            bmemberId.add("testgood1");
//            bmemberId.add("testgood2");
//            bmemberId.add("chungyiliu122");
//            bmemberId.add("0421andy");
//            bmemberId.add("jason940142");
//            bmemberId.add("mayday31332");
            
                    
                    
//            bmemberId.add("catherine112");
//            bmemberId.add("pieces1017");
//            bmemberId.add("genius05");
//            bmemberId.add("enjoyto");
//            "E060000892"
            
            
            List<MemberDrmLogDTO> drms = new ArrayList<MemberDrmLogDTO>();
//            MemberDrmLogDTO dto = new MemberDrmLogDTO();
//            dto.setItemId("E070000010");
//            dto.setReadExpireTime("2022/12/31 23:59:59");
//            drms.add(dto);

            List<String> items = new ArrayList<String>();
            
            items.add("G000042033");
            items.add("G000042036");
//            items.add("E070002558");
//            items.add("E070002404");
//            items.add("E070002406");
//            items.add("E070002412");
//            items.add("E070002423");
//            items.add("E070002426");
//            items.add("E070002427");
//            items.add("E070002440");
//            items.add("E080000458");
//            items.add("E080000459");
//            items.add("E080000468");
//            items.add("E080000469");
//            items.add("E070002574");
//            items.add("E070002577");


//            items.add("E080601398");
            
            for (String string : items) {
                MemberDrmLogDTO dto = new MemberDrmLogDTO();
                dto.setItemId(string);
                dto.setReadExpireTime("2099/12/31 23:59:59");
                drms.add(dto);
            }
            
            MemberDrmHandler handler = new MemberDrmHandler();
            for (String memberId : bmemberId) {
	            for (MemberDrmLogDTO memberDrmLogDTO : drms) {
	                try {
	                String txid = UUID.randomUUID().toString();
	                memberDrmLogDTO.setBooksMemberId(memberId);
	                //新增一般書籍授權,檢查書籍授權資料已新增
	                memberDrmLogDTO.setViewerBaseURI("http://123/");
	                memberDrmLogDTO.setReadDays("100");
	                memberDrmLogDTO.setSessionToken("");
	                memberDrmLogDTO.setDownloadExpireTime("2099/12/31 23:59:59");
	                memberDrmLogDTO.setTransactionId(memberDrmLogDTO.getItemId());
	                memberDrmLogDTO.setType("1");
	                memberDrmLogDTO.setSubmitId(memberDrmLogDTO.getItemId());
	                resp = handler.addDRM(em, memberDrmLogDTO);
	                System.out.println(memberId+" "+resp);
	                
                    } catch (ServerException e) {
                        System.out.println("finally: " + e.getError_message());
                    }
                }
        	}

        } catch (Exception e) {

            e.printStackTrace();
            fail();
        }
    }
	 
    @Test
    public void test001AddModifyDelDRM() {
        String resp = "";
        try {
            MemberDrmHandler handler = new MemberDrmHandler();
            MemberDrmLogDTO dto = new MemberDrmLogDTO();
            dto.setViewerBaseURI("http://123/");
            dto.setItemId(itemNormal.getId());
            dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
            dto.setReadDays("10");
            dto.setSessionToken("");
            dto.setTransactionId(UUID.randomUUID().toString());
            dto.setBooksMemberId(member.getBmemberId());
            dto.setType("1");
            
            //新增一般書籍授權,檢查書籍授權資料已新增
            resp = handler.addDRM(em, dto);
            System.err.println("addDRM: " + resp);
            em.refresh(member);
            MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
            assertNotNull("書籍未新增",targetBook);
            Assert.assertTrue(targetBook.getMemberBookDrmMappings().size()>0);
            Assert.assertTrue(targetBook.getMemberDrmLogs().size()>0);
            
            MemberBookDrmMapping memberBookDrmMapping = targetBook.getMemberBookDrmMappings().get(0);
            Long memberBookDrmMappingId = memberBookDrmMapping.getId();
            MemberDrmLog memberDrmLog = targetBook.getMemberDrmLogs().get(0);
            Long memberDrmLogId = memberDrmLog.getId();
            
            // 修改一般書籍授權,檢查書籍授權資料已修改
            String modifyDownloadExpireTime = "2999/10/10 12:00:00.00";
            String modifyReadStartTime = "2017/01/10 12:00:00.00";
            String modifyReadExpireTime = "2017/02/10 12:00:00.00";
            dto.setReadDays("5");
            dto.setType("3");//免費領取
            dto.setDownloadExpireTime(modifyDownloadExpireTime);
            dto.setReadStartTime(modifyReadStartTime);
            dto.setReadExpireTime(modifyReadExpireTime);
            
            resp = handler.modifyDRM(em, dto);
            System.err.println("modifyDRM: " + resp);
            em.refresh(member);
            em.refresh(memberDrmLog);
            Assert.assertTrue("修改DRM ReadDay 失敗",memberDrmLog.getReadDays() == 5);
            Assert.assertTrue("修改DRM Type 失敗",memberDrmLog.getType() == 3);
            Assert.assertTrue("修改DRM DownloadExpireTime 失敗",memberDrmLog.getDownloadExpireTime().equalsIgnoreCase(modifyDownloadExpireTime));
            Assert.assertTrue("修改DRM ReadStartTime 失敗",memberDrmLog.getReadStartTime().equalsIgnoreCase(modifyReadStartTime));
            Assert.assertTrue("修改DRM ReadExpireTime 失敗",memberDrmLog.getReadExpireTime().equalsIgnoreCase(modifyReadExpireTime));
            
            
            //刪除一般書籍授權,檢查書籍授權資料已刪除
            resp = handler.delDRM(em, dto);
            System.err.println("delDRM: " + resp);
            em.refresh(member);
            targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
            Assert.assertTrue(targetBook.isDeleted());
            Assert.assertTrue(em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class).setParameter("id", memberDrmLogId).getResultList().size() == 0);
            Assert.assertTrue(em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class).setParameter("id", memberBookDrmMappingId).getResultList().size() == 0);

        } catch (ServerException e) {
            System.out.println("finally: " + e.getError_message());
        } catch (Exception e) {

            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void test002AddTrialDRM() {
        String resp = "";
        try {
            MemberDrmHandler handler = new MemberDrmHandler();
            MemberDrmLogDTO dto = new MemberDrmLogDTO();
            dto.setViewerBaseURI("http://123/");
            dto.setItemId(itemTrial.getId());
            dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
            dto.setReadDays("10");
            dto.setSessionToken("");
            dto.setTransactionId(UUID.randomUUID().toString());
            dto.setBooksMemberId(member.getBmemberId());
            dto.setType("4");
            
            //新增試閱書籍,檢查書籍已新增
            resp = handler.addTrial(em, dto.getType(), String.valueOf(member.getId()), dto.getItemId(), dto.getViewerBaseURI());

            System.err.println("finally: " + resp);
            em.refresh(member);
            MemberBook targetBook = member.getMemberTrialBookByItemId(em, dto.getItemId());
            assertNotNull(targetBook);
            
        } catch (ServerException e) {
            System.out.println("finally: " + e.getError_message());
        } catch (Exception e) {

            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void test003AddModifyActiveAndInactiveDRM() {
        try {
            String transactionId = UUID.randomUUID().toString();
            MemberDrmHandler handler = new MemberDrmHandler();
            MemberDrmLogDTO dto = new MemberDrmLogDTO();
            dto.setBooksMemberId(member.getBmemberId());
            dto.setViewerBaseURI("http://123/");
            dto.setItemId(itemNormal.getId());
            dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
            dto.setReadDays("10");
            dto.setSessionToken("");
            dto.setTransactionId(transactionId);
            dto.setType("1");
            handler.addDRM(em,dto);
            
            //新增一般書籍授權,檢查書籍授權資料已新增
            String resp = handler.addDRM(em, dto);
            System.err.println("addDRM: " + resp);
            em.refresh(member);
            MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
            assertNotNull("書籍未新增",targetBook);
            Assert.assertTrue(targetBook.getMemberBookDrmMappings().size()>0);
            Assert.assertTrue(targetBook.getMemberDrmLogs().size()>0);
            
            //設定資料供後續檢查
            MemberBookDrmMapping memberBookDrmMapping = targetBook.getMemberBookDrmMappings().get(0);
            Long memberBookDrmMappingId = memberBookDrmMapping.getId();
            MemberDrmLog memberDrmLog = targetBook.getMemberDrmLogs().get(0);
            Long memberDrmLogId = memberDrmLog.getId();
            
            //刪除一般書籍授權,檢查書籍授權資料已刪除
            resp = handler.delDRM(em, dto);
            System.err.println("delDRM: " + resp);
            em.refresh(member);
            targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
            Assert.assertTrue(targetBook.isDeleted());
            Assert.assertTrue(em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class).setParameter("id", memberDrmLogId).getResultList().size() == 0);
            Assert.assertTrue(em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class).setParameter("id", memberBookDrmMappingId).getResultList().size() == 0);
            
            //刪除後重新啟用
            dto.setStatus("1");
            handler.modifyDRM(em, dto);
            targetBook = member.getMemberNormalBookByItemId(em, itemNormal.getId());
            Assert.assertTrue("重新啟用書籍未新增MemberBook","update".equalsIgnoreCase(targetBook.getAction())&&targetBook.isDeleted() == false);
            Assert.assertTrue("重新啟用書籍未新增MemberDrmLog",em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class).setParameter("id", memberDrmLogId).getResultList().size() == 1);
            Assert.assertTrue("重新啟用書籍未新增MemberBookDrmMapping",memberDrmLog.getMemberBookDrmMappings().get(0).getMemberBook().getId().equals(targetBook.getId()));
            
        }catch (ServerException e)
        {
            System.out.println("finally: " + e.getError_message());
        } catch (Exception e) {
            
            e.printStackTrace();
            fail();
        }
    }
	
	@Test
	public void test004AddDRMWithChilds() {
		try {
			MemberDrmHandler handler = new MemberDrmHandler();
			MemberDrmLogDTO dto = new MemberDrmLogDTO();
			dto.setBooksMemberId(member.getBmemberId());
			dto.setViewerBaseURI("http://123/");
			dto.setItemId(itemWithChilds.getId());
			dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
			dto.setReadDays("10");
			dto.setSessionToken("");
			dto.setTransactionId(UUID.randomUUID().toString());
			dto.setType("1");

	         //新增一般套書書籍授權,檢查書籍授權資料已新增
			String resp = handler.addDRM(em,dto);
			em.refresh(member);
			int serialSize = itemWithChilds.getChilds().size();
            List<Long> memberBookDrmMappingIds = new ArrayList<Long>();
            List<Long> memberBookIds = new ArrayList<Long>();
            Long memberDrmLogId = 0L;
            MemberDrmLog memberDrmLog = null;
            System.err.println("finally: " + resp);
            for (String itemId : itemWithChilds.getChilds()) {
                MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemId);
                assertNotNull("新增子書未新增",targetBook);
                Assert.assertTrue("新增套書,子書不存在對應DRM",targetBook.getMemberDrmLogs().size() > 0);
                Assert.assertTrue("新增套書,子書不存在對應關係",targetBook.getMemberBookDrmMappings().size() > 0);
                for (MemberBookDrmMapping mapping : targetBook.getMemberBookDrmMappings()) {
                    memberBookDrmMappingIds.add(mapping.getId());
                }
                if (memberDrmLog == null) {
                    memberDrmLog = targetBook.getMemberDrmLogs().get(0);
                    memberDrmLogId = memberDrmLog.getId();
                }
            }
            
            List<MemberBook> memberSerialBooks = memberDrmLog.getMemberBooks();
            for (MemberBook book : memberSerialBooks) {
                memberBookIds.add(book.getId());
            }
            
            Assert.assertTrue("新增套書後MemberBook 數量不一致",memberSerialBooks.size() == serialSize);
            Assert.assertTrue("新增套書後MemberBookDrmMappingIds數量不一致",memberBookDrmMappingIds.size() == serialSize );
            
            //刪除一般套書書籍授權,檢查書籍授權資料已刪除
            handler.delDRM(em, dto);
            em.refresh(member);
            Assert.assertTrue("刪除後MemberDrmLog未刪除",em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class).setParameter("id", memberDrmLogId).getResultList().size() == 0);
            
            for (Long mappingId : memberBookDrmMappingIds) {
                Assert.assertTrue("刪除後MemberBookDrmMapping未刪除",em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class).setParameter("id", mappingId).getResultList().size() == 0);
            }
            
            for (Long bookId : memberBookIds) {
                Assert.assertTrue("刪除後MemberBook未刪除",member.getMemberBookByMBId(bookId).getDeleted());
            }
			
			
		}catch (ServerException e){
		    e.printStackTrace();
			System.out.println("finally: " + e.getError_message());
			fail();
		} catch (Exception e) {
			
			e.printStackTrace();
			fail();
		}
		
	}
	
	//測試更新套書(異動套書內容)
	@Test
    public void test005RescanDRMWithModifyItemChilds() {
	    currentTestName = "testRescanDRMWithModifyItemChilds";
        try {
            MemberDrmHandler handler = new MemberDrmHandler();
            MemberDrmLogDTO dto = new MemberDrmLogDTO();
            dto.setBooksMemberId(member.getBmemberId());
            dto.setViewerBaseURI("http://123/");
            dto.setItemId(itemWithChilds.getId());
            dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
            dto.setReadDays("10");
            dto.setSessionToken("");
            dto.setTransactionId(UUID.randomUUID().toString());
            dto.setType("1");
            
            // add first
             //新增一般套書書籍授權,檢查書籍授權資料已新增
            String resp = handler.addDRM(em,dto);
            em.refresh(member);
            int serialSize = itemWithChilds.getChilds().size();
            List<Long> memberBookDrmMappingIds = new ArrayList<Long>();
            List<Long> memberBookIds = new ArrayList<Long>();
            Long memberDrmLogId = 0L;
            MemberDrmLog memberDrmLog = null;
            System.err.println("finally: " + resp);
            for (String itemId : itemWithChilds.getChilds()) {
                MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemId);
                assertNotNull("新增子書未新增",targetBook);
                Assert.assertTrue("新增套書,子書不存在對應DRM",targetBook.getMemberDrmLogs().size() > 0);
                Assert.assertTrue("新增套書,子書不存在對應關係",targetBook.getMemberBookDrmMappings().size() > 0);
                for (MemberBookDrmMapping mapping : targetBook.getMemberBookDrmMappings()) {
                    memberBookDrmMappingIds.add(mapping.getId());
                }
                if (memberDrmLog == null) {
                    memberDrmLog = targetBook.getMemberDrmLogs().get(0);
                    memberDrmLogId = memberDrmLog.getId();
                }
            }
            
            List<MemberBook> memberSerialBooks = memberDrmLog.getMemberBooks();
            for (MemberBook book : memberSerialBooks) {
                memberBookIds.add(book.getId());
            }
            Assert.assertTrue("新增套書後MemberBook 數量不一致",memberSerialBooks.size() == serialSize);
            Assert.assertTrue("新增套書後MemberBookDrmMappingIds數量不一致",memberBookDrmMappingIds.size() == serialSize );

            //異動套書內容
            memberBookDrmMappingIds.clear();
            
            List<String> childsList = itemWithChilds.getChilds();
            String removedItemChild = childsList.get(0); 
            String addedItemChild = itemNormal.getId();
            List<String> afterRescanChildsList = new ArrayList<String>();
            Long afterRescanMemberDrmLogId = 0L;
            MemberDrmLog afterScanMemberDrmLog = null;
            childsList.remove(0);
            childsList.add(addedItemChild);
            itemWithChilds.setChilds(childsList);
            em.getTransaction().begin();
            em.persist(itemWithChilds);
            em.getTransaction().commit();
            em.refresh(itemWithChilds);
            RescanDRMHandler rescanDRMHandler = new RescanDRMHandler();
            rescanDRMHandler.rescanMemberDRM(em, itemWithChilds.getId(),member.getId());
            em.refresh(member);
            memberBookIds.clear();
            //removedItemChildBook 的對應
            //ADD CHILD ITEM 的對應
            for (String itemId : itemWithChilds.getChilds()) {
                MemberBook targetBook = member.getMemberNormalBookByItemId(em, itemId);
                memberBookIds.add(targetBook.getId());
                assertNotNull("異動套書子書未新增",targetBook);
                Assert.assertTrue("異動新增套書,子書不存在對應DRM",targetBook.getMemberDrmLogs().size() > 0);
                Assert.assertTrue("異動新增套書,子書不存在對應關係",targetBook.getMemberBookDrmMappings().size() > 0);
                for (MemberBookDrmMapping mapping : targetBook.getMemberBookDrmMappings()) {
                    afterRescanChildsList.add(mapping.getMemberBook().getItem().getId());
                }
                if (afterScanMemberDrmLog == null) {
                    afterScanMemberDrmLog = targetBook.getMemberDrmLogs().get(0);
                    afterRescanMemberDrmLogId = memberDrmLog.getId();
                }
            }
            
            MemberBook markAsDeletedMemberBook = member.getMemberNormalBookByItemId(em, removedItemChild);
            Assert.assertTrue("異動套書後舊子書未刪除",markAsDeletedMemberBook.getDeleted());
            MemberBook newAddedMemberBook = member.getMemberNormalBookByItemId(em, addedItemChild);
            Assert.assertTrue("異動套書後新子書未新增",newAddedMemberBook!=null);
            Assert.assertTrue("異動套書子書內容不一致",childsList.containsAll(afterRescanChildsList));
            Assert.assertTrue("異動套書刪除子書 MemberBookDrmMapping 未刪除", markAsDeletedMemberBook.getMemberBookDrmMappings().size() == 0);
            Assert.assertTrue("異動套書DRM不一致",afterRescanMemberDrmLogId.equals(memberDrmLogId));
            
            //刪除一般套書書籍授權,檢查書籍授權資料已刪除
            handler.delDRM(em, dto);
            em.refresh(member);
            Assert.assertTrue("刪除後MemberDrmLog未刪除",em.createNamedQuery("MemberDrmLog.findById", MemberDrmLog.class).setParameter("id", memberDrmLogId).getResultList().size() == 0);
            
            for (Long mappingId : memberBookDrmMappingIds) {
                Assert.assertTrue("刪除後MemberBookDrmMapping未刪除",em.createNamedQuery("MemberBookDrmMapping.findById", MemberBookDrmMapping.class).setParameter("id", String.valueOf(mappingId)).getResultList().size() == 0);
            }
            
            for (Long bookId : memberBookIds) {
                Assert.assertTrue("刪除後MemberBook未刪除",member.getMemberBookByMBId(bookId).getDeleted());
            }
            
            
        }catch (ServerException e){
            e.printStackTrace();
            System.out.println("finally: " + e.getError_message());
            fail();
        } catch (Exception e) {
            
            e.printStackTrace();
            fail();
        }
        
    }

    public void testDrmMappingOperation() {

        try {
            long drmLogId = 713274;
            String itemId = "E050000948";
            Long memberId = 53126L;

            MemberBook memebrBook1 = MemberBook.findNormalByItemId(em, memberId, itemId);

            List<MemberDrmLog> drms = memebrBook1.getMemberDrmLogs();

            System.out.println(drms.get(0));

            for (MemberDrmLog memberDrmLog : drms) {
                System.out.println(memberDrmLog.getItem());
            }

            MemberDrmLog memberDrmLog = MemberDrmLog.getMemberDrmLogById(em, drmLogId);
            // List<MemberBookDrmMapping> MemberBookDrmMappings =
            // memberDrmLog.getMemberBookDrmMappings();
            List<MemberBook> MemberBooks = memberDrmLog.getMemberBooks();

            System.out.println(memberDrmLog);

            // System.out.println(MemberBookDrmMappings.get(0));

            System.out.println(MemberBooks.get(0));

            System.out.println("done");

        } catch (Exception e) {
            e.printStackTrace();
        }
        // List<MemberBookDrmMapping> mbdm =
        // em.createNamedQuery(arg0)
    }

    public void testGetLongestMemberDrmLog() {

        try {
            em.getTransaction().begin();
            long drmLogId = 713274;
            String itemId = "E050000939";
            Long memberId = 53126L;

            MemberBook memebrBook = MemberBook.findNormalByItemId(em, memberId, itemId);
            MemberDrmLog memberLog1 = DRMLogicUtility.getLongestMemberDrmLog(memebrBook.getMemberDrmLogs());
            MemberDrmLog memberLog2 = DRMLogicUtility.getLongestMemberDrmLogWithMapping(memebrBook.getMemberBookDrmMappings());

            org.junit.Assert.assertNotNull(memberLog1);
            org.junit.Assert.assertNotNull(memberLog2);

            List<MemberBook> list = memberLog1.getMemberBooks();
            System.out.println(list.get(0));

            org.junit.Assert.assertNotNull(list);

            em.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    public void testDrmOperation() {

        String itemId = "E050000968";

        try {

            String storedProcedureName = "booksapi.serial_book_operation";
            em.getTransaction().begin();
            StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcedureName);
            query.registerStoredProcedureParameter("in_item_id", String.class, ParameterMode.IN);
            query.setParameter("in_item_id", itemId);
            query.execute();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    public void testMemberDrmOperation() {

        String itemId = "E050000968";
        int memberId = 53126;

        try {
            String storedProcedureName = "serial_book_member_operation";
            em.getTransaction().begin();
            StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcedureName);
            query.registerStoredProcedureParameter("in_item_id", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("in_member_id", Integer.class, ParameterMode.IN);
            query.setParameter("in_item_id", itemId);
            query.setParameter("in_member_id", memberId);
            query.execute();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
	    
//    private MemberDrmLog insertDummyMemberDRMLog(String memberId, String itemId, String transactionId) throws Exception {
//        MemberDrmLog drmObject = new MemberDrmLog();
//        try {
//            em.getTransaction().begin();
//            Item item = em.find(Item.class, itemId);
//            drmObject.setId(80000L);
//            drmObject.setItem(item);
//            drmObject.setDownloadExpireTime("2999/10/10 12:00:00");
//            drmObject.setReadDays(Integer.valueOf(999));
//            drmObject.setReadExpireTime("2017/09/10 12:00:00");
//            drmObject.setType(3);
//            drmObject.setTransactionId(transactionId);
//            drmObject.setStatus(1);
//            drmObject.setLastUpdated(CommonUtil.zonedTime());
//            Member member = Member.getMemberByBMemberId(em, memberId, Member.MEMBERTYPE_NORMALREADER);
//            drmObject.setMember(member);
//            em.persist(drmObject);
//            em.getTransaction().commit();
//        } catch (Exception e) {
//            throw e;
//        }
//
//        return drmObject;
//    }
    
//  同樣的 TransactionId 不可重複新增書籍授權
//  @Test
//  public void testDelAddDRM() {
//      try {
//          MemberDrmHandler handler = new MemberDrmHandler();
//          MemberDrmLogDTO dto = new MemberDrmLogDTO();
//          dto.setBooksMemberId(member.getBmemberId());
//          dto.setViewerBaseURI("http://123/");
//          dto.setItemId(itemWithChilds.getId());
//          dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
//          dto.setReadDays("10");
//          dto.setSessionToken("");
//          dto.setTransactionId(UUID.randomUUID().toString());
//          if (itemWithChilds.getBookFiles().get(0).getIsTrial())
//              dto.setType("4");
//          else
//              dto.setType("1");
//          String resp = handler.addDRM(em,dto);
//          // delete 
//          resp = handler.delDRM(em,dto);
//          // tmp try re-add
//          resp = handler.addDRM(em,dto);
//          System.err.println("finally: " + resp);
//          boolean isTrial=dto.getType().equals("4");
//          
//          MemberBook targetBook= isTrial ? 
//                  member.getMemberTrialBookByItemId(em,itemTrial.getId()) 
//                  : member.getMemberNormalBookByItemId(em,itemTrial.getId());
//          
//          assertNotNull(targetBook);
//          
//      }catch (ServerException e)
//      {
//          System.out.println("finally: " + e.getError_message());
//      } catch (Exception e) {
//          
//          e.printStackTrace();
//          fail();
//      }
//  }
    
//    @Test
//    public void testDupAddDRM() {
//        String resp = "";
//        try {
//            MemberDrmHandler handler = new MemberDrmHandler();
//            MemberDrmLogDTO dto = new MemberDrmLogDTO();
//            dto.setViewerBaseURI("http://123/");
//            dto.setItemId(itemNormal.getId());
//            dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
//            dto.setReadDays("10");
//            dto.setSessionToken("");
//            dto.setTransactionId(UUID.randomUUID().toString());
//            dto.setBooksMemberId(member.getBmemberId());
//            
////          if (itemTrial.getBookFiles().get(0).getIsTrial()){
////              dto.setType("4");
////              resp = handler.addTrial(em, dto.getType(), dto.getBooksMemberId(), dto.getItemId(), dto.getViewerBaseURI());
////          }
////          else{
//                dto.setType("1");
//                resp = handler.addDRM(em,dto);              
////          }
//            
//            // tmp try re-add
//                try {
//                    resp = handler.addDRM(em,dto);
//                    System.err.println("finally: " + resp);
//                    fail();
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//                }
//            em.refresh(member);
//                
//            MemberBook targetBook=  member.getMemberNormalBookByItemId(em,itemNormal.getId());
//                    
//            assertNotNull("新增試閱書不存在",targetBook);
//            
//        }catch (ServerException e)
//        {
//            System.out.println("finally: " + e.getError_message());
//        } catch (Exception e) {
//            
//            e.printStackTrace();
//            fail();
//        }
//    }
    
//  套書的試閱只有一本 ,待確認
//  @Test
//  public void testAddTrialDRMWithChilds() {
//      try {
//          MemberDrmHandler handler = new MemberDrmHandler();
//          MemberDrmLogDTO dto = new MemberDrmLogDTO();
//          dto.setBooksMemberId(member.getBmemberId());
//          dto.setViewerBaseURI("http://123/");
//          dto.setItemId(itemWithChilds.getId());
//          dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
//          dto.setReadDays("10");
//          dto.setSessionToken("");
//          dto.setTransactionId(UUID.randomUUID().toString());
//          dto.setType("4");
//          String resp = handler.addDRM(em,dto);
//          
//          // tmp try re-add
////            resp = handler.addDRM(em);
//          System.err.println("finally2: " + resp);
//          if (!resp.contains("http"))
//              fail();
//          
//          MemberBook targetBook=member.getMemberTrialBookByItemId(em,itemWithChilds.getId());
//          assertNotNull(targetBook);
////            assertEquals(member.getMemberBooks().size(),5);
//          
//      }catch (ServerException e)
//      {
//          System.out.println("finally: " + e.getError_message());
//      } catch (Exception e) {
//          
//          e.printStackTrace();
//          fail();
//      }
//  }
    
//    @Test
//    public void testDelWithChildDRM() {
//        try {
//            String transactionId = UUID.randomUUID().toString();
//            MemberDrmHandler handler = new MemberDrmHandler();
//            MemberDrmLogDTO dto = new MemberDrmLogDTO();
//            dto.setBooksMemberId(member.getBmemberId());
//            dto.setViewerBaseURI("http://123/");
//            dto.setItemId(itemWithChilds.getId());
//            dto.setDownloadExpireTime("2999/10/10 12:00:00.00");
//            dto.setReadDays("10");
//            dto.setSessionToken("");
//            dto.setTransactionId(transactionId);
//            dto.setType("1");
//            handler.addDRM(em,dto);
//            
//            String resp2 = handler.delDRM(em,dto);
//            System.out.println("Del finally: " + resp2);
//            for (MemberBook m:member.getMemberBooks())
//            {
//                assertEquals("del",m.getAction());
//            }
//            
//        }catch (ServerException e)
//        {
//            System.out.println("finally: " + e.getError_message());
//            if (e.getError_code().endsWith("999"))
//            {
//                e.printStackTrace();
//                fail();
//            }
//        } catch (Exception e) {
//            
//            e.printStackTrace();
//            fail();
//        }
//    }
    
  

}
