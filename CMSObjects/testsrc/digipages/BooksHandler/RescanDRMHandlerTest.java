package digipages.BooksHandler;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.amazonaws.services.lambda.runtime.LambdaLogger;

import digipages.CommonJobHandler.PrimaryFlowTestHandler;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.ScheduleJobParam;
import digipages.common.StringHelper;
import digipages.dev.utility.RescanDRMForMember;
import digipages.dto.MemberDrmLogDTO;
import digipages.exceptions.ServerException;

import model.Item;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;
import model.ScheduleJob;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RescanDRMHandlerTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static Boolean testMode = false;

	private static Member member;
	private static Item item;
	private static UUID jobUUID;
	private static ArrayList<String> oriItems = new ArrayList<String>();
	private static ArrayList<String> newItems = new ArrayList<String>();

	static BlockingQueue<ScheduleJob> jobQueue = new ArrayBlockingQueue<>(100);
	static LocalRunner localRunner ;
	static Thread localRunnerThread;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();

		member = em.find(Member.class, 40653L); // ssvicky 10138L, twotomays 40653L
		item = em.find(Item.class, "004");

		ScheduleJobManager.setEntityManagerFactory(emf);
		localRunner =  new LocalRunner(jobQueue,emf, null); 
		localRunnerThread = new Thread(localRunner);
		localRunnerThread.start();

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (testMode) {
			em.getTransaction().begin();

			// remove child member_book
			List<Item> childItems = item.getChildItems();

			for (Item tmpChildItem : childItems) {
				MemberBook targetBook = member.getMemberNormalBookByItemId(em,tmpChildItem.getId());

				em.remove(targetBook);
			}

			// remove member_drm_log
//			List<MemberDrmLog> drmLogs = em.createNamedQuery("MemberDrmLog.findByMemberAndItemAndType", MemberDrmLog.class)
//					.setParameter("member", member)
//					.setParameter("item", item)
//					.setParameter("type", 1)
//					.getResultList();
//
//			for (MemberDrmLog tmpDrmLog : drmLogs) {
//				em.remove(tmpDrmLog);
//			}

			// remove schedule_job
			String jobQueryString = "SELECT * FROM schedule_job"
					+ " WHERE job_param ->> 'member_id' = '" + member.getId() + "'"
					+ " AND job_name = 'RescanDRM'"
					+ " AND status = 0"
					+ " AND retry_cnt = 0"
					+ " AND job_runner = 0";

			@SuppressWarnings("unchecked")
			List<ScheduleJob> scheduleJobs = em.createNativeQuery(jobQueryString, ScheduleJob.class).getResultList();

			for (ScheduleJob scheduleJob : scheduleJobs) {
				em.remove(scheduleJob);
			}

			em.getTransaction().commit();
		}

		em.close();
		emf.close();
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	public static void main(String args[]) {
		try {
			RescanDRMHandlerTest.setUpBeforeClass();

			RescanDRMHandlerTest test = new RescanDRMHandlerTest();

			test.setUp();
//			test.test001_CheckOriginalMemberBook();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Test
	public void test001_CheckOriginalMemberBook() {
		try {
			if (null != member && null != item) {
				// get original member books
				@SuppressWarnings("unchecked")
				List<Item> childItems = em.createNativeQuery("SELECT * FROM item WHERE id = ANY((SELECT childs FROM item WHERE id = '" + item.getId() + "')::varchar[])", Item.class).getResultList();

				if (childItems.size() > 0) {
					for (Item tmpChildItem : childItems) {
						MemberBook memberBook = member.getMemberNormalBookByItemId(em,tmpChildItem.getId());

						if (null != memberBook) {
//							System.out.println("oriItems add: " + memberBook.getItem().getId());

							oriItems.add(memberBook.getItem().getId());

							// check member book is exist
							assertNotNull(memberBook);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test002_CheckSerialBookChildItems() {
		try {
			if (null != member && null != item) {
				// add serial book drm
				MemberDrmHandler handler = new MemberDrmHandler();
				MemberDrmLogDTO dto = new MemberDrmLogDTO();
				dto.setBooksMemberId(member.getBmemberId());
				dto.setViewerBaseURI("http://123/");
				dto.setBooksMemberId(member.getBmemberId());
				dto.setItemId(item.getId());
				dto.setDownloadExpireTime("9999/11/30 23:59:59");
				dto.setReadDays(null);
				dto.setSessionToken("");
				dto.setTransactionId(UUID.randomUUID().toString());
				dto.setType("1");
				handler.addDRM(em,dto);

				List<String> childItems = item.getChilds();

				for (String tmpChildItem : childItems) {
//					System.out.println("newItems add: " + tmpChildItem);

					newItems.add(tmpChildItem);
				}

				// check child item number
				assertEquals(childItems.size(), 2);
			}
		} catch (ServerException e) {
			System.out.println("finally: " + e.getError_message());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test003_CheckMemberDrmLog() {
		try {
			if (null != member && null != item) {
				List<MemberDrmLog> memberDrmLog = em.createNamedQuery("MemberDrmLog.findByMemberAndItemAndType", MemberDrmLog.class)
						.setParameter("member", member)
						.setParameter("item", item)
						.setParameter("type", 1)
						.getResultList();

				// check member_drm_logs is exist
				assertNotNull(memberDrmLog);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

//	@Test
//	public void test004_CheckScheduleJob() {
//		try {
//			if (null != member && null != item) {
//				// add schedule job
//				RescanDRMForMember rdfm = new RescanDRMForMember(em);
//
//				rdfm.rescanDRM(item);
//
//				List<ScheduleJob> scheduleJobs = getScheduleJobsOrNull(member);
//
//				if (null != scheduleJobs) {
//					for (ScheduleJob tmpScheduleJob : scheduleJobs) {
//						jobUUID = tmpScheduleJob.getUuid();
//
//						// consumer work
//						RescanDRMHandler handler = new RescanDRMHandler();
//
//						dto.setScheduleJob((ScheduleJob) tmpScheduleJob);
//						dto.rescanDRM(em, member);
//
//						// check schedule job is exist
//						assertNotNull(tmpScheduleJob);
//					}
//				}
//			}
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			fail();
//		}
//	}

	@Test
	public void test005_CheckScheduleJobStatus() {
		try {
			if (null != jobUUID) {
				ScheduleJob scheduleJob = (ScheduleJob) em.createNativeQuery("SELECT * FROM schedule_job WHERE job_key = '" + jobUUID + "' AND job_name = 'RescanDRM'", ScheduleJob.class).getSingleResult();

				assertSame(scheduleJob.getStatus(), 9);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}

//	@Test
	public void test006_CheckMemberBookStatus() {
		try {
			if (null != member && null != item) {
				ArrayList<String> delMBList = new ArrayList<String>(oriItems);
				ArrayList<String> addMBList = new ArrayList<String>(newItems);
				ArrayList<String> mergedMBList = new ArrayList<String>(newItems);

				// System.out.println("newItems count: " + newItems.size());
				// System.out.println("oriItems count: " + oriItems.size());

				for (String s : newItems) {
					if (delMBList.contains(s)) {
						delMBList.remove(s);
					}
				}

				for (String s : oriItems) {
					if (addMBList.contains(s)) {
						addMBList.remove(s);
					}
				}

				System.out.println("original items: " + oriItems);
				System.out.println("new items: " + newItems);
				System.out.println("add member_book item: " + addMBList);
				System.out.println("remove member_book item: " + delMBList);
				System.out.println("final items: " + mergedMBList);

				System.out.println("jobUUID: " + jobUUID);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}

//	@Test
	public void test007_CheckMemberBookStatus() {
		try {
			if (null != jobUUID) {
				MemberDrmLog memberDrmLog = (MemberDrmLog) em.createNamedQuery("MemberDrmLog.findByMemberAndItemAndType", MemberDrmLog.class)
						.setParameter("member", member)
						.setParameter("item", item)
						.setParameter("type", 1)
						.setMaxResults(1)
						.getSingleResult();

				@SuppressWarnings("unchecked")
				List<Item> childItems = em.createNativeQuery("SELECT * FROM item WHERE id = ANY((SELECT childs FROM item WHERE id = '" + item.getId() + "')::varchar[])", Item.class).getResultList();

				for (Item tmpChildItem : childItems) {
					String memberBookQueryString = "SELECT * FROM member_book"
							+ " WHERE member_id = " + member.getId() + ""
							+ " AND item_id = '" + StringHelper.escapeSQL(tmpChildItem.getId()) + "'"
							+ " AND drm_log_ids && '{" + StringHelper.escapeSQL(memberDrmLog.getId().toString()) + "}'";

					MemberBook memberBook = (MemberBook) em.createNativeQuery(memberBookQueryString, MemberBook.class).getSingleResult();

					// check member book is exist
					assertNotNull(memberBook);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void test008_RunDirectRunner()
	{
		try {
			LambdaLogger logger = new LambdaLogger() {
				@Override
				public void log(String arg0) {
					System.out.println(arg0);
				}

			};
			RescanDRMHandler handler = new RescanDRMHandler();
			ScheduleJob job = new ScheduleJob();
			ScheduleJobParam jobParams = new ScheduleJobParam();
			
			jobParams.setMember_id(member.getId());
			job.setJobParam(jobParams);
			job.setShouldStartTime(new Date());
//			TODO temp disabled.		
//			dto.processEvent(RunMode.DirectRunMode, job);
		}catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void test009_RunLocalRunner()
	{
		try {
			LambdaLogger logger = new LambdaLogger() {
				@Override
				public void log(String arg0) {
					System.out.println(arg0);
				}

			};
			RescanDRMHandler handler = new RescanDRMHandler();
			ScheduleJob job = new ScheduleJob();
			ScheduleJobParam jobParams = new ScheduleJobParam();
			
			jobParams.setMember_id(member.getId());
			job.setJobParam(jobParams);
			job.setShouldStartTime(new Date());
//		TODO temp disabled.			
//			dto.processEvent(RunMode.LocalMode, job);
		}catch (Exception ex)
		{
			ex.printStackTrace();
			fail();
		}
	}

	@SuppressWarnings("unchecked")
	public List<ScheduleJob> getScheduleJobsOrNull(Member member) {
		List<ScheduleJob> scheduleJobs = null;

		try {
			String scheduleJobQueryString = "SELECT * FROM schedule_job"
					+ " WHERE job_param ->> 'member_id' = '" + member.getId() + "'"
					+ " AND job_name = 'RescanDRM'"
					+ " AND status = 0"
					+ " AND retry_cnt = 0"
					+ " AND job_runner = 0";

			// System.out.println(scheduleJobQueryString);

			scheduleJobs = em.createNativeQuery(scheduleJobQueryString, ScheduleJob.class).getResultList();
		}finally
		{
			// do nothing if anything happened.
		} 
		return scheduleJobs;
	}

	public MemberBook getBookByMemberOrNull(Member member, Item item, MemberDrmLog memberDrmLog) {
		MemberBook memberBook = null;

		try {
	    	String memberBookQueryString = "SELECT * FROM member_book"
					+ " WHERE member_id = " + member.getId() + ""
					+ " AND item_id = '" + item.getId() + "'"
					+ " AND drm_log_ids && '{" + memberDrmLog.getId() + "}'";

			System.out.println(memberBookQueryString);

			memberBook = (MemberBook) em.createNativeQuery(memberBookQueryString, MemberBook.class).getSingleResult();

			return memberBook;
		} catch (NoResultException e) {
			return null;
		}
	}

	public static String arrayConverter(List<String> arrayStr) {
		StringBuilder sb = new StringBuilder();

		for (String st : arrayStr) {
			sb.append(st).append(',');
		}

		if (arrayStr.size() != 0) {
			sb.deleteCharAt(sb.length() - 1);
		}

		return sb.toString();
	}
}