package digipages.BooksHandler;

import static org.junit.Assert.*;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.exceptions.ServerException;
import model.Member;
import model.MemberDevice;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ThirdPartyTestAccountHandlerTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}
	
	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testAddAccount() {
		ThirdPartyTestAccountHandler handler = new ThirdPartyTestAccountHandler();
		handler.setDeviceId("123123");
		handler.setOperator("test operator");
		handler.setPartyId("books");
		handler.setPartyType("PublisherReader");
		handler.setUserId("chlang");
		
		try {
			handler.addAccount(em);
			em.getTransaction().begin();
			Member m = Member.getMemberByBMemberId(em, "chlang", "PublisherReader");
			MemberDevice md = new MemberDevice();
			md.setMember(m);
			md.setDeviceModel("1111");
			md.setDeviceName("DeviceName");
			md.setDeviceType("Android");
			md.setDeviceVendor("HTC");
			md.setDeviceId("123123");
			md.setOsType("Android");
			md.setOsVersion("Android 6.0");
			md.setRegisterTime(new Date());
			md.setLastOnlineIp("127.0.0.1");
			m.addMemberDevice(md);
			em.getTransaction().commit();
		} catch (ServerException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testDeleteAccount() {
		ThirdPartyTestAccountHandler handler = new ThirdPartyTestAccountHandler();
		handler.setDeviceId("123123");
		handler.setOperator("test operator");
		handler.setPartyId("books");
		handler.setPartyType("PublisherReader");
		handler.setUserId("chlang");

		try { 
			handler.deleteAccount(em);
		} catch (ServerException e) {
			e.printStackTrace();
			fail();
		}

	}

}
