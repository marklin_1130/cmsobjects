package digipages.BooksHandler;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UpdateAuditItemHandlerTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}
	
	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testHandleMain() {
		UpdateAuditItemStatusData data = genData("E050004585");
		try {
			UpdateAuditItemHandler handler = new UpdateAuditItemHandler();
			String result=handler.handleMain(em, data);
			if (!result.contains("true"))
				fail();
			System.out.println("result=" + result);
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

	private UpdateAuditItemStatusData genData(String string) {
		UpdateAuditItemStatusData ret = new UpdateAuditItemStatusData();
		List <UpdateAuditItemRecords> records = new ArrayList<>();
		
		UpdateAuditItemRecords e=new UpdateAuditItemRecords();
		e.setItem(string);
		e.setItem_version("V001.0001");
		e.setAudit_status(9);
		e.setOperator("Tester");
		records.add(e);
		ret.setRecords(records);
		return ret;
	}

}
