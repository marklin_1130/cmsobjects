package digipages.BooksHandler;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.ScheduleJobParam;
import digipages.dto.MemberDrmLogDTO;
import digipages.exceptions.ServerException;

import model.Item;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;
import model.ScheduleJob;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RescanDRMTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private static Boolean testMode = false;
	private static Boolean testItemSwitch = false;

	private static Member member;
	private static Item item;
	private static String normalItemId;
	private static ArrayList<String> oriChildItems = new ArrayList<String>();
	private static ArrayList<String> newChildItems = new ArrayList<String>();

	static ScheduleJobManager jobHandler;
	static BlockingQueue<ScheduleJob> jobQueue = new ArrayBlockingQueue<>(100);
	static LocalRunner localRunner;
	static Thread localRunnerThread;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();

		member = em.find(Member.class, 17563L); // localhost:twotomays 17563L
		item = em.find(Item.class, "E050002806"); // serial book
		normalItemId = "E050001137"; // normal book

		if (testItemSwitch) {
			// a case
			oriChildItems.add("E050001236");
			oriChildItems.add("E050001237");
			oriChildItems.add("E050001238");

			newChildItems.add("E050001137");
			newChildItems.add("E050001238");
		} else {
			// b case
			oriChildItems.add("E050001137");
			oriChildItems.add("E050001238");

			newChildItems.add("E050001137");
			newChildItems.add("E050001236");
			newChildItems.add("E050001238");
		}

		// register handlers
//		RescanDRMTestHandler.getInstance();
//
//		jobHandler = new ScheduleJobManager(emf);
//		localRunner = new LocalRunner(jobQueue, emf);
//		localRunnerThread = new Thread(localRunner);
//		localRunnerThread.start();
//
//		System.out.println("LocalRunner Started...");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (testMode) {
			em.getTransaction().begin();

			// remove member_book
			@SuppressWarnings("unchecked")
			List<Item> childItems = em.createNativeQuery("SELECT * FROM item WHERE id = ANY((SELECT childs FROM item WHERE id = '" + item.getId() + "')::varchar[])", Item.class).getResultList();

			if (childItems.size() > 0) {
				for (Item tmpChildItem : childItems) {
					MemberBook memberBook = member.getMemberNormalBookByItemId(em, tmpChildItem.getId());

					if (null != memberBook) {
						em.remove(memberBook);
					}
				}
			}

			// remove member_drm_log
			List<MemberDrmLog> memberDrmLogs = em.createNamedQuery("MemberDrmLog.findByMemberAndItemAndType", MemberDrmLog.class)
					.setParameter("member", member)
					.setParameter("item", item)
					.setParameter("type", 1)
					.getResultList();

			for (MemberDrmLog tmpDrmLog : memberDrmLogs) {
				em.remove(tmpDrmLog);
			}

			// remove schedule_job
//			String jobQueryString = "SELECT * FROM schedule_job"
//					+ " WHERE job_param ->> 'member_id' = '" + member.getId() + "'"
//					+ " AND job_name = 'RescanDRM'"
//					+ " AND status = 0"
//					+ " AND retry_cnt = 0"
//					+ " AND job_runner = 0";
//
//			@SuppressWarnings("unchecked")
//			List<ScheduleJob> scheduleJobs = em.createNativeQuery(jobQueryString, ScheduleJob.class).getResultList();
//
//			for (ScheduleJob scheduleJob : scheduleJobs) {
//				em.remove(scheduleJob);
//			}

			em.getTransaction().commit();
		}

		em.close();
		emf.close();
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {

	}

	public static void main(String args[]) {
		try {
			RescanDRMTest.setUpBeforeClass();

			RescanDRMTest test = new RescanDRMTest();

			test.setUp();
			// test.test001_SerialMemberBookItem();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Test
	public void test001_SerialMemberBookItem() {
		try {
			if (null != member && null != item) {
				@SuppressWarnings("unchecked")
				List<Item> childItems = em.createNativeQuery("SELECT * FROM item WHERE id = ANY((SELECT childs FROM item WHERE id = '" + item.getId() + "')::varchar[])", Item.class).getResultList();

				if (childItems.size() > 0) {
					assertNotNull(childItems);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test002_SerialMemberBookDrm() {
		try {
			if (null != member && null != item) {
				@SuppressWarnings("unchecked")
				List<MemberDrmLog> drmLogs = em.createNativeQuery("SELECT * FROM member_drm_log WHERE member_id = " + member.getId() + " AND item_id = '" + item.getId() + "' AND status = 1", MemberDrmLog.class).getResultList();

				if (drmLogs.size() > 0) {
					for (MemberDrmLog tmpDrmLog : drmLogs) {
						assertNotNull(tmpDrmLog);
					}
				} else {
					// add drm
					MemberDrmHandler handler = new MemberDrmHandler();
					MemberDrmLogDTO dto = new MemberDrmLogDTO();
					dto.setBooksMemberId(member.getBmemberId());
					dto.setViewerBaseURI("http://123/");
					dto.setBooksMemberId(member.getBmemberId());
					dto.setItemId(item.getId());
					dto.setDownloadExpireTime("9999/11/30 23:59:59");
					dto.setReadDays(null);
					dto.setSessionToken("");
					dto.setTransactionId(UUID.randomUUID().toString());
					dto.setType("1");
					handler.addDRM(em,dto);

					@SuppressWarnings("unchecked")
					List<MemberDrmLog> newDrmLogs = em.createNativeQuery("SELECT * FROM member_drm_log WHERE member_id = " + member.getId() + " AND item_id = '" + item.getId() + "' AND status = 1", MemberDrmLog.class).getResultList();

					if (newDrmLogs.size() > 0) {
						for (MemberDrmLog tmpDrmLog : newDrmLogs) {
							assertNotNull(tmpDrmLog);
						}
					}
				}

				List<String> childItems = item.getChilds();

				if (childItems.size() > 0) {
					assertEquals(childItems.size(), oriChildItems.size());
				}
			}
		} catch (ServerException e) {
			System.out.println("finally: " + e.getError_message());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test003_SerialMemberBook() {
		try {
			if (null != member && null != item) {
				MemberDrmLog memberDrmLog = (MemberDrmLog) em.createNamedQuery("MemberDrmLog.findByMemberAndItemAndType", MemberDrmLog.class)
						.setParameter("member", member)
						.setParameter("item", item)
						.setParameter("type", 1)
						.setMaxResults(1)
						.getSingleResult();

				@SuppressWarnings("unchecked")
				List<MemberBook> memberBooks = em.createNativeQuery("SELECT * FROM member_book WHERE member_id = " + member.getId() + " AND readlist_idnames <> '{trial}' AND readlist_idnames IS NOT NULL AND item_id IN (SELECT id FROM item WHERE id = ANY((SELECT childs FROM item WHERE id = '" + item.getId() + "')::varchar[])) AND drm_log_ids && '{" + memberDrmLog.getId() + "}'", MemberBook.class).getResultList();

				if (memberBooks.size() > 0) {
					for (MemberBook tmpMemberBook : memberBooks) {
						assertNotNull(tmpMemberBook);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test004_NormalMemberBookItem() {
		try {
			if (null != member && null != normalItemId) {
				Item it = (Item) em.createNativeQuery("SELECT * FROM item WHERE id = '" + normalItemId + "'", Item.class).getSingleResult();

				if (null != it) {
					assertNotNull(it);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test005_NormalMemberBookDrm() {
		try {
			if (null != member && null != normalItemId) {
				@SuppressWarnings("unchecked")
				List<MemberDrmLog> drmLogs = em.createNativeQuery("SELECT * FROM member_drm_log WHERE member_id = " + member.getId() + " AND item_id = '" + normalItemId + "' AND status = 1", MemberDrmLog.class).getResultList();

				if (drmLogs.size() > 0) {
					for (MemberDrmLog tmpDrmLog : drmLogs) {
						assertNotNull(tmpDrmLog);
					}
				} else {
					// add drm
					MemberDrmHandler handler = new MemberDrmHandler();
					MemberDrmLogDTO dto = new MemberDrmLogDTO();
                    dto.setBooksMemberId(member.getBmemberId());
                    dto.setViewerBaseURI("http://123/");
                    dto.setBooksMemberId(member.getBmemberId());
                    dto.setItemId(item.getId());
                    dto.setDownloadExpireTime("9999/11/30 23:59:59");
                    dto.setReadDays(null);
                    dto.setSessionToken("");
                    dto.setTransactionId(UUID.randomUUID().toString());
                    dto.setType("1");
                    handler.addDRM(em,dto);

					@SuppressWarnings("unchecked")
					List<MemberDrmLog> newDrmLogs = em.createNativeQuery("SELECT * FROM member_drm_log WHERE member_id = " + member.getId() + " AND item_id = '" + normalItemId + "' AND status = 1", MemberDrmLog.class).getResultList();

					if (newDrmLogs.size() > 0) {
						for (MemberDrmLog tmpDrmLog : newDrmLogs) {
							assertNotNull(tmpDrmLog);
						}
					}
				}
			}
		} catch (ServerException e) {
			System.out.println("finally: " + e.getError_message());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test006_NormalMemberBook() {
		try {
			if (null != member && null != normalItemId) {
				MemberBook memberBook = member.getMemberNormalBookByItemId(em, normalItemId);

				if (null != memberBook) {
					assertNotNull(memberBook);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void test007_SerialMemberBookChildsUpdate() {
		try {
			if (null != member && null != item) {
				Item it = (Item) em.createNativeQuery("SELECT * FROM item WHERE id = '" + item.getId() + "' AND item_type = 'serial'", Item.class).getSingleResult();

				if (null != it) {
					it.setChilds(newChildItems);

					em.getTransaction().begin();
					em.persist(it);
					em.getTransaction().commit();
				}

				List<String> childItems = item.getChilds();

				if (childItems.size() > 0) {
					assertEquals(childItems.size(), newChildItems.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

//	@Test
//	public void test008_CreateScheduleJob() {
//		try {
//			if (null != member && null != item) {
//				ArrayList<String> rescanItems = new ArrayList<String>();
//
//				rescanItems.addAll(oriChildItems);
//
//				for (Object x : newChildItems) {
//					if (!rescanItems.contains(x)) {
//						rescanItems.add((String) x);
//					}
//				}
//
//				Collections.sort(rescanItems);
//
//				if (rescanItems.size() > 0) {
//					String memberQueryString = "SELECT * FROM member WHERE id IN (SELECT DISTINCT member_id FROM member_drm_log WHERE item_id = '" + item.getId() + "' AND status = 1)";
//
//					@SuppressWarnings("unchecked")
//					List<Member> members = em.createNativeQuery(memberQueryString, Member.class).getResultList();
//
//					if (members.size() > 0) {
//						for (Member tmpMember : members) {
//							RescanDRMHandler handler = new RescanDRMHandler();
//							handler.setItemId(item.getId());
//							handler.setItemIds(rescanItems);
//							handler.rescanDRM(em, tmpMember);
//
//							// schedule
////							ScheduleJobParam jobParams = new ScheduleJobParam();
////							jobParams.setMember_id(tmpMember.getId());
////							jobParams.setItem_id(item.getId());
////							jobParams.setItem_ids(rescanItems);
////
////							ScheduleJob mainLocaljob = new ScheduleJob();
////							mainLocaljob.setJobParam(jobParams);
////							mainLocaljob.setJobName(RescanDRMTestHandler.class.getName());
////							mainLocaljob.setJobGroup("DRM");
////							mainLocaljob.setJobRunner(RunMode.LocalMode.getValue());
////							ScheduleJobManager.addJob(mainLocaljob);
//						}
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
}