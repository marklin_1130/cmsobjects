package digipages.BooksHandler;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import digipages.BooksHandler.CommonData.BookPreviewRequestData;

public class BookPreviewHandlerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckFormat() {
		try{
			
			BookPreviewRequestData post=genRequestData("{\"preview_type\":\"1\",\"preview_content\":\"6\",\"book_format\":\"2\",\"status\":\"UF\",\"item\":\"E050020382\",\"call_type\":4,\"return_file_num\":1,\"file_url\":\"test-aws-ebook\\/ok\\/E05\\/002\\/03\",\"efile_nofixed_name\":\"E050020382.epub\",\"efile_fixed_pad_name\":\"\",\"efile_fixed_phone_name\":\"\"}");
			BookPreviewHandler.fixFormat(post);
			BookPreviewHandler.checkFormat(post);
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
	}

	private BookPreviewRequestData genRequestData(String string) {
		Gson gson = new Gson();
		return gson.fromJson(string, BookPreviewRequestData.class);
	}

}
