package digipages.BooksHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.ScheduleJobParam;
import digipages.exceptions.ServerException;

import model.Member;
import model.ScheduleJob;

public abstract class RescanDRMTestHandler implements CommonJobHandlerInterface {
	
	private EntityManagerFactory emf;

	public RescanDRMTestHandler() {
		LocalRunner.registerHandler(this);
	}

//	@Override
//	public String mainHandler(Properties config, ScheduleJob scheduleJob) {
//		try {
//			EntityManager em = emf.createEntityManager();
//
//			// datetime
//			String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
//			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//			LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
//			String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());
//
//			ScheduleJobParam jobParams = scheduleJob.getJobParam(ScheduleJobParam.class);
//			Member member = em.find(Member.class, jobParams.getMember_id());
//
//			String item_id = "";
//			ArrayList<String> item_ids = new ArrayList<String>();
//
//			if (null != jobParams.getItem_id()) {
//				item_id = jobParams.getItem_id();
//			}
//
//			if (null != jobParams.getItem_ids()) {
//				item_ids = jobParams.getItem_ids();
//			}
//
//			System.out.println("[LocalConsumer] item_id: " + item_id);
//			System.out.println("[LocalConsumer] item_ids: " + item_ids);
//			System.out.println("Rescan member_id: " + member.getId() + " drms, start at " + dateTimeString);
//
//			RescanDRMHandler handler = new RescanDRMHandler();
//			handler.setScheduleJob((ScheduleJob) scheduleJob);
//			handler.setItemId(item_id);
//			handler.setItemIds(item_ids);
//			handler.rescanDRM(em, member);
//
//			System.out.println("finish main job" + scheduleJob.getId());
//
//			ScheduleJobManager.finishJob(scheduleJob);
//		} catch (ServerException ex) {
//			if (scheduleJob != null) {
//				scheduleJob.setStatus(-1);
//				scheduleJob.setErrMessage(ex.toJsonStr());
//				ScheduleJobManager.updateJob(scheduleJob);
//			}
//		}
//		return "finish main job" + scheduleJob.getId();
//	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}



	
	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return null;
	}
	
	
	public static RunMode getInitRunMode() {
		return RunMode.LocalMode;
	}
}