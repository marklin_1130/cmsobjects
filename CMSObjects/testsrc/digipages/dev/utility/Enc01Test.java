package digipages.dev.utility;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Enc01Test {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	// remove unused code
//	@Test
//	public void testEncode() {
//		String key="!@#!@#432234312";
//		String downloadToken="hafsdohuq234r9pqwefrphafhp9oh98pvfr3 b34q08qwef98y7r13 y1h245v8 v34p9f213345v134rwtevw vere4vt3wq4v t43";
//		Enc01 enc = new Enc01(key,downloadToken);
//		try {
//			byte result[]=enc.encode("META-INF\\container.xml", "Hello World from Eric, for Test of enc01.".getBytes("UTF-8"));
//			System.out.println("encoded="+Hex.encodeHexString(result));
//		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
//			e.printStackTrace();
//			fail();
//		}
//		
//	}

	// removed unused code
//	@Test
//	public void testDecode() {
//		String key="!@#!@#432234312";
//		String downloadToken="hafsdohuq234r9pqwefrphafhp9oh98pvfr3 b34q08qwef98y7r13 y1h245v8 v34p9f213345v134rwtevw vere4vt3wq4v t43";
//		Enc01 enc = new Enc01(key,downloadToken);
//		try {
//			byte result[]=enc.encode("META-INF\\container.xml", "Hello World from Eric, for Test of enc01.".getBytes("UTF-8"));
//			
//			Enc01 dec = new Enc01(key,downloadToken);
//			byte restored[] = dec.decode("META-INF\\container.xml", result);
//			String restoreStr = new String(restored,"UTF-8");
//			if (!restoreStr.contains("for Test"))
//			{
//				fail();
//			}
//		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
	@Test
	public void testEncodeSpecialUnicode()
	{
		String filePath="resource/TestData/epub/Section0007.xhtml";
		String tmpFilePath="/tmp/tmpFile.xhtml";
		String targetPath="/tmp/target.xhtml";
		String key="!@#!@#432234312";
		String downloadToken="hafsdohuq234r9pqwefrphafhp9oh98pvfr3 b34q08qwef98y7r13 y1h245v8 v34p9f213345v134rwtevw vere4vt3wq4v t43";
		Enc01 enc = new Enc01(key,downloadToken);
		try {
			// encrypt
			InputStream fileInp = new FileInputStream(filePath);
			OutputStream fileOut = new FileOutputStream(tmpFilePath);
			enc.encrypt_copy(fileInp, fileOut, filePath, true);
			fileInp.close();
			fileOut.close();
			
			fileInp = new FileInputStream(tmpFilePath);
			fileOut = new FileOutputStream(targetPath);
			enc.encrypt_copy(fileInp, fileOut, filePath, true);
			fileInp.close();
			fileOut.close();
			
			// compare
			File file1 = new File(filePath);
			File file2 = new File(targetPath);
			boolean isTwoEqual = FileUtils.contentEqualsIgnoreEOL(file1, file2, "UTF-8");
			
			assertTrue(isTwoEqual);
		} catch (NoSuchAlgorithmException | IOException e) {
			e.printStackTrace();
			fail();
		}
	}


	

}
