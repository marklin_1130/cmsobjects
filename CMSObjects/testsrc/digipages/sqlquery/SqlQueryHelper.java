package digipages.sqlquery;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStreamReader;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SqlQueryHelper {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testSqlQueryByBookCountMember() {
		em.getTransaction().begin();
		try {

//			免費試讀店內碼	免費試讀書名	正式書店內碼	正式書書名	兩本書都有的id數	只有免費試讀店內碼id數	只有正式書店內碼id數
			try {
				InputStreamReader isr = new InputStreamReader(new FileInputStream("C://20201130_免費試讀店內碼_正式書店內碼.csv"));// 檔案讀取路徑
				BufferedReader reader = new BufferedReader(isr);
				BufferedWriter bw = new BufferedWriter(new FileWriter("C://20201130_免費試讀店內碼_正式書店內碼_result.csv"));// 檔案輸出路徑
				String line = null;
				reader.readLine();
				while ((line = reader.readLine()) != null) {
					String item[] = line.split(",");
					/** 讀取 **/
					String trialItemId = item[0].trim();
//			      String  data2= item[1].trim();
					String itemId = item[2].trim();
//			      String  data3= item[3].trim();
					String trailCount = String.valueOf(em.createNativeQuery("select count(*) from member_book mb where item_id ='" + trialItemId + "'").getSingleResult());

					String itemCount = String.valueOf( em.createNativeQuery("select count(*) from member_book mb where item_id ='" + itemId + "'").getSingleResult());

					String bothCount = String.valueOf( em.createNativeQuery("select count(*) from member_book mb where item_id ='" + itemId+ "'  and member_id  in (select member_id from member_book mb where item_id ='"+ trialItemId + "') ")
							.getSingleResult());

//					item[4] = String.valueOf(bothCount);
//					item[5] = String.valueOf(trailCount);
//					item[6] = String.valueOf(itemCount);
					String bookTitle = "";
					try {
					
					if(StringUtils.isBlank(item[3])) {
						bookTitle = item[3];
					}
					
					} catch (Exception e) {
						// TODO: handle exception
					}
					bw.newLine();
					bw.write(item[0] + "," + item[1] + "," + item[2] + "," + bookTitle + "," + bothCount + "," + trailCount
							+ "," + itemCount);
					bw.flush();
				}
				bw.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		em.getTransaction().commit();

	}
}
