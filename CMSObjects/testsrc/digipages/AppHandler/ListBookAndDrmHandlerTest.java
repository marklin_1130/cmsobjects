package digipages.AppHandler;

import static org.junit.Assert.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Member;

public class ListBookAndDrmHandlerTest {

	private static EntityManagerFactory emf;
	private static EntityManager em;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}
	// only in UAT, for specific record
//	@Test
	public void testGetReaderListBook() {
		try {
			ListBookAndDrmHandler handler = new ListBookAndDrmHandler();
			
			String readlistFilter = "";
			String isReadFilter = "";
			String isRead="";
			// default all = no read filter
			if ("y".equals(isRead)) {
				isReadFilter = " AND mb.last_read_time IS NOT NULL";
				readlistFilter = " AND mb.passworded = FALSE";
			} else if ("n".equals(isRead)) {
				isReadFilter = " AND mb.last_read_time IS NULL";
				readlistFilter = " AND mb.passworded = FALSE";
			}

			readlistFilter += " AND mb.is_hidden = FALSE";
			Member member = Member.getMemberByMemberId(em, 63521 );
			handler.setMember(member);
			handler.setOffSet(0);
			handler.setPageSize(30);
			handler.setSortOrder("default");
			handler.setSimpleMode("y");
			handler.setIsRead("false");
			handler.setLastUpdatedTime("2017-08-10");
			handler.setCurrentLastUpdatedTime("2017-08-10");
			handler.setClientUDT("2017-08-10");
			handler.setReadlistFilter(readlistFilter);
			handler.setIsReadFilter(isReadFilter);
			handler.setS3PublicBucket("tmp");
			// when set to true, list status >=8
			handler.setThirdParty(false);
	
			Gson gson = new GsonBuilder().create();
			String out=gson.toJson(handler.getReaderListBook(em));
			System.out.println("result:" + out);
		} catch (Exception e)
		{
			e.printStackTrace();
			fail();
		}
		
	}

}
