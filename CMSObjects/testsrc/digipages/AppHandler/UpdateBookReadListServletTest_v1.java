package digipages.AppHandler;

import static org.junit.Assert.fail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Member;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.AppHandler.UpdateBookReadListHandler.UpdateBookReadListData;

public class UpdateBookReadListServletTest_v1 {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testMemberBook() {
		try {
			Member m = Member.getRandomMember(em);
			System.out.print("member Id : "+m.getId()) ;
			//{all,book}		
			String s =" {\n" + 
					"	\"records\" : [\n" + 
					"		{\n" + 
					"			\"book_uni_id\" : [\"90437\"],\n" + 
					"			\"to_readlist\" : \"password\",\n" + 
					"			\"action\" : \"remove\",\n" + 
					"			\"updated_time\" : \"2015-07-06T17:12:56+08:00\"\n" + 
					"		},\n" + 
					"		{\n" + 
					"			\"book_uni_id\" : [\"90437\"],\n" + 
					"			\"to_readlist\" : \"magazine\",\n" + 
					"			\"action\" : \"add\",\n" + 
					"			\"updated_time\" : \"2015-07-06T17:50:56+08:00\"\n" + 
					"		}\n" + 
					"	],\n" + 
					"	\"CmsToken\" : \"03fddfc4fff684e8b57167a204ee09e9\"\n" + 
					"}" ;
			Gson gson = new GsonBuilder().create();
			UpdateBookReadListData updateBookReadList = gson.fromJson(s, UpdateBookReadListData.class) ;
			
//			UpdateBookReadListServlet.processUpdate(em, m, updateBookReadList);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}

}
