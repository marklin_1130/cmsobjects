package digipages.AppHandler;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import digipages.dev.utility.SampleDataGenerator;
import model.Member;
import model.Publisher;
import model.Vendor;

public class ListBookTest {
	private static EntityManagerFactory emf;
	private static EntityManager em;
	private Vendor vendor;
	private Publisher publisher;
	private Member normalReader;
	private Member superReader;
	private Member publisherReader;
	private Member vendorReader;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
		SampleDataGenerator gen = new SampleDataGenerator(em);
		em.getTransaction().begin();
		
		normalReader=gen.generateBenchMember(false);
		
		superReader = gen.generateSuperReader();

		vendor = gen.generateVendor();
//		vendorReader = gen.generateVendorReader(vendor);

		publisher = gen.generatePublisher("testPublisher");
//		publisherReader = gen.generatePublisherReader(publisher);
		
		em.getTransaction().commit();
	}

	@After
	public void tearDown() throws Exception {
		em.getTransaction().begin();
		normalReader=em.merge(normalReader);
		em.remove(publisherReader);
		em.remove(publisher);
		em.remove(vendorReader);
		em.remove(vendor);
		em.remove(superReader);
		em.remove(normalReader);
		em.getTransaction().commit();
		em.close();
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
