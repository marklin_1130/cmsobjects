package digipages.AppHandler;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ UpdateBookReadListServletTest_v1.class, UpdateBookReadListServletTest_v2.class })
public class AllTests {

}
