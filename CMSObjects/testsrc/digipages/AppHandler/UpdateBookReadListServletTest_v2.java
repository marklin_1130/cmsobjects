package digipages.AppHandler;

import static org.junit.Assert.fail;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.AppHandler.UpdateBookReadListHandler.UpdateBookReadListData;
import digipages.AppHandler.UpdateBookReadListHandler.records;
import model.Member;
import model.MemberBook;

public class UpdateBookReadListServletTest_v2 {

	private static EntityManagerFactory emf;
	private static EntityManager em;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		emf = Persistence.createEntityManagerFactory("CMSObjects");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		emf.close();
	}

	@Before
	public void setUp() throws Exception {
		em = emf.createEntityManager();
	}

	@After
	public void tearDown() throws Exception {
		em.close();
	}

	@Test
	public void testMemberBook() {
		try {
			Member m = Member.getRandomMember(em);
			System.out.print("member Id : "+m.getId()) ;
			//{all,book}		
			String s =" {\n" + 
					"	\"records\" : [\n" + 
					"		{\n" + 
					"			\"book_uni_id\" : [\"89327\"],\n" + 
					"			\"to_readlist\" : \"book\",\n" + 
					"			\"action\" : \"remove\",\n" + 
					"			\"updated_time\" : \"2015-07-06T17:12:56+08:00\"\n" + 
					"		}\n" + 
					"	],\n" + 
					"	\"CmsToken\" : \"03fddfc4fff684e8b57167a204ee09e9\"\n" + 
					"}" ;
			Gson gson = new GsonBuilder().create();
			UpdateBookReadListData updateBookReadList = gson.fromJson(s, UpdateBookReadListData.class) ;
			
			List<MemberBook> mbooks = m.getMemberBooks() ;
			//em.getTransaction().begin();
			
			for(records record: updateBookReadList.getRecords()){
				
				String target=record.getTo_readlist();
				em.getTransaction().begin();
				
				for( MemberBook memberBook : mbooks ){
					
					System.out.println("book : "+memberBook.getBookFile().getId()) ;
					
					if( record.getAction().equals("add") )
					{
						memberBook.addToReadList(target);
						
					}else if( record.getAction().equals("remove") )
					{
						memberBook.removeFromReadList(target);
					}
					
				}
				em.getTransaction().commit();
				
			}
			
			

			
		} catch (Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}

}
