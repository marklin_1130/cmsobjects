package digipages.info;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import digipages.common.CommonUtil;

import model.MagInfo;
import model.Member;
import model.MemberBook;

public class FreeMagInfo extends MagInfo {
	private List<String> readlist_idnames;
	private Integer percent;
	private String start_read_time = null;
	private String last_read_time = null;
	private String download_time = null;
	private String auth_time = null;
	private String start_time;
	private String end_time;
	private String updated_time;
	private String item_uri;
	private String last_loc;
	private String ask_update_version = "N";
	private String bg_update_version = "N";
	private String version_locked = "N";
	private String book_highlight_status;
	private String book_bookmark_status;
	private String cur_version;
	private String action;
	private long size = 0;
	private String free_type ="";
	private String free_start_time;
	private String free_end_time;
	private String free_read_expire_time;
	private String purchase_area_limit;
	private String purchase_area_carea;
	private Integer free_read_days;
	private String receive_status;
	
	public String getReceive_status() {
		return receive_status;
	}

	public void setReceive_status(String receive_status) {
		this.receive_status = receive_status;
	}

	public List<String> getReadlist_idnames() {
		return readlist_idnames;
	}

	public void setReadlist_idnames(List<String> readlist_idnames) {
		this.readlist_idnames = readlist_idnames;
	}

	public Integer getPercent() {
		return percent;
	}

	public void setPercent(Integer percent) {
		this.percent = percent;
	}

	public String getStart_read_time() {
		return start_read_time;
	}

	public void setStart_read_time(String start_read_time) {
		this.start_read_time = start_read_time;
	}

	public String getLast_read_time() {
		return last_read_time;
	}

	public void setLast_read_time(String last_read_time) {
		this.last_read_time = last_read_time;
	}

	public String getDownload_time() {
		return download_time;
	}

	public void setDownload_time(String download_time) {
		this.download_time = download_time;
	}

	public String getAuth_time() {
		return auth_time;
	}

	public void setAuth_time(String auth_time) {
		this.auth_time = auth_time;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public String getItem_uri() {
		return item_uri;
	}

	public void setItem_uri(String item_uri) {
		this.item_uri = item_uri;
	}

	public String getLast_loc() {
		return last_loc;
	}

	public void setLast_loc(String last_loc) {
		this.last_loc = last_loc;
	}

	public String getAsk_update_version() {
		return ask_update_version;
	}

	public void setAsk_update_version(String ask_update_version) {
		this.ask_update_version = ask_update_version;
	}

	public String getBg_update_version() {
		return bg_update_version;
	}

	public void setBg_update_version(String bg_update_version) {
		this.bg_update_version = bg_update_version;
	}

	public String getVersion_locked() {
		return version_locked;
	}

	public void setVersion_locked(String version_locked) {
		this.version_locked = version_locked;
	}

	public String getBook_highlight_status() {
		return book_highlight_status;
	}

	public void setBook_highlight_status(String book_highlight_status) {
		this.book_highlight_status = book_highlight_status;
	}

	public String getBook_bookmark_status() {
		return book_bookmark_status;
	}

	public void setBook_bookmark_status(String book_bookmark_status) {
		this.book_bookmark_status = book_bookmark_status;
	}

	public String getCur_version() {
		return cur_version;
	}

	public void setCur_version(String cur_version) {
		this.cur_version = cur_version;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public void setListBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
		if (!simpleMode.equals("n")) {
			// BaseBookInfo
			// this.setItem(null);
			this.setO_title(null);
			this.setPublisher_id(null);
			this.setCategory(null);
			// this.setRank(null);
			this.setToc(null);
			this.setEfile_nofixed_name(null);
			this.setEfile_fixed_pad_name(null);
			this.setEfile_fixed_phone_name(null);
			this.setEfile_url(null);
			// this.setShare_flag(null);
			this.setStatus(null);
			this.setReturn_file_num(0);
			this.setBookGroup(0);
			this.setPage_direction(null);
			this.setLanguage(null);
			this.setBuffet_flag(null);
			this.setBuffet_type(null);

			// MagInfo
			// this.setIssn(null);
			this.setMag_id(null);
			this.setIssue(null);
			this.setIssue_year(null);
			this.setPublish_type(null);
			this.setCover_man(null);
			this.setCover_story(null);
		}

		Boolean readlist_status = true;

		if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
			readlist_status = false;
		}

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

	public void setReadListInfo(Member member, MemberBook tmpMemberBook, String bookType, Date startReadTime, Date lastReadTime) {
		Boolean readlist_status = true;

		if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
			readlist_status = false;
		}

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

	public void setSearchBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
		if (!simpleMode.equals("n")) {
			// BaseBookInfo
			// this.setItem(null);
			this.setO_title(null);
			this.setPublisher_id(null);
			this.setCategory(null);
			// this.setRank(null);
			this.setToc(null);
			this.setEfile_nofixed_name(null);
			this.setEfile_fixed_pad_name(null);
			this.setEfile_fixed_phone_name(null);
			this.setEfile_url(null);
			// this.setShare_flag(null);
			this.setStatus(null);
			this.setReturn_file_num(0);
			this.setBookGroup(0);
			this.setPage_direction(null);
			this.setLanguage(null);
			this.setBuffet_flag(null);
			this.setBuffet_type(null);

			// MagInfo
			// this.setIssn(null);
			this.setMag_id(null);
			this.setIssue(null);
			this.setIssue_year(null);
			this.setPublish_type(null);
			this.setCover_man(null);
			this.setCover_story(null);
		}

		// Boolean readlist_status = true;
		//
		// if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
		// readlist_status = false;
		// }

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		// this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setReadlist_idnames(tmpMemberBook.getReadlistIdnames(tmpMemberBook));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

	public void setSearchTrialBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
		if (!simpleMode.equals("n")) {
			// BaseBookInfo
			// this.setItem(null);
			this.setO_title(null);
			this.setPublisher_id(null);
			this.setCategory(null);
			// this.setRank(null);
			this.setToc(null);
			this.setEfile_nofixed_name(null);
			this.setEfile_fixed_pad_name(null);
			this.setEfile_fixed_phone_name(null);
			this.setEfile_url(null);
			// this.setShare_flag(null);
			this.setStatus(null);
			this.setReturn_file_num(0);
			this.setBookGroup(0);
			this.setPage_direction(null);
			this.setLanguage(null);
			this.setBuffet_flag(null);
			this.setBuffet_type(null);

			// MagInfo
			// this.setIssn(null);
			this.setMag_id(null);
			this.setIssue(null);
			this.setIssue_year(null);
			this.setPublish_type(null);
			this.setCover_man(null);
			this.setCover_story(null);
		}

		// Boolean readlist_status = true;
		//
		// if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
		// readlist_status = false;
		// }

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		// this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setReadlist_idnames(tmpMemberBook.getReadlistIdnames(tmpMemberBook));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction("update");
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

	public String getFree_type() {
		return free_type;
	}

	public void setFree_type(String free_type) {
		this.free_type = free_type;
	}

	public String getFree_start_time() {
		return free_start_time;
	}

	public void setFree_start_time(String free_start_time) {
		this.free_start_time = free_start_time;
	}

	public String getFree_end_time() {
		return free_end_time;
	}

	public void setFree_end_time(String free_end_time) {
		this.free_end_time = free_end_time;
	}

	public String getFree_read_expire_time() {
		return free_read_expire_time;
	}

	public void setFree_read_expire_time(String free_read_expire_time) {
		this.free_read_expire_time = free_read_expire_time;
	}

	public String getPurchase_area_limit() {
		return purchase_area_limit;
	}

	public void setPurchase_area_limit(String purchase_area_limit) {
		this.purchase_area_limit = purchase_area_limit;
	}

	public String getPurchase_area_carea() {
		return purchase_area_carea;
	}

	public void setPurchase_area_carea(String purchase_area_carea) {
		this.purchase_area_carea = purchase_area_carea;
	}

	public Integer getFree_read_days() {
		return free_read_days;
	}

	public void setFree_read_days(Integer free_read_days) {
		this.free_read_days = free_read_days;
	}
}