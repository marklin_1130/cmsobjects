package digipages.info;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang3.StringEscapeUtils;

import digipages.common.CommonUtil;
import model.Member;
import model.MemberBook;

public class FreeMediaBookInfo extends ExtMediaBookInfo {
	private String free_type ="";
	private String free_start_time;
	private String free_end_time;
	private String free_read_expire_time;
	private String purchase_area_limit;
	private String purchase_area_carea;
	private Integer free_read_days;
	private String receive_status;

	
	
	public void setListBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
		if (!simpleMode.equals("n")) {
			// BaseBookInfo
			// this.setItem(null);
			this.setO_title(null);
			this.setPublisher_id(null);
			this.setCategory(null);
			// this.setRank(null);
			this.setToc(null);
			this.setEfile_nofixed_name(null);
			this.setEfile_fixed_pad_name(null);
			this.setEfile_fixed_phone_name(null);
			this.setEfile_url(null);
			// this.setShare_flag(null);
			this.setStatus(null);
			this.setReturn_file_num(0);
			this.setBookGroup(0);
			this.setPage_direction(null);
			this.setLanguage(null);
			this.setBuffet_flag(null);
			this.setBuffet_type(null);

		}

		Boolean readlist_status = true;

		if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
			readlist_status = false;
		}

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

	public void setReadListInfo(Member member, MemberBook tmpMemberBook, String bookType, Date startReadTime, Date lastReadTime) {
		Boolean readlist_status = true;

		if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
			readlist_status = false;
		}

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

	public void setSearchBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
		if (!simpleMode.equals("n")) {
			// BaseBookInfo
			// this.setItem(null);
			this.setO_title(null);
			this.setPublisher_id(null);
			this.setCategory(null);
			// this.setRank(null);
			this.setToc(null);
			this.setEfile_nofixed_name(null);
			this.setEfile_fixed_pad_name(null);
			this.setEfile_fixed_phone_name(null);
			this.setEfile_url(null);
			// this.setShare_flag(null);
			this.setStatus(null);
			this.setReturn_file_num(0);
			this.setBookGroup(0);
			this.setPage_direction(null);
			this.setLanguage(null);
			this.setBuffet_flag(null);
			this.setBuffet_type(null);

		}

		// Boolean readlist_status = true;
		//
		// if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
		// readlist_status = false;
		// }

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		// this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setReadlist_idnames(tmpMemberBook.getReadlistIdnames(tmpMemberBook));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

	public void setSearchTrialBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
		if (!simpleMode.equals("n")) {
			// BaseBookInfo
			// this.setItem(null);
			this.setO_title(null);
			this.setPublisher_id(null);
			this.setCategory(null);
			// this.setRank(null);
			this.setToc(null);
			this.setEfile_nofixed_name(null);
			this.setEfile_fixed_pad_name(null);
			this.setEfile_fixed_phone_name(null);
			this.setEfile_url(null);
			// this.setShare_flag(null);
			this.setStatus(null);
			this.setReturn_file_num(0);
			this.setBookGroup(0);
			this.setPage_direction(null);
			this.setLanguage(null);
			this.setBuffet_flag(null);
			this.setBuffet_type(null);

		}

		// Boolean readlist_status = true;
		//
		// if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
		// readlist_status = false;
		// }

		// this.setType(tmpMemberBook.getBookType().toLowerCase());
		this.setType(bookType);
		this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
		this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
		// this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
		this.setReadlist_idnames(tmpMemberBook.getReadlistIdnames(tmpMemberBook));
		this.setPercent(tmpMemberBook.getPercentage());
		this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
		this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
		this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
		// this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

		if ("NormalReader".equals(member.getMemberType())) {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
		} else {
			this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
		}

		// this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated()) : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
		this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
		this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
		this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
		this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
		this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
		this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
		this.setAction("update");
		this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
	}

    public String getFree_type() {
        return free_type;
    }

    public String getFree_start_time() {
        return free_start_time;
    }

    public String getFree_end_time() {
        return free_end_time;
    }

    public String getFree_read_expire_time() {
        return free_read_expire_time;
    }

    public String getPurchase_area_limit() {
        return purchase_area_limit;
    }

    public String getPurchase_area_carea() {
        return purchase_area_carea;
    }

    public Integer getFree_read_days() {
        return free_read_days;
    }

    public String getReceive_status() {
        return receive_status;
    }

    public void setFree_type(String free_type) {
        this.free_type = free_type;
    }

    public void setFree_start_time(String free_start_time) {
        this.free_start_time = free_start_time;
    }

    public void setFree_end_time(String free_end_time) {
        this.free_end_time = free_end_time;
    }

    public void setFree_read_expire_time(String free_read_expire_time) {
        this.free_read_expire_time = free_read_expire_time;
    }

    public void setPurchase_area_limit(String purchase_area_limit) {
        this.purchase_area_limit = purchase_area_limit;
    }

    public void setPurchase_area_carea(String purchase_area_carea) {
        this.purchase_area_carea = purchase_area_carea;
    }

    public void setFree_read_days(Integer free_read_days) {
        this.free_read_days = free_read_days;
    }

    public void setReceive_status(String receive_status) {
        this.receive_status = receive_status;
    }
}