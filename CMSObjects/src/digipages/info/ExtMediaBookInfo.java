package digipages.info;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.Expose;

import digipages.common.CommonUtil;
import digipages.dto.MediabookAttachDTO;
import digipages.dto.MediabookChapterDTO;
import model.MediaBookInfo;
import model.MediabookAttach;
import model.MediabookChapter;
import model.MediabookSubtitle;
import model.Member;
import model.MemberBook;

public class ExtMediaBookInfo extends MediaBookInfo {
    @Expose
    private List<String> readlist_idnames;
    @Expose
    private Integer percent;
    @Expose
    private String start_read_time = null;
    @Expose
    private String last_read_time = null;
    @Expose
    private String download_time = null;
    @Expose
    private String auth_time = "";
    @Expose
    private String start_time;
    @Expose
    private String end_time;
    @Expose
    private String updated_time;
    @Expose
    private String item_uri;
    @Expose
    private String last_loc;
    @Expose
    private String ask_update_version = "N";
    @Expose
    private String bg_update_version = "N";
    @Expose
    private String version_locked = "N";
    @Expose
    private String book_highlight_status = "N";
    @Expose
    private String book_bookmark_status = "N";
    @Expose
    private String cur_version;
    @Expose
    private String action;
    @Expose
    private long size = 0;
    @Expose
    private String finish_time = null;
    @Expose
    private String finish_flag = "N";
    @Expose
    private String speacker;
    @Expose
    private List<MediabookChapterDTO> chapters;

    private List<MediabookAttachDTO> attachs;

    public List<String> getReadlist_idnames() {
        return readlist_idnames;
    }

    public void setReadlist_idnames(List<String> readlist_idnames) {
        this.readlist_idnames = readlist_idnames;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public String getStart_read_time() {
        return start_read_time;
    }

    public void setStart_read_time(String start_read_time) {
        this.start_read_time = start_read_time;
    }

    public String getLast_read_time() {
        return last_read_time;
    }

    public void setLast_read_time(String last_read_time) {
        this.last_read_time = last_read_time;
    }

    public String getDownload_time() {
        return download_time;
    }

    public void setDownload_time(String download_time) {
        this.download_time = download_time;
    }

    public String getAuth_time() {
        return auth_time;
    }

    public void setAuth_time(String auth_time) {
        this.auth_time = auth_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public String getItem_uri() {
        return item_uri;
    }

    public void setItem_uri(String item_uri) {
        this.item_uri = item_uri;
    }

    public String getLast_loc() {
        return last_loc;
    }

    public void setLast_loc(String last_loc) {
        this.last_loc = last_loc;
    }

    public String getAsk_update_version() {
        return ask_update_version;
    }

    public void setAsk_update_version(String ask_update_version) {
        this.ask_update_version = ask_update_version;
    }

    public String getBg_update_version() {
        return bg_update_version;
    }

    public void setBg_update_version(String bg_update_version) {
        this.bg_update_version = bg_update_version;
    }

    public String getVersion_locked() {
        return version_locked;
    }

    public void setVersion_locked(String version_locked) {
        this.version_locked = version_locked;
    }

    public String getBook_highlight_status() {
        return book_highlight_status;
    }

    public void setBook_highlight_status(String book_highlight_status) {
        this.book_highlight_status = book_highlight_status;
    }

    public String getBook_bookmark_status() {
        return book_bookmark_status;
    }

    public void setBook_bookmark_status(String book_bookmark_status) {
        this.book_bookmark_status = book_bookmark_status;
    }

    public String getCur_version() {
        return cur_version;
    }

    public void setCur_version(String cur_version) {
        this.cur_version = cur_version;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setListBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime, Date finishTime) {
        if (!simpleMode.equals("n")) {
            // BaseBookInfo
            // this.setItem(null);
            this.setO_title(null);
            this.setPublisher_id(null);
            this.setCategory(null);
            // this.setRank(null);
            this.setToc(null);
            this.setEfile_nofixed_name(null);
            this.setEfile_fixed_pad_name(null);
            this.setEfile_fixed_phone_name(null);
            this.setEfile_url(null);
            // this.setShare_flag(null);
            this.setStatus(null);
            this.setReturn_file_num(0);
            this.setBookGroup(0L);
            this.setPage_direction(null);
            this.setLanguage(null);
            this.setBuffet_flag(null);
            this.setBuffet_type(null);

            // BookInfo
            // this.setIsbn(null);
            this.setO_author(null);
            this.setTranslator(null);
            this.setEditor(null);
            this.setIllustrator(null);
            this.setEdition(null);
            this.setForeword(null);
            this.setIntro(null);
        }

        this.setToc(null);
        this.setForeword(null);
        this.setIntro(null);

        Boolean readlist_status = true;

        if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
            readlist_status = false;
        }

        // this.setType(tmpMemberBook.getBookType().toLowerCase());
        this.setType(bookType);
        this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
        this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
        this.setAuthor(StringEscapeUtils.unescapeHtml4(StringUtils.trimToEmpty(tmpMemberBook.getBookFile().getItem().getAuthor())).replaceAll("\\<.*?>", ""));
        this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
        this.setPercent(tmpMemberBook.getPercentage());
        this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
        this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
        this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
        // this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) :
        // CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

        if ("NormalReader".equals(member.getMemberType())) {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
        } else {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
        }

        // this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated())
                : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
        this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
        this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
        this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
        this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
        this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
        this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
        this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
        this.setFinish_time(null == tmpMemberBook.getFinishTime() ? CommonUtil.fromDateToString(finishTime) : CommonUtil.fromDateToString(tmpMemberBook.getFinishTime()));
        this.setFinish_flag(tmpMemberBook.getFinishFlag());
    }

    public void setReadListInfo(Member member, MemberBook tmpMemberBook, String bookType, Date startReadTime, Date lastReadTime, Date finishTime) {
        Boolean readlist_status = true;

        if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
            readlist_status = false;
        }

        // this.setType(tmpMemberBook.getBookType().toLowerCase());
        this.setType(bookType);
        this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
        this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
        this.setAuthor(StringEscapeUtils.unescapeHtml4(StringUtils.trimToEmpty(tmpMemberBook.getBookFile().getItem().getAuthor())).replaceAll("\\<.*?>", ""));
        this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
        this.setPercent(tmpMemberBook.getPercentage());
        this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
        this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
        this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
        // this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) :
        // CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

        if ("NormalReader".equals(member.getMemberType())) {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
        } else {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
        }

        // this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated())
                : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
        this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
        this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
        this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
        this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
        this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
        this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
        this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
        this.setFinish_time(null == tmpMemberBook.getFinishTime() ? CommonUtil.fromDateToString(finishTime) : CommonUtil.fromDateToString(tmpMemberBook.getFinishTime()));
        this.setFinish_flag(tmpMemberBook.getFinishFlag());
    }

    public void setSearchBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
        if (!simpleMode.equals("n")) {
            // BaseBookInfo
            // this.setItem(null);
            this.setO_title(null);
            this.setPublisher_id(null);
            this.setCategory(null);
            // this.setRank(null);
            this.setToc(null);
            this.setEfile_nofixed_name(null);
            this.setEfile_fixed_pad_name(null);
            this.setEfile_fixed_phone_name(null);
            this.setEfile_url(null);
            // this.setShare_flag(null);
            this.setStatus(null);
            this.setReturn_file_num(0);
            this.setBookGroup(0L);
            this.setPage_direction(null);
            this.setLanguage(null);
            this.setBuffet_flag(null);
            this.setBuffet_type(null);

            // BookInfo
            // this.setIsbn(null);
            this.setO_author(null);
            this.setTranslator(null);
            this.setEditor(null);
            this.setIllustrator(null);
            this.setEdition(null);
            this.setForeword(null);
            this.setIntro(null);
        }

        this.setToc(null);
        this.setForeword(null);
        this.setIntro(null);
        // Boolean readlist_status = true;
        //
        // if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
        // readlist_status = false;
        // }

        // this.setType(tmpMemberBook.getBookType().toLowerCase());
        this.setType(bookType);
        this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
        this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
        this.setAuthor(StringEscapeUtils.unescapeHtml4(StringUtils.trimToEmpty(tmpMemberBook.getBookFile().getItem().getAuthor())).replaceAll("\\<.*?>", ""));
        // this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
        this.setReadlist_idnames(tmpMemberBook.getReadlistIdnames(tmpMemberBook));
        this.setPercent(tmpMemberBook.getPercentage());
        this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
        this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
        this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
        // this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) :
        // CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

        if ("NormalReader".equals(member.getMemberType())) {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
        } else {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
        }

        // this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated())
                : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
        this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
        this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
        this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
        this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
        this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
        this.setAction((tmpMemberBook.getAction().equals("del") ? "del" : "update"));
        this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
    }

    public void setSearchTrialBookInfo(Member member, MemberBook tmpMemberBook, String simpleMode, String bookType, Date startReadTime, Date lastReadTime) {
        if (!simpleMode.equals("n")) {
            // BaseBookInfo
            // this.setItem(null);
            this.setO_title(null);
            this.setPublisher_id(null);
            this.setCategory(null);
            // this.setRank(null);
            this.setToc(null);
            this.setEfile_nofixed_name(null);
            this.setEfile_fixed_pad_name(null);
            this.setEfile_fixed_phone_name(null);
            this.setEfile_url(null);
            // this.setShare_flag(null);
            this.setStatus(null);
            this.setReturn_file_num(0);
            this.setBookGroup(0L);
            this.setPage_direction(null);
            this.setLanguage(null);
            this.setBuffet_flag(null);
            this.setBuffet_type(null);

            // BookInfo
            // this.setIsbn(null);
            this.setO_author(null);
            this.setTranslator(null);
            this.setEditor(null);
            this.setIllustrator(null);
            this.setEdition(null);
            this.setForeword(null);
            this.setIntro(null);
        }

        this.setToc(null);
        this.setForeword(null);
        this.setIntro(null);
        // Boolean readlist_status = true;
        //
        // if (tmpMemberBook.getAction().equals("del") || (null != tmpMemberBook.getIsHidden() && tmpMemberBook.getIsHidden() == true)) {
        // readlist_status = false;
        // }

        // this.setType(tmpMemberBook.getBookType().toLowerCase());
        this.setType(bookType);
        this.setC_title(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getcTitle()).replaceAll("\\<.*?>", ""));
        this.setPublisher_name(StringEscapeUtils.unescapeHtml4(tmpMemberBook.getBookFile().getItem().getPublisherName()).replaceAll("\\<.*?>", ""));
        this.setAuthor(StringEscapeUtils.unescapeHtml4(StringUtils.trimToEmpty(tmpMemberBook.getBookFile().getItem().getAuthor())).replaceAll("\\<.*?>", ""));
        // this.setReadlist_idnames((readlist_status == false ? new ArrayList<String>() : tmpMemberBook.getReadlistIdnames(tmpMemberBook)));
        this.setReadlist_idnames(tmpMemberBook.getReadlistIdnames(tmpMemberBook));
        this.setPercent(tmpMemberBook.getPercentage());
        this.setStart_read_time(null == tmpMemberBook.getStartReadTime() ? CommonUtil.fromDateToString(startReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getStartReadTime()));
        this.setLast_loc(null == tmpMemberBook.getLastLoc() ? "" : tmpMemberBook.getLastLoc());
        this.setLast_read_time(null == tmpMemberBook.getLastReadTime() ? CommonUtil.fromDateToString(lastReadTime) : CommonUtil.fromDateToString(tmpMemberBook.getLastReadTime()));
        // this.setDownload_time(null == tmpMemberBook.getDownloadTime() ? CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()) :
        // CommonUtil.fromDateToString(tmpMemberBook.getDownloadTime()));

        if ("NormalReader".equals(member.getMemberType())) {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getCreateTime()));
        } else {
            this.setAuth_time(CommonUtil.fromDateToString(tmpMemberBook.getBookFile().getCreateTime()));
        }

        // this.setUpdated_time(CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setUpdated_time(tmpMemberBook.getLastUpdated().before(tmpMemberBook.getItem().getLastUpdated()) ? CommonUtil.fromDateToString(tmpMemberBook.getItem().getLastUpdated())
                : CommonUtil.fromDateToString(tmpMemberBook.getLastUpdated()));
        this.setAsk_update_version((true == tmpMemberBook.getAskUpdateVersion() ? "Y" : "N"));
        this.setBg_update_version((true == tmpMemberBook.getBgUpdateVersion() ? "Y" : "N"));
        this.setVersion_locked((true == tmpMemberBook.getVersionLocked() ? "Y" : "N"));
        this.setBook_highlight_status((true == tmpMemberBook.getBookHighlightStatus() ? "Y" : "N"));
        this.setBook_bookmark_status((true == tmpMemberBook.getBookBookmarkStatus() ? "Y" : "N"));
        this.setCur_version((null == tmpMemberBook.getCurVersion() ? tmpMemberBook.getBookFile().getVersion() : tmpMemberBook.getCurVersion()));
        this.setAction("update");
        this.setRank(null == tmpMemberBook.getItem().getRank() ? "1" : tmpMemberBook.getItem().getRank());
        if (tmpMemberBook.getFinishTime() != null) {
            this.setFinish_time(CommonUtil.fromDateToString(tmpMemberBook.getFinishTime()));
        }
        this.setFinish_flag(tmpMemberBook.getFinishFlag());
    }

    public void setChapter(List<MediabookChapter> chapterList, boolean isTrial) {

        this.chapters = new ArrayList<MediabookChapterDTO>();
        for (MediabookChapter mediabookChapter : chapterList) {
            if(isTrial && "N".equalsIgnoreCase(mediabookChapter.isIspreview())) {
                continue;
            }
                MediabookChapterDTO aMediabookChapterDTO = new MediabookChapterDTO();
                this.chapters.add(aMediabookChapterDTO);
                try {
                    for (MediabookSubtitle subtitle : mediabookChapter.getSubtitles()) {
                        subtitle.setMediabookChapter(null);
                        subtitle.setChapterId(null);
                        subtitle.setCreate_time(null);
                        subtitle.setDeleted(null);
                        subtitle.setId(null);
                        subtitle.setOperator(null);
                        subtitle.setSrcFname(null);
                    }
                    if (isTrial) {
                        mediabookChapter.setChapterInfo(null);
                    } else {
                        mediabookChapter.setPreviewInfo(null);
                    }
                    BeanUtils.copyProperties(aMediabookChapterDTO, mediabookChapter);
    
                    aMediabookChapterDTO.setScriptSize(0L);
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                }
     
        }
    }

    public void setAttachsDto(List<MediabookAttach> attachList) {

        this.attachs = new ArrayList<MediabookAttachDTO>();
        for (MediabookAttach attach : attachList) {
            MediabookAttachDTO aMediabookAttachDTO = new MediabookAttachDTO();
            this.attachs.add(aMediabookAttachDTO);
            try {
//                     mediabookChapter.setPreviewInfo(null);
                BeanUtils.copyProperties(aMediabookAttachDTO, attach);

            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
            } catch (InvocationTargetException e) {
                // TODO Auto-generated catch block
            }
        }
    }

    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

    public String getFinish_flag() {
        return finish_flag;
    }

    public void setFinish_flag(String finish_flag) {
        this.finish_flag = finish_flag;
    }

    public String getSpeacker() {
        return speacker;
    }

    public void setSpeacker(String speacker) {
        this.speacker = speacker;
    }

    public List<MediabookChapterDTO> getChapters() {
        return chapters;
    }

    public void setChapters(List<MediabookChapterDTO> chapters) {
        this.chapters = chapters;
    }

    public List<MediabookAttachDTO> getAttachs() {
        return attachs;
    }

    public void setAttachs(List<MediabookAttachDTO> attachs) {
        this.attachs = attachs;
    }

}