package digipages.common;

public class SimpleTimer {
	private long startTime=0;
	public SimpleTimer()
	{
		startTime = System.currentTimeMillis();
	}
	public long timeSpend()
	{
		return System.currentTimeMillis()-startTime;
	}
}
