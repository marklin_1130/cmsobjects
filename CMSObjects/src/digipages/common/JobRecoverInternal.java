package digipages.common;

import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import digipages.CommonJobHandler.JobRecoverHandler;
import model.ScheduleJob;

public class JobRecoverInternal implements Job {

	private static DPLogger logger =DPLogger.getLogger(JobRecoverInternal.class.getName());
	
	// every 30 sec check
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			// job recover

			
			JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
			
			
			@SuppressWarnings("unchecked")
			BlockingQueue<ScheduleJob> bq=(BlockingQueue<ScheduleJob>) dataMap.get("JobQueue");
			EntityManager postgres = (EntityManager) dataMap.get("postgres");
			
			JobRecoverHandler handler = new JobRecoverHandler(bq);
			
			handler.jobRecover(postgres);


			logger.info("V1.7 JobRecover Scan at " + CommonUtil.currentDateTime());
		} catch (Exception e) {
			logger.error(e);;
		}
	}


}