package digipages.common;

import javax.persistence.EntityManager;

import model.CmsToken;
import model.Member;
import model.MemberDevice;

public class MemberSession {
	private Member member;
	private MemberDevice memberDevice;
	private EntityManager connection;
	private CmsToken cmsToken;
	private String clientIpAddr;
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public MemberDevice getMemberDevice() {
		return memberDevice;
	}
	public void setMemberDevice(MemberDevice memberDevice) {
		this.memberDevice = memberDevice;
	}
	public EntityManager getConnection() {
		return connection;
	}
	public void setConnection(EntityManager connection) {
		this.connection = connection;
	}
	public CmsToken getCmsToken() {
		return cmsToken;
	}
	public void setCmsToken(CmsToken cmsToken) {
		this.cmsToken = cmsToken;
	}
	
	public void setClientIpAddr(String ipAddr)
	{
		clientIpAddr = ipAddr;
	}
	public String getClientIPAddr() {
		return clientIpAddr;
	}
	
}
