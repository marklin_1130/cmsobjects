package digipages.common;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.tomcat.util.codec.binary.Base64;

public class SessionInfo {
	
	private static DPLogger logger =DPLogger.getLogger(SessionInfo.class.getName());
	
	private String deviceID;
	private String memberID;
	private OffsetDateTime expireTime;
	private byte[] salt=RandomUtils.nextBytes(16); // salt = everytime new
	
	public SessionInfo (int timeOutSeconds)
	{
		expireTime=OffsetDateTime.now().plusSeconds(timeOutSeconds); //default 72 hrs
	}
	
	public SessionInfo ()
	{
		this(192000);
	}
	
	public static void main(String args[])
	{
		int loopCnt=100000;
		try {
			logger.info("SessionInfo Encrypt/Decrypt Benchmark");
			long startTime=System.currentTimeMillis();
			List <SessionInfo> results = new ArrayList<SessionInfo>();
			for (int i=0;i<loopCnt; i++){
				SessionInfo si = new SessionInfo(10000000);
				si.setMemberID("Member ID Test");
				si.setDeviceID("DeviceID");
				String encrypted=si.encryptToString();
				SessionInfo si2=SessionInfo.decryptFromString(encrypted);
				results.add(si2);
			}
			logger.info("TotalTime=" + (System.currentTimeMillis()-startTime)/(double)loopCnt + "ms/operation");
			logger.info("ResultCnt=" + results.size());
			
		} catch (IOException e) {
			logger.error(e);;
		} catch (Exception e) {
			logger.error(e);;
		}
	}
	

	public String encryptToString() throws Exception {
		byte [] x = encrypt();
		return Base64.encodeBase64String(x);
	}
	
	public static SessionInfo decryptFromString(String encrypted) throws Exception {
		byte [] ary = Base64.decodeBase64(encrypted);
		return decrypt(ary);
	}
	
	public boolean isValid()
	{
		if (expireTime.isAfter(OffsetDateTime.now()))
			return true;
		return false;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(deviceID).append(",");
		sb.append(memberID).append(",");
		sb.append(expireTime.toString());
//		sb.append(salt);
		return sb.toString();
	}
	
	private void setExpireTime(String string) {
		expireTime=OffsetDateTime.parse(string);
	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public OffsetDateTime getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(OffsetDateTime expireTime) {
		this.expireTime = expireTime;
	}
	// format: "deviceID,memberID,expireTime"
	private static SessionInfo fromString(String decrypted)
	{
		String [] ax = decrypted.split(",");
		SessionInfo info = new SessionInfo();
		info.setDeviceID(ax[0]);
		info.setMemberID(ax[1]);
		info.setExpireTime(ax[2]);
		return info;
	}
	
	//encrypt format of SessionInfo
	private byte[] encrypt() throws Exception
	{
		String shortStr = this.toString();
		byte [] compressed = Compressor.compressString(shortStr);
		byte [] encrypted = Protector.encrypt(compressed, salt);
		return ArrayUtils.addAll(salt, encrypted);
	}
	
	private static SessionInfo decrypt(byte []value) throws Exception
	{
		byte [] salt = ArrayUtils.subarray(value, 0, 16);
		byte [] realValue = ArrayUtils.subarray(value, 16, value.length);
		byte [] decrypted=Protector.decrypt(realValue, salt);
		
		String uncompressed=Compressor.unCompressString(decrypted);
		return fromString(uncompressed);
	}
	

}
