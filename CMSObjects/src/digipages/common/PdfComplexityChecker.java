package digipages.common;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import digipages.BookConvert.BookMain.PdfBook;

public class PdfComplexityChecker {
	
	private static DPLogger logger = DPLogger.getLogger(PdfComplexityChecker.class.getName());

	public PdfComplexityChecker() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		int pdfPageObjCntLimit=200;
    	long startTime = System.currentTimeMillis();
    	File pdfFile = new File(args[0]);
    	try {
	    	PDDocument sourceDocument = PDDocument.load(pdfFile);
			int totalPage= sourceDocument.getNumberOfPages();
			StringBuilder sb=new StringBuilder();
			for (int i=0;i< totalPage; i++)
			{
				PDPage tempPage = sourceDocument.getPage(i);

				int objCnt = PdfBook.countPageElements(tempPage);
					sb.append(pdfFile.getAbsolutePath())
					.append(" page:").append(i+1)
					.append(" count=").append(objCnt)
					.append("\r\n");
			}
			logger.info(sb.toString());
			sourceDocument.close();
    	} catch (IOException ioex)
    	{
    		logger.error(ioex);
    	}

	}

}
