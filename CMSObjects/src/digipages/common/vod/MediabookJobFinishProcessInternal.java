package digipages.common.vod;

import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import digipages.CommonJobHandler.JobMediabookFinishHandler;
import digipages.common.DPLogger;
import model.ScheduleJob;

public class MediabookJobFinishProcessInternal implements Job {

	private static DPLogger logger =DPLogger.getLogger(MediabookJobFinishProcessInternal.class.getName());
	
	// every 30 sec check
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			// job recover

			
			JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
			
			
			@SuppressWarnings("unchecked")
			BlockingQueue<ScheduleJob> bq=(BlockingQueue<ScheduleJob>) dataMap.get("JobQueue");
			EntityManager postgres = (EntityManager) dataMap.get("postgres");
			
			JobMediabookFinishHandler handler = new JobMediabookFinishHandler(bq);
			
			//本機測試中
//			handler.finishMediabookJob(postgres);


//			logger.info("JobRecover Scan at " + CommonUtil.currentDateTime());
		} catch (Exception e) {
			logger.error(e);;
		}
	}


}