package digipages.common;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.tomcat.util.codec.binary.Base64;


public class Protector {

    private static final String ALGORITHM = "AES";
    private static final int ITERATIONS = 2;
    private static final byte[] keyValue = Base64.decodeBase64("m08VISpYKltBF2a7x0mTYfQI");
    private Protector()
    {
    	
    }
    public static String encrypt(String value, String salt) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);  
        c.init(Cipher.ENCRYPT_MODE, key);
  
        String valueToEnc = null;
        String eValue = value;
        for (int i = 0; i < ITERATIONS; i++) {
            valueToEnc = salt + eValue;
            byte[] encValue = c.doFinal(valueToEnc.getBytes("UTF-8"));
            eValue = Base64.encodeBase64String(encValue);
        }
        return eValue;
    }
    
    /**
     * 
     * @param value
     * @param salt
     * @return
     * @throws Exception
     */
	public static byte[] encrypt(byte[] value, byte[] salt) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);  
        c.init(Cipher.ENCRYPT_MODE, key);

        byte [] encValue=value;
        byte [] valueToEnc=value;
        for (int i = 0; i < ITERATIONS; i++) {
            valueToEnc = ArrayUtils.addAll(salt, encValue);
            encValue = c.doFinal(valueToEnc);
        }
        return encValue;
	}

    public static String decrypt(String value, String salt) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);
  
        String dValue = null;
        String valueToDecrypt = value;
        for (int i = 0; i < ITERATIONS; i++) {
            byte[] decordedValue = Base64.decodeBase64(valueToDecrypt);
            byte[] decValue = c.doFinal(decordedValue);
            dValue = new String(decValue,"UTF-8").substring(salt.length());
            valueToDecrypt = dValue;
        }
        return dValue;
    }
    
    public static byte[] decrypt(byte[] value, byte[] salt) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);
  
        byte [] dValue = null;
        byte [] valueToDecrypt = value;
        for (int i = 0; i < ITERATIONS; i++) {
            byte[] decValue = c.doFinal(valueToDecrypt);
            dValue = ArrayUtils.subarray(decValue, salt.length, decValue.length);
            valueToDecrypt = dValue;
        }
        return dValue;
    }
    

    private static Key generateKey() throws Exception {
        Key key = new SecretKeySpec(keyValue, ALGORITHM);
        // SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
        // key = keyFactory.generateSecret(new DESKeySpec(keyValue));
        return key;
    }


}