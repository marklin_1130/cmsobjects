package digipages.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import digipages.exceptions.ServerException;

public class RequestsCheck {

	private HttpServletRequest request;
	public HashMap<String, Object> reqPara;

	public RequestsCheck(HttpServletRequest request) throws IOException, ServerException {
		this.request = request;
		String decodeString = getDecodedMessage(request);
		reqPara = toHashMap(decodeString);
	}

	public String getParameter(String key) throws ServerException {
		try {
			return (String) reqPara.get(key);
		} catch (Exception ex) {
			return "";
		}
	}

	public String getDecodedMessage(HttpServletRequest request) throws IOException {
		String message = null;
		message = getDecodeContent(request.getInputStream(), request.getMethod());
		return message;
	}

	public String getDecodedMessage(ServletRequest servletRequest) throws IOException {
		String message = null;
		message = getDecodeContent(servletRequest.getInputStream(), request.getMethod());
		return message;
	}

	public String getDecodeContent(InputStream in, String method) throws UnsupportedEncodingException, IOException {
		String message = null;
		String line;

		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));

		while ((line = reader.readLine()) != null) {
			if (!method.toUpperCase().equals("POST")) {
				buffer.append(URLDecoder.decode(line, "UTF-8"));
			} else {
				buffer.append(line);
			}
		}

		message = buffer.toString();
		return message;
	}

	public HashMap<String, Object> toHashMap(String paraStr) throws ServerException {
		String[] paraValue = null;
		HashMap<String, Object> hm = new HashMap<String, Object>();
		String key = "";

		if (null != paraStr && !"".equals(paraStr)) {
			String[] paras = paraStr.split("&");

			for (String tmpPara : paras) {
				paraValue = tmpPara.split("=");

				if (paraValue[0].equals("session_token")) {
					int ind = tmpPara.indexOf('=') + 1;
					hm.put(paraValue[0], tmpPara.substring(ind));
				} else if (paraValue.length > 1) {
					hm.put(paraValue[0], paraValue[1]);
				} else if (paraValue.length == 1 && paraValue[0] != "") {
					key += paraValue[0] + ",";
				}
			}
		}

//		if (key != "") {
//			throw new ServerException("id_err_000", "missed Parameter= " + key);
//		}

		return hm;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HashMap<String, Object> getReqPara() {
		return reqPara;
	}

	public void setReqPara(HashMap<String, Object> reqPara) {
		this.reqPara = reqPara;
	}
}