package digipages.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DPLogger {

	Logger localLogger = LogManager.getLogger(this.getClass().getSimpleName());

	String name;

	public DPLogger(String name) {
		this.name = name;
		localLogger = LogManager.getLogger(name);
	}

	public static DPLogger getLogger(String name) {
		return new DPLogger(name);
	}

	public void error(String message, Throwable e) {
		localLogger.error(message, e);

	}

	public void error(Throwable e) {
		localLogger.error(e);

	}

	public void error(String message) {
		localLogger.error(message);
	}

	public void debug(String message) {
		localLogger.debug(message);
	}

	public void info(String string) {
		localLogger.info(string);
	}

	public void warn(Object message) {
		localLogger.warn(message);
	}

	public void warn(Object message, Throwable t) {
		localLogger.warn(message, t);
	}

	public void debug(Object message, Throwable t) {
		localLogger.debug(message, t);
	}

	public void cmslogerror(String methodName, String msg) {
		localLogger.info("method:" + methodName + ", logtype:error, message:" + msg);
	}

	public void cmsloginfo(String methodName, String msg) {
		localLogger.info("method:" + methodName + ", logtype:info, message:" + msg);
	}

	public void cmslogInput(String methodName, String msg) {
		localLogger.info("method:" + methodName + ", logtype:input, message:" + msg);
	}

	public void cmslogOutput(String methodName, String msg) {
		localLogger.info("method:" + methodName + ", logtype:output, message:" + msg);
	}

	public void cmslogWithMemberId(String methodName, String msg) {
		localLogger.info("method:" + methodName + ", message:" + msg);
	}

	public void cmslogWithItemId(String methodName, String msg) {
		localLogger.info("method:" + methodName + ", message:" + msg);
	}

	public void info(String className, String methodName, String msg) {

		localLogger.info(className + ", method:" + methodName + ": " + msg);
	}

	public void infoInput(String className, String methodName, String msg) {

		localLogger.error(className + ", method:" + methodName + ": Input" + msg);
	}

	public void infoOutput(String className, String methodName, String msg) {

		localLogger.error(className + ", method:" + methodName + ": Output" + msg);
	}

	public void error(String className, String methodName, String msg, Throwable t) {

		localLogger.error(className + ", method:" + methodName + ": " + msg, t);
	}

}
