package digipages.common;

public class MessageQueueParam {
    private Long message_id;
    private String owner_id = "";
    private String gcm_server_api_key = "";
    private String gcm_application_name = "";
    private String apns_certificate = "";
    private String apns_private_key = "";
    private String apns_application_name = "";
    private String date_string = "";

    private String snpsaccount = "";
    private String snpspw = "";
    private String snpsDeviceUpdate = "";
    private String snpsBoardcast = "";
    private String snpsBoardcastPayload = "";
    private String snpsPublish = "";
    private String snpsSimple = "";
    private String snpsAppKeyios = "";
    private String snpsAppKeyandroid = "";
    private String snpsAppKey = "";

    public Long getMessage_id() {
        return message_id;
    }

    public void setMessage_id(Long message_id) {
        this.message_id = message_id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getGcm_server_api_key() {
        return gcm_server_api_key;
    }

    public void setGcm_server_api_key(String gcm_server_api_key) {
        this.gcm_server_api_key = gcm_server_api_key;
    }

    public String getGcm_application_name() {
        return gcm_application_name;
    }

    public void setGcm_application_name(String gcm_application_name) {
        this.gcm_application_name = gcm_application_name;
    }

    public String getApns_certificate() {
        return apns_certificate;
    }

    public void setApns_certificate(String apns_certificate) {
        this.apns_certificate = apns_certificate;
    }

    public String getApns_private_key() {
        return apns_private_key;
    }

    public void setApns_private_key(String apns_private_key) {
        this.apns_private_key = apns_private_key;
    }

    public String getApns_application_name() {
        return apns_application_name;
    }

    public void setApns_application_name(String apns_application_name) {
        this.apns_application_name = apns_application_name;
    }

    public String getDate_string() {
        return date_string;
    }

    public void setDate_string(String date_string) {
        this.date_string = date_string;
    }

    public String getSnpsaccount() {
        return snpsaccount;
    }

    public void setSnpsaccount(String snpsaccount) {
        this.snpsaccount = snpsaccount;
    }

    public String getSnpspw() {
        return snpspw;
    }

    public void setSnpspw(String snpspw) {
        this.snpspw = snpspw;
    }

    public String getSnpsDeviceUpdate() {
        return snpsDeviceUpdate;
    }

    public void setSnpsDeviceUpdate(String snpsDeviceUpdate) {
        this.snpsDeviceUpdate = snpsDeviceUpdate;
    }

    public String getSnpsBoardcast() {
        return snpsBoardcast;
    }

    public void setSnpsBoardcast(String snpsBoardcast) {
        this.snpsBoardcast = snpsBoardcast;
    }

    public String getSnpsPublish() {
        return snpsPublish;
    }

    public void setSnpsPublish(String snpsPublish) {
        this.snpsPublish = snpsPublish;
    }

    public String getSnpsAppKeyios() {
        return snpsAppKeyios;
    }

    public void setSnpsAppKeyios(String snpsAppKeyios) {
        this.snpsAppKeyios = snpsAppKeyios;
    }

    public String getSnpsAppKeyandroid() {
        return snpsAppKeyandroid;
    }

    public void setSnpsAppKeyandroid(String snpsAppKeyandroid) {
        this.snpsAppKeyandroid = snpsAppKeyandroid;
    }

    public String getSnpsAppKey() {
        return snpsAppKey;
    }

    public void setSnpsAppKey(String snpsAppKey) {
        this.snpsAppKey = snpsAppKey;
    }

    public String getSnpsSimple() {
        return snpsSimple;
    }

    public void setSnpsSimple(String snpsSimple) {
        this.snpsSimple = snpsSimple;
    }

	public String getSnpsBoardcastPayload() {
		return snpsBoardcastPayload;
	}

	public void setSnpsBoardcastPayload(String snpsBoardcastPayload) {
		this.snpsBoardcastPayload = snpsBoardcastPayload;
	}
}