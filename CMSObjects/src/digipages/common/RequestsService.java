package digipages.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import digipages.exceptions.ServerException;

import model.Member;

public class RequestsService {

	private HttpServletRequest request;
	public HashMap<String, Object> reqPara;

	public RequestsService(HttpServletRequest request) throws IOException, ServerException {
		this.request = request;
		String decodeString = getDecodedMessage(request);
		reqPara = toHashMap(decodeString);
	}
	
	public HashMap<String, Object> outPutMap() {
		return reqPara;
	}

	public String getParameter(String key) throws ServerException {
		if (key == null || key.length() < 1) {
			throw new ServerException("id_err_000", " Empty Parameter");
		}

		String paraValue = (String) reqPara.get(key);

		if (paraValue == null || paraValue.length() < 1) {
			throw new ServerException("id_err_000", "missed Parameter=" + key);
		}else {
			reqPara.put(key, paraValue);
		}

		return (String) reqPara.get(key);
	}

	public String getDecodedMessage(HttpServletRequest request) throws IOException {
		String message = null;
		message = getDecodeContent(request.getInputStream(), request.getMethod());
		return message;
	}

	public String getDecodedMessage(ServletRequest servletRequest) throws IOException {
		String message = null;
		message = getDecodeContent(servletRequest.getInputStream(), request.getMethod());
		return message;
	}

	public String getDecodeContent(InputStream in, String method) throws UnsupportedEncodingException, IOException {
		String message = null;
		String line;

		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));

		while ((line = reader.readLine()) != null) {
			if (!method.toUpperCase().equals("POST")) {
				buffer.append(URLDecoder.decode(line, "UTF-8"));
			} else {
				buffer.append(line);
			}
		}

		message = buffer.toString();
		return message;
	}

	public HashMap<String, Object> toHashMap(String paraStr) throws ServerException {
		HashMap<String, Object> hm = new HashMap<String, Object>();
		String key = "";

		if (null != paraStr && !"".equals(paraStr)) {
			String[] paras = paraStr.split("&");

			for (String tmpPara : paras) {
				
				int idx = tmpPara.indexOf("=");
				String paraKey = tmpPara.substring(0, idx);
				String Value = tmpPara.substring(idx + 1);
				String[] paraValue = {paraKey , Value};
				
				if (paraValue[0].equals("session_token")) {
					int ind = tmpPara.indexOf('=') + 1;
					hm.put(paraValue[0], tmpPara.substring(ind));
				} else if (paraValue.length > 1) {
					hm.put(paraValue[0], paraValue[1]);
				} else if (paraValue.length == 1 && paraValue[0] != "") {
					if (paraValue[0].equals("target_id")) {
						hm.put(paraValue[0], "");
					} else if (paraValue[0].equals("file_uri")) {
						hm.put(paraValue[0], "");
					} else {
						key += paraValue[0] + ",";
					}
				}
			}
		}

		if (key != "") {
			throw new ServerException("id_err_000", "missed Parameter= " + key);
		}

		return hm;
	}

	public Member getMember() throws IOException, ServerException {
		return getMemberById(getParameter("id"));
	}

	public Member getMemberById(String id) throws IOException, ServerException {
		EntityManager postgres = (EntityManager) request.getAttribute("connect");
		Member member = Member.getMemberByBMemberId(postgres, id, Member.MEMBERTYPE_NORMALREADER);

		if (null == member) {
			ServerException ex = new ServerException("id_err_204", " Account not exists. id=" + id );
			ex.printStackTrace();
			throw ex;
		} else {
			return member;
		}
	}

	public boolean hasParameter(String paraName) {
		return reqPara.containsKey(paraName);
	}

	public Long getParameterAsLong(String key) throws ServerException {
		if (null == key || key.length() < 1) {
			return null;
		}

		Long ret = null;
		String paraValue = getParameter(key);
		paraValue = paraValue.replaceAll("\"", "");
		ret = Long.valueOf(paraValue);
		return ret;
	}
}