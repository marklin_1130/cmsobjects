package digipages.common;

public class GenBenchParam {
	String bench_book_cnt;
	String is_trial;
	String bench_member_cnt;
	String avg_book_cnt;
	String avg_note_cnt;

	public String getBench_book_cnt() {
		return bench_book_cnt;
	}

	public void setBench_book_cnt(String bench_book_cnt) {
		this.bench_book_cnt = bench_book_cnt;
	}

	public String getIs_trial() {
		return is_trial;
	}

	public void setIs_trial(String is_trial) {
		this.is_trial = is_trial;
	}

	public String getBench_member_cnt() {
		return bench_member_cnt;
	}

	public void setBench_member_cnt(String bench_member_cnt) {
		this.bench_member_cnt = bench_member_cnt;
	}

	public String getAvg_book_cnt() {
		return avg_book_cnt;
	}

	public void setAvg_book_cnt(String avg_book_cnt) {
		this.avg_book_cnt = avg_book_cnt;
	}

	public String getAvg_note_cnt() {
		return avg_note_cnt;
	}

	public void setAvg_note_cnt(String avg_note_cnt) {
		this.avg_note_cnt = avg_note_cnt;
	}
}