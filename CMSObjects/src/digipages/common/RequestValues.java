package digipages.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;

import digipages.exceptions.ServerException;

/**
 * simple way to check parameter.
 * 
 * @author chlang
 *
 */
public class RequestValues {

	private HttpServletRequest request;
	Map <String,String>reqPara=new HashMap<>();
	
	public String outPutJson() {
		return new Gson().toJson(reqPara);
	}
	
	public Map<String,String> outPutMap() {
		return reqPara;
	}

	public RequestValues(HttpServletRequest request) {
		this.request = request;
		try {
			this.reqPara = buildParaFromInputStream(request);
		} catch (ServerException | IOException e) {
			e.printStackTrace();
		}
	}

	public String getParameter(String key) throws ServerException {
		if (key == null || key.length() < 1) {
			throw new ServerException("id_err_000", " Empty Parameter");
		}

		String paraValue = request.getParameter(key);

		
		if (paraValue == null || paraValue.length() < 1) {
			//use reqPara if exists
			if (reqPara.containsKey(key))
			{
				return reqPara.get(key);
			}
			throw new ServerException("id_err_000", " missed Parameter=" + key);
		}else {
			reqPara.put(key, paraValue);
		}

		return request.getParameter(key);
	}
	
	private static Map<String,String> buildParaFromInputStream(HttpServletRequest request) throws ServerException, UnsupportedEncodingException, IOException {
		Map <String,String> reqPara=new HashMap<>();
		// try to fill by item
		if (!request.getParameterMap().isEmpty())
		{
			return reqPara;
		}
		
		String decodeString = CommonUtil.inputStreamToString(request.getInputStream());
		reqPara = buildParameterMapFromStr(decodeString) ;
		return reqPara;
	}

	/**
	 * input format: (URLEncoded)
	 * party_type=SuperReader&party_id=ebook2&id=tebook033&device_id=5B72CD66BA0202C5E843E283A13A3D95&operator=iris&updated_time=2017%2F03%2F17&session_token=MjAxNi0xMi0xMiAxMDo0ODozNS4zOTc%3D
	 * @param decodeString
	 * @return
	 */
	private static Map<String, String> buildParameterMapFromStr(String urlEncodedStr) {
		Map <String,String> retParam=new HashMap<String,String>();
		String []split=urlEncodedStr.split("&");
		for (String pair:split)
		{
			String []keyValue = pair.split("=");
			retParam.put(keyValue[0], keyValue[1]);
		}
		
		return retParam;
	}

	public int getParameterAsInt(String key) throws ServerException {
		String paraValue = getParameter(key);
		paraValue = paraValue.replaceAll("\"", "");

		return Integer.parseInt(paraValue);
	}

	public Long getParameterAsLong(String key) throws ServerException {
		if (null==key || key.length()<1)
			return null;
		String paraValue = getParameter(key);
		paraValue = paraValue.replaceAll("\"", "");

		return Long.valueOf(paraValue);
	}

	public boolean hasParameter(String key) {
		String paraValue = request.getParameter(key);
		// using parsed if not exists.
		if (null==paraValue || paraValue.isEmpty())
		{
			paraValue=reqPara.get(key);
		}

		return null !=paraValue && !paraValue.isEmpty();
	}

	public boolean parameterEquals(String paraName, String paraValue) throws ServerException {
		if (hasParameter(paraName))
		{
			String tmpValue = getParameter(paraName);
			return paraValue.equals(tmpValue);
		}
		
		return false;
	}

	public String getParameterOrNull(String key) throws ServerException {
		if (hasParameter(key))
			return getParameter(key);
		return null;
	}
	
	
}