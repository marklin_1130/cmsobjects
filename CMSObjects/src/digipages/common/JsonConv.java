package digipages.common;


import java.io.IOException;

import org.boon.core.TypeType;
import org.boon.core.reflection.FastStringUtils;
import org.boon.core.reflection.fields.FieldAccess;
import org.boon.json.JsonFactory;
import org.boon.json.JsonSerializer;
import org.boon.json.JsonSerializerFactory;
import org.boon.json.serializers.CustomFieldSerializer;
import org.boon.json.serializers.JsonSerializerInternal;
import org.boon.primitive.CharBuf;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import com.google.common.base.CaseFormat;
import com.google.gson.Gson;


public class JsonConv {
	
	private static DPLogger logger = DPLogger.getLogger(JsonConv.class.getName());
	
	private JsonConv()
	{
	}
	/* special case for "BookMember" => "book_member" mapping, found not work well.
	 * this case have some "parent properties" problem
	 */
	final static private JsonSerializer serializer = new JsonSerializerFactory()
			.addPropertySerializer(new CustomFieldSerializer () {
				public boolean serializeField (JsonSerializerInternal serializer, Object parent,
                        FieldAccess fieldAccess, CharBuf builder ) {
					final String fieldName=fieldAccess.name();
					final TypeType typeEnum = fieldAccess.typeEnum ();
					
					Object value = fieldAccess.getObject ( parent );
		            /* Avoid back reference and infinite loops. */
		            if ( value == parent ) {
		                return false;
		            }
		            
					if (hasUpperCase(fieldName) && typeEnum==TypeType.STRING)
					{
						String newName = padUnderLine(fieldName);
						builder.addJsonFieldName ( FastStringUtils.toCharArray (newName));
						builder.add(JsonFactory.toJson(value));
	                    return true;
					}
					return false;
					
				}
			})
			.create();
	public static String toJson(Object obj)
	{
		return serializer.serialize(obj).toString();
	}
	
	protected static String padUnderLine(String origName) {
		return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, origName);
	}

	static boolean hasUpperCase(String string)
	{
		return !string.toLowerCase().equals(string.toLowerCase());
	}

	public static <T> T fromJson(String decrypted, Class <T> class1) {
		return JsonFactory.fromJson(decrypted,class1);
	}
	
	public static boolean isJSONValid(String JSON_STRING) {
		
		final Gson gson = new Gson();
		try {
			gson.fromJson(JSON_STRING, Object.class);
			return true;
		} catch(com.google.gson.JsonSyntaxException ex) {
			logger.info("CMSObject/id_err_000:"+JSON_STRING);
			return false;
		}
	  }
	
	/**
	 * use jackson, and annotation of @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
	 * reference : http://stackoverflow.com/questions/11757487/how-to-tell-jackson-to-ignore-a-field-during-serialization-if-its-value-is-null
	 * @param object
	 * @return
	 * @throws IOException
	 */
	public static String toJsonInc(Object object) throws IOException 
	{
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(object);
		return json;
		
	}
}
