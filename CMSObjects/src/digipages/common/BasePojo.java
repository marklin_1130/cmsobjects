package digipages.common;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import org.boon.json.JsonFactory;

import com.google.gson.Gson;

import digipages.AppHandler.UpdateBookReadListHandler;
import digipages.exceptions.ServerException;

public class BasePojo {

	private static DPLogger logger =DPLogger.getLogger(BasePojo.class.getName());

	public String toJsonString() {
		return JsonFactory.toJson(this);
	}
	
	public String toGsonStr() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public String toBase64String() throws ServerException {
		Encoder encoder = Base64.getEncoder();
		String base64Str = "";
		try {
			 base64Str = encoder.encodeToString(toJsonString().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
			throw new ServerException("id_err_999", e.getMessage());
		}
		return base64Str;
	}

	public static <T> T fromBase64String(String base64, Class<T> class1) throws UnsupportedEncodingException {
		Decoder decoder = Base64.getDecoder();
		byte[] jsonary = decoder.decode(base64);
		String json = new String(jsonary, "UTF-8");
		return JsonFactory.fromJson(json, class1);
	}

}
