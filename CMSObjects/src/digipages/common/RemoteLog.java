package digipages.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class RemoteLog {
	
	private static DPLogger logger =DPLogger.getLogger(RemoteLog.class.getName());

	private long startTime = 0;
	private long endTime = 0;
	private Long duration=Long.valueOf(0L);
	private List <String>tags;
	private String servletName;
	private String inputs;
	private String logContent;
	private String type;
	
	private static final String url="http://appapi.booksdev.digipage.info/V1.3/CMSAPIApp/RemoteLog";
	
	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
		duration = endTime-startTime;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getServletName() {
		return servletName;
	}

	public void setServletName(String servletName) {
		this.servletName = servletName;
	}

	public String getInputs() {
		return inputs;
	}

	public void setInputs(String inputs) {
		this.inputs = inputs;
	}

	public String getLogContent() {
		return logContent;
	}

	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static String getUrl() {
		return url;
	}

	public void startLog()
	{
		startTime = System.currentTimeMillis();
	}
	
	public void endLog()
	{
		endTime = System.currentTimeMillis();
		duration = endTime-startTime;
	}
	
	public static void logErr(String errMessage)
	{
		log("Error",errMessage);
	}
	public static void log(String type, String errMessage)
	{
		log(type,null,errMessage);
	}
	
	public static void log(String type, String[] tags, String errMessage){
		RemoteLog log = new RemoteLog();
		log.type=type;
		log.logContent=errMessage;
		for (String tag:tags)
			log.tags.add(tag);
		log.sendServer();
	}
	
	private void sendServer()
	{
		try {
		
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("logValues", toJson()));
			
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			httpclient.execute(httpPost);
			
//			logger.info("Response:" + response2);
		} catch (IOException e) {
			logger.error(e);;
		}
	}
	
	public RemoteLog()
	{
		
	}
	
	public static RemoteLog fromJson(String jsonStr)
	{
		Gson jsonParse = new GsonBuilder().create();
		RemoteLog ret=jsonParse.fromJson(jsonStr, RemoteLog.class);
		return ret;
	}
	
	public String toJson() throws IOException
	{
		return JsonConv.toJson(this);
	}
}
