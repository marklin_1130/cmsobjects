package digipages.common;

import java.util.List;

import javax.persistence.EntityManager;

import digipages.exceptions.ServerException;
import model.BookFile;
import model.BookUniId;
import model.BookVersion;

public class MemberBookFile {
	public BookFile findMaxVersionBookFileForThirdPartyAccount(EntityManager postgres, String bookUniId) throws ServerException {
		BookFile maxBookFile = null;
		boolean isTrial = bookUniId.endsWith("_trial");
		BookUniId buid = new BookUniId(bookUniId);
		String itemID = buid.getItemId();

		BookVersion maxVersion = new BookVersion("V000.0000");

		List<BookFile> getBookFiles = postgres.createNamedQuery("BookFile.findByItemIdForThirdPartyAccount", BookFile.class)
				.setParameter("item_id", itemID)
				.getResultList();

		for (BookFile bf : getBookFiles) {
			// skip non match trial flag.
			if (bf.getIsTrial() != isTrial)
				continue;
			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
				maxBookFile = bf;
			}
		}

		return maxBookFile;
	}
	
	public BookFile findMaxVersionBookFileForThumbGenerator(EntityManager postgres, String bookUniId) throws ServerException {
		BookFile maxBookFile = null;
		boolean isTrial = bookUniId.endsWith("_trial");
		BookUniId buid = new BookUniId(bookUniId);
		String itemID = buid.getItemId();

		BookVersion maxVersion = new BookVersion("V000.0000");

		List<BookFile> getBookFiles = postgres.createNamedQuery("BookFile.findByItemIdForThumbGenerator", BookFile.class)
				.setParameter("item_id", itemID)
				.getResultList();

		for (BookFile bf : getBookFiles) {
			// skip non match trial flag.
			if (bf.getIsTrial() != isTrial)
				continue;
			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
				maxBookFile = bf;
			}
		}

		return maxBookFile;
	}

	public BookFile findMaxVersionBookFileForNormalReader(EntityManager postgres, String bookUniId) throws ServerException {
		BookFile maxBookFile = null;
		boolean isTrial = bookUniId.endsWith("_trial");
		BookUniId buid = new BookUniId(bookUniId);
		String itemID = buid.getItemId();

		BookVersion maxVersion = new BookVersion("V000.0000");

		List<BookFile> getBookFiles = postgres.createNamedQuery("BookFile.findByItemIdForNormalReader", BookFile.class)
				.setParameter("item_id", itemID)
				.getResultList();

		for (BookFile bf : getBookFiles) {
			// skip non match trial flag.
			if (bf.getIsTrial() != isTrial)
				continue;
			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
				maxBookFile = bf;
			}
		}

		return maxBookFile;
	}
}