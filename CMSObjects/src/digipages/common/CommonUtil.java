package digipages.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.CharacterIterator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import javax.crypto.KeyGenerator;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.boon.json.JsonFactory;

import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.Utility;
import digipages.BookConvert.utility.vod.MediaBookRequestData;
import digipages.exceptions.ServerException;
import model.BookFile;
import model.CmsToken;
import model.Member;
import model.MemberBook;
import model.MemberDevice;
import model.MemberDrmLog;
public class CommonUtil {

	static DPLogger logger = DPLogger.getLogger(CommonUtil.class.getName());
	public static String DEFAULT_FORMATE = "yyyy/MM/dd HH:mm:ss";
	
	private CommonUtil()
	{
		
	}
	// format: 2015/05/12T00:00:00Z+8, 2015-07-06T15:41:20+08:00, 2015-07-06T15:41:20+0800
	public static OffsetDateTime parseDateTime(String dateTimeStr) {

		if (dateTimeStr.indexOf(" ") != -1) {
			dateTimeStr = dateTimeStr.replace(" ", "T");
		}
		return OffsetDateTime.parse(standardizeIso8601(dateTimeStr));
	}
	
	// format: 2015/05/12T00:00:00Z+8, 2015-07-06T15:41:20+08:00, 2015-07-06T15:41:20+0800
	public static java.util.Date parseDateTime2JDate(String dateTimeStr) {
		OffsetDateTime offset= parseDateTime(dateTimeStr);
		
		return new Date(offset.toInstant().toEpochMilli());
	}
	
	// from 9999/12/31 00:00:00 to iso8601 (2016-03-22T10:14:53+08:00)
	// if fail to recognize, return with original date String
	public static String normalizeDateTime(String wrongFormat)
	{
		String ret = wrongFormat;
		try {
			SimpleDateFormat oldDateTimeFormatter = new  SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date d = oldDateTimeFormatter.parse(wrongFormat);
			ret = fromDateToString(d);
		}catch (Exception e)
		{
//			e.printStackTrace();
		}
		return ret;
	}

	// if fail to recognize, return with original date String
	public static Date normalizeGetDateTime(String wrongFormat) {
		Date ret = null;
		try {
			SimpleDateFormat oldDateTimeFormatter = new  SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			 ret = oldDateTimeFormatter.parse(wrongFormat);
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return ret;
	}
	
	public static Date getDate(String str) throws ParseException {
		SimpleDateFormat sdf =new SimpleDateFormat(DEFAULT_FORMATE);
		return sdf.parse(str);
	}
	
	public static String getDateToString(java.util.Date date) {
		SimpleDateFormat sdf =new SimpleDateFormat(DEFAULT_FORMATE);
		return sdf.format(date);
	}
	
	public static String getDateToString(java.util.Date date, String format) {
		SimpleDateFormat sdf =new SimpleDateFormat(format);
		return sdf.format(date);
	}

	// from java.util.date to  2015-07-06T15:41:20+08:00
	public static String fromDateToString(java.util.Date date)
	{
		String ret=OffsetDateTime.ofInstant(date.toInstant(), ZoneId.of("+08:00"))
				.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX:00"));
		
		// remote need clean output, cut off .123 under second.
		// 2015-07-06T15:41:20.123+08:00 => 2015-07-06T15:41:20+08:00
		ret=ret.replaceAll("\\.\\d\\d\\d", "");
		
		return ret;
	}
	
	public static Map<String, String> getURIasMap(String baseURI,String requestURI) {
		// cut out baseURI
		int loc = requestURI.indexOf(baseURI);
		int rqLength = requestURI.length();
		int baseURILength = baseURI.length(); 
		requestURI=requestURI.substring(loc + baseURILength, rqLength);
		
		String [] split=requestURI.split("/");
		Map <String,String> parameters = new HashMap<String,String>();
		
		String curKey="";
		for (int i=0; i< split.length; i++)
		{
			if (i%2==0)
			{
				curKey=split[i];
			}else
			{
				parameters.put(curKey, split[i]);
			}
		}
		return parameters;

	}
	
	public static List<String> getURIasList(String baseURI,String requestURI) {
		// cut out baseURI
		int loc = requestURI.indexOf(baseURI);
		int rqLength = requestURI.length();
		int baseURILength = baseURI.length(); 
		requestURI=requestURI.substring(loc + baseURILength, rqLength);

		
		String []split=requestURI.split("/");
		List <String> parameters = Arrays.asList(split);
		return parameters;
	}

	/**
	 * 
	 * from "2015-07-06T15:41:20+0800"  to "2015-07-06T15:41:20+08:00"
	 * convert " " to "T"
	 * @param srcDT
	 * @return
	 */
	static public String standardizeIso8601(String srcDT) {
		srcDT = srcDT.replace(' ', 'T');
		if (srcDT.lastIndexOf(':')> (srcDT.length()-4))
		{
			return srcDT;
		}
		else
		{
			String newX=srcDT.substring(0,srcDT.length()-2) + ":00";
			return newX;
		}
		
	}
	
	public static void verifyParameters(String reqParameters, HttpServletRequest request) throws ServerException
	{
		String []split=reqParameters.split(",");
		for (String chkParam:split)
		{
			String paramValue=request.getParameter(chkParam);
			if (null==paramValue || paramValue.length()==0)
			{
				throw new ServerException("id_err_000", " " + chkParam + " is empty");
			}
		}
		
	}

	public static String zonedTime() {
		return fromDateToString(new Date());
	}

	public static boolean isEmpty(String stringValue) {
		
		return stringValue==null || stringValue.length()==0;
	}
	
	public static String getServLetURL(HttpServletRequest request) {
		String reqHead=request.getRequestURL().toString();
		reqHead=reqHead.replaceAll(request.getServletPath(), "");
		return reqHead;
	}
	
	
	public static byte[] generateAES256Key() throws NoSuchAlgorithmException{
		KeyGenerator keygen;
		keygen=KeyGenerator.getInstance("AES");
		keygen.init(256);
		byte[] encryptionKey=keygen.generateKey().getEncoded();
		return encryptionKey ;
	}

	/**
	 * use json to copy object to clone
	 * @param object
	 * @param clazz
	 * @return
	 */
	public static <T> T jsonClone(Object object, Class<T> clazz) {
		String jsonStr=JsonFactory.toJson(object);
		return JsonFactory.fromJson(jsonStr,clazz);
	}
	
	/**
	 * get tail parameter from uri, skip last empty item.
	 * @param uri
	 * @return
	 */
	public static String getTailSplitName(String uri) {
		String []tmpAry = uri.split("/");
		int lastIdx = tmpAry.length-1;
		
		if ((tmpAry[lastIdx]).length()>0)
			return tmpAry[lastIdx];
		else
			return tmpAry[lastIdx-1];
	}
	
	
	public static String inputStreamToString(InputStream inputStream, String encoding) throws IOException{
		
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream, encoding));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null){
			String tmpTail="";
			if (!(line.endsWith("\r") || line.endsWith("\n")))
				tmpTail="\r\n";
			result += line + tmpTail;
		}
		return result;
	}
	
	public static String inputStreamToString(InputStream inputStream) throws IOException{
			return inputStreamToString(inputStream,"utf-8");
		}
	
	public static String toBooksDateStr(Date lastUpdated){
		
		String dateTime ="";
		String udt = fromDateToString(lastUpdated) ;
		udt = udt.replace("-", "/")  ;
		udt = udt.replace("T", " ")  ;
		dateTime= udt.split("\\.")[0] ;
		return dateTime;
		
	}
	
	/**
	 * from 
	 * http://viewer.booksdev.digipage.info/viewer/index.html?readlist=magazine
	 * to 
	 * http://viewer.booksdev.digipage.info
	 * or 
	 * https://viewer.booksdev.digipage.info/viewer/index.html?readlist=magazine
	 * to
	 * https://viewer.booksdev.digipage.info
	 * @param referrer
	 * @return
	 */
	public static String getRequestHost(String referrer) {
		int loc =0;
		int cnt =0;
		for (int i=0;i<referrer.length();i++)
		{
			if (referrer.charAt(i)=='/')
			{
				cnt++;
			}
			if (cnt==3){
				loc = i;
				break;
			}
		}
		if (loc==0)
			loc = referrer.length();
		String ret = referrer.substring(0,loc);
		return ret;
	}
	
	//
	public static String getCurLocalDateTimeStr(String timeZone)
	{
		if (timeZone==null || timeZone.length()<1)
			timeZone = "UTC+8";
		ZoneId zoneId = ZoneId.of(timeZone);
		ZonedDateTime zdt = ZonedDateTime.now(zoneId);
		return zdt.toString();
	}
	public static String responseToStr(CloseableHttpResponse response)
			throws UnsupportedOperationException, IOException {
		String responseStr = null;
		try {
			// logger.info(response.getStatusLine());
			HttpEntity entity1 = response.getEntity();
			InputStream inp = entity1.getContent();
			Scanner scanner = new Scanner(inp, "UTF-8");
			scanner.useDelimiter("\\A");
			responseStr = scanner.next();
	
			scanner.close();
			inp.close();
			EntityUtils.consume(entity1);
		} finally {
			response.close();
		}
		return responseStr;
	}
	public static String readPutRequest(HttpServletRequest request) throws UnsupportedEncodingException, IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(request.getInputStream(), "UTF-8"));
		String line = "",result="";
		while ((line = bufferedReader.readLine()) != null) {
			result += line;
		}
		bufferedReader.close();
		result = URLDecoder.decode(result,"UTF-8");

		return result;
	}
	public static String dumpObject(Object anObject) {
		
		
		return ToStringBuilder.reflectionToString(anObject);
	}
	// notice: performance 
	public static boolean hasDuplicate(List<String> deviceNames, String deviceName) {
		for (String tmpName:deviceNames)
		{
			if (deviceName.equals(tmpName))
			{
				return true;
			}
		}
		return false;
	}
	
	public static String genNewName(List<String> deviceNames, String curName) {
		int max=1;
		for (String tmpName:deviceNames)
		{
			if (null==tmpName)
				continue;
			
			if (tmpName.startsWith(curName))
			{
				String []x = tmpName.split("#");
				if (x.length>1)
				{
					int curNum = 0;
					/* some user may use # as his favorite device name ==> cause parseInt crash
					 * make it exception (not handle)
					 * ex: iphone#1 ==> iphone#5s (my 5s) or iphone#rita's iphone
					*/
					try {
							curNum=Integer.parseInt(x[1]);
					} catch (Exception ex)
					{
						continue;
					}
					if (curNum > max)
						max=curNum;
				}
			}
		}
		return curName + "#" + (max+1);
	}
	
	@SuppressWarnings("resource")
	public static String execCmd(String cmd) throws java.io.IOException {
        java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(cmd)
        		.getInputStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
	

	public static String traceRoute(InetAddress address) {
		final String os = System.getProperty("os.name").toLowerCase();
		String route = "";
		try {
			Process traceRt;
			String encoding = "utf-8";
			if (os.contains("win")){
				encoding = "big5";
				traceRt = Runtime.getRuntime().exec("tracert -d -w 2 -h 15 " + address.getHostName());
			}
			else
				traceRt = Runtime.getRuntime().exec("traceroute -T -p 443 -n -w 2 -N 64 " + address.getHostName());

			// read the output from the command
			route = inputStreamToString(traceRt.getInputStream(), encoding);

			// read any errors from the attempted command
			String errors = inputStreamToString(traceRt.getErrorStream(),encoding);
			if (errors != "")
				logger.error(errors);
			traceRt.getErrorStream().close();
			traceRt.getOutputStream().close();
			traceRt.getInputStream().close();
		} catch (IOException e) {
			logger.error("error while performing trace route command", e);
		}

		return route;
	}
	public static String traceRoute(String address) throws UnknownHostException {
		InetAddress addr = InetAddress.getByName(address);
		return traceRoute(addr);
	}
	
	public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
	
	public static String httpClient(String url) throws UnsupportedOperationException, IOException {
		String responseStr = "";
		
		final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
		HttpGet httpGet = new HttpGet(url);
		httpclient.execute(httpGet);
		return responseStr ;
	}
	
	public static String escapeSpecialCharacter(String aText){

		// skip empty String
		if (aText==null || aText.length()<1)
			return aText;
		
	     final StringBuilder result = new StringBuilder();
	     final StringCharacterIterator iterator = new StringCharacterIterator(aText);
	     char character =  iterator.current();
	     while (character != CharacterIterator.DONE ){

	       if (character == '<' ||
				character == '!'  ||
				character == '@'  || 
				character == '#'  || 
				character == '$'  ||
				character == '%'  ||
				character == '^'  ||
				character == '&'  ||
				character == '*'  ||
				character == '('  ||
				character == ')'  ||
				character == '|'  ||
				character == '<'  ||
				character == '>'  ||
				character == '\\' ||
				character == '/'  ||
				character == '['  ||
				character == ']'  ||
				character == '{'  ||
				character == '}'  ||
				character == '"'  ||
				character == '+'  
	    		   ) {
	         result.append(""); 
	       }else{
	    	   result.append(character);  
	       }
	       character = iterator.next();
	     }
	     return result.toString();
	  }
	
	public static Date nextNMinutes(int i) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, i);
		return c.getTime();

	}
	public static BufferedRequestWrapper getBufferedReqestWrapper(HttpServletRequest req) throws IOException {
		BufferedRequestWrapper bufferedRequest = null;
		int length = req.getContentLength();
	
		if (length > 0) {
			bufferedRequest = new BufferedRequestWrapper(req, length);
		}
	
		return bufferedRequest;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public static String requestStream2String(HttpServletRequest request) throws IOException {
		BufferedRequestWrapper bufferedRe = CommonUtil.getBufferedReqestWrapper(request);

		InputStream st = bufferedRe.getInputStream();
		String resultStr = CommonUtil.inputStreamToString(st);
		st.close();
		return resultStr;
	}
	/**
	 * from httpURLConnection (already put/post), get response as String.
	 * @param connection
	 * @return
	 * @throws IOException
	 */
	public static String getResponseFromConnection(HttpURLConnection connection) throws IOException {
	       BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        StringBuilder response = new StringBuilder();
	
	        int bufferSize=1024;
	        char[] buffer = new char[bufferSize]; // or some other size, 
	        int charsRead ;
	        while ( (charsRead  = in.read(buffer, 0, bufferSize)) != -1) {
	            response.append(buffer, 0, charsRead);
	        }
	        in.close();
	        return response.toString();
	}
	

	public static String currentDateTime() {
		String curLocalDateTime = getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19)
				.replace('T', ' '), formatter);
		return String.valueOf(dateTime.getYear()) + "/" 
				+ String.format("%02d", dateTime.getMonthValue()) + "/" 
				+ String.format("%02d", dateTime.getDayOfMonth()) + " " 
				+ String.format("%02d", dateTime.getHour()) + ":" 
				+ String.format("%02d", dateTime.getMinute()) + ":" 
				+ String.format("%02d", dateTime.getSecond());
	}
	
	public static ZonedDateTime currentDate() {
		Instant now = Instant.now();
		ZoneId zoneId = ZoneId.of("UTC+8");
		ZonedDateTime zdt = ZonedDateTime.ofInstant(now, zoneId);
		return zdt;
	}
	
	public static ZonedDateTime convertOldDateToLocalDateTime(String oldDateTime) throws ParseException {
		ZonedDateTime zdt = null;
		try {
			SimpleDateFormat oldDateTimeFormatter = new  SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			zdt = oldDateTimeFormatter.parse(oldDateTime).toInstant().atZone(ZoneId.of("UTC+8"));
		} catch (Exception e) {
			logger.error("error to convert date:"+oldDateTime,e);
			throw e;
		}		
		return zdt;
	}
	
	public static String zoneDateToSybaseStr(String zoneDate)
	{
		SimpleDateFormat oldDateTimeFormatter = new  SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date=parseDateTime2JDate(zoneDate);
		return oldDateTimeFormatter.format(date);
	}
	/**
	 * ex:
	 * originHost = http://viewer.booksdev.digipage.info
	 * return = http://*.booksdev.digipage.info
	 * ex2:
	 * originHost = https://viewer.booksdev.digipage.info
	 * return = https://*.booksdev.digipage.info
	 * @param originHost
	 * @return
	 */
	public static String genCrossDomainHost(String originHost) {
		String head = originHost.contains("https://") ? "https://" :"http://";
		String domain = originHost.replace(head, "");
		String [] domainSplit = domain.split("\\.");
		StringBuilder sb = new StringBuilder();
		sb.append(head);
		sb.append("*").append(".");
		for (int i=1;i< domainSplit.length; i++)
		{
			sb.append(domainSplit[i]).append(".");
		}
		// remove tail "."
		sb.setLength(sb.length()-1);
		
		return sb.toString();
	}
	
	public static String getDisplayFileSize(long size) {
		if(size < 0) {
			return "";
		}

		String displayFileSize = "";
		double bytes = size;
		double kilobytes = (bytes / 1024);
		if(size >= 1048576 ) {
			
			BigDecimal mbDecimal = new BigDecimal(kilobytes).divide(new BigDecimal(1024)).setScale(1, RoundingMode.HALF_UP);
			displayFileSize = (mbDecimal.toString()+" MB");
			
			displayFileSize = displayFileSize.replaceAll(".0 MB", " MB");
			
		}else {
			BigDecimal mbDecimal = new BigDecimal(bytes).divide(new BigDecimal(1024)).setScale(0, RoundingMode.HALF_UP);
			displayFileSize = mbDecimal.toString()+" KB";
		}
		
		return displayFileSize;
	}

	public static MemberBook genSuperReaderMemberBook(Member member, BookFile bf, EntityManager postgres ) throws ServerException
	{
		// if already has book.
		MemberBook memberBook=member.getMemberBookByBUID(postgres, bf.getBookUniId());

		if (memberBook!=null) {
			if (memberBook.getMemberBookDrmMappings().isEmpty())
			{
				createDRM(memberBook,postgres);
			}
			return memberBook;
		}
		
//		postgres.getTransaction().begin();
		memberBook = new MemberBook();
		memberBook.setMember(member);
		memberBook.updateByBookFile(bf);
		memberBook.setCreateTime(bf.getCreateTime());
		member.addMemberBook(memberBook);
		createDRM(memberBook,postgres);
		postgres.persist(memberBook);
		postgres.flush();
//		postgres.getTransaction().commit();
		
		return memberBook;
	}

	private static void createDRM(MemberBook memberBook, EntityManager postgres) {
		MemberDrmLog drmLog = new MemberDrmLog();
		drmLog.setDownloadExpireTime("9999/12/31 23:59:59");
		drmLog.setItem(memberBook.getItem());
		drmLog.setMember(memberBook.getMember());
		drmLog.setReadDays(99999);
		drmLog.setReadExpireTime("9999/12/31 23:59:59.99");
		drmLog.setReadStartTime(CommonUtil.currentDateTime());
		drmLog.setStatus(1);
		drmLog.setSubmitId("autoGen");
		drmLog.setTransactionId("0000");
		drmLog.setType(1);
		List<MemberDrmLog> tmpLog = new ArrayList<>();
		tmpLog.add(drmLog);
		memberBook.setMemberDrmLogs(tmpLog);
		memberBook.getMember().addMemberDrmLog(drmLog);
		postgres.persist(drmLog);
	}
	
	/**
	 * for Thumbnail generator. 
	 * @param member
	 * @param postgres
	 * @return
	 * @throws ServerException 
	 */
	public static CmsToken genCmsToken(Member member, EntityManager postgres) throws ServerException {
		
		postgres.getTransaction().begin();
		CmsToken token = new CmsToken();
		token.setClientIp("127.0.0.1");
		token.setMemberId(member.getId());
		if (null==member.getMemberWebDevice()){
			MemberDevice md= new MemberDevice();
			md.setDeviceId(UUID.randomUUID().toString());
			md.setOsType("web");
			md.setLastOnlineIp("127.0.0.1");
			md.setStatus(2);
			md.setDeviceName("virtualDevice");
			member.addMemberDevice(md);
			postgres.persist(md);
		}
		token.setDeviceId(member.getMemberWebDevice().getId());
		java.sql.Timestamp ts=java.sql.Timestamp.valueOf(LocalDateTime.now());
		token.setLastUpdated(CommonUtil.fromDateToString(ts));
		postgres.persist(token);
		postgres.getTransaction().commit();
		token.setDeviceId(member.getMemberWebDevice().getId());
		
		return token;
	}
	
    public static String getFolderName(RequestData requestData) throws Exception {
        String folderPath;
        String hashCode;

        hashCode = Utility.encrypt(requestData.getItem() + requestData.getRequest_time());        
        folderPath = hashCode.substring(hashCode.length() - 6) + "/" +
                requestData.getBook_file_id(); // {hash_code}/{book_file_id}
        
        return folderPath;
    }
	
    public static String getFolderName(String itemId , String requestTime , long bookFileId) throws Exception {
        String folderPath;
        String hashCode;

        hashCode = Utility.encrypt(itemId + requestTime);        
        folderPath = hashCode.substring(hashCode.length() - 6) + "/" +bookFileId; // {hash_code}/{book_file_id}
        
        return folderPath;
    }
    
    public static String getFolderName(MediaBookRequestData requestData) throws Exception {
        String folderPath;
        String hashCode;

        hashCode = Utility.encrypt(requestData.getItem() + requestData.getRequest_time());        
        folderPath = hashCode.substring(hashCode.length() - 6) + "/" +
                requestData.getBook_file_id(); // {hash_code}/{book_file_id}
        
        return folderPath;
    }
}
