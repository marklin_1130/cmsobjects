package digipages.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class BufferedRequestWrapper extends HttpServletRequestWrapper {

	private static DPLogger logger =DPLogger.getLogger(BufferedRequestWrapper.class.getName());
	
	ByteArrayInputStream bais;
    private ByteArrayOutputStream baos = null;

    BufferedServletInputStream bsis;

    byte[] buffer;

    public BufferedRequestWrapper(HttpServletRequest req,int length) throws IOException {
        super(req);
        // Read InputStream and store its content in a buffer.
        InputStream is = req.getInputStream();
        buffer = new byte[length];

		int pad = 0;
		int readSize = 0;

		while ((readSize = is.read(buffer, pad, length)) > 0) {
			pad += readSize;
		}
    }
    
    public BufferedRequestWrapper(HttpServletRequest req,InputStream stream) throws IOException {
        super(req);
        // Read InputStream and store its content in a buffer.
//        InputStream is = stream.getInputStream();
    	InputStream is = stream;
        this.baos = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int letti;
        while ((letti = is.read(buf)) > 0) {
            this.baos.write(buf, 0, letti);
        }
        this.buffer = this.baos.toByteArray();
    }

    public ServletInputStream getInputStream() {
        try {
            // Generate a new InputStream by stored buffer
            bais = new ByteArrayInputStream(buffer);
            // Istantiate a subclass of ServletInputStream
            // (Only ServletInputStream or subclasses of it are accepted by the
            // servlet engine!)
            bsis = new BufferedServletInputStream(bais);
        } catch (Exception ex) {
        	logger.error(ex);;
        } finally {
        }
        return bsis;
    }


}
