package digipages.common;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

public class PDFPageSizeDump {
	
	private static DPLogger logger = DPLogger.getLogger(PDFPageSizeDump.class.getName());
	
	public static void main(String[] args) {
		int pdfPageObjCntLimit=200;
    	long startTime = System.currentTimeMillis();
    	File pdfFile = new File(args[0]);
    	try {
	    	PDDocument sourceDocument = PDDocument.load(pdfFile);
	    	
			int totalPage= sourceDocument.getNumberOfPages();
			StringBuilder sb=new StringBuilder();
			for (int i=0;i< totalPage; i++)
			{
				PDPage tempPage = sourceDocument.getPage(i);
				logger.info("page:" + i);
				logger.info("mediabox: " + dumpDimension(tempPage.getMediaBox()));
				logger.info("cropbox : " + dumpDimension(tempPage.getCropBox()));
				logger.info("Trimbox : " + dumpDimension(tempPage.getTrimBox()));
				logger.info("bleedbox: " + dumpDimension(tempPage.getBleedBox()));
				logger.info("Artbox  : " + dumpDimension(tempPage.getArtBox()));
				
				
			}
			logger.info(sb.toString());
			sourceDocument.close();
    	} catch (IOException ioex)
    	{
    		logger.error(ioex);
    	}

	}

	private static String dumpDimension(PDRectangle bBox) {
		StringBuilder sb = new StringBuilder();
		sb.append(bBox.getWidth()).append(":").append(bBox.getHeight());
		sb.append(",").append(bBox.toString());
		return sb.toString();
	}
}
