package digipages.common;

import org.boon.json.annotations.JsonInclude;

public class CommonResponse {
	@JsonInclude
	Boolean result = true;

	public Boolean isResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}
}
