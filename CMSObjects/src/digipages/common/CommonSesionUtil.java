package digipages.common;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import digipages.exceptions.ServerException;

public class CommonSesionUtil {
	
	private static DPLogger logger = DPLogger.getLogger(CommonSesionUtil.class.getName());
	
	private CommonSesionUtil()
	{
		
	}
	public static Map <String,Cookie> getCookieAsMap(HttpServletRequest request)
	{
		Map <String,Cookie> cookieMap = new HashMap<String,Cookie>();
		if (request!=null && request.getCookies()!=null){
			for (Cookie tmpCookie:request.getCookies())
			{
				// localhost is consider full matched
				if (serverIsLocalhost(request) && null!=tmpCookie.getDomain())
				{
					logger.info("Debug: cookie domain is not null, but yet server is localhost?!");
				}
//				System.err.println("Debug: cookie dumping => " + dumpCookie(tmpCookie));
				// when not in cross domain scenario(Web Test/App mode), the getDomain always null, which lead to block everything.
//				if (!serverIsLocalhost(request) && null==tmpCookie.getDomain())
//				{
//					logger.info("skip null domain Cookie From Client:\n" + dumpCookie(tmpCookie));
//					continue;
//				}
				cookieMap.put(tmpCookie.getName(), tmpCookie);
			}
		}
		return cookieMap;
	}
	
	@SuppressWarnings("unused")
	private static String dumpCookie(Cookie tmpCookie) {
		StringBuilder sb = new StringBuilder();
		sb.append("CookieName:").append(tmpCookie.getName()).append("\n");
		sb.append("Domain:").append(tmpCookie.getDomain()).append("\n");
		sb.append("Content:").append(tmpCookie.getValue()).append("\n");
		
		return sb.toString();
	}

	public static String getClientIpAddr(HttpServletRequest request) {
		// 先解 X-Real-IP ，若沒有，再解 X-Forwarded-For
		String ip = request.getHeader("X-Real-IP");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Forwarded-For");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		if(ip.indexOf(",")>0){
			String ips[]=ip.split(",");
            ip=ips[0];
		}

		return ip;
	}

	
	public static String getCookie(HttpServletRequest request, String cookieName) throws ServerException{
		try {
			Map <String,Cookie> tmpCookieMap=getCookieAsMap(request);
			return tmpCookieMap.get(cookieName).getValue();
		}catch (Exception ex)
		{
			logger.error("err_204:", ex);
			throw new ServerException("id_err_204", "Cookie not found:" + cookieName + " or expired.");
		}
	}
	public static boolean serverIsLocalhost(HttpServletRequest request) {
		
		// aws host name => not local
		if (request.getLocalName().contains("compute.internal"))
			return false;
		if (request.getLocalName().endsWith(".benqguru.com"))
			return false;
		if (request.getLocalName().endsWith(".digipage.info"))
			return false;
		if (request.getLocalName().endsWith(".books.com.tw"))
			return false;
		
		
		if (request.getLocalName().equals("localhost"))
			return true;
		if (request.getLocalAddr().startsWith("127.0.0."))
			return true;
		if (request.getLocalAddr().contains(":0:0:"))
			return true;
		if (request.getLocalAddr().startsWith("192.168"))
			return true;
		if (request.getLocalAddr().startsWith("10."))
			return true;
		if (request.getLocalAddr().startsWith("172."))
			return true;
		return false;
	}
	
	/**
	 * "test1.benq.com => ".benq.com"
	 * @param fullName
	 * @return
	 */
	public static String getSuperDomain(String fullName) {

		StringBuilder sb = new StringBuilder();
		String splited[]=fullName.split("\\.");
//		int i=1;
//		if (splited.length>2)
//			i=splited.length-2;
		for (int i=1;i<splited.length;i++)
		{
			sb.append(".").append(splited[i]);
		}
		return sb.toString();
	}
	
	public static String genAllowOriginHost(HttpServletRequest request) {
		String originHost="*";
		
		// switch origin and referer order
		
		String referrer =  request.getHeader("origin");
		if (null==referrer || referrer.length()<1)
			referrer = request.getHeader("referer");
		
		if (null==referrer || referrer.length()<1)
		{
			// nothing to use = default *
		}else
		{
			originHost = CommonUtil.getRequestHost(referrer);
		}
		// no cross sub domain allowed.
//		originHost = CommonUtil.genCrossDomainHost(originHost);
		
		return originHost;
	}
}
