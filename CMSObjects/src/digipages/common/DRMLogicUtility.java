package digipages.common;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import digipages.exceptions.ServerException;
import model.BaseBookInfo;
import model.ItemInfo;
import model.Member;
import model.MemberBook;
import model.MemberBookDrmMapping;
import model.MemberDrmLog;

public class DRMLogicUtility {
	private static DPLogger logger = DPLogger.getLogger(DRMLogicUtility.class.getName()); 
	final static String DEFAULT_ENDTIME = "2999-12-31T23:59:59+08:00";
	String dl_start_time = ""; // now()
	String dl_end_time = DEFAULT_ENDTIME; // member_drm_log,download_expire_time
	String license_type = "General"; // member_drm_log,type
	String license_group = ""; // phase2
	String isAuthorized = "Y"; // temporary
	String item_id = ""; // member_drm_log,item_id
	String member_id = ""; // bmember_id
	String read_start_time = ""; // member_book,start_read_time
	String read_end_time = DEFAULT_ENDTIME; // member_book,last_read_time??
	String duration = "0"; // member_drm_log,read_days * 24,phase 2
	String grace_period = "0"; // 0,phase 2
	String chapter_price = ""; // phase 2
	String chapter_limit = "ALL"; // all,phase 2
	String isShare = ""; // basebookInfo,share_flag
	String isPublic = ""; // basebookInfo,public_flag
	String isLike = ""; // basebookInfo,like_flag
	String hasAnnotation = ""; // basebookInfo,annotation_flag
	String hasBookmark = ""; // basebookInfo,bookmark_flag
	String hasNote = ""; // basebookInfo,note_flag
	String checksum = ""; // book_file,checksum
	String auth_id = ""; // member_drm_log,id
	String drm_type = ""; // drm_type
	String server_time ="";

	public static DRMLogicUtility fromJson(String drmJson) {
		Gson gson = new Gson();
		DRMLogicUtility drmUtil = gson.fromJson(drmJson, DRMLogicUtility.class);
		fixStartTime(drmUtil);
		return drmUtil;
	}

	public static void fixStartTime(DRMLogicUtility drmUtil) {
		
		drmUtil.dl_start_time="1970-01-01T00:00:00+08:00";
		drmUtil.read_start_time=fixDateToNow(drmUtil.read_start_time);
		
	}

	public static String fixDateToNow(String origStartTime) {
		String defaultStart = "2000-01-01T00:00:00+08:00";
		String targetTime = "";
		try {
			if(StringUtils.isBlank(origStartTime)){
				return defaultStart;
			}
			OffsetDateTime origTime=CommonUtil.parseDateTime(origStartTime);
			if (origTime.isAfter(OffsetDateTime.now())){
				targetTime = defaultStart;
//				logger.error("Start Time is not useable:" + origStartTime);
			}
			else{
				targetTime = origStartTime;
			}
		}catch (Exception e)
		{
//			logger.info("Date format error, use default instead " + origStartTime);
			return defaultStart;
		}
		return targetTime;

	}

	public DRMLogicUtility() {

	}
	
	//取得授權時間最久的  drm
	public static MemberDrmLog getLongestMemberDrmLogWithMapping(List<MemberBookDrmMapping> memberBookDrmMappings) {
	    List<MemberBookDrmMapping> mappingList = new ArrayList<MemberBookDrmMapping>();
	    List<MemberDrmLog> drmList = new ArrayList<MemberDrmLog>();
	    mappingList.addAll(memberBookDrmMappings);
	    if(mappingList.size()==1){}
	    if (mappingList.size() == 1) {
	        return mappingList.get(0).getMemberDrmLog();
        } else if (mappingList.size() > 1) {
        	
        	for (MemberDrmLog memberDrmLog : mappingList.stream().map(MemberBookDrmMapping::getMemberDrmLog).collect(Collectors.toList())) {
				if(memberDrmLog.getStatus() == 1){
					drmList.add(memberDrmLog);
				}
			}
        	Comparator<MemberDrmLog> readExpireTimeComparator = (Object1, Object2) -> CommonUtil.normalizeGetDateTime(Object1.getReadExpireTime()).compareTo(CommonUtil.normalizeGetDateTime(Object2.getReadExpireTime())) >= 0 ? -1 : 1;
        	return  drmList.stream().sorted(readExpireTimeComparator).findFirst().get();
        }
	    return null;
	    
    }
	
    // 取得授權時間最久的 drm
    public static MemberDrmLog getLongestMemberDrmLog(List<MemberDrmLog> memberDrmLogs) {
        
        MemberDrmLog drm = null;
        List<MemberDrmLog> drmList = new ArrayList<MemberDrmLog>();
        for (MemberDrmLog memberDrmLog : memberDrmLogs) {
			if(memberDrmLog.getStatus() == 1){
				drmList.add(memberDrmLog);
			}
		}
        
        if (drmList.size() == 1) {
            return drmList.get(0);
        } else if (drmList.size() > 1) {
        	Comparator<MemberDrmLog> readExpireTimeComparator = (Object1, Object2) -> CommonUtil.normalizeGetDateTime(Object1.getReadExpireTime()).compareTo(CommonUtil.normalizeGetDateTime(Object2.getReadExpireTime())) >= 0 ? -1 : 1;
            drm = drmList.stream().sorted(readExpireTimeComparator).findFirst().get();
        }
        
        return drm;
    }
    
    public static MemberDrmLog getLongestMemberDrmLogAndList(List<MemberDrmLog> memberDrmLogs , List<MemberDrmLog> drmList) {
        
        MemberDrmLog drm = null;
        for (MemberDrmLog memberDrmLog : memberDrmLogs) {
			if(memberDrmLog.getStatus() == 1){
				drmList.add(memberDrmLog);
			}
		}
        
        if (drmList.size() == 1) {
            return drmList.get(0);
        } else if (drmList.size() > 1) {
        	Comparator<MemberDrmLog> readExpireTimeComparator = (Object1, Object2) -> CommonUtil.normalizeGetDateTime(Object1.getReadExpireTime()).compareTo(CommonUtil.normalizeGetDateTime(Object2.getReadExpireTime())) >= 0 ? -1 : 1;
            drm = drmList.stream().sorted(readExpireTimeComparator).findFirst().get();
        }
        
        return drm;
    }

	public static DRMLogicUtility genDRMValidExpireTime(MemberBook memberBook) throws ServerException {
		DRMLogicUtility tmpDrmObj = new DRMLogicUtility();
		if (memberBook != null) {
			getBookInfo(memberBook, tmpDrmObj);
		}
		
        if (!memberBook.getIsTrial()) {
            MemberDrmLog tmpMemberDrmLog = getLongestMemberDrmLog(memberBook.getMemberDrmLogs());
            
            if (null == tmpMemberDrmLog) {
                throw new ServerException("id_err_501",
                        " DRM not exists. "
                        + "\nBookUniId=" + memberBook.getBookUniId()
                        );
            }
            
			try {
				
				if (CommonUtil.currentDate().isAfter(CommonUtil.convertOldDateToLocalDateTime(tmpMemberDrmLog.getReadExpireTime()))) {
					throw new ServerException("id_err_303", "DRM Expired:"+tmpMemberDrmLog.getReadExpireTime());
				}

			} catch (ServerException e) {
				throw e;
			} catch (Exception e) {
				logger.error("error to handler DRM expire time "+e.getMessage());
				throw new ServerException("id_err_303", "Has no permession for book.");
			}
			
            tmpDrmObj.item_id = memberBook.getItem().getId();
            if (tmpMemberDrmLog != null) {
                getDRMInfo(tmpMemberDrmLog, tmpDrmObj);
            }
            
            // Phase2 
//            java.sql.Timestamp ts = java.sql.Timestamp.valueOf(LocalDateTime.now().minus(24, ChronoUnit.HOURS));
            tmpDrmObj.dl_start_time = "1970-01-01T00:00:00+08:00";
        }

		return tmpDrmObj;
	}
	
	   public static DRMLogicUtility genDRM( MemberBook mb) throws ServerException {
	        DRMLogicUtility tmpDrmObj = new DRMLogicUtility();

	        try {
				
		        if (mb != null) {
		            getBookInfo(mb, tmpDrmObj);
		        }
		        
		        if (!mb.getIsTrial()) {
		            MemberDrmLog tmpMemberDrmLog = getLongestMemberDrmLog(mb.getMemberDrmLogs());
		            tmpDrmObj.item_id = mb.getItem().getId();
		            // Phase2 
		            java.sql.Timestamp ts = java.sql.Timestamp.valueOf(LocalDateTime.now().minus(24, ChronoUnit.HOURS));
		            tmpDrmObj.dl_start_time = "1970-01-01T00:00:00+08:00";
		            if (tmpMemberDrmLog != null) {
		                getDRMInfo(tmpMemberDrmLog, tmpDrmObj);
		            }
		        }
	        } catch (Exception e) {
	        	tmpDrmObj = null;
	        	logger.error(e);
			}
	        return tmpDrmObj;
	    }


	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		buf.append("<DRM>\n");
		buf.append("<DRMType>" + this.drm_type + "</DRMType>\n");
		buf.append("<DownloadLimit>\n");
		buf.append("	<StartTime>" + this.dl_start_time + "</StartTime>\n");
		buf.append("	<EndTime>" + this.dl_end_time + "</EndTime>\n");
		buf.append("</DownloadLimit>\n");
		buf.append("<License>\n");
		buf.append("	<LicenseType>" + this.license_type + "</LicenseType>\n");
		buf.append("	<LicenseGroup>" + this.license_group + "</LicenseGroup>\n");
		buf.append("	<Authorized>" + this.isAuthorized + "</Authorized>\n");
		buf.append("</License>\n");
		buf.append("<Item>" + this.item_id + "</Item>\n");
		buf.append("<Member>" + member_id + "</Member>\n");
		buf.append("<ReadLimit>\n");
		buf.append("	<StartTime>" + this.read_start_time + "</StartTime>\n");
		buf.append("	<EndTime>" + this.read_end_time + "</EndTime>\n");
		buf.append("	<Duration>" + this.duration + "</Duration>\n");
		buf.append("	<GracePeriod>" + this.grace_period + "</GracePeriod>\n");
		buf.append("	<ChapterPrice>" + this.chapter_price + "</ChapterPrice>\n");
		buf.append("	<ChapterLimit>" + this.chapter_limit + "</ChapterLimit>\n");
		buf.append("</ReadLimit>\n");
		buf.append("<FunctionLimit>\n");
		buf.append("	<SocialShare>" + this.isShare + "</SocialShare>\n");
		buf.append("	<MakePublic>" + this.isPublic + "</MakePublic>\n");
		buf.append("	<Like>" + this.isLike + "</Like>\n");
		buf.append("	<Annotation>" + this.hasAnnotation + "</Annotation>\n");
		buf.append("	<Bookmark>" + this.hasBookmark + "</Bookmark>\n");
		buf.append("	<Note>" + this.hasNote + "</Note>\n");
		buf.append("</FunctionLimit>\n");
		buf.append("<Checksum>" + this.checksum + "</Checksum>\n");
		buf.append("<AuthID>" + this.auth_id + "</AuthID>\n");
		buf.append("</DRM>\n");
		return buf.toString();
	}

	private static void getBookInfo(MemberBook mb, DRMLogicUtility tmpDrmObj) {
		Date start_time = mb.getStartReadTime();
		Date twoHourBack = CommonUtil.nextNMinutes(-120);

//		tmpDrmObj.read_start_time = (null != start_time) ? CommonUtil.fromDateToString(twoHourBack) :
//			CommonUtil.fromDateToString(java.sql.Timestamp.valueOf(LocalDateTime.now().minus(24, ChronoUnit.HOURS))); // member_book,start_read_time
		tmpDrmObj.checksum = mb.getBookFile().getChecksum(); // book_file,checksum
		ItemInfo info = mb.getBookFile().getItem().getInfo();
		BaseBookInfo tmpBookInfo = (BaseBookInfo) info;
		tmpDrmObj.isShare = tmpBookInfo.getShare_flag(); // basebookInfo,share_flag
		tmpDrmObj.isPublic = tmpBookInfo.getPublic_flag(); // basebookInfo,public_flag
		tmpDrmObj.isLike = tmpBookInfo.getLike_flag(); // basebookInfo,like_flag
		tmpDrmObj.hasAnnotation = tmpBookInfo.getAnnotation_flag(); // basebookInfo,annotation_flag
		tmpDrmObj.hasBookmark = tmpBookInfo.getBookmark_flag(); // basebookInfo,bookmark_flag
		tmpDrmObj.hasNote = tmpBookInfo.getNote_flag(); // basebookInfo,note_flag
	}

	private static void getDRMInfo(MemberDrmLog tmpMemberDrmLog, DRMLogicUtility tmpDrmObj) {
		tmpDrmObj.dl_end_time = tmpMemberDrmLog.getDownloadExpireTime(); // member_drm_log,download_expire_time
		tmpDrmObj.read_start_time = tmpMemberDrmLog.getReadStartTime();
		tmpDrmObj.read_end_time = tmpMemberDrmLog.getReadExpireTime();
		tmpDrmObj.license_type = String.valueOf(tmpMemberDrmLog.getType()); // member_drm_log,type
		tmpDrmObj.item_id = tmpMemberDrmLog.getItem().getId(); // member_drm_log,item_id
		tmpDrmObj.member_id = tmpMemberDrmLog.getMember().getBmemberId(); // bmember_id
		tmpDrmObj.auth_id = String.valueOf(tmpMemberDrmLog.getId()); // member_drm_log,id
		tmpDrmObj.drm_type = String.valueOf(tmpMemberDrmLog.getType());

		// fix wrong date time format from books server
		tmpDrmObj.dl_end_time = CommonUtil.normalizeDateTime(tmpDrmObj.dl_end_time);
		tmpDrmObj.read_start_time = CommonUtil.normalizeDateTime(tmpDrmObj.read_start_time);
		tmpDrmObj.read_end_time = CommonUtil.normalizeDateTime(tmpDrmObj.read_end_time);
		fixStartTime(tmpDrmObj);
	}

	public String toJson() {
		Gson gson = new Gson();
		String ret = gson.toJson(this);
		return ret;
	}

	public String getRead_end_time() {
		return read_end_time;
	}

	public void setRead_end_time(String read_end_time) {
		this.read_end_time = read_end_time;
	}
	
}