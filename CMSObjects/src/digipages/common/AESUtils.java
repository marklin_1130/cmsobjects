package digipages.common;

import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils {

    private static String keyString = "1469605896922355";

    private static String ivString = "BATMANVSSUPERMAN";

    public static String encryptAES(String content) {
        String encrypt = "";
        try {
            byte[] byteContent = content.getBytes("UTF-8");

            // 注意，为了能与 iOS 统一
            // 这里的 key 不可以使用 KeyGenerator、SecureRandom、SecretKey 生成
            byte[] enCodeFormat = keyString.getBytes();
            SecretKeySpec secretKeySpec = new SecretKeySpec(enCodeFormat, "AES");

            byte[] initParam = ivString.getBytes();
            IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);

            // 指定加密的算法、工作模式和填充方式
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            byte[] encryptedBytes = cipher.doFinal(byteContent);

            // 同样对加密后数据进行 base64 编码
            Encoder encoder = Base64.getEncoder();
            encrypt = encoder.encodeToString(encryptedBytes);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return encrypt;
    }

    public static String decryptAES(String content) throws Exception {

        // base64 解码
        Decoder decoder = Base64.getDecoder();
        byte[] encryptedBytes = decoder.decode(content);

        byte[] enCodeFormat = keyString.getBytes();
        SecretKeySpec secretKey = new SecretKeySpec(enCodeFormat, "AES");

        byte[] initParam = ivString.getBytes();
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);

        byte[] result = cipher.doFinal(encryptedBytes);

        return new String(result, "UTF-8");
    }

    public static String encrypt(String dataString) {

        String encrypt = "";
        try {
            byte[] key = keyString.getBytes("UTF-8");
            byte[] iv = ivString.getBytes("UTF-8");
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
            SecretKeySpec newKey = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            int blockSize = cipher.getBlockSize();

            byte[] inputBytes = dataString.getBytes();
            int byteLength = inputBytes.length;
            if (byteLength % blockSize != 0) {
                byteLength = byteLength + (blockSize - (byteLength % blockSize));
            }

            byte[] paddedBytes = new byte[byteLength];
            System.arraycopy(inputBytes, 0, paddedBytes, 0, inputBytes.length);

            cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
            byte[] results = cipher.doFinal(paddedBytes);
            String resultString = Base64.getEncoder().encodeToString(results);
            encrypt = resultString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encrypt;
    }

    public static String decrypte(String dataString) {
        String decrypte = "";
        try {

            byte[] key = keyString.getBytes("UTF-8");
            byte[] iv = ivString.getBytes("UTF-8");
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
            SecretKeySpec newKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
            byte[] input = Base64.getDecoder().decode(dataString);
            byte[] results2 = cipher.doFinal(input);
            decrypte = new String(results2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decrypte;
    }

    public String encryptUrlencode(String dataString) {

        String encrypt = "";
        try {

            byte[] key = keyString.getBytes("UTF-8");
            byte[] iv = ivString.getBytes("UTF-8");
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
            SecretKeySpec newKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            int blockSize = cipher.getBlockSize();

            byte[] inputBytes = dataString.getBytes();
            int byteLength = inputBytes.length;
            if (byteLength % blockSize != 0) {
                byteLength = byteLength + (blockSize - (byteLength % blockSize));
            }

            byte[] paddedBytes = new byte[byteLength];
            System.arraycopy(inputBytes, 0, paddedBytes, 0, inputBytes.length);

            cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
            byte[] results = cipher.doFinal(paddedBytes);
            String resultString = Base64.getUrlEncoder().encodeToString(results);
            encrypt = resultString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encrypt;
    }

    public String decrypteUrldecode(String dataString) {
        String decrypte = "";
        try {

            byte[] key = keyString.getBytes("UTF-8");
            byte[] iv = ivString.getBytes("UTF-8");
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
            SecretKeySpec newKey = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
            byte[] input = Base64.getUrlDecoder().decode(dataString);
            byte[] results2 = cipher.doFinal(input);
            decrypte = new String(results2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decrypte;
    }

}
