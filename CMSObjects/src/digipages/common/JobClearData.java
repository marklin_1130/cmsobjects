package digipages.common;

import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import digipages.CommonJobHandler.ScheduleJobManager;
import model.ScheduleJob;

public class JobClearData implements Job {

    private static DPLogger logger = DPLogger.getLogger(JobClearData.class.getName());

    // every 30 sec check
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            // job recover

            JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();

            @SuppressWarnings("unchecked")
            EntityManager postgres = (EntityManager) dataMap.get("postgres");
            ScheduleJobManager.cleanOldJobs(postgres);

            // logger.info("JobRecover Scan at " + CommonUtil.currentDateTime());
        } catch (Exception e) {
            logger.error(e);
            ;
        }
    }

}