package digipages.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;

public class Compressor {
	private Compressor()
	{
		
	}
	public static byte[] compressString(String inputString) throws IOException {
	    byte[] input = inputString.getBytes("UTF-8");
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    DeflaterOutputStream deflater = new DeflaterOutputStream(out);
	    deflater.write(input, 0, input.length);
	    deflater.flush();
	    deflater.close();
		return out.toByteArray();
	}

	public static String unCompressString(byte array[]) throws IOException
	{
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
		InflaterOutputStream inf = new InflaterOutputStream(out);
		inf.write(array);
		inf.flush();
		inf.close();
		byte []tmp = out.toByteArray();
		return new String(tmp,"UTF-8");
	}

}
