package digipages.common;

import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import digipages.CommonJobHandler.JobRecoverHandler;
import digipages.CommonJobHandler.JobTransferFileCheckHandler;
import model.ScheduleJob;

public class JobTransferFileCheckInternal implements Job {

	private static DPLogger logger =DPLogger.getLogger(JobTransferFileCheckInternal.class.getName());
	
	// every 30 min check
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			// job recover

			
			JobDataMap dataMap = arg0.getJobDetail().getJobDataMap();
			
			
			@SuppressWarnings("unchecked")
			BlockingQueue<ScheduleJob> bq=(BlockingQueue<ScheduleJob>) dataMap.get("JobQueue");
			EntityManager postgres = (EntityManager) dataMap.get("postgres");
			String env = (String) dataMap.get("env");
			
			JobTransferFileCheckHandler handler = new JobTransferFileCheckHandler(bq);
			
			handler.jobTransferFileCheck(env, postgres);


			logger.info("jobTransferFileCheck Scan at " + CommonUtil.currentDateTime());
		} catch (Exception e) {
			logger.error(e);;
		}
	}


}