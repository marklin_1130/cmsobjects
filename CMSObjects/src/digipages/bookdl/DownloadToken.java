package digipages.bookdl;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.boon.json.JsonFactory;

import com.google.gson.Gson;

import digipages.common.DPLogger;
import digipages.common.DRMLogicUtility;
import digipages.dev.utility.Enc01;
import digipages.exceptions.ServerException;
import model.MemberBook;

/**
 * download Token collect needed info for download (BoonEncrypt can depend on it, and not use db access)
 * 
 * @author ericjou
 *
 */
public class DownloadToken {
	
	private static final DPLogger logger = DPLogger.getLogger(DownloadToken.class.getName());
	
	private String encryptType =Enc01.EncryptType;
	private int bookFileId = 0;
	private String basePath = "";
	private boolean needEncrypt = false;
	private boolean isWeb=false;
	private String drmJson;

	public boolean isNeedEncrypt() {
		return needEncrypt;
	}

	public void setNeedEncrypt(boolean needEncrypt) {
		this.needEncrypt = needEncrypt;
	}

	public String getDrmContent() {
		DRMLogicUtility drm = DRMLogicUtility.fromJson(drmJson);
		return drm.toString();
	}



	public int getBookFileId() {
		return bookFileId;
	}

	public void setBookFileId(int bookFileId) {
		this.bookFileId = bookFileId;
	}


	public DownloadToken(String encrypted) {

	}

	public DownloadToken() {

	}

	public static DownloadToken fromString(String decodeStr) throws ServerException {
		DownloadToken ret = null;

		try {
				ret = decryptDownloadToken(decodeStr);
		} catch (Exception ex) {
//			logger.error("Eric Debug: decode fail "+ decodeStr,ex);
			ServerException sex = new ServerException("id_err_205", "DownloadToken Decrypt Fail:" + ex.getMessage());
			throw sex;
		}

		return ret;
	}

	private static DownloadToken decryptDownloadToken(String srcStr) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		String salt = srcStr.substring(0, 6);
		String Key = "*3:9.MumWx" + salt; // Key Size=56

		byte[] KeyData = Key.getBytes();
		SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");
		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(Cipher.DECRYPT_MODE, KS);
		String data = srcStr.substring(6);
		byte[] jsonary = cipher.doFinal(Base64.getDecoder().decode(data));
		String jsonStr = new String(jsonary, "UTF-8");
		return JsonFactory.fromJson(jsonStr, DownloadToken.class);
	}

	public String toEncStr() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		String salt = UUID.randomUUID().toString().substring(0, 6);
		return toEncStr(salt);
	}
	
	public String toEncStr(String salt) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		String jsonStr = this.toJsonString();

		String Key = "*3:9.MumWx" + salt; // Key Size=56 (448)

		byte[] KeyData = Key.getBytes();
		SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");
		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(Cipher.ENCRYPT_MODE, KS);
		byte[] enced = cipher.doFinal(jsonStr.getBytes("UTF-8"));
		String base64 = Base64.getEncoder().encodeToString(enced);
		return salt + base64;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String toJsonString() {
		String ret =JsonFactory.toJson(this);
		Gson gson = new Gson();
		String ret2=gson.toJson(this);
		return ret2; 
	}

	public String toBase64String() throws ServerException {
		Encoder encoder = Base64.getEncoder();
		String base64Str = "";

		try {
			base64Str = encoder.encodeToString(toJsonString().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error(e);;
			throw new ServerException("id_err_999", e.getMessage());
		}

		return base64Str;
	}

	public static <T> T fromBase64String(String base64, Class<T> class1) throws UnsupportedEncodingException {
		Decoder decoder = Base64.getDecoder();
		byte[] jsonary = decoder.decode(base64);
		String json = new String(jsonary, "UTF-8");
		return JsonFactory.fromJson(json, class1);
	}

	public String getEncryptType() {
		// real
		return encryptType;
	}

	public void setEncryptType(String encryptType) {
		this.encryptType = encryptType;
	}

	public boolean isWeb() {
		return isWeb;
	}

	public void setWeb(boolean isWeb) {
		this.isWeb = isWeb;
	}

	public void setDrmJson(String drmJson) {
		this.drmJson=drmJson;
		
	}
	
//	public static DownloadToken genDownloadToken(EntityManager postgres, long member_id, String bookUniId, boolean isWeb) throws ServerException {
//	DownloadToken downloadToken = new DownloadToken();
//	Member member = Member.getMemberByMemberId(postgres, (int) member_id);
//
//	MemberBookFinder finder = new MemberBookFinder(bookUniId);
//	finder.find(postgres, member);
//	MemberBook memberBook = finder.getMemberBook();
//
//	long book_id = memberBook.getBookFile().getId();
//	// make drm file
//	DRMLogicUtility drm = DRMLogicUtility.genDRM(postgres, member_id, book_id);
//	// encrypt
//	downloadToken.setBasePath(memberBook.getBookFile().getFileLocation());
//	downloadToken.setBookFileId((int) book_id);
//	downloadToken.setDrmJson(drm.toJson());
//	downloadToken.setEncryptType(memberBook.getEncryptType());
//	downloadToken.setNeedEncrypt(!memberBook.getEncryptType().equals(MemberBook.EncryptTypeNone) ? true : false);
//	downloadToken.setWeb(isWeb);
//
//	return downloadToken;
//}

public static DownloadToken genDownloadToken(MemberBook memberBook, boolean isWeb) throws ServerException {
	DownloadToken downloadToken = new DownloadToken();

	// make drm file
	DRMLogicUtility drm = DRMLogicUtility.genDRMValidExpireTime(memberBook);
	// encrypt
	downloadToken.setBasePath(memberBook.getBookFile().getFileLocation());
	downloadToken.setBookFileId((int) memberBook.getBookFile().getId());
	downloadToken.setDrmJson(drm.toJson());
	downloadToken.setEncryptType(memberBook.getEncryptType());
	downloadToken.setNeedEncrypt(!memberBook.getEncryptType().equals(MemberBook.EncryptTypeNone) ? true : false);
	downloadToken.setWeb(isWeb);

	return downloadToken;
}

}