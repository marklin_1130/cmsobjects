package digipages.pgconverter;


import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import digipages.common.DPLogger;

/**
 * Created by study on 11/15/14.
 */
@Converter(autoApply = true)
public class PGIntArrayConverter implements AttributeConverter<Set<Integer>, Object> {
	
	private static DPLogger logger =DPLogger.getLogger(PGIntArrayConverter.class.getName());
	
    @Override
    public PGSQLIntArray convertToDatabaseColumn(Set<Integer> attribute) {
        if (attribute == null || attribute.isEmpty()) {
            return null;
        }
        Integer[] rst = new Integer[attribute.size()];
        return new PGSQLIntArray(attribute.toArray(rst));
    }

    @Override
    public Set<Integer> convertToEntityAttribute(Object dbData) {
    	Set<Integer> rst = new HashSet<>();
    	if (dbData==null)
    		return rst;
    	if (dbData instanceof PGSQLIntArray)
    	{
    		
    		PGSQLIntArray x = (PGSQLIntArray)dbData;
    		
    		try {
				dbData=x.getArray();
			} catch (SQLException e) {
				logger.error(e);;
			}
    	}
    	
    	Integer[] elements = ((Integer[]) dbData);
        for (Integer element : elements) {
            rst.add(element);
        }
        return rst;
    }
}
