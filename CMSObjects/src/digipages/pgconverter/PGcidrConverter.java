package digipages.pgconverter;

import java.sql.SQLException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import digipages.common.DPLogger;

@Converter
public class PGcidrConverter implements AttributeConverter<String,PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGcidrConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(String ipAddress) {
		 if (ipAddress == null) {
	            return null;
        } else if (ipAddress instanceof String) {
        	PGobject po = new PGobject();
        	po.setType("cidr");
        	try {
				po.setValue(ipAddress);
			} catch (SQLException e) {
				logger.error(e);;
			}
        	return po;
        }
		return null;

	}

	@Override
	public String convertToEntityAttribute(PGobject dataValue) {
		if (dataValue==null)
			return null;
		return dataValue.getValue();
	}



}
