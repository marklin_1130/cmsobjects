package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import digipages.common.DPLogger;
import digipages.dto.ChapterInfo;
@Converter (autoApply=true)
public class PGChapterInfoConverter implements AttributeConverter<ChapterInfo, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGChapterInfoConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(ChapterInfo info) {
		  try {
	            PGobject po = new PGobject();
	            po.setType("jsonb");
	            String iiJson = toJson(info);
	            po.setValue(iiJson);
	            return po;
	        } catch (Exception e) {
	        	logger.error(e);;
	            return null;
	        }
	}

	@Override
	public ChapterInfo convertToEntityAttribute(PGobject arg0) {
		if (null==arg0)
			return null;
		String jsonStr = arg0.getValue();
		ChapterInfo itemInfo=null;
		
		itemInfo=fromJson(jsonStr,ChapterInfo.class);
		
		return (itemInfo);
	}
	
}
