package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.boon.json.JsonException;
import org.postgresql.util.PGobject;

import digipages.common.DPLogger;

@Converter
public class PGGenericJsonContentConverter implements AttributeConverter<Object, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGGenericJsonContentConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(Object genericJsonObj) {
		try {
			PGobject po = new PGobject();
			po.setType("jsonb");
			String iiJson = toJson(genericJsonObj);
			po.setValue(iiJson);
			return po;
		} catch (JsonException e) {
			logger.error(e);;
			return null;
		} catch (SQLException e) {
			logger.error(e);;
			return null;
		}
	}

	@Override
	public Object convertToEntityAttribute(PGobject pgObject) {
		Object tmpObj=null;
		try {
		String jsonStr = pgObject.getValue();
		if (jsonStr.startsWith("["))
			tmpObj = (Object)fromJson(jsonStr, List.class);
		else
			tmpObj = (Object)fromJson(jsonStr, Map.class);
		} catch (Exception e)
		{
			// don't report error
		}
		return (tmpObj);
	}
}