package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import digipages.common.DPLogger;
import model.BookInfo;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;
@Converter (autoApply=true)
public class PGItemInfoConverter implements AttributeConverter<ItemInfo, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGItemInfoConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(ItemInfo itemInfo) {
		  try {
	            PGobject po = new PGobject();
	            po.setType("jsonb");
	            String iiJson = toJson(itemInfo);
	            po.setValue(iiJson);
	            return po;
	        } catch (Exception e) {
	        	logger.error(e);;
	            return null;
	        }
	}

	@Override
	public ItemInfo convertToEntityAttribute(PGobject arg0) {
	    ItemInfo itemInfo=null;
	    String jsonStr = "";
	    try {
            
    		if (null==arg0)
    			return null;
    		jsonStr = arg0.getValue();
    		
    		
    		@SuppressWarnings("unchecked")
    		Map<String,String> tmpObj=fromJson(jsonStr, Map.class);
    		String type =tmpObj.get("type").toLowerCase();
    		String itemIdPrefix = "";
    		if(tmpObj.get("item")!=null) {
    		    itemIdPrefix = tmpObj.get("item").substring(0, 3);
    		}
    		
    		if (type.equals("magazine") || (type.equals("serial") && itemIdPrefix.equalsIgnoreCase("E06")))
    		{
    			itemInfo=fromJson(jsonStr,MagInfo.class);
    		}else if (type.equals("book") || (type.equals("serial") && itemIdPrefix.equalsIgnoreCase("E05"))) //(type.equals("book") || type.equals("trial") ||type.equals("serial"))
    		{
    			itemInfo=fromJson(jsonStr,BookInfo.class);
    		}else if (type.equalsIgnoreCase("mediabook")||type.equalsIgnoreCase("audiobook") || itemIdPrefix.equalsIgnoreCase("E07")|| itemIdPrefix.equalsIgnoreCase("E08")) {
    			itemInfo=fromJson(jsonStr,MediaBookInfo.class);
    		}else {
    		    itemInfo=fromJson(jsonStr,BookInfo.class);
    		}
		
        } catch (Exception e) {
            logger.cmslogerror("convertToEntityAttribute", jsonStr);
        }
		return (itemInfo);
	}
	
}
