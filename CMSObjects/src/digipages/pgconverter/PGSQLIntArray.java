package digipages.pgconverter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;


public class PGSQLIntArray implements java.sql.Array {
	

    private final Integer[] intArray;
    private final String stringValue;

    /**
     * Initializing constructor
     * @param intArray
     */
    public PGSQLIntArray(Integer[] intArray) {
        this.intArray = intArray;
        this.stringValue = intArrayToPostgreSQLIntArray(this.intArray);

    }

    @Override
    public String toString() {
        return stringValue;
    }

    private static final String NULL = "NULL";

    /**
     * This static method can be used to convert an string array to string representation of PostgreSQL text array.
     * @param a source String array
     * @return string representation of a given text array
     */
    public static String intArrayToPostgreSQLIntArray(Integer[] intArray) {

        if ( intArray == null ) {
            return NULL;
        } 
        
        StringBuilder sb = new StringBuilder();
        boolean hasElem=false;
        
        sb.append("{");
        for (Integer tmpInt:intArray)
        {
        	sb.append(tmpInt).append(",");
        	hasElem=true;
        }
        // cut tail ,
        if (hasElem)
        	sb.setLength(sb.length()-1);
        
        sb.append("}");
        return sb.toString();
    }


    @Override
    public Object getArray() throws SQLException {
        return intArray == null ? null : Arrays.copyOf(intArray, intArray.length);
    }

    @Override
    public Object getArray(Map<String, Class<?>> map) throws SQLException {
        return getArray();
    }

    @Override
    public Object getArray(long index, int count) throws SQLException {
        return intArray == null ? null : Arrays.copyOfRange(intArray, (int)index, (int)index + count);
    }

    @Override
    public Object getArray(long index, int count, Map<String, Class<?>> map) throws SQLException {
        return getArray(index, count);
    }

    @Override
    public int getBaseType() throws SQLException {
        return java.sql.Types.INTEGER;
    }

    @Override
    public String getBaseTypeName() throws SQLException {
        return "integer";
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ResultSet getResultSet(Map<String, Class<?>> map) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ResultSet getResultSet(long index, int count) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ResultSet getResultSet(long index, int count, Map<String, Class<?>> map) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void free() throws SQLException {
    }
}