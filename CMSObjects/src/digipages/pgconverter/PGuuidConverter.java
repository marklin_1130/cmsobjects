package digipages.pgconverter;

import java.sql.SQLException;
import java.util.UUID;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;


@Converter
public class PGuuidConverter implements AttributeConverter<UUID,Object> {

	@Override
	public Object convertToDatabaseColumn(UUID uuid) {
	       PostgresUuid object = new PostgresUuid();
	        object.setType("uuid");
	        try {
	            if (uuid == null) {
	                object.setValue(null);
	            } else {
	                object.setValue(uuid.toString());
	            }
	        } catch (SQLException e) {
	            throw new IllegalArgumentException("Error when creating Postgres uuid", e);
	        }
	        return object;

	}

	@Override
	public UUID convertToEntityAttribute(Object uuid) {
		if (uuid ==null)
			return null;
		if (uuid instanceof PGobject)
		{
			return UUID.fromString(((PGobject)uuid).getValue());
		
		}
		return (UUID)uuid;
	}


	public class PostgresUuid extends PGobject implements Comparable<Object> {

	    private static final long serialVersionUID = 1L;

	    @Override
	    public int compareTo(Object arg0) {
	        return 0;
	    }

	}
}
