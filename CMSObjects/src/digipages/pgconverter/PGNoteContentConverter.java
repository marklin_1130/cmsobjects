package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import java.sql.SQLException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.boon.json.JsonException;
import org.postgresql.util.PGobject;

import digipages.common.DPLogger;
import model.NoteContent;
@Converter
public class PGNoteContentConverter implements AttributeConverter<NoteContent, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGNoteContentConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(NoteContent itemInfo) {
		  try {
	            PGobject po = new PGobject();
	            po.setType("jsonb");
	            po.setValue(toJson(itemInfo));
	            return po;
	        } catch (JsonException e) {
	        	logger.error(e);;
	            return null;
	        } catch (SQLException e) {
	        	logger.error(e);;
	            return null;
	        }
	}

	@Override
	public NoteContent convertToEntityAttribute(PGobject arg0) {
		  return (fromJson(arg0.getValue(),NoteContent.class));
	}
	
}
