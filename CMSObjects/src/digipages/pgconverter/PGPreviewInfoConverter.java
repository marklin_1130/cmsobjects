package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import digipages.common.DPLogger;
import digipages.dto.PreviewInfo;
@Converter (autoApply=true)
public class PGPreviewInfoConverter implements AttributeConverter<PreviewInfo, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGPreviewInfoConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(PreviewInfo itemInfo) {
		  try {
	            PGobject po = new PGobject();
	            po.setType("jsonb");
	            String iiJson = toJson(itemInfo);
	            po.setValue(iiJson);
	            return po;
	        } catch (Exception e) {
	        	logger.error(e);;
	            return null;
	        }
	}

	@Override
	public PreviewInfo convertToEntityAttribute(PGobject arg0) {
		if (null==arg0)
			return null;
		String jsonStr = arg0.getValue();
		PreviewInfo itemInfo=fromJson(jsonStr,PreviewInfo.class);
		
		return (itemInfo);
	}
	
}
