package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import digipages.common.DPLogger;
import model.AttachInfo;
@Converter (autoApply=true)
public class PGAttachInfoConverter implements AttributeConverter<AttachInfo, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGAttachInfoConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(AttachInfo info) {
		  try {
	            PGobject po = new PGobject();
	            po.setType("jsonb");
	            String iiJson = toJson(info);
	            po.setValue(iiJson);
	            return po;
	        } catch (Exception e) {
	        	logger.error(e);;
	            return null;
	        }
	}

	@Override
	public AttachInfo convertToEntityAttribute(PGobject arg0) {
		if (null==arg0)
			return null;
		String jsonStr = arg0.getValue();
		AttachInfo itemInfo=null;
		itemInfo=fromJson(jsonStr,AttachInfo.class);
		
		return (itemInfo);
	}
	
}
