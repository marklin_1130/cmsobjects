package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import java.sql.SQLException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.boon.json.JsonException;
import org.postgresql.util.PGobject;

import digipages.common.DPLogger;

@Converter
public class PGObjectConverter implements AttributeConverter<Object, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGObjectConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(Object object) {
		 try {
	            PGobject po = new PGobject();
	            po.setType("jsonb");
	            po.setValue(toJson(object));
	            return po;
	        } catch (JsonException e) {
	        	logger.error(e);;
	            return null;
	        } catch (SQLException e) {
	        	logger.error(e);;
	            return null;
	        }
	}

	@Override
	public Object convertToEntityAttribute(PGobject arg0) {
		return fromJson(arg0.getValue());
	}

}
