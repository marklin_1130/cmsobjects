package digipages.pgconverter;

import static org.boon.json.JsonFactory.fromJson;
import static org.boon.json.JsonFactory.toJson;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import digipages.common.DPLogger;
import model.SubtitleConvertData;
@Converter (autoApply=true)
public class PGSubtitleConvertInfoConverter implements AttributeConverter<SubtitleConvertData, PGobject> {

	private static DPLogger logger =DPLogger.getLogger(PGSubtitleConvertInfoConverter.class.getName());
	
	@Override
	public PGobject convertToDatabaseColumn(SubtitleConvertData itemInfo) {
		  try {
	            PGobject po = new PGobject();
	            po.setType("jsonb");
	            String iiJson = toJson(itemInfo);
	            po.setValue(iiJson);
	            return po;
	        } catch (Exception e) {
	        	logger.error(e);;
	            return null;
	        }
	}

	@Override
	public SubtitleConvertData convertToEntityAttribute(PGobject arg0) {
		if (null==arg0)
			return null;
		String jsonStr = arg0.getValue();
		SubtitleConvertData itemInfo=fromJson(jsonStr,SubtitleConvertData.class);
		
		return (itemInfo);
	}
	
}
