package digipages.pgconverter;


import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import digipages.common.DPLogger;

/**
 * Created by study on 11/15/14.
 */
@Converter(autoApply = true)
public class PGArrayConverter implements AttributeConverter<Set<String>, Object> {
	
	private static DPLogger logger =DPLogger.getLogger(PGArrayConverter.class.getName());
	
    @Override
    public PGSQLTxtArray convertToDatabaseColumn(Set<String> attribute) {
        if (attribute == null || attribute.isEmpty()) {
            return null;
        }
        String[] rst = new String[attribute.size()];
        return new PGSQLTxtArray(attribute.toArray(rst));
    }

    @Override
    public Set<String> convertToEntityAttribute(Object dbData) {
    	Set<String> rst = new HashSet<>();
    	if (dbData==null)
    		return rst;
    	if (dbData instanceof PGSQLTxtArray)
    	{
    		
    		PGSQLTxtArray x = (PGSQLTxtArray)dbData;
    		
    		try {
				dbData=x.getArray();
			} catch (SQLException e) {
				logger.error(e);;
			}
    	}
        String[] elements = ((String[]) dbData);
        for (String element : elements) {
            rst.add(element);
        }
        return rst;
    }
}
