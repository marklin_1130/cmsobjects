package digipages.snps;

public class SNPSPublishBoardcastPayload {

    private String appKey;
    private String payload;
	public String getAppKey() {
		return appKey;
	}
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
    
}
