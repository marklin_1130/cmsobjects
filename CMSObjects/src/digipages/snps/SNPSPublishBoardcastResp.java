package digipages.snps;

public class SNPSPublishBoardcastResp {

    private String returnCode;
    private String returnMsg;
    private String total;
    private String version;
    private String backendCode;
    private String awsCode;
    public String getReturnCode() {
        return returnCode;
    }
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
    public String getReturnMsg() {
        return returnMsg;
    }
    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }
    public String getTotal() {
        return total;
    }
    public void setTotal(String total) {
        this.total = total;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
    public String getBackendCode() {
        return backendCode;
    }
    public void setBackendCode(String backendCode) {
        this.backendCode = backendCode;
    }
    public String getAwsCode() {
        return awsCode;
    }
    public void setAwsCode(String awsCode) {
        this.awsCode = awsCode;
    }
    
    
}
