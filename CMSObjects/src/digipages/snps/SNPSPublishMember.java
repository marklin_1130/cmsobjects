package digipages.snps;

public class SNPSPublishMember {

    private String appKey;
    private String cid;
    private String message;
    private String[] apps;
    
    public String getAppKey() {
        return appKey;
    }
    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getCid() {
        return cid;
    }
    public void setCid(String cid) {
        this.cid = cid;
    }
    public String[] getApps() {
        return apps;
    }
    public void setApps(String... apps) {
        this.apps = apps;
    }
}
