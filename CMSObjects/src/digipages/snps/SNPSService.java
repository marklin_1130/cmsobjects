package digipages.snps;

public interface SNPSService {

    void DeviceUpdate(SNPSDeviceUpdate aSNPSDeviceUpdate);

    SNPSPublishBoardcastResp boardCast(SNPSPublishBoardcast aSNPSPublishBoardcast);

    void sendSNSByMember(SNPSPublishMember aSNPSPublish);
    
    SNPSPublishSimpleResp sendSNSByToken(SNPSPublishSimple aSNPSPublishSimple);

	SNPSPublishBoardcastPayloadResp boardcastPayload(SNPSPublishBoardcastPayload aSNPSPublishBoardcastPayload);
}
