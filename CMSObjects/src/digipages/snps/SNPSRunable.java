package digipages.snps;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Base64;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class SNPSRunable implements Runnable {

    private static final Logger logger = LogManager.getLogger(SNPSRunable.class);

    private SNPSDeviceUpdate snpsDeviceUpdate;
    public String snpsaccount = "";
    public String snpspw = "";
    public String snpsDeviceUpdateUrl = "";

    public SNPSRunable(SNPSDeviceUpdate aSNPSDeviceUpdate) {
        this.snpsDeviceUpdate = aSNPSDeviceUpdate;
    }

    @Override
    public void run() {

        try {

            final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();

            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null,new TrustSelfSignedStrategy()).build();

            // Allow TLSv1 protocol only, use NoopHostnameVerifier to trust self-singed cert
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
                    new String[] { "TLSv1" }, null, new NoopHostnameVerifier());

            //do not set connection manager
            CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setSSLSocketFactory(sslsf).build();
            
            HttpPost httpPost = new HttpPost(snpsDeviceUpdateUrl);
            
            
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-Type", "application/json");
            String encoding = Base64.getEncoder().encodeToString((snpsaccount + ":" + snpspw).getBytes("UTF-8"));
            httpPost.setHeader("Authorization", "Basic " + encoding);

            String rdata = new Gson().toJson(snpsDeviceUpdate);
            StringEntity entity = new StringEntity(rdata);
            httpPost.setEntity(entity);

            IOException ioex = null;
            CloseableHttpResponse response_t = null;
            int cnt = 0;
            boolean success = false;
//            while (cnt < 5 && success == false) {
                try {
                    response_t = httpclient.execute(httpPost);
                    logger.debug("SNS-device req:"+rdata);
                    HttpEntity responseEntity = response_t.getEntity();
                    if (responseEntity != null) {

                        String respJson = EntityUtils.toString(responseEntity, "UTF-8");
                        logger.debug("SNS-device resp:"+respJson);
                        Type stringStringMap = new TypeToken<Map<String, String>>() {}.getType();
                        Map<String, String> map = new Gson().fromJson(respJson, stringStringMap);
                        String returnCode = map.get("returnCode");
                        if (StringUtils.isNotBlank(returnCode) || "000".equalsIgnoreCase(returnCode)) {
                            success = true;
                        }

                    }
                    response_t.close();
                    httpPost.releaseConnection();
                } catch (IOException ex) {
                    logger.error("SNS-device error:連線失敗:"+ex.getMessage());
                    ioex = ex;
                }
                cnt++;
//            }

        } catch (Exception e) {
            logger.error(e);
        }

    }

}
