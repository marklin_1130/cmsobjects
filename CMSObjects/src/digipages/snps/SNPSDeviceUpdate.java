package digipages.snps;

public class SNPSDeviceUpdate {

    private String appKey;
    private String deviceToken;
    private String cid;
    private String userCanceled;
    private String enable="1";
    
    public String getAppKey() {
        return appKey;
    }
    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
    public String getDeviceToken() {
        return deviceToken;
    }
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
    public String getCid() {
        return cid;
    }
    public void setCid(String cid) {
        this.cid = cid;
    }
    public String getUserCanceled() {
        return userCanceled;
    }
    public void setUserCanceled(String userCanceled) {
        this.userCanceled = userCanceled;
    }
    public String getEnable() {
        return enable;
    }
    public void setEnable(String enable) {
        this.enable = enable;
    }
    
}
