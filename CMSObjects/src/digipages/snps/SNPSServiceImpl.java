package digipages.snps;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Base64;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class SNPSServiceImpl implements SNPSService {

    private String snpsaccount = "";
    private String snpspw = "";
    private String snpsDeviceUpdate = "";
    private String snpsBoardcast = "";
    private String snpsBoardcastPayload = "";
    private String snpsPublish = "";
    private String snpsSimple = "";
    private String snpsAppKey = "";

    private static final Logger logger = LogManager.getLogger(SNPSServiceImpl.class);

    public void setSNPSInfo(final String snpsaccount, final String snpspw, final String snpsDeviceUpdate, final String snpsBoardcast,final String snpsBoardcastPayload, final String snpsPublish, final String snpsSimple, final String snpsAppKey) {
        this.snpsaccount = snpsaccount;
        this.snpspw = snpspw;
        this.snpsDeviceUpdate = snpsDeviceUpdate;
        this.snpsBoardcast = snpsBoardcast;
        this.snpsBoardcastPayload = snpsBoardcastPayload;
        this.snpsPublish = snpsPublish;
        this.snpsSimple = snpsSimple;
        this.snpsAppKey = snpsAppKey;
    }

    @Override
    public void DeviceUpdate(SNPSDeviceUpdate aSNPSDeviceUpdate) {

//        SNPSRunable aSNPSRunable = new SNPSRunable(aSNPSDeviceUpdate);
//        aSNPSRunable.snpsaccount = snpsaccount;
//        aSNPSRunable.snpsDeviceUpdateUrl = snpsDeviceUpdate;
//        aSNPSRunable.snpspw = snpspw;
//        new Thread(aSNPSRunable).start();
//        aSNPSRunable.run();

    }

    @Override
    public void sendSNSByMember(SNPSPublishMember aSNPSPublish) {

//        try {
//
//            final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
//            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null,new TrustSelfSignedStrategy()).build();
//
//            // Allow TLSv1 protocol only, use NoopHostnameVerifier to trust self-singed cert
//            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
//                    new String[] { "TLSv1" }, null, new NoopHostnameVerifier());
//            
//            CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setSSLSocketFactory(sslsf).build();
//
//            HttpPost httpPost = new HttpPost(snpsPublish);
//            httpPost.setHeader("Accept", "application/json");
//            httpPost.setHeader("Content-Type", "application/json");
//            String encoding = Base64.getEncoder().encodeToString((snpsaccount + ":" + snpspw).getBytes("UTF-8"));
//            httpPost.setHeader("Authorization", "Basic " + encoding);
//
//            String rdata = new Gson().toJson(aSNPSPublish);
//            StringEntity entity = new StringEntity(rdata, "UTF-8");
//            httpPost.setEntity(entity);
//
//            IOException ioex = null;
//            CloseableHttpResponse response_t = null;
//            int cnt = 0;
//            boolean success = false;
////            while (cnt < 5 && success == false) {
//                try {
//                    logger.debug("SNS-send req:" + rdata);
//                    response_t = httpclient.execute(httpPost);
//                    HttpEntity responseEntity = response_t.getEntity();
//                    if (responseEntity != null) {
//                        String respJson = EntityUtils.toString(responseEntity, "UTF-8");
//                        logger.debug("SNS-send resp:" + respJson);
//                        Type stringStringMap = new TypeToken<Map<String, String>>() {
//                        }.getType();
//                        Map<String, String> map = new Gson().fromJson(respJson, stringStringMap);
//                        String returnCode = map.get("returnCode");
//                        if (StringUtils.isNotBlank(returnCode) || "000".equalsIgnoreCase(returnCode)) {
//                            success = true;
//                        }
//                    }
//                    response_t.close();
//                    httpPost.releaseConnection();
//                } catch (IOException ex) {
//                    logger.error("SNS-send error:"+ex.getMessage());
//                    ioex = ex;
//                }
//                cnt++;
////            }
//
//        } catch (Exception e) {
//            logger.error(e);
//        }

    }

    @Override
    public SNPSPublishSimpleResp sendSNSByToken(SNPSPublishSimple aSNPSPublishSimple) {

        SNPSPublishSimpleResp resp = null;
//        try {
//
//            final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
//            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null,new TrustSelfSignedStrategy()).build();
//
//            // Allow TLSv1 protocol only, use NoopHostnameVerifier to trust self-singed cert
//            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
//                    new String[] { "TLSv1" }, null, new NoopHostnameVerifier());
//            
//            CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setSSLSocketFactory(sslsf).build();
//
//            HttpPost httpPost = new HttpPost(snpsSimple);
//            httpPost.setHeader("Accept", "application/json");
//            httpPost.setHeader("Content-Type", "application/json");
//            String encoding = Base64.getEncoder().encodeToString((snpsaccount + ":" + snpspw).getBytes("UTF-8"));
//            httpPost.setHeader("Authorization", "Basic " + encoding);
//
//            String rdata = new Gson().toJson(aSNPSPublishSimple);
//            StringEntity entity = new StringEntity(rdata, "UTF-8");
//            httpPost.setEntity(entity);
//
//            IOException ioex = null;
//            CloseableHttpResponse response_t = null;
//            int cnt = 0;
//            boolean success = false;
////            while (cnt < 5 && success == false) {
//                try {
//                    logger.debug("SNS-send req:" + rdata);
//                    response_t = httpclient.execute(httpPost);
//                    HttpEntity responseEntity = response_t.getEntity();
//                    if (responseEntity != null) {
//                        String respJson = EntityUtils.toString(responseEntity, "UTF-8");
//                        logger.debug("SNS-send resp:" + respJson);
//                        if(StringUtils.isBlank(respJson)){
//                            throw new Exception("response is empty");
//                        }
//                        resp = new Gson().fromJson(respJson, SNPSPublishSimpleResp.class);
//                        String returnCode = resp.getReturnCode();
//                        if (StringUtils.isNotBlank(returnCode) || "000".equalsIgnoreCase(returnCode)) {
//                            success = true;
//                        }
//                    }
//                    response_t.close();
//                    httpPost.releaseConnection();
//                } catch (IOException ex) {
//                    logger.error("SNS-send error:"+ex.getMessage());
//                    ioex = ex;
//                }
//                cnt++;
////            }
//
//        } catch (Exception e) {
//            logger.error(e);
//        }

        return resp;

    }

    @Override
    public SNPSPublishBoardcastResp boardCast(SNPSPublishBoardcast aSNPSPublishBoardcast) {

        SNPSPublishBoardcastResp resp = null;
//        try {
//
//            final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
//            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null,new TrustSelfSignedStrategy()).build();
//
//            // Allow TLSv1 protocol only, use NoopHostnameVerifier to trust self-singed cert
//            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
//            		new String[] {"TLSv1", "TLSv1.2"}, null, new NoopHostnameVerifier());
//            
//            CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setSSLSocketFactory(sslsf).build();
//            HttpPost httpPost = new HttpPost(snpsBoardcast);
//            httpPost.setHeader("Accept", "application/json");
//            httpPost.setHeader("Content-Type", "application/json");
//            String encoding = Base64.getEncoder().encodeToString((snpsaccount + ":" + snpspw).getBytes("UTF-8"));
//            httpPost.setHeader("Authorization", "Basic " + encoding);
//
//            String rdata = new Gson().toJson(aSNPSPublishBoardcast);
//            StringEntity entity = new StringEntity(rdata, "UTF-8");
//            httpPost.setEntity(entity);
//
//            IOException ioex = null;
//            CloseableHttpResponse response_t = null;
//            int cnt = 0;
//            boolean success = false;
////            while (cnt < 5 && success == false) {
//                try {
//                    logger.debug("SNS-boardCast req:" + rdata);
//                    response_t = httpclient.execute(httpPost);
//                    success = true;
//                    HttpEntity responseEntity = response_t.getEntity();
//                    if (responseEntity != null) {
//                        String respJson = EntityUtils.toString(responseEntity, "UTF-8");
//                        logger.debug("SNS-boardCast resp:" + respJson);
//                        resp = new Gson().fromJson(respJson, SNPSPublishBoardcastResp.class);
//                    }
//                    response_t.close();
//                    httpPost.releaseConnection();
//                } catch (IOException ex) {
//                    logger.error("SNS-boardCast error:"+ex.getMessage());
//                    ioex = ex;
//                }
//                cnt++;
////            }
//
//        } catch (Exception e) {
//            logger.error(e);
//        }
        return resp;
    }
    
    @Override
    public SNPSPublishBoardcastPayloadResp boardcastPayload(SNPSPublishBoardcastPayload aSNPSPublishBoardcastPayload) {

    	SNPSPublishBoardcastPayloadResp resp = null;
//        try {
//
//            final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
//            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null,new TrustSelfSignedStrategy()).build();
//
//            // Allow TLSv1 protocol only, use NoopHostnameVerifier to trust self-singed cert
//            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
//            		new String[] {"TLSv1", "TLSv1.2"}, null, new NoopHostnameVerifier());
//            
//            CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setSSLSocketFactory(sslsf).build();
//            HttpPost httpPost = new HttpPost(snpsBoardcastPayload);
//            httpPost.setHeader("Accept", "application/json");
//            httpPost.setHeader("Content-Type", "application/json");
//            String encoding = Base64.getEncoder().encodeToString((snpsaccount + ":" + snpspw).getBytes("UTF-8"));
//            httpPost.setHeader("Authorization", "Basic " + encoding);
//
//            String rdata = new Gson().toJson(aSNPSPublishBoardcastPayload);
//            StringEntity entity = new StringEntity(rdata, "UTF-8");
//            httpPost.setEntity(entity);
//
//            IOException ioex = null;
//            CloseableHttpResponse response_t = null;
//            int cnt = 0;
//            boolean success = false;
////            while (cnt < 5 && success == false) {
//                try {
//                    logger.debug("SNS-boardCast req:" + rdata);
//                    response_t = httpclient.execute(httpPost);
//                    success = true;
//                    HttpEntity responseEntity = response_t.getEntity();
//                    if (responseEntity != null) {
//                        String respJson = EntityUtils.toString(responseEntity, "UTF-8");
//                        logger.debug("SNS-boardCast resp:" + respJson);
//                        resp = new Gson().fromJson(respJson, SNPSPublishBoardcastPayloadResp.class);
//                    }
//                    response_t.close();
//                    httpPost.releaseConnection();
//                } catch (IOException ex) {
//                    logger.error("SNS-boardCast error:"+ex.getMessage());
//                    ioex = ex;
//                }
//                cnt++;
////            }
//
//        } catch (Exception e) {
//            logger.error(e);
//        }
        return resp;
    }

}
