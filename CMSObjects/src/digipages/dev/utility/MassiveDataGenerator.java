package digipages.dev.utility;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.kohsuke.randname.RandomNameGenerator;

import digipages.common.DPLogger;
import model.BaseBookInfo;
import model.BookFile;
import model.BookInfo;
import model.CmsToken;
import model.Item;
import model.Member;
import model.MemberBook;
import model.MemberBookNote;
import model.MemberDevice;
import model.MemberDrmLog;
import model.NoteContent;
import model.Publisher;
import model.Vendor;

public class MassiveDataGenerator {
	
	private static DPLogger logger =DPLogger.getLogger(MassiveDataGenerator.class.getName());
	
	private EntityManagerFactory emf;
	private EntityManager em;
	private static int publisherCnt=50;
	private static int vendorCnt=30;
	private static int itemCnt=50;
	private static int memberCnt=10;
	private static RandomNameGenerator rname = new RandomNameGenerator();
	private static Random rnd = new Random();
	private static PowerLaw pwRnd = new PowerLaw();
	private static List<Publisher> publishers = new ArrayList<>();
	private static List<Vendor> vendors = new ArrayList<>();
	private static List<BookFile> bookfiles = new ArrayList<>();
	
	
	public MassiveDataGenerator()
	{
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();
	}
	
	public static void main(String args[])
	{
		MassiveDataGenerator dg = new MassiveDataGenerator();
		try {
			long startTime = System.currentTimeMillis();
			dg.generateData(dg.getClass().getMethod("generatePublisher"), MassiveDataGenerator.publisherCnt);
			dg.generateData(dg.getClass().getMethod("generateVendor"), MassiveDataGenerator.vendorCnt);
			dg.generateData(dg.getClass().getMethod("generateItem"), MassiveDataGenerator.itemCnt);
			dg.generateData(dg.getClass().getMethod("generateMember"), MassiveDataGenerator.memberCnt);
			logger.info("\nTotalTime: " + (System.currentTimeMillis() -startTime) + " ms");
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			logger.error(e);;
		}
		
	}
	
	public void runMe()
	{
		
	}
	
	
	
	public void generatePublisher()
	{
		Publisher p =new Publisher();
		p.setBId("pubid_"+rname.next());
		p.setName("pname_" + rname.next());
		em.persist(p);
		publishers.add(p);
	}
	
	public void generateVendor()
	{
		Vendor v = new Vendor();
		v.setBId("VendBid_" + rname.next());
		v.setName("vname_" + rname.next());
		em.persist(v);
		vendors.add(v);
	}
	
	public void generateItem() {
		Item i = new Item();
		
		i.setId("item_" +rname.next());
		
		BookInfo bi = new BookInfo();
		bi.setIsbn(rname.next());
		bi.setName("Book_" + rname.next());
		bi.setType("book");
		i.setInfo(bi);
		
		i.setItemType("Book");
		i.setOperator(rname.next());
		
		Publisher curPublisher =publishers.get(rnd.nextInt(publishers.size())); 
		i.setPublisher(curPublisher);
		Vendor curVendor =vendors.get(rnd.nextInt(vendors.size())); 
		i.setVendor(curVendor);
		
		List <BookFile> bookFiles = genBookFiles(i,bi);
		i.setBookFiles(bookFiles);
		em.persist(i);
	}
	


	private List<BookFile> genBookFiles(Item item,BookInfo bi) {
		List<BookFile> bf = new ArrayList<>();
		// main File, each book has 3 version
		for (int i=1; i<4;i++){
			BookFile biMain = new BookFile();
			biMain.setFileLocation(bi.getName());
			biMain.setFormat(BaseBookInfo.BookFormatReflowable);
			biMain.setIsTrial(false);
			biMain.setItem(item);
			biMain.setVersion("V00"+i+ ".0001");
			bf.add(biMain);
			em.persist(biMain);
		}
		
		// trial File = 1
		BookFile biMain = new BookFile();
		biMain.setFileLocation(bi.getName()+"_trial");
		biMain.setFormat(BaseBookInfo.BookFormatReflowable);
		biMain.setIsTrial(false);
		biMain.setItem(item);
		biMain.setVersion("V001.0001");
		bf.add(biMain);
		em.persist(biMain);
		
		bookfiles.addAll(bf);
		
		return bf;
	}

	public void generateMember()
	{
		Member m = new Member();
		genMemberDevices(m);
		genMemberToken(m);
		genMemberBooks(m);
		genMemberDrmLog(m);
		genMemberReadList(m);
		m.setBmemberId("bid_"+rname.next());
		m.setMemberType("NormalReader");
		m.setName(rname.next());
		m.setNick(rname.next());

		em.persist(m);
	}
	
	private void genMemberReadList(Member m) {
		m.genDefaultReadLists(em);
	}

	private void genMemberBooks(Member m) {
		List <MemberBook>books = new ArrayList<>();
		// each Member has power low distribution of max 200 books;
		int bookCnt = pwRnd.zipf(200);
		for (int i=0; i< bookCnt;i++){
			MemberBook mb = genMemberBook(m);
			books.add(mb);
		}
		m.setMemberBooks(books);
	}

	private MemberBook genMemberBook(Member m) {
		MemberBook mb = new MemberBook();
		mb.setMember(m);
		BookFile b = bookfiles.get(rnd.nextInt(bookfiles.size()));
		mb.setBookFile(b);
		List <MemberBookNote> mbn = genMemberBookNotes(mb);
		mb.setMemberBookNotes(mbn);
		mb.setBookUniId(b.getBookUniId());
		mb.setDefaultReadList(mb);
		em.persist(mb);
		return mb;
	}

	private List<MemberBookNote> genMemberBookNotes(MemberBook mb) {
		List <MemberBookNote> bookNotes= new ArrayList<>();
		// each book has  power low dist of max 20 annotations
		int anCnt=pwRnd.zipf(20);
		for (int i=0; i< anCnt; i++)
		{
			MemberBookNote mbn = new MemberBookNote();
			mbn.setNoteContent(new NoteContent());
			UUID uuid = UUID.randomUUID();
			mbn.setUuid(uuid);
			mbn.setMemberBook(mb);
			em.persist(mbn);
			bookNotes.add(mbn);
		}
		return bookNotes;
	}

	private void genMemberDrmLog(Member m) {
		List <MemberDrmLog> drmLogs = new ArrayList<>();
		
		
		for (MemberBook mb: m.getMemberBooks())
		{
			// each book has most 6 drmInfo
			int drmCnt = pwRnd.zipf(5)+1;
			for (int i=0; i< drmCnt; i++)
			{
				MemberDrmLog mdl = new MemberDrmLog();
				mdl.setItem(mb.getBookFile().getItem());
				mdl.setMember(m);
				em.persist(mdl);
				drmLogs.add(mdl);
			}
			return;
		}

	}

	private void genMemberToken(Member m) {
		List <CmsToken>tokens = new ArrayList<>();
		// for each device, generate a cmstoken
		for (MemberDevice md:m.getMemberDevices()){
			CmsToken cm = new CmsToken();
			cm.setClientIp("192.168.1.102");
			cm.setDeviceId(md.getId());
			cm.setMemberId(m.getId());
			em.persist(cm);
			tokens.add(cm);
		}

	}

	private void genMemberDevices(Member m) {
		List <MemberDevice> devices = new ArrayList<>();
		// each has 6 devices (5 + 1 )
		for (int i=0; i< 6;i++)
		{
			MemberDevice md = new MemberDevice();
			md.setDeviceId(rname.next());
			md.setDeviceModel(rname.next());
			md.setDeviceVendor(rname.next());
			md.setLanguage("zh_TW");
			md.setLastOnlineIp("192.168.1.135");
			md.setLastOnlineTime(LocalDateTime.now());
			md.setMember(m);
			md.setOsType("ios");
			md.setOsVersion("1.0");
			md.setScreenDpi("400.5");
			md.setScreenResolution("1920x1080");
			em.persist(md);
			devices.add(md);
		}
		m.setMemberDevices(devices);
	}

	public void generateData(Method m, int cnt) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		long startTime = System.currentTimeMillis();
		em.getTransaction().begin();
		for (int i=1;i< cnt; i++)
		{
			m.invoke(this, (Object[])null);
			if (i%100==0)
			{
				em.flush();
			}
		}
		em.getTransaction().commit();
		long duringMs = System.currentTimeMillis()-startTime;
		
		logger.info("\nMethod:" + m.getName());
		logger.info("Average Time Per Unit:" + (duringMs/cnt) + "ms");
		logger.info("Time Used :" +duringMs+ " ms");

	}
	
}

