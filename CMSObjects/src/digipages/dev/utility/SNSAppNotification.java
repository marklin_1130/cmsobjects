package digipages.dev.utility;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import org.codehaus.jackson.map.ObjectMapper;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;

import digipages.common.DPLogger;
import digipages.dev.utility.SNSMessageGenerator.Platform;
import model.MessageQueue;

public class SNSAppNotification {
	
	private static DPLogger logger =DPLogger.getLogger(SNSAppNotification.class.getName());

	private SNSAppClientWrapper snsClientWrapper;
	private static final ObjectMapper objectMapper = new ObjectMapper();

	public SNSAppNotification(AmazonSNS snsClient) {
		this.snsClientWrapper = new SNSAppClientWrapper(snsClient);
	}

	public static final Map<Platform, Map<String, MessageAttributeValue>> attributesMap = new HashMap<Platform, Map<String, MessageAttributeValue>>();

	static {
		attributesMap.put(Platform.GCM, null);
		attributesMap.put(Platform.APNS, null);
		// attributesMap.put(Platform.APNS_SANDBOX, null);
		// attributesMap.put(Platform.ADM, null);
		// attributesMap.put(Platform.BAIDU, addBaiduNotificationAttributes());
		// attributesMap.put(Platform.WNS, addWNSNotificationAttributes());
		// attributesMap.put(Platform.MPNS, addMPNSNotificationAttributes());
	}

	public void pushAndroidAppNotification(EntityManager em, AmazonSNS sns, MessageQueue tmpMessageQueues, String gcmServerAPIKey, String gcmApplicationName, String gcmRegistrationId) throws IOException {
		Map<String, Object> androidMessageMap = new HashMap<String, Object>();
		Map<String, Object> andMessageMap = new HashMap<String, Object>();
		andMessageMap.put("message", tmpMessageQueues.getContent());
		androidMessageMap.put("data", andMessageMap);
		String message = jsonify(androidMessageMap);

		String serverAPIKey = gcmServerAPIKey;
		String applicationName = gcmApplicationName;
		String registrationId = gcmRegistrationId;

		snsClientWrapper.GCMNotification(em, Platform.GCM, "", serverAPIKey, registrationId, applicationName, attributesMap, message, tmpMessageQueues.getContent(), tmpMessageQueues, sns);
	}

	public void pushAppleAppNotification(EntityManager em, AmazonSNS sns, MessageQueue tmpMessageQueues, String apnsCertificate, String apnsPrivateKey, String apnsApplicationName, String apnsDeviceToken) throws IOException {
		Map<String, Object> appleMessageMap = new HashMap<String, Object>();
		Map<String, Object> appMessageMap = new HashMap<String, Object>();
		appMessageMap.put("alert", tmpMessageQueues.getContent());
		appMessageMap.put("sound", "default");
		appleMessageMap.put("aps", appMessageMap);
		String message = jsonify(appleMessageMap);

		String certificate = apnsCertificate;
		String privateKey = apnsPrivateKey;
		String applicationName = apnsApplicationName;
		String deviceToken = apnsDeviceToken;

		snsClientWrapper.APNSNotification(em, Platform.APNS, certificate, privateKey, deviceToken, applicationName, attributesMap, message, tmpMessageQueues.getContent(), tmpMessageQueues, sns);
	}

	public void pushAppleSandboxAppNotification(EntityManager em, AmazonSNS sns, MessageQueue tmpMessageQueues, String apnsCertificate, String apnsPrivateKey, String apnsApplicationName, String apnsDeviceToken) throws IOException {
		Map<String, Object> appleMessageMap = new HashMap<String, Object>();
		Map<String, Object> appMessageMap = new HashMap<String, Object>();
		appMessageMap.put("alert", tmpMessageQueues.getContent());
		appMessageMap.put("sound", "default");
		appleMessageMap.put("aps", appMessageMap);
		String message = jsonify(appleMessageMap);

		String certificate = apnsCertificate;
		String privateKey = apnsPrivateKey;
		String applicationName = apnsApplicationName;
		String deviceToken = apnsDeviceToken;

		snsClientWrapper.APNSNotification(em, Platform.APNS_SANDBOX, certificate, privateKey, deviceToken, applicationName, attributesMap, message, tmpMessageQueues.getContent(), tmpMessageQueues, sns);
	}

	public void pushKindleAppNotification(EntityManager em, AmazonSNS sns, MessageQueue tmpMessageQueues) {
		String clientId = "";
		String clientSecret = "";
		String applicationName = "";
		String registrationId = "";

		snsClientWrapper.demoNotification(em, Platform.ADM, clientId, clientSecret, registrationId, applicationName, attributesMap, "", "", null, sns);
	}

	public void pushBaiduAppNotification(EntityManager em, AmazonSNS sns, MessageQueue tmpMessageQueues) {
		String userId = "";
		String channelId = "";
		String apiKey = "";
		String secretKey = "";
		String applicationName = "";

		snsClientWrapper.demoNotification(em, Platform.BAIDU, apiKey, secretKey, channelId + "|" + userId, applicationName, attributesMap, "", "", null, sns);
	}

	public void pushWNSAppNotification(EntityManager em, AmazonSNS sns, MessageQueue tmpMessageQueues) {
		String notificationChannelURI = "";
		String packageSecurityIdentifier = "";
		String secretKey = "";
		String applicationName = "";

		snsClientWrapper.demoNotification(em, Platform.WNS, packageSecurityIdentifier, secretKey, notificationChannelURI, applicationName, attributesMap, "", "", null, sns);
	}

	public void pushMPNSAppNotification(EntityManager em, AmazonSNS sns, MessageQueue tmpMessageQueues) {
		String notificationChannelURI = "";
		String applicationName = "";

		snsClientWrapper.demoNotification(em, Platform.MPNS, "", "", notificationChannelURI, applicationName, attributesMap, "", "", null, sns);
	}

//	private static Map<String, MessageAttributeValue> addBaiduNotificationAttributes() {
//		Map<String, MessageAttributeValue> notificationAttributes = new HashMap<String, MessageAttributeValue>();
//		notificationAttributes.put("AWS.SNS.MOBILE.BAIDU.DeployStatus", new MessageAttributeValue().withDataType("String").withStringValue("1"));
//		notificationAttributes.put("AWS.SNS.MOBILE.BAIDU.MessageKey", new MessageAttributeValue().withDataType("String").withStringValue("default-channel-msg-key"));
//		notificationAttributes.put("AWS.SNS.MOBILE.BAIDU.MessageType", new MessageAttributeValue().withDataType("String").withStringValue("0"));
//		return notificationAttributes;
//	}

//	private static Map<String, MessageAttributeValue> addWNSNotificationAttributes() {
//		Map<String, MessageAttributeValue> notificationAttributes = new HashMap<String, MessageAttributeValue>();
//		notificationAttributes.put("AWS.SNS.MOBILE.WNS.CachePolicy", new MessageAttributeValue().withDataType("String").withStringValue("cache"));
//		notificationAttributes.put("AWS.SNS.MOBILE.WNS.Type", new MessageAttributeValue().withDataType("String").withStringValue("wns/badge"));
//		return notificationAttributes;
//	}

//	private static Map<String, MessageAttributeValue> addMPNSNotificationAttributes() {
//		Map<String, MessageAttributeValue> notificationAttributes = new HashMap<String, MessageAttributeValue>();
//		notificationAttributes.put("AWS.SNS.MOBILE.MPNS.Type", new MessageAttributeValue().withDataType("String").withStringValue("token")); // This attribute is required.
//		notificationAttributes.put("AWS.SNS.MOBILE.MPNS.NotificationClass", new MessageAttributeValue().withDataType("String").withStringValue("realtime")); // This attribute is required.
//		return notificationAttributes;
//	}

	public static String jsonify(Object message) {
		try {
			return objectMapper.writeValueAsString(message);
		} catch (Exception e) {
			logger.error(e);;
			throw (RuntimeException) e;
		}
	}
}