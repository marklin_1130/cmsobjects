package digipages.dev.utility;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.google.gson.Gson;

import digipages.exceptions.ServerException;

//import static org.boon.json.JsonFactory.*;
import org.kohsuke.randname.RandomNameGenerator;

import model.BaseBookInfo;
import model.BookFile;
import model.BookInfo;
import model.CmsToken;
import model.Item;
import model.ItemInfo;
import model.MagInfo;
import model.Member;
import model.MemberBook;
import model.MemberBookNote;
import model.MemberDevice;
import model.MemberDrmLog;
import model.NoteContent;
import model.Publisher;
import model.RecommendBook;
import model.Vendor;

public class SampleHotBookGenerator {
	
//	private EntityManagerFactory emf;
	private EntityManager em;
	private static RandomNameGenerator rname = new RandomNameGenerator();
	
	
	private static ArrayList<BookFile> allBookFile = new ArrayList<>();
	private static ArrayList<String> bookFormats = new ArrayList<>();
	
	
	public SampleHotBookGenerator(EntityManager em)
	{
		this.em = em;
		
	}
	
	public void delAllHotBook()
	{
		em.createNativeQuery("delete from recommend_books; ").executeUpdate();
		em.getEntityManagerFactory().getCache().evict(RecommendBook.class);
	}
	
	public void generateHotBooks(String itemIds) {
		
		String [] splited=itemIds.split(",");	

		OffsetDateTime odt = OffsetDateTime.now();

		for (String itemId:splited)
		{
			
			BookFile b = BookFile.findNewTrialByItemId(em, itemId);
			if (b==null) continue;
			
			RecommendBook rb = new RecommendBook();
			rb.setBookFile(b);
			rb.setStartTime(randStartTime(odt));
			rb.setEndTime(randEndTime(odt));
			rb.setLastUpdated(java.util.Date.from(odt.toInstant()));
			rb.setItemId(b.getItem().getId());
			odt = odt.plusHours(8);
			em.persist(rb);
		}
		
	}

	private Date randEndTime(OffsetDateTime baseDate) {
		OffsetDateTime o = baseDate.plusHours(72);
		return java.util.Date.from(o.toInstant());
	}

	private Date randStartTime(OffsetDateTime baseDate) {
		OffsetDateTime o = baseDate.minusHours(72);
		return java.util.Date.from(o.toInstant());
	}
	



	
	
}