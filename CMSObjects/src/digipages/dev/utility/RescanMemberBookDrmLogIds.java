package digipages.dev.utility;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.StringHelper;
import digipages.exceptions.ServerException;
import model.Member;
import staticmodel.StaticMemberBook;
import staticmodel.StaticMemberDrmLog;

public class RescanMemberBookDrmLogIds {
	
	private static DPLogger logger =DPLogger.getLogger(RescanMemberBookDrmLogIds.class.getName());
	
	private EntityManager em;
	private long sumTotalRescanTime = 0L;
	private long sumTotalRescanMember = 0L;
	private long sumTotalRescanMemberBook = 0L;

	public RescanMemberBookDrmLogIds(EntityManager entityManager) {
		this.em = entityManager;
	}

	public void rescanDRM() throws ServerException, IOException {
		try {
			String membersQueryString = "SELECT * FROM member"
					+ " WHERE bmember_id NOT LIKE '%bench%'"
					+ " AND bmember_id IS NOT NULL"
					+ " AND member_type != 'Guest'"
					+ " ORDER BY id ASC";

			@SuppressWarnings("unchecked")
			List<Member> members = em.createNativeQuery(membersQueryString, Member.class).getResultList();

			if (members.size() > 0) {
				for (Member member : members) {
					updateMemberBookDrmLogIds(em, member);
				}
			}

//			Member member = em.find(Member.class, 17563L);
//			updateMemberBookDrmLogIds(em, member);

			logger.info("sumTotalRescanTime=" + sumTotalRescanTime + "ms");
			logger.info("sumTotalRescanMember=" + sumTotalRescanMember);
			logger.info("sumTotalRescanMemberBook=" + sumTotalRescanMemberBook);
		} catch (ServerException ex) {
			if (ex.getError_code().endsWith("999")) {
				logger.error(ex);;
			}

			throw ex;
		} catch (Exception ex) {
			logger.error(ex);;
			throw new ServerException("id_err_999", "Unknown Exception " + ex.getMessage());
		}
	}

	private void updateMemberBookDrmLogIds(EntityManager postgres, Member member) throws ServerException {
		HashMap<String, ArrayList<Integer>> memberDrmLogMap = new HashMap<>();

		String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
		String nowDateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

		long startTime = System.currentTimeMillis();

		// reset drm_log_ids
//		int member_book_count = (int) (long) postgres.createNativeQuery("SELECT COUNT(*) FROM member_book"
//				+ " WHERE member_id = " + member.getId() + ""
//				+ " AND readlist_idnames <> '{trial}'").getSingleResult();
		int member_book_count = (int) (long) postgres.createNativeQuery("SELECT COUNT(*) FROM member_book"
				+ " WHERE member_id = " + member.getId() + ""
				+ " AND encrypt_type <> 'none'").getSingleResult();
		clearMemberBookDrmIds(postgres, member);

		long timeClearMemberBookDrmIds = System.currentTimeMillis() - startTime;

		// check serial book
		String serialBookQueryString = "SELECT mdl.* FROM member_drm_log mdl"
				+ " INNER JOIN item it ON mdl.item_id = it.id"
				+ " WHERE it.item_type = 'serial'"
				+ " AND mdl.member_id = " + member.getId() + ""
				+ " AND mdl.read_expire_time >= '" + nowDateTimeString + "'"
				+ " AND mdl.status = 1";

		@SuppressWarnings("unchecked")
		List<StaticMemberDrmLog> serialBookDrms = postgres.createNativeQuery(serialBookQueryString, StaticMemberDrmLog.class).getResultList();

		long timeListSerialBookDrms = System.currentTimeMillis() - startTime - timeClearMemberBookDrmIds;
		long timeListChildsMemberBooks = 0L;

		if (serialBookDrms.size() > 0) {
			for (StaticMemberDrmLog tmpSerialBookDrm : serialBookDrms) {
				String childsBookQueryString = "SELECT * FROM member_book"
						+ " WHERE member_id = " + StringHelper.escapeSQL(member.getId().toString()) + ""
						+ " AND item_id IN (SELECT id FROM item WHERE id = ANY((SELECT childs FROM item WHERE id = '" + StringHelper.escapeSQL(tmpSerialBookDrm.getItem()) + "')::varchar[]))";

				@SuppressWarnings("unchecked")
				List<StaticMemberBook> childsMemberBooks = postgres.createNativeQuery(childsBookQueryString, StaticMemberBook.class).getResultList();

				timeListChildsMemberBooks = System.currentTimeMillis() - startTime - timeClearMemberBookDrmIds - timeListSerialBookDrms;

				if (childsMemberBooks.size() > 0) {
					for (StaticMemberBook tmpChildsMemberBook : childsMemberBooks) {
						if (memberDrmLogMap.containsKey(tmpChildsMemberBook.getItem())) {
							ArrayList<Integer> serialDrms = memberDrmLogMap.get(tmpChildsMemberBook.getItem());
							serialDrms.add(tmpSerialBookDrm.getId().intValue());
							memberDrmLogMap.put(tmpChildsMemberBook.getItem(), serialDrms);
						} else {
							ArrayList<Integer> serialDrms = new ArrayList<Integer>();
							serialDrms.add(tmpSerialBookDrm.getId().intValue());
							memberDrmLogMap.put(tmpChildsMemberBook.getItem(), serialDrms);
						}
					}
				}
			}
		}

		// check normal book
		String normalBookQueryString = "SELECT mdl.* FROM member_drm_log mdl"
				+ " INNER JOIN item it ON mdl.item_id = it.id"
				+ " WHERE it.item_type != 'serial'"
				+ " AND mdl.member_id = " + member.getId() + ""
				+ " AND mdl.read_expire_time >= '" + nowDateTimeString + "'"
				+ " AND mdl.status = 1";

		@SuppressWarnings("unchecked")
		List<StaticMemberDrmLog> normalBookDrms = postgres.createNativeQuery(normalBookQueryString, StaticMemberDrmLog.class).getResultList();

		long timeListNormalBookDrms = System.currentTimeMillis() - startTime - timeClearMemberBookDrmIds - timeListSerialBookDrms - timeListChildsMemberBooks;

		if (normalBookDrms.size() > 0) {
			for (StaticMemberDrmLog tmpNormalBookDrm : normalBookDrms) {
				if (memberDrmLogMap.containsKey(tmpNormalBookDrm.getItem())) {
					ArrayList<Integer> normalDrms = memberDrmLogMap.get(tmpNormalBookDrm.getItem());
					normalDrms.add(tmpNormalBookDrm.getId().intValue());
					memberDrmLogMap.put(tmpNormalBookDrm.getItem(), normalDrms);
				} else {
					ArrayList<Integer> normalDrms = new ArrayList<Integer>();
					normalDrms.add(tmpNormalBookDrm.getId().intValue());
					memberDrmLogMap.put(tmpNormalBookDrm.getItem(), normalDrms);
				}
			}
		}

		// update drm_log_ids
		try {
			postgres.getTransaction().begin();

			for (Object key : memberDrmLogMap.keySet()) {
//				Query query = postgres.createNativeQuery("UPDATE member_book"
//						+ " SET drm_log_ids = '" + memberDrmLogMap.get(key).toString().replace("[", "{").replace("]", "}") + "', action = 'update'"
//						+ " WHERE member_id = " + member.getId() + ""
//						+ " AND item_id = '" + key.toString() + "'"
//						+ " AND readlist_idnames <> '{trial}'");
				Query query = postgres.createNativeQuery("UPDATE member_book"
						+ " SET drm_log_ids = '" + memberDrmLogMap.get(key).toString().replace("[", "{").replace("]", "}") + "', action = 'update'"
						+ " WHERE member_id = " + member.getId() + ""
						+ " AND item_id = '" + key.toString() + "'"
						+ " AND encrypt_type <> 'none'");
				query.executeUpdate();
			}

			postgres.getTransaction().commit();
			postgres.refresh(member);
		} catch (Exception ex) {
			logger.error(ex);
			throw new ServerException("id_err_999", ex.getMessage());
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		long timeUpdateMemberBookDrmIds = System.currentTimeMillis() - startTime - timeClearMemberBookDrmIds - timeListSerialBookDrms - timeListChildsMemberBooks - timeListNormalBookDrms;

		long sumRescanTime = timeClearMemberBookDrmIds + timeListSerialBookDrms + timeListChildsMemberBooks + timeListNormalBookDrms + timeUpdateMemberBookDrmIds;

		logger.info("RescanMemberBookDrmLogIds Debug: member=" + member.getBmemberId() + ", "
				+ "member_type=" + member.getMemberType() + ", "
				+ "member_book_count=" + member_book_count + ", "
				+ "timeClearMemberBookDrmIds=" + timeClearMemberBookDrmIds + "ms, "
				+ "timeListSerialBookDrms=" + timeListSerialBookDrms + "ms, "
				+ "timeListChildsMemberBooks=" + timeListChildsMemberBooks + "ms, "
				+ "timeListNormalBookDrms=" + timeListNormalBookDrms + "ms, "
				+ "timeUpdateMemberBookDrmIds=" + timeUpdateMemberBookDrmIds + "ms, "
				+ "sumRescanTime=" + sumRescanTime + "ms");

		sumTotalRescanTime += sumRescanTime;
		sumTotalRescanMember += 1;
		sumTotalRescanMemberBook += member_book_count;
	}

	private static void clearMemberBookDrmIds(EntityManager postgres, Member member) throws ServerException {
		try {
			postgres.getTransaction().begin();

//			Query query = postgres.createNativeQuery("UPDATE member_book"
//					+ " SET drm_log_ids = null, action = 'del'"
//					+ " WHERE member_id = " + member.getId() + ""
//					+ " AND readlist_idnames <> '{trial}'");
			Query query = postgres.createNativeQuery("UPDATE member_book"
					+ " SET drm_log_ids = null, action = 'del'"
					+ " WHERE member_id = " + member.getId() + ""
					+ " AND encrypt_type <> 'none'");
			query.executeUpdate();

			postgres.flush();
			postgres.getTransaction().commit();
		} catch (Exception ex) {
			logger.error(ex);
			throw new ServerException("id_err_999", ex.getMessage());
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}
	}
}