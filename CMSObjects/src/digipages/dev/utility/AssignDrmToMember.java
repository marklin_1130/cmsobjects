package digipages.dev.utility;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import digipages.BooksHandler.MemberDrmHandler;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.dto.MemberDrmLogDTO;
import model.BookFile;
import model.Member;
import model.MemberBook;
import model.MemberBookDrmMapping;
import model.MemberBookFinder;
import model.MemberDrmLog;

public class AssignDrmToMember {
	
	private static DPLogger logger =DPLogger.getLogger(AssignDrmToMember.class.getName());
	
	public static void addThirdPartyDrm(EntityManager em, Member member, String bookUniId, String viewerBaseURI, String internalApiAddTrial) {
		try {
			// get book file for authorization
			MemberBookFinder finder = new MemberBookFinder(bookUniId);
			finder.find(em, member);
			BookFile bf = finder.getBookFile();

			 // assign drm to member
	            if ((null != member)) {
	                boolean isThirdParty = member.hasItemByThirdPartyId(bf.getItem());

	                if (isThirdParty) {
	                    // Check existence of DRM.
	                    List<MemberDrmLog> drmLogs = em.createNamedQuery("MemberDrmLog.findByMemberAndItemAndType", MemberDrmLog.class)
	                            .setParameter("member", member)
	                            .setParameter("item", bf.getItem())
	                            .setParameter("type", bf.getIsTrial() ? 4 : 999)
	                            .getResultList();

	                    if (drmLogs.size() <= 0) {
	                        String transactionId = UUID.randomUUID().toString();
	                        if(!bf.getIsTrial()){
	                            MemberDrmHandler handler = new MemberDrmHandler();
	                            MemberDrmLogDTO dto = new MemberDrmLogDTO();
	                            dto.setBooksMemberId(member.getBmemberId());
	                            dto.setViewerBaseURI(viewerBaseURI);
	                            dto.setItemId(bf.getItem().getId());
	                            dto.setTransactionId(transactionId);
	                            dto.setType("999");
	                            dto.setSubmitId("");
	                            dto.setMemberType(member.getMemberType());
	                            handler.addDRM(em,dto);
	                        }
	                    }else {
	                    	if(member.getBmemberId().equalsIgnoreCase("nologin-ThumbGenerator")) {
	                    		//修復書籍關連資料避免無法轉檔
	                    		
	                    		MemberBook memberBook = member.getMemberNormalBookByItemId(em, bf.getItem().getId());
	                            // 新增DRM關係對應
	                            if (memberBook != null && memberBook.getMemberBookDrmMappings().isEmpty()) {
	                            	em.getTransaction().begin();
	                            	
	                                MemberBookDrmMapping memberBookDrmMapping = new MemberBookDrmMapping();
	                                memberBookDrmMapping.setMemberBook(memberBook);
	                                memberBookDrmMapping.setMemberId(member.getId());
	                                memberBookDrmMapping.setMemberDrmLog(drmLogs.get(0));
	                                memberBookDrmMapping.setStatus("update");
	                                memberBookDrmMapping.setUdt(Date.from(ZonedDateTime.parse(CommonUtil.zonedTime()).toInstant()));
	                                em.persist(memberBookDrmMapping);
	                                
	                                em.getTransaction().commit();
	                            }
	                    		
	                    	}
	                    		
	                    }
	                }
	            }
			
		} catch (Exception ex) {
			logger.error(ex);;
		}
	}
}