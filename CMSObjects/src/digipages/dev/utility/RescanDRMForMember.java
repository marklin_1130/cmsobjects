package digipages.dev.utility;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;

import digipages.BooksHandler.RescanDRMHandler;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;

import model.Item;
import model.Member;
import model.ScheduleJob;

public class RescanDRMForMember {
	private EntityManager em;
	private BlockingQueue<ScheduleJob> bq;
	private static List<ScheduleJob> jobs;
	private static DPLogger logger = DPLogger.getLogger(RescanDRMForMember.class.getName());

	public RescanDRMForMember(EntityManager entityManager) {
		this.em = entityManager;
	}

	public RescanDRMForMember(EntityManager entityManager, BlockingQueue<ScheduleJob> bq) {
		this.em = entityManager;
		this.bq = bq;
	}

	public void rescanDRM(Item item) {
		try {
			if (null == item) {
				return;
			}

			// add jobs
			List<ScheduleJob> scheduleJobs = new ArrayList<>();

			String memberQueryString = "SELECT * FROM member WHERE id IN (SELECT DISTINCT member_id FROM member_drm_log WHERE item_id = '" + item.getId() + "' AND status = 1)";

			@SuppressWarnings("unchecked")
			List<Member> members = em.createNativeQuery(memberQueryString, Member.class).getResultList();

			if (members.size() > 0) {
				ArrayList<String> rescanItems = new ArrayList<String>();
				rescanItems.add(item.getId());

				for (Member tmpMember : members) {
					ScheduleJobParam jobParams = new ScheduleJobParam();

					jobParams.setMember_id(tmpMember.getId());
					jobParams.setItem_id(item.getId());
					jobParams.setItem_ids(rescanItems);

					ScheduleJob tmpJob = new ScheduleJob();

					tmpJob.setJobName(RescanDRMHandler.class.getName());
					tmpJob.setJobGroup("DRM");
					tmpJob.setJobRunner(0); // for local
					tmpJob.setPriority(500);
					tmpJob.setRetryCnt(0);
					tmpJob.setStatus(0);
					tmpJob.setJobParam(jobParams);

					scheduleJobs.add(tmpJob);
				}

				ScheduleJobManager.addJobs(scheduleJobs);
			}
		} catch (Exception ex) {
			logger.error(ex);
		}
	}
}