package digipages.dev.utility;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.CreatePlatformApplicationRequest;
import com.amazonaws.services.sns.model.CreatePlatformApplicationResult;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.DeletePlatformApplicationRequest;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.common.DPLogger;
import digipages.common.StringHelper;
import digipages.dev.utility.SNSMessageGenerator.Platform;
import model.MemberDevice;
import model.Message;
import model.MessageQueue;

public class SNSAppClientWrapper {
	
	private static DPLogger logger = DPLogger.getLogger(SNSAppClientWrapper.class.getName());

	private final AmazonSNS snsClient;

	public SNSAppClientWrapper(AmazonSNS client) {
		this.snsClient = client;
	}

	private CreatePlatformApplicationResult createPlatformApplication(String applicationName, Platform platform, String principal, String credential) {
		CreatePlatformApplicationRequest platformApplicationRequest = new CreatePlatformApplicationRequest();
		Map<String, String> attributes = new HashMap<String, String>();

		attributes.put("PlatformPrincipal", principal);
		attributes.put("PlatformCredential", credential);

		platformApplicationRequest.setAttributes(attributes);
		platformApplicationRequest.setName(applicationName);
		platformApplicationRequest.setPlatform(platform.name());

		return snsClient.createPlatformApplication(platformApplicationRequest);
	}

	private CreatePlatformEndpointResult createPlatformEndpoint(Platform platform, String customData, String platformToken, String applicationArn) {
		CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();

		platformEndpointRequest.setCustomUserData(customData);

		String token = platformToken;
		String userId = null;

		if (platform == Platform.BAIDU) {
			String[] tokenBits = platformToken.split("\\|");
			token = tokenBits[0];
			userId = tokenBits[1];

			Map<String, String> endpointAttributes = new HashMap<String, String>();

			endpointAttributes.put("UserId", userId);
			endpointAttributes.put("ChannelId", token);

			platformEndpointRequest.setAttributes(endpointAttributes);
		}

		platformEndpointRequest.setToken(token);
		platformEndpointRequest.setPlatformApplicationArn(applicationArn);

		return snsClient.createPlatformEndpoint(platformEndpointRequest);
	}

	private void deletePlatformApplication(String applicationArn) {
		DeletePlatformApplicationRequest request = new DeletePlatformApplicationRequest();
		request.setPlatformApplicationArn(applicationArn);
		snsClient.deletePlatformApplication(request);
	}

	private PublishResult publish(String endpointArn, Platform platform, Map<Platform, Map<String, MessageAttributeValue>> attributesMap, String msg) {
		PublishRequest publishRequest = new PublishRequest();
		Map<String, MessageAttributeValue> notificationAttributes = getValidNotificationAttributes(attributesMap.get(platform));

		if (notificationAttributes != null && !notificationAttributes.isEmpty()) {
			publishRequest.setMessageAttributes(notificationAttributes);
		}

		publishRequest.setMessageStructure("json");
		// If the message attributes are not set in the requisite method, notification is sent with default attributes
		String message = msg;

		Map<String, String> messageMap = new HashMap<String, String>();
		messageMap.put(platform.name(), message);
		message = SNSMessageGenerator.jsonify(messageMap);
		// For direct publish to mobile end points, topicArn is not relevant.
		// publishRequest.setTopicArn(topicArn);
		publishRequest.setTargetArn(endpointArn);

		// Display the message that will be sent to the endpoint
		logger.info("{Message Body: " + message + "}");

		StringBuilder builder = new StringBuilder();

		builder.append("{Message Attributes: ");

		for (Map.Entry<String, MessageAttributeValue> entry : notificationAttributes.entrySet()) {
			builder.append("(\"" + entry.getKey() + "\": \"" + entry.getValue().getStringValue() + "\"),");
		}

		builder.deleteCharAt(builder.length() - 1);
		builder.append("}");

		logger.info(builder.toString());

		publishRequest.setMessage(message);

		return snsClient.publish(publishRequest);
	}

	public void GCMNotification(EntityManager em, Platform platform, String principal, String credential, String platformToken, String applicationName, Map<Platform, Map<String, MessageAttributeValue>> attrsMap, String msg, String msgContent, MessageQueue tmpMessageQueues, AmazonSNS sns) {
		// Create Platform Application. This corresponds to an app on a platform.
		CreatePlatformApplicationResult platformApplicationResult = createPlatformApplication(applicationName, platform, principal, credential);
		logger.info(ReflectionToStringBuilder.toString(platformApplicationResult,ToStringStyle.SHORT_PREFIX_STYLE));
		// The Platform Application Arn can be used to uniquely identify the Platform Application.
		String platformApplicationArn = platformApplicationResult.getPlatformApplicationArn();

		Gson gson = new GsonBuilder().create();
		String deviceIds = arrayConverter(gson.fromJson(tmpMessageQueues.getTargetId(), String[].class));

		@SuppressWarnings("unchecked")
		List<MemberDevice> memberDevices = em.createNativeQuery("SELECT DISTINCT ON (device_token) * FROM member_device WHERE id IN (" + deviceIds + ") ORDER BY device_token, udt::varchar::timestamp DESC", MemberDevice.class).getResultList();

		if (memberDevices.size() > 0) {
			HashMap<String, MemberDevice> failTokensMap = new HashMap<>();

			em.getTransaction().begin();

			for (MemberDevice tmpMemberDevice : memberDevices) {
				try {
					// Local DateTime
					java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(tmpMessageQueues.getStartDate().substring(0, 10).replace('/', '-') + " " + tmpMessageQueues.getSendTime() + ":00");

					Message messageContent = new Message();

					messageContent.setMember(tmpMemberDevice.getMember());
					messageContent.setMessageId(tmpMessageQueues.getMessageId());
					messageContent.setDeviceId(tmpMemberDevice.getDeviceId());
					messageContent.setContent(msgContent);
					messageContent.setSendTime(nowTime);
					messageContent.setLastUpdated(nowTime);

					em.persist(messageContent);

					CreatePlatformEndpointResult platformEndpointResult = createPlatformEndpoint(platform, "CustomData - Useful to store endpoint specific data", tmpMemberDevice.getDeviceToken(), platformApplicationArn);

					// Publish a push notification to an Endpoint.
					PublishResult publishResult = publish(platformEndpointResult.getEndpointArn(), platform, attrsMap, msg);

					logger.info("Published! \n{MessageId=" + publishResult.getMessageId() + "}");
				} catch (Exception e) {
					// collect failed token
					failTokensMap.put(tmpMemberDevice.getMember().getBmemberId(), tmpMemberDevice);

					// handle fail push (skip it)
					logger.error("Fail Publish:" + tmpMemberDevice.getMember().getBmemberId() + " / " + tmpMemberDevice.getDeviceId() + " at " + java.sql.Timestamp.valueOf(LocalDateTime.now()));
				}
			}

			em.getTransaction().commit();

			// delete failed token from member device
			for (HashMap.Entry<String, MemberDevice> deviceObj : failTokensMap.entrySet()) {
				changeDeviceStatus(em, deviceObj.getValue());
			}
		}

		deletePlatformApplication(platformApplicationArn);
	}

	public void APNSNotification(EntityManager em, Platform platform, String principal, String credential, String platformToken, String applicationName, Map<Platform, Map<String, MessageAttributeValue>> attrsMap, String msg, String msgContent, MessageQueue tmpMessageQueues, AmazonSNS sns) {
		// Create Platform Application. This corresponds to an app on a platform.
		CreatePlatformApplicationResult platformApplicationResult = createPlatformApplication(applicationName, platform, principal, credential);
		logger.info(ReflectionToStringBuilder.toString(platformApplicationResult,ToStringStyle.SHORT_PREFIX_STYLE));
		// The Platform Application Arn can be used to uniquely identify the Platform Application.
		String platformApplicationArn = platformApplicationResult.getPlatformApplicationArn();

		Gson gson = new GsonBuilder().create();
		String deviceIds = arrayConverter(gson.fromJson(tmpMessageQueues.getTargetId(), String[].class));

		@SuppressWarnings("unchecked")
		List<MemberDevice> memberDevices = em.createNativeQuery("SELECT DISTINCT ON (device_token) * FROM member_device WHERE id IN (" + deviceIds + ") ORDER BY device_token, udt::varchar::timestamp DESC", MemberDevice.class).getResultList();

		if (memberDevices.size() > 0) {
			HashMap<String, MemberDevice> failTokensMap = new HashMap<>();

			em.getTransaction().begin();

			for (MemberDevice tmpMemberDevice : memberDevices) {
				try {
					// Local DateTime
					java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(tmpMessageQueues.getStartDate().substring(0, 10).replace('/', '-') + " " + tmpMessageQueues.getSendTime() + ":00");

					Message messageContent = new Message();

					messageContent.setMember(tmpMemberDevice.getMember());
					messageContent.setMessageId(tmpMessageQueues.getMessageId());
					messageContent.setDeviceId(tmpMemberDevice.getDeviceId());
					messageContent.setContent(msgContent);
					messageContent.setSendTime(nowTime);
					messageContent.setLastUpdated(nowTime);

					em.persist(messageContent);

					CreatePlatformEndpointResult platformEndpointResult = createPlatformEndpoint(platform, "CustomData - Useful to store endpoint specific data", tmpMemberDevice.getDeviceToken(), platformApplicationArn);

					// Publish a push notification to an Endpoint.
					PublishResult publishResult = publish(platformEndpointResult.getEndpointArn(), platform, attrsMap, msg);

					logger.info("Published! \n{MessageId=" + publishResult.getMessageId() + "}");
				} catch (Exception e) {
					// collect failed token
					failTokensMap.put(tmpMemberDevice.getMember().getBmemberId(), tmpMemberDevice);

					// handle fail push (skip it)
					logger.error("Fail Publish:" + tmpMemberDevice.getMember().getBmemberId() + " / " + tmpMemberDevice.getDeviceId() + " at " + java.sql.Timestamp.valueOf(LocalDateTime.now()));
				}
			}

			em.getTransaction().commit();

			// delete failed token from member device
			for (HashMap.Entry<String, MemberDevice> deviceObj : failTokensMap.entrySet()) {
				changeDeviceStatus(em, deviceObj.getValue());
			}
		}

		deletePlatformApplication(platformApplicationArn);
	}

	private void changeDeviceStatus(EntityManager postgres, MemberDevice tmpMemberDevice) {
		if (null != tmpMemberDevice) {
			try {
				@SuppressWarnings("unchecked")
				List<MemberDevice> memberDevices = postgres.createNativeQuery("SELECT DISTINCT ON (device_token) * FROM member_device WHERE device_token = '" + StringHelper.escapeSQL(tmpMemberDevice.getDeviceToken()) + "' ORDER BY device_token, udt::varchar::timestamp DESC", MemberDevice.class).getResultList();

				postgres.getTransaction().begin();

				for (MemberDevice memberDevice : memberDevices) {
					memberDevice.setDeviceToken(null);
					postgres.persist(memberDevice);
				}

				postgres.getTransaction().commit();
			} finally {
				if (postgres.getTransaction().isActive()) {
					postgres.getTransaction().rollback();
				}
			}
		}
	}

	public void demoNotification(EntityManager em, Platform platform, String principal, String credential, String platformToken, String applicationName, Map<Platform, Map<String, MessageAttributeValue>> attrsMap, String msg, String msgContent, MessageQueue tmpMessageQueues, AmazonSNS sns) {

	}

	public static Map<String, MessageAttributeValue> getValidNotificationAttributes(Map<String, MessageAttributeValue> notificationAttributes) {
		Map<String, MessageAttributeValue> validAttributes = new HashMap<String, MessageAttributeValue>();

		if (notificationAttributes == null) {
			return validAttributes;
		}

		for (Map.Entry<String, MessageAttributeValue> entry : notificationAttributes.entrySet()) {
			if (!SNSStringUtils.isBlank(entry.getValue().getStringValue())) {
				validAttributes.put(entry.getKey(), entry.getValue());
			}
		}

		return validAttributes;
	}

	public static String arrayConverter(String[] arrayStr) {
		StringBuilder sb = new StringBuilder();

		for (String st : arrayStr) {
			sb.append(st).append(',');
		}

		if (arrayStr.length != 0) {
			sb.deleteCharAt(sb.length() - 1);
		}

		return sb.toString();
	}
}