package digipages.dev.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.catalina.connector.ClientAbortException;
import org.apache.tomcat.util.buf.HexUtils;

import digipages.common.DPLogger;

public class Enc01 {

	private static DPLogger logger = DPLogger.getLogger(Enc01.class.getName());
	
	public final static String EncryptType = "enc01"; 
	
	private String downloadToken = "";

	public Enc01(String key, String DownloadToken) {
		this.downloadToken = DownloadToken;
	}

	// for jsp sample.
	public byte[] encode(String filepath, byte[] src) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		byte[] ret = new byte[src.length];
		byte key[] = getRealKey(downloadToken, filepath);
		int loc = 0;
		while (loc < src.length) {
			encRange(src, loc, key.length, ret, loc, key);
			loc += key.length;
		}
		return ret;
	}
	
	private void encRange(byte[] src, int loc, int length, byte[] ret, int loc2, byte key[]) {
		for (int i = 0; i < length && i + loc < src.length; i++) {
			int xloc = loc + i;
			ret[xloc] = (byte) (src[xloc] ^ key[i]);
		}

	}

	// xor =decode = encode;
	public byte[] decode(String filePath, byte[] src) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return encode(filePath, src);
	}

	// revert back to private after debug
	public static byte[] getRealKey(String DownloadToken, String filePath)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] realKey ;
		filePath=URLDecoder.decode(filePath,"UTF-8");
		int offset = calcOffset(filePath);
		if (offset >=DownloadToken.length())
			throw new NoSuchAlgorithmException();
		String newSeed = DownloadToken.substring(0, offset) + filePath + 
				DownloadToken.substring(offset);
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(newSeed.getBytes("UTF-8"));
		realKey = md.digest();
		return realKey;
	}

	// revert back to private after debug
	public static int calcOffset(String filePath) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(filePath.getBytes("UTF-8"));
		byte[] x = md.digest();

		int sum = 0;
		for (int i = 0; i < x.length; i += 2) {

			byte tmp[] = new byte[2];
			System.arraycopy(x, i, tmp, 0, 2);
			int value = byteArrayToSmallInt(tmp);
			sum += value;
		}
		int offset = sum % 64;

		return offset;
	}

	public static int byteArrayToSmallInt(byte[] b) {
		return b[1] & 0xFF | (b[0] & 0xFF) << 8;
	}

	
	private static final int DEFAULT_BUFFER_SIZE = 10240;  //bytes = 10 KB

	public void encrypt_copy(InputStream input, OutputStream output, String path, boolean encrypt)
			throws IOException, NoSuchAlgorithmException {
		encrypt_copy(input,output,0,path,encrypt);
	}

	public void encrypt_copy(InputStream input, OutputStream output, long start, String path,
			boolean encrypt) throws NoSuchAlgorithmException, IOException {
		byte key[] =  getRealKey(downloadToken, path);
//		logger.info("Eric Debug: Enc01 Called, Token="+ downloadToken
//				+" path=" + path + 
//				" , key=" + HexUtils.toHexString(key) );
		
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int read;
        long index = 0;
        // since inputStream is already skipped, this skip is not used.
//        input.skip(start);
        while ((read = input.read(buffer)) > 0) {

        	if(encrypt) {
        		for (int i = 0; i < read; i++){
        			buffer[i] = (byte)(buffer[i] ^ key[(int)(index % key.length)]);
        			index++;
        		}
        	}
        	try {
        		output.write(buffer, 0, read);
        	} catch (ClientAbortException ex)
        	{
        		break;
        		// don't print anything when client abort
        	}
        }
		
	}

}
