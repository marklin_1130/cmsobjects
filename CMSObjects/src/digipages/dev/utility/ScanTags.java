package digipages.dev.utility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import digipages.common.DPLogger;

public class ScanTags {
	private static DPLogger logger = DPLogger.getLogger(ScanTags.class.getName());
	private static Set<String> tagNames=new TreeSet<>();
	public ScanTags() {
		
	}

	public static void main(String[] args) {
		try{
			String path ="/tmp/fixed_layout_samples";
			File parent=new File(path);
			ScanTags.scanDir(parent);
			ScanTags.writeTags("/tmp/fixed_layout_tags.txt");
		}catch (Exception e)
		{
			logger.error("Fail Scan item.",e);
		}

	}

	private static void writeTags(String string) throws IOException {
		File f = new File(string);
		FileWriter fw = new FileWriter(f);
		for (String tmpTag:tagNames)
		{
			fw.append(tmpTag).append("\n");
		}
		fw.close();
	}

	private static void scanDir(File curFile) throws IOException {
		String fname=curFile.getName(); 
		if (fname.endsWith(".xhtml"))
		{
			handleHtmlFile(curFile);
		}else {
			if (curFile.isDirectory()){
				//
			}if (fname.endsWith(".png") 
					|| fname.endsWith(".jpg")
					|| fname.endsWith(".css")
					|| fname.endsWith(".opf")
					|| fname.endsWith(".xml")
					|| !fname.contains("\\.")
					){
				//
			}else{
				logger.info("skip file:" + curFile.getName());
			}
		}
		if (curFile.isDirectory())
		{
			for (File child:curFile.listFiles())
			{
				scanDir(child);
			}
		}
		
	}

	private static void handleHtmlFile(File file) throws IOException {
		Document d=Jsoup.parse(file,"UTF-8");
		parseNode(d);
		
	}

	private static void parseNode(Node d) {
		
		tagNames.add(d.nodeName());
		if (d.childNodeSize()>0)
		{
			for (Node child:d.childNodes())
				parseNode(child);
		}
		
	}

}
