package digipages.dev.utility;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.boon.json.JsonFactory;

import digipages.common.CommonUtil;
import digipages.exceptions.ServerException;
import model.BookFile;
import model.BookUniId;
import model.BookVersion;
import model.Member;
import model.MemberBook;

public class BookUpgrader {
	private static final Logger logger = LogManager.getLogger(BookUpgrader.class);

	public static void upgradeMemberBookVersion(EntityManager postgres, BookFile newBook) throws ServerException {
		try {

			boolean thirdPartyOnly=false;
			if (newBook.getStatus()==8)
				thirdPartyOnly=true;
			BookFile maxVersionBookFile = newBook;
			// check which member books affected.
			List<MemberBook> memberBooks = postgres.createNamedQuery("MemberBook.findAllByBookFileIdAndVersionLocked", MemberBook.class)
					.setParameter("bookUniId", maxVersionBookFile.getBookUniId())
					.setParameter("versionLocked", false)
					.getResultList();

			// nothing affected.
			if (null == memberBooks || memberBooks.isEmpty()){
				logger.info("no member affected, end upgrade process");
				return;
			}
			
			for (MemberBook memberBook : memberBooks) {
				try {
					Member curMember = memberBook.getMember();

					// skip 8 (in audit) for normal users.
					if (curMember.isNormalReader() && thirdPartyOnly)
					{
						continue;
					}
										
					upgreadMemberBook(postgres, memberBook, maxVersionBookFile);
				} catch (javax.persistence.RollbackException rex) {
					logger.error("upgradeMemberBookVersion", rex);
					logger.error("rollback, Memberid=" + memberBook.getMember().getId() + " BookUniId=" + memberBook.getBookUniId());
				}
			}
		} catch (javax.persistence.RollbackException rex) {
			throw new ServerException("id_err_999", "Error Writing data to db");
		} catch (Exception e) {
			logger.error("upgradeMemberBookVersion", e);
			String errJsonMessage = JsonFactory.toJson(e);
			throw new ServerException("id_err_999", errJsonMessage);
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}
	}

	public static MemberBook upgreadMemberBook(EntityManager postgres, MemberBook memberBook, BookFile maxVersionBookFile) {
		BookVersion curVersion = new BookVersion(memberBook.getBookFile().getVersion());
		BookVersion recentVersion = new BookVersion(maxVersionBookFile.getVersion());
		
		if("audiobook".equalsIgnoreCase(memberBook.getBookType())||"mediabook".equalsIgnoreCase(memberBook.getBookType())) {
		    curVersion = new BookVersion(memberBook.getCurVersion());
		}
		
		int hightLightCnt = memberBook.getHighLights(postgres, memberBook).size();
		int bookMarkCnt = memberBook.getBookMarks(postgres, memberBook).size();

		boolean bookmarkModified = hightLightCnt > 0 || bookMarkCnt > 0;

		// Major Change and item affected
		if (memberBook.getMember().isThirdParty()) {
			if (recentVersion.isMarjorChange(curVersion) || recentVersion.isMinorChange(curVersion)) {
				// major / minor change (不管大小版更,所有出版社/供應商/博客來管理者帳號的書籍都強制升級到最新版)
				memberBook.setBookFile(maxVersionBookFile);
				memberBook.setAskUpdateVersion(false);
				memberBook.setBgUpdateVersion(true);
				memberBook.setBookHighlightStatus((hightLightCnt < 1 ? false : true));
				memberBook.setBookBookmarkStatus((bookMarkCnt < 1 ? false : true));
				memberBook.setCurVersion(maxVersionBookFile.getVersion());
				memberBook.setUpgradeTime(java.sql.Timestamp.valueOf(LocalDateTime.now()));
				memberBook.setAction("update");
				memberBook.setLastUpdated(CommonUtil.zonedTime());
			} else {
				// version same, nothing to do. (版號相同,不做任何更動)
				return memberBook;
			}
		} else {
			if (recentVersion.isMarjorChange(curVersion) && bookmarkModified) {
				// major change: waiting (大版更,書籍有畫線註記,需要等使用者同意書籍版號才能升版)
				// 不需要 setBookFile
				memberBook.setAskUpdateVersion(true);
				memberBook.setBgUpdateVersion(false);
				memberBook.setBookHighlightStatus((hightLightCnt < 1 ? false : true));
				memberBook.setBookBookmarkStatus((bookMarkCnt < 1 ? false : true));
				// 不需要 setCurVersion
				// 不需要 setUpgradeTime
				memberBook.setAction("update");
				memberBook.setLastUpdated(CommonUtil.zonedTime());
			} else if (recentVersion.isMarjorChange(curVersion) && !bookmarkModified) {
				// major change, no Note (大版更,書籍沒有畫線註記,通知App書籍背景升版)
				memberBook.setBookFile(maxVersionBookFile);
				memberBook.setAskUpdateVersion(false);
				memberBook.setBgUpdateVersion(true);
				memberBook.setBookHighlightStatus((hightLightCnt < 1 ? false : true));
				memberBook.setBookBookmarkStatus((bookMarkCnt < 1 ? false : true));
				memberBook.setCurVersion(maxVersionBookFile.getVersion());
				memberBook.setUpgradeTime(java.sql.Timestamp.valueOf(LocalDateTime.now()));
				memberBook.setAction("update");
				memberBook.setLastUpdated(CommonUtil.zonedTime());
			} else if (recentVersion.isMinorChange(curVersion)) {
				// minor change, or not affected (no Note) (小版更,通知App書籍背景升版)
				memberBook.setBookFile(maxVersionBookFile);
				memberBook.setAskUpdateVersion(false);
				memberBook.setBgUpdateVersion(true);
				memberBook.setBookHighlightStatus((hightLightCnt < 1 ? false : true));
				memberBook.setBookBookmarkStatus((bookMarkCnt < 1 ? false : true));
				memberBook.setCurVersion(maxVersionBookFile.getVersion());
				memberBook.setUpgradeTime(java.sql.Timestamp.valueOf(LocalDateTime.now()));
				memberBook.setAction("update");
				memberBook.setLastUpdated(CommonUtil.zonedTime());
			} else {
				// version same, nothing to do. (版號相同,不做任何更動)
				return memberBook;
			}
		}

		postgres.getTransaction().begin();
		postgres.persist(memberBook);
		postgres.getTransaction().commit();

		return memberBook;
	}

	private static BookFile findMaxVersionBookFileForThirdPartyAccount(EntityManager postgres, String bookUniId) throws ServerException {
		BookFile maxBookFile = null;
		boolean isTrial = bookUniId.endsWith("_trial");
		BookUniId buid = new BookUniId(bookUniId);
		String itemID = buid.getItemId();

		BookVersion maxVersion = new BookVersion("V000.0000");

		List<BookFile> getBookFiles = postgres.createNamedQuery("BookFile.findByItemIdForThirdPartyAccount", BookFile.class)
				.setParameter("item_id", itemID)
				.getResultList();

		for (BookFile bf : getBookFiles) {
			// skip non match trial flag.
			if (bf.getIsTrial() != isTrial)
				continue;
			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
				maxBookFile = bf;
			}
		}

		return maxBookFile;
	}

	private static BookFile findMaxVersionBookFileForNormalReader(EntityManager postgres, String bookUniId) throws ServerException {
		BookFile maxBookFile = null;
		boolean isTrial = bookUniId.endsWith("_trial");
		BookUniId buid = new BookUniId(bookUniId);
		String itemID = buid.getItemId();

		BookVersion maxVersion = new BookVersion("V000.0000");

		List<BookFile> getBookFiles = postgres.createNamedQuery("BookFile.findByItemIdForNormalReader", BookFile.class)
				.setParameter("item_id", itemID)
				.getResultList();

		for (BookFile bf : getBookFiles) {
			// skip non match trial flag.
			if (bf.getIsTrial() != isTrial)
				continue;
			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
				maxBookFile = bf;
			}
		}

		return maxBookFile;
	}
}