package digipages.dev.utility;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;

import javax.persistence.EntityManager;

import org.kohsuke.randname.RandomNameGenerator;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.BaseBookInfo;
import model.BookFile;
import model.Item;
import model.Member;
import model.MemberBook;
import model.MemberBookNote;
import model.MemberDevice;
import model.MemberDrmLog;
import model.NoteContent;
import model.RecommendBook;

public class BenchDataGenerator {
	
	private static DPLogger logger =DPLogger.getLogger(BenchDataGenerator.class.getName());
	
	private EntityManager em;
	private static int membercnt=100;
	private static int memberBookMaxSize = 200;
	private static int memberNoteMaxSize = 40;
	private int totalMemberBookCnt=0;
	private int totalMemberDrmLogCnt=0;
	private Vector<BookFile> benchBookFiles;

	private static RandomNameGenerator rname = new RandomNameGenerator();
	private static Random rnd = new Random();
	private static PowerLaw pwRnd = new PowerLaw();
	private static Vector<BookFile> sampleBookFiles= new Vector<>();
	
	public static String randomName()
	{
		return rname.next();
	}
	
	public BenchDataGenerator(EntityManager entityManager)
	{
		this.em=entityManager;
		loadSampleBookFiles();
	}
	
	@SuppressWarnings("unchecked")
	private void loadSampleBookFiles() {
		sampleBookFiles = (Vector<BookFile>) 
				em.createNativeQuery("select * from book_file where item_id not like 'bench_sample%' and status=9",BookFile.class).getResultList();
	}
	
	public void genBenchMembers(int count)
	{
		loadBenchmarkBooks();
		membercnt=count;
		try {
			em.getTransaction().begin();
			for (int i=0; i< membercnt; i++)
			{
				generateBenchMember();
				if (i%101==0)
					em.flush();
			}
			em.getTransaction().commit();
		} catch (ServerException e) {
			logger.error(e);;
		}finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		}
	}
	public void generateBenchMember() throws ServerException
	{
		Member m = new Member();
		m.setBmemberId("bench_sample_"+rname.next());
		m.setMemberType("NormalReader");
		m.setName(rname.next());
		m.setNick(rname.next());
		em.persist(m);
		generateMemberData(em,m);
	}
	public void generateMemberData(EntityManager em,Member m) throws ServerException
	{
		genMemberReadList(m);
		genMemberDevices(m);
		genMemberBooks(em,m,0);
	}
	
	
	public void genMemberBooks(EntityManager em,Member m, int size) throws ServerException {
		
		// each Member has power low distribution of max xxx books;
		int bookCnt = pwRnd.zipf(memberBookMaxSize)+1;
		// if size exists
		if (size>0)
			bookCnt=size;
		
		int curBookCnt=0;
		while (curBookCnt<size){
			BookFile bf = sampleBookFiles.get(rnd.nextInt(sampleBookFiles.size()));
			if (null==m.getMemberBookByBookFileId(em,bf.getId()))
			{	
				MemberBook mb = genMemberBook(m,bf);
				m.addMemberBook(mb);
				totalMemberBookCnt ++;
				curBookCnt++;
			}
		}
	}
	public void genMemberReadList(Member m) {
		m.genDefaultReadLists(em);
	}
	private void genMemberDevices(Member m) throws ServerException {
//		List <MemberDevice> devices = new ArrayList<>();
		// each has 2 devices (prevent new device from registering) 
		for (int i=0; i< 2;i++)
		{
			MemberDevice md = new MemberDevice();
			md.setDeviceId(rname.next());
			md.setDeviceModel(rname.next());
			md.setDeviceVendor(rname.next());
			md.setLanguage("zh_TW");
			
			md.setLastOnlineIp(genRandomIP());
			md.setLastOnlineTime(LocalDateTime.now());
			md.setMember(m);
			md.setOsType("ios");
			md.setOsVersion("1.0");
			md.setScreenDpi("400.5");
			md.setScreenResolution("1920x1080");
			m.addMemberDevice(md);
//			em.persist(md);
//			devices.add(md);
		}
//		m.setMemberDevices(devices);
	}
	private static String genRandomIP() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i< 4; i++)
		{
			sb.append(rnd.nextInt(253)+1);
			sb.append(".");
		}
		//remove tail dot
		sb.setLength(sb.length()-1);
		return sb.toString();
	}
	@SuppressWarnings("unchecked")
	private void loadBenchmarkBooks() {
	 
		benchBookFiles = (Vector<BookFile>) 
				em.createNativeQuery("select * from book_file where item_id like 'bench_sample%'",BookFile.class).getResultList();
	}
	
	/**
	 * warning, if you have no books in advance, this function will not fun properly
	 * @param count
	 */
	public void genBenchBooksFromSample(int count)
	{
		for (int i=0; i<count; i++)
		{
			int randIdx=rnd.nextInt(sampleBookFiles.size());
			BookFile sampleBook = sampleBookFiles.get(randIdx);
			
			if (sampleBook==null || sampleBook.getItem()==null){
				logger.error("empty Item/bookFile:" + sampleBook.getId());
				i--;
				continue;
			}
			if (sampleBook.getItem().getId().contains("bench_sample_"))
			{
				logger.error("Unexcepted item in select");
			} 
			
			
			BookFile targetbf = CommonUtil.jsonClone(sampleBook, BookFile.class);
			targetbf.setId(null);
			targetbf.setMemberBooks(new ArrayList<MemberBook>());
			targetbf.setRecommendBooks(new ArrayList<RecommendBook>());

			
			Item targetItem = CommonUtil.jsonClone(sampleBook.getItem(),Item.class);
			targetItem.setId("bench_sample_" + UUID.randomUUID().toString());
			targetItem.setBookFiles(new ArrayList<BookFile>());
			targetbf.setItem(targetItem);
			targetItem.setMemberDrmLogs(new ArrayList<>());
			targetItem.addBookFile(targetbf);
			
			em.persist(targetItem);
		}
	}
	
	private MemberBook genMemberBook(Member m, BookFile b) throws ServerException {
		
		MemberBook mb = new MemberBook();
		
		mb.setBookFile(b);
		mb.setItem(b.getItem());
		mb.setBookUniId(b.getBookUniId());
		genMemberBookNotes(mb);
		mb.setBookType(b.getItem().getInfo().getType());
		mb.setDefaultReadList(mb);

		if (b.getIsTrial())
		{
			mb.setEncryptType(MemberBook.EncryptTypeNone);
		}else
		{
			byte[] encryptKeykey={0x00};
			try {
				encryptKeykey = CommonUtil.generateAES256Key();
			} catch (NoSuchAlgorithmException e) {
				throw new ServerException("id_err_999","Internal Error:" + e.getMessage());
			}
			mb.setEncryptKey(encryptKeykey);
			mb.setEncryptType(Enc01.EncryptType);
		}
		genMemberDrmLog(m);

//		em.persist(mb);
		return mb;
	}
	
	private void genMemberBookNotes(MemberBook mb) {
		int anCnt=pwRnd.zipf(memberNoteMaxSize)+1;
		for (int i=0; i< anCnt; i++)
		{
			MemberBookNote mbNote = new MemberBookNote();
			UUID uuid = UUID.randomUUID();
			mbNote.setUuid(uuid);
			mbNote.setIsPublic(false);
			mbNote.setType("highlight");
			mbNote.setHighlightText(rname.next());
			mbNote.setLikeCnt((long) 0);
			mbNote.setSpine(rnd.nextDouble());
			mbNote.setColor("blue");
			mbNote.setName(mb.getMember().getName());
			mbNote.setNote(rname.next());
			mbNote.setNoteContent(genSampleNoteContent());
			
			mb.addMemberBookNote(mbNote);
			mbNote.setMemberBook(mb);
			
		}

	}

	
	private NoteContent genSampleNoteContent() {
		NoteContent content = new NoteContent();
		content.setAuthor_nick(rname.next());
		content.setBookformat(BaseBookInfo.BookFormatReflowable);
		content.setCfi("epubcfi(/6/12[ch1.xhtml]!4[字靈]/4/12/1:32,1:33)");
		content.setChapter("5");
		content.setColor("blue");
		
		content.setHighlight_text(rname.next());
		content.setNote(rname.next());
		content.setOrigAction("");
		content.setPage(String.valueOf(rnd.nextDouble()));
		content.setText(rname.next());
		return content;
	}

	public void genMemberDrmLog(Member m) {
		
		
		for (MemberBook mb: m.getMemberBooks())
		{
			if (mb.getBookFile().getIsTrial())
				continue;
			// each book has most 6 drmInfo, least 1
			int drmCnt = pwRnd.zipf(2)+1;
			for (int i=0; i< drmCnt; i++)
			{
				
				MemberDrmLog memberDrmLog=generateDrmLog(mb);
				m.addMemberDrmLog(memberDrmLog);
				

				totalMemberDrmLogCnt++;
			}
		}
		return;

	}
	
	private MemberDrmLog generateDrmLog(MemberBook mb) {
		MemberDrmLog mdl = new MemberDrmLog();

		mdl.setItem(mb.getItem());
		mdl.setLastUpdated(CommonUtil.zonedTime());
		mdl.setMember(mb.getMember());
		mdl.setStatus(1); // authorize
		mdl.setTransactionId(UUID.randomUUID().toString());
		mdl.setType(1);
		
		
		return mdl;
	}

	public static int getMemberBookMaxSize() {
		return memberBookMaxSize;
	}

	public static void setMemberBookMaxSize(int memberBookMaxSize) {
		BenchDataGenerator.memberBookMaxSize = memberBookMaxSize;
	}

	public static int getMemberNoteMaxSize() {
		return memberNoteMaxSize;
	}

	public static void setMemberNoteMaxSize(int memberNoteMaxSize) {
		BenchDataGenerator.memberNoteMaxSize = memberNoteMaxSize;
	}


}
