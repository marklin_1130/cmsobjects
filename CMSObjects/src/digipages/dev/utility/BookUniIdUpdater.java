package digipages.dev.utility;

import java.util.List;

import javax.persistence.EntityManager;

import digipages.common.DPLogger;
import model.MemberBook;

// run once only, when the memberbook lacks book_uni_id, do it.
public class BookUniIdUpdater {
	
	private static DPLogger logger =DPLogger.getLogger(BenchDataGenerator.class.getName());
	
	public static void update(EntityManager em)
	{
		try {
			
			em.getTransaction().begin();
			List<MemberBook> memberBooks=em.createNamedQuery("MemberBook.findAll",MemberBook.class).getResultList();
			
			for (MemberBook mb:memberBooks){
				if (mb==null || mb.getBookFile()==null)
					continue;
				mb.setBookUniId(mb.getBookFile().getBookUniId());
			}
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			logger.error(ex);;
		}finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();

		}
	}
}
