package digipages.dev.utility;

import javax.persistence.EntityManager;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;

import digipages.common.DPLogger;
import model.MessageQueue;

/**
 * 使用Amazon SNS和APNS向iOS App發送推送通知，根據所用的APNS憑證設定SNSMobilePush.java
 * 正式上線使用 snsPush.pushAppleAppNotification()
 * 開發中使用 snsPush.pushAppleSandboxAppNotification()
 * 檔案位置：CMSObjects/src/digipages/dev/utility/SNSMobilePush.java
 * Production環境：snsPush.pushAppleAppNotification() -> 正式上線
 * Development環境：snsPush.pushAppleSandboxAppNotification() -> 開發中
 */
public class SNSMobilePush {
	
	private static DPLogger logger = DPLogger.getLogger(SNSMobilePush.class.getName());
	
	@Deprecated
	public static void push(EntityManager postgres, MessageQueue mq, String gcmServerAPIKey, String gcmApplicationName, String gcmRegistrationId, String apnsCertificate, String apnsPrivateKey, String apnsApplicationName, String apnsDeviceToken) {
		logger.info("Push: " + mq.getId());


		AmazonSNS sns = new AmazonSNSClient();

		sns.setEndpoint("https://sns.ap-northeast-1.amazonaws.com");

		logger.info("===========================================\n");
		logger.info("Getting Started with Amazon SNS");
		logger.info("===========================================\n");

		try {
			SNSAppNotification snsPush = new SNSAppNotification(sns);

			switch (mq.getOsType().toLowerCase()) {
				case "android":
					snsPush.pushAndroidAppNotification(postgres, sns, mq, gcmServerAPIKey, gcmApplicationName, gcmRegistrationId);
					break;
				case "ios":
					// APNS
					snsPush.pushAppleAppNotification(postgres, sns, mq, apnsCertificate, apnsPrivateKey, apnsApplicationName, apnsDeviceToken);
					// APNS_SANDBOX
					// snsPush.pushAppleSandboxAppNotification(postgres, sns, mq, apnsCertificate, apnsPrivateKey, apnsApplicationName, apnsDeviceToken);
					break;
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon SNS, but was rejected with an error response for some reason.");
			logger.info("Error Message: " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code: " + ase.getErrorCode());
			logger.info("Error Type: " + ase.getErrorType());
			logger.info("Request ID: " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			logger.info("Caught an AmazonClientException, which means the client encountered a serious internal problem while trying to communicate with SNS, such as not being able to access the network.");
			logger.info("Error Message: " + ace.getMessage());
		} catch (Exception e) {
			logger.info("Exception: " + e.toString());
		}
	}
}