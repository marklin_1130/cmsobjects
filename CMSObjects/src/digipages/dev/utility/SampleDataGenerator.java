package digipages.dev.utility;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.commons.lang3.StringUtils;
import org.boon.json.JsonFactory;
//import static org.boon.json.JsonFactory.*;
import org.kohsuke.randname.RandomNameGenerator;

import com.google.gson.Gson;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.dto.MemberDrmLogDTO;
import digipages.exceptions.ServerException;
import model.BaseBookInfo;
import model.BookFile;
import model.BookInfo;
import model.BookVersion;
import model.CmsToken;
import model.Item;
import model.MagInfo;
import model.Member;
import model.MemberBook;
import model.MemberBookDrmMapping;
import model.MemberBookNote;
import model.MemberDevice;
import model.MemberDrmLog;
import model.NoteContent;
import model.Publisher;
import model.RecommendBook;
import model.Vendor;

public class SampleDataGenerator {
	
	private static DPLogger logger =DPLogger.getLogger(SampleDataGenerator.class.getName());
	
	//	private EntityManagerFactory emf;
	private EntityManager em;
	private static RandomNameGenerator rname = new RandomNameGenerator();
	private static List<Publisher> publishers = new ArrayList<>();
	private List<BookFile> realBookFiles = new ArrayList<>();
	private static Random rnd = new Random();
	private static PowerLaw pwRnd = new PowerLaw();
	private static int membercnt=100;
	private static int memberBookCnt=0;
    private static int memberBookNoteCnt=0;
	private static int memberDrmLogCnt=0;
	
	
	private static ArrayList<String> bookFilePathPdf=new ArrayList<String>();
	private static ArrayList<String> bookFilePathReflow=new ArrayList<String>();
	private static ArrayList<String> bookFilePathFixed=new ArrayList<String>();
	
//	private static ArrayList<BookFile> allBookFile = new ArrayList<>();
	private static ArrayList<String> bookFormats = new ArrayList<>();

	private static ArrayList<BookServletRecord> importBooks = new ArrayList<>();
	private static ArrayList<BookServletRecord> importMags = new ArrayList<>();
	
	private static Vector<BookFile> sampleBookFiles= new Vector<>();
	
	private static Vector<BookFile> sampleTrialBookFiles= new Vector<>();
	
	private static Vector<BookFile> sampleSerialBookFiles= new Vector<>();
	
	private static Vector<BookFile> benchBookFiles = new Vector<>();
	
	private static Vector<MemberBookNote> benchMemberBookNote = new Vector<>();
	
	public SampleDataGenerator(EntityManager entityManager)
	{
		this.em=entityManager;
		bookFilePathPdf.add("http://appapi.booksdev.benqguru.com:8080/V1.0/CMSStream/ListDir/BookSample/Encrypted/HappyPrince.epub/");
		bookFilePathPdf.add("http://appapi.booksdev.benqguru.com:8080/V1.0/CMSStream/ListDir/BookSample/Encrypted/sample_pdf_001/");
		bookFilePathReflow.add("http://appapi.booksdev.benqguru.com:8080/V1.0/CMSStream/ListDir/BookSample/Encrypted/sample_reflowable_002/");
		bookFilePathFixed.add("http://appapi.booksdev.benqguru.com:8080/V1.0/CMSStream/ListDir/BookSample/Encrypted/sample_fixed_002/");
		bookFilePathFixed.add("http://appapi.booksdev.benqguru.com:8080/V1.0/CMSStream/ListDir/BookSample/Encrypted/sample_fixed_001/");
		bookFormats.add(BaseBookInfo.BookFormatPDF);
		bookFormats.add(BaseBookInfo.BookFormatReflowable);
		bookFormats.add(BaseBookInfo.BookFormatFixedLayout);
	}
	
	@SuppressWarnings("unchecked")
	private void loadSampleBookFiles() {
		sampleBookFiles = (Vector<BookFile>)  
				em.createNativeQuery("select * from book_file where is_trial = false and udt::varchar::timestamp > (current_date-60) and item_id not like 'bench_sample%' and status=9 LIMIT 400",BookFile.class).getResultList();
		        
	}

    @SuppressWarnings("unchecked")
    private void loadSampleTrialBookFiles() {
        sampleTrialBookFiles = (Vector<BookFile>)  
                em.createNativeQuery("select * from book_file where is_trial = true and udt::varchar::timestamp > (current_date-60) and item_id not like 'bench_sample%' and status=9 LIMIT 400",BookFile.class).getResultList();
                
    }
    
	@SuppressWarnings("unchecked")
    private void loadSerialSampleBookFiles() {
	    sampleSerialBookFiles = (Vector<BookFile>)  
                em.createNativeQuery("select b.* from book_file b , item i where b.item_id = i.id and i.item_type = 'serial' and i.childs <> '{}' and b.item_id not like 'bench_sample%' and b.status=9 LIMIT 400",BookFile.class).getResultList();
    }
	
	// tmp item.
	public void setAllFlagToY()
	{
		StringBuilder sb = new  StringBuilder();
		List <Item> items= em.createNamedQuery("Item.findAll",Item.class).getResultList();
		em.getTransaction().begin();
		for (Item i:items)
		{
			BaseBookInfo info = (BaseBookInfo) i.getInfo();
			info.setShare_flag("Y");
			info.setLike_flag("Y");
			info.setNote_flag("Y");
			info.setShare_flag("Y");
			info.setPublic_flag("Y");
			info.setBookmark_flag("Y");
			info.setAnnotation_flag("Y");
			i.setInfo(info);
		}
		em.getTransaction().commit();
	}
	
	public String listShareStat()
	{
		StringBuilder sb = new StringBuilder();
		
		em.getEntityManagerFactory().getCache().evictAll(); 
		List <Item> items= em.createNamedQuery("Item.findAll",Item.class).getResultList();
		for (Item i:items)
		{
			BaseBookInfo info = (BaseBookInfo) i.getInfo();
			sb.append(i.getId()).append(":").append(info.getShare_flag());
			sb.append("\n<br/>");
		}
		return sb.toString();

	}
	
	public void genBenchMembers(int count,int bookcnt,int notecnt)
	{
		membercnt=count;
        memberBookCnt = bookcnt;
        memberBookNoteCnt = notecnt;
		try {
			generateMembers();
		} catch (ServerException e) {
			logger.error(e);;
		}
	}

	public Item genSampleItem()
	{
		if (null==sampleBookFiles || sampleBookFiles.size()<1)
			loadSampleBookFiles();
		BookFile sampleBook=getRandomSampleBook();
		
		if (sampleBook.getItem().getId().contains("bench_sample_"))
		{
			logger.error("Unexcepted item in select");
		} 
		
		
		BookFile targetbf = CommonUtil.jsonClone(sampleBook, BookFile.class);
		targetbf.setId(null);
		targetbf.setMemberBooks(new ArrayList<MemberBook>());
		targetbf.setRecommendBooks(new ArrayList<RecommendBook>());

		
		Item targetItem = CommonUtil.jsonClone(sampleBook.getItem(),Item.class);
		targetItem.setId("bench_sample_" + UUID.randomUUID().toString());
		targetItem.setBookFiles(new ArrayList<BookFile>());
		targetbf.setItem(targetItem);
		targetItem.setMemberDrmLogs(new ArrayList<>());
		targetItem.addBookFile(targetbf);
		
		return targetItem;
	}
	
	public Item genSampleTrialItem()
    {
        if (null==sampleTrialBookFiles || sampleTrialBookFiles.size()<1)
            loadSampleTrialBookFiles();
        BookFile sampleBook=getRandomTrialSampleBook();
        
        if (sampleBook.getItem().getId().contains("bench_sample_"))
        {
        	logger.error("Unexcepted item in select");
        } 
        
        
        BookFile targetbf = CommonUtil.jsonClone(sampleBook, BookFile.class);
        targetbf.setId(null);
        targetbf.setMemberBooks(new ArrayList<MemberBook>());
        targetbf.setRecommendBooks(new ArrayList<RecommendBook>());

        
        Item targetItem = CommonUtil.jsonClone(sampleBook.getItem(),Item.class);
        targetItem.setId("bench_sample_" + UUID.randomUUID().toString());
        targetItem.setBookFiles(new ArrayList<BookFile>());
        targetbf.setItem(targetItem);
        targetItem.setMemberDrmLogs(new ArrayList<>());
        targetItem.addBookFile(targetbf);
        targetItem.setcTitle("bench_" + targetItem.getcTitle());
        
        return targetItem;
    }
	
	
	   public Item genSerialSampleItem()
	    {
	        if (null==sampleSerialBookFiles || sampleSerialBookFiles.size()<1)
	            loadSerialSampleBookFiles();
	        BookFile sampleBook=getRandomSerialSampleBook();
	        
	        if (sampleBook.getItem().getId().contains("bench_sample_"))
	        {
	        	logger.error("Unexcepted item in select");
	        } 
	        
	        
	        BookFile targetbf = CommonUtil.jsonClone(sampleBook, BookFile.class);
	        targetbf.setId(null);
	        targetbf.setMemberBooks(new ArrayList<MemberBook>());
	        targetbf.setRecommendBooks(new ArrayList<RecommendBook>());

	        
	        Item targetItem = CommonUtil.jsonClone(sampleBook.getItem(),Item.class);
	        targetItem.setId("bench_sample_" + UUID.randomUUID().toString());
	        targetItem.setBookFiles(new ArrayList<BookFile>());
	        targetbf.setItem(targetItem);
	        targetItem.setMemberDrmLogs(new ArrayList<>());
	        targetItem.addBookFile(targetbf);
	        
	        return targetItem;
	    }
	
	public void genBooksFromSample(int count)
	{

		for (int i=0; i<count; i++)
		{
			
			em.persist(genSampleItem());
		}
	}

    public void genTrialBooksFromSample(int count) {

        for (int i = 0; i < count; i++) {

            em.persist(genSampleTrialItem());
        }
    }

	// input format : "1234,1234,1234" (bookUniId)
	public void addBookUniIdToAllMember(String list) throws ServerException
	{
		String bookFileIds[] = list.split(",");

		for (String bookid:bookFileIds)
		{
			BookFile bf = BookFile.findLastVersionByBookUniId(em, bookid);
			if (bf==null)
				continue;
			addBookToAllMember(bf);
		}
		
	}

	
	private void addBookToAllMember(BookFile bookFile) throws ServerException {
		List <Member> members = Member.listNormalReaderMembers(em);
		
		if( null==bookFile.getStatus() ){
			throw new ServerException("id_err_999"," bookFile ["+bookFile.getId()+" ] is null <br> ") ;
		}else if( -1==bookFile.getStatus() ){
			throw new ServerException("id_err_999"," bookFile ["+bookFile.getId()+" ] is status is -1 <br>") ;
		}
		
		for (Member member:members)
		{
			if (member.hasItemByBookUniId(em, bookFile.getBookUniId())) {
				MemberBook mb= genMemberBook(member, bookFile) ;
				member.addMemberBook(mb);
				MemberDrmLog memberDrmLog=generateDrmLog(mb);
				member.addMemberDrmLog(memberDrmLog);
				
			}
		}

	}
	
	public void addBookUniIdsToSpecificMember(EntityManager em,String list,String memberId) throws ServerException
	{
		String bookFileIds[] = list.split(",");
		String memberIds[] = memberId.split(",");
		String log = "";
		
		for (String bookUniid:bookFileIds)
		{
			BookFile bookFile  = BookFile.findLastVersionByBookUniId(em, bookUniid);
			
			if( null==bookFile ){
				throw new ServerException("id_err_301",bookUniid);
			}else{
				log += addBookToSpecificMember(em,bookFile,memberIds);	
			}
			
		}
		if( !"".contains(log)){
			throw new ServerException("id_err_999"," import : "+log) ;
		} 
	}
	
	private String addBookToSpecificMember(EntityManager em,BookFile bookFile,String[] members) throws ServerException {
		String memberLog = "";
			
		if( null==bookFile.getStatus() ){
			memberLog +="bookFile ID : "+bookFile.getId()+" | Status: is null <br>";
			return memberLog;
		}
		
		if( 9==bookFile.getStatus() ){
			for (String memberId:members)
			{
//					Member member = em.find(Member.class, Long.parseLong(memberId));
				List<Member> mbs = em.createNamedQuery("Member.findByBmemberId",Member.class)
				.setParameter("bmemberid", memberId)
				.getResultList();
				
				if( 0==mbs.size() ){
					memberLog += "member id: "+memberId+" not found <br>" ;
				}
				for(Member member: mbs ){
					if (member.hasItemByBookUniId(em, bookFile.getBookUniId()))
						continue;
					
					MemberBook mb= genMemberBook(member, bookFile) ;
					member.addMemberBook(mb);
					
					MemberDrmLog memberDrmLog = generateDrmLog(mb);
					member.addMemberDrmLog(memberDrmLog);

					em.persist(mb);
					em.persist(memberDrmLog);
					
					MemberBookDrmMapping memberBookDrmMapping = new MemberBookDrmMapping();
					memberBookDrmMapping.setMemberId(member.getId());
					memberBookDrmMapping.setMemberBook(mb);
					memberBookDrmMapping.setMemberDrmLog(memberDrmLog);
					memberBookDrmMapping.setStatus("update");
					memberBookDrmMapping.setUdt(Date.from(ZonedDateTime.parse(CommonUtil.zonedTime()).toInstant()));
					em.persist(memberBookDrmMapping);

				}
			}
		}else if( -1==bookFile.getStatus() ){
			memberLog +="bookFile ID : "+bookFile.getId()+" | Status:"+bookFile.getStatus()+"<br>";
		}
		
		return memberLog;
	}

	public static void main(String args[])
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("CMSObjects");
		EntityManager em = emf.createEntityManager();
		SampleDataGenerator ig = new SampleDataGenerator(em);
		try {
//			ig.genVendors();
			long startTime = System.currentTimeMillis();
			ig.genBooksFromSample(10);
			ig.generateMembers();
//			ig.generateCMSTokens();
			logger.info("\nTotalTime: " + (System.currentTimeMillis() -startTime) + " ms");
			logger.info("MemberBookCnt:" + ig.memberBookCnt );
			logger.info("MemberDrmCnt:" + ig.memberDrmLogCnt );
		} catch (IllegalArgumentException | SecurityException | ServerException  e) {
			logger.error(e);;
		}
		
	}
	
	public void genSampleBooksFromTxt(int loopSize) throws IOException, ServerException
	{
		importFile("resource/E05ebook_sample_1.txt", "1");
		importFile("resource/E06emag_sample_1.txt", "2");
//		logger.info("\nLoadTxt: " + (System.currentTimeMillis() -startTime) + " ms");
		em.getTransaction().begin();
		for (int i=0;i<loopSize;i++){
			generateItem(importBooks,"1");
			generateItem(importMags,"2");
		}
		em.getTransaction().commit();
	}
	
	private void generateItem(List<BookServletRecord> records,String callType) throws ServerException {
		for (BookServletRecord tmpRec:records)
		{
			createItem(tmpRec,callType);
		}

		
	}




	private void generateCMSTokens() {
		em.getTransaction().begin();
		for (Member member:em.createNamedQuery("Member.findAll",Member.class).getResultList())
		{
			// each Device has 1 token
			genMemberToken(member);
		}
		
		em.getTransaction().commit();
	}
	
	

	private void generateMembers() throws ServerException {
		loadBenchmarkBooks();
//		em.getTransaction().begin();
		for (int i=0; i< membercnt; i++)
		{
			generateBenchMember(true);
			/*
			if (i%101==0)
				em.flush();*/
		}
//		em.getTransaction().commit();
	}
	
	

	private void genVendors() {
		for (int i=0; i< 50;i++)
		{
			generateVendor();
		}
		
	}
	public Vendor generateVendor()
	{
		Vendor v = new Vendor();
		v.setBId("VendBid_" + rname.next());
		v.setName("vname_" + rname.next());
		em.persist(v);
		return v;
	}

	/**
	 * 
	 * @param fname
	 * @param callType 1 = Book, 2=Magazine
	 * @throws IOException
	 * @throws ServerException
	 */
	public void importFile(String fname, String callType) throws IOException, ServerException{
		String content = new String(Files.readAllBytes(Paths.get(fname)));
		
		BookServletRequest reqValue = new Gson().fromJson(content, BookServletRequest.class);
		// Boon Couldn't parse anyting here ?!
//		BookServletRequest ret=fromJson(content,BookServletRequest.class);
		List<BookServletRecord> records = reqValue.getRecords();
		for (BookServletRecord tmpRec:records)
		{
			if (callType.equals("1"))
				importBooks.add(tmpRec);
			else
				importMags.add(tmpRec);
		}
	}

	
	private void createItem(BookServletRecord tmpRec,String callType) throws ServerException {
		Item item = new Item();
		// publisher_name
		Publisher p = Publisher.findPublisherBybidMapped(em,tmpRec.getPublisher_id());
		if (p==null)
			p=generatePublisher(tmpRec.getPublisher_id());
		item.setPublisher(p);

		BaseBookInfo bi = genBaseBookInfo(tmpRec, callType,p.getName());
		item.setInfo(bi);

		List <BookFile> bookFiles = genBookFilesFromTxtSample(tmpRec,item);
		item.setBookFiles(bookFiles);

		/**
		 * 0=PDF, 1=EPUB_NOFIX(web、pad), 2=EPUB_FIX(web、pad、phone)
		 */
		if (bi.getType().equalsIgnoreCase("book"))
			item.setItemType("book");
		if (bi.getType().equalsIgnoreCase("magazine"))
			item.setItemType("magazine");
		item.setOperator("AutoAPI");
		Vendor vendor=Vendor.getRandomVendor(em);
		item.setVendor(vendor); 
		
		
		item.setId(UUID.randomUUID().toString());
		
		bi.setItem(item.getId());
		

		em.persist(item);
		
		
	}


	private BaseBookInfo genBaseBookInfo(BookServletRecord tmpRec, String callType, String publisherName) throws ServerException {
		BaseBookInfo bi =null;
		
		if (callType.equals("1")){
			BookInfo bookInfo = new BookInfo();
			bookInfo.setType("book");
			bookInfo.setAuthor(tmpRec.getAuthor());
			bookInfo.setEditor(tmpRec.getEditor());
			bookInfo.setEdition(tmpRec.getEdition());
			bookInfo.setForeword(tmpRec.getForeword());
			bookInfo.setIllustrator(tmpRec.getIllustrator());
			bookInfo.setIntro(tmpRec.getIntro());
			bookInfo.setIsbn(tmpRec.getIsbn());
			bookInfo.setO_author(tmpRec.getO_author());
			bookInfo.setTranslator(tmpRec.getTranslator());
			bi=bookInfo;
		}
		else if (callType.equals("2")){
			
			MagInfo mi = new MagInfo();
			mi.setType("Magazine");
			mi.setIssn(tmpRec.getIssn());
			mi.setMag_id(tmpRec.getMag_id());
			mi.setIssue(tmpRec.getIssue());
			mi.setIssue_year(tmpRec.getIssue_year());
			mi.setPublish_type(tmpRec.getPublish_type());
			mi.setCover_story(tmpRec.getCover_story());
			mi.setCover_man(tmpRec.getCover_man());
			bi =mi;
		}else
		{
			throw new ServerException("id_err_999","CallType Error:" + callType);
		}
		
		
//		String format = convBookFormat(Integer.parseInt(tmpRec.getBook_format())).toLowerCase();
//		bi.setBook_format(format);
		
		String randFormat = genRandFormat();
		bi.setBook_format(randFormat);
		
		bi.setC_title(tmpRec.getC_title());
		
		
		
		tmpRec.getCategory();//  not mapped attr
		bi.setEfile_cover_url(tmpRec.getEfile_cover_url()); // fetch and convert to thumbnail
		tmpRec.getEfile_fixed_pad_name(); //  not mapped attr
		tmpRec.getEfile_fixed_phone_name(); // not mapped Attr
		tmpRec.getEfile_nofixed_name(); //  not mapped Attr
		bi.setLanguage(tmpRec.getLanguage());
		bi.setO_title(tmpRec.getO_title());
		tmpRec.getPage_data(); //  not mapped Attr
		bi.setPage_direction(tmpRec.getPage_direction());
		bi.setPublish_date(tmpRec.getPublish_date()); // timestamp
		bi.setPublisher_id(tmpRec.getPublisher_id());
		bi.setRank(tmpRec.getRank());
		tmpRec.getReturn_file_num(); //  not mapped Attr;
		tmpRec.getS_item(); //  not mapped Attr;
		bi.setShare_flag(tmpRec.getShare_flag());
		tmpRec.getStatus(); //  not mapped Attr;
		bi.setToc(tmpRec.getToc());
		tmpRec.getVersion_type(); //  not mapped;

//		String bookType =bi.getType().toLowerCase();
		bi.setPublisher_name(publisherName);
		
		
		return bi;
	}


	private static String genRandFormat() {
		
		return bookFormats.get(rnd.nextInt(3));
	}

	public Publisher generatePublisher(String publisher_id)
	{
		Publisher p =new Publisher();
		p.setBId(publisher_id);
		p.setName("pname_" + rname.next());
		em.persist(p);
		publishers.add(p);
		return p;
	}


	private List<BookFile> genBookFilesFromTxtSample(BookServletRecord tmpRec, Item item) throws ServerException {
		List <BookFile> ret = new ArrayList<>();

		// real 
		BookFile bf = genBookFileFromTxtSample(tmpRec,item);
		ret.add(bf);

		// trial
		BookFile bfTrial = genBookFileFromTxtSample(tmpRec,item);
		bfTrial.setIsTrial(true);
		ret.add(bfTrial);
		
		return ret;
	}


	private BookFile genBookFileFromTxtSample(BookServletRecord tmpRec, Item item) throws ServerException {
		BookFile bf=new BookFile();
//		bf.setFileLocation(null); need to fetch from server tmpRec.getEfile_url(), store to S3, wait for convert, after convert,

		BaseBookInfo bi = (BaseBookInfo) item.getInfo();
		String format=bi.getBook_format();

		if (format.equals(BaseBookInfo.BookFormatPDF))
		{
			bf.setFileLocation(bookFilePathPdf.get(rnd.nextInt(bookFilePathPdf.size())));
		}else if (format.equals(BaseBookInfo.BookFormatReflowable))
		{
			bf.setFileLocation(bookFilePathReflow.get(rnd.nextInt(bookFilePathReflow.size())));
		}else if (format.equals(BaseBookInfo.BookFormatFixedLayout))
		{
			bf.setFileLocation(bookFilePathFixed.get(rnd.nextInt(bookFilePathFixed.size())));
		}else
		{
			throw new ServerException("id_err_305","BookFormat not supported " + format);
		}
		bf.setItem(item);
		bf.setFormat(format);
		
		bf.setIsTrial(false);
		bf.setVersion("V001.0001");
		
//		allBookFile.add(bf);
		
		return bf;
	}
	
	// used to load from database
//	public void loadBookFiles()
//	{
//		List<BookFile> bf = em.createNamedQuery("BookFile.findAll",BookFile.class).getResultList();
//		allBookFile.addAll(bf);
//	}

	public Member generateBenchMember(boolean full) throws ServerException
	{
		Member m = new Member();
		m.setBmemberId("bench_sample_"+rname.next());
		m.setMemberType("NormalReader");
		m.setName(rname.next());
		m.setNick(rname.next());
		em.persist(m);
		if (full)
			generateMemberData(m);
		return m;
	}
	
	public Member generateBenchMember() throws ServerException
	{
		return generateBenchMember(true);
	}
	
	public void generateMemberData(String bmemberid) throws ServerException
	{
		
//		if (allBookFile.isEmpty())
//			loadBookFiles();
		loadBenchmarkBooks();
		Member m =Member.getMemberByBMemberId(em, bmemberid,Member.MEMBERTYPE_NORMALREADER);
		generateMemberData(m);
	}
	
	public void generateMemberData(Member m) throws ServerException
	{
		genMemberReadList(m);
		genMemberDevices(m);
		genMemberBooks(em,m,memberBookCnt);
	}
	
	public void genMemberReadList(Member m) {
		m.genDefaultReadLists(em);
	}

	public void genMemberBooks(EntityManager em,Member m, int size) throws ServerException {
		
		if (null==benchBookFiles || benchBookFiles.size()<1)
			loadBenchmarkBooks();

		// logger.info("[SampleDataGenerator] genMemberBooks member:" + m.getBmemberId() + ", memberBookCnt:" + size);
		
		/*
		if (null==sampleBookFiles || sampleBookFiles.size()<1)
			loadSampleBookFiles();*/

		// each Member has power low distribution of max 200 books;
		//int bookCnt = size;//pwRnd.zipf(20)+1;
		// if size exists
		/*
		if (size>0)
			bookCnt=size;*/
		
		int curBookCnt=0;
		while (curBookCnt<size){
			BookFile bf = benchBookFiles.get(rnd.nextInt(benchBookFiles.size()));
			if (null==m.getMemberBookByBUID(em,bf.getBookUniId()))
			{	
				MemberBook mb = genMemberBook(m,bf);
				m.addBenchMemberBook(mb);

				// logger.info("[SampleDataGenerator] genMemberBooks member:" + m.getBmemberId() + ", MemberBook:" + mb.getItem().getId());
				//memberBookCnt ++;
				//curBookCnt++;
			}
			curBookCnt++;
		}
	}

	private MemberBook genMemberBook(Member m, BookFile b) throws ServerException {
		
		MemberBook mb = new MemberBook();
		
		mb.setBookFile(b);
		mb.setItem(b.getItem());
		mb.setBookUniId(b.getBookUniId());
		mb.setCreateTime(new Date());
		
		//mb.setIsHidden(false);
        if(memberBookNoteCnt != 0){
            genMemberBookNotes(mb);
            //mb.setMemberBookNotes(mbn);
        }		
		mb.setBookType(b.getItem().getInfo().getType());
		mb.setDefaultReadList(mb);

		if (b.getIsTrial())
		{
			mb.setEncryptType(MemberBook.EncryptTypeNone);
		}else
		{
			byte[] encryptKeykey={0x00};
			try {
				encryptKeykey = CommonUtil.generateAES256Key();
			} catch (NoSuchAlgorithmException e) {
				throw new ServerException("id_err_999","Internal Error:" + e.getMessage());
			}
			mb.setEncryptKey(encryptKeykey);
			mb.setEncryptType(Enc01.EncryptType);
		}

//		em.persist(mb);
		return mb;
	}


	private MemberDrmLog generateDrmLog(MemberBook mb) {
		MemberDrmLog mdl = new MemberDrmLog();

		mdl.setItem(mb.getItem());
		mdl.setLastUpdated(CommonUtil.zonedTime());
		mdl.setMember(mb.getMember());
		mdl.setStatus(1); // authorize
		mdl.setTransactionId(UUID.randomUUID().toString());
		mdl.setSubmitId("");
		boolean isThirdParty = mb.getMember().hasItemByThirdPartyId(mb.getItem());
		
		if( mb.getBookFile().getIsTrial() ){
			mdl.setType(4);	
		}else if( isThirdParty ){
			mdl.setType(999);
		}else{
			mdl.setType(1);
		}

		return mdl;
	}

	private void genMemberBookNotes(MemberBook mb) {
//		List <MemberBookNote> bookNotes= new ArrayList<>();
		// each book has  power low dist of max 20 annotations
		if(null==benchMemberBookNote || benchMemberBookNote.size()<1)
			loadBenchmarkBookNotes();
        
		//int anCnt=pwRnd.zipf(20);
        int anCnt = memberBookNoteCnt;
        
		for (int i=0; i< anCnt; i++)
		{
			
			MemberBookNote mbn = new MemberBookNote();
			mbn.setNoteContent(new NoteContent());
			UUID uuid = UUID.randomUUID();
			mbn.setUuid(uuid);
			mbn.setMemberBook(mb);
			mbn.setType("feedback");
			mbn.setNote(rname.next()+rname.next());
			mb.addMemberBookNote(mbn);
//			em.persist(mbn);
//			benchMemberBookNote.get(i).setMemberBook(mb);
//			UUID uuid = UUID.randomUUID();
//			benchMemberBookNote.get(i).setUuid(uuid);
//			mb.addMemberBookNote(benchMemberBookNote.get(i));
//			mbn.setIsPublic(false);
//			bookNotes.add(mbn);
		}
//		return bookNotes;
	}
	
	private void loadBenchmarkBookNotes(){
		int anCnt = memberBookNoteCnt;
		for (int i=0; i< anCnt; i++){
			MemberBookNote mbn = new MemberBookNote();
			mbn.setNoteContent(new NoteContent());
			//UUID uuid = UUID.randomUUID();
			//mbn.setUuid(uuid);
			mbn.setType("feedback");
			mbn.setNote(rname.next()+rname.next());
			mbn.setIsPublic(false);
			benchMemberBookNote.addElement(mbn);
		}
	}

	public void genMemberDrmLog(Member m) {
		
		
		for (MemberBook mb: m.getMemberBooks())
		{
			if (mb.getBookFile().getIsTrial())
				continue;
			// each book has most 6 drmInfo, least 1
			int drmCnt = 1;//pwRnd.zipf(5)+1;
			for (int i=0; i< drmCnt; i++)
			{
				
				MemberDrmLog memberDrmLog=generateDrmLog(mb);
				m.addMemberDrmLog(memberDrmLog);
				

				//memberDrmLogCnt++;
			}
		}
		return;

	}

	private void genMemberToken(Member m) {
		List <CmsToken>tokens = new ArrayList<>();
		// for each device, generate a cmstoken
		for (MemberDevice md:m.getMemberDevices()){
			CmsToken cm = new CmsToken();
			cm.setClientIp(genRandomIP());
			cm.setDeviceId(md.getId());
			cm.setMemberId(m.getId());
			em.persist(cm);
			tokens.add(cm);
		}
	}

	private void genMemberDevices(Member m) throws ServerException {
//		List <MemberDevice> devices = new ArrayList<>();
		// each has 2 devices (prevent new device from registering) 
		for (int i=0; i< 2;i++)
		{
			MemberDevice md = new MemberDevice();
			md.setDeviceId(rname.next());
			md.setDeviceModel(rname.next());
			md.setDeviceVendor(rname.next());
			md.setLanguage("zh_TW");
			
			md.setLastOnlineIp(genRandomIP());
			md.setLastOnlineTime(LocalDateTime.now());
			md.setMember(m);
			md.setOsType("ios");
			md.setOsVersion("1.0");
			md.setScreenDpi("400.5");
			md.setScreenResolution("1920x1080");
			String deviceName = md.getDeviceVendor()+ "_" + md.getDeviceModel();
            md.setDeviceName(deviceName);
			m.addMemberDevice(md);
//			em.persist(md);
//			devices.add(md);
		}
//		m.setMemberDevices(devices);
	}

	private static String genRandomIP() {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i< 4; i++)
		{
			sb.append(rnd.nextInt(253)+1);
			sb.append(".");
		}
		//remove tail dot
		sb.setLength(sb.length()-1);
		return sb.toString();
	}

	public void generateData(Method m, int cnt) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		long startTime = System.currentTimeMillis();
		em.getTransaction().begin();
		for (int i=1;i< cnt; i++)
		{
			m.invoke(this, (Object[])null);
			if (i%100==0)
			{
				em.flush();
			}
		}
		em.getTransaction().commit();
		long duringMs = System.currentTimeMillis()-startTime;
		
		logger.info("\nMethod:" + m.getName());
		logger.info("Average Time Per Unit:" + (duringMs/cnt) + "ms");
		logger.info("Time Used :" +duringMs+ " ms");

	}

	public String upgradeItemVersion(String bookUniId, boolean majorVersionUpgrade) throws ServerException
	{
		String message="Result:<br/>";
		em.getTransaction().begin();
		
		
		BookFile origBF = BookFile.findLastVersionByBookUniId(em, bookUniId);
		BookFile refBF =null;
		if (origBF.getIsTrial())
			refBF = BookFile.getRandomTrialBookFile(em,origBF.getFormat());
		else
			refBF = BookFile.getRandomRealBookFile(em,origBF.getFormat());
		BookFile newBF = CommonUtil.jsonClone(refBF, BookFile.class);
		
		newBF.setItem(origBF.getItem());
		newBF.setId(null);
		newBF.setLastUpdated(new Date());
		newBF.setMemberBooks(new ArrayList<>()); // not referenced by any member book yet.
		newBF.setRecommendBooks(new ArrayList<>());
		String version=origBF.getItem().getNewestReadyVersion(origBF.getIsTrial());
		BookVersion bv = new BookVersion(version);
		String newVersion = bv.upgradeVersion(majorVersionUpgrade).toString();
		newBF.setVersion(newVersion);
		
		em.persist(newBF);
		em.getTransaction().commit();
		
		message +="OrigVersion:"+version + "<br/>";
		message +="NewVersion:"+newVersion + "<br/>";
		return message;
	}


	//pdf/reflowable/fixedlayout
	public static String convBookFormat(int bookformat) throws ServerException {
		/**
		 * 0=PDF, 1=EPUB_NOFIX(web、pad), 2=EPUB_FIX(web、pad、phone)
		 */
		switch (bookformat)
		{
		case 0:
			return BaseBookInfo.BookFormatPDF;
		case 1:
			return BaseBookInfo.BookFormatReflowable;
		case 2:
			return BaseBookInfo.BookFormatFixedLayout;
		default :
			throw new ServerException("id_err_305"," UnSupported bookformat:" + bookformat);
		}
		
	}





	public class BookServletRequest
	{
		private List<BookServletRecord> records;
		private String updated_time;
		public List<BookServletRecord> getRecords() {
			return records;
		}

		public void setRecords(List<BookServletRecord> records) {
			this.records = records;
		}

		public String getUpdated_time() {
			return updated_time;
		}

		public void setUpdated_time(String updated_time) {
			this.updated_time = updated_time;
		}
		
	}
	
	public static class BookServletRecord
	{
		private String item;
		private String c_title;
		private String o_title;
		private String isbn;
		private String publisher_id;
		private String category;
		private String rank;
		private String publish_date;
		private String author;
		private String o_author;
		private String translator;
		private String editor;
		private String illustrator;
		private String edition;
		private String foreword;
		private String toc;
		private String intro;
		private String efile_nofixed_name;
		private String efile_fixed_pad_name;
		private String efile_fixed_phone_name;
		private String efile_url;
		private String efile_cover_url;
		private String share_flag;
		private String status;
		private int return_file_num;
		private String page_data;
		private String s_item;
		private String book_format;
		private int page_direction;
		private String language;
		private String version_type;
		private String buffet_flag;
		private String buffet_type;
		private String session_token;
		
		// for magazine
		private String issn;
		private String mag_id;
		private String issue;
		private String issue_year;
		private String publish_type;
		private String cover_man;
		private String cover_story;
		
		public String getItem() {
			return item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		public String getC_title() {
			return c_title;
		}
		public void setC_title(String c_title) {
			this.c_title = c_title;
		}
		public String getO_title() {
			return o_title;
		}
		public void setO_title(String o_title) {
			this.o_title = o_title;
		}
		public String getIsbn() {
			return isbn;
		}
		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}
		public String getPublisher_id() {
			return publisher_id;
		}
		public void setPublisher_id(String publisher_id) {
			this.publisher_id = publisher_id;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		public String getRank() {
			return rank;
		}
		public void setRank(String rank) {
			this.rank = rank;
		}
		public String getPublish_date() {
			return publish_date;
		}
		public void setPublish_date(String publish_date) {
			this.publish_date = publish_date;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getO_author() {
			return o_author;
		}
		public void setO_author(String o_author) {
			this.o_author = o_author;
		}
		public String getTranslator() {
			return translator;
		}
		public void setTranslator(String translator) {
			this.translator = translator;
		}
		public String getEditor() {
			return editor;
		}
		public void setEditor(String editor) {
			this.editor = editor;
		}
		public String getIllustrator() {
			return illustrator;
		}
		public void setIllustrator(String illustrator) {
			this.illustrator = illustrator;
		}
		public String getEdition() {
			return edition;
		}
		public void setEdition(String edition) {
			this.edition = edition;
		}
		public String getForeword() {
			return foreword;
		}
		public void setForeword(String foreword) {
			this.foreword = foreword;
		}
		public String getToc() {
			return toc;
		}
		public void setToc(String toc) {
			this.toc = toc;
		}
		public String getIntro() {
			return intro;
		}
		public void setIntro(String intro) {
			this.intro = intro;
		}
		public String getEfile_nofixed_name() {
			return efile_nofixed_name;
		}
		public void setEfile_nofixed_name(String efile_nofixed_name) {
			this.efile_nofixed_name = efile_nofixed_name;
		}
		public String getEfile_fixed_pad_name() {
			return efile_fixed_pad_name;
		}
		public void setEfile_fixed_pad_name(String efile_fixed_pad_name) {
			this.efile_fixed_pad_name = efile_fixed_pad_name;
		}
		public String getEfile_fixed_phone_name() {
			return efile_fixed_phone_name;
		}
		public void setEfile_fixed_phone_name(String efile_fixed_phone_name) {
			this.efile_fixed_phone_name = efile_fixed_phone_name;
		}
		public String getEfile_url() {
			return efile_url;
		}
		public void setEfile_url(String efile_url) {
			this.efile_url = efile_url;
		}
		public String getEfile_cover_url() {
			return efile_cover_url;
		}
		public void setEfile_cover_url(String efile_cover_url) {
			this.efile_cover_url = efile_cover_url;
		}
		public String getShare_flag() {
			return share_flag;
		}
		public void setShare_flag(String share_flag) {
			this.share_flag = share_flag;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public int getReturn_file_num() {
			return return_file_num;
		}
		public void setReturn_file_num(int return_file_num) {
			this.return_file_num = return_file_num;
		}
		public String getPage_data() {
			return page_data;
		}
		public void setPage_data(String page_data) {
			this.page_data = page_data;
		}
		public String getS_item() {
			return s_item;
		}
		public void setS_item(String s_item) {
			this.s_item = s_item;
		}
		public String getBook_format() {
			return book_format;
		}
		public void setBook_format(String book_format) {
			this.book_format = book_format;
		}
		public int getPage_direction() {
			return page_direction;
		}
		public void setPage_direction(int page_direction) {
			this.page_direction = page_direction;
		}
		public String getLanguage() {
			return language;
		}
		public void setLanguage(String language) {
			this.language = language;
		}
		public String getVersion_type() {
			return version_type;
		}
		public void setVersion_type(String version_type) {
			this.version_type = version_type;
		}
		public String getBuffet_flag() {
			return buffet_flag;
		}
		public void setBuffet_flag(String buffet_flag) {
			this.buffet_flag = buffet_flag;
		}
		public String getBuffet_type() {
			return buffet_type;
		}
		public void setBuffet_type(String buffet_type) {
			this.buffet_type = buffet_type;
		}
		public String getSession_token() {
			return session_token;
		}
		public void setSession_token(String session_token) {
			this.session_token = session_token;
		}
		
		// Magazine related
		
		public String getIssn() {
			return issn;
		}
		public void setIssn(String issn) {
			this.issn = issn;
		}
		public String getMag_id() {
			return mag_id;
		}
		public void setMag_id(String mag_id) {
			this.mag_id = mag_id;
		}
		public String getIssue() {
			return issue;
		}
		public void setIssue(String issue) {
			this.issue = issue;
		}
		public String getIssue_year() {
			return issue_year;
		}
		public void setIssue_year(String issue_year) {
			this.issue_year = issue_year;
		}
		public String getPublish_type() {
			return publish_type;
		}
		public void setPublish_type(String publish_type) {
			this.publish_type = publish_type;
		}
		public String getCover_man() {
			return cover_man;
		}
		public void setCover_man(String cover_man) {
			this.cover_man = cover_man;
		}
		public String getCover_story() {
			return cover_story;
		}
		public void setCover_story(String cover_story) {
			this.cover_story = cover_story;
		}
		
	}

	@SuppressWarnings("unchecked")
	public void loadRealBooks(int bookCount) {
		realBookFiles = em.
				createNativeQuery("select * from book_file where status=9 and src_fname is not null and (not src_fname like '0000%')  limit  " + bookCount,BookFile.class).getResultList();
	}

	
	public void replaceTopList()
	{
		int [] realAry = { 193177
			,193178
			,193188
			,193180
			,193189
			,193183
			,193181
			,193182
			,193190
			,193179
		};
		int [] fakeAry = {91318
				,91337
				,91322
				,91329
				,91317
				,91314
				,91333
				,91334
				,91331
				,91319};
		replaceList(fakeAry,realAry);
	}
	
	private void replaceList(int []fakeList, int [] realList)
	{
		if (fakeList.length!=realList.length)
			throw new RuntimeException("Unmatch array size");
		try {
			em.getTransaction().begin();
			for (int i=0;i<fakeList.length;i++)
			{
				BookFile fake = em.find(BookFile.class, fakeList[i]);
				BookFile real = em.find(BookFile.class, realList[i]);
				
				replaceBookFile(fake, real);
				replaceItem(fake.getItem(),real.getItem());
			}
			em.getTransaction().commit();
		} finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		}
	}

	@SuppressWarnings("unchecked")
	public void replaceFakeBooks() {
		List<BookFile> fakeFiles = em.createNativeQuery
				("select * from book_file where status is null or src_fname like '0000%' "
						,BookFile.class).getResultList();
		
		try {
			int i=0; 
			em.getTransaction().begin();
			for (BookFile fakefile:fakeFiles)
			{
				BookFile realBook = realBookFiles.get(i);
				replaceBookFile(fakefile,realBook);
				replaceItem(fakefile.getItem(),realBook.getItem());
				i++;
			}
			
			em.getTransaction().commit();
		} catch (Exception ex)
		{
			logger.error(ex);;
		}finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		}
	}

	// in transaction
	private void replaceBookFile(BookFile fake, BookFile real) {
		
		fake.setChecksum(real.getChecksum());
		fake.setFileLocation(real.getFileLocation());
		fake.setFormat(real.getFormat());
		fake.setIsTrial(real.getIsTrial());
		fake.setLastUpdated(real.getLastUpdated());
		fake.setSize(real.getSize());
		fake.setSrc_fname(real.getSrc_fname());
		fake.setStatus(real.getStatus());
		fake.setVersion(real.getVersion());
	}

	// in transaction
	private void replaceItem(Item fakeItem, Item realItem) {
		
		// item
		fakeItem.setAuthor(StringUtils.trimToEmpty(realItem.getAuthor()));
		fakeItem.setcTitle(realItem.getcTitle());
		fakeItem.setInfo(realItem.getInfo());
		fakeItem.setItemType(realItem.getItemType());
		fakeItem.setLastUpdated(realItem.getLastUpdated());
	}

	// coverURL is empty now
	@SuppressWarnings("unchecked")
	public void fixCoverURL(String urlHead) {
		
		// 1. get bookfile/item.
		List<Item> items = em.createQuery("select i from Item i",Item.class).getResultList();
		// 2. get ItemInfo => update url
		try {
			em.getTransaction().begin();
			for (Item i:items)
			{
				BaseBookInfo info=(BaseBookInfo) i.getInfo();
				if (i.getId().length()>15)
					continue;
				
				info.setEfile_cover_url(urlHead + i.getId() + ".jpg");
				i.setInfo(info);
				logger.info("Processing: " + info.getClass().getName() +" : "+ info.getEfile_cover_url());
			}
			em.getTransaction().commit();
			logger.info("after Commit");
		} finally
		{
			if (em.getTransaction().isActive()){
				em.getTransaction().rollback();
				logger.info("rolling back");
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void loadBenchmarkBooks() {
	 
		benchBookFiles = (Vector<BookFile>) 
				em.createNativeQuery("select * from book_file where item_id like 'bench_sample%'",BookFile.class).getResultList();
	}
	
	public void updateBookInfo(String itemId, String info)
	{
		BookInfo bi = JsonFactory.fromJson(info,BookInfo.class);
		Item i = em.find(Item.class, itemId);
		em.getTransaction().begin();
		i.setInfo(bi);
		em.getTransaction().commit();
	}

	public BookInfo getBookInfo(String itemId)
	{
		Item i = em.find(Item.class, itemId);
		return (BookInfo) i.getInfo();
	}

	// 
	public Item genSampleBookWithChilds(EntityManager em) {
		Item targetItem = genSerialSampleItem();
		return targetItem;
	}

	
	private BookFile getRandomSampleBook() {
		BookFile sampleBook=null;
		int randIdx=rnd.nextInt(sampleBookFiles.size());
		sampleBook = sampleBookFiles.get(randIdx);

		return sampleBook;
	}
	
	   private BookFile getRandomTrialSampleBook() {
	        BookFile sampleBook=null;
            int randIdx=rnd.nextInt(sampleTrialBookFiles.size());
            sampleBook = sampleTrialBookFiles.get(randIdx);

	        return sampleBook;
	    }
	
	private BookFile getRandomSerialSampleBook() {
        BookFile sampleBook=null;
        int randIdx=rnd.nextInt(sampleSerialBookFiles.size());
        sampleBook = sampleSerialBookFiles.get(randIdx);

        return sampleBook;
    }

	public Member generateSuperReader() {
		// superReader 
		return null;
	}

	public static int getMemberBookNoteCnt() {
		return memberBookNoteCnt;
	}

	public static void setMemberBookNoteCnt(int memberBookNoteCnt) {
		SampleDataGenerator.memberBookNoteCnt = memberBookNoteCnt;
	}

	public static int getMemberBookCnt() {
		return memberBookCnt;
	}

	public static void setMemberBookCnt(int memberBookCnt) {
		SampleDataGenerator.memberBookCnt = memberBookCnt;
	}

	public static int getMemberDrmLogCnt() {
		return memberDrmLogCnt;
	}

	public static void setMemberDrmLogCnt(int memberDrmLogCnt) {
		SampleDataGenerator.memberDrmLogCnt = memberDrmLogCnt;
	}
}