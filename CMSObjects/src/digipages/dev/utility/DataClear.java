package digipages.dev.utility;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import digipages.common.DPLogger;

public class DataClear {
	
	private static DPLogger logger =DPLogger.getLogger(DataClear.class.getName());
	
	private EntityManagerFactory emf;
	private EntityManager em;
	public DataClear()
	{
		emf = Persistence.createEntityManagerFactory("CMSObjects");
		em = emf.createEntityManager();
	}
	
	public static void main(String args[])
	{
		DataClear dc = new DataClear();
		try {
			long startTime = System.currentTimeMillis();
			dc.clearAll();
			logger.info("\nTotalTime: " + (System.currentTimeMillis() -startTime) + " ms");
		} catch (IllegalArgumentException | SecurityException e) {
			logger.error(e);;
		}
		

	}

	private void clearAll() {
		em.getTransaction().begin();
		em.createNativeQuery("truncate table member cascade").executeUpdate();
		em.createNativeQuery("truncate table vendor cascade").executeUpdate();
		em.createNativeQuery("truncate table publisher cascade").executeUpdate();
		em.createNativeQuery("truncate table item cascade").executeUpdate();
		em.createNativeQuery("truncate table cms_token cascade").executeUpdate();
		em.getTransaction().commit();
	}
}
