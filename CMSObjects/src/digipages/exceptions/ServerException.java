package digipages.exceptions;

import java.util.Locale;

import java.util.ResourceBundle;

import javax.servlet.ServletException;

import org.boon.json.JsonFactory;

import digipages.dev.utility.UTF8Control;

import java.nio.charset.StandardCharsets;

public class ServerException extends ServletException {
	/**
	 * just in case of collection with other object.
	 */
	
	private static final long serialVersionUID = 8832702160153210297L;
	private String error_code;
	private String error_message;
	static final ClassLoader cl = Thread.currentThread().getContextClassLoader();
	static Locale locale = new Locale ("zh", "TW");
	static ResourceBundle messageList = ResourceBundle.getBundle("BooksAPIProj",new UTF8Control());
	

	public ServerException(String error_code, String errorParameter) {

		this.setError_code(error_code);
		this.setError_message(messageList.getString(error_code) + ":" + errorParameter);
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
	
	public String toJsonStr()
	{
		String jsonStr="";
		
		jsonStr = JsonFactory.toJson(this);
		
		int loc = jsonStr.indexOf(",\"stackTrace\"");
		if (loc>0)
			jsonStr = jsonStr.substring(0, loc) + "}";
		
		return jsonStr;
	}
	
	
}
