package digipages.exceptions;

public class JobRetryException extends Exception {

	public JobRetryException() {
		
	}

	public JobRetryException(String message) {
		super(message);
	}

	public JobRetryException(Throwable cause) {
		super(cause);
	}

	public JobRetryException(String message, Throwable cause) {
		super(message, cause);
	}

	public JobRetryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
