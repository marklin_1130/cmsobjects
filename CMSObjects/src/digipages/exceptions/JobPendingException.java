package digipages.exceptions;

public class JobPendingException extends Exception {

	public JobPendingException() {
		
	}

	public JobPendingException(String message) {
		super(message);
	}

	public JobPendingException(Throwable cause) {
		super(cause);
	}

	public JobPendingException(String message, Throwable cause) {
		super(message, cause);
	}

	public JobPendingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
