package digipages.dto;

public class ChapterInfo {
	
    public String chapter_length;
    public String audio_codec;
    public String audio_sample_rate;
    public String channels;
    public String created_at;
    public String updated_at;
    public String finished_at;
    public String duration_in_ms;
    public String error_class;
    public String error_message;
    public String file_size_bytes;
    public String fragment_duration_in_ms;
    public String format;
    public String frame_rate;
    public String height;
    public String id;
    public String label;
    public String md5_checksum;
    public String privacy;
    public String rfc_6381_audio_codec;
    public String rfc_6381_video_codec;
    public String state;
    public String video_bitrate_in_kbps;
    public String video_codec;
    public String width;
    public String total_bitrate_in_kbps;
    public String dash_url;
    public String hls_url;
    public String url;
    public String chapter_original_length;
    public boolean thumbnail;

    public String getChapter_length() {
        return chapter_length;
    }

    public String getAudio_codec() {
        return audio_codec;
    }

    public String getAudio_sample_rate() {
        return audio_sample_rate;
    }

    public String getChannels() {
        return channels;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getFinished_at() {
        return finished_at;
    }

    public String getDuration_in_ms() {
        return duration_in_ms;
    }

    public String getError_class() {
        return error_class;
    }

    public String getError_message() {
        return error_message;
    }

    public String getFile_size_bytes() {
        return file_size_bytes;
    }

    public String getFragment_duration_in_ms() {
        return fragment_duration_in_ms;
    }

    public String getFormat() {
        return format;
    }

    public String getFrame_rate() {
        return frame_rate;
    }

    public String getHeight() {
        return height;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getMd5_checksum() {
        return md5_checksum;
    }

    public String getPrivacy() {
        return privacy;
    }

    public String getRfc_6381_audio_codec() {
        return rfc_6381_audio_codec;
    }

    public String getRfc_6381_video_codec() {
        return rfc_6381_video_codec;
    }

    public String getState() {
        return state;
    }

    public String getVideo_bitrate_in_kbps() {
        return video_bitrate_in_kbps;
    }

    public String getVideo_codec() {
        return video_codec;
    }

    public String getWidth() {
        return width;
    }

    public String getTotal_bitrate_in_kbps() {
        return total_bitrate_in_kbps;
    }

    public String getUrl() {
        return url;
    }

    public void setChapter_length(String chapter_length) {
        this.chapter_length = chapter_length;
    }

    public void setAudio_codec(String audio_codec) {
        this.audio_codec = audio_codec;
    }

    public void setAudio_sample_rate(String audio_sample_rate) {
        this.audio_sample_rate = audio_sample_rate;
    }

    public void setChannels(String channels) {
        this.channels = channels;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setFinished_at(String finished_at) {
        this.finished_at = finished_at;
    }

    public void setDuration_in_ms(String duration_in_ms) {
        this.duration_in_ms = duration_in_ms;
    }

    public void setError_class(String error_class) {
        this.error_class = error_class;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public void setFile_size_bytes(String file_size_bytes) {
        this.file_size_bytes = file_size_bytes;
    }

    public void setFragment_duration_in_ms(String fragment_duration_in_ms) {
        this.fragment_duration_in_ms = fragment_duration_in_ms;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setFrame_rate(String frame_rate) {
        this.frame_rate = frame_rate;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setMd5_checksum(String md5_checksum) {
        this.md5_checksum = md5_checksum;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public void setRfc_6381_audio_codec(String rfc_6381_audio_codec) {
        this.rfc_6381_audio_codec = rfc_6381_audio_codec;
    }

    public void setRfc_6381_video_codec(String rfc_6381_video_codec) {
        this.rfc_6381_video_codec = rfc_6381_video_codec;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setVideo_bitrate_in_kbps(String video_bitrate_in_kbps) {
        this.video_bitrate_in_kbps = video_bitrate_in_kbps;
    }

    public void setVideo_codec(String video_codec) {
        this.video_codec = video_codec;
    }

    public String getDash_url() {
		return dash_url;
	}

	public String getHls_url() {
		return hls_url;
	}

	public String getChapter_original_length() {
		return chapter_original_length;
	}

	public void setDash_url(String dash_url) {
		this.dash_url = dash_url;
	}

	public void setHls_url(String hls_url) {
		this.hls_url = hls_url;
	}

	public void setChapter_original_length(String chapter_original_length) {
		this.chapter_original_length = chapter_original_length;
	}

	public void setWidth(String width) {
        this.width = width;
    }

    public void setTotal_bitrate_in_kbps(String total_bitrate_in_kbps) {
        this.total_bitrate_in_kbps = total_bitrate_in_kbps;
    }

    public void setUrl(String url) {
        this.url = url;
    }

	public boolean getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(boolean thumbnail) {
		this.thumbnail = thumbnail;
	}
}
