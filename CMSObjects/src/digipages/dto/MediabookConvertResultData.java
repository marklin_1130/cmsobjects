package digipages.dto;

public class MediabookConvertResultData {

    private String item_id;
    private String chapter_no;
    private String processing_id;
    private boolean result;
    private String result_msg;
    private String result_code;
    private ChapterInfo chapter_info;
    private PreviewInfo preview_info;
    private String failures;
    private boolean thumbnail;

    public String getItem_id() {
        return item_id;
    }

    public String getChapter_no() {
        return chapter_no;
    }

    public String getProcessing_id() {
        return processing_id;
    }

    public boolean isResult() {
        return result;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public ChapterInfo getChapter_info() {
        return chapter_info;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public void setChapter_no(String chapter_no) {
        this.chapter_no = chapter_no;
    }

    public void setProcessing_id(String processing_id) {
        this.processing_id = processing_id;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public void setChapter_info(ChapterInfo chapter_info) {
        this.chapter_info = chapter_info;
    }

    public PreviewInfo getPreview_info() {
        return preview_info;
    }

    public void setPreview_info(PreviewInfo preview_info) {
        this.preview_info = preview_info;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

	public String getFailures() {
		return failures;
	}

	public void setFailures(String failures) {
		this.failures = failures;
	}

	public boolean getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(boolean thumbnail) {
		this.thumbnail = thumbnail;
	}


}
