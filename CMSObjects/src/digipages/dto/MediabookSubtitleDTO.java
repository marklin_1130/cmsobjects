package digipages.dto;

import java.util.Date;

import com.google.gson.annotations.Expose;

public class MediabookSubtitleDTO {

    @Expose
	private long id;
	
    @Expose
	private long chapterId;
	
    @Expose
	private long chapterNo;
	
    @Expose
	private String version;
	
    @Expose
	private String subtitleId;
	
    @Expose
	private String subtitleFile;
	
    @Expose
	private String srcFname;
	
    @Expose
	private String fileLocation;
	
    @Expose
	private int size;
	
    @Expose
	private Date create_time;
	
    @Expose
	protected Date lastUpdated;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getChapterId() {
		return chapterId;
	}

	public void setChapterId(long chapterId) {
		this.chapterId = chapterId;
	}

	public long getChapterNo() {
		return chapterNo;
	}

	public void setChapterNo(long chapterNo) {
		this.chapterNo = chapterNo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSubtitleId() {
		return subtitleId;
	}

	public void setSubtitleId(String subtitleId) {
		this.subtitleId = subtitleId;
	}

	public String getSubtitleFile() {
		return subtitleFile;
	}

	public void setSubtitleFile(String subtitleFile) {
		this.subtitleFile = subtitleFile;
	}

	public String getSrcFname() {
		return srcFname;
	}

	public void setSrcFname(String srcFname) {
		this.srcFname = srcFname;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	
	
}
