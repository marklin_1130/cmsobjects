package digipages.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Mutable;

import com.google.gson.annotations.Expose;

import model.AttachInfo;

public class MediabookAttachDTO {

    @Expose
    private long id;

    @Expose
    private String itemId;

    @Expose
    private String cat;

    @Expose
    private String version;

    @Expose
    private Integer attachfileNo;

    @Expose
    private String attachfileType;

    @Expose
    private String attachfileName;

    @Expose
    private String attachfileTitle;

    @Expose
    private String fileLocation;

    @Expose
    private int size;

    @Expose
    private Date create_time;

    @Expose
    protected Date lastUpdated;

    public long getId() {
        return id;
    }

    public String getItemId() {
        return itemId;
    }

    public String getCat() {
        return cat;
    }

    public String getVersion() {
        return version;
    }

    public Integer getAttachfileNo() {
        return attachfileNo;
    }

    public String getAttachfileType() {
        return attachfileType;
    }

    public String getAttachfileName() {
        return attachfileName;
    }

    public String getAttachfileTitle() {
        return attachfileTitle;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public int getSize() {
        return size;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setAttachfileNo(Integer attachfileNo) {
        this.attachfileNo = attachfileNo;
    }

    public void setAttachfileType(String attachfileType) {
        this.attachfileType = attachfileType;
    }

    public void setAttachfileName(String attachfileName) {
        this.attachfileName = attachfileName;
    }

    public void setAttachfileTitle(String attachfileTitle) {
        this.attachfileTitle = attachfileTitle;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
	
}
