package digipages.dto;

import java.util.Date;
import java.util.List;

import com.google.gson.annotations.Expose;

public class MediabookChapterDTO {

    @Expose
	private long id;
	
    @Expose
	private List<MediabookSubtitleDTO> subtitles;

    @Expose
	private String version;

    @Expose
	private String cat;

    @Expose
	private long chapterNo;

    @Expose
	private String chapterName;

    @Expose
	private String chapterSize;

    @Expose
	private String chapterLength;

    @Expose
	private String isScript;

    @Expose
	private String scriptFilename;

    @Expose
	private String scriptFile;
    
    @Expose
    private Long scriptSize;

    @Expose
	private String isSubtitle;

    @Expose
	private PreviewInfo previewInfo;

    @Expose
	private ChapterInfo chapterInfo;

    @Expose
	private String fileLocation;

    @Expose
	private String format;

    @Expose
	private Date createTime = new Date();

    @Expose
	protected Date lastUpdated;

    public long getId() {
        return id;
    }

    public List<MediabookSubtitleDTO> getSubtitles() {
        return subtitles;
    }

    public String getVersion() {
        return version;
    }

    public String getCat() {
        return cat;
    }

    public long getChapterNo() {
        return chapterNo;
    }

    public String getChapterName() {
        return chapterName;
    }

    public String getChapterSize() {
        return chapterSize;
    }

    public String getChapterLength() {
        return chapterLength;
    }

    public String getIsScript() {
        return isScript;
    }

    public String getScriptFilename() {
        return scriptFilename;
    }

    public String getScriptFile() {
        return scriptFile;
    }

    public String getIsSubtitle() {
        return isSubtitle;
    }

    public PreviewInfo getPreviewInfo() {
        return previewInfo;
    }

    public ChapterInfo getChapterInfo() {
        return chapterInfo;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public String getFormat() {
        return format;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSubtitles(List<MediabookSubtitleDTO> subtitles) {
        this.subtitles = subtitles;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public void setChapterNo(long chapterNo) {
        this.chapterNo = chapterNo;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public void setChapterSize(String chapterSize) {
        this.chapterSize = chapterSize;
    }

    public void setChapterLength(String chapterLength) {
        this.chapterLength = chapterLength;
    }

    public void setIsScript(String isScript) {
        this.isScript = isScript;
    }

    public void setScriptFilename(String scriptFilename) {
        this.scriptFilename = scriptFilename;
    }

    public void setScriptFile(String scriptFile) {
        this.scriptFile = scriptFile;
    }

    public void setIsSubtitle(String isSubtitle) {
        this.isSubtitle = isSubtitle;
    }

    public void setPreviewInfo(PreviewInfo previewInfo) {
        this.previewInfo = previewInfo;
    }

    public void setChapterInfo(ChapterInfo chapterInfo) {
        this.chapterInfo = chapterInfo;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Long getScriptSize() {
        return scriptSize;
    }

    public void setScriptSize(Long scriptSize) {
        this.scriptSize = scriptSize;
    }
	
}
