package digipages.dto;

public class MemberDrmLogDTO {
    
    @Override
    public String toString() {
        return "MemberDrmLogDTO [booksMemberId=" + booksMemberId + ", itemId=" + itemId + ", downloadExpireTime=" + downloadExpireTime
                + ", readStartTime=" + readStartTime + ", readDays=" + readDays + ", readExpireTime=" + readExpireTime + ", type=" + type
                + ", transactionId=" + transactionId + ", sessionToken=" + sessionToken + ", viewerBaseURI=" + viewerBaseURI + ", status=" + status
                + ", submitId=" + submitId + ", memberType=" + memberType + "]";
    }
    private String booksMemberId = "";
    private String itemId = "";
    private String downloadExpireTime = "";
    private String readStartTime = "";
    private String readDays = "";
    private String readExpireTime = "";
    private String type = "1"; // default type =1 (normal)
    private String transactionId = "";
    private String sessionToken = "";
    private String viewerBaseURI;
    private String status="";
	private String submitId="";
    private String memberType = "NormalReader";
    private String orgItemId = "";
    
    public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getReadStartTime() {
        return readStartTime;
    }
    public void setReadStartTime(String readStartTime) {
        this.readStartTime = readStartTime;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getBooksMemberId() {
        return booksMemberId;
    }
    public void setBooksMemberId(String booksMemberId) {
        this.booksMemberId = booksMemberId;
    }
    public String getItemId() {
        return itemId;
    }
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
    public String getDownloadExpireTime() {
        return downloadExpireTime;
    }
    public void setDownloadExpireTime(String downloadExpireTime) {
        this.downloadExpireTime = downloadExpireTime;
    }
    public String getReadDays() {
        return readDays;
    }
    public void setReadDays(String readDays) {
        this.readDays = readDays;
    }
    public String getReadExpireTime() {
        return readExpireTime;
    }
    public void setReadExpireTime(String readExpireTime) {
        this.readExpireTime = readExpireTime;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    public String getSessionToken() {
        return sessionToken;
    }
    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
    public String getViewerBaseURI() {
        return viewerBaseURI;
    }
    public void setViewerBaseURI(String viewerBaseURI) {
        this.viewerBaseURI = viewerBaseURI;
    }
    public String getSubmitId() {
		return submitId;
	}
	public void setSubmitId(String submitId) {
		this.submitId = submitId;
	}
	public String getOrgItemId() {
		return orgItemId;
	}
	public void setOrgItemId(String orgItemId) {
		this.orgItemId = orgItemId;
	}
}
