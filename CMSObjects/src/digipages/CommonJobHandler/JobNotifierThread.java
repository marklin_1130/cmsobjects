package digipages.CommonJobHandler;

import java.util.Date;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.google.gson.Gson;

import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.DPLogger;
import model.ScheduleJob;

public class JobNotifierThread extends Thread {
	static DPLogger logger = DPLogger.getLogger(JobNotifierThread.class.getName());
	private ScheduleJob job;
	private static Properties config;
	private static EntityManagerFactory emFactory;
	private static Gson gson=new Gson();
	
	public JobNotifierThread(ScheduleJob job, Properties config, EntityManagerFactory emFactory) {
		this.job=job;
		JobNotifierThread.config=config;
		JobNotifierThread.emFactory=emFactory;
	}
	
	@Override
	public void run()
	{
		// only if the job status is 0 (about run)
		if (!(job.getStatus()==0))
		{
			return;
		}
		
		// in the future => wait for it.
		if (job.getShouldStartTime().after(new Date())){
			return;
		}
		
		try {
//			logger.error("job "+ gson.toJson(job));
//			logger.error("jobRunner "+ job.getJobRunner());
			RunMode runMode = RunMode.getValue(job.getJobRunner());
			logger.info("Notify workers: runMode=" + runMode + "\nJob=" + gson.toJson(job));
			switch (runMode){
			case LambdaMode:
				pushLambdaJob(job);
				break;
			case ExtEc2Mode:
				Ec2WorkerManager.adjustWorkers();
				break;
			case OPENBOOK_EC2:
			    Ec2WorkerManager.adjustOpenBookWorkers();
			    break;
			case DirectRunMode:
				pushLocalJob();
				break;
			case LocalMode:
				pushLocalJob();
				break;
			default: // direct mode
				pushLocalJob();
				break;
			}
		}
		catch (Exception ex)
		{
			logger.error("Fail to Notify Workers !! ",ex);
			job.setStatus(-1);// failed
			job.setErrMessage(ex.getMessage());
			ScheduleJobManager.updateJob(job);
		}
	}
	
	private static void pushLocalJob() {
		LocalRunner.NotifyRunner();
	}


	private static void pushLambdaJob(ScheduleJob job) {
		
		// not yet my time
		if (job.getShouldStartTime().after(new Date())){
			return;
		}
		// only 0 = about run.
		if (job.getStatus()!=0)
			return;
		// lambda job runner =1
		job.setJobRunner(1);

		// doing uuid
		EntityManager em=emFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(job);
			job.setUuid(UUID.randomUUID());
			em.getTransaction().commit();
		} catch (Exception e){
			logger.error("Unexcepted job merge.",e);
		}finally{
			if (null!=em ){
				if (em.getTransaction().isActive())
					em.getTransaction().rollback();
				em.close();
			}
		}
	
		LambdaWorkerManager.NotifyRunner(config,job);
		
	}
	
}
