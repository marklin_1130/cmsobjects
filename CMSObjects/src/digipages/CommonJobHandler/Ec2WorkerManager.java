package digipages.CommonJobHandler;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.ActiveInstance;
import com.amazonaws.services.ec2.model.BlockDeviceMapping;
import com.amazonaws.services.ec2.model.CancelSpotFleetRequestsRequest;
import com.amazonaws.services.ec2.model.CancelSpotFleetRequestsResult;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotFleetInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeSpotFleetInstancesResult;
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsResult;
import com.amazonaws.services.ec2.model.DescribeVolumesRequest;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.EbsBlockDevice;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.FleetType;
import com.amazonaws.services.ec2.model.GroupIdentifier;
import com.amazonaws.services.ec2.model.IamInstanceProfileSpecification;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.RequestSpotFleetRequest;
import com.amazonaws.services.ec2.model.RequestSpotFleetResult;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.SpotFleetLaunchSpecification;
import com.amazonaws.services.ec2.model.SpotFleetRequestConfig;
import com.amazonaws.services.ec2.model.SpotFleetRequestConfigData;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.Volume;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.WorkerStatus;

/**
 * adjust Ec2 Runners
 * 
 * @author Eric.CL.Chou
 *
 */
public class Ec2WorkerManager {
    private static final DPLogger logger = DPLogger.getLogger(Ec2WorkerManager.class.getName());
    private static int workerLoadFactor = 5; // avg worker handle size. (small =
                                             // frequent start/stop, big =
                                             // longer digest time)
    private static EntityManagerFactory emFactory = null;
    private static boolean startingEc2 = false;
    private static boolean startingOpenBookEc2 = false;
    private static int maxEc2Count = 10;
    private static int maxEc2OPENBOOKCount = 20;
    // private static String endPoint = "ec2.ap-northeast-1.amazonaws.com";
    private static String Ec2WorkerImgId;
    private static String Ec2WorkerOpenBookImgId;
    private static boolean inDebug = false;
    private static String subnetId_1a;
    private static String subnetId_1c;
    private static String WorkerScript;
    private static String WorkerOpenBookScript;
    private static String SecurityGroupId;
    private static String spotInstanceType1;
    private static String spotInstancePrice1;
    private static String spotInstanceType2;
    private static String spotInstancePrice2;
    private static String onDemonInstanceType;
    private static String ec2S3Profile;
    private static String env;
    private static String iamFleetRole;

    private Ec2WorkerManager() {
        System.setProperty("org.xml.sax.driver", "com.sun.org.apache.xerces.internal.parsers.SAXParser");
    }

    // for test
    public static void main(String[] args) {

        startingEc2 = true;

        Properties config = new Properties();

        List<String> instanceId = null;
        String workScript = "";
        try {

            InputStream inputStream = null;

            try {
                inputStream = new FileInputStream("/usr/share/tomcat8/properties/apibook.properties");
                config.load(inputStream);
                inputStream.close();
                inputStream = new FileInputStream("/usr/share/tomcat8/properties/WorkerInitScript.sh");
                workScript = CommonUtil.inputStreamToString(inputStream);
                inputStream.close();
                inputStream = new FileInputStream("/usr/share/tomcat8/properties/WorkerOpenBookInitScript.sh");
                WorkerOpenBookScript = CommonUtil.inputStreamToString(inputStream);
                inputStream.close();
            } catch (FileNotFoundException e) {
                logger.error(e);
                ;
            } catch (IOException e) {
                logger.error(e);
                ;
            }

            setConfig(config, workScript , WorkerOpenBookScript);

            logger.debug("EC2-Convert-"+"Start a new EC2");

            AmazonEC2 amazonEC2Client = AmazonEC2ClientBuilder.defaultClient();
            BlockDeviceMapping deviceMapping = new BlockDeviceMapping();
            EbsBlockDevice ebs = new EbsBlockDevice();
            ebs.setDeleteOnTermination(true);
            ebs.setVolumeSize(30);
            deviceMapping.setEbs(ebs);
            deviceMapping.setDeviceName("/dev/sda1"); // centos
            IamInstanceProfileSpecification iamInstanceProfile = new IamInstanceProfileSpecification();
            iamInstanceProfile.withArn(ec2S3Profile);

            logger.debug("EC2-Convert-"+"RUN-TYPE: spot feel 1a&1c spec:"+spotInstanceType1);

            // spot type1 1a
            RequestSpotFleetRequest requestSpotFleetRequest = configRequestSpotFleetRequest(deviceMapping, iamInstanceProfile,spotInstanceType1,spotInstancePrice1);

            instanceId = doSpotFleetRequestEC2(amazonEC2Client, requestSpotFleetRequest);

            if (instanceId.isEmpty()) {
            	logger.debug("EC2-Convert-"+"RUN-TYPE: spot feel 1a&1c spec:"+spotInstanceType2);
                // on demon 1a
            	requestSpotFleetRequest = configRequestSpotFleetRequest(deviceMapping, iamInstanceProfile,spotInstanceType2,spotInstancePrice2);

                instanceId = doSpotFleetRequestEC2(amazonEC2Client, requestSpotFleetRequest);

            }
//
//            if (instanceId.isEmpty()) {
//                logger.debug("EC2-Convert-"+"RUN-TYPE: on demon 1c");
//                // on demon 1c
//                RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
//                runInstancesRequest.withImageId(Ec2WorkerImgId).withInstanceType(onDemonInstanceType).withMinCount(1).withMaxCount(1)
//                        .withBlockDeviceMappings(deviceMapping).withSecurityGroupIds(SecurityGroupId).withSubnetId(subnetId_1c)
//                        .withIamInstanceProfile(iamInstanceProfile).withUserData(Base64.encodeBase64String(WorkerScript.getBytes("UTF-8")))
//                        .withInstanceInitiatedShutdownBehavior("terminate");
//
//                instanceId = doRequestEC2(amazonEC2Client, runInstancesRequest);
//            }

            logger.debug("EC2-Convert-"+"start EC2 finish instanceId:" + instanceId.get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setInDebug(boolean inDebug) {
        Ec2WorkerManager.inDebug = inDebug;
    }

    // must be set before run.
    public static void setEntityManagerFactory(EntityManagerFactory emFactory) {
        Ec2WorkerManager.emFactory = emFactory;
    }

    // must be set before run.
    public static void setConfig(Properties prop, String startScript,String startOpenBookScript) throws IOException {
        workerLoadFactor = Integer.parseInt(prop.getProperty("WorkerLoadFactor", "5"));
        maxEc2Count = Integer.parseInt(prop.getProperty("MaxEc2Count", "10"));
        Ec2WorkerImgId = prop.getProperty("Ec2WorkerImgId");
        Ec2WorkerOpenBookImgId = prop.getProperty("Ec2WorkerOpenBookImgId");
        subnetId_1a = prop.getProperty("subnetId_1a");
        subnetId_1c = prop.getProperty("subnetId_1c");
        SecurityGroupId = prop.getProperty("SecurityGroupId");
        spotInstanceType1 = prop.getProperty("SpotInstanceType1");
        spotInstancePrice1 = prop.getProperty("SpotInstancePrice1");
        spotInstanceType2 = prop.getProperty("SpotInstanceType2");
        spotInstancePrice2 = prop.getProperty("SpotInstancePrice2");
        onDemonInstanceType = prop.getProperty("OnDemonInstanceType");
        WorkerScript = startScript;
        WorkerOpenBookScript = startOpenBookScript;
        ec2S3Profile = prop.getProperty("EC2S3Profile");
        env = prop.getProperty("env");
        iamFleetRole = prop.getProperty("IamFleetRole");
        try {
        
        XPathFactory xpf = XPathFactory.newInstance(
                XPathFactory.DEFAULT_OBJECT_MODEL_URI,
                "net.sf.saxon.xpath.XPathFactoryImpl",
                ClassLoader.getSystemClassLoader());
        
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    
    /**
     * rule: if (waitingJobs > 5 and no idle Ec2) start a new EC2 but pay
     * attention to starting machines. if fail, don't force it or crash the
     * process.
     */
    public static void adjustWorkers() {
        try {
            int totalEc2 = totalEc2Count();
            int idleEc2 = countIdleEC2();
            int waitingJobs = waitingJobs();
            if (totalEc2 >= maxEc2Count // too many Ec2
                    || startingEc2 // in Starting
                    || idleEc2 >= 3 // have idle Ec2
                    || waitingJobs < 1) // no jobs
            {
                return;
            }

            // init case no ec2
            if (totalEc2 == 0) {
                startNewEc2();
                return;
            }
            // compare ratio of job/Workers
            if ((waitingJobs / workerLoadFactor) > idleEc2) {
                startNewEc2();
                return;
            }
        } catch (Exception e) {
            logger.error("Fail adjustWorkers", e);
        }
    }
    
    public static void adjustOpenBookWorkers() {
        try {
            int totalEc2 = totalOpenBookEc2Count();
            int idleEc2 = countIdleOpenBookEC2();
            int waitingJobs = waitingOpenBookJobs();
            if (totalEc2 >= maxEc2OPENBOOKCount // too many Ec2
                    || startingOpenBookEc2 // in Starting
                    || idleEc2 >= 1 // have idle Ec2
                    || waitingJobs < 1) // no jobs
            {
                return;
            }

            // init case no ec2
            if (totalEc2 == 0) {
                startNewOpenBookEc2();
                return;
            }
            // compare ratio of job/Workers
            if ((waitingJobs / workerLoadFactor) > idleEc2) {
                startNewOpenBookEc2();
                return;
            }
        } catch (Exception e) {
            logger.error("Fail adjustWorkers", e);
        }
    }

    static int totalEc2Count() {
        int count = 0;
        EntityManager em = emFactory.createEntityManager();
        try {
            count = WorkerStatus.countAllInstances(em);
        } catch (Exception e) {
            logger.error("Fail to count Ec2 workers", e);
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            em.close();
        }

        return count;
    }
    
    static int totalOpenBookEc2Count() {
        int count = 0;
        EntityManager em = emFactory.createEntityManager();
        try {
            count = WorkerStatus.countOpenBookAllInstances(em);
        } catch (Exception e) {
            logger.error("Fail to count Ec2 workers", e);
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            em.close();
        }

        return count;
    }

    /**
     * start new Ec2 by command
     * 
     * @throws Exception
     */
    static synchronized void startNewEc2() throws Exception {

        WorkerStatus ws = null;
        String instanceId = "";
        ArrayList<String> instanceIds = new ArrayList<String>();
        
        boolean isSpot = false;
        
        try {

            startingEc2 = true;
            if (inDebug) {
                logger.debug("EC2-Convert-"+"Start a new EC2");
                instanceId = UUID.randomUUID().toString();
                ws = initNewWorker();
                ws.setInstanceId(instanceId);
                updateWorkerStatus(ws);
                startingEc2 = false;
                return;
            }

            ws = initNewWorker();
            AmazonEC2 amazonEC2Client = AmazonEC2ClientBuilder.defaultClient();
            BlockDeviceMapping deviceMapping = new BlockDeviceMapping();
            EbsBlockDevice ebs = new EbsBlockDevice();
            ebs.setDeleteOnTermination(true);
            ebs.setVolumeSize(30);
            deviceMapping.setEbs(ebs);
            deviceMapping.setDeviceName("/dev/sda1"); // centos
            IamInstanceProfileSpecification iamInstanceProfile = new IamInstanceProfileSpecification();
            iamInstanceProfile.withArn(ec2S3Profile);

            RequestSpotFleetRequest requestSpotFleetRequest = configRequestSpotFleetRequest(deviceMapping, iamInstanceProfile,spotInstanceType1,spotInstancePrice1);
            instanceIds.addAll(doSpotFleetRequestEC2(amazonEC2Client, requestSpotFleetRequest));

            RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
            runInstancesRequest.withImageId(Ec2WorkerImgId).withInstanceType(onDemonInstanceType).withMinCount(1).withMaxCount(1)
                    .withBlockDeviceMappings(deviceMapping).withSecurityGroupIds(SecurityGroupId).withIamInstanceProfile(iamInstanceProfile)
                    .withUserData(Base64.encodeBase64String(WorkerScript.getBytes("UTF-8"))).withInstanceInitiatedShutdownBehavior("terminate");
            
            if (instanceId.isEmpty()) {
            	logger.debug("EC2-Convert-"+"RUN-TYPE: spot feel 1a&1c spec:"+spotInstanceType2);
                // on demon 1a
            	requestSpotFleetRequest = configRequestSpotFleetRequest(deviceMapping, iamInstanceProfile,spotInstanceType2,spotInstancePrice2);

                List<String> instance = doSpotFleetRequestEC2(amazonEC2Client, requestSpotFleetRequest);
                if(instance!=null && !instance.isEmpty()) {
                	instanceId = instance.get(0);
                }

            }
            
//            if (instanceIds.isEmpty()) {
//                logger.debug("EC2-Convert-"+"request Instance with OnDemon: " + subnetId_1a);
//                runInstancesRequest.withSubnetId(subnetId_1a);
//                instanceIds.addAll(doRequestEC2(amazonEC2Client, runInstancesRequest));
//            }
//
//            if (instanceIds.isEmpty()) {
//                logger.debug("EC2-Convert-"+"request Instance with OnDemon: " + subnetId_1c);
//                runInstancesRequest.withSubnetId(subnetId_1c);
//                instanceIds.addAll(doRequestEC2(amazonEC2Client, runInstancesRequest));
//            }
            if (instanceIds.isEmpty()) {
                throw new ServerException("id_err_999", "Fail to Start Ec2");
            } else {
                ws.setInstanceId(instanceIds.get(0));
                updateWorkerStatus(ws);
            }
            startingEc2 = false;
        } catch (Exception e) {
            logger.error("Unexcepted when starting Ec2.", e);
            // update worker status if it is in fail mode.
            if (ws != null) {
                ws.setStatus(-1);
                updateWorkerStatus(ws);
            }
            startingEc2 = false;
            throw e;
        }

    }
    
    /**
     * start new Ec2 by command
     * 
     * @throws Exception
     */
    static synchronized void startNewOpenBookEc2() throws Exception {

        WorkerStatus ws = null;
        String instanceId = "";
        ArrayList<String> instanceIds = new ArrayList<String>();
        
        boolean isSpot = false;
        
        try {

            startingOpenBookEc2 = true;

            ws = initOpenBookNewWorker();
            AmazonEC2 amazonEC2Client = AmazonEC2ClientBuilder.defaultClient();
            BlockDeviceMapping deviceMapping = new BlockDeviceMapping();
            EbsBlockDevice ebs = new EbsBlockDevice();
            ebs.setDeleteOnTermination(true);
            ebs.setVolumeSize(30);
            deviceMapping.setEbs(ebs);
            deviceMapping.setDeviceName("/dev/sda1"); // centos
            IamInstanceProfileSpecification iamInstanceProfile = new IamInstanceProfileSpecification();
            iamInstanceProfile.withArn(ec2S3Profile);

            RequestSpotFleetRequest requestSpotFleetRequest = configOpenBookRequestSpotFleetRequest(deviceMapping, iamInstanceProfile);
            instanceIds.addAll(doOpenBookSpotFleetRequestEC2(amazonEC2Client, requestSpotFleetRequest));

            RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
            runInstancesRequest.withImageId(Ec2WorkerOpenBookImgId).withInstanceType(onDemonInstanceType).withMinCount(1).withMaxCount(1)
                    .withBlockDeviceMappings(deviceMapping).withSecurityGroupIds(SecurityGroupId).withIamInstanceProfile(iamInstanceProfile)
                    .withUserData(Base64.encodeBase64String(WorkerOpenBookScript.getBytes("UTF-8"))).withInstanceInitiatedShutdownBehavior("terminate");
//            if (instanceIds.isEmpty()) {
//                logger.debug("EC2-Convert-"+"request Instance with OnDemon: " + subnetId_1a);
//                runInstancesRequest.withSubnetId(subnetId_1a);
//                instanceIds.addAll(doRequestEC2(amazonEC2Client, runInstancesRequest));
//            }
//
//            if (instanceIds.isEmpty()) {
//                logger.debug("EC2-Convert-"+"request Instance with OnDemon: " + subnetId_1c);
//                runInstancesRequest.withSubnetId(subnetId_1c);
//                instanceIds.addAll(doRequestEC2(amazonEC2Client, runInstancesRequest));
//            }
            if (instanceIds.isEmpty()) {
                throw new ServerException("id_err_999", "Fail to Start Ec2");
            } else {
                ws.setInstanceId("OPENBOOK-"+instanceIds.get(0));
                updateWorkerStatus(ws);
            }
            startingOpenBookEc2 = false;
        } catch (Exception e) {
            logger.error("Unexcepted when starting Ec2.", e);
            // update worker status if it is in fail mode.
            if (ws != null) {
                ws.setStatus(-1);
                updateWorkerStatus(ws);
            }
            startingOpenBookEc2 = false;
            throw e;
        }

    }
    
    private static ArrayList<String> doSpotFleetRequestEC2(AmazonEC2 amazonEC2Client, RequestSpotFleetRequest requestSpotFleetRequest) {

        // a list of instances to tag.
        ArrayList<String> instanceIds = new ArrayList<String>();
        try {

            logger.debug("EC2-Convert-"+"Spot Instance Feel request start...");
            logger.debug("EC2-Convert-"+"request:" + ReflectionToStringBuilder.toString(requestSpotFleetRequest, ToStringStyle.SIMPLE_STYLE));
            RequestSpotFleetResult requestSpotFleetResult = amazonEC2Client.requestSpotFleet(requestSpotFleetRequest);
            String resultStr = requestSpotFleetResult.toString();
            logger.debug("EC2-Convert-"+"Run Instance result: " + resultStr);
            String lower = resultStr.toLowerCase();
            if (lower.contains("fail") || lower.contains("error")) {
                logger.error("Fail to start ec2 worker: " + resultStr);
                startingEc2 = true;
            }

            String requestId = requestSpotFleetResult.getSpotFleetRequestId();
            logger.debug("EC2-Convert-"+"get requestId:" + requestId);

            try {
                logger.debug("EC2-Convert-"+"wait for instance... 60s");
                Thread.sleep(60 * 1000); // sleep 60s.
            } catch (Exception e) {
                // Do nothing if the thread woke up early.
            }

            DescribeSpotFleetRequestsRequest describeSpotFleetRequestsRequest = new DescribeSpotFleetRequestsRequest();
            describeSpotFleetRequestsRequest.withSpotFleetRequestIds(requestId);

            CancelSpotFleetRequestsRequest cancelSpotFleetRequestsRequest = new CancelSpotFleetRequestsRequest();
            cancelSpotFleetRequestsRequest.withSpotFleetRequestIds(requestId);
            cancelSpotFleetRequestsRequest.withTerminateInstances(false);

            try {
                // Get the requests to monitor
                DescribeSpotFleetRequestsResult describeSpotFleetRequestsResult = amazonEC2Client.describeSpotFleetRequests(describeSpotFleetRequestsRequest);
                // are any requests open?
                List<SpotFleetRequestConfig> listSpotFleetRequestConfig = describeSpotFleetRequestsResult.getSpotFleetRequestConfigs();
                for (SpotFleetRequestConfig spotFleetRequestConfig : listSpotFleetRequestConfig) {
                    String state = spotFleetRequestConfig.getSpotFleetRequestState();
                    logger.debug("EC2-Convert-"+"current request:" + describeSpotFleetRequestsRequest.getSpotFleetRequestIds() + ",state:" + state);
                    if (state.equalsIgnoreCase("active")) {
                        DescribeSpotFleetInstancesRequest describeSpotFleetInstancesRequest = new DescribeSpotFleetInstancesRequest();
                        describeSpotFleetInstancesRequest.setSpotFleetRequestId(requestId);
                        DescribeSpotFleetInstancesResult describeSpotFleetInstancesResult = amazonEC2Client.describeSpotFleetInstances(describeSpotFleetInstancesRequest);
                        logger.debug("EC2-Convert-"+ReflectionToStringBuilder.toString(describeSpotFleetInstancesResult, ToStringStyle.SIMPLE_STYLE));
                        List<ActiveInstance> instanceList = describeSpotFleetInstancesResult.getActiveInstances();
                        for (ActiveInstance activeInstance : instanceList) {
                            logger.debug("EC2-Convert-"+"instanceId:" + activeInstance.getInstanceId());
                            instanceIds.add(activeInstance.getInstanceId());
                        }
                    }
                }

                if (!instanceIds.isEmpty()) {
                    logger.debug("EC2-Convert-"+"create Tag");
                    String tagName = "ebook-convert-" + env;
                    // Create a list of tags to create
                    ArrayList<Tag> instanceTags = new ArrayList<Tag>();
                    instanceTags.add(new Tag("Name", tagName));

                    // Create the tag request
                    CreateTagsRequest createTagsRequest_instances = new CreateTagsRequest();
                    createTagsRequest_instances.setResources(instanceIds);
                    createTagsRequest_instances.setTags(instanceTags);

                    // Tag the instance
                    try {
                        amazonEC2Client.createTags(createTagsRequest_instances);
                    } catch (AmazonServiceException e) {
                        // Write out any exceptions that may have occurred.
                        logger.error("Error terminating instances");
                        logger.error("Caught Exception: " + e.getMessage());
                        logger.error("Reponse Status Code: " + e.getStatusCode());
                        logger.error("Error Code: " + e.getErrorCode());
                        logger.error("Request ID: " + e.getRequestId());
                    }

                    CancelSpotFleetRequestsResult cancelSpotFleetRequestsResult = amazonEC2Client.cancelSpotFleetRequests(cancelSpotFleetRequestsRequest);
                    logger.debug("EC2-Convert-"+cancelSpotFleetRequestsResult.toString());
                    
                    new SpotInstanceMonitorThread(emFactory, amazonEC2Client,instanceIds.get(0),tagName).start();
                    
                    // describeVolumes       
                    DescribeVolumesRequest request = new DescribeVolumesRequest().withFilters(new Filter().withName("attachment.instance-id").withValues(instanceIds.get(0)));
                    DescribeVolumesResult response = amazonEC2Client.describeVolumes(request);
                    // Create a list of tags to create
                    for (Volume volume : response.getVolumes()) {
                        logger.info(">> Tagging volume {} / {}", volume.getVolumeId(), instanceIds.get(0));
                        amazonEC2Client.createTags(new CreateTagsRequest().withTags(instanceTags).withResources(volume.getVolumeId()));
                    }
                    
                }

            } catch (Exception e) {
                logger.debug("EC2-Convert-"+"cancel spot request ", e);
            } finally {
                logger.debug("EC2-Convert-"+"finally cancel spot request ");
                CancelSpotFleetRequestsResult cancelSpotFleetRequestsResult = amazonEC2Client.cancelSpotFleetRequests(cancelSpotFleetRequestsRequest);
                logger.debug("EC2-Convert-"+cancelSpotFleetRequestsResult.toString());
            }

        } catch (Exception e) {
            logger.debug("EC2-Convert-"+"fail to get instanceId", e);
        }

        return instanceIds;

    }
    private static ArrayList<String> doOpenBookSpotFleetRequestEC2(AmazonEC2 amazonEC2Client, RequestSpotFleetRequest requestSpotFleetRequest) {

        // a list of instances to tag.
        ArrayList<String> instanceIds = new ArrayList<String>();
        try {

            logger.debug("EC2-Convert-"+"Spot Instance Feel request start...");
            logger.debug("EC2-Convert-"+"request:" + ReflectionToStringBuilder.toString(requestSpotFleetRequest, ToStringStyle.SIMPLE_STYLE));
            RequestSpotFleetResult requestSpotFleetResult = amazonEC2Client.requestSpotFleet(requestSpotFleetRequest);
            String resultStr = requestSpotFleetResult.toString();
            logger.debug("EC2-Convert-"+"Run Instance result: " + resultStr);
            String lower = resultStr.toLowerCase();
            if (lower.contains("fail") || lower.contains("error")) {
                logger.error("Fail to start ec2 worker: " + resultStr);
                startingEc2 = true;
            }

            String requestId = requestSpotFleetResult.getSpotFleetRequestId();
            logger.debug("EC2-Convert-"+"get requestId:" + requestId);

            try {
                logger.debug("EC2-Convert-"+"wait for instance... 60s");
                Thread.sleep(60 * 1000); // sleep 60s.
            } catch (Exception e) {
                // Do nothing if the thread woke up early.
            }

            DescribeSpotFleetRequestsRequest describeSpotFleetRequestsRequest = new DescribeSpotFleetRequestsRequest();
            describeSpotFleetRequestsRequest.withSpotFleetRequestIds(requestId);

            CancelSpotFleetRequestsRequest cancelSpotFleetRequestsRequest = new CancelSpotFleetRequestsRequest();
            cancelSpotFleetRequestsRequest.withSpotFleetRequestIds(requestId);
            cancelSpotFleetRequestsRequest.withTerminateInstances(false);

            try {
                // Get the requests to monitor
                DescribeSpotFleetRequestsResult describeSpotFleetRequestsResult = amazonEC2Client.describeSpotFleetRequests(describeSpotFleetRequestsRequest);
                // are any requests open?
                List<SpotFleetRequestConfig> listSpotFleetRequestConfig = describeSpotFleetRequestsResult.getSpotFleetRequestConfigs();
                for (SpotFleetRequestConfig spotFleetRequestConfig : listSpotFleetRequestConfig) {
                    String state = spotFleetRequestConfig.getSpotFleetRequestState();
                    logger.debug("EC2-Convert-"+"current request:" + describeSpotFleetRequestsRequest.getSpotFleetRequestIds() + ",state:" + state);
                    if (state.equalsIgnoreCase("active")) {
                        DescribeSpotFleetInstancesRequest describeSpotFleetInstancesRequest = new DescribeSpotFleetInstancesRequest();
                        describeSpotFleetInstancesRequest.setSpotFleetRequestId(requestId);
                        DescribeSpotFleetInstancesResult describeSpotFleetInstancesResult = amazonEC2Client.describeSpotFleetInstances(describeSpotFleetInstancesRequest);
                        logger.debug("EC2-Convert-"+ReflectionToStringBuilder.toString(describeSpotFleetInstancesResult, ToStringStyle.SIMPLE_STYLE));
                        List<ActiveInstance> instanceList = describeSpotFleetInstancesResult.getActiveInstances();
                        for (ActiveInstance activeInstance : instanceList) {
                            logger.debug("EC2-Convert-"+"instanceId:" + activeInstance.getInstanceId());
                            instanceIds.add(activeInstance.getInstanceId());
                        }
                    }
                }

                if (!instanceIds.isEmpty()) {
                    logger.debug("EC2-Convert-"+"create Tag");
                    String tagName = "ebook-openbook-" + env;
                    // Create a list of tags to create
                    ArrayList<Tag> instanceTags = new ArrayList<Tag>();
                    instanceTags.add(new Tag("Name",tagName));

                    // Create the tag request
                    CreateTagsRequest createTagsRequest_instances = new CreateTagsRequest();
                    createTagsRequest_instances.setResources(instanceIds);
                    createTagsRequest_instances.setTags(instanceTags);

                    // Tag the instance
                    try {
                        amazonEC2Client.createTags(createTagsRequest_instances);
                    } catch (AmazonServiceException e) {
                        // Write out any exceptions that may have occurred.
                        logger.error("Error terminating instances");
                        logger.error("Caught Exception: " + e.getMessage());
                        logger.error("Reponse Status Code: " + e.getStatusCode());
                        logger.error("Error Code: " + e.getErrorCode());
                        logger.error("Request ID: " + e.getRequestId());
                    }

                    CancelSpotFleetRequestsResult cancelSpotFleetRequestsResult = amazonEC2Client.cancelSpotFleetRequests(cancelSpotFleetRequestsRequest);
                    logger.debug("EC2-Convert-"+cancelSpotFleetRequestsResult.toString());
                    
                    new SpotInstanceMonitorThread(emFactory, amazonEC2Client,instanceIds.get(0),tagName).start();
                    
                    // describeVolumes       
                    DescribeVolumesRequest request = new DescribeVolumesRequest().withFilters(new Filter().withName("attachment.instance-id").withValues(instanceIds.get(0)));
                    DescribeVolumesResult response = amazonEC2Client.describeVolumes(request);
                    // Create a list of tags to create
                    for (Volume volume : response.getVolumes()) {
                        logger.info(">> Tagging volume {} / {}", volume.getVolumeId(), instanceIds.get(0));
                        amazonEC2Client.createTags(new CreateTagsRequest().withTags(instanceTags).withResources(volume.getVolumeId()));
                    }
                    
                }

            } catch (Exception e) {
                logger.debug("EC2-Convert-"+"cancel spot request ", e);
            } finally {
                logger.debug("EC2-Convert-"+"finally cancel spot request ");
                CancelSpotFleetRequestsResult cancelSpotFleetRequestsResult = amazonEC2Client.cancelSpotFleetRequests(cancelSpotFleetRequestsRequest);
                logger.debug("EC2-Convert-"+cancelSpotFleetRequestsResult.toString());
            }

        } catch (Exception e) {
            logger.debug("EC2-Convert-"+"fail to get instanceId", e);
        }

        return instanceIds;

    }

    private static ArrayList<String> doRequestEC2(AmazonEC2 amazonEC2Client, RunInstancesRequest runInstancesRequest) {

        // a list of instances to tag.
        ArrayList<String> instanceIds = new ArrayList<String>();

        RunInstancesResult runInstancesResult = amazonEC2Client.runInstances(runInstancesRequest);
        try {
            instanceIds.add(runInstancesResult.getReservation().getInstances().get(0).getInstanceId());
            // TAG EC2 INSTANCES
            List<Instance> instances = runInstancesResult.getReservation().getInstances();
            int idx = 1;
            for (Instance instance : instances) {
                CreateTagsRequest createTagsRequest = new CreateTagsRequest();
                createTagsRequest.withResources(instance.getInstanceId()).withTags(new Tag("Name", "ebook-convert" + env));
                amazonEC2Client.createTags(createTagsRequest);
                idx++;
            }
        } catch (Exception e) {
            logger.error(e);
        }

        String resultStr = runInstancesResult.toString();
        logger.debug("EC2-Convert-"+"Run Instance result: " + resultStr);
        String lower = resultStr.toLowerCase();
        if (lower.contains("fail") || lower.contains("error")) {
            logger.error("Fail to start ec2 worker: " + resultStr);
            startingEc2 = true;
        }

        return instanceIds;

    }

    private static void updateWorkerStatus(WorkerStatus ws) {
        EntityManager em = emFactory.createEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(ws);
            ws.setStatus(0);
            em.getTransaction().commit();

        } catch (Exception e) {
            logger.debug("EC2-Convert-"+"Fail to update worker ?! ", e);
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            em.close();
        }
    }

    // initialize a Instance with thread id 0 (default)
    private static WorkerStatus initNewWorker() {
        EntityManager em = emFactory.createEntityManager();
        try {
            WorkerStatus ws = new WorkerStatus();
            ws.setStatus(0);

            em.getTransaction().begin();
            ws.setInstanceId("InitInstance" + UUID.randomUUID().toString());
            ws.setThreadId(0);
            ws.setWorkerStartTime(new Date());
            em.persist(ws);

            em.getTransaction().commit();
            return ws;
        } catch (Exception e) {
            logger.debug("EC2-Convert-"+"Fail to register worker ?! ", e);
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            em.close();
        }
        return null;
    }
    
    private static WorkerStatus initOpenBookNewWorker() {
        EntityManager em = emFactory.createEntityManager();
        try {
            WorkerStatus ws = new WorkerStatus();
            ws.setStatus(0);

            em.getTransaction().begin();
            ws.setInstanceId("InitInstance-OPENBOOK-" + UUID.randomUUID().toString());
            ws.setThreadId(0);
            ws.setWorkerStartTime(new Date());
            em.persist(ws);

            em.getTransaction().commit();
            return ws;
        } catch (Exception e) {
            logger.debug("EC2-Convert-"+"Fail to register worker ?! ", e);
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            em.close();
        }
        return null;
    }

    private static String getInstanceIdFromResultStr(String resultStr) {
        String result = null;
        try {
            String[] split = resultStr.split(",");
            for (String childItem : split) {
                if (childItem.contains("InstanceId")) {
                    result = childItem.split(":")[2].trim();
                    return result;
                }
            }

        } catch (Exception e) {
            logger.error("Fail to decode InstanceId from Result" + resultStr, e);
        }
        return result;
    }

    private static int countIdleEC2() {
        int ret = 0;
        EntityManager em = emFactory.createEntityManager();
        try {
            ret = WorkerStatus.countAvailInstances(em);
        } catch (Exception e) {
            logger.debug("EC2-Convert-"+"Fail to update worker ?! ", e);
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            em.close();
        }

        return ret;
    }
    
    private static int countIdleOpenBookEC2() {
        int ret = 0;
        EntityManager em = emFactory.createEntityManager();
        try {
            ret = WorkerStatus.countOpenBookAvailInstances(em);
        } catch (Exception e) {
            logger.debug("EC2-Convert-"+"Fail to update worker ?! ", e);
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            em.close();
        }

        return ret;
    }

    private static int waitingJobs() {
        return ScheduleJobManager.countPendingJobsByRunner(RunMode.ExtEc2Mode.getValue());
    }
    
    private static int waitingOpenBookJobs() {
        return ScheduleJobManager.countPendingJobsByRunner(RunMode.OPENBOOK_EC2.getValue());
    }

    public static void cleanOldRecords(EntityManager postgres) {
        WorkerStatus.cleanOldRecords(postgres);

    }

    private static RequestSpotFleetRequest configRequestSpotFleetRequest(BlockDeviceMapping deviceMapping,
            IamInstanceProfileSpecification iamInstanceProfile , String spotInstanceType , String spotInstancePrice) throws UnsupportedEncodingException {
        RequestSpotFleetRequest requestSpotFleetRequest = new RequestSpotFleetRequest();
        GroupIdentifier gi = new GroupIdentifier();
        gi.setGroupId(SecurityGroupId);

        // spot spec for instanceType1 & subnet1a & 1c
        SpotFleetLaunchSpecification spotFleetLaunchSpecification1 = new SpotFleetLaunchSpecification();
        spotFleetLaunchSpecification1.setImageId(Ec2WorkerImgId);
        spotFleetLaunchSpecification1.withBlockDeviceMappings(deviceMapping);
        spotFleetLaunchSpecification1.withSecurityGroups(gi);
        spotFleetLaunchSpecification1.setIamInstanceProfile(iamInstanceProfile);
        spotFleetLaunchSpecification1.setSubnetId(subnetId_1a + ", " + subnetId_1c);
        spotFleetLaunchSpecification1.setInstanceType(spotInstanceType);
        spotFleetLaunchSpecification1.withUserData(Base64.encodeBase64String(WorkerScript.getBytes("UTF-8")));
        spotFleetLaunchSpecification1.setSpotPrice(spotInstancePrice);
        
        
        logger.debug("EC2-Convert-Ec2WorkerImgId"+Ec2WorkerImgId);
        logger.debug("EC2-Convert-deviceMapping"+deviceMapping);
        logger.debug("EC2-Convert-SecurityGroups"+gi);
        logger.debug("EC2-Convert-iamInstanceProfile"+iamInstanceProfile);
        logger.debug("EC2-Convert-"+subnetId_1a + ", " + subnetId_1c);
        logger.debug("EC2-Convert-spotInstanceType1"+spotInstanceType);
        logger.debug("EC2-Convert-spotInstancePrice1"+spotInstancePrice);

        // spot spec for instanceType2 & subnet1a & 1c
//        SpotFleetLaunchSpecification spotFleetLaunchSpecification2 = new SpotFleetLaunchSpecification();
//        spotFleetLaunchSpecification2.setImageId(Ec2WorkerImgId);
//        spotFleetLaunchSpecification2.withBlockDeviceMappings(deviceMapping);
//        spotFleetLaunchSpecification2.withSecurityGroups(gi);
//        spotFleetLaunchSpecification2.setIamInstanceProfile(iamInstanceProfile);
//        spotFleetLaunchSpecification2.setSubnetId(subnetId_1a + ", " + subnetId_1c);
//        spotFleetLaunchSpecification2.setInstanceType(spotInstanceType2);
//        spotFleetLaunchSpecification2.withUserData(Base64.encodeBase64String(WorkerScript.getBytes("UTF-8")));
//        spotFleetLaunchSpecification2.setSpotPrice(spotInstancePrice2);

        SpotFleetRequestConfigData spotFleetRequestConfigData = new SpotFleetRequestConfigData();
        List<SpotFleetLaunchSpecification> launchSpecificationsList = new ArrayList<SpotFleetLaunchSpecification>();
        launchSpecificationsList.add(spotFleetLaunchSpecification1);
//        launchSpecificationsList.add(spotFleetLaunchSpecification2);
        spotFleetRequestConfigData.setLaunchSpecifications(launchSpecificationsList);
        spotFleetRequestConfigData.setTargetCapacity(1);
        spotFleetRequestConfigData.setType(FleetType.Request);
        spotFleetRequestConfigData.setSpotPrice(spotInstancePrice);
        spotFleetRequestConfigData.setTerminateInstancesWithExpiration(false);
        spotFleetRequestConfigData.setIamFleetRole(iamFleetRole);

        requestSpotFleetRequest.setSpotFleetRequestConfig(spotFleetRequestConfigData);

        return requestSpotFleetRequest;
    }
    
    private static RequestSpotFleetRequest configOpenBookRequestSpotFleetRequest(BlockDeviceMapping deviceMapping,
            IamInstanceProfileSpecification iamInstanceProfile) throws UnsupportedEncodingException {
        RequestSpotFleetRequest requestSpotFleetRequest = new RequestSpotFleetRequest();
        GroupIdentifier gi = new GroupIdentifier();
        gi.setGroupId(SecurityGroupId);

        // spot spec for instanceType1 & subnet1a & 1c
        SpotFleetLaunchSpecification spotFleetLaunchSpecification1 = new SpotFleetLaunchSpecification();
        spotFleetLaunchSpecification1.setImageId(Ec2WorkerOpenBookImgId);
        spotFleetLaunchSpecification1.withBlockDeviceMappings(deviceMapping);
        spotFleetLaunchSpecification1.withSecurityGroups(gi);
        spotFleetLaunchSpecification1.setIamInstanceProfile(iamInstanceProfile);
        spotFleetLaunchSpecification1.setSubnetId(subnetId_1a + ", " + subnetId_1c);
        spotFleetLaunchSpecification1.setInstanceType(spotInstanceType1);
        spotFleetLaunchSpecification1.withUserData(Base64.encodeBase64String(WorkerOpenBookScript.getBytes("UTF-8")));
        spotFleetLaunchSpecification1.setSpotPrice(spotInstancePrice1);

        // spot spec for instanceType2 & subnet1a & 1c
//        SpotFleetLaunchSpecification spotFleetLaunchSpecification2 = new SpotFleetLaunchSpecification();
//        spotFleetLaunchSpecification2.setImageId(Ec2WorkerOpenBookImgId);
//        spotFleetLaunchSpecification2.withBlockDeviceMappings(deviceMapping);
//        spotFleetLaunchSpecification2.withSecurityGroups(gi);
//        spotFleetLaunchSpecification2.setIamInstanceProfile(iamInstanceProfile);
//        spotFleetLaunchSpecification2.setSubnetId(subnetId_1a + ", " + subnetId_1c);
//        spotFleetLaunchSpecification2.setInstanceType(spotInstanceType2);
//        spotFleetLaunchSpecification2.withUserData(Base64.encodeBase64String(WorkerOpenBookScript.getBytes("UTF-8")));
//        spotFleetLaunchSpecification2.setSpotPrice(spotInstancePrice2);

        SpotFleetRequestConfigData spotFleetRequestConfigData = new SpotFleetRequestConfigData();
        List<SpotFleetLaunchSpecification> launchSpecificationsList = new ArrayList<SpotFleetLaunchSpecification>();
        launchSpecificationsList.add(spotFleetLaunchSpecification1);
//        launchSpecificationsList.add(spotFleetLaunchSpecification2);
        spotFleetRequestConfigData.setLaunchSpecifications(launchSpecificationsList);
        spotFleetRequestConfigData.setTargetCapacity(1);
        spotFleetRequestConfigData.setType(FleetType.Request);
        spotFleetRequestConfigData.setSpotPrice(spotInstancePrice1);
        spotFleetRequestConfigData.setTerminateInstancesWithExpiration(false);
        spotFleetRequestConfigData.setIamFleetRole(iamFleetRole);

        requestSpotFleetRequest.setSpotFleetRequestConfig(spotFleetRequestConfigData);

        return requestSpotFleetRequest;
    }

}
