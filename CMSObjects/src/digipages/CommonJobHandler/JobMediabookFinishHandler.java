package digipages.CommonJobHandler;

import static org.boon.Boon.toJson;

import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BooksHandler.vod.MediabookResultNotifyHandler;
import digipages.common.CommonResponse;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.MediabookConvertMapping;
import model.ScheduleJob;

public class JobMediabookFinishHandler {
    private BlockingQueue<ScheduleJob> localJobQueue;
    private static List<ScheduleJob> jobs;
    private static DPLogger logger = DPLogger.getLogger(JobMediabookFinishHandler.class.getName());

    public JobMediabookFinishHandler(BlockingQueue<ScheduleJob> jobQueue) {
        this.localJobQueue = jobQueue;
    }

    public String finishMediabookJob(EntityManager postgres) throws ServerException {
        String jsonStr = "";

        // response
        CommonResponse records = new CommonResponse();
        records.setResult(true);
        jsonStr = toJson(records);

        try {

            // 處理影音書回檔-查詢可回檔並新增回檔排程
            @SuppressWarnings("unchecked")
            List<MediabookConvertMapping> mediabookConvertMappingList = postgres.createNativeQuery("select * from mediabook_convert_mapping mcm where status = 2", MediabookConvertMapping.class)
                    .getResultList();

            Gson gson = new GsonBuilder().create();
            for (MediabookConvertMapping mediabookConvertMapping : mediabookConvertMappingList) {

                long convertId = mediabookConvertMapping.getId();
//		        List<MediabookConvert> MediabookConvertList = postgres.createNativeQuery("select * from mediabook_convert mc where convert_id="+convertId +" and (status <> 9 or status <> -1) " , MediabookConvert.class).getResultList();

                // 查詢全部已處理可以回檔的資料, 9 處理完成,-1處理失敗 = 已處理過
                if (((Number) postgres.createNativeQuery("select count(1) from mediabook_convert where (status <> 9 or status <> -1) and convert_id=" + convertId).getSingleResult()).intValue() <= 0) {
                    // 開始回檔資料處理
                    postgres.getTransaction().begin();
                    mediabookConvertMapping.setStatus(9); // 註記轉檔已全部處理
                    postgres.persist(mediabookConvertMapping);
                    postgres.getTransaction().commit();

                    String data = gson.toJson(mediabookConvertMapping);
                    ScheduleJob tmpJob = new ScheduleJob();
                    tmpJob.setJobName(MediabookResultNotifyHandler.class.getName());
                    tmpJob.setJobGroup(String.valueOf(convertId));
                    tmpJob.setJobRunner(0); // for local
                    tmpJob.setPriority(500);
                    tmpJob.setRetryCnt(0);
                    tmpJob.setStatus(0);
                    tmpJob.setJobParam(data);
                    ScheduleJobManager.addJob(tmpJob);
                }

            }
//			logger.info("Job recover at " + CommonUtil.currentDateTime());
        } catch (Exception ex) {
            logger.error("Exception when recover Job.", ex);
            throw new ServerException("id_err_000", ex.getMessage());
        }

        return jsonStr;
    }
}