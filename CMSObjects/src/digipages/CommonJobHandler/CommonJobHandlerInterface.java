package digipages.CommonJobHandler;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.amazonaws.services.lambda.runtime.LambdaLogger;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

/**
 * Each JobHandler must defined on servlet startup. and set Logger. and
 * implements must have below code * public static CommonJobHandlerInterface
 * getInstance() { return myInstance; }
 *
 * @author Eric.CL.Chou
 *
 */
public interface CommonJobHandlerInterface {

	/**
	 * the main loop process here.
	 * 
	 * @param job
	 * @param logger
	 * @throws ServerException
	 * @throws InterruptedException
	 */
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException;

	/**
	 * optional, for db access, local/direct runner only.
	 * 
	 * @param em
	 */
	public void setEntityManagerFactory(EntityManagerFactory emf);

	/**
	 * from now, estimate finish Time
	 * 
	 * @param runMode
	 * @return
	 */
	public static Date calcShouldFinishTime(RunMode runMode) {
		switch (runMode) {
		case DirectRunMode:
		case LocalMode:
		case ExtEc2Mode:
			return CommonUtil.nextNMinutes(41);
		case LambdaMode:
		default:
			return CommonUtil.nextNMinutes(6);
		}
	}
	
	/**
	 * children must implement below methods.
	 * public static RunMode getInitRunMode(ScheduleJob job);
	 * public static RunMode getRetryRunMode(RunMode prevRunMode)
	 */


	


}
