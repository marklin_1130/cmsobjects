package digipages.CommonJobHandler.Runner;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import com.amazonaws.util.EC2MetadataUtils;
import com.google.gson.Gson;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.JobStatusReporter;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.WorkerStatusClient;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;
import model.WorkerStatus;

/**
 * run mainly at Independent EC2 Server.
 * usually not doing db operations.
 * @author Eric.CL.Chou
 *
 */
public class Ec2Runner extends BaseRunner implements Runnable{
//	private static final int MAXIDLESEC = 60; //(60 sec)
	private static final int MAXIDLESEC = 300; //(5 min)
	private static List<Thread> threads = new ArrayList<>();
	private static List<Ec2Runner> runners = new ArrayList<>();

	// common item which store the last run time
	private static long lastRunTime = System.currentTimeMillis();
	
	private static Map <String, CommonJobHandlerInterface> handlers = new HashMap<>();
	private static DPLogger logger = DPLogger.getLogger(Ec2Runner.class.getName());
	private static boolean inShuttingDown=false;
	private static Properties config=new Properties();
	private String instanceId;
	private int threadId;
	private static Gson gson=new Gson();
	
	/**
	 * handlers must go here.
	 */
	public static void registerHandlers() {
//		registerHandler(ChildGenericTestHandler.getInstance());
	}


	/**
	 * initialize, tell server I am ready.
	 */
	public Ec2Runner()
	{
		initAPIURLs();
		instanceId =UUID.randomUUID().toString();
		try {
			// for EC2/Lambda.
			instanceId = EC2MetadataUtils.getInstanceId();
		}catch (Exception ex)
		{
			logger.debug("Getting InstanceId from aws fail. ",ex);
		}
		if (instanceId==null)
			instanceId =UUID.randomUUID().toString();
		threadId = runners.size();
		logger.info("Runner "+instanceId +"#" + threadId + " started");
		reportStatusNew(threadId);
		
		//寫入開機資訊
		// >>/var/log/si_info
		//ENV,TAG,INSTANCEID
		//PROD,OPENBOOK,i-07f1b0b9e6e0263f3
        try {
        	String[] cmdline = { "sh", "-c", "echo \""+ (config.get("env")+" convert "+instanceId)+"\" > /var/log/si_info"}; 
        	Process ps = Runtime.getRuntime().exec(cmdline);
        	ps.waitFor();
            BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            String result = sb.toString();
            System.out.println(result);
            
            
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
		Runnable run = new Runnable() {
			public void run() {
				try {
					String cmdGetType = "curl http://169.254.169.254/latest/meta-data/instance-type";
					Process psGetType = Runtime.getRuntime().exec(cmdGetType);
					psGetType.waitFor();
					BufferedReader brGetType = new BufferedReader(new InputStreamReader(psGetType.getInputStream()));
					StringBuffer sbGetType = new StringBuffer();
					String lineGetType;
					while ((lineGetType = brGetType.readLine()) != null) {
						sbGetType.append(lineGetType).append("\n");
					}
					String instanceType = sbGetType.toString();
					System.out.println(instanceType);
					int timeCount = 0;
					while (true) {
						try {
							String cmd = "aws cloudwatch put-metric-data --metric-name SpotInstanceStatus --namespace SpotInstance-Status --unit Count --value 1 --dimensions ENV="+config.get("env")+",Type=ebook-convert,InstanceType="+instanceType+" --region ap-northeast-1";
							System.out.println("run command:"+cmd);
							Process ps = Runtime.getRuntime().exec(cmd);
							ps.waitFor();
							BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream()));
							StringBuffer sb = new StringBuffer();
							String line;
							while ((line = br.readLine()) != null) {
								sb.append(line).append("\n");
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						Thread.sleep(60000);
						
						timeCount = timeCount++;
						
						if(timeCount>=60) {
							logger.info("Shutting down Ec2.");
							System.exit(0);
						}
					}
				} catch (Exception e) {
					System.out.println(" interrupted");
				}
			}
		};
		new Thread(run).start();
	}



	private void initAPIURLs() {

		InputStream inputStream=null;
		try {

			String filePath = "config.properties";
	
			inputStream = new FileInputStream(filePath);
			
		} catch (Exception ex)
		{
			logger.debug("fail to get config from file",ex);
		}
		
		//not found, try load from jar/war resource
		if (null == inputStream) {
			inputStream = Ec2Runner.class.getResourceAsStream("/config.properties");
		}
		try {
			config.load(inputStream);
		} catch (Exception e) {
			logger.debug("fail to get config from jar embadded info.",e);
		}
		logger.info("Config loaded:" + config);
		WorkerStatusClient.setBaseURI(config.getProperty("WorkerStatusURL"));
		JobStatusReporter.setBaseURI(config.getProperty("ScheduleJobManagerURL"));
	}



	private void reportStatusNew(int threadId) {
		List <WorkerStatus> statuses = new ArrayList<>();
		WorkerStatus ws = new WorkerStatus();
		ws.setInstanceId(instanceId);
		ws.setStatus(0);
		ws.setThreadId(threadId);
		ws.setWorkerStartTime(new Date());
		statuses.add(ws);
		try {
			WorkerStatusClient.addStatus(statuses);
		} catch (IOException e) {
			logger.error("Fail to add Status:", e);
		}
		
	}



	/**
	 * add handler
	 * @param handler
	 */
	public static void registerHandler(CommonJobHandlerInterface handler){
		handlers.put(handler.getClass().getName(), handler);
	}
	
	/**
	 * remove handler
	 * @param handler
	 */
	public static void removeHandler(CommonJobHandlerInterface handler)
	{
		handlers.remove(handler);
	}
	
	/**
	 * main fetch run
	 * @throws InterruptedException
	 */
	public void fetchAndRun() throws InterruptedException
	{
		ScheduleJob scheduleJob=null;
		try {
			scheduleJob=getJobFromAPI();
			
			// no job, =idle
			if (scheduleJob==null){
				reportWorkerStatusIdle(null);
				return;
			}
			String jobName = scheduleJob.getJobName();
			sendWorkerStatusRunning(scheduleJob);
			CommonJobHandlerInterface handler = (CommonJobHandlerInterface)Class.forName(jobName).newInstance();
//			CommonJobHandlerInterface handler = handlers.get(scheduleJob.getJobName());
			logger.debug("ScheduleJob fetched: " + gson.toJson(scheduleJob));
			
			// no job, idle
			if (handler==null)
			{
				reportWorkerStatusIdle(null);
				return;
			}
			
			reportJobStarted(scheduleJob);
			logger.debug("init handler "+jobName+" with config:" + config);
			
			executeAndUpdate(handler,scheduleJob);
			
		}catch (JobRetryException ex){
			logger.info("Job Need Retry: " + scheduleJob.getJobName());
			scheduleJob.setStatus(2); // retry
		}catch (Ec2ResourceException e)
		{
			logger.error("Ec2Resource is exhausted, need to shutdown and restart in other worker.",e);
			scheduleJob.setStatus(2); // retry
			shutdown();
		}
		catch (ServerException ex){
			
			if ("id_err_314".equals(ex.getError_code())){
				logger.info("Job Need Retry: " + scheduleJob.getJobName());
				scheduleJob.setStatus(2); // retry
			}else{
				scheduleJob.setStatus(-1);
				logger.error("ServerException",ex);
			}
			scheduleJob.setErrMessage(ex.toJsonStr());
		} catch (Exception e) {
			logger.error("Error Executing job:" + gson.toJson(scheduleJob),e);
		}finally
		{
			ScheduleJobManager.updateJob(scheduleJob);
			// finished, status=idle.
			reportWorkerStatusIdle(scheduleJob);
		}
	}


	private void executeAndUpdate( CommonJobHandlerInterface handler, ScheduleJob scheduleJob) throws ServerException, JobRetryException {
		String result=handler.mainHandler(config,scheduleJob);
		Ec2Runner.lastRunTime=System.currentTimeMillis();
		finishJob(scheduleJob, result);

		
	}


	private void sendWorkerStatusRunning(ScheduleJob job) {
		sendStatus(2,job);
		
	}
	
	
	private void sendStatus(int status, ScheduleJob job)
	{
		List <WorkerStatus> statuses = new ArrayList<>();
		WorkerStatus ws = new WorkerStatus();
		ws.setInstanceId(instanceId);
		ws.setStatus(status);
		ws.setThreadId(threadId);
		Date now = new Date();
		switch (status)
		{
		case 0:
			ws.setWorkerStartTime(now);
			break;
		case 1:
			updateTimeByJob(ws,job);
			break;
		case 2:
			ws.setJobInfo(job.getJobParam(Object.class));
			ws.setJobStartTime(now);
			break;
		default:
		}
		statuses.add(ws);
		try {
			WorkerStatusClient.updateStatus(statuses);
		} catch (IOException e) {
			logger.error(e);;
//			e.printStackTrace();
		}
	}
	
	

	private void updateTimeByJob(WorkerStatus ws, ScheduleJob job) {
		Date now=new Date();
		if (job==null)
		{
			ws.setWorkerStartTime(now);
		}else
		{
			ws.setJobFinishTime(now);
			ws.setJobInfo(job.getJobParam(Object.class));
		}
		
	}


	private static void finishJob(ScheduleJob scheduleJob,String result) {
		scheduleJob.setFinishTime(new Date());
		scheduleJob.setStatus(9);
		scheduleJob.setResultStr(result);
		JobStatusReporter.updateJobRetry(scheduleJob);
		Ec2Runner.lastRunTime = System.currentTimeMillis();
	}

	private void reportWorkerStatusIdle(ScheduleJob job) {
		
		long totalIdleTime = System.currentTimeMillis()-lastRunTime;
		if (totalIdleTime > MAXIDLESEC*1000)
		{
			sendStatusShutdown();
			shutdown();
			return;
		}
		sendStatusIdle(job);
	}

	private void sendStatusIdle(ScheduleJob job) {
		sendStatus(1,job);
		
	}

	private void sendStatusShutdown() {
		sendStatus(-1,null);
		
	}

	private static void shutdown() {
//		try {
//			File f = new File("/tmp/shutdownMe");
//			f.createNewFile();
//		}catch (Exception e)
//		{
//			logger.error(e);
//		}
		inShuttingDown = true;
	}

	private static ScheduleJob getJobFromAPI() {
		
		String jobKey = UUID.randomUUID().toString();
		List<ScheduleJob> jobs= JobStatusReporter.getJobsRetry(jobKey, 2, 1);
		if (null==jobs || jobs.isEmpty())
			return null;
		return jobs.get(0);
	}

	@Override
	public void run() {
		while (!inShuttingDown)
		{
			logger.debug("fetchAndRun");
			try {
				fetchAndRun();
				Thread.sleep(1000); // every 10 sec check.
			}catch (Exception e)
			{
				logger.error("InLoop",e);
			}
		}
	}
	


	public static void main(String args[])
	{
		
		registerHandlers();
//		int coreCnt = Runtime.getRuntime().availableProcessors();
//		Ec2Runner.initWithCores(coreCnt);
		Ec2Runner.initWithCores(1);
		
		// every 1 second ask jobs.
		while (!Ec2Runner.inShuttingDown)
		{
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		shutDownEc2();
	}






	/**
	 * shutdown by shell script (exit main)
	 */
	private static void shutDownEc2() {
		// send StatusShutdown ==> if any new job, must start new Ec2
		for (Ec2Runner runner:runners)
		{
			runner.sendStatusShutdown();
		}
		
		// max wait 45 min and force shutdown.
		for (Thread thread:threads)
		{
			try {
				thread.join(45*60*1000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		handlers.clear();
		logger.info("Shutting down Ec2.");
		System.exit(0);
	}



	private static void initWithCores(int coreCnt) {
		logger.info("Start with cores:" + coreCnt);
		for (int i=0; i< coreCnt;i++)
		{
			
			Ec2Runner tmpRunner= new Ec2Runner();
			runners.add(tmpRunner);
			Thread t = new Thread(tmpRunner);
			t.setDaemon(true);
			threads.add(t);
			t.start();
			
		}
	}
	
	public Properties getConfig()
	{
		return config;
	}

	
}
