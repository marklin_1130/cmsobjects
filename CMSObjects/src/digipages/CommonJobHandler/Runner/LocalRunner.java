package digipages.CommonJobHandler.Runner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import digipages.BooksHandler.RescanDRMHandler;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;

import model.Member;
import model.ScheduleJob;

public class LocalRunner extends BaseRunner implements  Runnable {
	private static DPLogger logger = DPLogger.getLogger(LocalRunner.class.getName());
	private static List<CommonJobHandlerInterface> handlers = new ArrayList<>();
	private static BlockingQueue<ScheduleJob> jobQueue;
	private static boolean interrupted = false;
	private EntityManagerFactory emf;
	private static Properties config = new Properties();

	public LocalRunner(BlockingQueue<ScheduleJob> jobQueue, EntityManagerFactory emf, Properties conf) {
		LocalRunner.jobQueue = jobQueue;
		this.emf = emf;
		this.config=conf;
	}

	public static void notifyRunner(ScheduleJob job) {
		jobQueue.add(job);
	}

	public static void registerHandler(CommonJobHandlerInterface handler) {
		handlers.add(handler);
	}

	public static void removeHandler(CommonJobHandlerInterface handler) {
		handlers.remove(handler);
	}

	public static List<ScheduleJob> queryLocalJob() {
		List<ScheduleJob> jobs = ScheduleJobManager.
				takeJobsByRunner(UUID.randomUUID().toString(), 
						RunMode.LocalMode.getValue(), 5);

		int bookUpgraderJobs = ScheduleJobManager.countBookUpgraderJobsByRunner();
		
		if(bookUpgraderJobs <= 50) {
			jobs.addAll(ScheduleJobManager.
			takeBookUpgraderJobsByRunner(UUID.randomUUID().toString(), 
							RunMode.LocalMode.getValue(), 1));
		}
		
		if (jobs.size() > 0) {
			ScheduleJob j = jobs.get(0);
			logger.debug("job queryed, param=" + j.getJobParam(Object.class) + ", status: " + j.getStatus());
		}

		return jobs;
	}

	public static void NotifyRunner() {
		jobQueue.addAll(queryLocalJob());
	}

	// main
	public void fetchJobAndRun() throws InterruptedException {
		ScheduleJob scheduleJob = null;

		try {
			scheduleJob = jobQueue.take();

			// skip empty items.
			if (scheduleJob == null)
				return;

			if (scheduleJob.getJobParam(Object.class) == null || scheduleJob.getJobName() == null) {

				return;
			}

			// datetime
			logger.debug("Consumed: " + scheduleJob + " type: " + scheduleJob.getClass().getTypeName() + scheduleJob.getId());
			logger.debug("JobName: " + scheduleJob.getJobName());

			printDebug(scheduleJob);
			
			reportJobStarted(scheduleJob);

			String jobName=scheduleJob.getJobName();
			CommonJobHandlerInterface handler = (CommonJobHandlerInterface)Class.forName(jobName).newInstance();
			handler.setEntityManagerFactory(emf);
			String result=handler.mainHandler(config, scheduleJob);
			scheduleJob.setResultStr(result);
			ScheduleJobManager.finishJob(scheduleJob);

			// look for more jobs
			NotifyRunner();
		}catch (JobRetryException jex) {
			
		    if(jex.getMessage().equalsIgnoreCase("pending")) {
		        logger.info("Job is Pending: " + scheduleJob.getJobName()+scheduleJob.getJobGroup());
		        if (scheduleJob != null) {
                    scheduleJob.setStatus(3); // mediabook retry
                    scheduleJob.setRetryCnt(0);
                    scheduleJob.setErrMessage(jex.getMessage());
                    //增加間隔時間 
//                    scheduleJob.setShouldStartTime(CommonUtil.nextNMinutes(30));
                    ScheduleJobManager.updateJob(scheduleJob);
                }
		    }else {
    			logger.info("Job Need Retry: " + scheduleJob.getJobName());
               
    			logger.error("JobRetryException", jex);
    			if (scheduleJob != null) {
    				scheduleJob.setStatus(3); // mediabook retry
    				scheduleJob.setErrMessage(jex.getMessage());
    				ScheduleJobManager.updateJob(scheduleJob);
    			}
		    }
		} catch (ServerException ex) {
			logger.error("ServerException", ex);

			if (scheduleJob != null) {
				scheduleJob.setStatus(-1);
				scheduleJob.setErrMessage(ex.toJsonStr());
				ScheduleJobManager.updateJob(scheduleJob);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} catch (Exception e) {
			logger.error("Unexception Error",e);
			if (scheduleJob != null) {
				scheduleJob.setStatus(-1);
				scheduleJob.setErrMessage(e.getMessage());
				ScheduleJobManager.updateJob(scheduleJob);
			}
		}
	}

	private void printDebug(ScheduleJob scheduleJob) {
		logger.debug("Local Runner: Job Consumed: " + scheduleJob.getJobName()
				+ " type: " + scheduleJob.getClass().getTypeName()
				+ " Parameter: " + scheduleJob.getJobParam(Object.class).toString());
	}

	@Override
	public void run() {
		while (!interrupted) {
			try {
				fetchJobAndRun();
			} catch (Exception ex) {
				if (ex instanceof java.lang.InterruptedException && interrupted) {

				} else {
					logger.error("Thread run Error", ex);
				}
			}
		}
	}

	public void finish() {
		interrupted = true;
	}
}