package digipages.CommonJobHandler.Runner;

import digipages.exceptions.ServerException;

/**
 * mainly used to detect ec2 continuous run caused too many open files error
 * @author Eric.CL.Chou
 *
 */
public class Ec2ResourceException extends ServerException {

	public Ec2ResourceException(String error_code, String errorParameter) {
		super(error_code, errorParameter);
	
	}

}
