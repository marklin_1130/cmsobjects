package digipages.CommonJobHandler.Runner;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.google.gson.Gson;

import digipages.snps.SNPSDeviceUpdate;
import digipages.snps.SNPSServiceImpl;
import model.MemberDevice;

public class SNPSDataTransRunner {

    private static EntityManagerFactory emf;
    private static EntityManager em;
    private static String snpsaccount = "ebookreader";
    private static String snpspw = "3x3fmf8s";
    // private String snpsDeviceUpdate =
    // "http://10.38.8.61:8080/SNPSApi/services/rs/internal/device/update";
    // private String snpsBoardcast =
    // "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/boardcast";
    // private String snpsPublish =
    // "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/customer";
    // private String snpsSimple =
    // "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/simple";
    // private String snpsAppKey =
    // "http://10.38.8.61:8080/SNPSApi/services/rs/internal/publish/customer";
    private static String snpsDeviceUpdate = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/device/update";
    private static String snpsBoardcast = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/boardcast";
    private static String snpsBoardcastPayload = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/boardcast/payload";
    private static String snpsPublish = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/customer";
    private static String snpsSimple = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/simple";
    private static String snpsAppKey = "https://snps.books.com.tw:4443/SNPSApi/services/rs/internal/publish/customer";

    public static void main(String args[]) {

        emf = Persistence.createEntityManagerFactory("CMSObjects");

        try {

            
            // ios: c4cccfdd272d42cdb418049e2ad8f268
            // andorid: e700570b4d924f43bbfffb03fe6b8a10
            em = emf.createEntityManager();
            List<MemberDevice> memberDevices = null;
            if(args[0].equalsIgnoreCase("10")) {
                memberDevices = em.createNamedQuery("MemberDevice.findByToken", MemberDevice.class).setMaxResults(10).getResultList();    
            }else {
                memberDevices = em.createNamedQuery("MemberDevice.findByToken", MemberDevice.class).getResultList();
            }
            
            SNPSDeviceUpdate aSNPSDeviceUpdate = new SNPSDeviceUpdate();
            System.out.println("data loaded");
            for (MemberDevice memberDevice : memberDevices) {

                if (memberDevice.getStatus() == 2) {
                    SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
                    snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
                    if ("ios".equalsIgnoreCase(memberDevice.getOsType())) {
                        aSNPSDeviceUpdate.setAppKey("427c8bc041164a2295e3b11dd58acc81");
                    } else {
                        aSNPSDeviceUpdate.setAppKey("ddafbffd3e5445bc86cd1a5e46475481");
                    }
                    aSNPSDeviceUpdate.setCid(memberDevice.getMember().getBmemberId());
                    aSNPSDeviceUpdate.setDeviceToken(memberDevice.getDeviceToken());
                    aSNPSDeviceUpdate.setEnable("1");
                    aSNPSDeviceUpdate.setUserCanceled("0");
                    snpsServiceImpl.DeviceUpdate(aSNPSDeviceUpdate);
                     System.out.println(new Gson().toJson(aSNPSDeviceUpdate));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        emf.close();

    }
}
