package digipages.CommonJobHandler.Runner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import digipages.BooksHandler.RescanDRMHandler;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;
import model.Member;
import model.ScheduleJob;

public class LocalJobConsumer implements Runnable {
	
	private static DPLogger logger = DPLogger.getLogger(LocalJobConsumer.class.getName());
	
	private EntityManagerFactory emf;

	protected BlockingQueue<ScheduleJob> bq = null;

	public LocalJobConsumer(BlockingQueue<ScheduleJob> queue, EntityManagerFactory emf) {
		this.emf = emf;
		this.bq = queue;
	}

	@Override
	public void run() {
		while (true) {
			try {
				ScheduleJob scheduleJob = bq.take();

				if (null != scheduleJob) {
					// datetime
					String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
					String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

					logger.info("Consumed: " + scheduleJob + " type: " + scheduleJob.getClass().getTypeName());

					if (scheduleJob.getJobParam(Object.class) == null || scheduleJob.getJobName() == null) {
						// empty pruduced object
						logger.info("Sample Produced Job Consumed.");
						continue;
					}

					logger.info("JobName: " + scheduleJob.getJobName());

					switch (scheduleJob.getJobName()) {
						case RescanDRMHandler.HANDLERNAME:
							EntityManager em = emf.createEntityManager();
							ScheduleJobParam jobParams = scheduleJob.getJobParam(ScheduleJobParam.class);
							Member member = em.find(Member.class, jobParams.getMember_id());

							String item_id = "";
							ArrayList<String> item_ids = new ArrayList<String>();

							if (null != jobParams.getItem_id()) {
								item_id = jobParams.getItem_id();
							}

							if (null != jobParams.getItem_ids()) {
								item_ids = jobParams.getItem_ids();
							}

							logger.info("[LocalJobConsumer] item_id: " + item_id);
							logger.info("[LocalJobConsumer] item_ids: " + item_ids);

							logger.info("Rescan member_id: " + member.getId() + " drms, start at " + dateTimeString);

//							RescanDRMHandler handler = new RescanDRMHandler();

//							handler.setScheduleJob((ScheduleJob) scheduleJob);
//							handler.setItemId(item_id);
//							handler.setItemIds(item_ids);
//							handler.rescanDRM(em, member);

							em.close();

							break;
						default:
							// what to do ?
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private Map<String, Object> createProties(Properties properties) {
		Map<String, Object> ret = new HashMap<>();

		ret.put(PersistenceUnitProperties.JDBC_DRIVER, properties.get("jdbcDriver"));
		ret.put(PersistenceUnitProperties.JDBC_URL, properties.getProperty("jdbcURL"));
		ret.put(PersistenceUnitProperties.JDBC_USER, properties.getProperty("jdbcUser"));
		ret.put(PersistenceUnitProperties.JDBC_PASSWORD, properties.getProperty("jdbcPassword"));
		// below default setting
		ret.put(PersistenceUnitProperties.WEAVING, properties.getProperty("true"));
		ret.put(PersistenceUnitProperties.WEAVING_CHANGE_TRACKING, "true");

		return ret;
	}
}