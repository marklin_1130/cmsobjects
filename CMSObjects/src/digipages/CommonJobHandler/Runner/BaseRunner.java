package digipages.CommonJobHandler.Runner;

import java.util.Date;

import digipages.CommonJobHandler.ScheduleJobManager;
import model.ScheduleJob;

public class BaseRunner {

	
	public static void reportJobStarted(ScheduleJob scheduleJob) {
		scheduleJob.setStatus(1);
		scheduleJob.setLastUpdated(new Date());
		ScheduleJobManager.updateJob(scheduleJob);
	}

}
