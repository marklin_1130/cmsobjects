package digipages.CommonJobHandler;

import static org.boon.Boon.toJson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.InstanceMonitoring;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.MonitorInstancesRequest;
import com.amazonaws.services.ec2.model.MonitorInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.google.gson.Gson;

import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.vod.MediaBookRequestApiData;
import digipages.common.CommonResponse;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.BaseBookInfo;
import model.Item;
import model.ScheduleJob;
import model.WorkerStatus;

public class JobTransferFileCheckHandler {
    private BlockingQueue<ScheduleJob> localJobQueue;
    private static List<ScheduleJob> jobs;
    private static DPLogger logger = DPLogger.getLogger(JobTransferFileCheckHandler.class.getName());
    private static final String CLASSNAME = "JobTransferFileCheckHandler";

    public JobTransferFileCheckHandler(BlockingQueue<ScheduleJob> jobQueue) {
        this.localJobQueue = jobQueue;
    }

    public String jobTransferFileCheck(String env, EntityManager postgres) throws ServerException {
    	logger.info(CLASSNAME, "jobTransferFileCheck", "jobTransferFileCheck start at " + CommonUtil.currentDateTime());
    	
        String jsonStr = "";
        String instanceId = "jobTransferFileCheck";
        Boolean isNew = false; //是否新增的WorkerStatus
        
        // response
        CommonResponse records = new CommonResponse();
        records.setResult(true);
        jsonStr = toJson(records);

        try {
        	Random random = new Random();
            Long delay = (long) ((random.nextInt(540) + 60) * 1000); //Random 1~10min
        	
            try {
                Thread.sleep(delay);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        	
        	//查詢是否正在執行的thread
        	WorkerStatus ws = WorkerStatus.findWorkerStatusByInstanceId(postgres, instanceId);
        	//若WorkerStatus沒有資料，則寫入新的一筆
        	Date now = new Date();
        	Date dateZone = getCurrentDateZone(now);
        	
        	if(ws == null) {
        		postgres.getTransaction().begin();
        		ws = new WorkerStatus();
        		ws.setInstanceId(instanceId);
        		ws.setWorkerStartTime(dateZone);
        		ws.setJobStartTime(now);
        		ws.setStatus(2);
        		ws.setUdt(now);
        		
        		postgres.persist(ws);
                postgres.getTransaction().commit();
                
                isNew = true;
        	}
        	
        	//因為是30分鐘執行一次，所以驗證是否為該區間資料
        	if(isNew) {
        		//若為新增的WorkerStatus，則表示沒有別的thread在執行
        		checkTransferFile(postgres, ws, env);
        		
        	}else if(!isNew && ws.getWorkerStartTime().compareTo(dateZone) != 0){
        		//不是該區間資料則表示沒有thread在執行，更新執行時間區間，
        		postgres.getTransaction().begin();
        		ws.setWorkerStartTime(dateZone);
        		ws.setJobStartTime(now);
        		ws.setStatus(2);
        		ws.setUdt(now);
        		
        		postgres.persist(ws);
                postgres.getTransaction().commit();
                
                checkTransferFile(postgres, ws, env);
        	}
        	
        } catch (Exception ex) {
            logger.error(CLASSNAME, "jobTransferFileCheck", "Exception when recover Job.", ex);
            throw new ServerException("id_err_000", ex.getMessage());
        }

        return jsonStr;
    }
    
    private void checkTransferFile(EntityManager postgres, WorkerStatus ws, String env) {
    	logger.info(CLASSNAME, "checkTransferFile", "Job checkTransferFile at " + CommonUtil.currentDateTime());
    	//TODO 轉檔排程狀況確認
    	
    	//1.縮圖失敗偵測
    	checkThumbnailFail(postgres, env);
		
		//2.偵測機器是否有多開
    	checkOpenAdditionalMachine(postgres, env);
    	
    	//3.偵測AWS雲端機器是否有關閉
    	checkCloseAWSMachine(postgres, env);
    	
    	//4.異常轉檔(確認Job是否執行過久仍未結束)
    	checkJobDone(postgres, env);
    	
    	//5.偵測機器是否開過久(一小時)
    	checkMachineOverTime(postgres, env);
    	
    	//排程處理結束
    	postgres.getTransaction().begin();
		ws.setJobFinishTime(new Date());
		ws.setStatus(-1);
		ws.setUdt(new Date());
		
		postgres.persist(ws);
        postgres.getTransaction().commit();
        logger.info(CLASSNAME, "checkTransferFile", "Job checkTransferFile end at " + CommonUtil.currentDateTime());
    }
    
    private void checkThumbnailFail(EntityManager postgres, String env) {
    	TypedQuery<ScheduleJob> queryScheduleJob = postgres.createNamedQuery("ScheduleJob.listByJobNameAndStatusAndRetry", ScheduleJob.class);
    	queryScheduleJob.setParameter("jobName", "FixedlayoutThumbnail");
    	queryScheduleJob.setParameter("status", -1);
    	queryScheduleJob.setParameter("retryCnt", 40);
		List<ScheduleJob> sjList = queryScheduleJob.getResultList();
		if (!sjList.isEmpty()) {
			postgres.getTransaction().begin();
			for(ScheduleJob scheduleJob : sjList) {
				String itemId = getItemId(postgres, scheduleJob);
				//將失敗訊息傳至slack
				String message = "[" + env + "] 縮圖失敗 - {" + scheduleJob.getJobGroup() + "} {" + itemId + "} - " + CommonUtil.getDateToString(new Date());
				sendMessageToSlack(message);
				
				scheduleJob.setRetryCnt(99);
				postgres.persist(scheduleJob);
			}
			
			postgres.getTransaction().commit();
		}
    }
    
    private void checkOpenAdditionalMachine(EntityManager postgres, String env) {
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date today = cal.getTime();
    	
		//查詢目前開多少機器
		List<WorkerStatus> workers = postgres.createQuery("select ws from WorkerStatus ws where ws.status <>-1 and ws.instanceId != 'jobTransferFileCheck' and ws.udt >= :today ", WorkerStatus.class)
    			.setParameter("today", today)
    			.getResultList();
    	
		//查詢目前有多少需要開機器的排程
    	List<ScheduleJob> scheduleJobs = postgres.createQuery("select sj from ScheduleJob sj where sj.jobRunner =2 and sj.status <> 9 and sj.status<>-1 and sj.lastUpdated >= :today ", ScheduleJob.class)
    			.setParameter("today", today)
    			.getResultList();
    	
    	if(scheduleJobs != null && workers != null) {
    		//如果開的機器數量大於所需數量則通知slack
    		if(workers.size() > scheduleJobs.size()) {
    			List<String> additionalList = new ArrayList<String>();
    			Gson gson = new Gson();
    			for(WorkerStatus ws : workers) {
    				additionalList.add(ws.getInstanceId());
    				
					String jobInfo = gson.toJson(ws.getJobInfo());
    				DataMessage wsMessage = gson.fromJson(jobInfo, DataMessage.class);
    				String bookUniId = wsMessage.getBookUniId();
    				
    				if(bookUniId != null) {
    					for(ScheduleJob job : scheduleJobs) {
        					DataMessage jobMessage= job.getJobParam(DataMessage.class);
        					if(bookUniId.equals(jobMessage.getBookUniId())) {
        						additionalList.remove(ws.getInstanceId());
        						break;
        					}
        				}
    				}
        				
    			}
    			
    			//沒有被移除掉的
    			if(additionalList.size() > 0) {
    				String instanceIds = getInstanceIds(additionalList);
    				String message = "[" + env + "] (DB)機器未關 - {" + instanceIds + "} - " + CommonUtil.getDateToString(new Date());
    				sendMessageToSlack(message);
    			}
    			
    		}
    	}
    }
    
    private void checkCloseAWSMachine(EntityManager postgres, String env) {
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) -3);
		Date threeDaysAgo = cal.getTime();
		
		Calendar cal2 = Calendar.getInstance();
		cal2.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE) -15);
		Date tenMinsAgo = cal2.getTime();
    	
		//查詢三天內有使用了多少機器，並且DB裡資料顯示已關閉(因雲端AWS關機可能需要15分鐘，所以實際區間為 3天前~15分鐘前 )
		List<WorkerStatus> workers = postgres.createQuery("select ws from WorkerStatus ws where ws.status =-1 and ws.instanceId != 'jobTransferFileCheck' and ws.udt between :threeDaysAgo and :tenMinsAgo  ", WorkerStatus.class)
				.setParameter("threeDaysAgo", threeDaysAgo)
				.setParameter("tenMinsAgo", tenMinsAgo)
    			.getResultList();
    	
    	if(workers != null) {
    		List<WorkerStatus> workerStatusList = new ArrayList<WorkerStatus>();
    		AmazonEC2 amazonEC2Client = AmazonEC2ClientBuilder.defaultClient();
    		
    		//本機測試用，記得上板前移除...............
////    		System.setProperty("com.amazonaws.sdk.disableEc2Metadata", AWS_EC2_METADATA_DISABLED_SYSTEM_PROPERTY);
//    		System.setProperty("com.amazonaws.sdk.disableEc2Metadata", "true");
//    		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAI47WKLD65AUYRWXA", "VWU4HmYz6necoGFqEKwPNKLROPlVpZBfxXKBj1VM");
//    		AmazonEC2 amazonEC2Client = AmazonEC2ClientBuilder.standard().withRegion("ap-northeast-1")
//    				                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
//    				                        .build();
    		//本機測試用，記得上板前移除..................
    		
    		//確認雲端AWS機器狀態
    		for(WorkerStatus workerStatus : workers) {
    			try {
            		MonitorInstancesRequest monitorInstancesRequest = new MonitorInstancesRequest().withInstanceIds(workerStatus.getInstanceId());
            		
            		MonitorInstancesResult monitorInstancesResult = amazonEC2Client.monitorInstances(monitorInstancesRequest);
            		List<InstanceMonitoring> instanceMonitoringList = monitorInstancesResult.getInstanceMonitorings();
            		for(InstanceMonitoring instanceMonitoring : instanceMonitoringList) {
            			//instanceMonitoring.getMonitoring().getState()  //enabled,pending
//            			logger.info(CLASSNAME, "checkCloseAWSMachine", "雲端AWS機器(" +instanceMonitoring.getInstanceId() + ")未關，狀態為" + instanceMonitoring.getMonitoring().getState());
            			workerStatusList.add(workerStatus);
            		}
            		
        		}catch(Exception e) {
        			//雲端AWS機器已關，故有錯誤訊息如下(無需處理)
        			//The instance ID 'i-051d509846628c4f4' does not exist (Service: AmazonEC2; Status Code: 400; Error Code: InvalidInstanceID.NotFound; Request ID: 4af5b316-7637-42c8-b79b-ccad769e88cb; Proxy: null)
        			logger.info(CLASSNAME, "checkCloseAWSMachine", "雲端AWS機器已關：" + e.getMessage());
        		}
    		}
    		
    		//若有雲端AWS有尚未關機的機器，則通知slack
    		if(workerStatusList.size() > 0) {
    			for(WorkerStatus ws : workerStatusList) {
    				String message = "[" + env + "] (AWS)機器未關 - {" + ws.getInstanceId() + "} - " + CommonUtil.getDateToString(ws.getWorkerStartTime());
    				sendMessageToSlack(message);
    				
    				//呼叫雲端AWS關機
    				try{
	    				List<String> instanceIds = new ArrayList<String>();
						instanceIds.add(ws.getInstanceId());
						
						TerminateInstancesRequest ti = new TerminateInstancesRequest(instanceIds);
						amazonEC2Client.terminateInstances(ti);
    				} catch (AmazonServiceException e) {
    					logger.error(CLASSNAME, "checkCloseAWSMachine", "雲端AWS異常", e);
    				}
    			}
				
				
			}
    	}
    }
    
    private void checkJobDone(EntityManager postgres, String env) {
    	String sql = "select * from Schedule_Job sj "
    			+ "where sj.status <> 9 "
    			+ "and sj.status<>-1 "
    			+ "and (sj.create_time + interval '6 hours' ) < sj.udt ";
    	
    	@SuppressWarnings("unchecked")
		List<ScheduleJob> scheduleJobs = postgres.createNativeQuery(sql, ScheduleJob.class)
    			.getResultList();
    	
		if (!scheduleJobs.isEmpty()) {
			for(ScheduleJob scheduleJob : scheduleJobs) {
				String itemId = getItemId(postgres, scheduleJob);
				//將失敗訊息傳至slack
				String message = "[" + env + "] Job執行超過6小時尚未結束 - {" + scheduleJob.getJobGroup() + "} {" + itemId + "} - " + CommonUtil.getDateToString(new Date());
				sendMessageToSlack(message);
			}
		}
    }
    
    private void checkMachineOverTime(EntityManager postgres, String env) {
    	String sql = "select * from worker_status ws "
    			+ "where ws.instance_Id != 'jobTransferFileCheck' "
    			+ " and ws.status <> -1 "
    			+ " and (ws.worker_start_time + interval '2 hour' ) > now() ";
    	
    	@SuppressWarnings("unchecked")
		List<WorkerStatus> workerStatusList = postgres.createNativeQuery(sql, WorkerStatus.class)
    			.getResultList();
    	
		if (!workerStatusList.isEmpty()) {
			AmazonEC2 amazonEC2Client = AmazonEC2ClientBuilder.defaultClient();
			
			for(WorkerStatus ws : workerStatusList) {
				//將失敗訊息傳至slack
				String message = "[" + env + "] 機器執行超過1小時尚未結束 - {" + ws.getInstanceId() + "} - " + CommonUtil.getDateToString(new Date());
				sendMessageToSlack(message);
				
				//呼叫雲端AWS關機
				try{
    				List<String> instanceIds = new ArrayList<String>();
					instanceIds.add(ws.getInstanceId());
					
					TerminateInstancesRequest ti = new TerminateInstancesRequest(instanceIds);
					amazonEC2Client.terminateInstances(ti);
				} catch (AmazonServiceException e) {
					logger.error(CLASSNAME, "checkCloseAWSMachine", "雲端AWS異常", e);
				}
			}
		}
    }
    
    private void sendMessageToSlack(String message) {
    	try {
			String url = "https://hooks.slack.com/services/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";
			
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/json");

			Gson gson = new Gson();
			Map<String, String> reqmap = new HashMap<String, String>();
	        reqmap.put("text", message);
	        StringEntity postingString = new StringEntity(gson.toJson(reqmap),"UTF-8");
	        httpPost.setEntity(postingString);
	        client.execute(httpPost);
		} catch (Exception e) {
			logger.error("JobTransferFileCheckHandler",e);
		}
    }
    
    private String getItemId(EntityManager postgres, ScheduleJob sj) {
    	String itemId = "";
    	DataMessage message= sj.getJobParam(DataMessage.class);
		String bookUniId = message.getBookUniId();
		if(bookUniId == null) {
			MediaBookRequestApiData mediaBookRequestApiData = sj.getJobParam(MediaBookRequestApiData.class);
			itemId = mediaBookRequestApiData.getItem_id();
		}else {
			String[] sp = bookUniId.split("_");
			try {
				Item item = Item.getItemByItemId(postgres, sp[0]);
				BaseBookInfo info = (BaseBookInfo) item.getInfo();
				String url = info.getEfile_cover_url();
				String[] sp2 = url.split("/");
				String name = sp2[sp2.length -1];
				itemId = name.split("\\.")[0];
			} catch (ServerException e) {
				//若查不到店內碼的itemId(如:E05xxxx)，則使用bookUniId前面的itemId(如:ec29a20f-59a1-4c3c-9e44-7089aa7881e4)
				itemId = sp[0];
			} 
		}
		return itemId;
    }
    
    private Date getCurrentDateZone(Date date) {
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(date);
    	//每30分鐘一個區間
//    	int minute = cal.get(Calendar.MINUTE);
//		minute = minute >= 30 ? 30 : 0;
		cal.add(Calendar.MINUTE , (cal.get(Calendar.MINUTE)-30));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
    	return cal.getTime();
    }
    
    private String getInstanceIds(List<String> list) {
    	String instanceIds = "";
		for(String instanceId : list) {
			if(!"".equals(instanceIds)) {
				instanceIds += ", ";
			}
			instanceIds += instanceId;
		}
		return instanceIds;
    }
}