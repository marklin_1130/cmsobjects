package digipages.CommonJobHandler;

import static org.boon.Boon.toJson;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;

import digipages.BooksHandler.RescanDRMHandler;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonResponse;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;

import model.ScheduleJob;

public class JobRecoverHandler {
	private BlockingQueue<ScheduleJob> localJobQueue;
	private static List<ScheduleJob> jobs;
	private static DPLogger logger = DPLogger.getLogger(JobRecoverHandler.class.getName());

	public JobRecoverHandler(BlockingQueue<ScheduleJob> jobQueue) {
		this.localJobQueue = jobQueue;
	}

	public String jobRecover(EntityManager postgres) throws ServerException {
		String jsonStr = "";


		// response
		CommonResponse records = new CommonResponse();
		records.setResult(true);
		jsonStr = toJson(records);

		try {
			ScheduleJobManager.refreshJobs();
			ScheduleJobManager.refreshWorkerStatus();
			Ec2WorkerManager.cleanOldRecords(postgres);
			recoverLocalJobs(postgres);
			adjustEC2Workers(postgres);
			Ec2WorkerManager.adjustOpenBookWorkers();
			recoverLambdaWorkers(postgres);
			logger.info("V1.7 Job recover at " + CommonUtil.currentDateTime());
		} catch (Exception ex) {
			logger.error("Exception when recover Job.",ex);
			throw new ServerException("id_err_000", ex.getMessage());
		}

		return jsonStr;
	}
	
	

	private void recoverLambdaWorkers(EntityManager postgres) {
		// in most case: lambda just start run, no recovery is needed.
		// if fail ==> job is pushed to Ec2
		
		List <ScheduleJob> tmpJobs=ScheduleJobManager.
				takeJobsByRunner(UUID.randomUUID().toString()
				, RunMode.LambdaMode.getValue(), 100);
		
		// put queue
		for (ScheduleJob job : tmpJobs) {
			ScheduleJobManager.notifyWorkers(job);
		}
	}

	// 
	private void adjustEC2Workers(EntityManager postgres) throws Exception{
		Ec2WorkerManager.adjustWorkers();
		
	}
	

	private void recoverLocalJobs(EntityManager postgres) throws InterruptedException {
		LocalRunner.NotifyRunner();
	}
}