package digipages.CommonJobHandler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.codec.binary.Base64;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceRequest;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.BlockDeviceMapping;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.EbsBlockDevice;
import com.amazonaws.services.ec2.model.GroupIdentifier;
import com.amazonaws.services.ec2.model.IamInstanceProfileSpecification;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.LaunchSpecification;
import com.amazonaws.services.ec2.model.RequestSpotInstancesRequest;
import com.amazonaws.services.ec2.model.RequestSpotInstancesResult;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.SpotInstanceRequest;
import com.amazonaws.services.ec2.model.SpotPlacement;
import com.amazonaws.services.ec2.model.Tag;

import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.WorkerStatus;

/**
 * adjust Ec2 Runners 
 * @author Eric.CL.Chou
 *
 */
public class Ec2WorkerManager_nonFleetVersion {
	private static final DPLogger logger = DPLogger.getLogger(Ec2WorkerManager_nonFleetVersion.class.getName());
	private static int workerLoadFactor = 5; // avg worker handle size. (small = frequent start/stop, big = longer digest time)
	private static EntityManagerFactory emFactory=null;
	private static boolean startingEc2=false;
	private static int maxEc2Count=10;
	private static String endPoint="ec2.ap-northeast-1.amazonaws.com";
	private static String EcWorker2ImgId ;
	private static boolean inDebug=false;
	private static String subnetId_1a;
	private static String subnetId_1c;
	private static String WorkerScript;
	private static String SecurityGroupId;
	private static String spotInstanceType1;
	private static String spotInstancePrice1;
	private static String spotInstanceType2;
	private static String spotInstancePrice2;
	private static String onDemonInstanceType;
	private static String ec2S3Profile;
	private static String env;

	private Ec2WorkerManager_nonFleetVersion(){
	}

	public static void setInDebug(boolean inDebug)
	{
		Ec2WorkerManager_nonFleetVersion.inDebug=inDebug;
	}
	
	// must be set before run.
	public static void setEntityManagerFactory(EntityManagerFactory emFactory)
	{
		Ec2WorkerManager_nonFleetVersion.emFactory=emFactory;
	}
	
	// must be set before run.
	public static void setConfig(Properties prop, String startScript) throws IOException
	{
		workerLoadFactor = Integer.parseInt(prop.getProperty("WorkerLoadFactor","5"));
		maxEc2Count = Integer.parseInt(prop.getProperty("MaxEc2Count","10"));
		EcWorker2ImgId = prop.getProperty("EcWorker2ImgId","ami-43c60d22");
		subnetId_1a = prop.getProperty("subnetId_1a");
		subnetId_1c = prop.getProperty("subnetId_1c");
		SecurityGroupId = prop.getProperty("SecurityGroupId");
		spotInstanceType1 = prop.getProperty("SpotInstanceType1");
		spotInstancePrice1 = prop.getProperty("SpotInstancePrice1");
		spotInstanceType2 = prop.getProperty("SpotInstanceType2");
		spotInstancePrice2 = prop.getProperty("SpotInstancePrice2");
		onDemonInstanceType = prop.getProperty("OnDemonInstanceType");
		WorkerScript=startScript;
		ec2S3Profile = prop.getProperty("EC2S3Profile");
		env = prop.getProperty("env");
	}


	/**
	 * rule:
	 * if (waitingJobs > 5 and no idle Ec2)
	 *   start a new EC2
	 * but pay attention to starting machines.
	 * if fail, don't force it or crash the process.
	 */
	public static void adjustWorkers() {
		try {
			int totalEc2 = totalEc2Count();
			int idleEc2 = countIdleEC2();
			int waitingJobs = waitingJobs();
			if (totalEc2 >=maxEc2Count // too many Ec2
					|| startingEc2  // in Starting
					|| idleEc2>=1 // have idle Ec2
					|| waitingJobs<1) // no jobs
			{
				return;
			}
			
			// init case no ec2
			if (totalEc2==0){
				startNewEc2();
				return;
			}
			// compare ratio of job/Workers
			if ((waitingJobs/workerLoadFactor) > idleEc2){
				startNewEc2();
				return;
			}
		} catch (Exception e)
		{
			logger.error("Fail adjustWorkers",e);
		}
	}

	static int totalEc2Count() {
		int count=0;
		EntityManager em = emFactory.createEntityManager();
		try {
			count=WorkerStatus.countAllInstances(em);
		}catch (Exception e)
		{
			logger.error("Fail to count Ec2 workers",e);
		}finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			em.close();
		}
		
		return	count;

	}
	
	private static RequestSpotInstancesRequest configSpotInstance(BlockDeviceMapping deviceMapping , IamInstanceProfileSpecification iamInstanceProfile,String InstanceType,String price,String subnetId) throws UnsupportedEncodingException{
		RequestSpotInstancesRequest runInstancesRequest = new RequestSpotInstancesRequest();
		LaunchSpecification launchSpecification = new LaunchSpecification();
		launchSpecification.setImageId(EcWorker2ImgId);
		launchSpecification.setInstanceType(InstanceType);
		launchSpecification.withBlockDeviceMappings(deviceMapping);
		GroupIdentifier gi = new GroupIdentifier();
		gi.setGroupId(SecurityGroupId);
		launchSpecification.withAllSecurityGroups(gi);
//		SpotPlacement sp = new SpotPlacement();
//		sp.setAvailabilityZone("ap-northeast-1a");
//		launchSpecification.withPlacement(sp);
		launchSpecification.setSubnetId(subnetId);
		launchSpecification.setIamInstanceProfile(iamInstanceProfile);
		runInstancesRequest.setSpotPrice(price);
		runInstancesRequest.setInstanceCount(Integer.valueOf(1));
		runInstancesRequest.setLaunchSpecification(launchSpecification);
		launchSpecification.withUserData(Base64.encodeBase64String(WorkerScript.getBytes("UTF-8")));
		return runInstancesRequest;
	}
	
	/**
	 * start new Ec2 by command
	 * @throws Exception 
	 */
	static synchronized void startNewEc2() throws Exception {
		
		WorkerStatus ws=null;
		String instanceId = "";
		try {
			
			startingEc2=true;
			if (inDebug){
				logger.debug("Start a new EC2");
				instanceId = UUID.randomUUID().toString(); 
				ws = initNewWorker();
				ws.setInstanceId(instanceId);
				updateWorkerStatus(ws);
				startingEc2=false;
				return;
			}
			
			ws = initNewWorker();
			AmazonEC2Client amazonEC2Client = new AmazonEC2Client();
			amazonEC2Client.setEndpoint(endPoint);
			BlockDeviceMapping deviceMapping = new BlockDeviceMapping();
			EbsBlockDevice ebs = new EbsBlockDevice();
			ebs.setDeleteOnTermination(true);
			ebs.setVolumeSize(30);
			deviceMapping.setEbs(ebs);
			deviceMapping.setDeviceName("/dev/sda1"); // centos
			IamInstanceProfileSpecification iamInstanceProfile = new IamInstanceProfileSpecification();
			iamInstanceProfile.withArn(ec2S3Profile);
			
			//spot type1 1c
			RequestSpotInstancesRequest requestSpotInstancesRequest = configSpotInstance(deviceMapping,iamInstanceProfile,spotInstanceType1,spotInstancePrice1,subnetId_1c);
			
			instanceId = doRequestEC2(amazonEC2Client,requestSpotInstancesRequest);
			
			if(startingEc2){
				//spot type1 1a
				requestSpotInstancesRequest = configSpotInstance(deviceMapping,iamInstanceProfile,spotInstanceType1,spotInstancePrice1,subnetId_1a);
				instanceId = doRequestEC2(amazonEC2Client,requestSpotInstancesRequest);
			}
			
			if(startingEc2){
				//spot type2 1c
				requestSpotInstancesRequest = configSpotInstance(deviceMapping,iamInstanceProfile,spotInstanceType2,spotInstancePrice2,subnetId_1c);
				instanceId = doRequestEC2(amazonEC2Client,requestSpotInstancesRequest);
			}
			
			if(startingEc2){
				//spot type2 1a
				requestSpotInstancesRequest = configSpotInstance(deviceMapping,iamInstanceProfile,spotInstanceType2,spotInstancePrice2,subnetId_1a);
				instanceId = doRequestEC2(amazonEC2Client,requestSpotInstancesRequest);
			}
			
			if(startingEc2){
				//on demon 1a
				RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
				runInstancesRequest
				.withImageId(EcWorker2ImgId)
				.withInstanceType(onDemonInstanceType)
				.withMinCount(1)
				.withMaxCount(1)
				.withBlockDeviceMappings(deviceMapping)
				.withSecurityGroupIds(SecurityGroupId)
				.withSubnetId(subnetId_1a)
				.withIamInstanceProfile(iamInstanceProfile)
				.withUserData(Base64.encodeBase64String(WorkerScript.getBytes("UTF-8")))
				.withInstanceInitiatedShutdownBehavior("terminate");
				
				instanceId = doRequestEC2(amazonEC2Client,runInstancesRequest);
			}
			
			if(startingEc2){
				//on demon 1c
				RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
				runInstancesRequest
				.withImageId(EcWorker2ImgId)
				.withInstanceType(onDemonInstanceType)
				.withMinCount(1)
				.withMaxCount(1)
				.withBlockDeviceMappings(deviceMapping)
				.withSecurityGroupIds(SecurityGroupId)
				.withSubnetId(subnetId_1c)
				.withIamInstanceProfile(iamInstanceProfile)
				.withUserData(Base64.encodeBase64String(WorkerScript.getBytes("UTF-8")))
				.withInstanceInitiatedShutdownBehavior("terminate");
				
				instanceId = doRequestEC2(amazonEC2Client,runInstancesRequest);
			}
			
			if(startingEc2){
				throw new ServerException("id_err_999","Fail to Start Ec2");
			}else{
				ws.setInstanceId(instanceId);
				updateWorkerStatus(ws);
			}
			
			
		}catch (Exception e)
		{
			logger.error("Unexcepted when starting Ec2.",e);
			// update worker status if it is in fail mode.
			if (ws!=null){
				ws.setStatus(-1);
				updateWorkerStatus(ws);
			}
			throw e;
		}
		
	}
	
	private static String doRequestEC2(AmazonEC2Client amazonEC2Client,AmazonWebServiceRequest amazonWebServiceRequest){
		
		// a list of instances to tag.
		ArrayList<String> instanceIds = new ArrayList<String>();
					
		startingEc2 = false;
		if(amazonWebServiceRequest instanceof RequestSpotInstancesRequest){
			RequestSpotInstancesRequest requestSpotInstancesRequest = (RequestSpotInstancesRequest)amazonWebServiceRequest;
			RequestSpotInstancesResult runSpotInstancesResult = amazonEC2Client.requestSpotInstances(requestSpotInstancesRequest);
			String resultStr = runSpotInstancesResult.toString();
			logger.debug("Run Instance result: "+resultStr);
			String lower = resultStr.toLowerCase();
			if (lower.contains("fail") || lower.contains("error"))
			{
				logger.error("Fail to start ec2 worker: "+resultStr);
				startingEc2=true;
			}
			
			List<SpotInstanceRequest> requestResponses = null;
			// Call the RequestSpotInstance API.
			requestResponses = runSpotInstancesResult.getSpotInstanceRequests();
			// A list of request IDs to tag
			ArrayList<String> spotInstanceRequestIds = new ArrayList<String>();

			// Add the request ids to the hashset, so we can determine when they hit the
			// active state.
			for (SpotInstanceRequest requestResponse : requestResponses) {
				logger.info("Created Spot Request: "+requestResponse.getSpotInstanceRequestId());
			    spotInstanceRequestIds.add(requestResponse.getSpotInstanceRequestId());
			}
			
			boolean anyOpen; // tracks whether any requests are still open

			int breakCount = 0;
			
			do {
				breakCount++;
			    DescribeSpotInstanceRequestsRequest describeRequest = new DescribeSpotInstanceRequestsRequest();
			    describeRequest.setSpotInstanceRequestIds(spotInstanceRequestIds);

			    anyOpen=false; // assume no requests are still open

			    try {
			        // Get the requests to monitor
			        DescribeSpotInstanceRequestsResult describeResult = amazonEC2Client.describeSpotInstanceRequests(describeRequest);

			        List<SpotInstanceRequest> describeResponses = describeResult.getSpotInstanceRequests();

			        // are any requests open?
			        for (SpotInstanceRequest describeResponse : describeResponses) {
			                if (describeResponse.getState().equals("active")) {
			                	// get the corresponding instance ID of the spot request
				                instanceIds.add(describeResponse.getInstanceId());
			                    break;
			                }else{
			                    anyOpen = true;
			                }
			        }
			    }
			    catch (AmazonServiceException e) {
			        // Don't break the loop due to an exception (it may be a temporary issue)
			        anyOpen = true;
			    }

			    try {
			    	if(breakCount>3){
			    		break;
			    	}
			        Thread.sleep(60*1000); // sleep 60s.
			    }
			    catch (Exception e) {
			        // Do nothing if the thread woke up early.
			    }
			} while (anyOpen);
			
			// Create a list of tags to create
			ArrayList<Tag> instanceTags = new ArrayList<Tag>();
			instanceTags.add(new Tag("Name","ebook-convert-"+env));

			// Create the tag request
			CreateTagsRequest createTagsRequest_instances = new CreateTagsRequest();
			createTagsRequest_instances.setResources(instanceIds);
			createTagsRequest_instances.setTags(instanceTags);

			// Tag the instance
			try {
				amazonEC2Client.createTags(createTagsRequest_instances);
			}
			catch (AmazonServiceException e) {
			    // Write out any exceptions that may have occurred.
			    logger.error("Error terminating instances");
			    logger.error("Caught Exception: " + e.getMessage());
			    logger.error("Reponse Status Code: " + e.getStatusCode());
			    logger.error("Error Code: " + e.getErrorCode());
			    logger.error("Request ID: " + e.getRequestId());
			}
			
		}
		
		if(amazonWebServiceRequest instanceof RunInstancesRequest){
			RunInstancesRequest runInstancesRequest = (RunInstancesRequest)amazonWebServiceRequest;
			RunInstancesResult runInstancesResult = amazonEC2Client.runInstances(runInstancesRequest);
			try {
				instanceIds.add(runInstancesResult.getReservation().getInstances().get(0).getInstanceId());
				// TAG EC2 INSTANCES
				List<Instance> instances = runInstancesResult.getReservation().getInstances();
				int idx = 1;
				for (Instance instance : instances) {
				  CreateTagsRequest createTagsRequest = new CreateTagsRequest();
				  createTagsRequest.withResources(instance.getInstanceId()).withTags(new Tag("Name", "ebook-convert-"+env));
				  amazonEC2Client.createTags(createTagsRequest);
				  idx++;
				}
			} catch (Exception e) {
				logger.error(e);
			}
			
			String resultStr = runInstancesResult.toString();
			logger.debug("Run Instance result: "+resultStr);
			String lower = resultStr.toLowerCase();
			if (lower.contains("fail") || lower.contains("error"))
			{
				logger.error("Fail to start ec2 worker: "+resultStr);
				startingEc2=true;
			}
			
		}
		
		if(!instanceIds.isEmpty()){
			return instanceIds.get(0);
		}
		
		return "";
		
	}

	
	private static void updateWorkerStatus(WorkerStatus ws) {
		EntityManager em = emFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(ws);
			ws.setStatus(0);
			em.getTransaction().commit();

		}catch (Exception e)
		{
			logger.debug("Fail to update worker ?! ",e);
		}finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			em.close();
		}
		
	}

	// initialize a Instance with thread id 0 (default)
	private static WorkerStatus initNewWorker() {
		EntityManager em = emFactory.createEntityManager();
		try {
			WorkerStatus ws = new WorkerStatus();
			ws.setStatus(0);
			
			em.getTransaction().begin();
			ws.setInstanceId("InitInstance"+ UUID.randomUUID().toString());
			ws.setThreadId(0);
			ws.setWorkerStartTime(new Date());
			em.persist(ws);
			
			em.getTransaction().commit();
			return ws;
		}catch (Exception e)
		{
			logger.debug("Fail to register worker ?! ",e);
		}finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			em.close();
		}
		return null;
	}

	private static String getInstanceIdFromResultStr(String resultStr) {
		String result=null;
		try {
			String []split=resultStr.split(",");
			for (String childItem:split)
			{
				if (childItem.contains("InstanceId"))
				{
					result = childItem.split(":")[2].trim();
					return result;
				}
			}

			
		} catch  (Exception e)
		{
			logger.error("Fail to decode InstanceId from Result" + resultStr,e);
		}
		return result;
	}

	private static int countIdleEC2() {
		int ret = 0;
		EntityManager em = emFactory.createEntityManager();
		try {
			ret=WorkerStatus.countAvailInstances(em);
		}catch (Exception e)
		{
			logger.debug("Fail to update worker ?! ",e);
		}finally
		{
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			em.close();
		}

		return ret;
	}

	private static int waitingJobs() {
		return ScheduleJobManager.countPendingJobsByRunner(2);
	}

	public static void cleanOldRecords(EntityManager postgres) {
		WorkerStatus.cleanOldRecords(postgres);
		
	}

	
}
