package digipages.CommonJobHandler;

public enum RunMode {
	LocalMode(0), LambdaMode(1), ExtEc2Mode(2), DirectRunMode(3), UnknownRunMode(999), FailRunMode(-1),OPENBOOK_EC2(20);

	private final int value;

	private RunMode(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	/**
	 * run mode => integer (for db)
	 * @param value
	 * @return
	 */
	public static RunMode getValue(int value) {
		for (RunMode e : RunMode.values()) {
			if (e.value == value) {
				return e;
			}
		}
		return null;// not found
	}
	
	
}
