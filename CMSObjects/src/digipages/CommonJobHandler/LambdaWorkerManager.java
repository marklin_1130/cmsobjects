package digipages.CommonJobHandler;


import java.net.URLEncoder;
import java.util.Properties;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;

import digipages.common.DPLogger;
import model.ScheduleJob;

public class LambdaWorkerManager {
	private static Gson gson=new Gson();
	private static DPLogger logger = DPLogger.getLogger(LambdaWorkerManager.class.getName());
	public static void NotifyRunner(Properties config,ScheduleJob job) {
		
		String topicArn = config.getProperty("GenericLambdaTopic");

		AmazonSNSClient snsClient;

		try {
			snsClient = new AmazonSNSClient();

			snsClient.setRegion(Region.getRegion(Regions.AP_NORTHEAST_1));

			String msg = gson.toJson(job);
			logger.debug("Sending lambda job:"+msg);
			msg = URLEncoder.encode(msg, "UTF-8");
			PublishRequest publishRequest = new PublishRequest(topicArn, msg);
			PublishResult publishResult = snsClient.publish(publishRequest);
			logger.debug("Sending Lambda topic:" + publishResult.toString());

			
			// status is update by lambda itself.
//			job.setStatus(1);
//			ScheduleJobManager.updateJob(job);
		} catch (Exception e) {
			logger.error("Fail send Lambda Job:" + job.toGsonStr(),e);
		}
		
		
	}
	

}
