package digipages.CommonJobHandler;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import model.ScheduleJob;

/**
 * getJob/add Job/Finish job utility
 * @author Eric.CL.Chou
 *
 */
public class JobStatusReporter {

	private static String baseURI = "http://localhost:8080/CMSAPIBook/ScheduleJobManager"; // will be replaced once in server/lambda env
	private static final DPLogger logger = DPLogger.getLogger(JobStatusReporter.class.getName());
	private static JsonSerializer<Date> dateJsonSerializer = 
		     (src, typeOfSrc, context) -> src == null ? null : new JsonPrimitive(src.getTime());
	private static	JsonDeserializer<Date> dateJsonDeserializer = 
		    	     (json, typeOfT, context) -> json == null ? null : new Date(json.getAsLong());
	private static	Gson gson = new GsonBuilder().
			registerTypeAdapter(Date.class,dateJsonDeserializer).
			registerTypeAdapter(Date.class,dateJsonSerializer).create();

	/**
	 * updateJob by list
	 * @param jobs
	 * @return
	 * @throws IOException
	 */
	public static String updateJobs(List<ScheduleJob> jobs) throws IOException {
		logger.debug("Sending info to "+ baseURI);
        URL obj = new URL(baseURI);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("PUT");
        connection.setRequestProperty("Content-Type", "application/json");
        String urlParameters = gson.toJson(jobs);
        logger.debug("Sending job list:" + urlParameters);
        urlParameters = URLEncoder.encode(urlParameters,"UTF-8");
        connection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int responseCode = connection.getResponseCode();
        logger.info( "Response Code:" + responseCode);

        String response = CommonUtil.getResponseFromConnection(connection);
        response = URLDecoder.decode(response,"UTF-8");
        logger.info("response:" + response);
        
        connection.disconnect();
        
        return response;
	}
	
	public static List<ScheduleJob> getJobsRetry(String jobKey, int runner, int jobCnt)  {
		List <ScheduleJob> jobs = new ArrayList<>();
		int runCnt=0;
		while (runCnt<5){
		    
		    try {
                Thread.sleep(5000L + new Random().nextInt(50000));
            } catch (InterruptedException e) {
            } 
		    
			try {
				jobs = getJobs(jobKey,runner,jobCnt);
				return jobs;
			}catch (Exception e)
			{
				e.printStackTrace();
				logger.info("Retrying get Job.");
			}
			runCnt++;
		}
		logger.warn("Retry failed");
		return jobs;
	}
	
	/**
	 * getJobs by runner
	 * @param jobKey for reversation. (only corrosponding jobKey will be fetched)
	 * @param runner LocalMode(0), LambdaMode(1), ExtEc2Mode(2), DirectRunMode(3);
	 * @param jobCnt how many jobs took (default 1)
	 * @return 
	 * @throws IOException
	 */
	public static List<ScheduleJob> getJobs(String jobKey, int runner, int jobCnt) throws IOException {
		String paras = "jobKey="+jobKey+"&" + "runner=" + runner + "&jobCnt=" +jobCnt;
        URL obj = new URL(baseURI+"?" + paras);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        System.out.println("connect:"+baseURI+"?" + paras);
        //add reuqest header
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();

		logger.info("GET Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			String response = CommonUtil.getResponseFromConnection(connection);
			response = URLDecoder.decode(response,"UTF-8");
			logger.info("get Job: "+response);
			Type listType = new TypeToken<ArrayList<ScheduleJob>>(){}.getType();
			return gson.fromJson(response,listType);

		} else {
			throw new IOException("fail to connect");
		}
		
	}



	public static String addJobs(List<ScheduleJob> jobs) throws IOException {
		return addJobs(jobs,0);
	}
	public static String addJobs(List<ScheduleJob> jobs, int retryCnt) throws IOException {
		String response = "Fail to sendJob.";
		try {
			logger.debug("sending addJob to " + baseURI);
	        URL obj = new URL(baseURI);
	        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("Content-Type", "application/json");
	
	        String urlParameters = gson.toJson(jobs);
	        logger.debug("adding jobs" + urlParameters);
	        urlParameters=URLEncoder.encode(urlParameters,"UTF-8");
	        connection.setDoOutput(true);
	        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
	        wr.writeBytes(urlParameters);
	        wr.flush();
	        wr.close();
	        int responseCode = connection.getResponseCode();
	        logger.info( "Response Code:" + responseCode);
	        response = CommonUtil.getResponseFromConnection(connection);
	        response = URLDecoder.decode(response,"UTF-8");
	        logger.info("response:" + response);
	        connection.disconnect();
	        
		} catch (IOException e)
		{
			
			if (retryCnt>=5){
				throw e;
			}
			
			try {
				Thread.sleep(10*1000); // wait 10 sec and retry.
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			response = addJobs(jobs,retryCnt+1);
		}
        return response;
	}



	/**
	 * set before use, just once is enough
	 * @param baseURI
	 */
	public static void setBaseURI(String baseURI) {
		JobStatusReporter.baseURI = baseURI;
	}

	/**
	 * with retry 
	 */
	public static void updateJobsRetry(List<ScheduleJob> jobs) {
		int runCnt=0;
		while (runCnt<5){
			try {
				updateJobs(jobs);
				return ;
			}catch (Exception e)
			{
				logger.error("Retrying Update Jobs.",e);
			}
			runCnt++;
			try {
				Thread.sleep(5000L + new Random().nextInt(5000));
			} catch (InterruptedException e) {
			} 
		}
		logger.error("Retry failed");
		
		
	}

	public static void updateJobRetry(ScheduleJob job) {
		List <ScheduleJob> list = new ArrayList<>();
		list.add(job);
		updateJobsRetry(list);
	}
	
	public static void findishJobRetry(ScheduleJob job)
	{
		job.setFinishTime(new Date());
		job.setStatus(9);
		updateJobRetry(job);
	}

	public static void addJob(ScheduleJob sj) throws IOException {
		logger.debug("adding job to http request:" + sj.toGsonStr());
		List <ScheduleJob>list = new ArrayList<>();
		list.add(sj);
		addJobs(list);
		
	}

	public static ArrayList<ScheduleJob> getJobById(Long id) throws IOException {
		String paras = "id="+id;
		String fullURL=baseURI+"?" + paras;
		logger.debug("Sending request To:" + fullURL);
        URL urlObj = new URL(fullURL);
        HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();

        //add reuqest header
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();

		logger.debug("GET Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			String response = CommonUtil.getResponseFromConnection(connection);
			response = URLDecoder.decode(response,"UTF-8");
			logger.debug("get Job: "+response);
			Type listType = new TypeToken<ArrayList<ScheduleJob>>(){}.getType();
			return gson.fromJson(response,listType);

		} else {
			throw new IOException("fail to connect");
		}
	}

}
