package digipages.CommonJobHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusResult;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.google.gson.Gson;

import digipages.common.DPLogger;
import model.WorkerStatus;

public class SpotInstanceMonitorThread extends Thread {

    private static EntityManagerFactory emFactory = null;

    private static final DPLogger ec2Logger = DPLogger.getLogger("ec2");

    private String instanceId;
    
    private String tagName;

    private AmazonEC2 amazonEC2Client;
    
    private Map<Date,String> jobHistoryMap;
    
    static Gson gson = new Gson();
    
    SpotInstanceMonitorThread(EntityManagerFactory emFactory, AmazonEC2 amazonEC2Client, String instanceId , String tagName) {
        this.instanceId = instanceId;
        SpotInstanceMonitorThread.emFactory = emFactory;
        this.amazonEC2Client = amazonEC2Client;
        this.tagName = tagName;
        this.jobHistoryMap = new HashMap<Date,String>();
    }

    @Override
    public void run() {

        boolean isRunning = true;
        DescribeInstanceStatusRequest describeInstanceRequest = new DescribeInstanceStatusRequest().withInstanceIds(instanceId).withIncludeAllInstances(true);
        DescribeInstanceStatusResult describeInstanceResult = amazonEC2Client.describeInstanceStatus(describeInstanceRequest);
        List<InstanceStatus> state = describeInstanceResult.getInstanceStatuses();
        while (isRunning) {
            // Do nothing, just wait, have thread sleep if needed
            try {
                sleep(5000);
            } catch (InterruptedException e) {
            }
            describeInstanceResult = amazonEC2Client.describeInstanceStatus(describeInstanceRequest);
            state = describeInstanceResult.getInstanceStatuses();
            for (InstanceStatus instanceStatus : state) {
                String status = instanceStatus.getInstanceState().getName();
//                ec2Logger.debug("EC2 convert instance:"+instanceId+" tag:"+tagName+" status:"+status);
                
                List<WorkerStatus> wsList = new ArrayList<WorkerStatus>();
                EntityManager em = emFactory.createEntityManager();
                wsList = WorkerStatus.listByInstanceId(em, instanceId, 0);
                for (WorkerStatus workerStatus : wsList) {
                    if(workerStatus.getJobStartTime()!=null&&jobHistoryMap.get(workerStatus.getJobStartTime())==null){
                        String jobInfo = gson.toJson(workerStatus.getJobInfo());
                        jobHistoryMap.put(workerStatus.getJobStartTime(),jobInfo);
                        ec2Logger.debug("EC2 convert instance:"+instanceId+" tag:"+tagName+" ,getJob:"+workerStatus.getJobStartTime()+",jobInfo:"+jobInfo);
                    }
                }
                
                if (!(StringUtils.equalsIgnoreCase("running", status) || StringUtils.equalsIgnoreCase("pending", status))) {
                    isRunning = false;
                    
                    try {
                        em.getTransaction().begin();;
                        wsList = WorkerStatus.listByInstanceId(em, instanceId, 0);
                        for (WorkerStatus workerStatus : wsList) {
                            if (workerStatus.getStatus() > -1) {
                                workerStatus.setStatus(-1);
                                em.persist(workerStatus);
                            }
                        }
                        em.getTransaction().commit();
                        ec2Logger.info("EC2 convert instance:"+instanceId+" tag:"+tagName+" shutdown:"+status);
                    } catch (Exception e) {
                        em.getTransaction().rollback();
                        ec2Logger.error("Fail to update Ec2 workers", e);
                    } finally {
                        if (em.getTransaction().isActive())
                            em.getTransaction().rollback();
                        em.close();
                    }

                }

            }
        }

    }

}
