package digipages.CommonJobHandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class ScheduleJobManager {
	private static final int MAXEC2_RETRYCNT=3;
	private static final int MAXLAMBDA_RETRYCNT=40;
	private static EntityManagerFactory emFactory =null; 
	private static DPLogger logger = DPLogger.getLogger(ScheduleJobManager.class.getName());
	private static Map<String,CommonJobHandlerInterface> handlerList=new HashMap<>();
	private static Properties config;
	static JsonSerializer<Date> ser = new JsonSerializer<Date>() {
		  @Override
		  public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext 
		             context) {
		    return src == null ? null : new JsonPrimitive(src.getTime());
		  }
		};

	static JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		  @Override
		  public Date deserialize(JsonElement json, Type typeOfT,
		       JsonDeserializationContext context) throws JsonParseException {
		    return json == null ? null : new Date(json.getAsLong());
		  }
		};

	static	Gson gson = new GsonBuilder()
		   .registerTypeAdapter(Date.class, ser)
		   .registerTypeAdapter(Date.class, deser).create();
	
	
	public ScheduleJobManager(EntityManagerFactory emFactory) {
		ScheduleJobManager.emFactory = emFactory;
		Ec2WorkerManager.setEntityManagerFactory(emFactory);
	}
	
	public static void setEntityManagerFactory(EntityManagerFactory emf)
	{
		emFactory=emf;
	}
	
	// without entityManager 
	public ScheduleJobManager()
	{
		
	}
	public static void addJob(ScheduleJob job) throws ServerException, IOException 
	{
		if (null==emFactory){
			JobStatusReporter.addJob(job);
		}else{
			_addJob(job);
		}
	}
	/**
	 * 
	 * @param job
	 * @throws Exception 
	 */
	public static void _addJob(ScheduleJob job) throws ServerException 
	{
		EntityManager postgres=emFactory.createEntityManager();
		try {
			// update time
			job.setLastUpdated(new Date());
			if ("FixedlayoutThumbnail".equals(job.getJobName()))
			{
				job.setJobRunner(1);
			}else{
				RunMode r = (RunMode)Class.forName(job.getJobName()).getMethod("getInitRunMode", ScheduleJob.class).invoke(null, job);
				job.setJobRunner(r.getValue());
			}
			postgres.getTransaction().begin();
			postgres.persist(job);
			StringBuilder tmpLog=new StringBuilder();
			tmpLog.append("jobAddto DB ")
				.append(job.getJobName()).append(" ")
				.append(job.getJobParam(Object.class).toString()).append("\n");
			logger.debug(tmpLog.toString());
			postgres.getTransaction().commit();
			logger.info("Job add to Queue:" + job.getId());
			
			 new Thread()
             {
				 @Override
                 public void run() {
                     notifyWorkers(job);
                 }
             }.start();
             
		} catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
		{
			logger.error("Unexcepted error adding Job",e);
			throw new ServerException("id_err_999","Unexcepted add job error:"+ e.getMessage());
		}finally
		{
			if (postgres.getTransaction().isActive())
				postgres.getTransaction().rollback();
			postgres.close();
		}
	}
	
	public static int addJobs(List<ScheduleJob> jobs) throws ServerException
	{
		if (null==emFactory){
			String response;
			try {
				response = JobStatusReporter.addJobs(jobs);
			} catch (IOException e) {
				logger.error("Fail to send Job to Server",e);
				return 0;
			}
			logger.debug("response: " + response);
			return 1;
		}else {
			return _addJobs(jobs);
		}
	}
	public static int _addJobs(List<ScheduleJob> jobs) throws ServerException
	{
		int jobAdded=0;
		EntityManager postgres=emFactory.createEntityManager();
		try {
			postgres.getTransaction().begin();

			for (ScheduleJob job:jobs) {
				if (!"FixedlayoutThumbnail".equals(job.getJobName()) && !job.getJobName().contains("LosslessCompressionHandler")){
					RunMode r = (RunMode)Class.forName(job.getJobName())
							.getMethod("getInitRunMode", ScheduleJob.class).invoke(null,job);
					job.setJobRunner(r.getValue());
				}
				postgres.persist(job);
				StringBuilder tmpLog=new StringBuilder();
				tmpLog.append("jobAddto DB ")
					.append(job.getJobName()).append(" ")
					.append(job.getJobParam(Object.class).toString()).append("\n");
//				logger.debug(tmpLog.toString());
				jobAdded++;
			}
			postgres.getTransaction().commit();
			
			// after commit, have pk and send to workers.
			for (ScheduleJob job:jobs) {
	            notifyWorkers(job);
			}
			
		} catch (Exception ex)
		{
			logger.error("Unexcepted error adding Job",ex);
			throw new ServerException("id_err_999",ex.getMessage());
		}finally
		{
			if (postgres.getTransaction().isActive())
				postgres.getTransaction().rollback();
			postgres.close();
		}
		

		return jobAdded;
	}
	
	/**
	 * according to job runner, notify and start jobs.
	 * @param job
	 */
	public static void notifyWorkers(ScheduleJob job) {
		
		JobNotifierThread notifyThread = new JobNotifierThread(job, config,emFactory);
		notifyThread.start();
	}
	

	// refresh died jobs 
	public static int refreshJobs()
	{
		int updatedCnt=0;
		String 		queryString =""; 
		EntityManager postgres=emFactory.createEntityManager();
		postgres.getTransaction().begin();
		
		// EC2 already confirm retry => mark as retry
		queryString = 
				  "update schedule_job set "
				+ " status=0 ,"
				+ " retry_cnt=retry_cnt+1,  "
//				+ " should_start_time=null,"
//				+ " should_finish_time=null,  "
				+ " start_time=null, "
				+ " job_key=null "
				+ " where "
				+ " (status=2 and job_runner=2) "
				+ " and retry_cnt<" + MAXEC2_RETRYCNT;
		updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();

		// lambda retry
		queryString = 
				  "update schedule_job set "
				+ " status=0 ,"
				+ " retry_cnt=retry_cnt+1,  "
//				+ " should_start_time=null,"
//				+ " should_finish_time=null,  "
				+ " start_time=null, "
				+ " job_key=null "
				+ " where "
				+ " (status=2 and job_runner!=2) "
				+ " and retry_cnt<" + MAXLAMBDA_RETRYCNT;
		updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();

		
		// timeout/fail ==> retry
		updatedCnt+=updateTimeoutStatus(postgres);
		
		//
		// ec2 retry cnt > 3
		queryString = 
				  "update schedule_job set "
				+ " status=-1 "
				+ " where "
				+ " job_runner=2 and retry_cnt >=" + MAXEC2_RETRYCNT ;
		updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
		
		
		// lambda retry cnt >20 
		queryString = 
				  "update schedule_job set "
				+ " status=-1 "
				+ " where "
				+ " job_runner!=2 and retry_cnt >=" + MAXLAMBDA_RETRYCNT;
		updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
		
		// mediabook convert already confirm retry => mark as retry
		queryString = 
				  "update schedule_job set "
				+ " status=0 ,"
				+ " retry_cnt=retry_cnt+1,  "
//				+ " should_start_time=null,"
//				+ " should_finish_time=null,  "
				+ " start_time=null, "
				+ " job_key=null "
				+ " where "
				+ " (status=3 and job_runner=0) "
				+ " and retry_cnt<" + MAXEC2_RETRYCNT;
		updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
		
	      // MediabookConvertNotifyHandler unkonw retry => mark as retry
	     queryString = 
	               "update schedule_job set "
	             + " status=-1 ,"
	             + " retry_cnt=retry_cnt+1,  "
//	           + " should_start_time=null,"
//	           + " should_finish_time=null,  "
	             + " start_time=null, "
	             + " job_key=null "
	             + " where "
	             + " job_runner=0 "
	             + " and job_name = 'digipages.BooksHandler.vod.MediabookResultNotifyHandler'"
	             + " and job_key is not null "
	             + " and udt < now( )- interval '6 hours'"
	             + " and status <> 9 ";
	     updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
	     
	      // MediabookConvertNotifyHandler unkonw retry => mark as retry
        queryString = 
                  "update schedule_job set "
                + " status=0 ,"
                + " retry_cnt=retry_cnt+1,  "
//              + " should_start_time=null,"
//              + " should_finish_time=null,  "
                + " start_time=null, "
                + " job_key=null "
                + " where "
                + " job_runner=0 "
                + " and job_name = 'digipages.BooksHandler.vod.MediabookConvertNotifyHandler'"
                + " and job_key is not null "
                + " and udt < now( )- interval '30 min'"
                + " and status=0 "
                + " and retry_cnt<" + MAXEC2_RETRYCNT;
        updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
        
        
        // MediabookConvertNotifyHandler unkonw retry => mark as retry
      queryString = 
                "update schedule_job set "
              + " status=0 ,"
              + " retry_cnt=retry_cnt+1,  "
//            + " should_start_time=null,"
//            + " should_finish_time=null,  "
              + " start_time=null, "
              + " job_key=null "
              + " where "
              + " job_runner=0 "
              + " and job_name = 'digipages.BooksHandler.vod.MediabookResultNotifyHandler'"
              + " and job_key is not null "
              + " and udt < now( )- interval '30 min'"
              + " and status=0 "
              + " and retry_cnt<" + MAXEC2_RETRYCNT;
      updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
      
      // BookUpgraderHandler unkonw retry => mark as retry
      queryString = 
                "update schedule_job set "
              + " status=0 ,"
              + " retry_cnt=retry_cnt+1,  "
//            + " should_start_time=null,"
//            + " should_finish_time=null,  "
              + " start_time=null, "
              + " job_key=null "
              + " where "
              + " job_runner=0 "
              + " and job_name = 'digipages.BooksHandler.BookUpgraderHandler'"                                                
              + " and job_key is not null "
              + " and udt < now( )- interval '30 min'"
              + " and status=0 "
              + " and retry_cnt<" + MAXEC2_RETRYCNT;
      updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
        
		postgres.getTransaction().commit();
		postgres.close();
		return updatedCnt;
	}
	
	/**
	 * original version (timeout always to ec2)
	 * @param postgres
	 * @return
	 */
	private static int updateTimeoutStatus_old(EntityManager postgres) {
		String queryString = 
				  "update schedule_job set "
				+ " status=0 ,"
				+ " retry_cnt=retry_cnt+1, "
				+ " job_runner=2 "
				+ " where "
				+ " (status=1 or status=2) "
				+ " and should_finish_time < now() "
				+ " and retry_cnt < " + MAXEC2_RETRYCNT;
		int updatedCnt = postgres.createNativeQuery(queryString).executeUpdate();
		return updatedCnt;
	}
	
	/**
	 * 
	 * @param postgres
	 * @return
	 */
	static int updateTimeoutStatus(EntityManager postgres) {
		List<ScheduleJob> timeoutJobs = ScheduleJob.listTimeoutRetryJobs(postgres);
		for (ScheduleJob job:timeoutJobs)
		{
			
			job.setStatus(0);
			job.setUuid(null); // need to reset to null once it is status=0
			job.setRetryCnt(job.getRetryCnt()+1);
			job.setJobRunner(getJobRunner2ndModeByName(job));
			postgres.persist(job);
		}
		return timeoutJobs.size();
	}

	private static Integer getJobRunner2ndModeByName(ScheduleJob job) {
		RunMode r;
		try {
			r = (RunMode)Class.forName(job.getJobName()).getMethod("getRetryRunMode"
					, RunMode.class).invoke(null, RunMode.getValue(job.getJobRunner()));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException | ClassNotFoundException e) {
			logger.warn("fail to find run mode, continue use same mode.");
			r = RunMode.getValue(job.getJobRunner());
		}
		if (null==r)
			r=RunMode.ExtEc2Mode;
		return r.getValue();
	}

	/**
	 * should be moved out of ScheduleJobManager.
	 * 1. idle worker/Starting Ec2 is too old => mark as dead. (30 min)
	 * 2. running worker is too old mark as dead. (2 hr)
	 * @return how many record is updated
	 */
	public static int refreshWorkerStatus()
	{
		int updatedCnt=0;
		String 		queryString =""; 
		EntityManager postgres=emFactory.createEntityManager();
		try {
			postgres.getTransaction().begin();
			
			// idle/init (udt > 30 min) consider it dead.
			queryString = 
					  "update worker_status set "
					+ " status=-1 "
					+ " where "
					+ " (status=0 or status=1) "
					+ " and udt::varchar::timestamp < (now() - interval '30 min')";
			updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
	
			queryString = 
					  "update worker_status set "
					+ " status=-1 "
					+ " where "
					+ " ( status=2 ) "
					+ " and udt::varchar::timestamp < (now() - interval '120 min')";
			updatedCnt += postgres.createNativeQuery(queryString).executeUpdate();
	
			// 
			
			postgres.getTransaction().commit();
		} catch (Exception e)
		{
			logger.error("fail to refresh Worker Status",e);
		}finally
		{
			if (postgres.getTransaction().isActive())
				postgres.getTransaction().rollback();
		}
		postgres.close();
		return updatedCnt;
		
	}
	
	// most simple version (not used any more)
	public static List<ScheduleJob> takeJobs(ScheduleJob jobParameter, int jobCnt)
	{
		Date shouldEndTime = calcShouldEndTime(RunMode.getValue(jobParameter.getJobRunner()));
		List<ScheduleJob> jobs = new ArrayList<>();
		EntityManager postgres=emFactory.createEntityManager();
		postgres.getTransaction().begin();
		
		int jobCount=postgres.createNativeQuery(
				"update schedule_job sj "
				+ "set job_key = ? , "
				+ "status=1 , "
				+ "start_time=now() , "
				+ "udt=now() , "
				+ "should_finish_time = ? "
				+ "where id in "
				+ "(select id from schedule_job where "
				+ "job_group =? and "
				+ "status=0 and "
				+ "job_name = ? and "
				+ "job_runner= ? "
				+ "limit ? ) ")
		.setParameter(1, jobParameter.getUuid())
		.setParameter(2, shouldEndTime)
		.setParameter(3, jobParameter.getJobGroup())
		.setParameter(4, jobParameter.getJobName())
		.setParameter(5, jobParameter.getJobRunner())
		.setParameter(6, jobCnt).executeUpdate();

		postgres.getTransaction().commit();
		
		
		if (jobCount==0)
			return jobs;
		
		jobs=postgres.createQuery(
				"SELECT sj FROM ScheduleJob sj WHERE sj.uuid = :uuid and sj.status=1"
				,ScheduleJob.class)
		.setParameter("uuid", jobParameter.getUuid()).getResultList();
		postgres.close();
		return jobs;
	}

	// most simple version
	public static List<ScheduleJob> takeJobsByRunner(String jobKey, int runner, int jobCnt)
	{
	    
	    //有重複執行轉檔的情況發生
	    
		Date shouldEndTime = calcShouldEndTime(RunMode.getValue(runner));
		List<ScheduleJob> jobs = new ArrayList<>();
		EntityManager postgres=emFactory.createEntityManager();
		postgres.getTransaction().begin();
		
		String db =	(String) emFactory.getProperties().get(PersistenceUnitProperties.JDBC_URL);
		
		postgres.createNativeQuery(
				"update schedule_job sj "
				+ "set job_key = ?::uuid , "
				+ "status=0 , "
				+ "start_time=now() , "
				+ "udt=now() , "
				+ "should_finish_time = ? "
				+ "where id in "
				+ "(select id from schedule_job where "
				+ "status=0 and "
				+ "job_runner= ? and "
				+ "job_name <> 'digipages.BooksHandler.BookUpgraderHandler' and "
				+ "should_start_time < now() "
				+ "and job_key is null "
				+ "limit ? ) ")
		.setParameter(1, jobKey)
		.setParameter(2, shouldEndTime)
		.setParameter(3, runner)
		.setParameter(4, jobCnt).executeUpdate();

		postgres.getTransaction().commit();
		
		logger.info("V1.7 job query "+db + ",update schedule_job sj "
                + "set job_key = ?::uuid , "
                + "status=0 , "
                + "start_time=now() , "
                + "udt=now() , "
                + "should_finish_time = ? "
                + "where id in "
                + "(select id from schedule_job where "
                + "status=0 and "
                + "job_runner= ? and "
                + "job_name <> 'digipages.BooksHandler.BookUpgraderHandler' and "
                + "should_start_time < now() "
                + "and job_key is null "
                + "limit ? ) ");
		
		//strange situation: update jobCount is always 0, which prevent job from recover
//		if (jobCount==0)
//			return jobs;
		
		jobs=postgres.createQuery(
				"SELECT sj FROM ScheduleJob sj WHERE sj.uuid = :uuid and sj.status=0"
				,ScheduleJob.class)
		.setParameter("uuid", java.util.UUID.fromString(jobKey)).getResultList();
		postgres.close();
		return jobs;
	}
	
	public static List<ScheduleJob> takeBookUpgraderJobsByRunner(String jobKey, int runner, int jobCnt)
	{
	    
	    //有重複執行轉檔的情況發生
	    
		Date shouldEndTime = calcShouldEndTime(RunMode.getValue(runner));
		List<ScheduleJob> jobs = new ArrayList<>();
		EntityManager postgres=emFactory.createEntityManager();
		postgres.getTransaction().begin();
		
		postgres.createNativeQuery(
				"update schedule_job sj "
				+ "set job_key = ?::uuid , "
				+ "status=0 , "
				+ "start_time=now() , "
				+ "udt=now() , "
				+ "should_finish_time = ? "
				+ "where id in "
				+ "(select id from schedule_job where "
				+ "status=0 and "
				+ "job_runner= ? and "
				+ "job_name = 'digipages.BooksHandler.BookUpgraderHandler' and "
				+ "should_start_time < now() "
				+ "and job_key is null "
				+ "limit ? ) ")
		.setParameter(1, jobKey)
		.setParameter(2, shouldEndTime)
		.setParameter(3, runner)
		.setParameter(4, jobCnt).executeUpdate();

		postgres.getTransaction().commit();
		
		//strange situation: update jobCount is always 0, which prevent job from recover
//		if (jobCount==0)
//			return jobs;
		
		jobs=postgres.createQuery(
				"SELECT sj FROM ScheduleJob sj WHERE sj.uuid = :uuid and sj.status=0"
				,ScheduleJob.class)
		.setParameter("uuid", java.util.UUID.fromString(jobKey)).getResultList();
		postgres.close();
		return jobs;
	}
	
	
	public static int countPendingJobsByRunner(int runner)
	{
		EntityManager postgres=emFactory.createEntityManager();
		Object o = postgres.createNativeQuery(
				"select count(*) from schedule_job "
				+ "where status=0 and retry_cnt<3 and job_runner= ?  and should_start_time < now()")
			.setParameter(1, runner)
			.getSingleResult();
		postgres.close();
		return ((Long)o).intValue();
	}
	
	public static int countBookUpgraderJobsByRunner()
	{
		EntityManager postgres=emFactory.createEntityManager();
		Object o = postgres.createNativeQuery(
				"select count(*) from schedule_job "
				+ "where (status = 1) and job_name = 'digipages.BooksHandler.BookUpgraderHandler' ")
			.getSingleResult();
		postgres.close();
		return ((Long)o).intValue();
	}
	
	static private Date calcShouldEndTime(RunMode mode) {
		switch (mode)
		{
		case LambdaMode:
				return CommonUtil.nextNMinutes(6);
		case LocalMode:
				return CommonUtil.nextNMinutes(600); // 600 minutes = 10 hrs
			default:
				return CommonUtil.nextNMinutes(31);
		}

	}

	/**
	 * 
	 * @param postgres
	 * @param jobs: at lease id, status, if error => err message must apply
	 */
	public static void finishJobs(List<ScheduleJob> jobs)
	{
		// first update finish time and status
		Date now = new Date();
		for (ScheduleJob job:jobs)
		{
			job.setFinishTime(now);
			job.setStatus(9);
		}
		

		
		EntityManager postgres=emFactory.createEntityManager();
		postgres.getTransaction().begin();
		for (ScheduleJob tmpJob:jobs)
		{
			ScheduleJob xJob=postgres.find(ScheduleJob.class, tmpJob.getId());
			tmpJob.setLastUpdated(new Date());
			xJob = xJob.mergeScheduleJob(tmpJob);
			postgres.persist(xJob);
		}
		postgres.getTransaction().commit();
		postgres.close();
	}

	public static void finishJob(Long jobId) throws Exception
	{
		if (null==emFactory)
		{
			throw new Exception("UnImplement function.");
		}else
		{
			_finishJob(jobId);
		}
	}
	
	private static void _finishJob(Long jobId)
	{
		EntityManager postgres=emFactory.createEntityManager();
		// merged.
		ScheduleJob scheduleJob =postgres.find(ScheduleJob.class, jobId);
		postgres.detach(scheduleJob);
		postgres.close();
		scheduleJob.setStatus(9);
		scheduleJob.setLastUpdated(new Date());
		updateJob(scheduleJob);
	}
	
	
	
	public static void updateJobs(List<ScheduleJob> jobs) {
		if (null==emFactory)
		{
			JobStatusReporter.updateJobsRetry(jobs);
		}else{
			_updateJobs(jobs);
		}
	}
	
	private static void _updateJobs(List<ScheduleJob> jobs) {
		EntityManager postgres=emFactory.createEntityManager();
		// merged.
		try { 
			postgres.getTransaction().begin();
			for (ScheduleJob job:jobs){
				ScheduleJob dbJob =postgres.find(ScheduleJob.class, job.getId());
				job.setLastUpdated(new Date());
//				logger.error("Eric Debug input job:" + gson.toJson(job));
				dbJob.mergeScheduleJob(job);
//				logger.error("Eric Debug writing job:" + gson.toJson(dbJob));
				postgres.persist(dbJob);
				notifyWorkers(dbJob);
			}
			postgres.getTransaction().commit();
			
//			for (ScheduleJob job:jobs)
//			{
//				notifyWorkers(job);
//			}
			
		} catch (Exception e)
		{
			logger.error("Error finish Job",e);
		}
		finally {
			if (postgres.getTransaction().isActive())
			{
				postgres.getTransaction().rollback();
			}
			postgres.close();
		}
		
	}

	public static void updateJob(ScheduleJob job) {
		if (job==null)
			return;
		if (null==emFactory)
		{
			JobStatusReporter.updateJobRetry(job);
		}else{
			_updateJob(job);
		}
	}

	
	private static void _updateJob(ScheduleJob job) {
		EntityManager postgres=emFactory.createEntityManager();
		// merged.
		ScheduleJob job1 =postgres.find(ScheduleJob.class, job.getId());
		try { 
			postgres.getTransaction().begin();
			job.setLastUpdated(new Date());
			job1.mergeScheduleJob(job);
			if(job.getJobName().contains("MediabookResultNotifyHandler")) {
			    job1.setRetryCnt(job.getRetryCnt());
			}
			postgres.persist(job1);
			postgres.getTransaction().commit();
			notifyWorkers(job1);
		} catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Error finish Job",e);
		}
		finally {
			if (postgres.getTransaction().isActive())
			{
				postgres.getTransaction().rollback();
			}
			postgres.close();
		}
	}

	
	public static void finishJob(ScheduleJob scheduleJob) {
		if (null==emFactory)
		{
			JobStatusReporter.findishJobRetry(scheduleJob);
		}else
		{
			_finishJob(scheduleJob);
		}
	}
	/**
	 * mark job as finished.
	 * @param scheduleJob
	 */
	private static void _finishJob(ScheduleJob scheduleJob) {
		scheduleJob.setStatus(9);
		scheduleJob.setLastUpdated(new Date());
		scheduleJob.setFinishTime(new Date());
		updateJob(scheduleJob);
	}

	public static void setConfig(Properties config2) {
		ScheduleJobManager.config = config2;
		JobStatusReporter.setBaseURI(config.getProperty("ScheduleJobManagerURL"));

	}

	/**
	 * only keep recent 3 months jobs
	 * @param postgres
	 */
	public static void cleanOldJobs(EntityManager postgres) {
		ScheduleJob.cleanOldJobs(postgres);
	}

	
	public static ScheduleJob getJobById(Long id) throws IOException {
		if (null==emFactory)
		{
			return JobStatusReporter.getJobById(id).get(0);
		}else{
			return _getJobById(id);
		}
	}
	/**
	 * 
	 * @param id 
	 * @return
	 */
	public static ScheduleJob _getJobById(Long id) {
		EntityManager postgres=emFactory.createEntityManager();
		// merged.
		ScheduleJob scheduleJob =postgres.find(ScheduleJob.class, id);
		postgres.detach(scheduleJob);
		postgres.close();
		
		return scheduleJob;
	}



}
