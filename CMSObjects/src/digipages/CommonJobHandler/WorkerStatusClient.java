package digipages.CommonJobHandler;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.gson.Gson;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import model.WorkerStatus;

public class WorkerStatusClient {
	private static String baseURI = "http://localhost:8080/CMSAPIBook/WorkerStatus";
	private static final  DPLogger logger = DPLogger.getLogger(WorkerStatusClient.class.getName());
	
	public WorkerStatusClient()
	{
		
	}
	
	public static String updateStatusRetry(WorkerStatus statuses){
		List <WorkerStatus> list = new ArrayList<>();
		list.add(statuses);
		return updateStatusRetry(list);
	}
	
	public static String updateStatusRetry(List<WorkerStatus> statuses){
		
		int runCnt=0;
		while (runCnt<5){
			try {
				return updateStatus(statuses);
			}catch (Exception e)
			{
				logger.info("URI = "+baseURI+"\tRetrying get Job.");
			}
			runCnt++;
			try {
				//random wait for 5 - 10 Sec 
				Thread.sleep(5000L + new Random().nextInt(5000));
			} catch (InterruptedException e) {
			} 
		}
		logger.warn("URI = "+baseURI+"\tRetry failed");
		return null;
	}
	
	/**
	 * update Status of workers
	 * @param statuses
	 * @return
	 * @throws IOException
	 */
	public static String updateStatus(List <WorkerStatus> statuses) throws IOException
	{

        return submitStatus("PUT",statuses);

	}
	
	public static String addStatus(List <WorkerStatus> statuses) throws IOException
	{
       
        
        return submitStatus("POST",statuses);
		
	}
	
	public static String submitStatus(String method, List <WorkerStatus> statuses) throws IOException
	{
        URL obj = new URL(baseURI);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod(method);
        connection.setRequestProperty("Content-Type", "application/json");
        Gson gson = new Gson();
        String urlParameters = URLEncoder.encode(gson.toJson(statuses),"UTF-8");
        connection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int responseCode = connection.getResponseCode();
        logger.info( "URI = "+baseURI+"\tResponse Code:" + responseCode + " method=" + method);

        String response = CommonUtil.getResponseFromConnection(connection);
        logger.info("URI = "+baseURI+"\tresponse:" + response);
        
        connection.disconnect();
        
        return response;
		
	}

	public static String getBaseURI() {
		return baseURI;
	}

	public static void setBaseURI(String baseURI) {
		WorkerStatusClient.baseURI = baseURI;
	}
	
	
	

}
