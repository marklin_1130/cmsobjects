package digipages.AppHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import digipages.BooksHandler.BookDrmDeleteNotifyHandler;
import digipages.BooksHandler.MemberDrmHandler;
import digipages.common.CommonUtil;
import digipages.common.DRMLogicUtility;
import digipages.exceptions.ServerException;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import digipages.info.ExtMediaBookInfo;
import model.BookInfo;
import model.DRMInfo;
import model.EplanPackageTime;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;

public class ReadListHandler {
	private Member member;
	private Integer offSet = 0;
	private Integer pageSize = 100;
	private String sortOrder = "";
	private String readlistFilter = "";
	private String isReadFilter = "";
	private String buyOutFilter = "";
	private String isBuyOutFilter = "";
	private boolean isNocustomFilter =false;
	private String s3PublicBucket = "";
	private String booksServerNotifyUrl = "";
    private String drmDelNotification = "";
    private String clientId ,clientSecret;
    private List<String> mainsubArray;
    private String isBuyOut = "";

	public ReadListServletData getNormalReaderReadList(EntityManager postgres) throws ServerException {
		ReadListServletData ret = new ReadListServletData();

		String queryString = "";
		String countQueryString = "";

		QueryStringResult resultQuery = normalQueryString(member, sortOrder, readlistFilter, isReadFilter, buyOutFilter, isBuyOutFilter, offSet, pageSize);

		queryString = resultQuery.getQueryString();
		countQueryString = resultQuery.getCountQueryString();

		@SuppressWarnings("unchecked")
		List<MemberBook> books = postgres.createNativeQuery(queryString, MemberBook.class).setHint("javax.persistence.cache.storeMode", "REFRESH").getResultList();

		int total_count = 0;

		try {
			total_count = (int) (long) postgres.createNativeQuery(countQueryString).getSingleResult();
		} catch (NoResultException e) {
			total_count = 0;
		}

		int deletedCount = 0;

		MemberDrmHandler memberDrmHandler = new MemberDrmHandler();
		BookDrmDeleteNotifyHandler bookDrmDeleteNotifyHandler = new BookDrmDeleteNotifyHandler(booksServerNotifyUrl,drmDelNotification,clientId ,clientSecret);
		if (total_count > 0) {
			books:
			for (MemberBook tmpMb : books) {
				DRMInfo drmInfo = new DRMInfo();
				List<String> eplanids = new ArrayList<String>();
				boolean isDeleted = false;
				boolean isDeleteAllEplanDrm = false; //是否有刪除月租授權，且將所有月租授權刪除
				String isBuyoutValue = "Y";
				String eplanExpireDate = null;
				String drmReadExpireTime = "";
				
				if (!tmpMb.getIsTrial()) {
					if ("serial".equalsIgnoreCase(tmpMb.getBookType())) {
						postgres.refresh(tmpMb);
					}
			        List<MemberDrmLog> drmList = new ArrayList<MemberDrmLog>();
					MemberDrmLog memberDrmLog = DRMLogicUtility.getLongestMemberDrmLogAndList(tmpMb.getMemberDrmLogs() , drmList);

					if (memberDrmLog != null) {
						drmReadExpireTime = memberDrmLog.getReadExpireTime();
						String readExpireTime = CommonUtil.normalizeDateTime(memberDrmLog.getReadExpireTime());
						drmInfo.setRead_end_time(readExpireTime);
						drmInfo.setDrm_type(String.valueOf(memberDrmLog.getType()));
						isDeleted = memberDrmHandler.delDRMifExpire(postgres, memberDrmLog.getId());
						if(isDeleted) {
							//最大授權過期，整本書及授權皆須刪除，則不用再檢查月租授權
						    bookDrmDeleteNotifyHandler.addNotifyMemberDrmlog(memberDrmLog);
						}else {
							//檢查是否有月租授權過期，因還有授權沒有過期，所以不能整本書刪除(因最大授權沒有過期，所以不能整本書刪除，只刪除該授權及mapping)
							isDeleteAllEplanDrm = memberDrmHandler.delEplanDRMifExpire(postgres, tmpMb, drmList);
						}
						
						if(6 == memberDrmLog.getType()) {
							isBuyoutValue = "N";
						}
						
						//放入書籍有效(沒有過期的)的月租包授權eplanId 
						Date now = new Date();
						for (MemberDrmLog memberDrmLogItem : drmList) {
							if(memberDrmLogItem.getType()==6 && CommonUtil.normalizeGetDateTime(memberDrmLogItem.getReadExpireTime()).after(now)  ) {
								eplanids.add(memberDrmLogItem.getSubmitId());
							}
						}
						
					} else {
						drmInfo.setRead_end_time(StringUtils.EMPTY);
						drmInfo.setDrm_type(StringUtils.EMPTY);
						try {
    						postgres.getTransaction().begin();
    						tmpMb.markAsDelete();
                            postgres.persist(tmpMb);
                            postgres.getTransaction().commit();
                            isDeleted = true;                            
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
					}
					
					//取得該本書籍的 月租書籍指定時間 (eplan_package_time) , 如果有的話
					try {
		            	EplanPackageTime eplanPackageTime = 
		            			postgres.createNamedQuery("EplanPackageTime.findByEplanidAndItemId", EplanPackageTime.class)
		                        .setParameter("eplanid", "")
		                        .setParameter("itemId", tmpMb.getItem().getId())
		                        .getSingleResult();
		            	
		            	//is_always_eplan = N 時才要檢查expire_date, Y時表示沒有從月租移除的打算
		            	if("N".equals(eplanPackageTime.getIsAlwaysEplan())){
		            		eplanExpireDate = CommonUtil.getDateToString(eplanPackageTime.getEplanExpireDate());
		            	}
		            	
	            	} catch (NoResultException ex) {
	            		//該本書籍沒有月租書籍指定時間
	            		eplanExpireDate = null;
	            	}
				}

				if (isDeleted) {
					postgres.refresh(tmpMb);
					deletedCount++;
				}else if ("n".equals(isBuyOut) && isDeleteAllEplanDrm) {
					//查詢月租書籍時，已同時將該書籍的所有月租授權回收，則必須移除此筆資料
					postgres.refresh(tmpMb);
					total_count += -1;
					deletedCount++;
				} else {
					//查詢月租書籍(非買斷)，且該本書籍有月租書籍指定時間 (eplan_package_time)時
					if("n".equals(isBuyOut) && eplanExpireDate != null ) {
						//若月租書籍指定時間已過期，則不回傳該本書籍資訊
						if(CommonUtil.getDateToString(new Date()).compareTo(eplanExpireDate) > 0){
							total_count += -1;
							continue;
						}
						//若月租書籍指定時間小於授權期限，則將授權期限改為月租書籍指定時間
						if(eplanExpireDate.compareTo(drmReadExpireTime) < 0) {
							drmInfo.setRead_end_time(CommonUtil.normalizeDateTime(eplanExpireDate));
						}
					}
					
					ReadListServletRecordData rd = getReadListRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), drmInfo, eplanids);
					rd.getItem_info().setIsbuyout(isBuyoutValue);
					ret.addRecord(rd);
				}
			}
		}
		new Thread(bookDrmDeleteNotifyHandler).start();
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	private ReadListServletRecordData getReadListRecordData(MemberBook tmpMemberBook, ItemInfo info, DRMInfo drmInfo, List<String> eplanids) {
		ReadListServletRecordData rd = new ReadListServletRecordData();

		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;
		Date finishTime = null;

		if (null == tmpMemberBook.getStartReadTime()) {
			startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (null == tmpMemberBook.getLastReadTime()) {
			lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}
		
		if (null == tmpMemberBook.getFinishTime()) {
			finishTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (tmpMemberBook.getBookType().toLowerCase().equals("book") || tmpMemberBook.getBookType().toLowerCase().equals("serial") && info instanceof BookInfo) {
			String bookType = "book";

			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);
			
			bInfo.setSize(tmpMemberBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));

			bInfo.setReadListInfo(member, tmpMemberBook, bookType, startReadTime, lastReadTime,finishTime);
			bInfo.setDrm_info(drmInfo);
			bInfo.setEplanids(eplanids);
			bInfo.setToc(null);
			bInfo.setIntro(null);
			bInfo.setForeword(null);
			rd.setItem_info(bInfo);
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("magazine")) {
			String bookType = "magazine";

			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			mInfo.setSize(tmpMemberBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			mInfo.setReadListInfo(member, tmpMemberBook, bookType, startReadTime, lastReadTime,finishTime);
			mInfo.setDrm_info(drmInfo);
			mInfo.setEplanids(eplanids);
			mInfo.setToc(null);
			rd.setItem_info(mInfo);
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("mediabook") ) {
            String bookType = "mediabook";

            MediaBookInfo mediaBookInfo = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mediaBookInfo), ExtMediaBookInfo.class);

            mInfo.setBookGroup(null);
            mInfo.setO_title(null);
            mInfo.setAnnotation_flag(null);
            
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
            mInfo.setReadListInfo(member, tmpMemberBook, bookType, startReadTime, lastReadTime,finishTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setEplanids(eplanids);
            mInfo.setToc(null);
            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        } else if (tmpMemberBook.getBookType().toLowerCase().equals("audiobook")) {
            String bookType = "audiobook";

            MediaBookInfo mediaBookInfo = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mediaBookInfo), ExtMediaBookInfo.class);

            mInfo.setBookGroup(null);
            mInfo.setO_title(null);
            mInfo.setAnnotation_flag(null);
            
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
            mInfo.setReadListInfo(member, tmpMemberBook, bookType, startReadTime, lastReadTime,finishTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setEplanids(eplanids);
            mInfo.setToc(null);
            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        } else if (info instanceof MediaBookInfo && tmpMemberBook.getBookType().toLowerCase().equals("serial")) {
            String bookType ="mediabook";
            if(tmpMemberBook.getItem().getId().contains("E07")) {
                 bookType = "audiobook";
            }else {
                 bookType = "mediabook";
            }
            
            MediaBookInfo mediaBookInfo = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mediaBookInfo), ExtMediaBookInfo.class);

            mInfo.setBookGroup(null);
            mInfo.setO_title(null);
            mInfo.setAnnotation_flag(null);
            
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
            mInfo.setReadListInfo(member, tmpMemberBook, bookType, startReadTime, lastReadTime,finishTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setEplanids(eplanids);
            mInfo.setToc(null);
            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        }

		return rd;
	}

	private QueryStringResult normalQueryString(Member member, String sortOrder, String readlistFilter, String isReadFilter, String buyOutFilter, String isBuyoutFilter, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";
		String categoryFilter = "";
		String nocustomFilter = "";
		if(!mainsubArray.isEmpty()) {
			for (String mainsub : mainsubArray) {
				categoryFilter += " OR it.info ->> 'category' like '"+mainsub+"%' "; 
			}
		}
		if(isNocustomFilter) {
			nocustomFilter = " and array_to_string(mb.readlist_idnames,'','') not like '%custom%' ";
		}
		if(mainsubArray.size()>1) {
			categoryFilter = categoryFilter.replaceFirst(" OR it.info", " AND (it.info")+")";
		}else {
			categoryFilter = categoryFilter.replaceFirst(" OR it.info", " AND it.info");
		}
		
		
//		 1.最近閱讀 ReadTimeDesc 2.購買時間（新到舊）TimeDesc 3,購買時間（舊到新）TimeAsc 4.閱讀進度（高到低）ProgressDesc 5.閱讀進度（低到高）6.書名 Name 7.作者 Author 8.出版社 Publisher 9. 檔案大小（大到小）SizeDesc 10. 檔案大小（小到大）SizeAsc
		switch (sortOrder) {
			case "default":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY date_trunc('second',mb2.create_time) DESC NULLS LAST, it2.c_title COLLATE \"C\" ASC, it2.author COLLATE \"C\" ASC, it2.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "timedesc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY date_trunc('second',mb2.create_time) DESC NULLS LAST, it2.c_title COLLATE \"C\" ASC, it2.author COLLATE \"C\" ASC, it2.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "timeasc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY date_trunc('second',mb2.create_time) ASC NULLS LAST, it2.c_title COLLATE \"C\" DESC, it2.author COLLATE \"C\" DESC, it2.publisher_name COLLATE \"C\" DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "name":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY it2.c_title COLLATE \"C\" NULLS FIRST, date_trunc('second',mb2.create_time) DESC, it2.author COLLATE \"C\" ASC, it2.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "publisher":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY it2.publisher_name COLLATE \"C\" NULLS FIRST, date_trunc('second',mb2.create_time) DESC, it2.c_title COLLATE \"C\" ASC, it2.author COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "author":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY it2.author COLLATE \"C\" NULLS FIRST, date_trunc('second',mb2.create_time) DESC, it2.c_title COLLATE \"C\" ASC, it2.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "readtimedesc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY date_trunc('second',mb2.last_read_time) DESC NULLS LAST, it2.c_title COLLATE \"C\" ASC, it2.author COLLATE \"C\" ASC, it2.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "readtimeasc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY date_trunc('second',mb2.last_read_time) ASC NULLS LAST, it2.c_title COLLATE \"C\" DESC, it2.author COLLATE \"C\" DESC, it2.publisher_name COLLATE \"C\" DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "unreadtimedesc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " AND mb.start_read_time IS NULL"
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY date_trunc('second',mb2.create_time) DESC NULLS LAST, it2.c_title COLLATE \"C\" ASC, it2.author COLLATE \"C\" ASC, it2.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " AND mb.start_read_time IS NULL"
						+ " group by mb.id,mdl.id)";
				break;
			case "progressdesc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY mb2.percentage DESC NULLS LAST"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "progressasc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY mb2.percentage ASC NULLS FIRST"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "sizedesc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN book_file bf2 ON mb2.book_file_id = bf2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY bf2.size DESC NULLS LAST"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "sizeasc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN book_file bf2 ON mb2.book_file_id = bf2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY bf2.size ASC NULLS FIRST"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "pubdatedesc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY it2.info -> 'publish_date' DESC, it2.id DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			case "pubdateasc":
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY it2.info -> 'publish_date' ASC, it2.id DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
				break;
			default:
				queryString = "SELECT mb2.* FROM member_book mb2 "
						+ " INNER JOIN item it2 ON mb2.item_id = it2.id "
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)" 
						+ " ORDER BY date_trunc('second',mb2.create_time) DESC NULLS LAST, it2.c_title COLLATE \"C\" ASC, it2.author COLLATE \"C\" ASC, it2.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb2"
						+ " where mb2.id in ( "
						+ " SELECT mb.id FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " LEFT JOIN MEMBER_BOOK_DRM_MAPPING MBDM ON MB.MEMBER_ID = MBDM.MEMBER_ID AND MB.ID =MBDM.MEMBER_BOOK_ID "
						+ " LEFT JOIN MEMBER_DRM_LOG MDL ON MBDM.MEMBER_BOOK_DRM_LOG_ID = MDL.ID"
						+ buyOutFilter
						+ " WHERE bf.status = 9"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND mb.action = 'update'"
						+ " AND mb.is_hidden = false"
						+ readlistFilter
						+ isReadFilter
						+ nocustomFilter
						+ categoryFilter
						+ isBuyoutFilter
						+ " group by mb.id,mdl.id)";
		}

		return new QueryStringResult(queryString, countQueryString);
	}

	public static String getLineInfo() {
		StackTraceElement ste = new Throwable().getStackTrace()[1];
		return ste.getFileName() + ": Line " + ste.getLineNumber() + "/";
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Integer getOffSet() {
		return offSet;
	}

	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getReadlistFilter() {
		return readlistFilter;
	}

	public void setReadlistFilter(String readlistFilter) {
		this.readlistFilter = readlistFilter;
	}

	public String getIsReadFilter() {
		return isReadFilter;
	}

	public void setIsReadFilter(String isReadFilter) {
		this.isReadFilter = isReadFilter;
	}
	
	public String getBuyOutFilter() {
		return buyOutFilter;
	}

	public void setBuyOutFilter(String buyOutFilter) {
		this.buyOutFilter = buyOutFilter;
	}

	public String getIsBuyOutFilter() {
		return isBuyOutFilter;
	}

	public void setIsBuyOutFilter(String isBuyOutFilter) {
		this.isBuyOutFilter = isBuyOutFilter;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
		this.s3PublicBucket = s3PublicBucket;
	}

    public String getBooksServerNotifyUrl() {
        return booksServerNotifyUrl;
    }

    public void setBooksServerNotifyUrl(String booksServerNotifyUrl) {
        this.booksServerNotifyUrl = booksServerNotifyUrl;
    }

    public String getDrmDelNotification() {
        return drmDelNotification;
    }

    public void setDrmDelNotification(String drmDelNotification) {
        this.drmDelNotification = drmDelNotification;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

	public boolean isNocustomFilter() {
		return isNocustomFilter;
	}

	public void setNocustomFilter(boolean isNocustomFilter) {
		this.isNocustomFilter = isNocustomFilter;
	}

	public List<String> getMainsubArray() {
		return mainsubArray;
	}

	public void setMainsubArray(List<String> mainsubArray) {
		this.mainsubArray = mainsubArray;
	}

	public String getIsBuyOut() {
		return isBuyOut;
	}

	public void setIsBuyOut(String isBuyOut) {
		this.isBuyOut = isBuyOut;
	}

}

class ReadListServletResultError {
	String error_code;
	String error_message;

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}

class ReadListServletRecordData {
	String book_uni_id;
	String item_type;
	String updated_time;
	ItemInfo item_info;

	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ItemInfo getItem_info() {
		return item_info;
	}

	public void setItem_info(ItemInfo item_info) {
		this.item_info = item_info;
	}
}

class ReadListServletData {
	Integer total_records = 100;
	Integer current_offset = 0;
	String updated_time;
	ArrayList<ReadListServletRecordData> records = new ArrayList<>();

	public Integer getTotal_records() {
		return total_records;
	}

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public Integer getCurrent_offset() {
		return current_offset;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ArrayList<ReadListServletRecordData> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<ReadListServletRecordData> records) {
		this.records = records;
	}

	public void addRecord(ReadListServletRecordData record) {
		records.add(record);
	}
}

class ReadListServletDataNone {
	Integer total_records = 0;
	Integer current_offset = 0;
	String updated_time;
	List<String> records;

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public void addRecord(List<String> record) {
		this.records = record;
	}
}