package digipages.AppHandler;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import digipages.common.CommonUtil;
import digipages.common.DRMLogicUtility;
import digipages.common.StringHelper;
import digipages.exceptions.ServerException;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import model.BookFile;
import model.BookInfo;
import model.DRMInfo;
import model.ItemInfo;
import model.MagInfo;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;

public class SearchMainSiteBookHandler extends SearchBookHandler {
	private Member member;
	private Integer offSet = 0;
	private Integer pageSize = 100;
	private String sortOrder = "";
	private String searchType = "";
	private String searchKeyword = "";
	private List<String> searchParameter;
	private String searchMainSiteTotal;
	private String simpleMode = "";
	private String listName = "";
	private String readlistFilter = "";
	private String s3PublicBucket = "";
	private boolean isApp ;
	private String searchApiUrl;
	private String searchApiKey;
	
	public List<String> getMainSiteSearchResult(String searchKeyWord, Integer page , Integer pageCount) throws Exception{
		String responseStr = null;
		
		RequestConfig defaultRequestConfig = RequestConfig.custom()
			    .setSocketTimeout(10000)
			    .setConnectTimeout(10000)
			    .setConnectionRequestTimeout(10000)
			    .build();
		
		CloseableHttpClient httpclient = HttpClients.custom()
			    .setDefaultRequestConfig(defaultRequestConfig).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
			    .build();
		
		HttpUriRequest memberMe = RequestBuilder.get()
				.setUri(new URI(searchApiUrl))
				.setHeader("x-api-key", searchApiKey)
				.addParameter("key", searchKeyWord)
				.addParameter("page", String.valueOf(page))
				.addParameter("pagecount",  String.valueOf(pageCount))
				.build();
		CloseableHttpResponse response = httpclient.execute(memberMe);
		responseStr = CommonUtil.responseToStr(response);

		JsonObject returnData = new JsonParser().parse(responseStr).getAsJsonObject();
		
		JsonObject responseBody = returnData.getAsJsonObject("responseBody");
		
		searchMainSiteTotal = responseBody.get("total").getAsString();
		JsonArray prods =  responseBody.getAsJsonArray("prods");
		searchParameter = new ArrayList<String>();
		for (JsonElement jsonElement : prods) {
			searchParameter.add(StringHelper.escapeSQL(jsonElement.getAsString()));
		}
		return searchParameter;
	}
	
	public Object getSearchBook(EntityManager postgres) throws ServerException {
		
		try {
			int page = 1;
			if(offSet>0) {
				page = (int) Math.ceil(Double.valueOf(offSet) / Double.valueOf(pageSize));
				if(offSet == pageSize) {
					page++;
				}
			}
			searchParameter = getMainSiteSearchResult(searchKeyword,page,pageSize);
		} catch (Exception e) {
			//當發生錯誤回傳沒有資料的結果，改執行CMS搜尋
			SearchBookServletData ret = new SearchBookServletData();
			ret.setSearch_keyword(searchKeyword);
			ret.setTotal_records(0);
			ret.setCurrent_offset(offSet);
			ret.setUpdated_time(CommonUtil.zonedTime());
			return ret;
		}
		
		Object f = null;
		if (member.isThirdParty()) {
			// 博客來帳號/出版社帳號/供應商帳號
			if ("trial".equals(searchType)) {
				// 更多試閱
				if ("*".equals(searchKeyword)) {
					// 搜尋關鍵字等於"*"時,更多試閱列出所有供應商允許的書籍(包含試閱本),根據上架時間desc排序,不用過濾隱藏/密碼/封存(此時一般書單不列出內容)
					f = getThirdPartyAllBook(postgres);
				} else {
					// 搜尋關鍵字不等於"*"時,更多試閱列出符合關鍵字且供應商允許的書籍(包含試閱本),根據上架時間desc排序,不用過濾隱藏/密碼/封存(此時一般書單不列出內容)
					f = getThirdPartySearchAllBook(postgres);
				}
			} else if ("readlist".equals(searchType) && ("all".equals(listName) || "trial".equals(listName) || "archive".equals(listName))) {
				// 我的書籍(一般書單)
				if ("*".equals(searchKeyword)) {
					// 搜尋關鍵字等於"*"時,一般書單不列出內容(已購買書籍,試閱書單,封存書單)
					f = getSearchBookDataNone(postgres);
				} else {
					// 搜尋關鍵字不等於"*"時,一般書單列出內容(已購買書籍,試閱書單,封存書單)
					f = getThirdPartyMemberBook(postgres);
				}
			} else {
				f = getSearchBookDataNone(postgres);
			}
		} else if (member.isNormalReader()) {
			if ("trial".equals(searchType)) {
				// 更多試閱
//				if(isApp) {
//					f = getNormalReaderMoreTrialBookAndFreeBook(postgres);
//				}else {
					f = getNormalReaderMoreTrialBook(postgres);	
//				}
			}else if ("free".equalsIgnoreCase(searchType)) {
				// 免費領用
				f = getNormalReaderFreeBook(postgres);
			} else {
				// 我的書籍(已購買書籍,試閱書單,封存書單)
				f = getNormalReaderMemberBook(postgres);
			}
		} else if (member.isGuest()) {
			// 只顯示試閱書籍
			if(isApp) {
//				f = getGuestMoreTrialAndFreeBook(postgres);
				f = getGuestMoreTrialBook(postgres);
			}else {
				f = getGuestMoreTrialBook(postgres);
			}
		}

		return f;
	}

	public SearchBookServletData getThirdPartyMemberBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = thirdPartyQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		// get MemberBook
		List<MemberBook> books = listMemberBook(postgres, resultQuery, searchParameter);

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchParameter);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
            for (MemberBook tmpMb : books) {
                SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
                rd.getItem_info().setEplanids(getEplanInfoList(postgres, member.getId(), tmpMb.getItem().getId()));
                ret.addRecord(rd);
            }
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getThirdPartyAllBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = thirdPartyAllBookQueryString(member, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listAllBookFile(postgres, resultQuery);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countAllBookFile(postgres, resultQuery);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
            for (MemberBook tmpMb : books) {
                SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
                ret.addRecord(rd);
            }
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getThirdPartySearchAllBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = thirdPartySearchTrialBookQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listBookFile(postgres, resultQuery, searchParameter);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchParameter);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
            for (MemberBook tmpMb : books) {
                SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
                ret.addRecord(rd);
            }
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getNormalReaderMemberBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = normalQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		// get MemberBook
		List<MemberBook> books = listMemberBook(postgres, resultQuery, searchParameter);

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchParameter);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
            for (MemberBook tmpMb : books) {
                SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
                rd.getItem_info().setEplanids(getEplanInfoList(postgres, member.getId(), tmpMb.getItem().getId()));
                ret.addRecord(rd);
            }
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(Integer.valueOf(searchMainSiteTotal));
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}
	
	public SearchBookServletData getNormalReaderMoreTrialBookAndFreeBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = normalSearchTrialAndFreeBookQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listTrialAndFreeBookFile(postgres, resultQuery, searchParameter);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countTrialAndFreeBookFile(postgres, resultQuery, searchParameter);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
			    SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getNormalReaderMoreTrialBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = normalSearchTrialBookQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listBookFile(postgres, resultQuery, searchParameter);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchParameter);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
            for (MemberBook tmpMb : books) {
                SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
                ret.addRecord(rd);
            }
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(Integer.valueOf(searchMainSiteTotal));
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}
	
	public SearchBookServletData getGuestMoreTrialBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = guestQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listBookFile(postgres, resultQuery, searchParameter);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchParameter);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
            for (MemberBook tmpMb : books) {
                SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
                ret.addRecord(rd);
            }
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(Integer.valueOf(searchMainSiteTotal));
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}
	
	public SearchBookServletData getGuestMoreTrialAndFreeBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = guestQueryWithFreeBookString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listTrialAndFreeBookFile(postgres, resultQuery, searchParameter);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchParameter);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
            for (MemberBook tmpMb : books) {
                SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
                ret.addRecord(rd);
            }
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	private SearchMainSiteBookServletRecordData getSearchBookRecordData(MemberBook tmpMemberBook, ItemInfo info, String simpleMode) {
		SearchMainSiteBookServletRecordData rd = new SearchMainSiteBookServletRecordData();
		DRMInfo drmInfo = new DRMInfo();

        if (!tmpMemberBook.getIsTrial()&& tmpMemberBook.getMemberDrmLogs()!=null) {
            MemberDrmLog memberDrmLog = DRMLogicUtility.getLongestMemberDrmLog(tmpMemberBook.getMemberDrmLogs());

            if (memberDrmLog != null) {
                String readExpireTime = CommonUtil.normalizeDateTime(memberDrmLog.getReadExpireTime());
                drmInfo.setRead_end_time(readExpireTime);
                drmInfo.setDrm_type(String.valueOf(memberDrmLog.getType()));
            } else {
                drmInfo.setRead_end_time(StringUtils.EMPTY);
                drmInfo.setDrm_type(StringUtils.EMPTY);
            }
        }
        
		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;

		if (null == tmpMemberBook.getStartReadTime()) {
			startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (null == tmpMemberBook.getLastReadTime()) {
			lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (tmpMemberBook.getBookType().toLowerCase().equals("book") || tmpMemberBook.getBookType().toLowerCase().equals("serial")) {
			String bookType = "book";

			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);

			bInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
			bInfo.setDrm_info(drmInfo);
			bInfo.setSize(tmpMemberBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(bInfo);
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("magazine")) {
			String bookType = "magazine";

			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
			mInfo.setDrm_info(drmInfo);
			mInfo.setSize(tmpMemberBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(mInfo);
		}

		return rd;
	}
	
	private List<String> getEplanInfoList(EntityManager postgres, Long memberId, String itemId) {
		List<String> eplanids = new ArrayList<String>();
        
        @SuppressWarnings("unchecked")
		//List<String> temp = postgres.createNativeQuery("SELECT mdl.submit_id FROM member_drm_log mdl join eplan e on mdl.submit_id  = e.eplanid WHERE mdl.member_id = " + memberId + " and mdl.item_id = '" + itemId + "' and mdl.status = '1' ORDER BY mdl.submit_id ").getResultList();
		
       String eplanListQueryString = "select e.eplan_id  FROM member_drm_log mdl , member_eplan_drm_log e "
       		+ " WHERE mdl.member_id = e.member_id"
       		+ " and mdl.submit_id = e.eplan_id"
       		+ " and mdl.member_id = "+memberId
       		+ " and mdl.item_id = '"+itemId+"'"
       		+ " and mdl.type='6'"
       		+ " and mdl.status=1"
       		+ " and to_timestamp(mdl.read_expire_time ,'YYYY/MM/DD HH24:MI:Ss')  >= now() "
       		+ " and to_timestamp(e.read_expire_time ,'YYYY/MM/DD HH24:MI:Ss')  >= now() ORDER BY mdl.submit_id"; 
        
        @SuppressWarnings("unchecked")
        List<String> temp = postgres.createNativeQuery( eplanListQueryString ).getResultList();
        		
		if(temp != null) {
			for(String eplanId : temp) {
				eplanids.add(eplanId);
			}
		}
		
		return eplanids;
	}

	private List<MemberBook> listMemberBook(EntityManager postgres, QueryStringResult queryResult, List<String> searchKeyword) throws ServerException {
		String commaSeparatedString = String.join("','",searchKeyword);
		commaSeparatedString = "'"+commaSeparatedString+"'";
		@SuppressWarnings("unchecked")
		List<MemberBook> books = postgres.createNativeQuery(queryResult.getQueryString().replace("?", commaSeparatedString), MemberBook.class)
				.setHint("javax.persistence.cache.storeMode", "REFRESH")
				.getResultList();

		return books;
	}

	private List<BookFile> listAllBookFile(EntityManager postgres, QueryStringResult queryResult) throws ServerException {
		@SuppressWarnings("unchecked")
		List<BookFile> bf = postgres.createNativeQuery(queryResult.getQueryString(), BookFile.class).getResultList();

		return bf;
	}

	private int countAllBookFile(EntityManager postgres, QueryStringResult queryResult) throws ServerException {
		int total_count = 0;

		total_count = (int) (long) postgres.createNativeQuery(queryResult.getCountQueryString()).getSingleResult();

		return total_count;
	}

	private List<BookFile> listBookFile(EntityManager postgres, QueryStringResult queryResult, List<String> searchKeyword) throws ServerException {
		String commaSeparatedString = String.join("','",searchKeyword);
		commaSeparatedString = "'"+commaSeparatedString+"'";
		@SuppressWarnings("unchecked")
		List<BookFile> bf = postgres.createNativeQuery(queryResult.getQueryString().replace("?", commaSeparatedString), BookFile.class)
				.getResultList();

		return bf;
	}
	
	private List<BookFile> listTrialAndFreeBookFile(EntityManager postgres, QueryStringResult queryResult, List<String> searchKeyword) throws ServerException {
		String commaSeparatedString = String.join("','",searchKeyword);
		commaSeparatedString = "'"+commaSeparatedString+"'";
		@SuppressWarnings("unchecked")
		List<BookFile> bf = postgres.createNativeQuery(queryResult.getQueryString().replaceAll("?", commaSeparatedString), BookFile.class)
				.getResultList();

		return bf;
	}

	private int countBookFile(EntityManager postgres, QueryStringResult queryResult, List<String> searchKeyword) throws ServerException {
		int total_count = 0;
		String commaSeparatedString = String.join("','",searchKeyword);
		commaSeparatedString = "'"+commaSeparatedString+"'";
		total_count = (int) (long) postgres.createNativeQuery(queryResult.getCountQueryString().replace("?", commaSeparatedString))
				.getSingleResult();

		return total_count;
	}
	
	private int countTrialAndFreeBookFile(EntityManager postgres, QueryStringResult queryResult, List<String> searchKeyword) throws ServerException {
		int total_count = 0;
		String commaSeparatedString = String.join("','",searchKeyword);
		commaSeparatedString = "'"+commaSeparatedString+"'";
		total_count = (int) (long) postgres.createNativeQuery(queryResult.getCountQueryString().replaceAll("?", commaSeparatedString))
				.getSingleResult();

		return total_count;
	}

	private QueryStringResult thirdPartyQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";
		String thirdPartyTypeFilter = "";
		int publisher_id = 0;
		int vendor_id = 0;

		if ("PublisherReader".equals(member.getMemberType()) && null != member.getPublisher()) {
			publisher_id = member.getPublisher().getId();
			thirdPartyTypeFilter = " AND it.publisher_id = " + publisher_id;
		}

		if ("VendorReader".equals(member.getMemberType()) && null != member.getVendor()) {
			vendor_id = member.getVendor().getId();
			thirdPartyTypeFilter = " AND it.vendor_id = " + vendor_id;
		}

		queryString = "SELECT mb.* FROM member_book mb"
				+ " INNER JOIN item it ON mb.item_id = it.id"
				+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
				+ " WHERE bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND mb.member_id = " + member.getId() + ""
				+ " AND it.id in (?)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ thirdPartyTypeFilter
				+ readlistFilter
				+ " ORDER BY mb.create_time DESC NULLS LAST" // 符/數/英/中
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM member_book mb"
				+ " INNER JOIN item it ON mb.item_id = it.id"
				+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
				+ " WHERE bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND mb.member_id = " + member.getId() + ""
				+ " AND it.id in (?)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ thirdPartyTypeFilter
				+ readlistFilter;

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult thirdPartyAllBookQueryString(Member member, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";
		String thirdPartyTypeFilter = "";
		int publisher_id = 0;
		int vendor_id = 0;

		if ("PublisherReader".equals(member.getMemberType()) && null != member.getPublisher()) {
			publisher_id = member.getPublisher().getId();
			thirdPartyTypeFilter = " AND it.publisher_id = " + publisher_id;
		}

		if ("VendorReader".equals(member.getMemberType()) && null != member.getVendor()) {
			vendor_id = member.getVendor().getId();
			thirdPartyTypeFilter = " AND it.vendor_id = " + vendor_id;
		}

		queryString = "SELECT * FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.* FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " WHERE bf.version = (SELECT MAX(bf_self.version) FROM book_file bf_self WHERE bf_self.id = bf.id)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") x"
				+ " ORDER BY x.udt::varchar::timestamp DESC, x.create_time DESC, x.item_id DESC"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.udt, MAX(bf.version) OVER (partition by bf.version) AS max_version FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " WHERE (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") AS temp";

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult thirdPartySearchTrialBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";
		String thirdPartyTypeFilter = "";
		int publisher_id = 0;
		int vendor_id = 0;

		if ("PublisherReader".equals(member.getMemberType()) && null != member.getPublisher()) {
			publisher_id = member.getPublisher().getId();
			thirdPartyTypeFilter = " AND it.publisher_id = " + publisher_id;
		}

		if ("VendorReader".equals(member.getMemberType()) && null != member.getVendor()) {
			vendor_id = member.getVendor().getId();
			thirdPartyTypeFilter = " AND it.vendor_id = " + vendor_id;
		}

		queryString = "SELECT * FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.* FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " AND it.id in (?)"
				+ " AND bf.version = (SELECT MAX(bf_self.version) FROM book_file bf_self WHERE bf_self.id = bf.id)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") x"
				+ " ORDER BY x.udt::varchar::timestamp DESC, x.create_time DESC, x.item_id DESC"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.udt, MAX(bf.version) OVER (partition by bf.version) AS max_version FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " AND it.id in (?)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") AS temp";

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult normalQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		switch (sortOrder) {
			case "default":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY mb.create_time DESC NULLS LAST"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "timedesc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY mb.create_time DESC NULLS LAST, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "timeasc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY mb.create_time ASC NULLS LAST, it.c_title COLLATE \"C\" DESC, it.author COLLATE \"C\" DESC, it.publisher_name COLLATE \"C\" DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "name":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY it.c_title COLLATE \"C\" NULLS FIRST, mb.create_time DESC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "publisher":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY it.publisher_name COLLATE \"C\" NULLS FIRST, mb.create_time DESC, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "author":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY it.author COLLATE \"C\" NULLS FIRST, mb.create_time DESC, it.c_title COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "readtimedesc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY mb.last_read_time DESC NULLS LAST, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "readtimeasc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY mb.last_read_time ASC NULLS LAST, it.c_title COLLATE \"C\" DESC, it.author COLLATE \"C\" DESC, it.publisher_name COLLATE \"C\" DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
				break;
			case "unreadtimedesc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " AND mb.start_read_time IS NULL"
						+ " ORDER BY mb.last_read_time DESC NULLS LAST, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " AND mb.start_read_time IS NULL";
				break;
			default:
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter
						+ " ORDER BY mb.create_time DESC NULLS LAST, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND it.id in (?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ readlistFilter;
		}

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult normalSearchTrialAndFreeBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString ="SELECT * FROM (" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its " + 
				" ON bf.item_id = its.id " + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND action = 'update')" + 
				" AND bf.status = 9 " + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = TRUE" + 
				" UNION" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and item.id in (?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND action = 'update')" + 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
				" ) x " + 
				" ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		
		countQueryString =" SELECT COUNT(*) FROM (" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its " + 
				" ON bf.item_id = its.id" + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND action = 'update')" + 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = TRUE" + 
				" UNION" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and id in (?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND action = 'update')" + 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
				" ) x " ;

		return new QueryStringResult(queryString, countQueryString);
	}
	
	private QueryStringResult normalSearchTrialBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its ON bf.item_id = its.id"
				+ " WHERE bf.item_id NOT IN (SELECT DISTINCT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = TRUE AND action = 'update' AND is_hidden = FALSE)"
				+ " AND bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = TRUE) x"
				+ " ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its ON bf.item_id = its.id"
				+ " WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = TRUE AND action = 'update' AND is_hidden = FALSE)"
				+ " AND bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = TRUE) x";

		return new QueryStringResult(queryString, countQueryString);
	}
	
	private QueryStringResult normalSearchFreeBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM "
				+ " (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN "
				+ " (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now())) "
				+ " AND id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its"
				+ " ON bf.item_id = its.id"
				+ " WHERE bf.item_id NOT IN (SELECT DISTINCT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = FALSE AND action = 'update')"
				+ " AND bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = FALSE) x"
				+ " ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM "
				+ " (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN "
				+ " (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now())) "
				+ " WHERE id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its ON bf.item_id = its.id"
				+ " WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = FALSE AND action = 'update' )"
				+ " AND bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = FALSE) x";

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult guestQueryWithFreeBookString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ""+
			    " UNION " + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and id in (?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE 1=1 "+ 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
			    ") x"
				+ " ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ""+
			    " UNION " + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and id in (?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE 1=1 "+ 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
			    ") x";

		return new QueryStringResult(queryString, countQueryString);
	}
	

	private QueryStringResult guestQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ") x"
				+ " ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ") x";

		return new QueryStringResult(queryString, countQueryString);
	}

	public SearchBookServletDataNone getSearchBookDataNone(EntityManager postgres) throws ServerException {
		SearchBookServletDataNone f = new SearchBookServletDataNone();

		int totalCount = 0;

		f.addRecord(new ArrayList<String>());
		f.setSearch_keyword(searchKeyword);
		f.setTotal_records(totalCount);
		f.setCurrent_offset(offSet);
		f.setUpdated_time(CommonUtil.zonedTime());

		return f;
	}

	public static String getLineInfo() {
		StackTraceElement ste = new Throwable().getStackTrace()[1];
		return ste.getFileName() + ": Line " + ste.getLineNumber() + "/";
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
		super.setMember(member);
	}

	public Integer getOffSet() {
		return offSet;
	}

	public void setOffSet(Integer offSet) {
	    super.setOffSet(offSet);
		this.offSet = offSet;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
	    super.setPageSize(pageSize);
		this.pageSize = pageSize;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
	    super.setSortOrder(sortOrder);
		this.sortOrder = sortOrder;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
	    super.setSearchType(searchType);
		this.searchType = searchType;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
	    super.setSearchKeyword(searchKeyword);
		this.searchKeyword = searchKeyword;
	}

	public String getSimpleMode() {
		return simpleMode;
	}

	public void setSimpleMode(String simpleMode) {
	    super.setSimpleMode(simpleMode);
		this.simpleMode = simpleMode;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
	    super.setListName(listName);
		this.listName = listName;
	}

	public String getReadlistFilter() {
		return readlistFilter;
	}

	public void setReadlistFilter(String readlistFilter) {
	    super.setReadlistFilter(readlistFilter);
		this.readlistFilter = readlistFilter;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
	    super.setS3PublicBucket(s3PublicBucket);
		this.s3PublicBucket = s3PublicBucket;
	}

	public boolean isApp() {
		return isApp;
	}

	public void setApp(boolean isApp) {
	    super.setApp(isApp);
		this.isApp = isApp;
	}

	public String getSearchApiUrl() {
		return searchApiUrl;
	}

	public void setSearchApiUrl(String searchApiUrl) {
		this.searchApiUrl = searchApiUrl;
	}

	public String getSearchApiKey() {
		return searchApiKey;
	}

	public void setSearchApiKey(String searchApiKey) {
		this.searchApiKey = searchApiKey;
	}
}

class SearchMainSiteBookServletResultError {
	String error_code;
	String error_message;

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}

class SearchMainSiteBookServletRecordData {
	String book_uni_id;
	String item_type;
	String updated_time;
	ItemInfo item_info;

	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ItemInfo getItem_info() {
		return item_info;
	}

	public void setItem_info(ItemInfo item_info) {
		this.item_info = item_info;
	}
}


