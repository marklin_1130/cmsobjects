package digipages.AppHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.StringUtils;
import org.boon.json.JsonFactory;

import com.google.gson.Gson;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.MessageQueueParam;
import digipages.common.StringHelper;
import digipages.dev.utility.SNSMobilePush;
import digipages.exceptions.ServerException;
import digipages.snps.SNPSPublishSimple;
import digipages.snps.SNPSPublishSimpleResp;
import digipages.snps.SNPSServiceImpl;
import model.MemberDevice;
import model.Message;
import model.MessageQueue;
import model.ScheduleJob;

public class MessageQueueHandler implements CommonJobHandlerInterface {
    public static final String HANDLERNAME = "MessageQueueHandler";
    public static final String JOBGROUPNAME = "GENBENCHMEMBER";
    public static final int JOBRUNNER = 0;
    private ScheduleJob scheduleJob;
    private static final DPLogger logger = DPLogger.getLogger(MessageQueueHandler.class.getName());
    private EntityManagerFactory emf;
    private static MessageQueueHandler myInstance = new MessageQueueHandler();
    private Long messageId;
    private String ownerId = "";
    private String gcmServerAPIKey = "";
    private String gcmApplicationName = "";
    private String apnsCertificate = "";
    private String apnsPrivateKey = "";
    private String apnsApplicationName = "";
    private String dateString = "";

    private String snpsaccount = "";
    private String snpspw = "";
    private String snpsDeviceUpdate = "";
    private String snpsBoardcast = "";
    private String snpsBoardcastPayload = "";
    private String snpsAppKeyios = "";
    private String snpsAppKeyandroid = "";
    private String snpsPublish = "";
    private String snpsAppKey = "";
    private String snpsSimple = "";

    public MessageQueueHandler() {
        LocalRunner.registerHandler(this);
    }

    public static void initJobHandler() {
        // not used.
    }

    public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
        // take jobs
        List<ScheduleJob> jobs;

        ScheduleJob jobParam = new ScheduleJob();
        jobParam.setUuid(UUID.randomUUID());
        jobParam.setJobName(MessageQueueHandler.HANDLERNAME);
        jobParam.setJobGroup(MessageQueueHandler.JOBGROUPNAME);
        jobParam.setJobRunner(MessageQueueHandler.JOBRUNNER);

        jobs = ScheduleJobManager.takeJobs(jobParam, 10);

        return jobs;
    }

    public String messagePushSNPS(EntityManager postgres, Long messageId, String ownerId, String dateString) throws ServerException {
        String jsonStr = "";

        try {
            @SuppressWarnings("unchecked")
            List<MessageQueue> queues = postgres.createNativeQuery("SELECT * From message_queue" + " WHERE id = " + messageId + "" + " AND owner_id = '" + ownerId + "'" + " AND status = 0", MessageQueue.class)
                    .getResultList();

            SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
            snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);

            for (MessageQueue mq : queues) {
                try {
                    // Update Status: 0:尚未推播,1:正在推播,9:推播完成,-1:發送失敗
                    changeStatus(postgres, mq, 1);

                    String deviceIds = arrayConverter(new Gson().fromJson(mq.getTargetId(), String[].class));

                    @SuppressWarnings("unchecked")
                    List<MemberDevice> memberDevices = postgres
                            .createNativeQuery("SELECT DISTINCT ON (device_token) * FROM member_device WHERE id IN (" + StringHelper.escapeSQL(deviceIds) + ") ORDER BY device_token, udt::varchar::timestamp DESC", MemberDevice.class)
                            .getResultList();

                    for (MemberDevice memberDevice : memberDevices) {
                        try {

                            memberDevice.getMember().getBmemberId();
                            // push processor
                            SNPSPublishSimple aSNPSPublishSimple = new SNPSPublishSimple();

                            if ("ios".equalsIgnoreCase(memberDevice.getOsType())) {
                                aSNPSPublishSimple.setAppKey(snpsAppKeyios);
                            } else {
                                aSNPSPublishSimple.setAppKey(snpsAppKeyandroid);
                            }
                            aSNPSPublishSimple.setDeviceToken(memberDevice.getDeviceToken());
                            aSNPSPublishSimple.setMessage(mq.getContent());
                            SNPSPublishSimpleResp resp = snpsServiceImpl.sendSNSByToken(aSNPSPublishSimple);
                            // 裝置失效
                            if ("011".equalsIgnoreCase(resp.getReturnCode())) {
                                changeDeviceStatus(postgres, memberDevice);
                            }

                            postgres.getTransaction().begin();
                            java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(mq.getStartDate().substring(0, 10).replace('/', '-') + " " + mq.getSendTime() + ":00");
                            Message messageContent = new Message();
                            messageContent.setMember(memberDevice.getMember());
                            messageContent.setMessageId(mq.getMessageId());
                            messageContent.setDeviceId(memberDevice.getDeviceId());
                            messageContent.setContent(mq.getContent());
                            messageContent.setSendTime(nowTime);
                            messageContent.setLastUpdated(nowTime);
                            postgres.persist(messageContent);
                            postgres.getTransaction().commit();

                        } catch (Exception e) {
                            logger.error("Fail Publish:" + memberDevice.getMember().getBmemberId() + " / " + memberDevice.getDeviceId() + " at " + java.sql.Timestamp.valueOf(LocalDateTime.now()));
                        }
                    }

                    // Update Status: 0:尚未推播,1:正在推播,9:推播完成,-1:發送失敗
                    changeStatus(postgres, mq, 9);
                } catch (Exception ex) {
                    // Update Status: 0:尚未推播,1:正在推播,9:推播完成,-1:發送失敗
                    changeStatus(postgres, mq, -1);
                }
            }
        } catch (javax.persistence.RollbackException rex) {
            throw new ServerException("id_err_999", "Error Writing data to db");
        } catch (Exception e) {
            e.printStackTrace();
            String errJsonMessage = JsonFactory.toJson(e);
            throw new ServerException("id_err_999", errJsonMessage);
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        // finish jobs
        if (null != scheduleJob) {
            // datetime
            String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
            String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " "
                    + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

            try {
                ScheduleJobManager.finishJob(scheduleJob);
            } catch (Exception ex) {
                logger.error(ex);
            }
        }

        return jsonStr;
    }

    @Deprecated
    public String messagePush(EntityManager postgres, Long messageId, String ownerId, String gcmServerAPIKey, String gcmApplicationName, String apnsCertificate, String apnsPrivateKey, String apnsApplicationName,
            String dateString) throws ServerException {
        String jsonStr = "";

        try {
            @SuppressWarnings("unchecked")
            List<MessageQueue> queues = postgres.createNativeQuery("SELECT * From message_queue" + " WHERE id = " + messageId + "" + " AND owner_id = '" + ownerId + "'" + " AND status = 0", MessageQueue.class)
                    .getResultList();

            SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
            snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, "", "");
            for (MessageQueue mq : queues) {
                try {
                    // Update Status: 0:尚未推播,1:正在推播,9:推播完成,-1:發送失敗
                    changeStatus(postgres, mq, 1);

                    SNSMobilePush.push(postgres, mq, gcmServerAPIKey, gcmApplicationName + "." + dateString + "." + ownerId, "CMS", apnsCertificate, apnsPrivateKey, apnsApplicationName + "." + dateString + "." + ownerId,
                            "CMS");

                    // Update Status: 0:尚未推播,1:正在推播,9:推播完成,-1:發送失敗
                    changeStatus(postgres, mq, 9);
                } catch (Exception ex) {
                    // Update Status: 0:尚未推播,1:正在推播,9:推播完成,-1:發送失敗
                    changeStatus(postgres, mq, -1);
                }
            }
        } catch (javax.persistence.RollbackException rex) {
            throw new ServerException("id_err_999", "Error Writing data to db");
        } catch (Exception e) {
            e.printStackTrace();
            String errJsonMessage = JsonFactory.toJson(e);
            throw new ServerException("id_err_999", errJsonMessage);
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        // finish jobs
        if (null != scheduleJob) {
            // datetime
            String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
            String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " "
                    + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

            try {
                ScheduleJobManager.finishJob(scheduleJob);

            } catch (Exception ex) {
                logger.error(ex);
            }
        }

        return jsonStr;
    }

    private static boolean changeStatus(EntityManager postgres, MessageQueue tmpMessageQueues, Integer statusCode) {
        if (null != statusCode) {
            try {
                MessageQueue messageQueue = (MessageQueue) postgres.createNamedQuery("MessageQueue.findAllByMessageId").setParameter("messageId", tmpMessageQueues.getMessageId()).getSingleResult();

                messageQueue.setStatus(statusCode);

                switch (statusCode) {
                case 1:
                    messageQueue.setMsgStartTime(CommonUtil.zonedTime());
                    break;
                case 9:
                    messageQueue.setMsgEndTime(CommonUtil.zonedTime());
                    break;
                }

                postgres.getTransaction().begin();
                postgres.persist(messageQueue);
                postgres.getTransaction().commit();
            } finally {
                if (postgres.getTransaction().isActive()) {
                    postgres.getTransaction().rollback();
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public ScheduleJob getScheduleJob() {
        return scheduleJob;
    }

    public void setScheduleJob(ScheduleJob scheduleJob) {
        this.scheduleJob = scheduleJob;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getGcmServerAPIKey() {
        return gcmServerAPIKey;
    }

    public void setGcmServerAPIKey(String gcmServerAPIKey) {
        this.gcmServerAPIKey = gcmServerAPIKey;
    }

    public String getGcmApplicationName() {
        return gcmApplicationName;
    }

    public void setGcmApplicationName(String gcmApplicationName) {
        this.gcmApplicationName = gcmApplicationName;
    }

    public String getApnsCertificate() {
        return apnsCertificate;
    }

    public void setApnsCertificate(String apnsCertificate) {
        this.apnsCertificate = apnsCertificate;
    }

    public String getApnsPrivateKey() {
        return apnsPrivateKey;
    }

    public void setApnsPrivateKey(String apnsPrivateKey) {
        this.apnsPrivateKey = apnsPrivateKey;
    }

    public String getApnsApplicationName() {
        return apnsApplicationName;
    }

    public void setApnsApplicationName(String apnsApplicationName) {
        this.apnsApplicationName = apnsApplicationName;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public static MessageQueueHandler getInstance() {
        return myInstance;
    }

    @Override
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException {
        EntityManager em = emf.createEntityManager();
        MessageQueueParam jobParams = scheduleJob.getJobParam(MessageQueueParam.class);

        Long messageId = null;
        String ownerId = "";
        String gcmServerAPIKey = "";
        String gcmApplicationName = "";
        String apnsCertificate = "";
        String apnsPrivateKey = "";
        String apnsApplicationName = "";
        String dateString = "";

        if (null != jobParams.getMessage_id()) {
            messageId = jobParams.getMessage_id();
        }

        if (null != jobParams.getOwner_id()) {
            ownerId = jobParams.getOwner_id();
        }

        if (null != jobParams.getGcm_server_api_key()) {
            gcmServerAPIKey = jobParams.getGcm_server_api_key();
        }

        if (null != jobParams.getGcm_application_name()) {
            gcmApplicationName = jobParams.getGcm_application_name();
        }

        if (null != jobParams.getApns_certificate()) {
            apnsCertificate = jobParams.getApns_certificate();
        }

        if (null != jobParams.getApns_private_key()) {
            apnsPrivateKey = jobParams.getApns_private_key();
        }

        if (null != jobParams.getApns_application_name()) {
            apnsApplicationName = jobParams.getApns_application_name();
        }

        if (null != jobParams.getDate_string()) {
            dateString = jobParams.getDate_string();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsaccount())) {
            snpsaccount = jobParams.getSnpsaccount();
        }
        
        if (StringUtils.isNotBlank(jobParams.getSnpspw())) {
            snpspw = jobParams.getSnpspw();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsAppKeyandroid())) {
            snpsAppKeyandroid = jobParams.getSnpsAppKeyandroid();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsAppKeyios())) {
            snpsAppKeyios = jobParams.getSnpsAppKeyios();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsBoardcast())) {
            snpsBoardcast = jobParams.getSnpsBoardcast();
        }
        
        if (StringUtils.isNotBlank(jobParams.getSnpsBoardcastPayload())) {
            snpsBoardcastPayload = jobParams.getSnpsBoardcastPayload();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsDeviceUpdate())) {
            snpsDeviceUpdate = jobParams.getSnpsDeviceUpdate();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsPublish())) {
            snpsPublish = jobParams.getSnpsPublish();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsSimple())) {
            snpsSimple = jobParams.getSnpsSimple();
        }

        if (StringUtils.isNotBlank(jobParams.getSnpsAppKey())) {
            snpsAppKey = jobParams.getSnpsAppKey();
        }

        setScheduleJob((ScheduleJob) scheduleJob);
        setMessageId(messageId);
        setOwnerId(ownerId);
        setGcmServerAPIKey(gcmServerAPIKey);
        setGcmApplicationName(gcmApplicationName);
        setApnsCertificate(apnsCertificate);
        setApnsPrivateKey(apnsPrivateKey);
        setApnsApplicationName(apnsApplicationName);
        setDateString(dateString);

        try {
            if (null != messageId) {
                messagePushSNPS(em, messageId, ownerId, dateString);
            }
        } catch (ServerException e) {
            logger.error("ServerException ", e);
            throw e;
        } finally {
            em.close();
        }

        return "MessageQueueHandler Finished.";
    }

    public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
        return RunMode.LocalMode;
    }

    public static RunMode getInitRunMode(ScheduleJob job) {
        return RunMode.LocalMode;
    }

    public static String arrayConverter(String[] arrayStr) {
        StringBuilder sb = new StringBuilder();

        for (String st : arrayStr) {
            sb.append(st).append(',');
        }

        if (arrayStr.length != 0) {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }

    private void changeDeviceStatus(EntityManager postgres, MemberDevice tmpMemberDevice) {
        if (null != tmpMemberDevice) {
            try {
                @SuppressWarnings("unchecked")
                List<MemberDevice> memberDevices = postgres
                        .createNativeQuery("SELECT DISTINCT ON (device_token) * FROM member_device WHERE device_token = '" + StringHelper.escapeSQL(tmpMemberDevice.getDeviceToken()) + "' ORDER BY device_token, udt::varchar::timestamp DESC",
                                MemberDevice.class)
                        .getResultList();

                postgres.getTransaction().begin();

                for (MemberDevice memberDevice : memberDevices) {
                    memberDevice.setDeviceToken(null);
                    postgres.persist(memberDevice);
                }

                postgres.getTransaction().commit();
            } finally {
                if (postgres.getTransaction().isActive()) {
                    postgres.getTransaction().rollback();
                }
            }
        }
    }

	public String getSnpsBoardcastPayload() {
		return snpsBoardcastPayload;
	}

	public void setSnpsBoardcastPayload(String snpsBoardcastPayload) {
		this.snpsBoardcastPayload = snpsBoardcastPayload;
	}

}