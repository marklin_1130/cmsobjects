package digipages.AppHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.MemberBookFile;
import digipages.exceptions.ServerException;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import digipages.info.ExtMediaBookInfo;
import model.BookFile;
import model.BookInfo;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;
import model.MediabookAttach;
import model.MediabookChapter;
import model.Member;
import model.MemberBook;
import model.MemberBookFinder;

public class BookInfoHandler {
	private Member member;
	private List<String> bookUniIDsArray;
	private String s3PublicBucket = "";
	private static DPLogger logger = DPLogger.getLogger(BookInfoHandler.class.getName());

	public BookInfoServletData getBookInfo(EntityManager postgres) throws ServerException {
		BookInfoServletData f = new BookInfoServletData();
		String bookUniIdLogValue = "";
		String bmemberId = "";
		try {
			for (String bookUniId:bookUniIDsArray) {
				bookUniIdLogValue = bookUniId;
				bmemberId = member.getBmemberId();
				BookFile bf = null;
				MemberBook mb = null;

				MemberBookFinder finder = new MemberBookFinder(bookUniId);
				finder.find(postgres, member);
				bf = finder.getBookFile();
				mb = finder.getMemberBook();

				if (bf == null) {
					throw new ServerException("id_err_303", "book not found");
				}

				MemberBookFile memberBF = new MemberBookFile();

				if("nologin-ThumbGenerator".equalsIgnoreCase(member.getBmemberId())) {
					bf = memberBF.findMaxVersionBookFileForThumbGenerator(postgres, bookUniId);
				}else if ("superreader".equals(member.getMemberType().toLowerCase()) 
						|| ("publisherreader".equals(member.getMemberType().toLowerCase()) 
								&& null != member.getPublisher()) 
						|| ("vendorreader".equals(member.getMemberType().toLowerCase()) 
								&& null != member.getVendor())) {
					if (bf.getStatus() < 8) {
						bf = memberBF.findMaxVersionBookFileForThirdPartyAccount(postgres, bookUniId);
					}
					
				} else {
					if (bf.getStatus() < 9) {
						bf = memberBF.findMaxVersionBookFileForNormalReader(postgres, bookUniId);
					}
				}

				BookInfoServletRecordData rd = new BookInfoServletRecordData(postgres ,member, mb, bf, s3PublicBucket);

				if (member.hasItemByBookUniId(postgres, bookUniId)) {
					f.addRecord(rd);
				}
			}

			f.setUpdated_time(CommonUtil.zonedTime());
		} catch (ServerException x) {
			logger.error("Unexcepted ServerException:bmemberId:"+bmemberId+",bookUniId:"+bookUniIdLogValue+",e:"+AppUtility.shortenedStackTrace(x, 10) ,x);
			throw x;
		} catch (Exception ex) {
			logger.error("Unexcepted Exception:bmemberId:"+bmemberId+",bookUniId:"+bookUniIdLogValue+",e:"+AppUtility.shortenedStackTrace(ex, 10),ex);
			throw new ServerException("id_err_999",ex.getMessage());
		}

		return f;
	}
	
	public ItemInfo getBookInfoForNextbuy(EntityManager postgres ) throws ServerException {
		String bookUniIdLogValue = "";
		String bmemberId = "";
		ItemInfo iteminfo = null;
		try {
			for (String bookUniId:bookUniIDsArray) {
				bookUniIdLogValue = bookUniId;
				bmemberId = member.getBmemberId();
				BookFile bf = null;
				MemberBook mb = null;

				MemberBookFinder finder = new MemberBookFinder(bookUniId);
				finder.find(postgres, member);
				bf = finder.getBookFile();
				mb = finder.getMemberBook();

				if (bf == null) {
					throw new ServerException("id_err_303", "book not found");
				}

				MemberBookFile memberBF = new MemberBookFile();

				if ("superreader".equals(member.getMemberType().toLowerCase()) 
						|| ("publisherreader".equals(member.getMemberType().toLowerCase()) 
								&& null != member.getPublisher()) 
						|| ("vendorreader".equals(member.getMemberType().toLowerCase()) 
								&& null != member.getVendor())) {
					if (bf.getStatus() < 8) {
						bf = memberBF.findMaxVersionBookFileForThirdPartyAccount(postgres, bookUniId);
					}
				} else {
					if (bf.getStatus() < 9) {
						bf = memberBF.findMaxVersionBookFileForNormalReader(postgres, bookUniId);
					}
				}

				BookInfoServletRecordData rd = new BookInfoServletRecordData(postgres,member, mb, bf, s3PublicBucket);
				iteminfo = rd.getItem_info();
			}

		} catch (ServerException x) {
			logger.error("Unexcepted Error:bmemberId:"+bmemberId+",bookUniId:"+bookUniIdLogValue+"e:"+x.getError_message(),x);
			throw x;
		} catch (Exception ex) {
			logger.error("Unexcepted Error:bmemberId:"+bmemberId+",bookUniId:"+bookUniIdLogValue+"e:"+ex.getMessage(),ex);
			throw new ServerException("id_err_999",ex.getMessage());
		}

		return iteminfo;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public List<String> getBookUniIDsArray() {
		return bookUniIDsArray;
	}

	public void setBookUniIDsArray(List<String> bookUniIDsArray) {
		this.bookUniIDsArray = bookUniIDsArray;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
		this.s3PublicBucket = s3PublicBucket;
	}
}

class BookInfoServletRecordData {
    @Expose
	String book_uni_id;
    @Expose
	String item_type;
    @Expose
	ItemInfo item_info;
    @Expose
	Boolean archived;
    @Expose
	Boolean deleted;

	public BookInfoServletRecordData(EntityManager postgres ,Member member, MemberBook memberBook, BookFile bookFile, String s3PublicBucket) {
		ItemInfo info = bookFile.getItem().getInfo();

		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;
		Date finishTime = null;

		startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		finishTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		
		setBook_uni_id(bookFile.getBookUniId());
		setItem_type(bookFile.getItem().getItemType().toLowerCase());

		if (info instanceof BookInfo) {
			BookInfo bi = (BookInfo) info;

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);

			bInfo.setType(bookFile.getItem().getItemType().toLowerCase());
			bInfo.setCur_version(bookFile.getVersion());
			bInfo.setPublisher_name(bookFile.getItem().getPublisherName());
			bInfo.setStart_read_time(CommonUtil.fromDateToString(startReadTime));
			bInfo.setLast_read_time(CommonUtil.fromDateToString(lastReadTime));
			bInfo.setFinish_time(CommonUtil.fromDateToString(finishTime));
			bInfo.setItem_uri("https://www.books.com.tw/products/" + bookFile.getItem().getId());
			bInfo.setSize(bookFile.getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(bookFile.getSize()));
			// memberBook not exist = default
			if (null != memberBook) {
				if ("NormalReader".equals(member.getMemberType())) {
					bInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
				} else {
					bInfo.setAuth_time(CommonUtil.fromDateToString(bookFile.getCreateTime()));
				}

				// bInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
				bInfo.setAsk_update_version((true == memberBook.getAskUpdateVersion() ? "Y" : "N"));
				bInfo.setBg_update_version((true == memberBook.getBgUpdateVersion() ? "Y" : "N"));
				bInfo.setVersion_locked((true == memberBook.getVersionLocked() ? "Y" : "N"));
				bInfo.setBook_highlight_status((true == memberBook.getBookHighlightStatus() ? "Y" : "N"));
				bInfo.setBook_bookmark_status((true == memberBook.getBookBookmarkStatus() ? "Y" : "N"));

				// need to handle null case
				if (memberBook.getStartReadTime() != null) {
					bInfo.setStart_read_time(CommonUtil.fromDateToString(memberBook.getStartReadTime()));
				}

				if (memberBook.getLastReadTime() != null) {
					bInfo.setLast_read_time(CommonUtil.fromDateToString(memberBook.getLastReadTime()));
				}

				if (memberBook.getLastLoc() != null) {
					bInfo.setLast_loc(memberBook.getLastLoc());
				}

				bInfo.setPercent(memberBook.getPercentage());
				bInfo.setFinish_flag(memberBook.getFinishFlag());
			}

			bInfo.setToc(null);
			bInfo.setIntro(null);
			bInfo.setForeword(null);
			
			
			setItem_info(bInfo);
		} else if (info instanceof MagInfo) {
			MagInfo mi = (MagInfo) info;

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			mInfo.setType(bookFile.getItem().getItemType().toLowerCase());
			mInfo.setCur_version(bookFile.getVersion());
			mInfo.setPublisher_name(bookFile.getItem().getPublisherName());
			mInfo.setStart_read_time(CommonUtil.fromDateToString(startReadTime));
			mInfo.setLast_read_time(CommonUtil.fromDateToString(lastReadTime));
			mInfo.setFinish_time(CommonUtil.fromDateToString(finishTime));
			mInfo.setItem_uri("https://www.books.com.tw/products/" + bookFile.getItem().getId());
			mInfo.setSize(bookFile.getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(bookFile.getSize()));
			// memberBook not exist = default
			if (null != memberBook) {
				if ("NormalReader".equals(member.getMemberType())) {
					mInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
				} else {
					mInfo.setAuth_time(CommonUtil.fromDateToString(bookFile.getCreateTime()));
				}

				// mInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
				mInfo.setAsk_update_version((true == memberBook.getAskUpdateVersion() ? "Y" : "N"));
				mInfo.setBg_update_version((true == memberBook.getBgUpdateVersion() ? "Y" : "N"));
				mInfo.setVersion_locked((true == memberBook.getVersionLocked() ? "Y" : "N"));
				mInfo.setBook_highlight_status((true == memberBook.getBookHighlightStatus() ? "Y" : "N"));
				mInfo.setBook_bookmark_status((true == memberBook.getBookBookmarkStatus() ? "Y" : "N"));

				// need to handle null case
				if (memberBook.getStartReadTime() != null) {
					mInfo.setStart_read_time(CommonUtil.fromDateToString(memberBook.getStartReadTime()));
				}

				if (memberBook.getLastReadTime() != null) {
					mInfo.setLast_read_time(CommonUtil.fromDateToString(memberBook.getLastReadTime()));
				}

				if (null != memberBook.getLastLoc()) {
					mInfo.setLast_loc(memberBook.getLastLoc());
				}

				mInfo.setPercent(memberBook.getPercentage());
				mInfo.setFinish_flag(memberBook.getFinishFlag());
			}

			mInfo.setToc(null);
			setItem_info(mInfo);
		}else if (info instanceof MediaBookInfo) {
		    MediaBookInfo mi = (MediaBookInfo) info;

            // Clone
		    ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            mInfo.setType(bookFile.getItem().getItemType().toLowerCase());
            mInfo.setCur_version(bookFile.getVersion());
            mInfo.setPublisher_name(bookFile.getItem().getPublisherName());
            mInfo.setStart_read_time(CommonUtil.fromDateToString(startReadTime));
            mInfo.setLast_read_time(CommonUtil.fromDateToString(lastReadTime));
            mInfo.setFinish_time(CommonUtil.fromDateToString(finishTime));
            mInfo.setItem_uri("https://www.books.com.tw/products/" + bookFile.getItem().getId());
            mInfo.setSize(bookFile.getSize());
            // memberBook not exist = default
            if (null != memberBook) {
                if ("NormalReader".equals(member.getMemberType())) {
                    mInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
                } else {
                    mInfo.setAuth_time(CommonUtil.fromDateToString(bookFile.getCreateTime()));
                }

                // mInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
                mInfo.setAsk_update_version((true == memberBook.getAskUpdateVersion() ? "Y" : "N"));
                mInfo.setBg_update_version((true == memberBook.getBgUpdateVersion() ? "Y" : "N"));
                mInfo.setVersion_locked((true == memberBook.getVersionLocked() ? "Y" : "N"));
                mInfo.setBook_highlight_status((true == memberBook.getBookHighlightStatus() ? "Y" : "N"));
                mInfo.setBook_bookmark_status((true == memberBook.getBookBookmarkStatus() ? "Y" : "N"));

                // need to handle null case
                if (memberBook.getStartReadTime() != null) {
                    mInfo.setStart_read_time(CommonUtil.fromDateToString(memberBook.getStartReadTime()));
                }

                if (memberBook.getLastReadTime() != null) {
                    mInfo.setLast_read_time(CommonUtil.fromDateToString(memberBook.getLastReadTime()));
                }

                if (null != memberBook.getLastLoc()) {
                    mInfo.setLast_loc(memberBook.getLastLoc());
                }

                mInfo.setPercent(memberBook.getPercentage());
                mInfo.setFinish_flag(memberBook.getFinishFlag());
            }

            mInfo.setToc(null);
            mInfo.setBookGroup(null);
            mInfo.setO_title(null);
            mInfo.setAnnotation_flag(null);
            
            //回傳影音書INFO
            if(bookFile.getIsTrial()) {
                List<MediabookChapter> chapterList = postgres.createNamedQuery("MediabookChapter.findByBookFileTrialId", MediabookChapter.class).setParameter("bookFileId", bookFile.getId()).getResultList();
                mInfo.setChapter(chapterList,bookFile.getIsTrial());
            }else {
                List<MediabookChapter> chapterList = postgres.createNamedQuery("MediabookChapter.findByBookFileId", MediabookChapter.class).setParameter("bookFileId", bookFile.getId()).getResultList();
                mInfo.setChapter(chapterList,bookFile.getIsTrial());
                List<MediabookAttach> attachList = postgres.createNamedQuery("MediabookAttach.findByItemId", MediabookAttach.class).setParameter("itemId", bookFile.getItem().getId()).getResultList();
                mInfo.setAttachsDto(attachList);
            }
            
            
            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(bookFile.getSize()));
            setItem_info(mInfo);
        }

		if (null != memberBook) {
			setArchived(memberBook.getArchived());
			setDeleted(memberBook.getDeleted());
		}
	}

	public void updateByMemberBook(MemberBook memberBook) {
		ItemInfo info = memberBook.getItem().getInfo();

		if (info instanceof BookInfo) {
			// Clone
			ExtBookInfo bInfo = (ExtBookInfo) item_info;

			bInfo.setReadlist_idnames(memberBook.getReadlistIdnames());
			bInfo.setCur_version(memberBook.getBookFile().getVersion());
			bInfo.setPercent(memberBook.getPercentage());
			bInfo.setStart_read_time(null == memberBook.getStartReadTime() ? bInfo.getStart_read_time()
					: CommonUtil.fromDateToString(memberBook.getStartReadTime()));
			bInfo.setLast_loc(null == memberBook.getLastLoc() ? "" : memberBook.getLastLoc());
			bInfo.setLast_read_time(null == memberBook.getLastReadTime() ? bInfo.getLast_read_time()
					: CommonUtil.fromDateToString(memberBook.getLastReadTime()));
			bInfo.setUpdated_time(CommonUtil.fromDateToString(memberBook.getLastUpdated()));
			bInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
			bInfo.setAsk_update_version((true == memberBook.getAskUpdateVersion() ? "Y" : "N"));
			bInfo.setBg_update_version((true == memberBook.getBgUpdateVersion() ? "Y" : "N"));
			bInfo.setVersion_locked((true == memberBook.getVersionLocked() ? "Y" : "N"));
			bInfo.setBook_highlight_status((true == memberBook.getBookHighlightStatus() ? "Y" : "N"));
			bInfo.setBook_bookmark_status((true == memberBook.getBookBookmarkStatus() ? "Y" : "N"));
			bInfo.setToc(null);
			bInfo.setIntro(null);
			setItem_info(bInfo);
		} else if (info instanceof MagInfo) {
			// Clone
			ExtMagInfo mInfo = (ExtMagInfo) item_info;

			mInfo.setReadlist_idnames(memberBook.getReadlistIdnames());
			mInfo.setCur_version(memberBook.getBookFile().getVersion());
			mInfo.setPercent(memberBook.getPercentage());
			mInfo.setStart_read_time(null == memberBook.getStartReadTime() ? mInfo.getStart_read_time()
					: CommonUtil.fromDateToString(memberBook.getStartReadTime()));
			mInfo.setLast_loc(null == memberBook.getLastLoc() ? "" : memberBook.getLastLoc());
			mInfo.setLast_read_time(null == memberBook.getLastReadTime() ? mInfo.getLast_read_time()
					: CommonUtil.fromDateToString(memberBook.getLastReadTime()));
			mInfo.setUpdated_time(CommonUtil.fromDateToString(memberBook.getLastUpdated()));
			mInfo.setAuth_time(CommonUtil.fromDateToString(memberBook.getCreateTime()));
			mInfo.setAsk_update_version((true == memberBook.getAskUpdateVersion() ? "Y" : "N"));
			mInfo.setBg_update_version((true == memberBook.getBgUpdateVersion() ? "Y" : "N"));
			mInfo.setVersion_locked((true == memberBook.getVersionLocked() ? "Y" : "N"));
			mInfo.setBook_highlight_status((true == memberBook.getBookHighlightStatus() ? "Y" : "N"));
			mInfo.setBook_bookmark_status((true == memberBook.getBookBookmarkStatus() ? "Y" : "N"));
			mInfo.setToc(null);
			setItem_info(mInfo);
		}
	}

	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public ItemInfo getItem_info() {
		return item_info;
	}

	public void setItem_info(ItemInfo item_info) {
		this.item_info = item_info;
	}

	public Boolean getArchived() {
		return archived;
	}

	public void setArchived(Boolean archived) {
		this.archived = archived;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
}

class BookInfoServletData {
    @Expose
	String updated_time;
    @Expose
	ArrayList<BookInfoServletRecordData> records = new ArrayList<>();

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public List<BookInfoServletRecordData> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<BookInfoServletRecordData> records) {
		this.records = records;
	}

	public void addRecord(BookInfoServletRecordData record) {
		records.add(record);
	}
}