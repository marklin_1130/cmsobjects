package digipages.AppHandler;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import digipages.BooksHandler.BookDrmDeleteNotifyHandler;
import digipages.BooksHandler.MemberDrmHandler;
import digipages.common.AESUtils;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.DRMLogicUtility;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import digipages.info.ExtMediaBookInfo;
import model.BookInfo;
import model.DRMInfo;
import model.EplanPackageTime;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;

public class ListBookAndDrmHandler {
	public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    private Member member;
	private Integer offSet = 0;
	private Integer pageSize = 100;
	private String sortOrder = "";
	private String simpleMode = "n";
	private String isRead = "all";
	private String lastUpdatedTime = "";
	private String currentLastUpdatedTime = "";
	private String clientUDT = "";
	private String readlistFilter = "";
	private String isReadFilter = "";
	private String s3PublicBucket = "";
	private String booksServerNotifyUrl = "";
	private String drmDelNotification = "";
	private String clientId ,clientSecret;
	private boolean isThirdParty =false;
	private String bookFileStatusFilter = " bf.status = 9 ";

	private static DPLogger logger = DPLogger.getLogger(ListBookAndDrmHandler.class.getName());

	public ListBookServletData getReaderListBook(EntityManager postgres) throws Exception {
		ListBookServletData ret = new ListBookServletData();
		MemberDrmHandler memberDrmHandler = new MemberDrmHandler();
		
		// 先查詢全部過期的書並刪除 start
		QueryStringResult needDelMemebrBookQueryStringResult = needDelMemebrBookQueryString(member, sortOrder,readlistFilter, clientUDT, isReadFilter, offSet, pageSize);

		int deletedCount = 0;
		
		if (isThirdParty)
		{
			bookFileStatusFilter=" bf.status >=8 ";
		}
		
		try {
			deletedCount = (int) (long) postgres.createNativeQuery(needDelMemebrBookQueryStringResult.getCountQueryString()).getSingleResult();
		} catch (NoResultException e) {
			deletedCount = 0;
		}

		if (deletedCount > 0) {
		    BookDrmDeleteNotifyHandler bookDrmDeleteNotifyHandler = new BookDrmDeleteNotifyHandler(booksServerNotifyUrl,drmDelNotification,clientId,clientSecret);
			@SuppressWarnings("unchecked")
			List<MemberBook> needDelbooks = postgres.createNativeQuery(needDelMemebrBookQueryStringResult.getQueryString(), MemberBook.class).setHint("javax.persistence.cache.storeMode", "REFRESH").getResultList();
			for (MemberBook memberBook : needDelbooks) {
				if (!memberBook.getIsTrial()) {
					if (memberBook.getMemberDrmLogs() != null && !memberBook.getMemberDrmLogs().isEmpty()) {
						List<MemberDrmLog> drmList = new ArrayList<MemberDrmLog>();
						MemberDrmLog drm = DRMLogicUtility.getLongestMemberDrmLogAndList(memberBook.getMemberDrmLogs() , drmList);
						if (drm != null) {
							boolean isDeleted = memberDrmHandler.delDRMifExpire(postgres, drm.getId());
							if(isDeleted) {
								//最大授權過期，整本書及授權皆須刪除，則不用再檢查月租授權
	                            bookDrmDeleteNotifyHandler.addNotifyMemberDrmlog(drm);
							}else {
								//檢查是否有月租授權過期，因還有授權沒有過期，所以不能整本書刪除(因最大授權沒有過期，所以不能整本書刪除，只刪除該授權及mapping)
								memberDrmHandler.delEplanDRMifExpire(postgres, memberBook, drmList);
	                        }
						}
					}
				}
			}
			new Thread(bookDrmDeleteNotifyHandler).start();
		}
		
		// 先查詢全部過期的書並刪除 end
		
		String queryString = "";
		String countQueryString = "";

		QueryStringResult resultQuery = normalQueryString(member, sortOrder, readlistFilter, clientUDT, isReadFilter, offSet, pageSize);

		queryString = resultQuery.getQueryString();
		countQueryString = resultQuery.getCountQueryString();

		@SuppressWarnings("unchecked")
		List<MemberBook> books = postgres.createNativeQuery(queryString, MemberBook.class).setHint("javax.persistence.cache.storeMode", "REFRESH").getResultList();

		int total_count = 0;

		try {
			total_count = (int) (long) postgres.createNativeQuery(countQueryString).getSingleResult();
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				
				DRMInfo drmInfo = new DRMInfo();
				List<String> eplanids = new ArrayList<String>();
				boolean isDeleted = false;
				String isBuyout = "Y";
				String eplanExpireDate = null;
				String drmReadExpireTime = "";
				
				if (!tmpMb.getIsTrial()) {
					if ("serial".equalsIgnoreCase(tmpMb.getBookType())) {
						postgres.refresh(tmpMb);
					}

					List<MemberDrmLog> drmList = new ArrayList<MemberDrmLog>();
					MemberDrmLog memberDrmLog = DRMLogicUtility.getLongestMemberDrmLogAndList(tmpMb.getMemberDrmLogs() , drmList);
					if (memberDrmLog != null) {
						drmReadExpireTime = memberDrmLog.getReadExpireTime();
						String readExpireTime = CommonUtil.normalizeDateTime(memberDrmLog.getReadExpireTime());
						drmInfo.setRead_end_time(readExpireTime);
						drmInfo.setDrm_type(String.valueOf(memberDrmLog.getType()));
						DRMLogicUtility drmUtil = DRMLogicUtility.genDRM(tmpMb);
						
						if(6 == memberDrmLog.getType()) {
							isBuyout = "N";
							
							//取得該本書籍的 月租書籍指定時間 (eplan_package_time) , 如果有的話
							try {
				            	EplanPackageTime eplanPackageTime = 
				            			postgres.createNamedQuery("EplanPackageTime.findByEplanidAndItemId", EplanPackageTime.class)
				                        .setParameter("eplanid", "")
				                        .setParameter("itemId", tmpMb.getItem().getId())
				                        .getSingleResult();
				            	
				            	//is_always_eplan = N 時才要檢查expire_date, Y時表示沒有從月租移除的打算
				            	if("N".equals(eplanPackageTime.getIsAlwaysEplan())){
				            		eplanExpireDate = CommonUtil.getDateToString(eplanPackageTime.getEplanExpireDate());
				            	}
				            	
			            	} catch (NoResultException ex) {
			            		//該本書籍沒有月租書籍指定時間
			            		eplanExpireDate = null;
			            	}
							
							//若有月租書籍指定時間 (eplan_package_time)且比月租期限早，則使用月租書籍指定時間
							if(eplanExpireDate != null && eplanExpireDate.compareTo(memberDrmLog.getReadExpireTime()) < 0) {
								drmUtil.setRead_end_time(CommonUtil.normalizeDateTime(eplanExpireDate));
								drmInfo.setRead_end_time(CommonUtil.normalizeDateTime(eplanExpireDate));
							}
						}
						
						drmInfo.setDrm_info(AESUtils.encryptAES(drmUtil.toString()));
						drmInfo.setBook_uni_id(tmpMb.getBookUniId());
						
						//放入書籍有效(沒有過期的)的月租包授權eplanId 
						Date now = new Date();
						for (MemberDrmLog memberDrmLogItem : drmList) {
							if(memberDrmLogItem.getType()==6 && CommonUtil.normalizeGetDateTime(memberDrmLogItem.getReadExpireTime()).after(now)  ) {
								eplanids.add(memberDrmLogItem.getSubmitId());
							}
						}
					} else {
						drmInfo.setRead_end_time(StringUtils.EMPTY);
						drmInfo.setDrm_type(StringUtils.EMPTY);
						drmInfo.setDrm_info(StringUtils.EMPTY);
						drmInfo.setBook_uni_id(StringUtils.EMPTY);
                        try {
                            postgres.getTransaction().begin();
                            tmpMb.markAsDelete();
                            postgres.persist(tmpMb);
                            postgres.getTransaction().commit();
                            isDeleted = true;                            
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
					}
				}

				if (isDeleted) {
					postgres.refresh(tmpMb);
					deletedCount++;
				}

				ListBookServletRecordData rd = getListBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode, drmInfo, eplanids);
				rd.getItem_info().setIsbuyout(isBuyout);
				
				if (rd != null) {
					ret.addRecord(rd);
				}
			}
		}

		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(MemberBook.findMemberBookLastUpdateTime(postgres, member.getId()));

		return ret;
	}

	private QueryStringResult needDelMemebrBookQueryString(Member member2, String sortOrder2, String readlistFilter2,
			String clientUDT2, String isReadFilter2, Integer offSet2, Integer pageSize2) {

		String queryString = "";
		String countQueryString = "";

		queryString = "select * from member_book where  member_id = " + member.getId() +" "
				+ " and item_id in (select item_id from member_drm_log  where member_id = " + member.getId()
				+ " and to_date (read_expire_time,'yyyy/mm/dd HH24:MI:ss') <= now() and status = 1 )";

		countQueryString = "select count(*) from member_book where  member_id = " + member.getId() +" "
				+ " and item_id in (select item_id from member_drm_log  where member_id = " + member.getId()
				+ " and to_date (read_expire_time,'yyyy/mm/dd HH24:MI:ss') <= now() and status = 1 )";

		return new QueryStringResult(queryString, countQueryString);
	}

	private ListBookServletRecordData getListBookRecordData(MemberBook tmpMemberBook, ItemInfo info, String simpleMode, DRMInfo drmInfo, List<String> eplanids) {
		ListBookServletRecordData rd = new ListBookServletRecordData();
		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;
		Date finishTime = null;
		
		if (null == tmpMemberBook.getStartReadTime()) {
			startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (null == tmpMemberBook.getLastReadTime()) {
			lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}
		
		if (null == tmpMemberBook.getFinishTime()) {
			finishTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}
		
		if ( info instanceof BookInfo) {
			String bookType = "book";

			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);

			bInfo.setToc(null);
			bInfo.setIntro(null);
			bInfo.setForeword(null);
			bInfo.setListBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime,finishTime);
			bInfo.setDrm_info(drmInfo);
			bInfo.setEplanids(eplanids);
			bInfo.setSize(tmpMemberBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(bInfo);
		} else if (info instanceof MagInfo) {
			String bookType = "magazine";

			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			mInfo.setToc(null);
			mInfo.setListBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime,finishTime);
			mInfo.setDrm_info(drmInfo);
			mInfo.setEplanids(eplanids);
			mInfo.setSize(tmpMemberBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(mInfo);
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("mediabook")) {
            String bookType = "mediabook";

            MediaBookInfo mediaBookInfo = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mediaBookInfo), ExtMediaBookInfo.class);

            mInfo.setBookGroup(null);
            mInfo.setO_title(null);
            mInfo.setAnnotation_flag(null);
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
            mInfo.setListBookInfo(member, tmpMemberBook,simpleMode, bookType, startReadTime, lastReadTime,finishTime);
            mInfo.setCur_version(tmpMemberBook.getBookFile().getVersion());
            mInfo.setDrm_info(drmInfo);
            mInfo.setEplanids(eplanids);
            mInfo.setToc(null);
            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        } else if (tmpMemberBook.getBookType().toLowerCase().equals("audiobook")) {
            String bookType = "audiobook";

            MediaBookInfo mediaBookInfo = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
             rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mediaBookInfo), ExtMediaBookInfo.class);

            mInfo.setBookGroup(null);
            mInfo.setO_title(null);
            mInfo.setAnnotation_flag(null);
            
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
            mInfo.setListBookInfo(member, tmpMemberBook, simpleMode,bookType, startReadTime, lastReadTime,finishTime);
            mInfo.setCur_version(tmpMemberBook.getBookFile().getVersion());
            mInfo.setDrm_info(drmInfo);
            mInfo.setEplanids(eplanids);
            mInfo.setToc(null);
            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        } else if (info instanceof MediaBookInfo && tmpMemberBook.getBookType().toLowerCase().equals("serial")) {
            String bookType ="mediabook";
            if(tmpMemberBook.getItem().getId().contains("E07")) {
                 bookType = "audiobook";
            }else {
                 bookType = "mediabook";
            }

            MediaBookInfo mediaBookInfo = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
             rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mediaBookInfo), ExtMediaBookInfo.class);

            mInfo.setBookGroup(null);
            mInfo.setO_title(null);
            mInfo.setAnnotation_flag(null);
            
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
            mInfo.setListBookInfo(member, tmpMemberBook, simpleMode,bookType, startReadTime, lastReadTime,finishTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setEplanids(eplanids);
            mInfo.setToc(null);
            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        }

		return rd;
	}

	private QueryStringResult normalQueryString(Member member, String sortOrder, String readlistFilter, String clientUDT, String isReadFilter, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		switch (sortOrder) {
        case "default":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "timedesc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "timeasc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time ASC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "name":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " ORDER BY it.c_title COLLATE \"C\"" + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "publisher":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " ORDER BY it.publisher_name COLLATE \"C\"" + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "author":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " ORDER BY it.author NULLS FIRST, it.author COLLATE \"C\"" + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "readtimedesc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.last_read_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "readtimeasc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.last_read_time ASC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "unreadtimedesc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " AND mb.start_read_time IS NULL"
                    + " ORDER BY mb.last_read_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " AND mb.start_read_time IS NULL";
            break;
        default:
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE " + bookFileStatusFilter + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
		}

		return new QueryStringResult(queryString, countQueryString);
	}

	public static String getLineInfo() {
		StackTraceElement ste = new Throwable().getStackTrace()[1];
		return ste.getFileName() + ": Line " + ste.getLineNumber() + "/";
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Integer getOffSet() {
		return offSet;
	}

	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSimpleMode() {
		return simpleMode;
	}

	public void setSimpleMode(String simpleMode) {
		this.simpleMode = simpleMode;
	}

	public String getIsRead() {
		return isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}

	public String getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(String lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getCurrentLastUpdatedTime() {
		return currentLastUpdatedTime;
	}

	public void setCurrentLastUpdatedTime(String currentLastUpdatedTime) {
		this.currentLastUpdatedTime = currentLastUpdatedTime;
	}

	public String getClientUDT() {
		return clientUDT;
	}

	public void setClientUDT(String clientUDT) {
		this.clientUDT = clientUDT;
	}

	public String getReadlistFilter() {
		return readlistFilter;
	}

	public void setReadlistFilter(String readlistFilter) {
		this.readlistFilter = readlistFilter;
	}

	public String getIsReadFilter() {
		return isReadFilter;
	}

	public void setIsReadFilter(String isReadFilter) {
		this.isReadFilter = isReadFilter;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
		this.s3PublicBucket = s3PublicBucket;
	}

	public boolean isThirdParty() {
		return isThirdParty;
	}

	public void setThirdParty(boolean isThirdParty) {
		this.isThirdParty = isThirdParty;
	}

    public String getBooksServerNotifyUrl() {
        return booksServerNotifyUrl;
    }

    public void setBooksServerNotifyUrl(String booksServerNotifyUrl) {
        this.booksServerNotifyUrl = booksServerNotifyUrl;
    }

    public String getDrmDelNotification() {
        return drmDelNotification;
    }

    public void setDrmDelNotification(String drmDelNotification) {
        this.drmDelNotification = drmDelNotification;
    }
    
}
