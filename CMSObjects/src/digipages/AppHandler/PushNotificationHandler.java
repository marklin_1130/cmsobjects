package digipages.AppHandler;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;

import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.CommonResponse;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.MessageQueueParam;
import digipages.exceptions.ServerException;
import digipages.snps.SNPSPublishBoardcastPayload;
import digipages.snps.SNPSPublishBoardcastPayloadResp;
import digipages.snps.SNPSPublishBoardcastResp;
import digipages.snps.SNPSServiceImpl;
import model.MessageQueue;
import model.MessageQueueOwner;
import model.ScheduleJob;

public class PushNotificationHandler {
    private static DPLogger logger = DPLogger.getLogger(PushNotificationHandler.class.getName());
    private static String gcmServerAPIKey = "";
    private static String gcmApplicationName = "";
    private static String apnsCertificate = "";
    private static String apnsPrivateKey = "";
    private static String apnsApplicationName = "";
    private static String dateString = "";
    private static String snpsaccount = "";
    private static String snpspw = "";
    private static String snpsDeviceUpdate = "";
    private static String snpsBoardcast = "";
    private static String snpsBoardcastPayload = "";
    private static String snpsSimple = "";
    private static String snpsAppKeyios = "";
    private static String snpsAppKeyandroid = "";
    private static String snpsPublish = "";
    private static String snpsAppKey = "";

    public Object pushNotification(EntityManager postgres) throws Exception {
        // process push
        processPush(postgres);

        return boardCastNotification(postgres);
    }

    private void processPush(EntityManager postgres) throws ServerException {
        // create message queue
        String curLocalDateTime = getCurLocalDateTimeStr("UTC+8");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime bookDateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
        String bookDateTimeString = String.valueOf(bookDateTime.getYear()) + "/" + String.format("%02d", bookDateTime.getMonthValue()) + "/" + String.format("%02d", bookDateTime.getDayOfMonth()) + " "
                + String.format("%02d", bookDateTime.getHour()) + ":" + String.format("%02d", bookDateTime.getMinute()) + ":" + String.format("%02d", bookDateTime.getSecond());
        String dateString = String.valueOf(bookDateTime.getYear()) + String.format("%02d", bookDateTime.getMonthValue()) + String.format("%02d", bookDateTime.getDayOfMonth());
        String timeString = String.format("%02d", bookDateTime.getHour()) + ":" + String.format("%02d", bookDateTime.getMinute()) + ":00";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Calendar startTimecalendar = Calendar.getInstance();
        startTimecalendar.setTime(java.sql.Time.valueOf(timeString));
        startTimecalendar.add(Calendar.MINUTE, -10);
        Calendar endTimecalendar = Calendar.getInstance();
        endTimecalendar.setTime(java.sql.Time.valueOf(timeString));
        endTimecalendar.add(Calendar.MINUTE, +0);
        Date getEndTime = endTimecalendar.getTime();
        String endTime = sdf.format(getEndTime);

        @SuppressWarnings("unchecked")
        List<MessageQueue> messageQueues = postgres.createNativeQuery("SELECT * From message_queue" + " WHERE start_date <= '" + bookDateTimeString + "'" + " AND send_time <= '" + endTime + "'"
                + " AND target_id <> 'ALL' " + " AND (owner_id IS NULL or owner_id = '')" + " AND (os_type = 'android' OR os_type = 'ios')" + " AND status = 0", MessageQueue.class).getResultList();

        if (messageQueues.size() > 0) {
            // add time judgement.
            String tmpOwnerId = createMessageQueueOwner(postgres);

            try {
                postgres.getTransaction().begin();
                postgres.createNativeQuery("UPDATE message_queue" + " SET owner_id = '" + tmpOwnerId + "'" + " WHERE id IN (SELECT id From message_queue" + " WHERE start_date <= '" + bookDateTimeString + "'"
                        + " AND send_time <= '" + endTime + "'" + " AND target_id <> 'ALL' " + " AND (owner_id IS NULL or owner_id = '')" + " AND (os_type = 'android' OR os_type = 'ios')" + " AND status = 0)")
                        .executeUpdate();
                postgres.getTransaction().commit();

                @SuppressWarnings("unchecked")
                List<MessageQueue> queues = postgres
                        .createNativeQuery("SELECT * From message_queue" + " WHERE owner_id = '" + tmpOwnerId + "'" + " AND (os_type = 'android' OR os_type = 'ios')" + " AND status = 0" + " ORDER BY id ASC",
                                MessageQueue.class)
                        .getResultList();

                // add job
                for (MessageQueue mq : queues) {
                    messageQueueSNPSJob(postgres, mq.getId(), tmpOwnerId,dateString);
                }
            } finally {
                if (postgres.getTransaction().isActive()) {
                    postgres.getTransaction().rollback();
                }
            }
        }
    }

    public Object boardCastNotification(EntityManager postgres) throws Exception {
    	
    	SNPSPublishBoardcastPayloadResp resp = null;
        // create message queue
        String curLocalDateTime = getCurLocalDateTimeStr("UTC+8");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime bookDateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
        String bookDateTimeString = String.valueOf(bookDateTime.getYear()) + "/" + String.format("%02d", bookDateTime.getMonthValue()) + "/" + String.format("%02d", bookDateTime.getDayOfMonth()) + " "
                + String.format("%02d", bookDateTime.getHour()) + ":" + String.format("%02d", bookDateTime.getMinute()) + ":" + String.format("%02d", bookDateTime.getSecond());
        String dateString = String.valueOf(bookDateTime.getYear()) + String.format("%02d", bookDateTime.getMonthValue()) + String.format("%02d", bookDateTime.getDayOfMonth());
        String timeString = String.format("%02d", bookDateTime.getHour()) + ":" + String.format("%02d", bookDateTime.getMinute()) + ":00";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Calendar startTimecalendar = Calendar.getInstance();
        startTimecalendar.setTime(java.sql.Time.valueOf(timeString));
        startTimecalendar.add(Calendar.MINUTE, -10);
        Calendar endTimecalendar = Calendar.getInstance();
        endTimecalendar.setTime(java.sql.Time.valueOf(timeString));
        endTimecalendar.add(Calendar.MINUTE, +0);
        Date getEndTime = endTimecalendar.getTime();
        String endTime = sdf.format(getEndTime);

        // List<MessageQueue> messageQueues =
        // postgres.createNamedQuery("MessageQueue.findAllJobs",
        // MessageQueue.class)
        // .setParameter("startDate", bookDateTimeString)
        // .setParameter("sendTime", endTime)
        // .getResultList();

        @SuppressWarnings("unchecked")
        List<MessageQueue> messageQueues = postgres.createNativeQuery("SELECT * From message_queue" + " WHERE start_date <= '" + bookDateTimeString + "'" + " AND send_time <= '" + endTime + "'"
                + " AND target_id = 'ALL' " + " AND (owner_id IS NULL or owner_id = '' )" + " AND (os_type = 'android' OR os_type = 'ios')" + " AND status = 0", MessageQueue.class).getResultList();

        if (messageQueues.size() > 0) {
            // add time judgement.
            String tmpOwnerId = createMessageQueueOwner(postgres);

            try {
                postgres.getTransaction().begin();
                postgres.createNativeQuery("UPDATE message_queue" + " SET owner_id = '" + tmpOwnerId + "'" + " WHERE id IN (SELECT id From message_queue" + " WHERE start_date <= '" + bookDateTimeString + "'"
                        + " AND send_time <= '" + endTime + "'" + " AND target_id = 'ALL' " + " AND (owner_id IS NULL or owner_id = '')" + " AND (os_type = 'android' OR os_type = 'ios')" + " AND status = 0)")
                        .executeUpdate();
                postgres.getTransaction().commit();

                @SuppressWarnings("unchecked")
                List<MessageQueue> queues = postgres
                        .createNativeQuery("SELECT * From message_queue" + " WHERE owner_id = '" + tmpOwnerId + "'" + " AND (os_type = 'android' OR os_type = 'ios')" + " AND status = 0" + " ORDER BY id ASC",
                                MessageQueue.class)
                        .getResultList();
                
                /* 推播訊息資料格式轉換
                Android:
                	appKey;
                	payload = 
                	 {"data":{"message_id":"23","message_title":"\u512a\u60e0\u8a0a\u606f9","message_content":"[\u6703\u54e1\u5c08\u5340]\u5168\u90e8\uff0c\u512a\u60e0\u8a0a\u606f\u6e2c\u8a66"}}
            	message_id = 從資料庫
            	message_title = "" 空值
            	message_content = content(DB)
                
                IOS:
                	appKey;
                	payload =   {"aps":{"alert":{"body":"\u964d\u50f9\u8a0a\u606f\u6587\u5b57\u6e2c\u8a662\u964d\u50f9\u8a0a\u606f\u6587\u5b57\u6e2c\u8a662"},"sound":"default","datainfo":{"message_id":"11"}}}

                	message_id = message_id(DB)
                	body= content(DB)
                	sound = "default"
                */
                for (MessageQueue mq : queues) {
                	SNPSPublishBoardcastPayload androidSNPSPublishBoardcast = new SNPSPublishBoardcastPayload();
                    if ("android".equalsIgnoreCase(mq.getOsType())) {
                        androidSNPSPublishBoardcast.setAppKey(snpsAppKeyandroid);
                        String payloadAndroid = "{\"data\":{\"message_id\":\""+mq.getId()+"\",\"message_title\":\""+StringUtils.EMPTY+"\",\"message_content\":\""+mq.getContent()+"\", \"send_time\":\""+mq.getSendTime()+"\" }}";//yyyy-MM-dd'T'HH:mm:ssZZZZZ
                        androidSNPSPublishBoardcast.setPayload(payloadAndroid);
                    }
                    if ("ios".equalsIgnoreCase(mq.getOsType())) {
                        androidSNPSPublishBoardcast.setAppKey(snpsAppKeyios);
                        String payloadIOS = "{\"aps\":{\"alert\":{\"body\":\""+mq.getContent()+"\"},\"sound\":\"default\",\"datainfo\":{\"message_id\":\""+mq.getId()+"\" , \"send_time\":\""+mq.getSendTime()+"\"}}}";
                        androidSNPSPublishBoardcast.setPayload(payloadIOS);
                    }
                    SNPSServiceImpl snpsServiceImpl = new SNPSServiceImpl();
                    snpsServiceImpl.setSNPSInfo(snpsaccount, snpspw, snpsDeviceUpdate, snpsBoardcast,snpsBoardcastPayload, snpsPublish, snpsSimple, snpsAppKey);
//                    SNPSPublishBoardcastResp resp = snpsServiceImpl.boardCast(androidSNPSPublishBoardcast);
                    resp = snpsServiceImpl.boardcastPayload(androidSNPSPublishBoardcast);
                    if ("000".equalsIgnoreCase(resp.getReturnCode())) {
                        mq.setStatus(9);
                        postgres.getTransaction().begin();
                        postgres.persist(mq);
                        postgres.getTransaction().commit();
                    }
                }
            }catch (Exception e) {
				throw e;
            } finally {
                if (postgres.getTransaction().isActive()) {
                    postgres.getTransaction().rollback();
                }
            }
        }else {
        	resp = new SNPSPublishBoardcastPayloadResp();
        	resp.setReturnCode("000");
        	resp.setReturnMsg("no msg");
        }
        // Response
        CommonResponse records = new CommonResponse();
        records.setResult(true);

        return resp;
    }

    public static String getCurLocalDateTimeStr(String timeZone) {
        if (timeZone == null || timeZone.length() < 1) {
            timeZone = "UTC+8";
        }

        ZoneId zoneId = ZoneId.of(timeZone);
        ZonedDateTime zdt = ZonedDateTime.now(zoneId);

        return zdt.toString();
    }

    private static String createMessageQueueOwner(EntityManager postgres) {
        String tmpOwnerId = UUID.randomUUID().toString().substring(0, 6);

        try {
            postgres.getTransaction().begin();

            MessageQueueOwner messageQueueOwnerData = new MessageQueueOwner();
            messageQueueOwnerData.setOwnerId(tmpOwnerId);
            messageQueueOwnerData.setLastUpdated(CommonUtil.zonedTime());

            postgres.persist(messageQueueOwnerData);
            postgres.getTransaction().commit();
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        return tmpOwnerId;
    }
    
    private void messageQueueSNPSJob(EntityManager postgres, final Long messageId,final String ownerId,final String dateString) {
        try {
            if (null == messageId) {
                return;
            }

            // add job
            List<ScheduleJob> scheduleJobs = new ArrayList<>();

            MessageQueueParam jobParams = new MessageQueueParam();
            jobParams.setMessage_id(messageId);
            jobParams.setOwner_id(ownerId);
//            jobParams.setGcm_server_api_key(gcmServerAPIKey);
//            jobParams.setGcm_application_name(gcmApplicationName);
//            jobParams.setApns_certificate(apnsCertificate);
//            jobParams.setApns_private_key(apnsPrivateKey);
//            jobParams.setApns_application_name(apnsApplicationName);
            jobParams.setDate_string(dateString);

            jobParams.setSnpsaccount(snpsaccount);
            jobParams.setSnpsBoardcast(snpsBoardcast);
            jobParams.setSnpsBoardcastPayload(snpsBoardcastPayload);
            jobParams.setSnpsDeviceUpdate(snpsDeviceUpdate);
            jobParams.setSnpsPublish(snpsPublish);
            jobParams.setSnpspw(snpspw);
            jobParams.setSnpsAppKeyandroid(snpsAppKeyandroid);
            jobParams.setSnpsAppKeyios(snpsAppKeyios);
            jobParams.setSnpsSimple(snpsSimple);
            jobParams.setSnpsAppKey(snpsAppKey);

            ScheduleJob tmpJob = new ScheduleJob();
            tmpJob.setJobName(MessageQueueHandler.class.getName());
            tmpJob.setJobGroup("MESSAGEQUEUE");
            tmpJob.setJobRunner(0); // for local
            tmpJob.setPriority(500);
            tmpJob.setRetryCnt(0);
            tmpJob.setStatus(0);
            tmpJob.setJobParam(jobParams);

            scheduleJobs.add(tmpJob);

            ScheduleJobManager.addJobs(scheduleJobs);
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    @Deprecated
    private void messageQueueJob(EntityManager postgres, Long messageId, String ownerId, String gcmServerAPIKey, String gcmApplicationName, String apnsCertificate, String apnsPrivateKey, String apnsApplicationName,
            String dateString) {
        try {
            if (null == messageId) {
                return;
            }

            // add job
            List<ScheduleJob> scheduleJobs = new ArrayList<>();

            MessageQueueParam jobParams = new MessageQueueParam();
            jobParams.setMessage_id(messageId);
            jobParams.setOwner_id(ownerId);
            jobParams.setGcm_server_api_key(gcmServerAPIKey);
            jobParams.setGcm_application_name(gcmApplicationName);
            jobParams.setApns_certificate(apnsCertificate);
            jobParams.setApns_private_key(apnsPrivateKey);
            jobParams.setApns_application_name(apnsApplicationName);
            jobParams.setDate_string(dateString);

            jobParams.setSnpsaccount(snpsaccount);
            jobParams.setSnpsBoardcast(snpsBoardcast);
            jobParams.setSnpsDeviceUpdate(snpsDeviceUpdate);
            jobParams.setSnpsPublish(snpsPublish);
            jobParams.setSnpspw(snpspw);
            jobParams.setSnpsAppKeyandroid(snpsAppKeyandroid);
            jobParams.setSnpsAppKeyios(snpsAppKeyios);
            jobParams.setSnpsSimple(snpsSimple);
            jobParams.setSnpsAppKey(snpsAppKey);

            ScheduleJob tmpJob = new ScheduleJob();
            tmpJob.setJobName(MessageQueueHandler.class.getName());
            tmpJob.setJobGroup("MESSAGEQUEUE");
            tmpJob.setJobRunner(0); // for local
            tmpJob.setPriority(500);
            tmpJob.setRetryCnt(0);
            tmpJob.setStatus(0);
            tmpJob.setJobParam(jobParams);

            scheduleJobs.add(tmpJob);

            ScheduleJobManager.addJobs(scheduleJobs);
        } catch (Exception ex) {
            logger.error(ex);
        }
    }

    public static String getSnpsaccount() {
        return snpsaccount;
    }

    public static void setSnpsaccount(String snpsaccount) {
        PushNotificationHandler.snpsaccount = snpsaccount;
    }

    public static String getSnpspw() {
        return snpspw;
    }

    public static void setSnpspw(String snpspw) {
        PushNotificationHandler.snpspw = snpspw;
    }

    public static String getSnpsDeviceUpdate() {
        return snpsDeviceUpdate;
    }

    public static void setSnpsDeviceUpdate(String snpsDeviceUpdate) {
        PushNotificationHandler.snpsDeviceUpdate = snpsDeviceUpdate;
    }

    public static String getSnpsBoardcast() {
        return snpsBoardcast;
    }

    public static void setSnpsBoardcast(String snpsBoardcast) {
        PushNotificationHandler.snpsBoardcast = snpsBoardcast;
    }

    public static String getSnpsSimple() {
        return snpsSimple;
    }

    public static void setSnpsSimple(String snpsSimple) {
        PushNotificationHandler.snpsSimple = snpsSimple;
    }

    public static String getSnpsAppKeyios() {
        return snpsAppKeyios;
    }

    public static void setSnpsAppKeyios(String snpsAppKeyios) {
        PushNotificationHandler.snpsAppKeyios = snpsAppKeyios;
    }

    public static String getSnpsAppKeyandroid() {
        return snpsAppKeyandroid;
    }

    public static void setSnpsAppKeyandroid(String snpsAppKeyandroid) {
        PushNotificationHandler.snpsAppKeyandroid = snpsAppKeyandroid;
    }

    public static String getSnpsPublish() {
        return snpsPublish;
    }

    public static void setSnpsPublish(String snpsPublish) {
        PushNotificationHandler.snpsPublish = snpsPublish;
    }

    public static String getSnpsAppKey() {
        return snpsAppKey;
    }

    public static void setSnpsAppKey(String snpsAppKey) {
        PushNotificationHandler.snpsAppKey = snpsAppKey;
    }

	public static String getSnpsBoardcastPayload() {
		return snpsBoardcastPayload;
	}

	public static void setSnpsBoardcastPayload(String snpsBoardcastPayload) {
		PushNotificationHandler.snpsBoardcastPayload = snpsBoardcastPayload;
	}
}


class AndroidPayload {
	
}