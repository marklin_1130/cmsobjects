package digipages.AppHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.boon.json.JsonFactory;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.GenBenchParam;
import digipages.dev.utility.SampleDataGenerator;
import digipages.exceptions.ServerException;

import model.ScheduleJob;

public class GenBenchBookHandler implements CommonJobHandlerInterface {
	public static final String HANDLERNAME = "GenBenchBookHandler";
	public static final String JOBGROUPNAME = "GENBENCHBOOK";
	public static final int JOBRUNNER = 0;
	private ScheduleJob scheduleJob;
	private static final DPLogger logger = DPLogger.getLogger(GenBenchBookHandler.class.getName());
	private EntityManagerFactory emf;
	private static GenBenchBookHandler myInstance = new GenBenchBookHandler();

	private Integer benchBookCnt;
	private Boolean isTrial;

	public GenBenchBookHandler() {
		LocalRunner.registerHandler(this);
	}

	public static void initJobHandler() {
		// not used.
	}

	public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
		// take jobs
		List<ScheduleJob> jobs;

		ScheduleJob jobParam = new ScheduleJob();
		jobParam.setUuid(UUID.randomUUID());
		jobParam.setJobName(GenBenchBookHandler.HANDLERNAME);
		jobParam.setJobGroup(GenBenchBookHandler.JOBGROUPNAME);
		jobParam.setJobRunner(GenBenchBookHandler.JOBRUNNER);

		jobs = ScheduleJobManager.takeJobs(jobParam, 10);

		return jobs;
	}

	public String genBenchBooks(EntityManager postgres, Integer benchBookCnt, Boolean isTrial) throws ServerException {
		String jsonStr = "";

		try {
			postgres.getTransaction().begin();

			SampleDataGenerator sg = new SampleDataGenerator(postgres);

			if (isTrial) {
				sg.genTrialBooksFromSample(benchBookCnt);
			} else {
				sg.genBooksFromSample(benchBookCnt);
			}

			postgres.getTransaction().commit();
		} catch (javax.persistence.RollbackException rex) {
			throw new ServerException("id_err_999", "Error Writing data to db");
		} catch (Exception e) {
			e.printStackTrace();
			String errJsonMessage = JsonFactory.toJson(e);
			throw new ServerException("id_err_999", errJsonMessage);
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		// finish jobs
		if (null != scheduleJob) {
			// datetime
			String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
			String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

			try {
				ScheduleJobManager.finishJob(scheduleJob);
			} catch (Exception ex) {
				logger.error(ex);
			}
		}

		return jsonStr;
	}

	public ScheduleJob getScheduleJob() {
		return scheduleJob;
	}

	public void setScheduleJob(ScheduleJob scheduleJob) {
		this.scheduleJob = scheduleJob;
	}

	public Integer getBenchBookCnt() {
		return benchBookCnt;
	}

	public void setBenchBookCnt(Integer benchBookCnt) {
		this.benchBookCnt = benchBookCnt;
	}

	public Boolean getIsTrial() {
		return isTrial;
	}

	public void setIsTrial(Boolean isTrial) {
		this.isTrial = isTrial;
	}

	public static GenBenchBookHandler getInstance() {
		return myInstance;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException {
		EntityManager em = emf.createEntityManager();
		GenBenchParam jobParams = scheduleJob.getJobParam(GenBenchParam.class);
		Integer benchBookCnt = 10;
		Boolean isTrial = true;

		// datetime
		String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
		String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

		if (null != jobParams.getBench_book_cnt()) {
			benchBookCnt = Integer.parseInt(jobParams.getBench_book_cnt());
		}

		if (null != jobParams.getIs_trial() && jobParams.getIs_trial().toLowerCase().equals("false")) {
			isTrial = false;
		}

		setScheduleJob((ScheduleJob) scheduleJob);
		setBenchBookCnt(benchBookCnt);
		setIsTrial(isTrial);

		try {
			if (benchBookCnt > 0) {
				genBenchBooks(em, benchBookCnt, isTrial);
			}
		} catch (ServerException e) {
			logger.error("ServerException ", e);
			throw e;
		} finally {
			em.close();
		}

		return "Generate bench books finished.";
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
}