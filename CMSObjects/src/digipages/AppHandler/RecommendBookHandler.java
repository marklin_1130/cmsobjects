package digipages.AppHandler;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.google.gson.Gson;

import digipages.common.CommonUtil;
import digipages.exceptions.ServerException;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import digipages.info.ExtMediaBookInfo;
import model.BookFile;
import model.BookInfo;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;
import model.Member;
import model.RecommendBook;

public class RecommendBookHandler {
	private Member member;
	private java.sql.Timestamp nowTime;
	private Integer maxResults = 24;
	private String s3PublicBucket = "";
	private static RecommendBookServletData cache = null;
	private static RecommendBookServletData cacheE05 = null;
	private static RecommendBookServletData cacheE06 = null;
	private static RecommendBookServletData cacheE07 = null;
	private static RecommendBookServletData cacheE08 = null;
	private String cat;

	public RecommendBookServletData getRecommendBook(EntityManager postgres) throws ServerException {
		int total_count = 0;
		if (member.isGuest() || member.isNormalReader()) {

			// cache is still valid => use cache instead.
			if (null != cache && !cache.isOld() && "all".equalsIgnoreCase(cat)) {
				return cache;
			}
			
			if (null != cacheE05 && !cacheE05.isOld() && "e05".equalsIgnoreCase(cat)) {
				return cacheE05;
			}
			
			if (null != cacheE06 && !cacheE06.isOld() && "e06".equalsIgnoreCase(cat)) {
				return cacheE06;
			}

			String itemType = "";
			
			if("e05".equalsIgnoreCase(cat)) {
				itemType = "book";
			}
			
			if("e06".equalsIgnoreCase(cat)) {
				itemType = "magazine";
			}
			
			if("e07".equalsIgnoreCase(cat)) {
                itemType = "audiobook";
            }
            
            if("e08".equalsIgnoreCase(cat)) {
                itemType = "mediabook";
            }
			
			String queryString ="";
			if(cat.equalsIgnoreCase("all")) {
				queryString = "SELECT * FROM"
						+ " (SELECT DISTINCT ON (rb.item_id) rb.* FROM"
						+ " recommend_books rb RIGHT JOIN book_file bf ON rb.item_id = bf.item_id"
						+ " WHERE 1 = 1 "
						+ " AND now() between rb.start_time  AND rb.end_time "
						+ " AND bf.status = 9"
						+ " AND rb.item_id IN (SELECT id FROM item WHERE status = 1)) x"
						+ " ORDER BY x.start_time DESC, x.udt::varchar::timestamp DESC";
//						+ " LIMIT " + maxResults;
			}else {
				queryString = "SELECT * FROM"
						+ " (SELECT DISTINCT ON (rb.item_id) rb.* FROM"
						+ " recommend_books rb RIGHT JOIN book_file bf ON rb.item_id = bf.item_id RIGHT JOIN item it ON bf.item_id = it.id"
						+ " WHERE it.item_type = '"+itemType+"' "
						+ " AND it.status = 1 " 
						+ " AND now() between rb.start_time  AND rb.end_time "
						+ " AND bf.status = 9"
						+ " AND rb.item_id IN (SELECT id FROM item WHERE status = 1)) x"
						+ " ORDER BY x.start_time DESC, x.udt::varchar::timestamp DESC";
//						+ " LIMIT " + maxResults;
			}
			@SuppressWarnings("unchecked")
			List<RecommendBook> books = postgres.createNativeQuery(queryString, RecommendBook.class).getResultList();

			total_count = books.size();

			if(cat.equalsIgnoreCase("all")) {
				cache = getRecommendBookData(postgres, books, total_count);
				return cache;
			}else if(cat.equalsIgnoreCase("E05")) {
				cacheE05 = getRecommendBookData(postgres, books, total_count);
				return cacheE05;
			}else if(cat.equalsIgnoreCase("E06")) {
				cacheE06 = getRecommendBookData(postgres, books, total_count);
				return cacheE06;
			}else if(cat.equalsIgnoreCase("E07")) {
                cacheE07 = getRecommendBookData(postgres, books, total_count);
                return cacheE07;
            }else if(cat.equalsIgnoreCase("E08")) {
                cacheE08 = getRecommendBookData(postgres, books, total_count);
                return cacheE08;
            }
			
		} else {

			if(cat.equalsIgnoreCase("all")) {
				cache = getRecommendBookData(postgres, new ArrayList<>(), total_count);
				return cache;
			}else if(cat.equalsIgnoreCase("E05")) {
				cacheE05 = getRecommendBookData(postgres, new ArrayList<>(), total_count);
				return cacheE05;
			}else if(cat.equalsIgnoreCase("E06")) {
				cacheE06 = getRecommendBookData(postgres, new ArrayList<>(), total_count);
				return cacheE06;
			}else if(cat.equalsIgnoreCase("E07")) {
                cacheE07 = getRecommendBookData(postgres, new ArrayList<>(), total_count);
                return cacheE05;
            }else if(cat.equalsIgnoreCase("E08")) {
                cacheE08 = getRecommendBookData(postgres, new ArrayList<>(), total_count);
                return cacheE06;
            }
		}

		return cache;
	}

	public RecommendBookServletData getRecommendBookData(EntityManager postgres, List<RecommendBook> books, int total_count) throws ServerException {
		RecommendBookServletData f = new RecommendBookServletData();

		for (RecommendBook tmpRecommendBook : books) {
			// fix to newest version if possible.
			BookFile bf = tmpRecommendBook.getBookFile();

			if (null != bf) {
				bf = bf.getItem().getNewestReadyTrialBookFile(postgres);
				tmpRecommendBook.setBookFile(bf);
			}

			// skip problem items
			if (null == tmpRecommendBook || null == tmpRecommendBook.getBookFile() || null == tmpRecommendBook.getBookFile().getItem() || null == tmpRecommendBook.getBookFile().getItem().getInfo() || null == tmpRecommendBook.getBookFile().getStatus() || 9 != tmpRecommendBook.getBookFile().getStatus()) {
				continue;
			}

			RecommendBookServletRecordData sd = getRecommendBookServletRecordData(tmpRecommendBook);
			
			f.addRecord(sd);
		}

		f.setTotal_records(total_count);
		f.setUpdated_time(CommonUtil.zonedTime());

		return f;
	}

	private RecommendBookServletRecordData getRecommendBookServletRecordData(RecommendBook tmpRecommendBook) {
		RecommendBookServletRecordData rd = new RecommendBookServletRecordData();

		ItemInfo info = tmpRecommendBook.getBookFile().getItem().getInfo();

		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		if (info instanceof BookInfo) {
			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpRecommendBook.getBookFile().getBookUniId());
			rd.setItem_type(tmpRecommendBook.getBookFile().getItem().getItemType());

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);

			// BaseBookInfo
//			bInfo.setO_title(null);
//			bInfo.setPublisher_id(null);
//			bInfo.setCategory(null);
//			bInfo.setRank(null);
			bInfo.setToc(null);
//			bInfo.setEfile_nofixed_name(null);
//			bInfo.setEfile_fixed_pad_name(null);
//			bInfo.setEfile_fixed_phone_name(null);
//			bInfo.setEfile_url(null);
//			bInfo.setShare_flag(null);
//			bInfo.setStatus(null);
//			bInfo.setReturn_file_num(0);
//			bInfo.setBookGroup(0);
//			bInfo.setLanguage(null);
//			bInfo.setBuffet_flag(null);
//			bInfo.setBuffet_type(null);

			// BookInfo
//			bInfo.setO_author(null);
//			bInfo.setTranslator(null);
//			bInfo.setEditor(null);
//			bInfo.setIllustrator(null);
//			bInfo.setEdition(null);
			bInfo.setForeword(null);
			bInfo.setIntro(null);
			bInfo.setStart_time(CommonUtil.fromDateToString(tmpRecommendBook.getStartTime()));
			bInfo.setEnd_time(CommonUtil.fromDateToString(tmpRecommendBook.getEndTime()));
			bInfo.setUpdated_time(CommonUtil.fromDateToString(tmpRecommendBook.getLastUpdated()));
			bInfo.setCur_version(tmpRecommendBook.getBookFile().getVersion());
			bInfo.setSize(tmpRecommendBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpRecommendBook.getBookFile().getSize()));
			rd.setItem_info(bInfo);
		} else if (info instanceof MagInfo) {
			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpRecommendBook.getBookFile().getBookUniId());
			rd.setItem_type(tmpRecommendBook.getBookFile().getItem().getItemType());

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			// BaseBookInfo
//			mInfo.setO_title(null);
//			mInfo.setPublisher_id(null);
//			mInfo.setCategory(null);
//			mInfo.setRank(null);
			mInfo.setToc(null);
//			mInfo.setEfile_nofixed_name(null);
//			mInfo.setEfile_fixed_pad_name(null);
//			mInfo.setEfile_fixed_phone_name(null);
//			mInfo.setEfile_url(null);
//			mInfo.setShare_flag(null);
//			mInfo.setStatus(null);
//			mInfo.setReturn_file_num(0);
//			mInfo.setBookGroup(0);
//			mInfo.setLanguage(null);
//			mInfo.setBuffet_flag(null);
//			mInfo.setBuffet_type(null);

			// MagInfo
//			mInfo.setMag_id(null);
//			mInfo.setIssue(null);
//			mInfo.setIssue_year(null);
//			mInfo.setPublish_type(null);
//			mInfo.setCover_man(null);
//			mInfo.setCover_story(null);
			mInfo.setStart_time(CommonUtil.fromDateToString(tmpRecommendBook.getStartTime()));
			mInfo.setEnd_time(CommonUtil.fromDateToString(tmpRecommendBook.getEndTime()));
			mInfo.setUpdated_time(CommonUtil.fromDateToString(tmpRecommendBook.getLastUpdated()));
			mInfo.setCur_version(tmpRecommendBook.getBookFile().getVersion());
			mInfo.setSize(tmpRecommendBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpRecommendBook.getBookFile().getSize()));
			rd.setItem_info(mInfo);
		}else if (info instanceof MediaBookInfo) {
		    MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpRecommendBook.getBookFile().getBookUniId());
            rd.setItem_type(tmpRecommendBook.getBookFile().getItem().getItemType());

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            // BaseBookInfo
//          mInfo.setO_title(null);
//          mInfo.setPublisher_id(null);
//          mInfo.setCategory(null);
//          mInfo.setRank(null);
            mInfo.setToc(null);
//          mInfo.setEfile_nofixed_name(null);
//          mInfo.setEfile_fixed_pad_name(null);
//          mInfo.setEfile_fixed_phone_name(null);
//          mInfo.setEfile_url(null);
//          mInfo.setShare_flag(null);
//          mInfo.setStatus(null);
//          mInfo.setReturn_file_num(0);
//          mInfo.setBookGroup(0);
//          mInfo.setLanguage(null);
//          mInfo.setBuffet_flag(null);
//          mInfo.setBuffet_type(null);

            // MagInfo
//          mInfo.setMag_id(null);
//          mInfo.setIssue(null);
//          mInfo.setIssue_year(null);
//          mInfo.setPublish_type(null);
//          mInfo.setCover_man(null);
//          mInfo.setCover_story(null);
            mInfo.setStart_time(CommonUtil.fromDateToString(tmpRecommendBook.getStartTime()));
            mInfo.setEnd_time(CommonUtil.fromDateToString(tmpRecommendBook.getEndTime()));
            mInfo.setUpdated_time(CommonUtil.fromDateToString(tmpRecommendBook.getLastUpdated()));
            mInfo.setCur_version(tmpRecommendBook.getBookFile().getVersion());
            mInfo.setSize(tmpRecommendBook.getBookFile().getSize());
//            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpRecommendBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        }

		return rd;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public java.sql.Timestamp getNowTime() {
		return nowTime;
	}

	public void setNowTime(java.sql.Timestamp nowTime) {
		this.nowTime = nowTime;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
		this.s3PublicBucket = s3PublicBucket;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}
}

class RecommendBookServletResultError {
	String error_code;
	String error_message;

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}

class RecommendBookServletRecordData {
	String book_uni_id;
	String item_type;
	String updated_time;
	ItemInfo item_info;

	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ItemInfo getItem_info() {
		return item_info;
	}

	public void setItem_info(ItemInfo item_info) {
		this.item_info = item_info;
	}
}

class RecommendBookServletData {
	Integer total_records = 20;
	String updated_time;
	ArrayList<RecommendBookServletRecordData> records = new ArrayList<>();
	long lastQueryTime = System.currentTimeMillis();

	public Integer getTotal_records() {
		return total_records;
	}

	/**
	 * if this result is older than 60
	 * 
	 * @return
	 */
	public boolean isOld() {
		long now = System.currentTimeMillis();
		long timeDiff = now - lastQueryTime;

		// if no records => old lower to 1 minute.
		if (records.isEmpty())
		{
			return true;
		}
		return timeDiff > (1000L * 60 * 60);
	}

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ArrayList<RecommendBookServletRecordData> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<RecommendBookServletRecordData> records) {
		this.records = records;
	}

	public void addRecord(RecommendBookServletRecordData record) {
		records.add(record);
	}
}