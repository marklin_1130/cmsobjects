package digipages.AppHandler;

import static org.boon.Boon.toJson;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.common.CommonResponse;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.Member;
import model.MemberBook;
import model.MemberBookFinder;

public class UpdateBookReadListHandler {
	
	private static DPLogger logger =DPLogger.getLogger(UpdateBookReadListHandler.class.getName());
	
	private String inputJson = "";
	private Member member = null;

	public String processUpdate(EntityManager postgres) throws ServerException {
		try {
			Gson gson = new GsonBuilder().create();
			UpdateBookReadListData records = gson.fromJson(inputJson, UpdateBookReadListData.class);

			postgres.getTransaction().begin();
			for (records record : records.getRecords()) {
				int idLength = record.getBook_uni_id().size();

				for (int idSerial = 0; idSerial < idLength; idSerial++) {
					MemberBookFinder finder = new MemberBookFinder(record.getBook_uni_id().get(idSerial));
					finder.find(postgres, member);
					MemberBook memberBook = finder.getMemberBook();

					if (null == memberBook) {
						throw new ServerException("id_err_301", "  Book not found/ member id is :" + member.getId());
					}

					String memberBookNoteudt = null;

					if (null == record.getUpdated_time()) {
						memberBook.setLastUpdated(CommonUtil.zonedTime());
						memberBookNoteudt = CommonUtil.zonedTime();
					} else if (null != record.getUpdated_time() && !ckDate(record.getUpdated_time(), memberBook.getLastUpdated())) {
						memberBookNoteudt = record.getUpdated_time();
					} else {
						continue;
					}

					TargetReadList targetReadlist = new TargetReadList();
					targetReadlist.TargetToReadList(record);

					if (memberBook.getIsTrial()) {
						memberBook.setIsTrial(true);
					} else {
						memberBook.setIsTrial(targetReadlist.isTrial());
						memberBook.setPassworded(targetReadlist.isPassworded());
						memberBook.setArchived(targetReadlist.isArchived());
					}

					String target = record.getTo_readlist();

					if (record.getAction().equals("add")) {
						memberBook.addToReadList(target);
					} else if (record.getAction().equals("remove")) {
						// TODO verify me, (trail removed action)
//						if (target.equals("trial")) {
//							memberBook.setMemberBookNoteDel(memberBookNoteudt);
//						}
						memberBook.removeFromReadList(target);
					}

					if (null != record.getUpdated_time()) {
						memberBook.setLastUpdated(record.getUpdated_time());
					}

					postgres.persist(memberBook);
				}
			}

			postgres.getTransaction().commit();
			postgres.refresh(member);
		} catch (ServerException e) {
			throw e;
		} catch (Exception ex) {
			logger.error(ex);;
			throw new ServerException("id_err_999", " Unexcepted Exception:" + ex);
		} finally {
			if (postgres.getTransaction().isActive())
				postgres.getTransaction().rollback();
		}

		CommonResponse responseJson = new CommonResponse();
		responseJson.setResult(true);
		return toJson(responseJson);
	}

	public static Boolean ckDate(String updateDate, Date dbDate) {
		Date updatedTime = null;
		updatedTime = Date.from(CommonUtil.parseDateTime(updateDate).toInstant());
		return dbDate.after(updatedTime);
		// return dbDate.before(updatedTime);
	}

	public static class TargetReadList {
		records records;
		Set<String> ReadListIdnames = new HashSet<String>();
		boolean Trial;
		boolean Passworded;
		boolean Archived;

		public void TargetToReadList(records record) {
			switch (record.getTo_readlist()) {
				case "trial":
					Private Trial = new Private();
					this.ReadListIdnames = Trial.getIdNames();
					this.Trial = Trial.isTrial();
					this.Passworded = Trial.isPassworded();
					this.Archived = Trial.isArchived();
					break;
				case "private":
					Private Private = new Private();
					this.ReadListIdnames = Private.getIdNames();
					this.Trial = Private.isTrial();
					this.Passworded = Private.isPassworded();
					this.Archived = Private.isArchived();
					break;
				case "password":
					Password Password = new Password();
					this.ReadListIdnames = Password.getIdNames();
					this.Trial = Password.isTrial();
					this.Passworded = Password.isPassworded();
					this.Archived = Password.isArchived();
					break;
				case "all":
					All All = new All();
					this.ReadListIdnames = All.getIdNames();
					this.Trial = All.isTrial();
					this.Passworded = All.isPassworded();
					this.Archived = All.isArchived();
					break;
				case "archive":
					Archive Archive = new Archive();
					this.ReadListIdnames = Archive.getIdNames();
					this.Trial = Archive.isTrial();
					this.Passworded = Archive.isPassworded();
					this.Archived = Archive.isArchived();
					break;
				default:
					if (record.getTo_readlist().length() > 6 && record.getTo_readlist().substring(0, 6).equals("custom")) {
						Custom Custom = new Custom();
						this.ReadListIdnames = Custom.getIdNames();
						this.Trial = Custom.isTrial();
						this.Passworded = Custom.isPassworded();
						this.Archived = Custom.isArchived();
					}
			}
		}

		public records getRecords() {
			return records;
		}

		public void setRecords(records records) {
			this.records = records;
		}

		public Set<String> getReadListIdnames() {
			return ReadListIdnames;
		}

		public void setReadListIdnames(Set<String> readListIdnames) {
			ReadListIdnames = readListIdnames;
		}

		public boolean isTrial() {
			return Trial;
		}

		public void setTrial(boolean trial) {
			Trial = trial;
		}

		public boolean isPassworded() {
			return Passworded;
		}

		public void setPassworded(boolean passworded) {
			Passworded = passworded;
		}

		public boolean isArchived() {
			return Archived;
		}

		public void setArchived(boolean archived) {
			Archived = archived;
		}
	}

	public class UpdateBookReadListData {
		List<records> records;

		public List<records> getRecords() {
			return records;
		}

		public void setRecords(List<records> records) {
			this.records = records;
		}
	}

	public class records {
		List<String> book_uni_id;
		String to_readlist;
		String action;
		String updated_time;

		public List<String> getBook_uni_id() {
			return book_uni_id;
		}

		public void setBook_uni_id(List<String> book_uni_id) {
			this.book_uni_id = book_uni_id;
		}

		public String getTo_readlist() {
			return to_readlist;
		}

		public void setTo_readlist(String to_readlist) {
			this.to_readlist = to_readlist;
		}

		public String getAction() {
			return action;
		}

		public void setAction(String action) {
			this.action = action;
		}

		public String getUpdated_time() {
			return updated_time;
		}

		public void setUpdated_time(String updated_time) {
			this.updated_time = updated_time;
		}
	}

	public static class Trial {
		boolean is_trial = true;
		boolean archived = false;
		boolean passworded = false;
		Set<String> idNames = new HashSet<String>() {
			/**
			* 
			*/
			private static final long serialVersionUID = -262660038348071154L;
			{
				add("trial");
			}
		};

		public boolean isIs_trial() {
			return is_trial;
		}

		public void setIs_trial(boolean is_trial) {
			this.is_trial = is_trial;
		}

		public boolean isArchived() {
			return archived;
		}

		public void setArchived(boolean archived) {
			this.archived = archived;
		}

		public boolean isPassworded() {
			return passworded;
		}

		public void setPassworded(boolean passworded) {
			this.passworded = passworded;
		}

		public Set<String> getIdNames() {
			return idNames;
		}

		public void setIdNames(Set<String> idNames) {
			this.idNames = idNames;
		}
	}

	public static class Private {
		boolean trial = false;
		boolean archived = false;
		boolean passworded = true;
		Set<String> idNames = new HashSet<String>() {
			/**
			* 
			*/
			private static final long serialVersionUID = -262660038348071154L;
			{
				add("private");
			}
		};

		public boolean isTrial() {
			return trial;
		}

		public void setTrial(boolean trial) {
			this.trial = trial;
		}

		public boolean isArchived() {
			return archived;
		}

		public void setArchived(boolean archived) {
			this.archived = archived;
		}

		public boolean isPassworded() {
			return passworded;
		}

		public void setPassworded(boolean passworded) {
			this.passworded = passworded;
		}

		public Set<String> getIdNames() {
			return idNames;
		}

		public void setIdNames(Set<String> idNames) {
			this.idNames = idNames;
		}
	}

	public static class Archive {
		boolean trial = false;
		boolean archived = true;
		boolean passworded = false;
		Set<String> idNames = new HashSet<String>() {
			/**
			* 
			*/
			private static final long serialVersionUID = 1551229249351648190L;
			{
				add("archive");
			}
		};

		public boolean isTrial() {
			return trial;
		}

		public void setTrial(boolean trial) {
			this.trial = trial;
		}

		public boolean isArchived() {
			return archived;
		}

		public void setArchived(boolean archived) {
			this.archived = archived;
		}

		public boolean isPassworded() {
			return passworded;
		}

		public void setPassworded(boolean passworded) {
			this.passworded = passworded;
		}

		public Set<String> getIdNames() {
			return idNames;
		}

		public void setIdNames(Set<String> idNames) {
			this.idNames = idNames;
		}
	}

	public static class Password {
		boolean trial = false;
		boolean archived = false;
		boolean passworded = true;
		Set<String> idNames = new HashSet<String>() {
			/**
			* 
			*/
			private static final long serialVersionUID = 6188661959994888313L;
			{
				add("password");
			}
		};

		public boolean isTrial() {
			return trial;
		}

		public void setTrial(boolean trial) {
			this.trial = trial;
		}

		public boolean isArchived() {
			return archived;
		}

		public void setArchived(boolean archived) {
			this.archived = archived;
		}

		public boolean isPassworded() {
			return passworded;
		}

		public void setPassworded(boolean passworded) {
			this.passworded = passworded;
		}

		public Set<String> getIdNames() {
			return idNames;
		}

		public void setIdNames(Set<String> idNames) {
			this.idNames = idNames;
		}
	}

	public static class Custom {
		boolean trial = false;
		boolean archived = false;
		boolean passworded = false;
		Set<String> idNames = new HashSet<String>();

		public boolean isTrial() {
			return trial;
		}

		public void setTrial(boolean trial) {
			this.trial = trial;
		}

		public boolean isArchived() {
			return archived;
		}

		public void setArchived(boolean archived) {
			this.archived = archived;
		}

		public boolean isPassworded() {
			return passworded;
		}

		public void setPassworded(boolean passworded) {
			this.passworded = passworded;
		}

		public Set<String> getIdNames() {
			return idNames;
		}

		public Set<String> setIdNames(String cusidname) {
			idNames.add(cusidname);
			return idNames;
		}
	}

	public static class All {
		boolean trial = false;
		boolean archived = true;
		boolean passworded = false;
		Set<String> idNames = new HashSet<String>() {
			/**
			* 
			*/
			private static final long serialVersionUID = 3679016177209564576L;
			{
				add("all");
			}
		};

		public String getType(MemberBook memberbook) {
			return null;
		}

		public boolean isTrial() {
			return trial;
		}

		public void setTrial(boolean trial) {
			this.trial = trial;
		}

		public boolean isArchived() {
			return archived;
		}

		public void setArchived(boolean archived) {
			this.archived = archived;
		}

		public boolean isPassworded() {
			return passworded;
		}

		public void setPassworded(boolean passworded) {
			this.passworded = passworded;
		}

		public Set<String> getIdNames() {
			return idNames;
		}

		public void setIdNames(Set<String> idNames) {
			this.idNames = idNames;
		}
	}

	public String getInputJson() {
		return inputJson;
	}

	public void setInputJson(String inputJson) {
		this.inputJson = inputJson;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
}