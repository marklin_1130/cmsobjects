package digipages.AppHandler;

import java.io.PrintWriter;
import java.io.StringWriter;

import digipages.common.CommonUtil;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import digipages.info.ExtMediaBookInfo;
import model.BaseBookInfo;
import model.BookInfo;
import model.BookPreviewInfo;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;

public class AppUtility {
	/**
	 * fix Iteminfo according to bucket info
	 * 
	 * @param info
	 */
	static public ItemInfo fixCoverURL(ItemInfo info, String s3PublicBucket) {
		
		ItemInfo ret = null;
		
		
		
		if (info instanceof ExtMagInfo)
			ret = CommonUtil.jsonClone(info, ExtMagInfo.class);
		if (info instanceof ExtBookInfo)
			ret = CommonUtil.jsonClone(info, ExtBookInfo.class);
		if (info instanceof MagInfo)
			ret = CommonUtil.jsonClone(info, MagInfo.class);
		else if (info instanceof BookInfo)
			ret = CommonUtil.jsonClone(info, BookInfo.class);
		else if (info instanceof BookPreviewInfo)
			ret = CommonUtil.jsonClone(info, BookPreviewInfo.class);
		else if (info instanceof ExtMediaBookInfo)
            ret = CommonUtil.jsonClone(info, ExtMediaBookInfo.class);
		else if (info instanceof MediaBookInfo)
            ret = CommonUtil.jsonClone(info, MediaBookInfo.class);
		
		// fix url info
		BaseBookInfo xbInfo = (BaseBookInfo) ret;

		// don't do cover if no cover
		if (null == xbInfo.getEfile_cover_url()) {
			return ret;
		}

		xbInfo.setEfile_cover_url(replaceOldStr(xbInfo.getEfile_cover_url(), s3PublicBucket));
		return ret;
	}

	public static String replaceOldStr(String orig, String s3PublicBucket) {
		// @ Eric temprory fix for old data.
		if (orig.contains("s3public.staging.books.com.tw")) {
			return orig.replace("http://s3public.staging.books.com.tw/cover/", "https://" + s3PublicBucket + "/cover/");
		}

		if (!orig.startsWith("http")){
			return "https://" + s3PublicBucket + "/cover/" + orig;
		}
		return orig;
	}
	
	public static String shortenedStackTrace(Exception e, int maxLines) {
	    StringWriter writer = new StringWriter();
	    StringBuilder sb = new StringBuilder();
	    try {
	    e.printStackTrace(new PrintWriter(writer));
	    String[] lines = writer.toString().split("\n");
	    for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
	        sb.append(lines[i]).append("\n");
	    }
		
		} catch (Exception e2) {
			// TODO: handle exception
		}
	    return sb.toString();
	}

}