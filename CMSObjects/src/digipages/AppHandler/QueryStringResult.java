package digipages.AppHandler;

public class QueryStringResult {
	private String queryString = "";
	private String countQueryString = "";

	public QueryStringResult(String queryString, String countQueryString) {
		this.queryString = queryString;
		this.countQueryString = countQueryString;
	}

	public String getQueryString() {
		return queryString;
	}

	public String getCountQueryString() {
		return countQueryString;
	}
}