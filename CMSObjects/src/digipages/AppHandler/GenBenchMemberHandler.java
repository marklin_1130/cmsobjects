package digipages.AppHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.boon.json.JsonFactory;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.GenBenchParam;
import digipages.dev.utility.SampleDataGenerator;
import digipages.exceptions.ServerException;

import model.ScheduleJob;

public class GenBenchMemberHandler implements CommonJobHandlerInterface {
	public static final String HANDLERNAME = "GenBenchMemberHandler";
	public static final String JOBGROUPNAME = "GENBENCHMEMBER";
	public static final int JOBRUNNER = 0;
	private ScheduleJob scheduleJob;
	private static final DPLogger logger = DPLogger.getLogger(GenBenchMemberHandler.class.getName());
	private EntityManagerFactory emf;
	private static GenBenchMemberHandler myInstance = new GenBenchMemberHandler();

	private Integer benchMemberCnt;
	private Integer avgBookCnt;
	private Integer avgNoteCnt;

	public GenBenchMemberHandler() {
		LocalRunner.registerHandler(this);
	}

	public static void initJobHandler() {
		// not used.
	}

	public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
		// take jobs
		List<ScheduleJob> jobs;

		ScheduleJob jobParam = new ScheduleJob();
		jobParam.setUuid(UUID.randomUUID());
		jobParam.setJobName(GenBenchMemberHandler.HANDLERNAME);
		jobParam.setJobGroup(GenBenchMemberHandler.JOBGROUPNAME);
		jobParam.setJobRunner(GenBenchMemberHandler.JOBRUNNER);

		jobs = ScheduleJobManager.takeJobs(jobParam, 10);

		return jobs;
	}

	public String genBenchMembers(EntityManager postgres, Integer benchMemberCnt, Integer avgBookCnt, Integer avgNoteCnt) throws ServerException {
		String jsonStr = "";

		try {
			postgres.getTransaction().begin();

			SampleDataGenerator sg = new SampleDataGenerator(postgres);
			sg.genBenchMembers(benchMemberCnt, avgBookCnt, avgNoteCnt);

			postgres.getTransaction().commit();
		} catch (javax.persistence.RollbackException rex) {
			throw new ServerException("id_err_999", "Error Writing data to db");
		} catch (Exception e) {
			e.printStackTrace();
			String errJsonMessage = JsonFactory.toJson(e);
			throw new ServerException("id_err_999", errJsonMessage);
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		// finish jobs
		if (null != scheduleJob) {
			// datetime
			String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
			String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

			try {
				ScheduleJobManager.finishJob(scheduleJob);

			} catch (Exception ex) {
				logger.error(ex);
			}
		}

		return jsonStr;
	}

	public ScheduleJob getScheduleJob() {
		return scheduleJob;
	}

	public void setScheduleJob(ScheduleJob scheduleJob) {
		this.scheduleJob = scheduleJob;
	}

	public Integer getBenchMemberCnt() {
		return benchMemberCnt;
	}

	public void setBenchMemberCnt(Integer benchMemberCnt) {
		this.benchMemberCnt = benchMemberCnt;
	}

	public Integer getAvgBookCnt() {
		return avgBookCnt;
	}

	public void setAvgBookCnt(Integer avgBookCnt) {
		this.avgBookCnt = avgBookCnt;
	}

	public Integer getAvgNoteCnt() {
		return avgNoteCnt;
	}

	public void setAvgNoteCnt(Integer avgNoteCnt) {
		this.avgNoteCnt = avgNoteCnt;
	}

	public static GenBenchMemberHandler getInstance() {
		return myInstance;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException {
		EntityManager em = emf.createEntityManager();
		GenBenchParam jobParams = scheduleJob.getJobParam(GenBenchParam.class);
		Integer benchMemberCnt = 10;
		Integer avgBookCnt = 20;
		Integer avgNoteCnt = 5;

		// datetime
		String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
		String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

		if (null != jobParams.getBench_member_cnt()) {
			benchMemberCnt = Integer.parseInt(jobParams.getBench_member_cnt());
		}

		if (null != jobParams.getAvg_book_cnt()) {
			avgBookCnt = Integer.parseInt(jobParams.getAvg_book_cnt());
		}

		if (null != jobParams.getAvg_note_cnt()) {
			avgNoteCnt = Integer.parseInt(jobParams.getAvg_note_cnt());
		}

		setScheduleJob((ScheduleJob) scheduleJob);
		setBenchMemberCnt(benchMemberCnt);
		setAvgBookCnt(avgBookCnt);
		setAvgNoteCnt(avgNoteCnt);

		try {
			if (benchMemberCnt > 0) {
				genBenchMembers(em, benchMemberCnt, avgBookCnt, avgNoteCnt);
			}
		} catch (ServerException e) {
			logger.error("ServerException ", e);
			throw e;
		} finally {
			em.close();
		}

		return "Generate bench members finished.";
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
}