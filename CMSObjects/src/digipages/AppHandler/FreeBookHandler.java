package digipages.AppHandler;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import digipages.common.CommonUtil;
import digipages.common.StringHelper;
import digipages.exceptions.ServerException;
import digipages.info.FreeBookInfo;
import digipages.info.FreeMagInfo;
import digipages.info.FreeMediaBookInfo;
import model.BookFile;
import model.BookInfo;
import model.FreeBook;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;

public class FreeBookHandler {
	private Member member;
	private java.sql.Timestamp nowTime;
	private String s3PublicBucket = "";
	private int offSet;
	private int pageSize;
	private String cat;
	private String sort_order;
	private ArrayList<String> sub;
	private static FreeBookServletData cache = null;

	public FreeBookServletData getFreeBook(EntityManager postgres) throws ServerException {
		int total_count = 0;

//		output調整：
//		．item_info視sheet[item_info]
//		．排序：上線時間新到舊，若上線時間一樣依分類cat排序由小至大排，分類一致再依店內碼大到小
//		input調整：
//		．業種參數cat：分E05書籍/E06雜誌/all全列
//		．排序參數sort_order：
//		1、出版日期PublisherDate（新到舊）：依出版日期排序新到舊 => item_info->publish_date
//		2、書名Name：依書名排序=>item_info->c_titile
//		3、作者Author：依作者名稱排序，若無作者，依次為編者／譯者／原文作者／繪者(太麻煩就先依作者) => item_info->xxx
//		4、出版社Publisher：依出版社名稱排序=>item_info->publisher_name
//		5、首頁排序Default：上線時間新到舊，若上線時間一樣依分類cat排序由小至大排，分類一致再依店內碼大到小=>
//		．分類過濾sub：tem_info->category like ~

		String sortPublisherDate = " i.info ->> 'publish_date' desc ";
		String sortName = " i.c_title ";
		String sortAuthor = " i.author ";
		String sortPublisher = " i.publisher_name ";
		String sortDefault = " fb.sequence ,fb.udt DESC,substring(fb.item_id,0,4) ,fb.item_id desc ";
		String subQueryCondition = "";
		if (sub!=null && !sub.isEmpty()) {
			
			String subConditionsSql = "";
			for (String subConditions : sub) {
				subConditionsSql += " i.info ->> 'category' like " + "'" + subConditions + "%' OR";
			}
			
			
			subConditionsSql = subConditionsSql.substring(0, subConditionsSql.length()-2);
			
			subQueryCondition = " AND ("+subConditionsSql+")";
		}

		String orderConditions = " ORDER BY ";

		if ("PublisherDate".equalsIgnoreCase(sort_order)) {
			orderConditions += sortPublisherDate;
		}

		if ("Name".equalsIgnoreCase(sort_order)) {
			orderConditions += sortName;
		}

		if ("Author".equalsIgnoreCase(sort_order)) {
			orderConditions += sortAuthor;
		}
		if ("Publisher".equalsIgnoreCase(sort_order)) {
			orderConditions += sortPublisher;
		}

		if ("Default".equalsIgnoreCase(sort_order)) {
			orderConditions += sortDefault;
		}
		String queryString = "";
		if (pageSize > 0) {

			if (cat.equalsIgnoreCase("all")) {
				
				queryString = "SELECT fb.* FROM free_books fb , item i" + " WHERE fb.item_id = i.id " + " and fb.status = 1 and i.status = 1"
						+ " AND fb.start_time <= current_date " + " AND fb.end_time >= current_date"
						+ " AND fb.free_type = 'NORMAL' " + subQueryCondition + orderConditions + " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				
			} else {
				
				queryString = "SELECT fb.* FROM free_books fb , item i" + " WHERE fb.item_id = i.id " + " and fb.status = 1 and i.status = 1" + " and fb.item_id like '" + cat + "%' "
						+ " AND fb.start_time <= current_date " + " AND fb.end_time >= current_date"
						+ " AND fb.free_type = 'NORMAL' " + subQueryCondition + orderConditions + " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
			}
			
			
		} else {
			
			if (cat.equalsIgnoreCase("all")) {
				
				queryString = "SELECT fb.* FROM free_books fb , item i" + " WHERE fb.item_id = i.id " + " and fb.status = 1 and i.status = 1"
						+ " AND fb.start_time <= current_date " + " AND fb.end_time >= current_date"
						+ " AND fb.free_type = 'NORMAL' " + subQueryCondition + orderConditions + " LIMIT 1500";
				
			} else {
				
				queryString = "SELECT fb.* FROM free_books fb , item i" + " WHERE fb.item_id = i.id " + " and fb.status = 1 and i.status = 1" + " and fb.item_id like '" + cat + "%' "
						+ " AND fb.start_time <= current_date " + " AND fb.end_time >= current_date"
						+ " AND fb.free_type = 'NORMAL' " + subQueryCondition + orderConditions + " LIMIT 1500";
			}
			
		}

		List<FreeBook> books = null;

		try {

			String queryCountString = "";
			if (cat.equalsIgnoreCase("all")) {
				queryCountString = "SELECT count(*) FROM free_books fb ,item i WHERE fb.item_id = i.id AND fb.status = 1 AND i.status = 1 AND fb.start_time <= now() AND fb.end_time >= now() AND fb.free_type = 'NORMAL'" +subQueryCondition;
			} else {
				queryCountString = "SELECT count(*) FROM free_books fb ,item i WHERE fb.item_id = i.id AND fb.item_id like '" + cat
						+ "%' AND fb.status = 1 AND i.status = 1  AND fb.start_time <= now() AND fb.end_time >= now() AND fb.free_type = 'NORMAL'" +subQueryCondition;
			}

			total_count = (int) (long) postgres.createNativeQuery(queryCountString).getSingleResult();

			books = postgres.createNativeQuery(queryString, FreeBook.class).getResultList();

		} catch (NoResultException e) {
			total_count = 0;
		}

		FreeBookServletData result = null;

		for (FreeBook freeBook : books) {
			freeBook.setReceive_status("N");
			List<MemberDrmLog> memberDrmLogs = (List<MemberDrmLog>) postgres.createNativeQuery("select * from member_drm_log where member_id = " + StringHelper.escapeSQL(member.getId().toString())+ " and item_id = '" + StringHelper.escapeSQL(freeBook.getItemId()) + "'", MemberDrmLog.class).getResultList();
			for (MemberDrmLog memberDrmLog : memberDrmLogs) {
				if (freeBook.getItemId().equalsIgnoreCase(memberDrmLog.getItem().getId())
						&& freeBook.getCorrelation_id().equalsIgnoreCase(memberDrmLog.getSubmitId())) {
					freeBook.setReceive_status("Y");
					break;
				}
			}
		}

		result = getFreeBookData(postgres, books, total_count, offSet);
		return result;

	}

	public FreeBookServletData getFreeBookByIds(EntityManager postgres, List<String> itemIds) throws ServerException {
		int total_count = 0;
		boolean isSqlError = false;

		try {

			String queryCountString = "";
			String itemIdConditions = "";
			for (String itemId : itemIds) {
				itemIdConditions += "'" + itemId + "',";
			}
			if (StringUtils.isNotBlank(itemIdConditions)) {
				itemIdConditions = itemIdConditions.substring(0, itemIdConditions.length() - 1);
			}else {
				itemIdConditions = "''";
				isSqlError = true;
			}
			queryCountString = "SELECT count(*) FROM free_books fb WHERE status = 1 AND start_time <= now() AND end_time >= now() AND free_type = 'NORMAL' AND item_id IN ("
					+ itemIdConditions + ")";
			total_count = (int) (long) postgres.createNativeQuery(queryCountString).getSingleResult();

		} catch (NoResultException e) {
			total_count = 0;
		}
		FreeBookServletData result = null;
		if (!member.isGuest()) {

			List<FreeBook> books;

			if (pageSize > 0) {

				books = postgres.createNamedQuery("FreeBook.findAllByItemIds", FreeBook.class)
						.setParameter("itemIds", itemIds).setParameter("nowTime", nowTime).getResultList();
			} else {
				books = postgres.createNamedQuery("FreeBook.findAllByItemIds", FreeBook.class)
						.setParameter("itemIds", itemIds).setParameter("nowTime", nowTime).setMaxResults(1500)
						.getResultList();

				total_count = books.size();
			}

//			for (FreeBook freeBook : books) {
//				freeBook.setReceive_status("N");
//				List<MemberDrmLog> memberDrmLogs = (List<MemberDrmLog>) postgres.createNativeQuery("select * from member_drm_log where member_id = "+member.getId()+" and item_id = '"+freeBook.getItemId()+"'", MemberDrmLog.class).getResultList();
//				for (MemberDrmLog memberDrmLog : memberDrmLogs) {
//					if(freeBook.getItemId().equalsIgnoreCase(memberDrmLog.getItem().getId()) && freeBook.getCorrelation_id().equalsIgnoreCase(memberDrmLog.getSubmitId())) {
//						freeBook.setReceive_status("Y");
//						break;
//					}
//				}
//			}
			for (FreeBook freeBook : books) {
				freeBook.setReceive_status("N");
			}
			result = getFreeBookData(postgres, books, total_count, offSet);

		} else {
			if (pageSize > 0) {
				List<FreeBook> books = postgres.createNamedQuery("FreeBook.findAllByItemIds", FreeBook.class)
						.setParameter("itemIds", itemIds).setParameter("nowTime", nowTime).getResultList();
				for (FreeBook freeBook : books) {
					freeBook.setReceive_status("N");
				}
				return getFreeBookData(postgres, books, total_count, offSet);
			} else {
				// cache is still valid => use cache instead.
				List<FreeBook> books = postgres.createNamedQuery("FreeBook.findAllByItemIds", FreeBook.class)
						.setParameter("itemIds", itemIds).setParameter("nowTime", nowTime).setMaxResults(1500)
						.getResultList();
				total_count = books.size();
				for (FreeBook freeBook : books) {
					freeBook.setReceive_status("N");
				}
				result = getFreeBookData(postgres, books, total_count, offSet);
			}

		}
		
		//塞log，查itemIdConditions為空的原因
		if(isSqlError) {
			result.setLog("getFreeBookByIds sql query error.");
		}
		return result;
	}

	public FreeBookServletData getFreeBookData(EntityManager postgres, List<FreeBook> books, int total_count,
			int offSet) throws ServerException {
		FreeBookServletData f = new FreeBookServletData();

		for (FreeBook tmpFreeBook : books) {

			if (null != tmpFreeBook.getReceive_status() && tmpFreeBook.getReceive_status().equalsIgnoreCase("Y")) {

				
				MemberBook memberBook = MemberBook.findNormalByItemId(postgres, member.getId(),
						tmpFreeBook.getItemId());
				if(memberBook!=null) {
					if (memberBook.getArchived() || memberBook.getPassworded()) {
						continue;
					}
				}

			}

			// fix to newest version if possible.
			BookFile bf = tmpFreeBook.getBookFile();

			if (null != bf) {
				bf = bf.getItem().getNewestReadyNormalBookFile(postgres);
				tmpFreeBook.setBookFile(bf);
			}

			// skip problem items
			if (null == tmpFreeBook || null == tmpFreeBook.getBookFile() || null == tmpFreeBook.getBookFile().getItem()
					|| tmpFreeBook.getBookFile().getItem().getStatus() != 1
					|| null == tmpFreeBook.getBookFile().getItem().getInfo()
					|| null == tmpFreeBook.getBookFile().getStatus() || 9 != tmpFreeBook.getBookFile().getStatus()) {
				continue;
			}

			FreeBookServletRecordData sd = getFreeBookServletRecordData(tmpFreeBook);
			sd.getItem_info().setIsbuyout("Y");
			sd.setCurrent_offset(null);
			f.addRecord(sd);
		}

		f.setTotal_records(total_count);
		f.setUpdated_time(CommonUtil.zonedTime());
		f.setCurrent_offset(offSet);
		return f;
	}

	private FreeBookServletRecordData getFreeBookServletRecordData(FreeBook tmpFreeBook) {
		FreeBookServletRecordData rd = new FreeBookServletRecordData();

		ItemInfo info = tmpFreeBook.getBookFile().getItem().getInfo();

		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		if (info instanceof BookInfo) {
			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpFreeBook.getBookFile().getBookUniId());
			rd.setItem_type(tmpFreeBook.getBookFile().getItem().getItemType());

			// Clone
			FreeBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), FreeBookInfo.class);

			// BaseBookInfo
//			bInfo.setO_title(null);
//			bInfo.setPublisher_id(null);
//			bInfo.setCategory(null);
//			bInfo.setRank(null);
//			bInfo.setToc(null);
//			bInfo.setEfile_nofixed_name(null);
//			bInfo.setEfile_fixed_pad_name(null);
//			bInfo.setEfile_fixed_phone_name(null);
//			bInfo.setEfile_url(null);
//			bInfo.setShare_flag(null);
//			bInfo.setStatus(null);
//			bInfo.setReturn_file_num(0);
//			bInfo.setBookGroup(0);
//			bInfo.setLanguage(null);
//			bInfo.setBuffet_flag(null);
//			bInfo.setBuffet_type(null);

			// BookInfo
//			bInfo.setO_author(null);
//			bInfo.setTranslator(null);
//			bInfo.setEditor(null);
//			bInfo.setIllustrator(null);
//			bInfo.setEdition(null);
			bInfo.setForeword(null);
			bInfo.setIntro(null);
			bInfo.setToc(null);
			bInfo.setStart_time(CommonUtil.fromDateToString(tmpFreeBook.getStartTime()));
			bInfo.setEnd_time(CommonUtil.fromDateToString(tmpFreeBook.getEndTime()));
			bInfo.setUpdated_time(CommonUtil.fromDateToString(tmpFreeBook.getLastUpdated()));
			bInfo.setCur_version(tmpFreeBook.getBookFile().getVersion());
			bInfo.setSize(tmpFreeBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpFreeBook.getBookFile().getSize()));

			bInfo.setFree_type(tmpFreeBook.getFree_type());
			bInfo.setFree_start_time(CommonUtil.fromDateToString(tmpFreeBook.getStartTime()));
			bInfo.setFree_end_time(CommonUtil.fromDateToString(tmpFreeBook.getEndTime()));
			bInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(tmpFreeBook.getRead_expire_time()));
			bInfo.setFree_read_days(tmpFreeBook.getRead_days());
			bInfo.setPurchase_area_carea(tmpFreeBook.getPurchase_area_carea());
			bInfo.setPurchase_area_limit(tmpFreeBook.getPurchase_area_limit());
			bInfo.setReceive_status(tmpFreeBook.getReceive_status());

			rd.setItem_info(bInfo);
		} else if (info instanceof MagInfo) {
			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpFreeBook.getBookFile().getBookUniId());
			rd.setItem_type(tmpFreeBook.getBookFile().getItem().getItemType());

			// Clone
			FreeMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), FreeMagInfo.class);

			// BaseBookInfo
//			mInfo.setO_title(null);
//			mInfo.setPublisher_id(null);
//			mInfo.setCategory(null);
//			mInfo.setRank(null);
//			mInfo.setToc(null);
//			mInfo.setEfile_nofixed_name(null);
//			mInfo.setEfile_fixed_pad_name(null);
//			mInfo.setEfile_fixed_phone_name(null);
//			mInfo.setEfile_url(null);
//			mInfo.setShare_flag(null);
//			mInfo.setStatus(null);
//			mInfo.setReturn_file_num(0);
//			mInfo.setBookGroup(0);
//			mInfo.setLanguage(null);
//			mInfo.setBuffet_flag(null);
//			mInfo.setBuffet_type(null);

			// MagInfo
//			mInfo.setMag_id(null);
//			mInfo.setIssue(null);
//			mInfo.setIssue_year(null);
//			mInfo.setPublish_type(null);
//			mInfo.setCover_man(null);
//			mInfo.setCover_story(null);
			mInfo.setToc(null);
			mInfo.setStart_time(CommonUtil.fromDateToString(tmpFreeBook.getStartTime()));
			mInfo.setEnd_time(CommonUtil.fromDateToString(tmpFreeBook.getEndTime()));
			mInfo.setUpdated_time(CommonUtil.fromDateToString(tmpFreeBook.getLastUpdated()));
			mInfo.setCur_version(tmpFreeBook.getBookFile().getVersion());
			mInfo.setSize(tmpFreeBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpFreeBook.getBookFile().getSize()));

			mInfo.setFree_type(tmpFreeBook.getFree_type());
			mInfo.setFree_start_time(CommonUtil.fromDateToString(tmpFreeBook.getStartTime()));
			mInfo.setFree_end_time(CommonUtil.fromDateToString(tmpFreeBook.getEndTime()));
			mInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(tmpFreeBook.getRead_expire_time()));
			mInfo.setFree_read_days(tmpFreeBook.getRead_days());
			mInfo.setPurchase_area_carea(tmpFreeBook.getPurchase_area_carea());
			mInfo.setPurchase_area_limit(tmpFreeBook.getPurchase_area_limit());
			mInfo.setReceive_status(tmpFreeBook.getReceive_status());

			rd.setItem_info(mInfo);
		}else if (info instanceof MediaBookInfo) {
		    
		    rd.setBook_uni_id(tmpFreeBook.getBookFile().getBookUniId());
		    rd.setItem_type(tmpFreeBook.getBookFile().getItem().getItemType());
		    
            MediaBookInfo mi = (MediaBookInfo) info;

            // Clone
            FreeMediaBookInfo freemediaInfo = new Gson().fromJson(new Gson().toJson(mi), FreeMediaBookInfo.class);
            freemediaInfo.setToc(null);
            freemediaInfo.setStart_time(CommonUtil.fromDateToString(tmpFreeBook.getStartTime()));
            freemediaInfo.setEnd_time(CommonUtil.fromDateToString(tmpFreeBook.getEndTime()));
            freemediaInfo.setUpdated_time(CommonUtil.fromDateToString(tmpFreeBook.getLastUpdated()));
            freemediaInfo.setCur_version(tmpFreeBook.getBookFile().getVersion());
            freemediaInfo.setSize(tmpFreeBook.getBookFile().getSize());
            freemediaInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpFreeBook.getBookFile().getSize()));

            freemediaInfo.setFree_type(tmpFreeBook.getFree_type());
            freemediaInfo.setFree_start_time(CommonUtil.fromDateToString(tmpFreeBook.getStartTime()));
            freemediaInfo.setFree_end_time(CommonUtil.fromDateToString(tmpFreeBook.getEndTime()));
            freemediaInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(tmpFreeBook.getRead_expire_time()));
            freemediaInfo.setFree_read_days(tmpFreeBook.getRead_days());
            freemediaInfo.setPurchase_area_carea(tmpFreeBook.getPurchase_area_carea());
            freemediaInfo.setPurchase_area_limit(tmpFreeBook.getPurchase_area_limit());
            freemediaInfo.setReceive_status(tmpFreeBook.getReceive_status());
            rd.setItem_info(freemediaInfo);
            
        }

		return rd;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public java.sql.Timestamp getNowTime() {
		return nowTime;
	}

	public void setNowTime(java.sql.Timestamp nowTime) {
		this.nowTime = nowTime;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
		this.s3PublicBucket = s3PublicBucket;
	}

	public int getOffSet() {
		return offSet;
	}

	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getSort_order() {
		return sort_order;
	}

	public void setSort_order(String sort_order) {
		this.sort_order = sort_order;
	}

	public ArrayList<String> getSub() {
		return sub;
	}

	public void setSub(ArrayList<String> sub) {
		this.sub = sub;
	}
}

class FreeBookServletResultError {
	String error_code;
	String error_message;

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}

class FreeBookServletRecordData {
	String book_uni_id;
	String item_type;
	String updated_time;
	ItemInfo item_info;
	Integer current_offset = 0;

	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ItemInfo getItem_info() {
		return item_info;
	}

	public void setItem_info(ItemInfo item_info) {
		this.item_info = item_info;
	}

	public Integer getCurrent_offset() {
		return current_offset;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}
}

class FreeBookServletData {
	Integer total_records = 20;
	String updated_time;
	ArrayList<FreeBookServletRecordData> records = new ArrayList<>();
	long lastQueryTime = System.currentTimeMillis();
	Integer current_offset = 0;
	String log;

	public Integer getTotal_records() {
		return total_records;
	}

	/**
	 * if this result is older than 60
	 * 
	 * @return
	 */
	public boolean isOld() {
		long now = System.currentTimeMillis();
		long timeDiff = now - lastQueryTime;

		// if no records => old lower to 1 minute.
		if (records.isEmpty()) {
			return true;
		}
		return timeDiff > (1000L * 60 * 60);
	}

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ArrayList<FreeBookServletRecordData> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<FreeBookServletRecordData> records) {
		this.records = records;
	}

	public void addRecord(FreeBookServletRecordData record) {
		records.add(record);
	}

	public Integer getCurrent_offset() {
		return current_offset;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}
	
}