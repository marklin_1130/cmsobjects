package digipages.AppHandler;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.common.CommonUtil;
import digipages.common.DRMLogicUtility;
import digipages.exceptions.ServerException;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import digipages.info.ExtMediaBookInfo;
import digipages.info.FreeBookInfo;
import digipages.info.FreeMagInfo;
import digipages.info.FreeMediaBookInfo;
import model.BookFile;
import model.BookInfo;
import model.DRMInfo;
import model.EplanPackageTime;
import model.FreeBook;
import model.ItemInfo;
import model.MagInfo;
import model.MediaBookInfo;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;

public class SearchBookHandler {
	private Member member;
	private Integer offSet = 0;
	private Integer pageSize = 100;
	private String sortOrder = "";
	private String searchType = "";
	private String searchKeyword = "";
	private String simpleMode = "";
	private String listName = "";
	private String readlistFilter = "";
	private String s3PublicBucket = "";
	private String eplanId = "";
	private boolean isApp ;

	public Object getSearchBook(EntityManager postgres) throws ServerException {
		Object f = null;
		if (member.isThirdParty()) {
			// 博客來帳號/出版社帳號/供應商帳號
			if ("trial".equals(searchType)) {
				// 更多試閱
				if ("*".equals(searchKeyword)) {
					// 搜尋關鍵字等於"*"時,更多試閱列出所有供應商允許的書籍(包含試閱本),根據上架時間desc排序,不用過濾隱藏/密碼/封存(此時一般書單不列出內容)
					f = getThirdPartyAllBook(postgres);
				} else {
					// 搜尋關鍵字不等於"*"時,更多試閱列出符合關鍵字且供應商允許的書籍(包含試閱本),根據上架時間desc排序,不用過濾隱藏/密碼/封存(此時一般書單不列出內容)
					f = getThirdPartySearchAllBook(postgres);
				}
			} else if ("readlist".equals(searchType) && ("all".equals(listName) || "trial".equals(listName) || "archive".equals(listName))) {
				// 我的書籍(一般書單)
				if ("*".equals(searchKeyword)) {
					// 搜尋關鍵字等於"*"時,一般書單不列出內容(已購買書籍,試閱書單,封存書單)
					f = getSearchBookDataNone(postgres);
				} else {
					// 搜尋關鍵字不等於"*"時,一般書單列出內容(已購買書籍,試閱書單,封存書單)
					f = getThirdPartyMemberBook(postgres);
				}
			} else {
				f = getSearchBookDataNone(postgres);
			}
		} else if (member.isNormalReader()) {
			if ("trial".equals(searchType)) {
				// 更多試閱
//				if(isApp) {
//					f = getNormalReaderMoreTrialBookAndFreeBook(postgres);
//				}else {
					f = getNormalReaderMoreTrialBook(postgres);	
//				}
			}else if ("Free".equalsIgnoreCase(searchType)) {
				// 免費領用
				f = getNormalReaderFreeBook(postgres);
			}else if ("eplan".equalsIgnoreCase(searchType)) {
				// 電子書
				f = getNormalReaderEplanBook(postgres);
			} else {
				// 我的書籍(已購買書籍,試閱書單,封存書單)
				f = getNormalReaderMemberBook(postgres);
			}
		} else if (member.isGuest()) {
			
			if ("Free".equalsIgnoreCase(searchType)) {
				// 免費領用
				f = getNormalReaderFreeBook(postgres);
			} else
			
			// 只顯示試閱書籍
			if(isApp) {
//				f = getGuestMoreTrialAndFreeBook(postgres);
				f = getGuestMoreTrialBook(postgres);
			}else {
				f = getGuestMoreTrialBook(postgres);
			}
		}

		return f;
	}

	public SearchBookServletData getThirdPartyMemberBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = thirdPartyQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		// get MemberBook
		List<MemberBook> books = listMemberBook(postgres, resultQuery, searchKeyword);

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				rd.getItem_info().setEplanids(getEplanInfoList(postgres, member.getId(), tmpMb.getItem().getId()));
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getThirdPartyAllBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = thirdPartyAllBookQueryString(member, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listAllBookFile(postgres, resultQuery, searchKeyword);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countAllBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getThirdPartySearchAllBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = thirdPartySearchTrialBookQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listBookFile(postgres, resultQuery, searchKeyword);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getNormalReaderMemberBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = normalQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		// get MemberBook
		List<MemberBook> books = listMemberBook(postgres, resultQuery, searchKeyword);

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				rd.getItem_info().setEplanids(getEplanInfoList(postgres, member.getId(), tmpMb.getItem().getId()));
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}
	
	public SearchBookServletData getNormalReaderMoreTrialBookAndFreeBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = normalSearchTrialAndFreeBookQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listTrialAndFreeBookFile(postgres, resultQuery, searchKeyword);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countTrialAndFreeBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres,tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getNormalReaderMoreTrialBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = normalSearchTrialBookQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listBookFile(postgres, resultQuery, searchKeyword);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}
	
	public FreeBookServletData getNormalReaderFreeBook(EntityManager postgres) throws ServerException {
		FreeBookServletData result = new FreeBookServletData();

		// query
		QueryStringResult resultQuery = normalSearchFreeBookQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listBookFile(postgres, resultQuery, searchKeyword);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			
			List<String> itemIds = new ArrayList<String> ();
			
			for (MemberBook tmpMb : books) {
				
				itemIds.add(tmpMb.getItem().getId());
				
			}
			java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(LocalDateTime.now());
			FreeBookHandler handler = new FreeBookHandler();
			handler.setMember(member);
			handler.setNowTime(nowTime);
			handler.setOffSet(offSet);
			handler.setPageSize(pageSize);
			handler.setS3PublicBucket(s3PublicBucket);
//			handler.setCat(cat);
			
			result = handler.getFreeBookByIds(postgres,itemIds);
		}
		result.setTotal_records(total_count);
		return result;
	}
	
	public SearchBookServletData getNormalReaderEplanBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();
		String booksResponseStr = "";
		try {
			String REDIRECTURL = "https://search.books.com.tw/search/cms_search_erent";
	
			RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(60000)
					.setConnectionRequestTimeout(60000).build();
			CloseableHttpClient httpclient = createSSLClientDefault(defaultRequestConfig);

			HttpPost post = new HttpPost(REDIRECTURL);
			post.addHeader("x-api-key", "15a2329c998e4669ac46ec9ede17abde");
	
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			Integer page = offSet/ pageSize+1;
			urlParameters.add(new BasicNameValuePair("key", searchKeyword));
			urlParameters.add(new BasicNameValuePair("eplanid", eplanId));
			urlParameters.add(new BasicNameValuePair("page", page.toString()));
			urlParameters.add(new BasicNameValuePair("pagecount", pageSize.toString()));
		
			post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));
			
			HttpResponse result= httpclient.execute(post);
			String booksResponse = EntityUtils.toString(result.getEntity(), "UTF-8");
			booksResponseStr = booksResponse;
			Gson gson = new GsonBuilder().create();
			EplanBookServletResult rfb = gson.fromJson(booksResponse, EplanBookServletResult.class);
			
			String err = rfb.getResponseHeader().getErr();
			if("1".equals(err)) {
				
				throw new ServerException("id_err_999", "Books Server Exception:" + rfb.getResponseHeader().getErr_msg());
			}else {
				String[] itemIdArray = rfb.getResponseBody().getProds();
				String itemIdStr = String.join("','",itemIdArray);
				itemIdStr = "'" + itemIdStr + "'"; 
				// query
				QueryStringResult resultQuery = eplanQueryString(member, itemIdStr, readlistFilter,  offSet, pageSize);
				
				List<MemberBook> books = new ArrayList<>();
				
				// get BookFile
				List<BookFile> bookFiles = listBookFile(postgres, resultQuery);

				// add MemberBook
				for (BookFile bookFile : bookFiles) {
					MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);
					sudoMB.getBookFile().getItem().getInfo();
					books.add(sudoMB);
				}

				int total_count = 0;

				try {
					total_count = countBookFile(postgres, resultQuery);
				} catch (NoResultException e) {
					total_count = 0;
				}

				if (total_count > 0) {
		            for (MemberBook tmpMb : books) {
		                SearchBookServletRecordData rd = getSearchBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
		                
		                String eplanExpireDate = null;
		                //取得該本書籍的 月租書籍指定時間 (eplan_package_time) , 如果有的話
						try {
			            	EplanPackageTime eplanPackageTime = 
			            			postgres.createNamedQuery("EplanPackageTime.findByEplanidAndItemId", EplanPackageTime.class)
			                        .setParameter("eplanid", "")
			                        .setParameter("itemId", tmpMb.getItem().getId())
			                        .getSingleResult();
			            	
			            	//is_always_eplan = N 時才要檢查expire_date, Y時表示沒有從月租移除的打算
			            	if("N".equals(eplanPackageTime.getIsAlwaysEplan())){
			            		eplanExpireDate = CommonUtil.getDateToString(eplanPackageTime.getEplanExpireDate());
			            	}
			            	
		            	} catch (NoResultException ex) {
		            		//該本書籍沒有月租書籍指定時間
		            		eplanExpireDate = null;
		            	}
						
						//若月租書籍指定時間小於授權期限，則將授權期限改為月租書籍指定時間
						if(eplanExpireDate != null && !StringUtils.isBlank(rd.getItem_info().getDrm_info().getRead_end_time())){
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX:00");
							String readEndTime = CommonUtil.getDateToString(sdf.parse(rd.getItem_info().getDrm_info().getRead_end_time()));
							if(eplanExpireDate.compareTo(readEndTime) < 0) {
								rd.getItem_info().getDrm_info().setRead_end_time(CommonUtil.normalizeDateTime(eplanExpireDate));
							}
						}
		                
		                rd.getItem_info().setEplanids(getEplanInfoList(postgres, member.getId(), tmpMb.getItem().getId()));
		                ret.addRecord(rd);
		            }
				}
				
				ret.setTotal_records(Integer.valueOf(itemIdArray.length));
			}
		} catch (ServerException e) {
			throw e;
		} catch (Exception e) {
			throw new ServerException("id_err_999", "Fail to connect Books Server:" + e.getMessage() + ". booksResponse:" + booksResponseStr);
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	public SearchBookServletData getGuestMoreTrialBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = guestQueryString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listBookFile(postgres, resultQuery, searchKeyword);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}
	
	public SearchBookServletData getGuestMoreTrialAndFreeBook(EntityManager postgres) throws ServerException {
		SearchBookServletData ret = new SearchBookServletData();

		// query
		QueryStringResult resultQuery = guestQueryWithFreeBookString(member, sortOrder, readlistFilter, searchType, searchKeyword, offSet, pageSize);

		List<MemberBook> books = new ArrayList<>();

		// get BookFile
		List<BookFile> bookFiles = listTrialAndFreeBookFile(postgres, resultQuery, searchKeyword);

		// add MemberBook
		for (BookFile bookFile : bookFiles) {
			MemberBook sudoMB = MemberBook.findMemberBookByBookFile(postgres, member, bookFile);

			books.add(sudoMB);
		}

		int total_count = 0;

		try {
			total_count = countBookFile(postgres, resultQuery, searchKeyword);
		} catch (NoResultException e) {
			total_count = 0;
		}

		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				SearchBookServletRecordData rd = getSearchBookRecordDataAndFreeBookData(postgres, tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode);
				ret.addRecord(rd);
			}
		}

		ret.setSearch_keyword(searchKeyword);
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(CommonUtil.zonedTime());

		return ret;
	}

	private SearchBookServletRecordData getSearchBookRecordData(MemberBook tmpMemberBook, ItemInfo info, String simpleMode) {
		SearchBookServletRecordData rd = new SearchBookServletRecordData();
		DRMInfo drmInfo = new DRMInfo();
		String isBuyout = "Y";

        if (!tmpMemberBook.getIsTrial()&& tmpMemberBook.getMemberDrmLogs()!=null) {
            MemberDrmLog memberDrmLog = DRMLogicUtility.getLongestMemberDrmLog(tmpMemberBook.getMemberDrmLogs());

            if (memberDrmLog != null) {
                String readExpireTime = CommonUtil.normalizeDateTime(memberDrmLog.getReadExpireTime());
                drmInfo.setRead_end_time(readExpireTime);
                drmInfo.setDrm_type(String.valueOf(memberDrmLog.getType()));
                
                if(6 == memberDrmLog.getType()) {
					isBuyout = "N";
				}
            } else {
                drmInfo.setRead_end_time(StringUtils.EMPTY);
                drmInfo.setDrm_type(StringUtils.EMPTY);
            }
        }
        
		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;

		if (null == tmpMemberBook.getStartReadTime()) {
			startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (null == tmpMemberBook.getLastReadTime()) {
			lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (tmpMemberBook.getBookType().toLowerCase().equals("book") || tmpMemberBook.getBookType().toLowerCase().equals("serial") && info instanceof BookInfo) {
			String bookType = "book";

			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);

			bInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
			bInfo.setDrm_info(drmInfo);
			bInfo.setSize(tmpMemberBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			bInfo.setIsbuyout(isBuyout);
			rd.setItem_info(bInfo);
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("magazine")) {
			String bookType = "magazine";

			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
			mInfo.setDrm_info(drmInfo);
			mInfo.setSize(tmpMemberBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			mInfo.setIsbuyout(isBuyout);
			rd.setItem_info(mInfo);
		}else if (tmpMemberBook.getBookType().toLowerCase().equals("mediabook")) {
            String bookType = "mediabook";

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            mInfo.setIsbuyout(isBuyout);
            rd.setItem_info(mInfo);
        }else if (tmpMemberBook.getBookType().toLowerCase().equals("audiobook")) {
            String bookType = "audiobook";

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            mInfo.setIsbuyout(isBuyout);
            rd.setItem_info(mInfo);
        }else if (tmpMemberBook.getBookType().toLowerCase().equals("serial") && info instanceof MediaBookInfo) {
            String bookType ="mediabook";
            if(tmpMemberBook.getItem().getId().contains("E07")) {
                 bookType = "audiobook";
            }else {
                 bookType = "mediabook";
            }
            
            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            mInfo.setIsbuyout(isBuyout);
            rd.setItem_info(mInfo);
            
        }

		return rd;
	}
	
	private SearchBookServletRecordData getSearchFreeBookRecordData(MemberBook tmpMemberBook, ItemInfo info, String simpleMode) {
		SearchBookServletRecordData rd = new SearchBookServletRecordData();
		DRMInfo drmInfo = new DRMInfo();

        if (!tmpMemberBook.getIsTrial()&& tmpMemberBook.getMemberDrmLogs()!=null) {
            MemberDrmLog memberDrmLog = DRMLogicUtility.getLongestMemberDrmLog(tmpMemberBook.getMemberDrmLogs());

            if (memberDrmLog != null) {
            	rd.setReceive_status("Y");
                String readExpireTime = CommonUtil.normalizeDateTime(memberDrmLog.getReadExpireTime());
                drmInfo.setRead_end_time(readExpireTime);
                drmInfo.setDrm_type(String.valueOf(memberDrmLog.getType()));
            } else {
                drmInfo.setRead_end_time(StringUtils.EMPTY);
                drmInfo.setDrm_type(StringUtils.EMPTY);
            }
        }
        
		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;

		if (null == tmpMemberBook.getStartReadTime()) {
			startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (null == tmpMemberBook.getLastReadTime()) {
			lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (tmpMemberBook.getBookType().toLowerCase().equals("book") || tmpMemberBook.getBookType().toLowerCase().equals("serial") && info instanceof BookInfo) {
			String bookType = "book";

			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);

			bInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
			bInfo.setDrm_info(drmInfo);
			bInfo.setSize(tmpMemberBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(bInfo);
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("magazine")) {
			String bookType = "magazine";

			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
			mInfo.setDrm_info(drmInfo);
			mInfo.setSize(tmpMemberBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(mInfo);
		}else if (tmpMemberBook.getBookType().toLowerCase().equals("mediabook")) {
            String bookType = "mediabook";

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        }else if (tmpMemberBook.getBookType().toLowerCase().equals("audiobook")) {
            String bookType = "audiobook";

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        }else if (tmpMemberBook.getBookType().toLowerCase().equals("serial") && info instanceof MediaBookInfo) {
            String bookType ="mediabook";
            if(tmpMemberBook.getItem().getId().contains("E07")) {
                 bookType = "audiobook";
            }else {
                 bookType = "mediabook";
            }

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            // Clone
            ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);

            mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
            mInfo.setDrm_info(drmInfo);
            mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//            mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
            rd.setItem_info(mInfo);
        }

		return rd;
	}
	
	public SearchBookServletRecordData getSearchBookRecordDataAndFreeBookData(EntityManager postgres ,MemberBook tmpMemberBook, ItemInfo info, String simpleMode) {
		SearchBookServletRecordData rd = new SearchBookServletRecordData();
		DRMInfo drmInfo = new DRMInfo();

        if (!tmpMemberBook.getIsTrial()&& tmpMemberBook.getMemberDrmLogs()!=null) {
            MemberDrmLog memberDrmLog = DRMLogicUtility.getLongestMemberDrmLog(tmpMemberBook.getMemberDrmLogs());

            if (memberDrmLog != null) {
                String readExpireTime = CommonUtil.normalizeDateTime(memberDrmLog.getReadExpireTime());
                drmInfo.setRead_end_time(readExpireTime);
                drmInfo.setDrm_type(String.valueOf(memberDrmLog.getType()));
            } else {
                drmInfo.setRead_end_time(StringUtils.EMPTY);
                drmInfo.setDrm_type(StringUtils.EMPTY);
            }
        }
        
		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;

		if (null == tmpMemberBook.getStartReadTime()) {
			startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (null == tmpMemberBook.getLastReadTime()) {
			lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (tmpMemberBook.getBookType().toLowerCase().equals("book") || tmpMemberBook.getBookType().toLowerCase().equals("serial") && info instanceof BookInfo) {
			String bookType = "book";

			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			if(tmpMemberBook.getBookFile().getIsTrial()) {
				
				ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);
				bInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
				bInfo.setDrm_info(drmInfo);
				bInfo.setSize(tmpMemberBook.getBookFile().getSize());
				bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
				rd.setItem_info(bInfo);
				
			}else {
				java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(LocalDateTime.now());
				List<FreeBook> freeBookList = postgres.createNamedQuery("FreeBook.findAvailableByItemId", FreeBook.class)
						.setParameter("nowTime", nowTime)
						.setParameter("itemId", tmpMemberBook.getItem().getId()).getResultList();
				FreeBook freebook = freeBookList.size() == 0 ? null : freeBookList.get(0);
				
				FreeBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), FreeBookInfo.class);
				bInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
				bInfo.setDrm_info(drmInfo);
				bInfo.setSize(tmpMemberBook.getBookFile().getSize());
				bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
				if(freebook!=null) {
					bInfo.setFree_type(freebook.getFree_type());
					bInfo.setFree_start_time(CommonUtil.fromDateToString(freebook.getStartTime()));
					bInfo.setFree_end_time(CommonUtil.fromDateToString(freebook.getEndTime()));
					bInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(freebook.getRead_expire_time()));
					bInfo.setFree_read_days(freebook.getRead_days());
					bInfo.setPurchase_area_carea(freebook.getPurchase_area_carea());
					bInfo.setPurchase_area_limit(freebook.getPurchase_area_limit());
					bInfo.setReceive_status("N");
				}
				rd.setItem_info(bInfo);
			}
			
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("magazine")) {
			String bookType = "magazine";

			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			if(tmpMemberBook.getBookFile().getIsTrial()) {
				// Clone
				ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);
				mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
				mInfo.setDrm_info(drmInfo);
				mInfo.setSize(tmpMemberBook.getBookFile().getSize());
				mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
				rd.setItem_info(mInfo);
			}else {
				java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(LocalDateTime.now());
				List<FreeBook> freeBookList = postgres.createNamedQuery("FreeBook.findAvailableByItemId", FreeBook.class)
						.setParameter("nowTime", nowTime)
						.setParameter("itemId", tmpMemberBook.getItem().getId()).getResultList();
				FreeBook freebook = freeBookList.size() == 0 ? null : freeBookList.get(0);
				// Clone
				FreeMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), FreeMagInfo.class);
				mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
				mInfo.setDrm_info(drmInfo);
				mInfo.setSize(tmpMemberBook.getBookFile().getSize());
				mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
				if(freebook!=null) {
					mInfo.setFree_type(freebook.getFree_type());
					mInfo.setFree_start_time(CommonUtil.fromDateToString(freebook.getStartTime()));
					mInfo.setFree_end_time(CommonUtil.fromDateToString(freebook.getEndTime()));
					mInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(freebook.getRead_expire_time()));
					mInfo.setFree_read_days(freebook.getRead_days());
					mInfo.setPurchase_area_carea(freebook.getPurchase_area_carea());
					mInfo.setPurchase_area_limit(freebook.getPurchase_area_limit());
					mInfo.setReceive_status("N");
				}
				rd.setItem_info(mInfo);
			}

		}else if (tmpMemberBook.getBookType().toLowerCase().equals("mediabook")) {
            String bookType = "mediabook";

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            if(tmpMemberBook.getBookFile().getIsTrial()) {
                // Clone
                ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);
                mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
                mInfo.setDrm_info(drmInfo);
                mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//                mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
                rd.setItem_info(mInfo);
            }else {
                java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(LocalDateTime.now());
                List<FreeBook> freeBookList = postgres.createNamedQuery("FreeBook.findAvailableByItemId", FreeBook.class)
                        .setParameter("nowTime", nowTime)
                        .setParameter("itemId", tmpMemberBook.getItem().getId()).getResultList();
				FreeBook freebook = freeBookList.size() == 0 ? null : freeBookList.get(0);
                // Clone
                FreeMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), FreeMediaBookInfo.class);
                mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
                mInfo.setDrm_info(drmInfo);
                mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//                mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
                if(freebook!=null) {
                    mInfo.setFree_type(freebook.getFree_type());
                    mInfo.setFree_start_time(CommonUtil.fromDateToString(freebook.getStartTime()));
                    mInfo.setFree_end_time(CommonUtil.fromDateToString(freebook.getEndTime()));
                    mInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(freebook.getRead_expire_time()));
                    mInfo.setFree_read_days(freebook.getRead_days());
                    mInfo.setPurchase_area_carea(freebook.getPurchase_area_carea());
                    mInfo.setPurchase_area_limit(freebook.getPurchase_area_limit());
                    mInfo.setReceive_status("N");
                }
                rd.setItem_info(mInfo);
            }

        }else if (tmpMemberBook.getBookType().toLowerCase().equals("audiobook")) {
            String bookType = "audiobook";

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            if(tmpMemberBook.getBookFile().getIsTrial()) {
                // Clone
                ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);
                mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
                mInfo.setDrm_info(drmInfo);
                mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//                mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
                rd.setItem_info(mInfo);
            }else {
                java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(LocalDateTime.now());
                List<FreeBook> freeBookList = postgres.createNamedQuery("FreeBook.findAvailableByItemId", FreeBook.class)
                        .setParameter("nowTime", nowTime)
                        .setParameter("itemId", tmpMemberBook.getItem().getId()).getResultList();
				FreeBook freebook = freeBookList.size() == 0 ? null : freeBookList.get(0);
                // Clone
                FreeMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), FreeMediaBookInfo.class);
                mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
                mInfo.setDrm_info(drmInfo);
                mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//                mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
                if(freebook!=null) {
                    mInfo.setFree_type(freebook.getFree_type());
                    mInfo.setFree_start_time(CommonUtil.fromDateToString(freebook.getStartTime()));
                    mInfo.setFree_end_time(CommonUtil.fromDateToString(freebook.getEndTime()));
                    mInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(freebook.getRead_expire_time()));
                    mInfo.setFree_read_days(freebook.getRead_days());
                    mInfo.setPurchase_area_carea(freebook.getPurchase_area_carea());
                    mInfo.setPurchase_area_limit(freebook.getPurchase_area_limit());
                    mInfo.setReceive_status("N");
                }
                rd.setItem_info(mInfo);
            }

        }else if (tmpMemberBook.getBookType().toLowerCase().equals("serial") && info instanceof MediaBookInfo) {
            String bookType ="mediabook";
            if(tmpMemberBook.getItem().getId().contains("E07")) {
                 bookType = "audiobook";
            }else {
                 bookType = "mediabook";
            }

            MediaBookInfo mi = (MediaBookInfo) info;

            rd.setBook_uni_id(tmpMemberBook.getBookUniId());
            // rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
            rd.setItem_type(bookType);

            if(tmpMemberBook.getBookFile().getIsTrial()) {
                // Clone
                ExtMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMediaBookInfo.class);
                mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
                mInfo.setDrm_info(drmInfo);
                mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//                mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
                rd.setItem_info(mInfo);
            }else {
                java.sql.Timestamp nowTime = java.sql.Timestamp.valueOf(LocalDateTime.now());
                List<FreeBook> freeBookList = postgres.createNamedQuery("FreeBook.findAvailableByItemId", FreeBook.class)
                        .setParameter("nowTime", nowTime)
                        .setParameter("itemId", tmpMemberBook.getItem().getId()).getResultList();
				FreeBook freebook = freeBookList.size() == 0 ? null : freeBookList.get(0);
                // Clone
                FreeMediaBookInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), FreeMediaBookInfo.class);
                mInfo.setSearchTrialBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime);
                mInfo.setDrm_info(drmInfo);
                mInfo.setSize(tmpMemberBook.getBookFile().getSize());
//                mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
                if(freebook!=null) {
                    mInfo.setFree_type(freebook.getFree_type());
                    mInfo.setFree_start_time(CommonUtil.fromDateToString(freebook.getStartTime()));
                    mInfo.setFree_end_time(CommonUtil.fromDateToString(freebook.getEndTime()));
                    mInfo.setFree_read_expire_time(CommonUtil.normalizeDateTime(freebook.getRead_expire_time()));
                    mInfo.setFree_read_days(freebook.getRead_days());
                    mInfo.setPurchase_area_carea(freebook.getPurchase_area_carea());
                    mInfo.setPurchase_area_limit(freebook.getPurchase_area_limit());
                    mInfo.setReceive_status("N");
                }
                rd.setItem_info(mInfo);
            }
        }

		return rd;
	}
	
	private List<String> getEplanInfoList(EntityManager postgres, Long memberId, String itemId) {
		List<String> eplanids = new ArrayList<String>();
        
//        @SuppressWarnings("unchecked")
//		List<String> temp = postgres.createNativeQuery("SELECT mdl.submit_id FROM member_drm_log mdl join eplan e on mdl.submit_id  = e.eplanid WHERE mdl.member_id = " + memberId + " and mdl.item_id = '" + itemId + "' and mdl.status = '1' ORDER BY mdl.submit_id ").getResultList();
        String eplanListQueryString = "select e.eplan_id  FROM member_drm_log mdl , member_eplan_drm_log e "
           		+ " WHERE mdl.member_id = e.member_id"
           		+ " and mdl.submit_id = e.eplan_id"
           		+ " and mdl.transaction_id = e.transaction_id"
           		+ " and mdl.member_id = "+memberId
           		+ " and mdl.item_id = '"+itemId+"'"
           		+ " and mdl.type='6'"
           		+ " and mdl.status=1"
           		+ " and to_timestamp(mdl.read_expire_time ,'YYYY/MM/DD HH24:MI:Ss')  >= now() "
           		+ " and to_timestamp(e.read_expire_time ,'YYYY/MM/DD HH24:MI:Ss')  >= now() ORDER BY mdl.submit_id"; 
        
        @SuppressWarnings("unchecked")
        List<String> temp = postgres.createNativeQuery( eplanListQueryString ).getResultList();
        
		if(temp != null) {
			for(String eplanId : temp) {
				eplanids.add(eplanId);
			}
		}
		
		return eplanids;
	}

	private List<MemberBook> listMemberBook(EntityManager postgres, QueryStringResult queryResult, String searchKeyword) throws ServerException {
		@SuppressWarnings("unchecked")
		List<MemberBook> books = postgres.createNativeQuery(queryResult.getQueryString(), MemberBook.class)
				.setParameter(1, "%" + searchKeyword + "%")
				.setParameter(2, "%" + searchKeyword + "%")
				.setParameter(3, "%" + searchKeyword + "%")
				.setHint("javax.persistence.cache.storeMode", "REFRESH")
				.getResultList();

		return books;
	}

	private List<BookFile> listAllBookFile(EntityManager postgres, QueryStringResult queryResult, String searchKeyword) throws ServerException {
		@SuppressWarnings("unchecked")
		List<BookFile> bf = postgres.createNativeQuery(queryResult.getQueryString(), BookFile.class).getResultList();

		return bf;
	}

	private int countAllBookFile(EntityManager postgres, QueryStringResult queryResult, String searchKeyword) throws ServerException {
		int total_count = 0;

		total_count = (int) (long) postgres.createNativeQuery(queryResult.getCountQueryString()).getSingleResult();

		return total_count;
	}

	private List<BookFile> listBookFile(EntityManager postgres, QueryStringResult queryResult, String searchKeyword) throws ServerException {
		@SuppressWarnings("unchecked")
		List<BookFile> bf = postgres.createNativeQuery(queryResult.getQueryString(), BookFile.class)
				.setParameter(1, "%" + searchKeyword + "%")
				.setParameter(2, "%" + searchKeyword + "%")
				.setParameter(3, "%" + searchKeyword + "%")
				.getResultList();

		return bf;
	}
	
	private List<BookFile> listBookFile(EntityManager postgres, QueryStringResult queryResult) throws ServerException {
		@SuppressWarnings("unchecked")
		List<BookFile> bf = postgres.createNativeQuery(queryResult.getQueryString(), BookFile.class)
				.getResultList();
		return bf;
	}
	
	private List<BookFile> listTrialAndFreeBookFile(EntityManager postgres, QueryStringResult queryResult, String searchKeyword) throws ServerException {
		@SuppressWarnings("unchecked")
		List<BookFile> bf = postgres.createNativeQuery(queryResult.getQueryString(), BookFile.class)
				.setParameter(1, "%" + searchKeyword + "%")
				.setParameter(2, "%" + searchKeyword + "%")
				.setParameter(3, "%" + searchKeyword + "%")
				.setParameter(4, "%" + searchKeyword + "%")
				.setParameter(5, "%" + searchKeyword + "%")
				.setParameter(6, "%" + searchKeyword + "%")
				.getResultList();

		return bf;
	}

	private int countBookFile(EntityManager postgres, QueryStringResult queryResult, String searchKeyword) throws ServerException {
		int total_count = 0;

		total_count = (int) (long) postgres.createNativeQuery(queryResult.getCountQueryString())
				.setParameter(1, "%" + searchKeyword + "%")
				.setParameter(2, "%" + searchKeyword + "%")
				.setParameter(3, "%" + searchKeyword + "%")
				.getSingleResult();

		return total_count;
	}
	
	private int countBookFile(EntityManager postgres, QueryStringResult queryResult) throws ServerException {
		int total_count = 0;

		total_count = (int) (long) postgres.createNativeQuery(queryResult.getCountQueryString())
				.getSingleResult();

		return total_count;
	}
	
	private int countTrialAndFreeBookFile(EntityManager postgres, QueryStringResult queryResult, String searchKeyword) throws ServerException {
		int total_count = 0;

		total_count = (int) (long) postgres.createNativeQuery(queryResult.getCountQueryString())
				.setParameter(1, "%" + searchKeyword + "%")
				.setParameter(2, "%" + searchKeyword + "%")
				.setParameter(3, "%" + searchKeyword + "%")
				.setParameter(4, "%" + searchKeyword + "%")
				.setParameter(5, "%" + searchKeyword + "%")
				.setParameter(6, "%" + searchKeyword + "%")
				.getSingleResult();

		return total_count;
	}

	private QueryStringResult thirdPartyQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";
		String thirdPartyTypeFilter = "";
		int publisher_id = 0;
		int vendor_id = 0;

		if ("PublisherReader".equals(member.getMemberType()) && null != member.getPublisher()) {
			publisher_id = member.getPublisher().getId();
			thirdPartyTypeFilter = " AND it.publisher_id = " + publisher_id;
		}

		if ("VendorReader".equals(member.getMemberType()) && null != member.getVendor()) {
			vendor_id = member.getVendor().getId();
			thirdPartyTypeFilter = " AND it.vendor_id = " + vendor_id;
		}

		queryString = "SELECT mb.* FROM member_book mb"
				+ " INNER JOIN item it ON mb.item_id = it.id"
				+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
				//限制isbuyout = Y
				+ " LEFT JOIN ( "
				+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
				+ " where mdl.status=1 and mdl.member_id = " + member.getId()
				+ " and mdl.type <> 6 "
				+ " GROUP BY item_id) a on mb.item_id = a.item_id "
				+ " WHERE bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND mb.member_id = " + member.getId() + ""
				+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
				+ thirdPartyTypeFilter
				+ readlistFilter
				+ " ORDER BY mb.create_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM member_book mb"
				+ " INNER JOIN item it ON mb.item_id = it.id"
				+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
				//限制isbuyout = Y
				+ " LEFT JOIN ( "
				+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
				+ " where mdl.status=1 and mdl.member_id = " + member.getId()
				+ " and mdl.type <> 6 "
				+ " GROUP BY item_id) a on mb.item_id = a.item_id "
				+ " WHERE bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND mb.member_id = " + member.getId() + ""
				+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
				+ thirdPartyTypeFilter
				+ readlistFilter;

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult thirdPartyAllBookQueryString(Member member, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";
		String thirdPartyTypeFilter = "";
		int publisher_id = 0;
		int vendor_id = 0;

		if ("PublisherReader".equals(member.getMemberType()) && null != member.getPublisher()) {
			publisher_id = member.getPublisher().getId();
			thirdPartyTypeFilter = " AND it.publisher_id = " + publisher_id;
		}

		if ("VendorReader".equals(member.getMemberType()) && null != member.getVendor()) {
			vendor_id = member.getVendor().getId();
			thirdPartyTypeFilter = " AND it.vendor_id = " + vendor_id;
		}

		queryString = "SELECT * FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.* FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " WHERE bf.version = (SELECT MAX(bf_self.version) FROM book_file bf_self WHERE bf_self.id = bf.id)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") x"
				+ " ORDER BY x.udt::varchar::timestamp DESC, x.create_time DESC, x.item_id DESC"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.udt, MAX(bf.version) OVER (partition by bf.version) AS max_version FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " WHERE (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") AS temp";

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult thirdPartySearchTrialBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";
		String thirdPartyTypeFilter = "";
		int publisher_id = 0;
		int vendor_id = 0;

		if ("PublisherReader".equals(member.getMemberType()) && null != member.getPublisher()) {
			publisher_id = member.getPublisher().getId();
			thirdPartyTypeFilter = " AND it.publisher_id = " + publisher_id;
		}

		if ("VendorReader".equals(member.getMemberType()) && null != member.getVendor()) {
			vendor_id = member.getVendor().getId();
			thirdPartyTypeFilter = " AND it.vendor_id = " + vendor_id;
		}

		queryString = "SELECT * FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.* FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " WHERE (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
				+ " AND bf.version = (SELECT MAX(bf_self.version) FROM book_file bf_self WHERE bf_self.id = bf.id)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") x"
				+ " ORDER BY x.udt::varchar::timestamp DESC, x.create_time DESC, x.item_id DESC"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM"
				+ " (SELECT DISTINCT ON (bf.item_id, bf.is_trial, bf.format) bf.udt, MAX(bf.version) OVER (partition by bf.version) AS max_version FROM book_file bf"
				+ " INNER JOIN item it ON bf.item_id = it.id"
				+ " WHERE (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
				+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
				+ " AND bf.status >= 8"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ thirdPartyTypeFilter + ") AS temp";

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult normalQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		switch (sortOrder) {
			case "default":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY mb.create_time DESC NULLS LAST"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "timedesc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY mb.create_time DESC NULLS LAST, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "timeasc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY mb.create_time ASC NULLS LAST, it.c_title COLLATE \"C\" DESC, it.author COLLATE \"C\" DESC, it.publisher_name COLLATE \"C\" DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "name":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY it.c_title COLLATE \"C\" NULLS FIRST, mb.create_time DESC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "publisher":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY it.publisher_name COLLATE \"C\" NULLS FIRST, mb.create_time DESC, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "author":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY it.author COLLATE \"C\" NULLS FIRST, mb.create_time DESC, it.c_title COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "readtimedesc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY mb.last_read_time DESC NULLS LAST, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "readtimeasc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY mb.last_read_time ASC NULLS LAST, it.c_title COLLATE \"C\" DESC, it.author COLLATE \"C\" DESC, it.publisher_name COLLATE \"C\" DESC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
				break;
			case "unreadtimedesc":
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " AND mb.start_read_time IS NULL"
						+ " ORDER BY mb.last_read_time DESC NULLS LAST, it.c_title COLLATE \"C\" ASC, it.author COLLATE \"C\" ASC, it.publisher_name COLLATE \"C\" ASC"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " AND mb.start_read_time IS NULL";
				break;
			default:
				queryString = "SELECT mb.* FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter
						+ " ORDER BY mb.create_time DESC NULLS LAST"
						+ " OFFSET " + offSet + ""
						+ " LIMIT " + pageSize;
				countQueryString = "SELECT COUNT(*) FROM member_book mb"
						+ " INNER JOIN item it ON mb.item_id = it.id"
						+ " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
						//限制isbuyout = Y
						+ " LEFT JOIN ( "
						+ " SELECT max(read_expire_time) as max_read_expire_time, mdl.item_id FROM  member_drm_log mdl "
						+ " where mdl.status=1 and mdl.member_id = " + member.getId()
						+ " and mdl.type <> 6 "
						+ " GROUP BY item_id) a on mb.item_id = a.item_id "
						+ " WHERE bf.status = 9"
						+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
						+ " AND mb.member_id = " + member.getId() + ""
						+ " AND (it.c_title ILIKE ? OR it.author ILIKE ? OR it.publisher_name ILIKE ?)"
						+ " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine' OR lower(it.item_type) = 'mediabook' OR lower(it.item_type) = 'audiobook')"
						+ " AND (a.max_read_expire_time is not null or mb.is_trial=true) "
						+ readlistFilter;
		}

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult normalSearchTrialAndFreeBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString ="SELECT * FROM (" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ? )" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its " + 
				" ON bf.item_id = its.id " + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND is_trial = TRUE AND action = 'update')" + 
				" AND bf.status = 9 " + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = TRUE" + 
				" UNION" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND is_trial = FALSE AND action = 'update')" + 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
				" ) x " + 
				" ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		
		countQueryString =" SELECT COUNT(*) FROM (" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ? )" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its " + 
				" ON bf.item_id = its.id" + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND is_trial = TRUE AND action = 'update')" + 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = TRUE" + 
				" UNION" + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() +" AND is_trial = FALSE AND action = 'update')" + 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
				" ) x " ;

		return new QueryStringResult(queryString, countQueryString);
	}
	
	private QueryStringResult normalSearchTrialBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its ON bf.item_id = its.id"
				//不排除已加入書櫃的書
//				+ " WHERE bf.item_id NOT IN (SELECT DISTINCT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = TRUE AND action = 'update' AND is_hidden = FALSE)"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = TRUE) x"
				+ " ORDER BY x.create_time DESC NULLS LAST, x.c_title COLLATE \"C\" ASC, x.author COLLATE \"C\" ASC, x.publisher_name COLLATE \"C\" ASC"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its ON bf.item_id = its.id"
				//不排除已加入書櫃的書
//				+ " WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = TRUE AND action = 'update' AND is_hidden = FALSE)"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = TRUE) x";

		return new QueryStringResult(queryString, countQueryString);
	}
	
	private QueryStringResult normalSearchFreeBookQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM "
				+ " (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN "
				+ " (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now())) "
				+ " AND (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its"
				+ " ON bf.item_id = its.id"
				//不排除已加入書櫃的書
//				+ " WHERE bf.item_id NOT IN (SELECT DISTINCT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = FALSE AND action = 'update')"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = FALSE) x"
				+ " ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM "
				+ " (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN "
				+ " (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now())) "
				+ " AND (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1) AS its ON bf.item_id = its.id"
				//不排除已加入書櫃的書
//				+ " WHERE bf.item_id NOT IN (SELECT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = FALSE AND action = 'update' )"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ " AND bf.is_trial = FALSE) x";

		return new QueryStringResult(queryString, countQueryString);
	}

	private QueryStringResult guestQueryWithFreeBookString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ""+
			    " UNION " + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE 1=1 "+ 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
			    ") x"
				+ " ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ""+
			    " UNION " + 
				" SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf " + 
				" INNER JOIN " + 
				" (SELECT id, c_title, author, publisher_name FROM item" + 
				" WHERE id in (select item_id from free_books where free_type = 'NORMAL' and status = 1 and (start_time <= now() and end_time >= now()))" + 
				" and (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)" + 
				" AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')" + 
				" AND status = 1) AS its" + 
				" ON bf.item_id = its.id" + 
				" WHERE 1=1 "+ 
				" AND bf.status = 9" + 
				" AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')" + 
				" AND bf.is_trial = FALSE" + 
			    ") x";

		return new QueryStringResult(queryString, countQueryString);
	}
	

	private QueryStringResult guestQueryString(Member member, String sortOrder, String readlistFilter, String searchType, String searchKeyword, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		queryString = "SELECT * FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ") x"
				+ " ORDER BY x.create_time DESC NULLS LAST"
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		countQueryString = "SELECT COUNT(*) FROM (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, bf.* FROM book_file bf"
				+ " INNER JOIN (SELECT id, c_title, author, publisher_name FROM item"
				+ " WHERE (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?)"
				+ " AND (lower(item_type) = 'book' OR lower(item_type) = 'magazine' OR lower(item_type) = 'mediabook' OR lower(item_type) = 'audiobook')"
				+ " AND status = 1)"
				+ " AS its ON bf.item_id = its.id"
				+ " WHERE bf.status = 9"
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio')"
				+ readlistFilter + ") x";

		return new QueryStringResult(queryString, countQueryString);
	}
	
	private QueryStringResult eplanQueryString(Member member, String itemIds, String readlistFilter,  int offSet, int pageSize) {
		
		String queryString = "SELECT * FROM "
				+ " (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf "
				+ " INNER JOIN "
				+ " (SELECT id, c_title, author, publisher_name FROM item "
				+ " WHERE id in (" + itemIds + ")  "
				//串日旭回傳結果不再過濾keyword
//				+ " AND (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?) "
				+ " AND lower(item_type) in('book','magazine','mediabook','audiobook') "
				+ " AND status = 1) AS its ON bf.item_id = its.id "
				//不排除已加入書櫃的書
//				+ " WHERE bf.item_id NOT IN (SELECT DISTINCT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = FALSE AND action = 'update')"
				+ " WHERE bf.status = 9 "
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio') "
				+ " AND bf.is_trial = false ) x "
				+ " ORDER BY x.create_time DESC NULLS LAST "
				+ " OFFSET " + offSet + ""
				+ " LIMIT " + pageSize;
		String countQueryString = "SELECT COUNT(*) FROM "
				+ " (SELECT DISTINCT ON (bf.item_id, bf.format, bf.is_trial) its.c_title, its.author, its.publisher_name, bf.* FROM book_file bf "
				+ " INNER JOIN "
				+ " (SELECT id, c_title, author, publisher_name FROM item "
				+ " WHERE id in (" + itemIds + ")  "
				//串日旭回傳結果不再過濾keyword
//				+ " AND (c_title ILIKE ? OR author ILIKE ? OR publisher_name ILIKE ?) "
				+ " AND lower(item_type) in('book','magazine','mediabook','audiobook') "
				+ " AND status = 1) AS its ON bf.item_id = its.id "
				//不排除已加入書櫃的書
//				+ " WHERE bf.item_id NOT IN (SELECT DISTINCT item_id FROM member_book WHERE member_id = " + member.getId() + " AND is_trial = FALSE AND action = 'update')"
				+ " WHERE bf.status = 9 "
				+ " AND bf.format IN ('pdf','reflowable','fixedlayout','media','audio') "
				+ " AND bf.is_trial = false ) x ";

		return new QueryStringResult(queryString, countQueryString);
	}
	
	private static CloseableHttpClient createSSLClientDefault(RequestConfig defaultRequestConfig) {
		try {
			SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
				// 信任所有
				public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					return true;
				}
			}).build();

			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, new HostnameVerifier() {
				@Override
				public boolean verify(String s, SSLSession sslSession) {
					return true;
				}
			});

			return HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).setSSLSocketFactory(sslsf)
					.build();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return HttpClients.createDefault();
	}

	public SearchBookServletDataNone getSearchBookDataNone(EntityManager postgres) throws ServerException {
		SearchBookServletDataNone f = new SearchBookServletDataNone();

		int totalCount = 0;

		f.addRecord(new ArrayList<String>());
		f.setSearch_keyword(searchKeyword);
		f.setTotal_records(totalCount);
		f.setCurrent_offset(offSet);
		f.setUpdated_time(CommonUtil.zonedTime());

		return f;
	}

	public SearchBookServletResultError getSearchBookDataError(EntityManager postgres) throws ServerException {
		SearchBookServletResultError f = new SearchBookServletResultError();

		f.setError_code("id_err_311");
		f.setError_message("Search results has reached the upper limit.");

		return f;
	}

	public static String getLineInfo() {
		StackTraceElement ste = new Throwable().getStackTrace()[1];
		return ste.getFileName() + ": Line " + ste.getLineNumber() + "/";
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Integer getOffSet() {
		return offSet;
	}

	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public String getSimpleMode() {
		return simpleMode;
	}

	public void setSimpleMode(String simpleMode) {
		this.simpleMode = simpleMode;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	public String getReadlistFilter() {
		return readlistFilter;
	}

	public void setReadlistFilter(String readlistFilter) {
		this.readlistFilter = readlistFilter;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
		this.s3PublicBucket = s3PublicBucket;
	}

	public String getEplanId() {
		return eplanId;
	}

	public void setEplanId(String eplanId) {
		this.eplanId = eplanId;
	}

	public boolean isApp() {
		return isApp;
	}

	public void setApp(boolean isApp) {
		this.isApp = isApp;
	}
}

class SearchBookServletResultError {
	String error_code;
	String error_message;

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}

class SearchBookServletRecordData {
	String book_uni_id;
	String item_type;
	String updated_time;
	ItemInfo item_info;

	@Transient
    private String receive_status ;
	
	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ItemInfo getItem_info() {
		return item_info;
	}

	public void setItem_info(ItemInfo item_info) {
		this.item_info = item_info;
	}

	public String getReceive_status() {
		return receive_status;
	}

	public void setReceive_status(String receive_status) {
		this.receive_status = receive_status;
	}
}

class SearchBookServletData {
	String search_keyword;
	Integer total_records = 100;
	Integer current_offset = 0;
	String updated_time;
	ArrayList<SearchBookServletRecordData> records = new ArrayList<>();

	public String getSearch_keyword() {
		return search_keyword;
	}

	public void setSearch_keyword(String search_keyword) {
		this.search_keyword = search_keyword;
	}

	public Integer getTotal_records() {
		return total_records;
	}

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public Integer getCurrent_offset() {
		return current_offset;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ArrayList<SearchBookServletRecordData> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<SearchBookServletRecordData> records) {
		this.records = records;
	}

	public void addRecord(SearchBookServletRecordData record) {
		records.add(record);
	}
}

class SearchBookServletDataNone {
	String search_keyword;
	Integer total_records = 0;
	Integer current_offset = 0;
	String updated_time;
	List<String> records;

	public void setSearch_keyword(String search_keyword) {
		this.search_keyword = search_keyword;
	}

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public void addRecord(List<String> record) {
		this.records = record;
	}
}

class EplanBookServletResult {
	EplanBookResponseHeader responseHeader;
	EplanBookResponseBody responseBody;
	
	public EplanBookResponseHeader getResponseHeader() {
		return responseHeader;
	}
	
	public void setResponseHeader(EplanBookResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}
	
	public EplanBookResponseBody getResponseBody() {
		return responseBody;
	}
	
	public void setResponseBody(EplanBookResponseBody responseBody) {
		this.responseBody = responseBody;
	}
	
}

class EplanBookResponseHeader {
	String desc;
	
	String err;
	
	String err_msg;
	
	String mtime;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}

	public String getErr_msg() {
		return err_msg;
	}

	public void setErr_msg(String err_msg) {
		this.err_msg = err_msg;
	}

	public String getMtime() {
		return mtime;
	}

	public void setMtime(String mtime) {
		this.mtime = mtime;
	}
	
}

class EplanBookResponseBody {
	String page;
	
	String pagecount;
	
	String total;
	
	String[] prods;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getPagecount() {
		return pagecount;
	}

	public void setPagecount(String pagecount) {
		this.pagecount = pagecount;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String[] getProds() {
		return prods;
	}

	public void setProds(String[] prods) {
		this.prods = prods;
	}
	
}