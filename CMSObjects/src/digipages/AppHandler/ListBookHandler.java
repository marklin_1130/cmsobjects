package digipages.AppHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import digipages.BooksHandler.BookDrmDeleteNotifyHandler;
import digipages.BooksHandler.MemberDrmHandler;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.DRMLogicUtility;
import digipages.exceptions.ServerException;
import digipages.info.ExtBookInfo;
import digipages.info.ExtMagInfo;
import model.BookInfo;
import model.DRMInfo;
import model.ItemInfo;
import model.MagInfo;
import model.Member;
import model.MemberBook;
import model.MemberDrmLog;

public class ListBookHandler {
	private Member member;
	private Integer offSet = 0;
	private Integer pageSize = 100;
	private String sortOrder = "";
	private String simpleMode = "n";
	private String isRead = "all";
	private String lastUpdatedTime = "";
	private String currentLastUpdatedTime = "";
	private String clientUDT = "";
	private String readlistFilter = "";
	private String isReadFilter = "";
    private String s3PublicBucket = "";
    private String booksServerNotifyUrl = "";
    private String drmDelNotification = "";
    private String clientId ,clientSecret;
    
	private static DPLogger logger = DPLogger.getLogger(ListBookHandler.class.getName());

	public ListBookServletData getNormalReaderListBook(EntityManager postgres) throws ServerException {
		ListBookServletData ret = new ListBookServletData();

		String queryString = "";
		String countQueryString = "";

		QueryStringResult resultQuery = normalQueryString(member, sortOrder, readlistFilter, clientUDT, isReadFilter, offSet, pageSize);

		queryString = resultQuery.getQueryString();
		countQueryString = resultQuery.getCountQueryString();

		@SuppressWarnings("unchecked")
		List<MemberBook> books = postgres.createNativeQuery(queryString, MemberBook.class).setHint("javax.persistence.cache.storeMode", "REFRESH").getResultList();

		int total_count = 0;

		try {
			total_count = (int) (long) postgres.createNativeQuery(countQueryString).getSingleResult();
		} catch (NoResultException e) {
			total_count = 0;
		}

		int deletedCount = 0;

		MemberDrmHandler memberDrmHandler = new MemberDrmHandler();
		BookDrmDeleteNotifyHandler bookDrmDeleteNotifyHandler = new BookDrmDeleteNotifyHandler(booksServerNotifyUrl,drmDelNotification,clientId ,clientSecret);
		if (total_count > 0) {
			for (MemberBook tmpMb : books) {
				DRMInfo drmInfo = new DRMInfo();
				boolean isDeleted = false;

				if (!tmpMb.getIsTrial()) {
					if ("serial".equalsIgnoreCase(tmpMb.getBookType())) {
						postgres.refresh(tmpMb);
					}

					List<MemberDrmLog> drmList = new ArrayList<MemberDrmLog>();
					MemberDrmLog memberDrmLog = DRMLogicUtility.getLongestMemberDrmLogAndList(tmpMb.getMemberDrmLogs() , drmList);

					if (memberDrmLog != null) {
						String readExpireTime = CommonUtil.normalizeDateTime(memberDrmLog.getReadExpireTime());
						drmInfo.setRead_end_time(readExpireTime);
						drmInfo.setDrm_type(String.valueOf(memberDrmLog.getType()));
						isDeleted = memberDrmHandler.delDRMifExpire(postgres, memberDrmLog.getId());
						if(isDeleted) {
							//最大授權過期，整本書及授權皆須刪除，則不用再檢查月租授權
                            bookDrmDeleteNotifyHandler.addNotifyMemberDrmlog(memberDrmLog);
                        }else {
							//檢查是否有月租授權過期，因還有授權沒有過期，所以不能整本書刪除(因最大授權沒有過期，所以不能整本書刪除，只刪除該授權及mapping)
                        	memberDrmHandler.delEplanDRMifExpire(postgres, tmpMb, drmList);
                        }
					} else {
						drmInfo.setRead_end_time(StringUtils.EMPTY);
						drmInfo.setDrm_type(StringUtils.EMPTY);
					}
				}

				if (isDeleted) {
					postgres.refresh(tmpMb);
					deletedCount++;
				}

				ListBookServletRecordData rd = getListBookRecordData(tmpMb, tmpMb.getBookFile().getItem().getInfo(), simpleMode, drmInfo);

				if (rd != null) {
					ret.addRecord(rd);
				}
			}
		}
		new Thread(bookDrmDeleteNotifyHandler).start();
		ret.setTotal_records(total_count);
		ret.setCurrent_offset(offSet);
		ret.setUpdated_time(MemberBook.findMemberBookLastUpdateTime(postgres, member.getId()));

		return ret;
	}

	private ListBookServletRecordData getListBookRecordData(MemberBook tmpMemberBook, ItemInfo info, String simpleMode, DRMInfo drmInfo) {
		ListBookServletRecordData rd = new ListBookServletRecordData();
		info = AppUtility.fixCoverURL(info, s3PublicBucket);

		Date startReadTime = null;
		Date lastReadTime = null;
		Date finishTime = null;
		
		if (null == tmpMemberBook.getStartReadTime()) {
			startReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (null == tmpMemberBook.getLastReadTime()) {
			lastReadTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}
		
		if (null == tmpMemberBook.getFinishTime()) {
			finishTime = Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant());
		}

		if (tmpMemberBook.getBookType().toLowerCase().equals("book") || tmpMemberBook.getBookType().toLowerCase().equals("serial")) {
			String bookType = "book";

			BookInfo bi = (BookInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtBookInfo bInfo = new Gson().fromJson(new Gson().toJson(bi), ExtBookInfo.class);

			bInfo.setListBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime,finishTime);
			bInfo.setDrm_info(drmInfo);
			bInfo.setSize(tmpMemberBook.getBookFile().getSize());
			bInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(bInfo);
		} else if (tmpMemberBook.getBookType().toLowerCase().equals("magazine")) {
			String bookType = "magazine";

			MagInfo mi = (MagInfo) info;

			rd.setBook_uni_id(tmpMemberBook.getBookUniId());
			// rd.setItem_type(tmpMemberBook.getBookType().toLowerCase());
			rd.setItem_type(bookType);

			// Clone
			ExtMagInfo mInfo = new Gson().fromJson(new Gson().toJson(mi), ExtMagInfo.class);

			mInfo.setListBookInfo(member, tmpMemberBook, simpleMode, bookType, startReadTime, lastReadTime,finishTime);
			mInfo.setDrm_info(drmInfo);
			mInfo.setSize(tmpMemberBook.getBookFile().getSize());
			mInfo.setDisplay_file_size(CommonUtil.getDisplayFileSize(tmpMemberBook.getBookFile().getSize()));
			rd.setItem_info(mInfo);
		}

		return rd;
	}

	private QueryStringResult normalQueryString(Member member, String sortOrder, String readlistFilter, String clientUDT, String isReadFilter, int offSet, int pageSize) {
		String queryString = "";
		String countQueryString = "";

		switch (sortOrder) {
        case "default":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "timedesc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "timeasc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time ASC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "name":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " ORDER BY it.c_title COLLATE \"C\"" + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "publisher":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " ORDER BY it.publisher_name COLLATE \"C\"" + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "author":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " ORDER BY it.author NULLS FIRST, it.author COLLATE \"C\"" + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "readtimedesc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.last_read_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "readtimeasc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.last_read_time ASC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
            break;
        case "unreadtimedesc":
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " AND mb.start_read_time IS NULL"
                    + " ORDER BY mb.last_read_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'" + " AND mb.start_read_time IS NULL";
            break;
        default:
            queryString = "SELECT mb.* FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'"
                    + " ORDER BY mb.create_time DESC NULLS LAST, regexp_replace (left(it.c_title, 1), '[0-9]', '0') ASC, it.c_title ASC" // 符/數/英/中
                    + " OFFSET " + offSet + "" + " LIMIT " + pageSize;
            countQueryString = "SELECT COUNT(*) FROM member_book mb" + " INNER JOIN item it ON mb.item_id = it.id" + " INNER JOIN book_file bf ON mb.book_file_id = bf.id" + " WHERE bf.status = 9" + " AND mb.member_id = " + member.getId() + ""
//                    + " AND (lower(it.item_type) = 'book' OR lower(it.item_type) = 'magazine')" 
            		+ readlistFilter + isReadFilter + " AND mb.udt::varchar::timestamp >= '" + clientUDT + "'";
		}

		return new QueryStringResult(queryString, countQueryString);
	}

	public static String getLineInfo() {
		StackTraceElement ste = new Throwable().getStackTrace()[1];
		return ste.getFileName() + ": Line " + ste.getLineNumber() + "/";
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Integer getOffSet() {
		return offSet;
	}

	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSimpleMode() {
		return simpleMode;
	}

	public void setSimpleMode(String simpleMode) {
		this.simpleMode = simpleMode;
	}

	public String getIsRead() {
		return isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}

	public String getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(String lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getCurrentLastUpdatedTime() {
		return currentLastUpdatedTime;
	}

	public void setCurrentLastUpdatedTime(String currentLastUpdatedTime) {
		this.currentLastUpdatedTime = currentLastUpdatedTime;
	}

	public String getClientUDT() {
		return clientUDT;
	}

	public void setClientUDT(String clientUDT) {
		this.clientUDT = clientUDT;
	}

	public String getReadlistFilter() {
		return readlistFilter;
	}

	public void setReadlistFilter(String readlistFilter) {
		this.readlistFilter = readlistFilter;
	}

	public String getIsReadFilter() {
		return isReadFilter;
	}

	public void setIsReadFilter(String isReadFilter) {
		this.isReadFilter = isReadFilter;
	}

	public String getS3PublicBucket() {
		return s3PublicBucket;
	}

	public void setS3PublicBucket(String s3PublicBucket) {
		this.s3PublicBucket = s3PublicBucket;
	}

    public String getBooksServerNotifyUrl() {
        return booksServerNotifyUrl;
    }

    public void setBooksServerNotifyUrl(String booksServerNotifyUrl) {
        this.booksServerNotifyUrl = booksServerNotifyUrl;
    }

    public String getDrmDelNotification() {
        return drmDelNotification;
    }

    public void setDrmDelNotification(String drmDelNotification) {
        this.drmDelNotification = drmDelNotification;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }
}

class ListBookServletResultError {
	String error_code;
	String error_message;

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}

class ListBookServletRecordData {
	String book_uni_id;
	String item_type;
	String updated_time;
	ItemInfo item_info;

	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public String getItem_type() {
		return item_type;
	}

	public void setItem_type(String item_type) {
		this.item_type = item_type;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ItemInfo getItem_info() {
		return item_info;
	}

	public void setItem_info(ItemInfo item_info) {
		this.item_info = item_info;
	}
}

class ListBookServletData {
	Integer total_records = 100;
	Integer current_offset = 0;
	String updated_time;
	ArrayList<ListBookServletRecordData> records = new ArrayList<>();

	public Integer getTotal_records() {
		return total_records;
	}

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public Integer getCurrent_offset() {
		return current_offset;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ArrayList<ListBookServletRecordData> getRecords() {
		return records;
	}

	public void setRecords(ArrayList<ListBookServletRecordData> records) {
		this.records = records;
	}

	public void addRecord(ListBookServletRecordData record) {
		records.add(record);
	}
}

class ListBookServletDataNone {
	Integer total_records = 0;
	Integer current_offset = 0;
	String updated_time;
	List<String> records;

	public void setTotal_records(Integer total_records) {
		this.total_records = total_records;
	}

	public void setCurrent_offset(Integer current_offset) {
		this.current_offset = current_offset;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public void addRecord(List<String> record) {
		this.records = record;
	}
}