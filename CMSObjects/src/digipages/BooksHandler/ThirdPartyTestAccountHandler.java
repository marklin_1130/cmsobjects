package digipages.BooksHandler;

import java.util.List;

import javax.persistence.EntityManager;

import digipages.common.CommonUtil;
import digipages.exceptions.ServerException;

import model.Member;
import model.MemberDevice;
import model.Publisher;
import model.ThirdPartyDevice;
import model.Vendor;

public class ThirdPartyTestAccountHandler {
	private String partyType;
	private String partyId;
	private String deviceId;
	private String userId;
	private String operator;

	public void addAccount(EntityManager postgres) throws ServerException {
		try {
			Member member = genThirdPartyMember(postgres);

			if (null == member) {
				// Response
				throw new ServerException("id_err_204", "Account not exists.");
			}

			Integer memberID = member.getId().intValue();
			// Check Third Party Device
			updateThirdPartyDevice(postgres, memberID, "add");
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}
	}

	public void deleteAccount(EntityManager postgres) throws ServerException {
		String memberType = partyType2MemberType(partyType);

		Member member = Member.getMemberByBMemberId(postgres, userId, memberType);

		if (null == member) {
			// Response
			throw new ServerException("id_err_204", "Account not exists.");
		}

		Integer memberID = member.getId().intValue();
		// Check Third Party Device
		updateThirdPartyDevice(postgres, memberID, "delete");
	}

	private static String partyType2MemberType(String partyType) {
		String memberType = "";

		if ("publisherreader".equalsIgnoreCase(partyType)) {
			memberType = Member.MEMBERTYPE_PUBLISHERREADER;
		} else if ("vendorreader".equalsIgnoreCase(partyType)) {
			memberType = Member.MEMBERTYPE_VNDORREADER;
		} else if ("superreader".equalsIgnoreCase(partyType)) {
			memberType = Member.MEMBERTYPE_SUPERREADER;
		}

		return memberType;
	}

	private Member genThirdPartyMember(EntityManager postgres) throws ServerException {
		Member member = Member.getMemberByBMemberId(postgres, userId, partyType2MemberType(partyType));

		if (null == member) {
			Member m = new Member();

			m.setBmemberId(userId);
			m.setMemberType(partyType2MemberType(partyType));

			if ("publisherreader".equalsIgnoreCase(partyType)) {
				Publisher p = Publisher.getPublisherBybid(postgres, partyId);
				m.setPublisher(p);
			} else if ("vendorreader".equalsIgnoreCase(partyType)) {
				Vendor v = Vendor.getVendorBybid(postgres, partyId);
				m.setVendor(v);
			} else if ("superreader".equalsIgnoreCase(partyType)) {
				// superreader don't need to set anything
			}

			m.setName(userId);
			if (m.getNick()==null || m.getNick().length()<1)
				m.setNick(userId);
			m.setOperator(operator);
			m.setLastUpdated(CommonUtil.zonedTime());

			m.genDefaultReadLists(postgres);

			postgres.getTransaction().begin();
			postgres.persist(m);
			postgres.getTransaction().commit();

			member = m;
		}

		return member;
	}

	private void updateThirdPartyDevice(EntityManager postgres, Integer memberID, String act) throws ServerException {
		try {
			postgres.getTransaction().begin();
			if ("add".equals(act) || "modify".equals(act)) {
				List<ThirdPartyDevice> thirdPartyDevices = 
						postgres.createNamedQuery(
								"ThirdPartyDevice.findAllByMemberIdAndDeviceId", 
								ThirdPartyDevice.class)
						.setParameter("memberId", memberID)
						.setParameter("deviceId", deviceId)
						.getResultList();

				if (thirdPartyDevices.isEmpty()) {
					ThirdPartyDevice thirdPartyDevice = new ThirdPartyDevice();

					thirdPartyDevice.setMemberId(memberID);
					thirdPartyDevice.setDeviceId(deviceId);
					thirdPartyDevice.setLastUpdated(CommonUtil.zonedTime());
					thirdPartyDevice.setPartyType(partyType);
					thirdPartyDevice.setBpartyId(partyId);
					thirdPartyDevice.setOperator(operator);

					postgres.persist(thirdPartyDevice);
				}
				
				// try to update member_device table (set deleted back to logout)
				List<MemberDevice> thirdPartyMemberDevices = postgres.createNamedQuery
						("MemberDevice.findThirdPartyDevice",MemberDevice.class)
						.setParameter("memberId", memberID)
						.setParameter("deviceId", deviceId)
						.getResultList();
				for (MemberDevice md :thirdPartyMemberDevices)
				{
					// recover from deleted (logout)
					if (md.getStatus()==MemberDevice.STATUSDELETED)
						md.setStatus(MemberDevice.STATUSLOGOUT);
				}
				
			} else if (act.equals("delete")) {
				List<ThirdPartyDevice> thirdPartyDevices = postgres.createNamedQuery(
						"ThirdPartyDevice.findAllByMemberId", ThirdPartyDevice.class)
						.setParameter("memberId", memberID)
						.getResultList();
				
				List<MemberDevice> thirdPartyMemberDevices = postgres.createNamedQuery
						("MemberDevice.findThirdPartyDevice",MemberDevice.class)
						.setParameter("memberId", memberID)
						.setParameter("deviceId", deviceId)
						.getResultList();


				for (ThirdPartyDevice thirdPartyDevice : thirdPartyDevices) {
					if (thirdPartyDevice.getDeviceId().equals(deviceId)) {
						postgres.remove(thirdPartyDevice);
					}
				}
				for (MemberDevice tmpDevice:thirdPartyMemberDevices)
				{
					tmpDevice.setStatus(MemberDevice.STATUSDELETED);
				}

				postgres.flush();
			}
			postgres.getTransaction().commit();
		} catch (javax.persistence.RollbackException rex) {
			throw new ServerException("id_err_202", "Third Party Device not exists.");
		} catch (Exception x) {
			x.printStackTrace();
			throw new ServerException("id_err_202", "Third Party Device not exists.");
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}
	}

	public String getPartyType() {
		return partyType;
	}

	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}