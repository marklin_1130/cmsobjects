package digipages.BooksHandler;

import java.util.List;

public class UpdateAuditItemStatusData {
	List<UpdateAuditItemRecords> records;

	public List<UpdateAuditItemRecords> getRecords() {
		return records;
	}

	public void setRecords(List<UpdateAuditItemRecords> records) {
		this.records = records;
	}
}