package digipages.BooksHandler;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import digipages.CommonJobHandler.RunMode;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.Item;
import model.ScheduleJob;

public class RescanDRMHandler {
	public static final String HANDLERNAME = "RescanDRMHandler";
	private static final DPLogger logger = DPLogger.getLogger(RescanDRMHandler.class.getName());
	
	 /**
     * rescanMemberDRM.
     * 
     * 指定itemId (必須為套書的)
     * 
     * 1.更新/刪除所有會員擁有此套書內的子書 
     * 2.新增或異動所有會員套書內的子書DRM mapping
     * 
     * @param postgres the postgres
     * @param itemId the item id from item (店內碼)
     * @param memberId the member id from (cms member)
     * @throws Exception the exception
     */
	public void rescanMemberDRM(EntityManager postgres, final String itemId, final long memberId) throws Exception {
	    
	    logger.info("[rescanDRM] itemId: " + itemId+",memberId:"+memberId);

        Item item = Item.getItemByItemId(postgres, itemId);

        if ( ("serial".equals(item.getItemType()) ) && !item.getChilds().isEmpty() && memberId >0) {
                renewMemberDrmDataWithStoreProcedure(postgres,itemId,memberId);
        } else{
            throw new ServerException("id_err_301", " no corrosponding type found " + item.getId());
        }
	    
	}
	 /**
     * rescanDRM.
     * 
     * 指定itemId (必須為套書),memberId   
     * 
     * 1.新增套書內的子書 
     * 2.新增套書內的子書DRM mapping
     * 
     * @param postgres the postgres
     * @param itemId the item id from item (店內碼)
     * @throws Exception the exception
     */
	public void rescanDRM(EntityManager postgres, final String itemId) throws Exception {

		logger.info("[rescanDRM] itemId: " + itemId);

		Item item = Item.getItemByItemId(postgres, itemId);

        if (("serial".equals(item.getItemType()) ) && !item.getChilds().isEmpty()) {
				renewDrmDataWithStoreProcedure(postgres,itemId);
		} else{
		    throw new ServerException("id_err_301", " no corrosponding type found " + item.getId());
		}

	}

 
    /**
     * Renew drm data with store procedure.
     *
     * @param postgres the postgres
     * @param itemId the item id
     * @throws Exception the exception
     */
    private void renewDrmDataWithStoreProcedure(EntityManager postgres, final String itemId) throws Exception {
        try {
            String storedProcedureName = "serial_book_operation";
            StoredProcedureQuery query = postgres.createStoredProcedureQuery(storedProcedureName);
            query.registerStoredProcedureParameter("in_item_id", String.class, ParameterMode.IN);
            query.setParameter("in_item_id", itemId);
            query.execute();
        } catch (Exception e) {
            throw new ServerException("id_err_301", "Excute Store Procedure Fail " +" itemId:"+ itemId);
        }
    }
	
   
    /**
     * Renew member drm data with store procedure.
     *
     * @param postgres the postgres
     * @param itemId the item id
     * @param memberId the member id
     * @throws Exception the exception
     */
    private void renewMemberDrmDataWithStoreProcedure(EntityManager postgres, final String itemId, final long memberId) throws Exception{
        try {
            String storedProcedureName = "serial_book_member_operation";
            StoredProcedureQuery query = postgres.createStoredProcedureQuery(storedProcedureName);
            query.registerStoredProcedureParameter("in_item_id", String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter("in_member_id", Integer.class, ParameterMode.IN);
            query.setParameter("in_item_id", itemId);
            query.setParameter("in_member_id", memberId);
            query.execute();
        } catch (Exception e) {
            throw new ServerException("id_err_301", "Excute Store Procedure Fail " +" itemId:"+ itemId + " memberId:"+memberId);
        }
    }
	
	
	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	
	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
}