package digipages.BooksHandler;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import model.MemberDrmLog;

public class BookDrmDeleteNotifyHandler implements Runnable {

    private static final Logger logger = LogManager.getLogger(BookDrmDeleteNotifyHandler.class);

    private String booksServerNotifyUrl = "";

    private BookDrmDeleteNotifyData bookDrmDeleteNotifyData;
    
    private String arn ;
    
    private String clientId ,clientSecret;

    public BookDrmDeleteNotifyHandler(String booksServerNotifyUrl,String arn,String clientId ,String clientSecret) {

        if (StringUtils.isBlank(booksServerNotifyUrl)) {
            this.booksServerNotifyUrl = "";
        } else {
            this.booksServerNotifyUrl = booksServerNotifyUrl;
        }
        
        if (StringUtils.isBlank(arn)) {
            this.arn = "arn:aws:sns:ap-northeast-1:600602166480:DRM-Del-Notification";
        } else {
            this.arn = arn;
        }
        this.clientId=clientId;
        this.clientSecret=clientSecret;
        // memberDrmLogs = new ArrayList<MemberDrmLog>();
        bookDrmDeleteNotifyData = new BookDrmDeleteNotifyData();
        bookDrmDeleteNotifyData.setData(new ArrayList<Map<String, String>>());
    }

    public void addNotifyMemberDrmlog(MemberDrmLog aMemberDrmLog) {
        // memberDrmLogs.add(aMemberDrmLog);
        Map<String, String> jsonMap = new HashMap<String, String>();
        jsonMap.put("itemId", aMemberDrmLog.getItem().getId());
        jsonMap.put("transactionId", aMemberDrmLog.getTransactionId());
        jsonMap.put("submitId", aMemberDrmLog.getSubmitId());
        jsonMap.put("memberId", aMemberDrmLog.getMember().getBmemberId());
        jsonMap.put("drmType", String.valueOf(aMemberDrmLog.getType()));
        bookDrmDeleteNotifyData.getData().add(jsonMap);
    }

    @Override
    public void run() {

        if (!bookDrmDeleteNotifyData.getData().isEmpty()) {
            sendNotifyToBooksServer();
        }

    }

    private void sendErrorSNS(String url , String content ,String result ,  String errorMsg) {

        // create a new SNS client and set endpoint
        AmazonSNSClient snsClient = new AmazonSNSClient();
        snsClient.setRegion(Region.getRegion(Regions.AP_NORTHEAST_1));
        // publish to an SNS topic
        String msg = "Hi Admin \r\n \r\n"+"電子書授權取消錯誤通知信\r\n \r\n"+"requestUrl:"+url+"\r\n \r\n"+"request:"+content+"\r\n \r\n"+"errorMsg:"+errorMsg+"\r\n \r\n"+"result:"+result;
        PublishRequest publishRequest = new PublishRequest(arn, msg);
        PublishResult publishResult = snsClient.publish(publishRequest);
        // print MessageId of message published to SNS topic
        System.out.println("MessageId - " + publishResult.getMessageId());

    }

    public void sendNotifyToBooksServer() {
        try {
            String result="";
            System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,SSLv3");

            final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();

            // Allow TLSv1 protocol only, use NoopHostnameVerifier to trust self-singed cert
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1", "TLSv1.1", "TLSv1.2" }, null, new NoopHostnameVerifier());

            CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setSSLSocketFactory(sslsf).build();

            HttpPost httpPost = new HttpPost(booksServerNotifyUrl);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.setHeader("app", "cms");
            httpPost.setHeader("auth_id", clientId);
            httpPost.setHeader("auth_key", clientSecret);
            
            // String encoding = Base64.getEncoder().encodeToString((snpsaccount + ":" +
            // snpspw).getBytes("UTF-8"));
            // httpPost.setHeader("Authorization", "Basic " + encoding);

            String rdata = new Gson().toJson(bookDrmDeleteNotifyData.getData());
            StringEntity entity = new StringEntity(rdata, "UTF-8");
            
            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("data", rdata));
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            
//            httpPost.setEntity(entity);
            String errorMsg = "";
            IOException ioex = null;
            CloseableHttpResponse response_t = null;
            int cnt = 0;
            boolean success = false;
            while (cnt < 3 && success == false) {
                try {
                    logger.debug("drm delete notify send req:" + rdata);
                    response_t = httpclient.execute(httpPost);
                    HttpEntity responseEntity = response_t.getEntity();
                    if (responseEntity != null) {
                        String respJson = EntityUtils.toString(responseEntity, "UTF-8");
                        result= respJson;
                        logger.debug("drm delete notify resp:" + respJson);
                        Type stringStringMap = new TypeToken<Map<String, String>>() {
                        }.getType();
                        Map<String, String> map = new Gson().fromJson(respJson, stringStringMap);
                        String returnCode = map.get("result");
                        if (StringUtils.isNotBlank(returnCode) || "true".equalsIgnoreCase(returnCode)) {
                            success = true;
                        }
                    }
                    response_t.close();
                    httpPost.releaseConnection();
                } catch (IOException ex) {
                    logger.error("drm delete error:" + ExceptionUtils.getRootCauseMessage(ex));
                    errorMsg = "drm delete error:" + ExceptionUtils.getRootCauseMessage(ex);
                    ioex = ex;
                }
                cnt++;
            }
            if (!success) {
                sendErrorSNS(booksServerNotifyUrl,"data="+rdata,result,errorMsg);
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }

}

class BookDrmDeleteNotifyData {
    private List<Map<String, String>> data;

    public List<Map<String, String>> getData() {
        return data;
    }

    public void setData(List<Map<String, String>> data) {
        this.data = data;
    }

}
