package digipages.BooksHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;
import digipages.dev.utility.BookUpgrader;
import digipages.exceptions.ServerException;

import model.BookFile;
import model.ScheduleJob;

public class BookUpgraderHandler implements CommonJobHandlerInterface {
	public static final String HANDLERNAME = "BookUpgraderHandler";
	public static final String JOBGROUPNAME = "UPGRADE";
	public static final int JOBRUNNER = 0;
	private ScheduleJob scheduleJob;
	private static final DPLogger logger = DPLogger.getLogger(BookUpgraderHandler.class.getName());
	private String itemId;
	private ArrayList<String> itemIds;
	private EntityManagerFactory emf;
	private static BookUpgraderHandler myInstance = new BookUpgraderHandler();

	public BookUpgraderHandler() {
		LocalRunner.registerHandler(this);
	}

	public static void initJobHandler() {
		// not used.
	}

	public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
		// take jobs
		List<ScheduleJob> jobs;

		ScheduleJob jobParam = new ScheduleJob();
		jobParam.setUuid(UUID.randomUUID());
		jobParam.setJobName(BookUpgraderHandler.HANDLERNAME);
		jobParam.setJobGroup(BookUpgraderHandler.JOBGROUPNAME);
		jobParam.setJobRunner(BookUpgraderHandler.JOBRUNNER);

		jobs = ScheduleJobManager.takeJobs(jobParam, 10);

		return jobs;
	}

	public ScheduleJob getScheduleJob() {
		return scheduleJob;
	}

	public void setScheduleJob(ScheduleJob scheduleJob) {
		this.scheduleJob = scheduleJob;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public ArrayList<String> getItemIds() {
		return itemIds;
	}

	public void setItemIds(ArrayList<String> itemIds) {
		this.itemIds = itemIds;
	}

	public static BookUpgraderHandler getInstance() {
		return myInstance;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException {
		EntityManager em = emf.createEntityManager();
		ScheduleJobParam jobParams = scheduleJob.getJobParam(ScheduleJobParam.class);
		String item_id = "";

		String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
		String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

		if (null != jobParams.getItem_id()) {
			item_id = jobParams.getItem_id();
		}

		logger.info("[LocalConsumer] item_id: " + item_id);
		logger.info("Upgrade item_id: " + item_id + " member book version, start at " + dateTimeString);

		setScheduleJob((ScheduleJob) scheduleJob);
		setItemId(item_id);
 
		try {
			Integer bookFileId = Integer.parseInt(item_id);
			BookFile tmpbookfile = BookFile.getBookFile(em,bookFileId);
			
	        String itemIdTop3 = tmpbookfile.getItem().getId().substring(0, 3);
	        if("E05".equals(itemIdTop3) ||  "E06".equals(itemIdTop3) ||"G00".equals(itemIdTop3) ||"E07".equals(itemIdTop3)||"E08".equals(itemIdTop3) ) {
				BookUpgrader.upgradeMemberBookVersion(em, tmpbookfile);
			}

			
		} catch (ServerException e) {
			logger.error("ServerException ", e);
			throw e;
		} finally {
			em.close();
		}

		return "Upgrade book version Finished. " + item_id;
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
}