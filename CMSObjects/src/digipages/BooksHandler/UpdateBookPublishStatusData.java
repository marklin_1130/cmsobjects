package digipages.BooksHandler;

import java.util.List;

public class UpdateBookPublishStatusData {
	List<UpdateBookPublishStatusRecord> records;

	public List<UpdateBookPublishStatusRecord> getRecords() {
		return records;
	}

	public void setRecords(List<UpdateBookPublishStatusRecord> records) {
		this.records = records;
	}
}