package digipages.BooksHandler.vod;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.vod.MediaBookRequestApiData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.MediabookChapter;
import model.ScheduleJob;

//影音轉檔 步驟 3 通知宜沛轉檔
//呼叫宜沛API 通知轉檔
public class MediabookConvertNotifyHandler implements CommonJobHandlerInterface {
    public static final String HANDLERNAME = "MediabookConvertNotifyHandler";
    public static final String JOBGROUPNAME = "APICALL";
    public static final int JOBRUNNER = 0;
    private ScheduleJob scheduleJob;
    private static final DPLogger logger = DPLogger.getLogger(MediabookConvertNotifyHandler.class.getName());
    private EntityManagerFactory emf;
    private static MediabookConvertNotifyHandler myInstance = new MediabookConvertNotifyHandler();
    static LogToS3 convertingLogger = null;
    public MediabookConvertNotifyHandler() {
        LocalRunner.registerHandler(this);
    }

    public static void initJobHandler() {
        // not used.
    }

    public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
        // take jobs
        List<ScheduleJob> jobs;

        ScheduleJob jobParam = new ScheduleJob();
        jobParam.setUuid(UUID.randomUUID());
        jobParam.setJobName(MediabookConvertNotifyHandler.HANDLERNAME);
        jobParam.setJobGroup(MediabookConvertNotifyHandler.JOBGROUPNAME);
        jobParam.setJobRunner(MediabookConvertNotifyHandler.JOBRUNNER);

        jobs = ScheduleJobManager.takeJobs(jobParam, 10);

        return jobs;
    }

    public ScheduleJob getScheduleJob() {
        return scheduleJob;
    }

    public void setScheduleJob(ScheduleJob scheduleJob) {
        this.scheduleJob = scheduleJob;
    }

    public static MediabookConvertNotifyHandler getInstance() {
        return myInstance;
    }

    @Override
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException, JobRetryException {
        EntityManager em = emf.createEntityManager();
        MediaBookRequestApiData mediaBookRequestApiData = scheduleJob.getJobParam(MediaBookRequestApiData.class);
        String item_id = "";
        String respString = "";
        String retryMsg = "";
        String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
        String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " "
                + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

        if (null != mediaBookRequestApiData.getItem_id()) {
            item_id = mediaBookRequestApiData.getItem_id();
        }

        if(mediaBookRequestApiData.getPreview_content() == null) {
            mediaBookRequestApiData.setPreview_content("");
        }
        
        
        logger.info("[MediabookConvertNotifyHandler] item_id: " + item_id + ",chapter no" + mediaBookRequestApiData.getChapter_no() + ", start at " + dateTimeString);

        setScheduleJob((ScheduleJob) scheduleJob);

        try {
           
            boolean result = true;
            // 呼叫宜沛
            try {
                respString = reportToEqAPI(mediaBookRequestApiData, conf);
                if(respString.contains("\"status\":\"0\"")) {
                   
                }else {
                    result = false;
                    retryMsg = respString;
                }
                
            } catch (Exception e) {
                logger.info("[MediabookConvertNotifyHandler] 通知EQAPI轉檔REQ 失敗: " + ExceptionUtils.getStackTrace(e));
                result = false;
                retryMsg = "通知EQAPI轉檔REQ 失敗"+ExceptionUtils.getMessage(e);
            }

            if (scheduleJob.getRetryCnt() >= 3 && !result) {
                List<MediabookChapter> mediabookChapterList = em.createNamedQuery("MediabookChapter.findById", MediabookChapter.class).setParameter("id", Long.valueOf( mediaBookRequestApiData.getProcessing_id()))
                        .getResultList();
                em.getTransaction().begin();
                for (MediabookChapter mediabookChapter : mediabookChapterList) {
                    mediabookChapter.setStatus(-1);
                    mediabookChapter.setResponseMsg("呼叫宜沛轉檔失敗");
                    em.persist(mediabookChapter);
                }
                em.getTransaction().commit();
                scheduleJob.setErrMessage(respString);
                throw new ServerException("id_err_999", "fail to call convert api");
            }
            // 成功
            if (result) {
                em.getTransaction().begin();
                List<MediabookChapter> mediabookChapterList = em.createNamedQuery("MediabookChapter.findById", MediabookChapter.class).setParameter("id", Long.valueOf( mediaBookRequestApiData.getProcessing_id()))
                        .getResultList();
                for (MediabookChapter mediabookChapter : mediabookChapterList) {
                    mediabookChapter.setStatus(2);
                    em.persist(mediabookChapter);
                }
                em.getTransaction().commit();
            }else {
                throw new JobRetryException(retryMsg);
            }

        } catch (Exception e) {
            logger.error("Exception ", e);
            throw e;
        } finally {
            em.close();
        }

        logger.info("[MediabookConvertNotifyHandler] 通知EQAPI轉檔結果: " + "item_id: " + item_id + ",chapter no" + mediaBookRequestApiData.getChapter_no()+",resp:"+respString);
        
        return "MediabookConvertNotifyHandler Finished. item_id: " + item_id + ",chapter no" + mediaBookRequestApiData.getChapter_no()+",resp:"+respString;
    }

    public static String reportToEqAPI(MediaBookRequestApiData mediaBookRequestApiData,Properties conf) throws IOException {

        final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
        String eqapiUrl = conf.getProperty("eqapiUrl");
        HttpPost httpPost = new HttpPost(eqapiUrl);
        // HttpPost httpPost = new HttpPost("https://ens2tu7mwtq5jeq.m.pipedream.net");

        String eqapiAuthorization = conf.getProperty("eqapiSecret");
        
        httpPost.setHeader("Authorization",eqapiAuthorization);
        httpPost.setHeader("Content-Type", "application/json");
        Gson gson = new Gson();
        StringEntity postingString = new StringEntity(gson.toJson(mediaBookRequestApiData),"UTF-8");
        httpPost.setEntity(postingString);

        logger.info("[MediabookConvertNotifyHandler] 通知EQAPI轉檔REQ: " + gson.toJson(mediaBookRequestApiData));

        IOException ioex = null;
        CloseableHttpResponse response_t = null;
        int cnt = 0;
        boolean success = false;
        while (cnt < 5 && success == false) {
            try {
                response_t = httpclient.execute(httpPost);
                success = true;
//                logger.info("[MediabookConvertNotifyHandler] 通知EQAPI轉檔REQ 成功: " + responseToStr(response_t));
            } catch (IOException ex) {
                ioex = ex;
                logger.info("[MediabookConvertNotifyHandler] 通知EQAPI轉檔REQ 失敗: " + ExceptionUtils.getStackTrace(ex));
            }
            cnt++;
        }

        if (success == false) {

            String dumpResult = CommonUtil.traceRoute(httpPost.getURI().getHost());
            logger.info("[MediabookConvertNotifyHandler] 通知EQAPI轉檔REQ 失敗dumpResult: " + dumpResult);
            throw ioex;
        }

        String responseStr = responseToStr(response_t);

        return responseStr;
    }

    private static String responseToStr(CloseableHttpResponse response) throws UnsupportedOperationException, IOException {
        String responseStr = null;

        try {
            HttpEntity entity1 = response.getEntity();
            InputStream inp = entity1.getContent();
            Scanner scanner = new Scanner(inp, "UTF-8");
            scanner.useDelimiter("\\A");
            responseStr = scanner.next();

            scanner.close();
            inp.close();
            EntityUtils.consume(entity1);
        } finally {
            response.close();
        }

        return responseStr;
    }

    public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
        return RunMode.LocalMode;
    }

    public static RunMode getInitRunMode(ScheduleJob job) {
        return RunMode.LocalMode;
    }
}