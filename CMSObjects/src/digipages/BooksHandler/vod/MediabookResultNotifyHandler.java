package digipages.BooksHandler.vod;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.internal.AWSS3V4Signer;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CopyPartRequest;
import com.amazonaws.services.s3.model.CopyPartResult;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.GetObjectMetadataRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.BookMain.MediabookProcessResultData;
import digipages.BookConvert.BookMain.MediabookReturntData;
import digipages.BookConvert.utility.LogToS3;
import digipages.BooksHandler.BookUpgraderHandler;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.BookFile;
import model.Item;
import model.MediaBookInfo;
import model.MediabookChapter;
import model.MediabookConvertMapping;
import model.MediabookSubtitle;
import model.ScheduleJob;

//影音轉檔 步驟 4 完成處理轉檔請求
//呼叫博客來供應鏈 通知轉檔結果
public class MediabookResultNotifyHandler implements CommonJobHandlerInterface {
    public static final String HANDLERNAME = "MediabookResultNotifyHandler";
    public static final String JOBGROUPNAME = "RESULTCALL";
    public static final int JOBRUNNER = 0;
    protected static LogToS3 ConvertingLogger;
    private ScheduleJob scheduleJob;
    private static final DPLogger logger = DPLogger.getLogger(MediabookResultNotifyHandler.class.getName());
    private EntityManagerFactory emf;
    private static MediabookResultNotifyHandler myInstance = new MediabookResultNotifyHandler();
    private static String CLASSNAME = "MediabookResultNotifyHandler";
    static LogToS3 convertingLogger = null;
    private String from_bucket ;
    private String src_Folder ;
    private String mediabook_process_Folder ;
    private String mediabook_ok_folder ;
    private String mediabook_fail_folder ;
    private String itemFolderRulePath = "";
    private static final String PATHSEP = "/";
    
    public static Date calcShouldFinishTime() {
        return CommonUtil.nextNMinutes(41);
    }

    public MediabookResultNotifyHandler() {
        LocalRunner.registerHandler(this);
    }

    public static void initJobHandler() {
        // not used.
    }

    public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
        // take jobs
        List<ScheduleJob> jobs;

        ScheduleJob jobParam = new ScheduleJob();
        jobParam.setUuid(UUID.randomUUID());
        jobParam.setJobName(MediabookResultNotifyHandler.HANDLERNAME);
        jobParam.setJobGroup(MediabookResultNotifyHandler.JOBGROUPNAME);
        jobParam.setJobRunner(MediabookResultNotifyHandler.JOBRUNNER);

        jobs = ScheduleJobManager.takeJobs(jobParam, 10);

        return jobs;
    }

    public ScheduleJob getScheduleJob() {
        return scheduleJob;
    }

    public void setScheduleJob(ScheduleJob scheduleJob) {
        this.scheduleJob = scheduleJob;
    }

    public static MediabookResultNotifyHandler getInstance() {
        return myInstance;
    }

    @Override
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException, JobRetryException {
        EntityManager postgres = emf.createEntityManager();
        String item_id = "";
//		String originalBucket = conf.getProperty("originalBucket"); // s3private-ebook.books.com.tw
//		rootFolder = getRootFolder(mediaBookRequestData.getItem(), mediaBookRequestData.getRequest_time(), originalFolder,mediaBookRequestData.getBook_file_id());
//		String folderPath = logFolder + "/" + originalFolder + "/" + mediaBookRequestData.getItem() + "/"+ mediaBookRequestData.getBook_file_id() + "/" + mediaBookRequestData.getVersion();
//		String convertedLogPath = folderPath + "/" + "converted.log";
//		this.ConvertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
        String jobResultMsg = "";
        String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        
//        String dateTimeString = "";
//        try {
//        
//        LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
//        dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " "
//                + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());
//        
//        } catch (Exception e) {
//            // TODO: handle exception
//        }
        
        MediabookProcessResultData mediabookProcessResultData = scheduleJob.getJobParam(MediabookProcessResultData.class);
        if (null != mediabookProcessResultData.getItem()) {
            item_id = mediabookProcessResultData.getItem();
        }

        setScheduleJob((ScheduleJob) scheduleJob);
        String logTag = "[MediabookResultNotifyHandler]";
        try {
            
            // 處理影音書回檔-查詢可回檔並新增回檔排程
            long convertId = Long.valueOf(mediabookProcessResultData.getConvert_id());

            List<MediabookConvertMapping> mediabookConvertMappingList = postgres.createNativeQuery("select * from mediabook_convert_mapping mcm where id = " + convertId, MediabookConvertMapping.class).getResultList();
            MediabookConvertMapping mediabookConvertMapping = mediabookConvertMappingList.get(0);
            Gson gson = new GsonBuilder().create();

//              List<MediabookConvert> MediabookConvertList = postgres.createNativeQuery("select * from mediabook_convert mc where convert_id="+convertId +" and (status <> 9 or status <> -1) " , MediabookConvert.class).getResultList();
            boolean isTimeOut = false;
            
            try {

                Calendar cal = Calendar.getInstance();
                cal.setTime(scheduleJob.getCreateTime());
                cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + 6);
                //超時
                if(new Date().after(cal.getTime())) {
                    isTimeOut = true;
                }
            
            } catch (Exception e) {
                // 日期有錯誤則不處理,當下次重跑則會正常
            }
            
            
            // 查詢全部已處理可以回檔的資料, 9 處理完成,-1處理失敗 = 已處理過
            if (((Number) postgres.createNativeQuery("select count(1) from mediabook_chapter where (status <> 9 AND status <> -1) and convert_id=" + convertId).getSingleResult()).intValue() <= 0 || isTimeOut) {

                logger.info("[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id());
                
                @SuppressWarnings("unchecked")
                
                List<MediabookChapter> chapterList = postgres.createNamedQuery("MediabookChapter.findByConverId", MediabookChapter.class).setParameter("convertId",Long.valueOf( mediabookProcessResultData.getConvert_id())).getResultList();

                // 開始回檔資料處理
                postgres.getTransaction().begin();

                BookFile bookFile = BookFile.findLastestByItemId(postgres, item_id);
                BookFile bookFileTrial = BookFile.findNewTrialByItemId(postgres, item_id);
                
                if(bookFile==null) { // 如果是套書只有試閱,給同一筆即可
                    bookFile = bookFileTrial;
                }
                
                //init file location
                src_Folder = conf.getProperty("MediabookSrcFolder");
                itemFolderRulePath = getFolderDecodeFromItemId(mediabookConvertMapping.getItemId());
                mediabook_process_Folder = conf.getProperty("MediabookProcessFolder");
                mediabook_ok_folder = conf.getProperty("MediabookOkFolder");
                mediabook_fail_folder = conf.getProperty("MediabookFailFolder");
                from_bucket = conf.getProperty("MediabookSrcBucket");
                
              //init log
                String originalBucket = conf.getProperty("originalBucket"); // s3private-ebook.books.com.tw
                String originalFolder = conf.getProperty("originalFolder"); // converted
                String logFolder = conf.getProperty("logFolder"); // log-space
                String folderPath="";
                if(bookFile!=null) {
                     folderPath = logFolder + "/" + originalFolder + "/" + chapterList.get(0).getItemId() + "/"+ chapterList.get(0).getBookFileId() + "/" + mediabookProcessResultData.getConvert_id();
                }else {
                     folderPath = logFolder + "/" + originalFolder + "/" + chapterList.get(0).getItemId() + "/"+ chapterList.get(0).getBookFileId() + "/" + mediabookProcessResultData.getConvert_id();
                }
                String convertedLogPath = folderPath + "/" + "converted.log";
                
                AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
//                BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAI47WKLD65AUYRWXA", "VWU4HmYz6necoGFqEKwPNKLROPlVpZBfxXKBj1VM");
//                AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion("ap-northeast-1").withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
//                System.setProperty("org.xml.sax.driver", "com.sun.org.apache.xerces.internal.parsers.SAXParser");
                convertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
                convertingLogger.log(logTag,"[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id() );

                boolean convertResult = true;
                List<MediabookReturntData> rDataList = new ArrayList<MediabookReturntData>();
                mediabookProcessResultData.setR_data(rDataList);
                
                //檢查是否有成功的任一章節
                boolean isAnyChapterConvertSuccess = false;
                
                ArrayList<KeyVersion> keys = new ArrayList<KeyVersion>();
                // 針對每個章節做回檔
                for (MediabookChapter chapter : chapterList) {

                    long chapterId = chapter.getId();

                    MediabookReturntData rData = new MediabookReturntData();
                    rData.setChapter_no(chapter.getChapterNo());
                    if (chapter.getStatus() == 9) {
                        // 檔案檢查失敗回檔
                        rData.setResult(true);
                        
                        //轉檔完成搬移檔案
                        String object_key = mediabook_process_Folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getChapterFile();
                        keys.add(new KeyVersion(object_key));
                        String copy_to_object_key = mediabook_ok_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getChapterFile();
                        String to_bucket = from_bucket; // copy to is same bucket
                        try {
//                            s3Client.copyObject(from_bucket, object_key, to_bucket, copy_to_object_key);
                            copyS3ObjectMutiplePart(s3Client,from_bucket,object_key,to_bucket,copy_to_object_key);
                            
                            keys.add(new KeyVersion(object_key));
                        } catch (AmazonServiceException e) {
                            convertingLogger.error(logTag, "chpater檔案移動異常:"+object_key + ",error:"+e.getMessage());
                        }
                        
                        //試閱指定檔案
                        String object_preview_key="";
                        try {
                        	 
                            if(StringUtils.isNotBlank(chapter.getPreviewContent())) {
                            	object_preview_key = mediabook_process_Folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getPreviewContent();
                                String copy_to_object_script_key = mediabook_ok_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getPreviewContent();
//                                s3Client.copyObject(from_bucket, object_script_key, to_bucket, copy_to_object_script_key);
                                copyS3ObjectMutiplePart(s3Client,from_bucket,object_preview_key,to_bucket,copy_to_object_script_key);
                                keys.add(new KeyVersion(object_preview_key));
                            }
                            
                        } catch (Exception e) {
                            convertingLogger.error(logTag, "試閱指定檔案移動異常:"+object_preview_key + ",error:"+e.getMessage());
                        }


                        try {
                            
                            if("Y".equalsIgnoreCase(chapter.getIsScript())) {
                                String object_script_key = mediabook_process_Folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getScriptFile();
                                String copy_to_object_script_key = mediabook_ok_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getScriptFile();
                                s3Client.copyObject(from_bucket, object_script_key, to_bucket, copy_to_object_script_key);
                                keys.add(new KeyVersion(object_script_key));
                            }
                            
                        } catch (Exception e) {
                            convertingLogger.error(logTag, "script檔案移動異常:"+object_key + ",error:"+e.getMessage());
                        }
                        
                        try {
                            
                            List<MediabookSubtitle> subtitleList = chapter.getSubtitles();
                            
                            if(!subtitleList.isEmpty()) {
                                
                                for (MediabookSubtitle subtitle: subtitleList) {
                                    
                                    String object_subtitle_key = mediabook_process_Folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + subtitle.getSubtitleFile();
                                    String copy_to_object_subtitle_key = mediabook_ok_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + subtitle.getSubtitleFile();
                                    s3Client.copyObject(from_bucket, object_subtitle_key, to_bucket, copy_to_object_subtitle_key);
                                    keys.add(new KeyVersion(object_subtitle_key));
                                }
                                
                            }
                        } catch (Exception e) {
                            convertingLogger.error(logTag, "subtitle檔案移動異常:"+object_key + ",error:"+e.getMessage());
                        }
                        
                        
                        rData.setError_code(chapter.getResultCode());
                        rData.setError_message(chapter.getResponseMsg());
                        
                        isAnyChapterConvertSuccess = true;
                    } else {
                        rData.setResult(false);
                        convertResult = false;
                        rData.setError_code(chapter.getResultCode());
                        rData.setError_message(chapter.getResponseMsg());
                        
                      //轉檔完成搬移檔案
                        String object_key = mediabook_process_Folder + itemFolderRulePath  + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getChapterFile();
                        String copy_to_object_key = mediabook_fail_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getChapterFile();
                        String to_bucket = from_bucket; // copy to is same bucket
                        try {
//                            s3Client.copyObject(from_bucket, object_key, to_bucket, copy_to_object_key);
                            copyS3ObjectMutiplePart(s3Client,from_bucket,object_key,to_bucket,copy_to_object_key);
                            keys.add(new KeyVersion(object_key));
                        } catch (AmazonServiceException e) {
                            convertingLogger.error(logTag, "chpater檔案移動異常:"+object_key + ",error:"+e.getMessage());
                        }
                        
                      //試閱指定檔案
                        try {
                            
                            if(StringUtils.isNotBlank(chapter.getPreviewContent())) {
                                String object_script_key = mediabook_process_Folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getPreviewContent();
                                String copy_to_object_script_key = mediabook_fail_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getPreviewContent();
//                                s3Client.copyObject(from_bucket, object_script_key, to_bucket, copy_to_object_script_key);
                                copyS3ObjectMutiplePart(s3Client,from_bucket,object_key,to_bucket,copy_to_object_key);
                                keys.add(new KeyVersion(object_script_key));
                            }
                            
                        } catch (Exception e) {
                            convertingLogger.error(logTag, "試閱指定檔案移動異常:"+object_key + ",error:"+e.getMessage());
                        }
                        
                        try {
                            
                            if("Y".equalsIgnoreCase(chapter.getIsScript())) {
                                String object_script_key = mediabook_process_Folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getScriptFile();
                                String copy_to_object_script_key = mediabook_fail_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + chapter.getScriptFile();
                                s3Client.copyObject(from_bucket, object_script_key, to_bucket, copy_to_object_script_key);
                                keys.add(new KeyVersion(object_script_key));
                            }
                            
                        } catch (Exception e) {
                            convertingLogger.error(logTag, "script檔案移動異常:"+object_key + ",error:"+e.getMessage());
                        }
                        
                        try {
                            
                            List<MediabookSubtitle> subtitleList = chapter.getSubtitles();
                            
                            if(!subtitleList.isEmpty()) {
                                
                                for (MediabookSubtitle subtitle: subtitleList) {
                                    
                                    String object_subtitle_key = mediabook_process_Folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + subtitle.getSubtitleFile();
                                    String copy_to_object_subtitle_key = mediabook_fail_folder + itemFolderRulePath + chapter.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + subtitle.getSubtitleFile();
                                    s3Client.copyObject(from_bucket, object_subtitle_key, to_bucket, copy_to_object_subtitle_key);
                                    keys.add(new KeyVersion(object_subtitle_key));
                                }
                                
                            }
                        } catch (Exception e) {
                            convertingLogger.error(logTag, "subtitle檔案移動異常:"+object_key + ",error:"+e.getMessage());
                        }
                        
                    }

                    rData.setChapter_info(chapter.getChapterInfo());
                    rData.setPreview_info(chapter.getPreviewInfo());
                    rData.setVersion(chapter.getVersion());
                    rDataList.add(rData);

                }
                
                // 轉檔結果
                mediabookProcessResultData.setConvert_result(convertResult);
                if(!convertResult) {
                    if(isTimeOut) {
                        mediabookProcessResultData.setError_message("timeout");
                    }else {
                        mediabookProcessResultData.setError_message("fail");
                    }
                    jobResultMsg = "FAIL";
                    convertingLogger.log(logTag,"[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id() + "轉檔失敗");
                }else {
                    mediabookProcessResultData.setError_message("");
                    jobResultMsg = "OK";
                    convertingLogger.log(logTag,"[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id() + "轉檔成功");
                }
                
                mediabookProcessResultData.setReturn_file_num(chapterList.size());
                
                convertingLogger.log(logTag,"[轉檔結果資料更新] mediabookConvertMapping.getStatus(): " + mediabookConvertMapping.getStatus() + ",convert_id" + mediabookProcessResultData.getConvert_id() );
//                if(mediabookConvertMapping.getStatus()!=9) {
                    //更新書籍轉檔資料.ex.書封 狀態
                    updateDB(postgres, convertResult, bookFile, bookFileTrial, mediabookConvertMapping, mediabookProcessResultData,isAnyChapterConvertSuccess);
                    // 註記轉檔已全部處理
                    mediabookConvertMapping.setStatus(9);
                    convertingLogger.log(logTag,"[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id() + "mediabookConvertMapping setStatus(9) 註記mediabookConvertMapping轉檔已全部處理");
//                }
                postgres.getTransaction().commit();
                
                scheduleJob.setResultStr(gson.toJson(mediabookProcessResultData));
                
                if(isTimeOut) {
                    scheduleJob.setErrMessage("timeout");
                }
                
                mediabookProcessResultData.setBook_version(bookFile.getVersion());
                
                try {
                    reportToBookServer(mediabookProcessResultData, conf.getProperty("efileProcessResult"), conf.getProperty("bookAPIAuthKey"));    
                } catch (Exception e) {
                    convertingLogger.log(logTag,"[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id() + "回檔錯誤"+e.getMessage());
                    if(scheduleJob.getRetryCnt()>=3) {
                        throw new Exception("reportToBookServer error retry end");
                    }else {
                        throw new JobRetryException("reportToBookServer error");
                    }
                }
                
                //刪除process 檔案
                try {
                    // Delete the sample objects.
                    DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest(from_bucket)
                            .withKeys(keys)
                            .withQuiet(false);
    
                    // Verify that the objects were deleted successfully.
                    DeleteObjectsResult delObjRes = s3Client.deleteObjects(multiObjectDeleteRequest);
                    int successfulDeletes = delObjRes.getDeletedObjects().size();
                    System.out.println(successfulDeletes + " objects successfully deleted.");
                    convertingLogger.info(logTag, "mediabook_process_Folder 檔案刪除成功,count:"+successfulDeletes);
                
                } catch (Exception e) {
                    convertingLogger.error(logTag, "mediabook_process_Folder 檔案刪除異常,error:"+e.getMessage());
                }

//                if (true) {
//                    throw new JobRetryException("not yet implementant");
//                }
                logger.info("[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id() + ",result: " + convertResult);
                
            }else {
//                convertingLogger.log("[MediabookResultNotifyHandler]","job running , keep pending");
                
                throw new JobRetryException("pending");
            }

        }catch (JobRetryException jre) {
            throw jre;
        } catch (Exception e) {
            if (convertingLogger != null) {
                convertingLogger.log(logTag,"[MediabookResultNotifyHandler] item_id: " + item_id + ",convert_id" + mediabookProcessResultData.getConvert_id() + "error"+ExceptionUtils.getStackTrace(e));
            }
            logger.error("Exception ", e);
            throw new ServerException("id_err_999", e.getMessage());
        } finally {
            if (convertingLogger != null) {
                convertingLogger.uploadLogWithItemId(item_id);
            }
            if(postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
            postgres.close();
        }

        return "Mediabook result. item_id: " + item_id+"jobResultMsg:"+jobResultMsg;
    }

    private void updateDB(EntityManager postgres ,final boolean convertResult, BookFile bookFile, BookFile bookFileTrial , MediabookConvertMapping mediabookConvertMapping,MediabookProcessResultData mediabookProcessResultData,boolean isAnyChapterConvertSuccess) throws Exception {

        try {
//            postgres.getTransaction().begin();
            
            Item tmpItem = bookFile.getItem();
            MediaBookInfo info=(MediaBookInfo) tmpItem.getInfo();

            //影音書 新建檔修改為已上架 
            if(convertResult) {
                tmpItem.setStatus(1);
                postgres.persist(tmpItem);
                // 試閱書籍轉檔完成且轉檔成功的話,狀態直接轉為完成審核
                if(bookFileTrial!=null) {
                    bookFileTrial.setStatus(9);
                }
                bookFile.setStatus(9);
                
            }
            
          //計算總時間 , 檔案大小
            Long totalPlayTime = 0L;
            Long totalPreviewPlayTime = 0L;
            Long totalSize = 0L;
            Long totalPreviewSize = 0L;
            
            List<MediabookChapter> chapterList = postgres.createNamedQuery("MediabookChapter.findByBookFileId", MediabookChapter.class).setParameter("bookFileId", bookFile.getId()).getResultList();
            
            for (MediabookChapter mediabookChapter : chapterList) {
                try {
                    if(mediabookChapter.getChapterLength()!=null) {
                        totalPlayTime = totalPlayTime + Double.valueOf(mediabookChapter.getChapterLength()).longValue();
                    }
                    if(mediabookChapter.getChapterInfo()!=null&&mediabookChapter.getChapterInfo().getFile_size_bytes()!=null) {
                        totalSize = totalSize + Long.valueOf(mediabookChapter.getChapterInfo().getFile_size_bytes());
                    }

                    if ("Y".equalsIgnoreCase(mediabookChapter.getIspreview())) {
                        totalPreviewPlayTime = totalPreviewPlayTime + Double.valueOf(mediabookChapter.getPreviewInfo().getChapter_length()).longValue();
                        totalPreviewSize = totalPreviewSize + Long.valueOf(mediabookChapter.getPreviewInfo().getFile_size_bytes());
                    }
                } catch (Exception e) {
                    if (convertingLogger != null) {
                        convertingLogger.log("[MediabookResultNotifyHandler]","[MediabookResultNotifyHandler] item_id: " + tmpItem.getId() + ",convert_id" + mediabookProcessResultData.getConvert_id() + "error"+ExceptionUtils.getStackTrace(e));
                    }
                }
            }
            
            info.setTotal_play_time(totalPlayTime);
            info.setTotal_preview_play_time(totalPreviewPlayTime );
            info.setTotal_size(totalSize);
            info.setTotal_preview_size(totalPreviewSize);
            bookFile.setSize(totalSize);
            if(bookFileTrial!=null) {
                bookFileTrial.setSize(totalPreviewSize.intValue());
            }
           
            //回傳總時長, size
            
            mediabookProcessResultData.setMedia_lengthtime(info.getTotal_play_time());
            mediabookProcessResultData.setMedia_size(info.getTotal_size());
            
            
//                    int availTrialCount = tmpbookfile.getItem().getAvailTrialBookCnt(postgres);
//                    rData.setTrial_count(availTrialCount);

            postgres.flush();
//            postgres.getTransaction().commit();

            // handle cover if exists.
            if (mediabookProcessResultData.getFile_cover_url() != null && !mediabookProcessResultData.getFile_cover_url().isEmpty()) {
                // update
                info.setEfile_cover_url(mediabookProcessResultData.getFile_cover_url());
            }
            tmpItem.setInfo(info);
            
            //版號處理
//          影音書轉檔 轉檔完成後再壓跳版號
            
            convertingLogger.log("[MediabookResultNotifyHandler]","[轉檔結果資料更新] isAnyChapterConvertSuccess: " + isAnyChapterConvertSuccess);
            
            if(isAnyChapterConvertSuccess && "UF".equalsIgnoreCase(mediabookConvertMapping.getConvertStatus())) {
                
                double book_file_version = 001.0001;
                double book_file_trial_version = 001.0001;

                // 只會有一筆bookfile 正式/試閱
                
                book_file_version = Double.parseDouble(bookFile.getVersion().substring(1));
                if (!tmpItem.getId().contains("G00")) {
                    book_file_trial_version = Double.parseDouble(bookFileTrial.getVersion().substring(1));
                }
                
                book_file_version = book_file_version + 0.0001;
                book_file_trial_version = book_file_trial_version + 0.0001;
                
                bookFile.setVersion("V" + new DecimalFormat("000.0000").format(book_file_version));
                if (!tmpItem.getId().contains("G00")) {
                    bookFileTrial.setVersion("V" + new DecimalFormat("000.0000").format(book_file_trial_version));
                }
                
                convertingLogger.log("[MediabookResultNotifyHandler]","[轉檔結果資料更新] mediabookConvertMapping.getConvertStatus(): " + mediabookConvertMapping.getConvertStatus());
                convertingLogger.log("[MediabookResultNotifyHandler]","[轉檔結果資料更新] book_file_version: " + book_file_version);
                convertingLogger.log("[MediabookResultNotifyHandler]","[轉檔結果資料更新] book_file_trial_version: " + book_file_trial_version);
                
            }
            
            convertingLogger.log("[MediabookResultNotifyHandler]","[轉檔結果資料更新] bookFile.getVersion(): " + bookFile.getVersion());
            mediabookProcessResultData.setBook_version(bookFile.getVersion());
            
            postgres.persist(tmpItem);
            postgres.persist(bookFile);
            if(bookFileTrial!=null) {
                postgres.persist(bookFileTrial);
            }
            postgres.flush();

//            不管轉檔成功或失敗 都更新,因為章節檔案會有變動
            if (isAnyChapterConvertSuccess) {
                
                List<ScheduleJob> scheduleJobs = new ArrayList<>();

                ScheduleJobParam jobParams = new ScheduleJobParam();
                // change from item_id to book_file_id
                jobParams.setItem_id(String.valueOf(bookFile.getId()));
                ScheduleJob tmpJob = new ScheduleJob();
                tmpJob.setJobName(BookUpgraderHandler.class.getName());
                tmpJob.setJobGroup(UUID.randomUUID().toString());
                tmpJob.setJobRunner(0); // for local
                tmpJob.setPriority(500);
                tmpJob.setRetryCnt(0);
                tmpJob.setStatus(0);
                tmpJob.setJobParam(jobParams);
                scheduleJobs.add(tmpJob);
                
                if(bookFileTrial!=null) {
                
                jobParams.setItem_id(String.valueOf(bookFileTrial.getId()));
                ScheduleJob tmpJobTrial = new ScheduleJob();
                tmpJobTrial.setJobName(BookUpgraderHandler.class.getName());
                tmpJobTrial.setJobGroup(UUID.randomUUID().toString());
                tmpJobTrial.setJobRunner(0); // for local
                tmpJobTrial.setPriority(500);
                tmpJobTrial.setRetryCnt(0);
                tmpJobTrial.setStatus(0);
                tmpJobTrial.setJobParam(jobParams);
                scheduleJobs.add(tmpJobTrial);
                ScheduleJobManager.addJobs(scheduleJobs);
                }
            }
        } catch (Exception ex) {
            logger.error(CLASSNAME,ex);
            throw ex;
        }

    }
    
    public static String reportToBookServer(MediabookProcessResultData post, String efileProcessResultURL, String bookAPIAuthKey) throws IOException {

        final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();

        HttpPost httpPost = new HttpPost(efileProcessResultURL);
        httpPost.setHeader("app", "CMS");
        httpPost.setHeader("auth_id", "cms");
        httpPost.setHeader("auth_key", bookAPIAuthKey);
        httpPost.setHeader("Accept", "application/json");

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addTextBody("item", post.getItem());
        builder.addTextBody("call_type", post.getCall_type());
        builder.addTextBody("processing_id", post.getConvert_id());
        builder.addTextBody("return_file_num", String.valueOf(post.getReturn_file_num()));
        builder.addTextBody("convert_result", post.isConvert_result()? "true":"false");
        builder.addTextBody("error_message", StringUtils.trimToEmpty(post.getError_message()));
        builder.addTextBody("file_cover_url", StringUtils.trimToEmpty(post.getFile_cover_url()));
        builder.addTextBody("file_url", StringUtils.trimToEmpty(post.getFile_url()));
        builder.addTextBody("media_lengthtime", post.getMedia_lengthtime()!=null? String.valueOf(post.getMedia_lengthtime()) : "");
        builder.addTextBody("media_size", post.getMedia_size()!=null? String.valueOf(post.getMedia_size()) : "");
        builder.addTextBody("book_version", post.getBook_version()!=null? String.valueOf(post.getBook_version()) : "");
        String rdata = new Gson().toJson(post.getR_data());
        String postString = new Gson().toJson(post);
        builder.addTextBody("r_data", URLEncoder.encode(rdata, "UTF-8"));
        HttpEntity entity = builder.build();
        
        if(convertingLogger!=null) {
//            ByteArrayOutputStream content = new ByteArrayOutputStream();
//            entity.writeTo(content);
            // either convert stream to string
//            String string = content.toString();
            convertingLogger.log("[MediabookResultNotifyHandler]","efileProcessResultURL:"+efileProcessResultURL);
            convertingLogger.log("[MediabookResultNotifyHandler]","reportToBookServer input:"+postString);
        }
        
        httpPost.setEntity(entity);

        IOException ioex = null;
        CloseableHttpResponse response_t = null;
        int cnt = 0;
        boolean success = false;
        while (cnt < 5 && success == false) {
            try {
                response_t = httpclient.execute(httpPost);
                success = true;
            } catch (IOException ex) {
                ioex = ex;
                convertingLogger.log("[MediabookResultNotifyHandler]","reportToBookServer 連線錯誤:"+ex.getMessage());
            }
            cnt++;
        }

        if (success == false) {
            String dumpResult = CommonUtil.traceRoute(httpPost.getURI().getHost());
            convertingLogger.log("[reportToBookServer] dumpTraceRoute", dumpResult);
            throw ioex;
        }
        String responseStr ="";
        try {
            responseStr = responseToStr(response_t);
        } catch (Exception e) {
            // TODO: handle exception
        }
        if(convertingLogger!=null) {
            convertingLogger.log("[MediabookResultNotifyHandler]","reportToBookServer output:"+responseStr);
        }
        return responseStr;
    }

    public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
        return RunMode.LocalMode;
    }

    public static RunMode getInitRunMode(ScheduleJob job) {
        return RunMode.LocalMode;
    }

    private static String responseToStr(CloseableHttpResponse response) throws UnsupportedOperationException, IOException {
        String responseStr = null;

        try {
            HttpEntity entity1 = response.getEntity();
            InputStream inp = entity1.getContent();
            Scanner scanner = new Scanner(inp, "UTF-8");
            scanner.useDelimiter("\\A");
            responseStr = scanner.next();

            scanner.close();
            inp.close();
            EntityUtils.consume(entity1);
        } finally {
            response.close();
        }

        return responseStr;
    }

    private static void dumpTraceRoute(String address) throws UnknownHostException {
        String dumpResult = CommonUtil.traceRoute(address);
        ConvertingLogger.error("[reportToBookServer] dumpTraceRoute", dumpResult);
    }
    
    private String getFolderDecodeFromItemId(String itemId) {
        StringBuilder folder = new StringBuilder();

//      media_tmp/
//      E07/
//      190/
//      88/
//      E071908862/

        return folder.append(itemId.substring(0, 3)).append("/").append(itemId.substring(3, 6)).append("/")
                .append(itemId.substring(6, 8)).append("/").append(itemId).append("/").toString();
    }
    
    private void copyS3ObjectMutiplePart(AmazonS3 s3Client , String from_bucket ,String object_key, String to_bucket ,String copy_to_object_key ) throws Exception{
        // Initiate the multipart upload.
        InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(to_bucket, copy_to_object_key);
        InitiateMultipartUploadResult initResult = s3Client.initiateMultipartUpload(initRequest);
        
     // Get the object size to track the end of the copy operation.
        GetObjectMetadataRequest metadataRequest = new GetObjectMetadataRequest(from_bucket, object_key);
        ObjectMetadata metadataResult = s3Client.getObjectMetadata(metadataRequest);
        long objectSize = metadataResult.getContentLength();
        
        // Copy the object using 5 MB parts.
        long partSize = 5 * 1024 * 1024;
        long bytePosition = 0;
        int partNum = 1;
        List<CopyPartResult> copyResponses = new ArrayList<CopyPartResult>();
        while (bytePosition < objectSize) {
            // The last part might be smaller than partSize, so check to make sure
            // that lastByte isn't beyond the end of the object.
            long lastByte = Math.min(bytePosition + partSize - 1, objectSize - 1);

            // Copy this part.
            CopyPartRequest copyRequest = new CopyPartRequest()
                    .withSourceBucketName(from_bucket)
                    .withSourceKey(object_key)
                    .withDestinationBucketName(to_bucket)
                    .withDestinationKey(copy_to_object_key)
                    .withUploadId(initResult.getUploadId())
                    .withFirstByte(bytePosition)
                    .withLastByte(lastByte)
                    .withPartNumber(partNum++);
            copyResponses.add(s3Client.copyPart(copyRequest));
            bytePosition += partSize;
        }

     // Complete the upload request to concatenate all uploaded parts and make the copied object available.
        CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest(
                to_bucket,
                copy_to_object_key,
                initResult.getUploadId(),
                getETags(copyResponses));
        s3Client.completeMultipartUpload(completeRequest);
    }
    
    private static List<PartETag> getETags(List<CopyPartResult> responses) {
        List<PartETag> etags = new ArrayList<PartETag>();
        for (CopyPartResult response : responses) {
            etags.add(new PartETag(response.getPartNumber(), response.getETag()));
        }
        return etags;
    }
}