package digipages.BooksHandler.CommonData;

public class MediaBookAttachPostData {

	private String item;
	private String cat;
	private Integer attachfile_no;
	private String attachfile_type;
	private String attachfile_name;
	private String attachfile_title;
	private String convertstatus;
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public Integer getAttachfile_no() {
		return attachfile_no;
	}
	public void setAttachfile_no(Integer attachfile_no) {
		this.attachfile_no = attachfile_no;
	}
	public String getAttachfile_type() {
		return attachfile_type;
	}
	public void setAttachfile_type(String attachfile_type) {
		this.attachfile_type = attachfile_type;
	}
	public String getAttachfile_name() {
		return attachfile_name;
	}
	public void setAttachfile_name(String attachfile_name) {
		this.attachfile_name = attachfile_name;
	}
	public String getAttachfile_title() {
		return attachfile_title;
	}
	public void setAttachfile_title(String attachfile_title) {
		this.attachfile_title = attachfile_title;
	}
	public String getConvertstatus() {
		return convertstatus;
	}
	public void setConvertstatus(String convertstatus) {
		this.convertstatus = convertstatus;
	}
	
	
}
