package digipages.BooksHandler.CommonData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 影音書 轉檔請求參數
 * 
 * @author 20000764
 *
 */
public class MediaBookServletRecordPostData {
	private String item;
	private String cat;
	private String r_item;
	private String supplier;
	private String call_type;
	private String c_title;
	private String o_title;
	private String isbn;
	private String publisher_id;
	private String vendor_id;
	private String category;
	private String rank;
	private String publish_date;
	private String author;
	private String o_author;
	private String translator;
	private String editor;
	private String illustrator;
	private String edition;
	private String reader;
	private String author_reading;
	private String p_convertstatus;

	@SerializedName("package")
	private String package_flag;
	private Integer package_num;
	private String package_type;
	private String package_content;
	
    private String foreword;
	private String toc;
	private String intro;

	private String efile_qty;
	private String e_convertstatus;
	private String efile_url;
	private String efile_cover_url;
	private String share_flag;
	private String like_flag;
	private String annotation_flag;
	private String bookmark_flag;
	private String note_flag;
	private String public_flag;
	private String status;
	private int return_file_num;
	private ArrayList<PageData> page_data = new ArrayList<>();
	private String book_format;
	private int page_direction;
	private String language;
	private String version_type;
	private String buffet_flag;
	private String buffet_type;
	private String session_token;
	private List<String> childs = new ArrayList<>();
	private String issue;
	private String publish_type;
	private String issn;
	private String mag_id;
	private String cover_man;
	private String issue_year;
	private String cover_story;
	private String tts_flag;
	private Integer item_status;
	private String speaker;
	
	public Integer getItem_status() {
		return item_status;
	}
	public void setItem_status(Integer item_status) {
		this.item_status = item_status;
	}
	// 新增參數
	private List<MediaBookChapterPostData> chapter;
	
	private List<MediaBookAttachPostData> attach;
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getR_item() {
		return r_item;
	}
	public void setR_item(String r_item) {
		this.r_item = r_item;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getCall_type() {
		return call_type;
	}
	public void setCall_type(String call_type) {
		this.call_type = call_type;
	}
	public String getC_title() {
		return c_title;
	}
	public void setC_title(String c_title) {
		this.c_title = c_title;
	}
	public String getO_title() {
		return o_title;
	}
	public void setO_title(String o_title) {
		this.o_title = o_title;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPublisher_id() {
		return publisher_id;
	}
	public void setPublisher_id(String publisher_id) {
		this.publisher_id = publisher_id;
	}
	public String getVendor_id() {
		return vendor_id;
	}
	public void setVendor_id(String vendor_id) {
		this.vendor_id = vendor_id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getPublish_date() {
		return publish_date;
	}
	public void setPublish_date(String publish_date) {
		this.publish_date = publish_date;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getO_author() {
		return o_author;
	}
	public void setO_author(String o_author) {
		this.o_author = o_author;
	}
	public String getTranslator() {
		return translator;
	}
	public void setTranslator(String translator) {
		this.translator = translator;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public String getIllustrator() {
		return illustrator;
	}
	public void setIllustrator(String illustrator) {
		this.illustrator = illustrator;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getReader() {
		return reader;
	}
	public void setReader(String reader) {
		this.reader = reader;
	}
	public String getAuthor_reading() {
		return author_reading;
	}
	public void setAuthor_reading(String author_reading) {
		this.author_reading = author_reading;
	}
	public String getP_convertstatus() {
		return p_convertstatus;
	}
	public void setP_convertstatus(String p_convertstatus) {
		this.p_convertstatus = p_convertstatus;
	}
	public String getPackage_flag() {
		return package_flag;
	}
	public void setPackage_flag(String package_flag) {
		this.package_flag = package_flag;
	}
	public String getForeword() {
		return foreword;
	}
	public void setForeword(String foreword) {
		this.foreword = foreword;
	}
	public String getToc() {
		return toc;
	}
	public void setToc(String toc) {
		this.toc = toc;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getEfile_qty() {
		return efile_qty;
	}
	public void setEfile_qty(String efile_qty) {
		this.efile_qty = efile_qty;
	}
	public String getE_convertstatus() {
		return e_convertstatus;
	}
	public void setE_convertstatus(String e_convertstatus) {
		this.e_convertstatus = e_convertstatus;
	}
	public String getEfile_url() {
		return efile_url;
	}
	public void setEfile_url(String efile_url) {
		this.efile_url = efile_url;
	}
	public String getEfile_cover_url() {
		return efile_cover_url;
	}
	public void setEfile_cover_url(String efile_cover_url) {
		this.efile_cover_url = efile_cover_url;
	}
	public String getShare_flag() {
		return share_flag;
	}
	public void setShare_flag(String share_flag) {
		this.share_flag = share_flag;
	}
	public String getLike_flag() {
		return like_flag;
	}
	public void setLike_flag(String like_flag) {
		this.like_flag = like_flag;
	}
	public String getAnnotation_flag() {
		return annotation_flag;
	}
	public void setAnnotation_flag(String annotation_flag) {
		this.annotation_flag = annotation_flag;
	}
	public String getBookmark_flag() {
		return bookmark_flag;
	}
	public void setBookmark_flag(String bookmark_flag) {
		this.bookmark_flag = bookmark_flag;
	}
	public String getNote_flag() {
		return note_flag;
	}
	public void setNote_flag(String note_flag) {
		this.note_flag = note_flag;
	}
	public String getPublic_flag() {
		return public_flag;
	}
	public void setPublic_flag(String public_flag) {
		this.public_flag = public_flag;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getReturn_file_num() {
		return return_file_num;
	}
	public void setReturn_file_num(int return_file_num) {
		this.return_file_num = return_file_num;
	}
	public ArrayList<PageData> getPage_data() {
		return page_data;
	}
	public void setPage_data(ArrayList<PageData> page_data) {
		this.page_data = page_data;
	}
	public String getBook_format() {
		return book_format;
	}
	public void setBook_format(String book_format) {
		this.book_format = book_format;
	}
	public int getPage_direction() {
		return page_direction;
	}
	public void setPage_direction(int page_direction) {
		this.page_direction = page_direction;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getVersion_type() {
		return version_type;
	}
	public void setVersion_type(String version_type) {
		this.version_type = version_type;
	}
	public String getBuffet_flag() {
		return buffet_flag;
	}
	public void setBuffet_flag(String buffet_flag) {
		this.buffet_flag = buffet_flag;
	}
	public String getBuffet_type() {
		return buffet_type;
	}
	public void setBuffet_type(String buffet_type) {
		this.buffet_type = buffet_type;
	}
	public String getSession_token() {
		return session_token;
	}
	public void setSession_token(String session_token) {
		this.session_token = session_token;
	}
	public List<String> getChilds() {
		return childs;
	}
	public void setChilds(List<String> childs) {
		this.childs = childs;
	}
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	public String getPublish_type() {
		return publish_type;
	}
	public void setPublish_type(String publish_type) {
		this.publish_type = publish_type;
	}
	public String getIssn() {
		return issn;
	}
	public void setIssn(String issn) {
		this.issn = issn;
	}
	public String getMag_id() {
		return mag_id;
	}
	public void setMag_id(String mag_id) {
		this.mag_id = mag_id;
	}
	public String getCover_man() {
		return cover_man;
	}
	public void setCover_man(String cover_man) {
		this.cover_man = cover_man;
	}
	public String getIssue_year() {
		return issue_year;
	}
	public void setIssue_year(String issue_year) {
		this.issue_year = issue_year;
	}
	public String getCover_story() {
		return cover_story;
	}
	public void setCover_story(String cover_story) {
		this.cover_story = cover_story;
	}
	public String getTts_flag() {
		return tts_flag;
	}
	public void setTts_flag(String tts_flag) {
		this.tts_flag = tts_flag;
	}
	public List<MediaBookChapterPostData> getChapter() {
		return chapter;
	}
	public void setChapter(List<MediaBookChapterPostData> chapter) {
		this.chapter = chapter;
	}
	public List<MediaBookAttachPostData> getAttach() {
		return attach;
	}
	public void setAttach(List<MediaBookAttachPostData> attach) {
		this.attach = attach;
	}
	public Integer getPackage_num() {
        return package_num;
    }
    public String getPackage_type() {
        return package_type;
    }
    public String getPackage_content() {
        return package_content;
    }
    public void setPackage_num(Integer package_num) {
        this.package_num = package_num;
    }
    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }
    public void setPackage_content(String package_content) {
        this.package_content = package_content;
    }
    public String getSpeaker() {
        return speaker;
    }
    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }
}