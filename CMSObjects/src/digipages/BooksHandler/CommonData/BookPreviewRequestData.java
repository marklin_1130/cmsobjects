package digipages.BooksHandler.CommonData;

public class BookPreviewRequestData {

		public int getReturn_file_num() {
			return return_file_num;
		}

		public void setReturn_file_num(int return_file_num) {
			this.return_file_num = return_file_num;
		}

		public String getPreview_type() {
			return preview_type;
		}

		public void setPreview_type(String preview_type) {
			this.preview_type = preview_type;
		}

		public String getPreview_content() {
			return preview_content;
		}

		public void setPreview_content(String preview_content) {
			this.preview_content = preview_content;
		}

		public String getFile_url() {
			return file_url;
		}

		public void setFile_url(String file_url) {
			this.file_url = file_url;
		}

		public String getItem() {
			return item;
		}

		public void setItem(String item) {
			this.item = item;
		}

		public String getCall_type() {
			return call_type;
		}

		public void setCall_type(String call_type) {
			this.call_type = call_type;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getSession_token() {
			return session_token;
		}

		public void setSession_token(String session_token) {
			this.session_token = session_token;
		}

		public String getEfile_nofixed_name() {
			return efile_nofixed_name;
		}

		public void setEfile_nofixed_name(String efile_nofixed_name) {
			this.efile_nofixed_name = efile_nofixed_name;
		}

		public String getEfile_fixed_pad_name() {
			return efile_fixed_pad_name;
		}

		public void setEfile_fixed_pad_name(String efile_fixed_pad_name) {
			this.efile_fixed_pad_name = efile_fixed_pad_name;
		}

		public String getEfile_fixed_phone_name() {
			return efile_fixed_phone_name;
		}

		public void setEfile_fixed_phone_name(String efile_fixed_phone_name) {
			this.efile_fixed_phone_name = efile_fixed_phone_name;
		}
		

		public String getFormat() {
			return book_format;
		}

		public void setFormat(String format) {
			this.book_format = format;
		}


		String item;
		String call_type;
		String preview_type;
		String preview_content;
		String file_url;
		String efile_nofixed_name;
		String efile_fixed_pad_name;
		String efile_fixed_phone_name;
		String book_format;
		// String publisher_name;
		String status;
		int return_file_num;
		String session_token;
}
