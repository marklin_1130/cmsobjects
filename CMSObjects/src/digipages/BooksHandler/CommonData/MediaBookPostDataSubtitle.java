package digipages.BooksHandler.CommonData;

public class MediaBookPostDataSubtitle {

	private String item;
	private String chapter_no;
	private String subtitle_id;
	private String subtitle_file;
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getChapter_no() {
		return chapter_no;
	}
	public void setChapter_no(String chapter_no) {
		this.chapter_no = chapter_no;
	}
	public String getSubtitle_id() {
		return subtitle_id;
	}
	public void setSubtitle_id(String subtitle_id) {
		this.subtitle_id = subtitle_id;
	}
	public String getSubtitle_file() {
		return subtitle_file;
	}
	public void setSubtitle_file(String subtitle_file) {
		this.subtitle_file = subtitle_file;
	}
}
