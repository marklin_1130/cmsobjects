package digipages.BooksHandler.CommonData;

import java.util.List;

public class MediaBookPostDataChapter {

	private String item;
	private String cat;
	private String chapter_no;
	private String chapter_name;
	private String chapter_file;
	private String is_preview;
	private String preview_type;
	private String preview_content;
	private String is_script;
	private String script_filename;
	private String script_file;
	private String is_subtitle;
	List<MediaBookPostDataSubtitle> subtitle;
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getChapter_no() {
		return chapter_no;
	}
	public void setChapter_no(String chapter_no) {
		this.chapter_no = chapter_no;
	}
	public String getChapter_name() {
		return chapter_name;
	}
	public void setChapter_name(String chapter_name) {
		this.chapter_name = chapter_name;
	}
	public String getChapter_file() {
		return chapter_file;
	}
	public void setChapter_file(String chapter_file) {
		this.chapter_file = chapter_file;
	}
	public String getIs_preview() {
		return is_preview;
	}
	public void setIs_preview(String is_preview) {
		this.is_preview = is_preview;
	}
	public String getPreview_type() {
		return preview_type;
	}
	public void setPreview_type(String preview_type) {
		this.preview_type = preview_type;
	}
	public String getPreview_content() {
		return preview_content;
	}
	public void setPreview_content(String preview_content) {
		this.preview_content = preview_content;
	}
	public String getIs_script() {
		return is_script;
	}
	public void setIs_script(String is_script) {
		this.is_script = is_script;
	}
	public String getScript_filename() {
		return script_filename;
	}
	public void setScript_filename(String script_filename) {
		this.script_filename = script_filename;
	}
	public String getScript_file() {
		return script_file;
	}
	public void setScript_file(String script_file) {
		this.script_file = script_file;
	}
	public String getIs_subtitle() {
		return is_subtitle;
	}
	public void setIs_subtitle(String is_subtitle) {
		this.is_subtitle = is_subtitle;
	}
	public List<MediaBookPostDataSubtitle> getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(List<MediaBookPostDataSubtitle> subtitle) {
		this.subtitle = subtitle;
	}
}
