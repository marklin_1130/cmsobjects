package digipages.BooksHandler.CommonData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;



public class BookServletRecordPostData {
	String item;
	Integer item_status;
	String call_type;
	String c_title;
	String o_title;
	String isbn;

	String publisher_id;
	String vendor_id;
	String category;
	String rank;
	String publish_date;
	String author;
	String o_author;
	String translator;
	String editor;
	String illustrator;
	String edition;
	String foreword;
	String toc;
	String intro;
	String efile_nofixed_name;
	String efile_fixed_pad_name;
	String efile_fixed_phone_name;
	String efile_url;
	String efile_cover_url;
	String share_flag;
	String like_flag;
	String annotation_flag;
	String bookmark_flag;
	String note_flag;
	String public_flag;
	String status;
	int return_file_num;
	ArrayList<PageData> page_data = new ArrayList<>();
	String book_format;
	int page_direction;
	String language;
	String version_type;
	String buffet_flag;
	String buffet_type;
	String session_token;
	List<String> childs = new ArrayList<>();
	private String issue;
	private String publish_type;
	private String issn;
	private String mag_id;
	private String cover_man;
	private String issue_year;
	private String cover_story;
	private String tts_flag; 
	
	public String getEfile_cover_url() {
		return efile_cover_url;
	}
	public void setEfile_cover_url(String efile_cover_url) {
		this.efile_cover_url = efile_cover_url;
	}
	public int getPage_direction() {
		return page_direction;
	}
	public void setPage_direction(int page_direction) {
		this.page_direction = page_direction;
	}

	public String getVendor_id() {
		return vendor_id;
	}
	public void setVendor_id(String vendor_id) {
		this.vendor_id = vendor_id;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Integer getItem_status() {
		return item_status;
	}
	public void setItem_status(int item_status) {
		this.item_status = item_status;
	}
	public String getCall_type() {
		return call_type;
	}
	public void setCall_type(String call_type) {
		this.call_type = call_type;
	}
	public String getC_title() {
		return c_title;
	}
	public void setC_title(String c_title) {
		this.c_title = StringEscapeUtils.escapeHtml4(c_title);
	}
	public String getO_title() {
		return o_title;
	}
	public void setO_title(String o_title) {
		this.o_title = o_title;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPublisher_id() {
		return publisher_id;
	}
	public void setPublisher_id(String publisher_id) {
		this.publisher_id = publisher_id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getPublish_date() {
		return publish_date;
	}
	public void setPublish_date(String publish_date) {
		this.publish_date = publish_date;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = StringEscapeUtils.escapeHtml4(author);
	}
	public String getO_author() {
		return o_author;
	}
	public void setO_author(String o_author) {
		this.o_author = o_author;
	}
	public String getTranslator() {
		return translator;
	}
	public void setTranslator(String translator) {
		this.translator = translator;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public String getIllustrator() {
		return illustrator;
	}
	public void setIllustrator(String illustrator) {
		this.illustrator = illustrator;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getForeword() {
		return foreword;
	}
	public void setForeword(String foreword) {
		this.foreword = foreword;
	}
	public String getToc() {
		return toc;
	}
	public void setToc(String toc) {
		this.toc = toc;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getEfile_nofixed_name() {
		return efile_nofixed_name;
	}
	public void setEfile_nofixed_name(String efile_nofixed_name) {
		this.efile_nofixed_name = efile_nofixed_name;
	}
	public String getEfile_fixed_pad_name() {
		return efile_fixed_pad_name;
	}
	public void setEfile_fixed_pad_name(String efile_fixed_pad_name) {
		this.efile_fixed_pad_name = efile_fixed_pad_name;
	}
	public String getEfile_fixed_phone_name() {
		return efile_fixed_phone_name;
	}
	public void setEfile_fixed_phone_name(String efile_fixed_phone_name) {
		this.efile_fixed_phone_name = efile_fixed_phone_name;
	}
	public String getEfile_url() {
		return efile_url;
	}
	public void setEfile_url(String efile_url) {
		this.efile_url = efile_url;
	}
	public String getShare_flag() {
		return share_flag;
	}
	public void setShare_flag(String share_flag) {
		this.share_flag = share_flag;
	}
	public String getLike_flag() {
		return like_flag;
	}
	public void setLike_flag(String like_flag) {
		this.like_flag = like_flag;
	}
	public String getAnnotation_flag() {
		return annotation_flag;
	}
	public void setAnnotation_flag(String annotation_flag) {
		this.annotation_flag = annotation_flag;
	}
	public String getBookmark_flag() {
		return bookmark_flag;
	}
	public void setBookmark_flag(String bookmark_flag) {
		this.bookmark_flag = bookmark_flag;
	}
	public String getNote_flag() {
		return note_flag;
	}
	public void setNote_flag(String note_flag) {
		this.note_flag = note_flag;
	}
	public String getPublic_flag() {
		return public_flag;
	}
	public void setPublic_flag(String public_flag) {
		this.public_flag = public_flag;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getReturn_file_num() {
		return return_file_num;
	}
	public void setReturn_file_num(int return_file_num) {
		this.return_file_num = return_file_num;
	}
	public ArrayList<PageData> getPage_data() {
		return page_data;
	}
	public void setPage_data(ArrayList<PageData> page_data) {
		this.page_data = page_data;
	}
	public String getBook_format() {
		return book_format;
	}
	public void setBook_format(String book_format) {
		this.book_format = book_format;
	}

	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getVersion_type() {
		return version_type;
	}
	public void setVersion_type(String version_type) {
		this.version_type = version_type;
	}
	public String getBuffet_flag() {
		return buffet_flag;
	}
	public void setBuffet_flag(String buffet_flag) {
		this.buffet_flag = buffet_flag;
	}
	public String getBuffet_type() {
		return buffet_type;
	}
	public void setBuffet_type(String buffet_type) {
		this.buffet_type = buffet_type;
	}
	public String getSession_token() {
		return session_token;
	}
	public void setSession_token(String session_token) {
		this.session_token = session_token;
	}
	public List<String> getChilds() {
		return childs;
	}
	public void setChilds(List<String> childs) {
		this.childs = childs;
	}

	public String getIssue() {
		return issue;
	}
	public String getPublish_type() {
		
		return publish_type;
	}
	public String getIssn() {
		return issn;
	}
	public String getMag_id() {
		return mag_id;
	}
	public String getIssue_year() {
		return issue_year;
	}
	public String getCover_man() {
		return cover_man;
	}
	public String getCover_story() {
		return cover_story;
	}
	public String getTts_flag() {
		return tts_flag;
	}
	public void setTts_flag(String tts_flag) {
		this.tts_flag = tts_flag;
	}
}