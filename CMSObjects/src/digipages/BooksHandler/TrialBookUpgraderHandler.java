package digipages.BooksHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.boon.json.JsonFactory;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;
import digipages.exceptions.ServerException;

import model.BookFile;
import model.BookUniId;
import model.BookVersion;
import model.MemberBook;
import model.ScheduleJob;

public class TrialBookUpgraderHandler implements CommonJobHandlerInterface {
	public static final String HANDLERNAME = "TrialBookUpgraderHandler";
	public static final String JOBGROUPNAME = "UPGRADE";
	public static final int JOBRUNNER = 0;
	private ScheduleJob scheduleJob;
	private static final DPLogger logger = DPLogger.getLogger(TrialBookUpgraderHandler.class.getName());
	private String itemId;
	private ArrayList<String> itemIds;
	private EntityManagerFactory emf;
	private static TrialBookUpgraderHandler myInstance = new TrialBookUpgraderHandler();

	public TrialBookUpgraderHandler() {
		LocalRunner.registerHandler(this);
	}

	public static void initJobHandler() {
		// not used.
	}

	public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
		// take jobs
		List<ScheduleJob> jobs;

		ScheduleJob jobParam = new ScheduleJob();
		jobParam.setUuid(UUID.randomUUID());
		jobParam.setJobName(TrialBookUpgraderHandler.HANDLERNAME);
		jobParam.setJobGroup(TrialBookUpgraderHandler.JOBGROUPNAME);
		jobParam.setJobRunner(TrialBookUpgraderHandler.JOBRUNNER);

		jobs = ScheduleJobManager.takeJobs(jobParam, 10);

		return jobs;
	}

	public String upgradeMemberTrialBookVersion(EntityManager postgres, String bookUniId) throws ServerException {
		String jsonStr = "";

		logger.info("[upgradeMemberTrialBookVersion] book_uni_id: " + bookUniId);

		try {
			// check max available version
			BookFile maxVersionBookFile = findMaxVersionTrialBookFileForNormalReader(postgres, bookUniId);

			if (null == maxVersionBookFile) {
				throw new ServerException("id_err_301", "Book " + bookUniId + " not found!");
			}

			// check which member books affected.
			List<MemberBook> memberBooks = postgres.createNamedQuery("MemberBook.findAllByBookFileIdAndVersionLocked", MemberBook.class)
					.setParameter("bookUniId", bookUniId)
					.setParameter("versionLocked", false)
					.getResultList();

			for (MemberBook memberBook : memberBooks) {
				try {
					if ("superreader".equals(memberBook.getMember().getMemberType().toLowerCase()) || ("publisherreader".equals(memberBook.getMember().getMemberType().toLowerCase()) && null != memberBook.getMember().getPublisher()) || ("vendorreader".equals(memberBook.getMember().getMemberType().toLowerCase()) && null != memberBook.getMember().getVendor())) {
						maxVersionBookFile = findMaxVersionBookFileForThirdPartyAccount(postgres, bookUniId);
					} else {
						maxVersionBookFile = findMaxVersionTrialBookFileForNormalReader(postgres, bookUniId);
					}

					upgreadMemberBook(postgres, memberBook, maxVersionBookFile);
				} catch (javax.persistence.RollbackException rex) {
					logger.error("rollback, Memberid=" + memberBook.getMember().getId() + " BookUniId=" + memberBook.getBookUniId(),rex);
				}
			}
		} catch (javax.persistence.RollbackException rex) {
			throw new ServerException("id_err_999", "Error Writing data to db");
		} catch (Exception e) {
			e.printStackTrace();
			String errJsonMessage = JsonFactory.toJson(e);
			throw new ServerException("id_err_999", errJsonMessage);
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		// finish jobs
		if (null != scheduleJob) {
			// datetime
			String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
			String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

			try {
				ScheduleJobManager.finishJob(scheduleJob);

				logger.info("Upgrade book_uni_id: " + bookUniId + " book version, finished at " + dateTimeString);
			} catch (Exception ex) {
				logger.error(ex);
			}
		}

		return jsonStr;
	}

	public static void upgreadMemberBook(EntityManager postgres, MemberBook memberBook, BookFile maxVersionBookFile) {
		BookVersion curVersion = new BookVersion(memberBook.getBookFile().getVersion());
		BookVersion recentVersion = new BookVersion(maxVersionBookFile.getVersion());
		BookFile target = maxVersionBookFile;

		int hightLightCnt = memberBook.getHighLights(postgres, memberBook).size();
		int bookMarkCnt = memberBook.getBookMarks(postgres, memberBook).size();

		boolean bookmarkModified = hightLightCnt > 0 || bookMarkCnt > 0;

		// Marjor Change and item affected
		if (recentVersion.isMarjorChange(curVersion) && bookmarkModified) {
			// Marjor Change: Waiting
			memberBook.setAskUpdateVersion(true);
			memberBook.setBgUpdateVersion(false);
			memberBook.setBookHighlightStatus((hightLightCnt < 1 ? false : true));
			memberBook.setBookBookmarkStatus((bookMarkCnt < 1 ? false : true));
			memberBook.setCurVersion(target.getVersion());
			memberBook.setAction("update");
			memberBook.setLastUpdated(CommonUtil.zonedTime());
		} else if (recentVersion.isMarjorChange(curVersion) && !bookmarkModified) {
			// marjor change, no Note
			memberBook.setBookFile(target);
			memberBook.setAskUpdateVersion(false);
			memberBook.setBgUpdateVersion(true);
			memberBook.setBookHighlightStatus((hightLightCnt < 1 ? false : true));
			memberBook.setBookBookmarkStatus((bookMarkCnt < 1 ? false : true));
			memberBook.setCurVersion(target.getVersion());
			memberBook.setUpgradeTime(java.sql.Timestamp.valueOf(LocalDateTime.now()));
			memberBook.setAction("update");
			memberBook.setLastUpdated(CommonUtil.zonedTime());
		} else if (recentVersion.isMinorChange(curVersion)) {
			// minor change, or not affected (no Note)
			memberBook.setBookFile(target);
			memberBook.setAskUpdateVersion(false);
			memberBook.setBgUpdateVersion(true);
			memberBook.setBookHighlightStatus((hightLightCnt < 1 ? false : true));
			memberBook.setBookBookmarkStatus((bookMarkCnt < 1 ? false : true));
			memberBook.setCurVersion(target.getVersion());
			memberBook.setUpgradeTime(java.sql.Timestamp.valueOf(LocalDateTime.now()));
			memberBook.setAction("update");
			memberBook.setLastUpdated(CommonUtil.zonedTime());
		} else {
			// version same, nothing to do.
			return;
		}

		postgres.getTransaction().begin();
		postgres.persist(memberBook);
		postgres.getTransaction().commit();
	}

	private static BookFile findMaxVersionBookFileForThirdPartyAccount(EntityManager postgres, String bookUniId) throws ServerException {
		BookFile maxBookFile = null;
		boolean isTrial = bookUniId.endsWith("_trial");
		BookUniId buid = new BookUniId(bookUniId);
		String itemID = buid.getItemId();

		BookVersion maxVersion = new BookVersion("V000.0000");

		List<BookFile> getBookFiles = postgres.createNamedQuery("BookFile.findByItemIdForThirdPartyAccount", BookFile.class)
				.setParameter("item_id", itemID)
				.getResultList();

		for (BookFile bf : getBookFiles) {
			// skip non match trial flag.
			if (bf.getIsTrial() != isTrial) {
				continue;
			}

			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
				maxBookFile = bf;
			}
		}

		return maxBookFile;
	}

	private static BookFile findMaxVersionTrialBookFileForNormalReader(EntityManager postgres, String bookUniId) throws ServerException {
		BookFile maxBookFile = null;
		BookUniId buid = new BookUniId(bookUniId);
		String itemID = buid.getItemId();

		BookVersion maxVersion = new BookVersion("V000.0000");

		List<BookFile> getBookFiles = postgres.createNamedQuery("BookFile.findByItemIdForTrialBook", BookFile.class)
				.setParameter("item_id", itemID)
				.setParameter("isTrial", true)
				.getResultList();

		for (BookFile bf : getBookFiles) {
			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
				maxBookFile = bf;
			}
		}

		return maxBookFile;
	}

	public ScheduleJob getScheduleJob() {
		return scheduleJob;
	}

	public void setScheduleJob(ScheduleJob scheduleJob) {
		this.scheduleJob = scheduleJob;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public ArrayList<String> getItemIds() {
		return itemIds;
	}

	public void setItemIds(ArrayList<String> itemIds) {
		this.itemIds = itemIds;
	}

	public static TrialBookUpgraderHandler getInstance() {
		return myInstance;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException {
		EntityManager em = emf.createEntityManager();
		ScheduleJobParam jobParams = scheduleJob.getJobParam(ScheduleJobParam.class);
		String item_id = "";

		String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
		String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

		if (null != jobParams.getItem_id()) {
			item_id = jobParams.getItem_id();
		}

		logger.info("[LocalConsumer] item_id: " + item_id);
		logger.info("Upgrade item_id: " + item_id + " member trial book version, start at " + dateTimeString);

		setScheduleJob((ScheduleJob) scheduleJob);
		setItemId(item_id);

		try {
			BookFile tmpbookfile = BookFile.findLastVersionByItemIdAndTrial(em, item_id);

			if (tmpbookfile.getStatus() == 9) {
				upgradeMemberTrialBookVersion(em, tmpbookfile.getBookUniId());
			}
		} catch (ServerException e) {
			logger.error("ServerException ", e);
			throw e;
		} finally {
			em.close();
		}

		return "Upgrade trial book version Finished. " + item_id;
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
}