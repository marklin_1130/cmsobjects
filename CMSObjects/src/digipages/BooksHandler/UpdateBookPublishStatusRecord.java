package digipages.BooksHandler;

public class UpdateBookPublishStatusRecord {
	String item;
	Integer item_status;
	String operator;

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Integer getItemStatus() {
		return item_status;
	}

	public void setItemStatus(Integer audit_status) {
		this.item_status = audit_status;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}