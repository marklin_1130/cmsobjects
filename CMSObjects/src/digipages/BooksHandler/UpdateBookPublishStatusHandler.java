package digipages.BooksHandler;

import static org.boon.Boon.toJson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import digipages.common.CommonResponse;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.BookFile;
import model.Item;

public class UpdateBookPublishStatusHandler {
	static DPLogger logger = DPLogger.getLogger(UpdateBookPublishStatusHandler.class.getName());
	List <String> readyItemIds = new ArrayList<>();
	public String handleMain(EntityManager postgres,UpdateBookPublishStatusData inputData) throws ServerException
	{
		List<String> failedItems = new ArrayList<>();

		postgres.getTransaction().begin();

		for (UpdateBookPublishStatusRecord record : inputData.getRecords()) {
			String itemId=record.getItem();
			Item item = Item.getItemByItemId(postgres, itemId);
			item.setStatus(record.getItemStatus());

		}

		// handle failed items.
		if (!failedItems.isEmpty()) {
			throw new ServerException("id_err_301", "Item not found or Item version not most recent." + Arrays.toString(failedItems.toArray()));
		}

		postgres.getTransaction().commit();
		
		// Response
		CommonResponse result = new CommonResponse();
		result.setResult(true);
		String jsonStr = toJson(result);
		return jsonStr;
	}
	public List<String> getReadyItemIds() {
		return readyItemIds;
		
	}
	

}



