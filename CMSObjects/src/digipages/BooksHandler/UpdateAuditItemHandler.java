package digipages.BooksHandler;

import static org.boon.Boon.toJson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import digipages.common.CommonResponse;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.BookFile;

public class UpdateAuditItemHandler {
	static DPLogger logger = DPLogger.getLogger(UpdateAuditItemHandler.class.getName());
	List <String> readyItemIds = new ArrayList<>();
	public String handleMain(EntityManager postgres,UpdateAuditItemStatusData inputData) throws ServerException
	{
		List<String> failedItems = new ArrayList<>();

		BookFile curBookFile = null;

		postgres.getTransaction().begin();

		for (UpdateAuditItemRecords record : inputData.getRecords()) {
			String xversion = record.getItem_version();

			if (!xversion.startsWith("V")) {
				xversion = "V" + xversion;
			}

			curBookFile = BookFile.findByItemIdAndVersion(postgres, record.getItem(), xversion);
			
			if (null == curBookFile 
					|| curBookFile.getStatus() <= 0 
					|| curBookFile.hasNewerReadyVersion(curBookFile.getIsTrial())) {
				failedItems.add(record.getItem());
				continue;
			}

			curBookFile.setStatus(Integer.valueOf(record.getAudit_status()));
			curBookFile.setOperator(record.getOperator());
			curBookFile.setAuditTime(CommonUtil.zonedTime());

			postgres.persist(curBookFile);
			readyItemIds.add(record.item);

		}

		// handle failed items.
		if (!failedItems.isEmpty()) {
			throw new ServerException("id_err_301", "Item not found or Item version not most recent." + Arrays.toString(failedItems.toArray()));
		}

		postgres.getTransaction().commit();
		
		// Response
		CommonResponse result = new CommonResponse();
		result.setResult(true);
		String jsonStr = toJson(result);
		return jsonStr;
	}
	public List<String> getReadyItemIds() {
		return readyItemIds;
		
	}
	

}



