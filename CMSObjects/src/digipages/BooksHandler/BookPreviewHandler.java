package digipages.BooksHandler;

import static org.boon.Boon.toJson;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.auth.AWSCredentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.BookMain.BookConvertHandler;
import digipages.BookConvert.BookMain.TrialOpenBookHandler;
import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.RequestData;
import digipages.BooksHandler.CommonData.BookPreviewRequestData;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;
import digipages.exceptions.ServerException;
import model.BaseBookInfo;
import model.BookFile;
import model.Item;
import model.Member;
import model.ScheduleJob;

public class BookPreviewHandler {
	private static DPLogger logger = DPLogger.getLogger(BookPreviewHandler.class.getName());
	private static final long serialVersionUID = 1L;
	private static AWSCredentials credentials;
	private BlockingQueue<ScheduleJob> bq;
	private static List<ScheduleJob> jobs;
	private Properties config;
	public BookPreviewHandler(Properties properties) {
		config=properties;
	}

	private boolean checkRequest(BookPreviewRequestData post, HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		PrintWriter writer = response.getWriter();
		EntityManager postgres = (EntityManager) request.getAttribute("connect");
		String Error_message = "";
		BookServletResultError errorResult = new BookServletResultError();
		errorResult.setError_code("0");
		if (post.getCall_type() == null || post.getCall_type().isEmpty()) {
			errorResult.setError_code("id_err_000");
			Error_message = Error_message + "必須提供call_type ";
		}
		if (post.getItem() == null || post.getItem().isEmpty()) {
			errorResult.setError_code("id_err_000");
			Error_message = Error_message + "必須提供Item ";
		}
		if (post.getPreview_type() == null || post.getPreview_type().isEmpty()) {
			errorResult.setError_code("id_err_000");
			Error_message = Error_message + "必須提供preview_type ";
		} else if (post.getPreview_type().equals("3")) {
			if (post.getFile_url() == null || post.getFile_url().isEmpty()) {
				errorResult.setError_code("id_err_000");
				Error_message = Error_message + "必須提供file_url & efile_name";
			}
		}

		if (post.getPreview_content() == null || post.getPreview_content().isEmpty()) {
			errorResult.setError_code("id_err_000");
			Error_message = Error_message + "必須提供preview_content ";
		}

		if (post.getStatus() == null || post.getStatus().isEmpty()) {
			errorResult.setError_code("id_err_000");
			Error_message = Error_message + "必須提供status ";
		}
		if (post.getStatus() != null) {
			if (!post.getStatus().equals("A") && !post.getStatus().equals("UF")) {
				errorResult.setError_code("id_err_000");
				Error_message = Error_message + "status必須為A或是UF ";
			}

		}
		if (post.getReturn_file_num() == 0) {
			errorResult.setError_code("id_err_000");
			Error_message = Error_message + "必須提供return_file_num ";
		}

		List<BookFile> bookfiles = postgres.createNamedQuery("BookFile.findProcessedBookFileByItemId", BookFile.class)
				.setParameter("item_id", post.getItem())
				.getResultList();

		boolean hasBook = false;

		if (!bookfiles.isEmpty()) {
			for (BookFile tmpbookfile : bookfiles) {
				if (!tmpbookfile.getIsTrial()) {
					hasBook = true;
				}
			}
		}
		if (!bookfiles.isEmpty() && hasBook) {
			for (BookFile tmpbookfile : bookfiles) {
				if (tmpbookfile.getIsTrial() && post.getStatus().equals("A")) {
					errorResult.setError_code("id_err_000");
					Error_message = Error_message + "已經有試閱本，請使用UF為status更新 ";
					break;
				}
			}
		}

		if ("id_err_000".equals(errorResult.getError_code())) {
			errorResult.setError_message(Error_message);
			String jsonStr = toJson(errorResult);
			writer.write(jsonStr);
			return false;
		}
		return true;
	}

	void scheduleBookConvert(BookPreviewRequestData post, BookFile bookFile,
			String ts, String efile_url, String version,
			String file_name, String format, Item item , EntityManager postgres) {

		try {
		    String jobName = "";
			RequestData bookdata = new RequestData();

			bookdata.setEfile_url(efile_url);
			bookdata.setPreview_content(post.getPreview_content());
			bookdata.setEfile_nofixed_name(post.getEfile_nofixed_name());
			
			bookdata.setCall_type(post.getCall_type());
			bookdata.setReturn_file_num(post.getReturn_file_num());
			bookdata.setStatus(post.getStatus());
			bookdata.setItem(post.getItem());
			bookdata.setIsTrial(true);
			bookdata.setBook_file_id(String.valueOf(bookFile.getId()));
			bookdata.setVersion(version);

			bookdata.setPreview_type(post.getPreview_type());

			bookdata.setRequest_time(ts.toString());

			bookdata.setFormat(format);

			// fill bookinfo 
			bookdata.updateByItem(item);
			
			//#11 針對 Reflowable 開書
			if(BaseBookInfo.BookFormatReflowable.equalsIgnoreCase(bookdata.getFormat()) && bookdata.getPreview_type().equalsIgnoreCase("1")) {
			    jobName = TrialOpenBookHandler.class.getName();
			    String itemId = bookdata.getItem();
			    
			    if(StringUtils.isNoneBlank(itemId)&&StringUtils.isNoneBlank(bookdata.getVersion())) {
			        
			        List<BookFile> bookFiles = BookFile.findByItemIdAndTrialPrivilaged(postgres, itemId, false);
			        if(!bookFiles.isEmpty()) {
			            BookFile currentNormalBookFile = bookFiles.get(0);
			            String normalBookFileS3Location = currentNormalBookFile.getFileLocation();
	                    bookdata.setNormalBookFileS3Location(normalBookFileS3Location);
			        }else {
			            throw new ServerException("id_err_000","normalbook not found ,status must >=8 "+itemId);
			        }
			    }else {
			        throw new ServerException("id_err_000","bookdata itemId or Version is null");
			    }
			    
			}else {
			    jobName = BookConvertHandler.class.getName();    
			}
			
			DataMessage dataMessage = new DataMessage();
	        Gson gson = new GsonBuilder().create();
	        String data = gson.toJson(bookdata);
	        dataMessage.setRequestData(data);
	        dataMessage.setBookUniId(bookFile.getBookUniId());
	        dataMessage.setBookFileId(bookFile.getId());
	        // cmstoken for thumbnail
	        Member superReader = Member.getThumbGenerator(postgres);
	        dataMessage.setCmsToken(CommonUtil.genCmsToken(superReader, postgres).toEncStr());
	        dataMessage.setFolder(CommonUtil.getFolderName(bookdata));
	        
	        
			ScheduleJob sj = new ScheduleJob();
			sj.setJobName(jobName);
			sj.setJobGroup(bookdata.getBook_file_id());
			sj.setJobParam(dataMessage);
//			sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
			ScheduleJobManager.addJob(sj);
			
			// change status and file location
			postgres.getTransaction().begin();
			bookFile.setStatus(1);
			bookFile.setFileLocation(CommonUtil.getFolderName(bookdata));
			postgres.getTransaction().commit();
			
			
			logger.debug("Adding scheduleJob: " + new Gson().toJson(sj));
		} catch (Exception e) {
			logger.error("Unexcepted Error",e);
		}
	}

	public String processBookPreviewRequest(EntityManager postgres, BookPreviewRequestData post)
			throws IOException, ServerException {

		try {
			
			
			//套書的頭書可以傳入店內碼指定某一本試閱資料進行複製
			boolean isEqItemId = (post.getPreview_content().startsWith("E05"));
			if (post.getPreview_type().equals("4") && isEqItemId) {
				int newBookFileId = 0;
				BookFile newBookFile = new BookFile();
				newBookFile.setIsTrial(true); // preview is always true
				Item item = Item.getItemByItemId(postgres, post.getItem());
					//確認為店內碼進行資料複製
					try {
						BookFile targetBookFile = postgres.createNamedQuery("BookFile.findLastVersionByItemId", BookFile.class)
								.setParameter("itemId",post.getPreview_content())
								.setParameter("isTrial", true)
								.setMaxResults(1)
								.getSingleResult();
						newBookFile.setAuditTime(targetBookFile.getAuditTime());
						newBookFile.setChecksum(targetBookFile.getChecksum());
						newBookFile.setCreateTime(targetBookFile.getCreateTime());
						newBookFile.setFileLocation(targetBookFile.getFileLocation());
						newBookFile.setFormat(targetBookFile.getFormat());
						newBookFile.setLastUpdated(targetBookFile.getLastUpdated());
						newBookFile.setOperator(targetBookFile.getOperator());
						newBookFile.setOrigFileLocation(targetBookFile.getOrigFileLocation());
						newBookFile.setOrigFormat(targetBookFile.getOrigFormat());
						newBookFile.setResponseMsg(targetBookFile.getResponseMsg());
						newBookFile.setSize(targetBookFile.getSize());
						newBookFile.setSrc_fname(targetBookFile.getSrc_fname());
						newBookFile.setStatus(targetBookFile.getStatus());
						newBookFile.setVersion(item.genNewTrialVersion(postgres, false));
					} catch (Exception e) {e.printStackTrace();
						throw new ServerException("id_err_000","複製子書試閱資料失敗");
					}
					newBookFile.setItem(item);
					postgres.getTransaction().begin();
					postgres.persist(newBookFile);
					postgres.flush();
					postgres.getTransaction().commit();
					newBookFileId=newBookFile.getId();
					
					String ts = CommonUtil.zonedTime();
					
					RequestData bookdata = new RequestData();
					bookdata.setEfile_url(post.getFile_url());
					bookdata.setPreview_content(post.getPreview_content());
					bookdata.setEfile_nofixed_name(post.getEfile_nofixed_name());
					bookdata.setCall_type(post.getCall_type());
					bookdata.setReturn_file_num(post.getReturn_file_num());
					bookdata.setStatus(post.getStatus());
					bookdata.setItem(post.getItem());
					bookdata.setIsTrial(true);
					bookdata.setBook_file_id(String.valueOf(newBookFileId));
					bookdata.setVersion(newBookFile.getVersion());
					bookdata.setPreview_type(post.getPreview_type());
					bookdata.setRequest_time(ts.toString());
					bookdata.setFormat(post.getFormat());

					// fill bookinfo 
					bookdata.updateByItem(item);
					DataMessage dataMessage = new DataMessage();
			        Gson gson = new GsonBuilder().create();
			        String data = gson.toJson(bookdata);
			        dataMessage.setRequestData(data);
			        dataMessage.setBookUniId(newBookFile.getBookUniId());
			        dataMessage.setBookFileId(newBookFile.getId());
			        dataMessage.setResult(true);
			        
			        dataMessage.setLogFolder("log-space/trial-converted/"+newBookFile.getId()); //log-space/trial-converted/2b528cbe-e197-4b36-87fc-46e1b0ba76dd/285078
			        dataMessage.setFolder("trial-converted/"+newBookFile.getFileLocation()); //"folder": "trial-converted/2BF97D/285078"
			        dataMessage.setTargetBucket(config.getProperty("trialBucket"));
			        ScheduleJob sj =new ScheduleJob();
			        sj.setJobParam(dataMessage);
			        sj.setJobGroup(String.valueOf(newBookFileId)); 
			        sj.setJobName(NotifyAPIHandler.class.getName());
			        sj.setJobRunner(1);
			        ScheduleJobManager.addJob(sj);
			        
					BookServletRecordData f = new BookServletRecordData();
					f.setProcessing_id(String.valueOf(newBookFileId));
					f.setResult(true);
					return toJson(f);
					
			}else {
				
				//#69 試閱轉檔邏輯變更 
		        if (post.getPreview_content() == null || post.getPreview_content().isEmpty()||post.getPreview_content() =="0") {
		          throw new ServerException("id_err_000","必須提供preview_content :");
		        }
		        
				// insert book_file
				BookFile newBookFile = new BookFile();
				newBookFile.setIsTrial(true); // preview is always true

				int newBookFileId = 0;

				String fileName = "";

				Item item = Item.getItemByItemId(postgres, post.getItem());

				fixFormat(post);
				checkFormat(post);
				
			      //#11 針對 Reflowable 開書檢查
	            if(BaseBookInfo.BookFormatReflowable.equalsIgnoreCase(post.getFormat()) && post.getPreview_type().equalsIgnoreCase("1")) {
	                List<BookFile> bookFiles = BookFile.findByItemIdAndTrialPrivilaged(postgres, item.getId(), false);
	                if(bookFiles.isEmpty()) {
	                    throw new ServerException("id_err_000","normalbook not found ,status must >=8 "+item.getId());
	                }
	            }
				
				String ts = CommonUtil.zonedTime();
				newBookFile.setOrigFileLocation(post.getFile_url());
				newBookFile.setOrigFormat(post.getFormat());
				newBookFile.setOrigFileLocation(post.getFile_url());
				if ("pdf".equalsIgnoreCase(newBookFile.getOrigFormat()))
					newBookFile.setFormat("fixedlayout");
				else
					newBookFile.setFormat(post.getFormat());
				
				newBookFile.setItem(item);
				newBookFile.setSrc_fname(post.getEfile_nofixed_name());
				newBookFile.setIsTrial(true);
				newBookFile.setLastUpdated(ts);

				boolean isMajor = "3".equals(post.getPreview_content());
				newBookFile.setVersion(item.genNewTrialVersion(postgres, isMajor));

				postgres.getTransaction().begin();
				postgres.persist(newBookFile);
				postgres.flush();
				postgres.getTransaction().commit();
				newBookFileId=newBookFile.getId();

				scheduleBookConvert(post, newBookFile, ts, post.getFile_url(), newBookFile.getVersion(), fileName, post.getFormat(),item , postgres);

				BookServletRecordData f = new BookServletRecordData();
				f.setProcessing_id(String.valueOf(newBookFileId));
				f.setResult(true);
				return toJson(f);
				
			}
			
		} finally {

			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

	}



	public static void fixFormat(BookPreviewRequestData post) throws ServerException{
		if (null!=post.getFormat() && !post.getFormat().isEmpty()){
			if (post.getFormat().equals("0")) {
				post.setFormat(BaseBookInfo.BookFormatPDF);
			} else if (post.getFormat().equals("1")) {
				post.setFormat(BaseBookInfo.BookFormatReflowable);
			} else if (post.getFormat().equals("2")) {
				post.setFormat(BaseBookInfo.BookFormatFixedLayout);
			}
			return;
		}
		if (post.getPreview_content().endsWith(BaseBookInfo.BookFormatPDF)) {
			post.setFormat(BaseBookInfo.BookFormatPDF);
		} else {
			post.setFormat(BaseBookInfo.BookFormatReflowable);
		}
		
	}

	public static void checkFormat(BookPreviewRequestData post) throws ServerException{
		String fileExt="";
		try{
			fileExt = post.getEfile_nofixed_name().split("\\.")[1].toLowerCase();
			
		} catch (Exception e)
		{
			throw new ServerException("id_err_000","Unexcepted file name/extension " + post.getEfile_nofixed_name());
		}
		String format = post.getFormat();
		// pdf
		if (fileExt.equals(format))
		{
			return;
		}
		
		if ("epub".equals(fileExt) &&
				!(("reflowable").equals(format) || "fixedlayout".equals(format)))
		{
			throw new ServerException("id_err_000","format not match:" + format + " vs " + fileExt);
		}
	}

	private void RescanDRM(EntityManager postgres, String itemId) {
		try {
			if (null == itemId) {
				return;
			}

			// add jobs
			List<ScheduleJob> scheduleJobs = new ArrayList<>();

			String memberQueryString = "SELECT * FROM member WHERE id IN (SELECT DISTINCT member_id FROM member_drm_log WHERE item_id = '"
					+ itemId + "' AND status = 1)";

			@SuppressWarnings("unchecked")
			List<Member> members = postgres.createNativeQuery(memberQueryString, Member.class).getResultList();

			if (members.size() > 0) {
				for (Member tmpMember : members) {
					ScheduleJobParam jobParams = new ScheduleJobParam();

					jobParams.setMember_id(tmpMember.getId());

					ScheduleJob tmpJob = new ScheduleJob();

					tmpJob.setJobName(RescanDRMHandler.HANDLERNAME);
					tmpJob.setJobGroup("DRM");
					tmpJob.setJobRunner(0); // for local
					tmpJob.setPriority(500);
					tmpJob.setRetryCnt(0);
					tmpJob.setStatus(0);
					tmpJob.setJobParam(jobParams);

					scheduleJobs.add(tmpJob);
				}

				ScheduleJobManager.addJobs(scheduleJobs);

				LocalRunner.NotifyRunner();
			}
		} catch (Exception ex) {
			logger.error(ex);
		}
	}



	class PageData {
		public String getS_item() {
			return s_item;
		}

		public void setS_item(String s_item) {
			this.s_item = s_item;
		}

		String s_item;
	}

	class BookServletRecordData {

		public boolean isResult() {
			return result;
		}

		public void setResult(boolean result) {
			this.result = result;
		}

		public String getProcessing_id() {
			return processing_id;
		}

		public void setProcessing_id(String processing_id) {
			this.processing_id = processing_id;
		}

		boolean result;
		String processing_id;
	}

	class BookServletResultError {
		String error_code;
		String error_message;

		public String getError_code() {
			return error_code;
		}

		public void setError_code(String error_code) {
			this.error_code = error_code;
		}

		public String getError_message() {
			return error_message;
		}

		public void setError_message(String error_message) {
			this.error_message = error_message;
		}
	}

}
