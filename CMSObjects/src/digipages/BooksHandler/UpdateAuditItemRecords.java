package digipages.BooksHandler;

public class UpdateAuditItemRecords {
	String item;
	String item_version;
	Integer audit_status;
	String operator;

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getItem_version() {
		return item_version;
	}

	public void setItem_version(String item_version) {
		this.item_version = item_version;
	}

	public Integer getAudit_status() {
		return audit_status;
	}

	public void setAudit_status(Integer audit_status) {
		this.audit_status = audit_status;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}