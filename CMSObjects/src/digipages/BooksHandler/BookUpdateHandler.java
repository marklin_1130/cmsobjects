package digipages.BooksHandler;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.boon.json.JsonFactory;

import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.ScheduleJobParam;
import digipages.exceptions.ServerException;

import model.Item;
import model.MemberBook;
import model.ScheduleJob;

public class BookUpdateHandler implements CommonJobHandlerInterface {
	public static final String HANDLERNAME = "BookUpdateHandler";
	public static final String JOBGROUPNAME = "UPDATE";
	public static final int JOBRUNNER = 0;
	private ScheduleJob scheduleJob;
	private static final DPLogger logger = DPLogger.getLogger(BookUpdateHandler.class.getName());
	private String itemId;
	private String lastUpdateTime;
	private EntityManagerFactory emf;
	private static BookUpdateHandler myInstance = new BookUpdateHandler();

	public BookUpdateHandler() {
		LocalRunner.registerHandler(this);
	}

	public static void initJobHandler() {
		// not used.
	}

	public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
		// take jobs
		List<ScheduleJob> jobs;

		ScheduleJob jobParam = new ScheduleJob();
		jobParam.setUuid(UUID.randomUUID());
		jobParam.setJobName(BookUpdateHandler.HANDLERNAME);
		jobParam.setJobGroup(BookUpdateHandler.JOBGROUPNAME);
		jobParam.setJobRunner(BookUpdateHandler.JOBRUNNER);

		jobs = ScheduleJobManager.takeJobs(jobParam, 10);

		return jobs;
	}

	public String updateMemberBook(EntityManager postgres, String itemId, String lastUpdateTime) throws ServerException {
		String jsonStr = "";

		ZonedDateTime zdt = ZonedDateTime.parse(CommonUtil.standardizeIso8601(lastUpdateTime.replace("/", "-")));

		logger.info("[updateMemberBook] item_id: " + itemId);

		try {
			List<Item> items = postgres.createNamedQuery("Item.findById", Item.class)
					.setParameter("id", itemId)
					.getResultList();

			if (items.size() > 0) {
				for (Item tmpitem : items) {
					List<MemberBook> memberBooks = postgres.createNamedQuery("MemberBook.findAllByItemId", MemberBook.class)
							.setParameter("item", tmpitem)
							.getResultList();

					postgres.getTransaction().begin();

					for (MemberBook memberBook : memberBooks) {
						try {
							memberBook.setLastUpdated(Date.from(zdt.toInstant()));

							postgres.persist(memberBook);
						} catch (javax.persistence.RollbackException rex) {
							logger.error("rollback, Memberid=" + memberBook.getMember().getId() + " BookUniId=" + memberBook.getBookUniId(), rex);
						}
					}

					postgres.getTransaction().commit();
				}
			}
		} catch (javax.persistence.RollbackException rex) {
			throw new ServerException("id_err_999", "Error Writing data to db");
		} catch (Exception e) {
			e.printStackTrace();
			String errJsonMessage = JsonFactory.toJson(e);
			throw new ServerException("id_err_999", errJsonMessage);
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		// finish jobs
		if (null != scheduleJob) {
			// datetime
			String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
			String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

			try {
				ScheduleJobManager.finishJob(scheduleJob);

				logger.info("Update item_id: " + itemId + " member book udt, finished at " + dateTimeString);
			} catch (Exception ex) {
				logger.error(ex);
			}
		}

		return jsonStr;
	}

	public ScheduleJob getScheduleJob() {
		return scheduleJob;
	}

	public void setScheduleJob(ScheduleJob scheduleJob) {
		this.scheduleJob = scheduleJob;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public static BookUpdateHandler getInstance() {
		return myInstance;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException {
		EntityManager em = emf.createEntityManager();
		ScheduleJobParam jobParams = scheduleJob.getJobParam(ScheduleJobParam.class);
		String item_id = "";
		String last_update_time = "";

		String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
		String dateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

		if (null != jobParams.getItem_id()) {
			item_id = jobParams.getItem_id();
		}

		if (null != jobParams.getLast_update_time()) {
			last_update_time = jobParams.getLast_update_time();
		}

		logger.info("[LocalConsumer] item_id: " + item_id);
		logger.info("Update item_id: " + item_id + " member book udt, start at " + dateTimeString);

		setScheduleJob((ScheduleJob) scheduleJob);
		setItemId(item_id);
		setLastUpdateTime(last_update_time);

		try {
			if (null != item_id) {
				updateMemberBook(em, item_id, last_update_time);
			}
		} catch (ServerException e) {
			logger.error("ServerException ", e);
			throw e;
		} finally {
			em.close();
		}

		return "Update member book udt Finished. " + item_id;
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
}