package digipages.BooksHandler;

import static org.boon.Boon.toJson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.common.DRMLogicUtility;
import digipages.common.StringHelper;
import digipages.dto.MemberDrmLogDTO;
import digipages.exceptions.ServerException;

import model.BaseBookInfo;
import model.BookFile;
import model.DRMInfo;
import model.Eplan;
import model.Item;
import model.Member;
import model.MemberBook;
import model.MemberBookDrmMapping;
import model.MemberDrmLog;
import model.MemberEplanDrmLog;

public class MemberDrmHandler {
    
    private static DPLogger logger =DPLogger.getLogger(MemberDrmHandler.class.getName());
    
    public String addTrial(EntityManager postgres, final String type, final String memberId, final String itemId, final String url) throws ServerException {
        String jsonStr = "";
        try {
            boolean isTrial = type.equals("4");
            if (isTrial) {
                writeInternalLog("method=POST&deviceId=booksAPI-addMemberBookDRM&bMemberId=" + memberId + "&Item=" + itemId, url);
            }

            postgres.getTransaction().begin();
            
            Member member = Member.getOrCreateNormalReader(postgres, memberId);
            List<BookFile> bookFiles = BookFile.findByItemIdAndTrial(postgres, member, itemId, isTrial);
            if (null == bookFiles || bookFiles.isEmpty()) {
                throw new ServerException("id_err_303", " Book not found");
            }
            BookFile tmpBook = bookFiles.get(0);
            MemberBook mb = member.getMemberTrialBookByItemId(postgres, itemId);
            Item item = Item.getItemByItemId(postgres, itemId);
            if (tmpBook == null) {
                throw new ServerException("id_err_303", " Book not found");
            }

            if (mb == null) {
                mb = new MemberBook();
                mb.updateByBookFile(tmpBook);
                mb.setMember(member);
                member.addMemberBook(mb);
                mb.setDefaultReadList(mb);
            }else{
                mb.setLastUpdated(CommonUtil.zonedTime());
                mb.setCreateTime(new Date());
                mb.setAction("update");
                mb.setDefaultReadList(mb);
            }
            
            postgres.persist(mb);
            postgres.flush();
            postgres.getTransaction().commit();
            
            // Response
            MemberBookDRMResultTrue records = genResponse(postgres, String.valueOf(member.getId()), item, type, url);
            
            if (null == records.getLink_url() || records.getLink_url().isEmpty()) {
                ServerException e = new ServerException("id_err_313", "book process is not finished");
                throw e;
            }
            
            jsonStr = toJson(records);
        } catch (ServerException e) {
            throw e;
        } catch (Exception ex) {
            logger.error(ex);;
            throw new ServerException("id_err_999", "Unknown Error " + ex.getMessage());
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        return jsonStr;
    }
    
    public String addDRM(EntityManager postgres, final MemberDrmLogDTO memberDrmLogDTO) throws ServerException, UnsupportedOperationException, IOException {
        String jsonStr = "";

        try {
            // Check existence of item.
            Item item = postgres.find(Item.class, memberDrmLogDTO.getItemId());

            if (null == item) {
                ServerException e = new ServerException("id_err_303", "Book not found.");
                throw e;
            }
            
            postgres.getTransaction().begin();

            // 因為bmember_id會有重覆資料需要依照member type 取得不同身分的會員, 若是會員資料不存在, 新增一筆NormalReader會員資料
            Member member = Member.getOrCreateReaderByType(postgres, memberDrmLogDTO.getBooksMemberId(), memberDrmLogDTO.getMemberType());

            //TODO if type=6 確認eplan的授權 status = 1,read exp time >new date . Exception eplan drm not found.
            if("6".equals(memberDrmLogDTO.getType())) {
            	// Check existence of eplan.
                Eplan eplan = postgres.find(Eplan.class, memberDrmLogDTO.getSubmitId());

                if (eplan == null) {
                    ServerException e = new ServerException("id_err_303", "Eplan not found.");
                    throw e;
                }
            	
                List<MemberEplanDrmLog> memberEplanDrmLogs = listMemberEplanDRMLogDesc(postgres, member, eplan, memberDrmLogDTO.getTransactionId());
                
                if(memberEplanDrmLogs != null && memberEplanDrmLogs.size() > 0){
                	MemberEplanDrmLog memberEplanDrmLog = memberEplanDrmLogs.get(0);
                	
                	SimpleDateFormat sdf =new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                	Date readExpireTime = sdf.parse(memberEplanDrmLog.getReadExpireTime());
                	Date now = new Date();
                	
                	if(memberEplanDrmLog.getStatus() == 1 && readExpireTime.compareTo(now) > 0) {
                		//給月租包授權中授權最久的時間
                		memberDrmLogDTO.setReadExpireTime(memberEplanDrmLog.getReadExpireTime());
                	}else{
                		ServerException e = new ServerException("id_err_303", "Eplan drm not found.");
                        throw e;
                	}
                }else {
                	ServerException e = new ServerException("id_err_303", "Eplan drm not found.");
                    throw e;
                }
            }
            
            // set by other = member exists (case: third party)
            // Check existence of DRM.
            // 以 member_id, item, transaction_id作為key，同key不能重複授權，其餘無限制
            // type=1,3 item會共用，type=2 item不會與type=1,3共用
            // 商品類型(1=一般電子書,2=贈品,3=免費領取,4=試閱收藏,5=租賃,6=吃到飽,7=章節)
            List<MemberDrmLog> memberBookDRM = listAllMemberDRMLog(postgres,member,memberDrmLogDTO.getTransactionId(), item, Integer.valueOf(memberDrmLogDTO.getType()),memberDrmLogDTO.getSubmitId());

            // 判斷是否為套書
            boolean isSerialBook = ("serial".equalsIgnoreCase(item.getItemType()))  && !item.getChilds().isEmpty();

            MemberDrmLog newMemberDrmLog = null;
             
            // 如果DRM是空的 則可以新增DRM
            if (memberBookDRM.isEmpty()) {
                if (!isSerialBook) {
                    MemberBook memberBook = createOrFindMemberBook(postgres, member, item);
                    if (memberBook == null) {
                        new ServerException("id_err_313", "book process is not finished");
                    }
                    // Add single member book DRM.
                    // 新增DRM關係對應
                    newMemberDrmLog = addDRMData(postgres, member, item, memberDrmLogDTO);

                    if (null != memberBook && null != newMemberDrmLog ) {
                        MemberBookDrmMapping memberBookDrmMapping = new MemberBookDrmMapping();
                        memberBookDrmMapping.setMemberId(member.getId());
                        memberBookDrmMapping.setMemberBook(memberBook);
                        memberBookDrmMapping.setMemberDrmLog(newMemberDrmLog);
                        memberBookDrmMapping.setStatus("update");
                        memberBookDrmMapping.setUdt(Date.from(ZonedDateTime.parse(CommonUtil.zonedTime()).toInstant()));
                        postgres.persist(memberBookDrmMapping);
                    }

                } else if (isSerialBook) {
                    newMemberDrmLog = addDRMData(postgres, member, item, memberDrmLogDTO);
                }
                
            } else {
               newMemberDrmLog = memberBookDRM.get(0);
               //重新設定為可用 DRM ,修改狀態由-1(刪除),改為 1(啟用)
               newMemberDrmLog.setStatus(1);
               SimpleDateFormat sdf =new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
               if (StringUtils.isNotBlank(memberDrmLogDTO.getReadExpireTime())) {
            	   newMemberDrmLog.setReadExpireTime(memberDrmLogDTO.getReadExpireTime());
               }else{
                   if(StringUtils.isNotBlank(memberDrmLogDTO.getReadDays())) {
                       Calendar cal = Calendar.getInstance();
                       cal.setTime(new Date());
                       cal.set(Calendar.DATE, cal.get(Calendar.DATE)+Integer.valueOf(memberDrmLogDTO.getReadDays()));
                       newMemberDrmLog.setReadExpireTime(sdf.format(cal.getTime()));
                   }else {
                	   newMemberDrmLog.setReadExpireTime("2099/12/31 23:59:59");
                   }
               }

               newMemberDrmLog.setLastUpdated(CommonUtil.fromDateToString(java.sql.Timestamp.valueOf(LocalDateTime.now())));
               if (!isSerialBook) {
               MemberBook memberBook = createOrFindMemberBook(postgres, member, item);
                // 新增DRM關係對應
                if (memberBook != null && checkMappingNotExist(postgres, memberBook.getId(), memberBookDRM.get(0).getId())) {
                    MemberBookDrmMapping memberBookDrmMapping = new MemberBookDrmMapping();
                    memberBookDrmMapping.setMemberBook(memberBook);
                    memberBookDrmMapping.setMemberId(member.getId());
                    memberBookDrmMapping.setMemberDrmLog(memberBookDRM.get(0));
                    memberBookDrmMapping.setStatus("update");
                    memberBookDrmMapping.setUdt(Date.from(ZonedDateTime.parse(CommonUtil.zonedTime()).toInstant()));
                    postgres.persist(memberBookDrmMapping);
                }
                postgres.persist(memberBookDRM.get(0));
               }
                   
            }

            // 新增該會員的套書內子書及其資料 (memebrBook,memberDrmLog,memberBookDrmMapping)
            if (isSerialBook) {
                pushRescanDRMJob(postgres, item.getId(), member.getId());

                // 根據套書ITEM ID 順序 修改時間,供排序使用
                if (newMemberDrmLog != null) {
                    postgres.refresh(newMemberDrmLog);
                    List<MemberBook> memberBooks = newMemberDrmLog.getMemberBooks();
                    if (memberBooks.size() > 0) {
                        List<MemberBook> sortMemberBookList = new ArrayList<MemberBook>();
                        sortMemberBookList.addAll(memberBooks);
                        sortMemberBookList.sort((p1, p2) -> p1.getItem().getId().compareTo(p2.getItem().getId()));
                        Calendar modifyDate = Calendar.getInstance();
                        sortMemberBookList.stream().forEach(memberBook -> {
                            modifyDate.set(Calendar.SECOND,modifyDate.get(Calendar.SECOND) + sortMemberBookList.indexOf(memberBook));
                            memberBook.setCreateTime(modifyDate.getTime());
                        });

                        for (MemberBook modifedTimeMemberBook : sortMemberBookList) {
                        	//若子書是被刪除狀態，需在新增套書時改回來
                        	if("del".equals(modifedTimeMemberBook.getAction())) {
                        		modifedTimeMemberBook.setAction("update");
                        		modifedTimeMemberBook.setDeleted(false);
                        	}
                            postgres.persist(modifedTimeMemberBook);

                        }
                    }
                }
            }

            postgres.getTransaction().commit();

            // Response
            MemberBookDRMResultTrue records  = null;
            if(isSerialBook) {
                records =  genSerialResponse(postgres, String.valueOf(member.getId()), item, memberDrmLogDTO.getType(), memberDrmLogDTO.getViewerBaseURI());
            }else {
                records = genResponse(postgres, String.valueOf(member.getId()), item, memberDrmLogDTO.getType(), memberDrmLogDTO.getViewerBaseURI());
            }
            
            if (null == records.getLink_url() || records.getLink_url().isEmpty()) {
                ServerException e = new ServerException("id_err_313", "book process is not finished");
                throw e;
            }
            
            records.setRead_expire_time(newMemberDrmLog.getReadExpireTime());
            jsonStr = toJson(records);
        } catch (ServerException e) {
            throw e;
        } catch (Exception ex) {
            logger.error(ex);;
            throw new ServerException("id_err_999", "Unknown Error " + ex.getMessage());
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        return jsonStr;
    }
    
    public String modifyDRM(EntityManager postgres, final MemberDrmLogDTO memberDrmLogDTO) throws ServerException, UnsupportedOperationException, IOException {
        boolean isTrial = memberDrmLogDTO.getType().equals("4");

        String jsonStr = "";

        try {
            
            if(StringUtils.isNotBlank(memberDrmLogDTO.getStatus()) && StringUtils.equalsIgnoreCase("-1", memberDrmLogDTO.getStatus())){
                ServerException e = new ServerException("id_err_303", "use delDRM for status -1.");
                throw e;
            }
            
            postgres.getTransaction().begin();

            Member member = Member.getMemberByBMemberId(postgres, memberDrmLogDTO.getBooksMemberId(), Member.MEMBERTYPE_NORMALREADER);
            Item item = Item.getItemByItemId(postgres, memberDrmLogDTO.getItemId());
            
            if (null == item) {
                ServerException e = new ServerException("id_err_303", "Book not found.");
                throw e;
            }

            MemberDrmLog drmObject = MemberDrmLog.getMemberDrmLogByTransactionIdWithTypeAndSubmitId(postgres, member, item, memberDrmLogDTO.getTransactionId(), Integer.valueOf(memberDrmLogDTO.getType()),memberDrmLogDTO.getSubmitId());
            
            if (drmObject == null) {
                throw new ServerException("id_err_501", "DRM not exist.");
            }
            
            boolean isSerialBook = ("serial".equals(item.getItemType()) ) && !item.getChilds().isEmpty();
            
            //重新設定為可用 DRM ,修改狀態由-1(刪除),改為 1(啟用)
            if(drmObject.getStatus() == -1 && StringUtils.isNotBlank(memberDrmLogDTO.getStatus()) && StringUtils.endsWithIgnoreCase("1", memberDrmLogDTO.getStatus())){
               MemberBook memberBook = createOrFindMemberBook(postgres, member, item);
                // 新增DRM關係對應
                if (memberBook != null && checkMappingNotExist(postgres, memberBook.getId(), drmObject.getId())) {
                    MemberBookDrmMapping memberBookDrmMapping = new MemberBookDrmMapping();
                    memberBookDrmMapping.setMemberBook(memberBook);
                    memberBookDrmMapping.setMemberId(member.getId());
                    memberBookDrmMapping.setMemberDrmLog(drmObject);
                    memberBookDrmMapping.setStatus("update");
                    memberBookDrmMapping.setUdt(Date.from(ZonedDateTime.parse(CommonUtil.zonedTime()).toInstant()));
                    postgres.persist(memberBookDrmMapping);
                }
                //如果重新啟用為套書則更新或新增該會員的套書內子書及其資料 (memebrBook,memberDrmLog,memberBookDrmMapping)
                if (isSerialBook) {
                    pushRescanDRMJob(postgres, item.getId(), member.getId());
                }
            }
            
            //更新DRM資料
            drmObject.setMember(member);
            drmObject.setItem(item);
            if(StringUtils.isNoneBlank(memberDrmLogDTO.getStatus())){
                drmObject.setStatus(Integer.valueOf(memberDrmLogDTO.getStatus()));
            }
            drmObject = updateDrm(drmObject, memberDrmLogDTO);
            
        	//更新書籍權限後，更新memberBook最後更新時間(避免取不到買斷或月租書籍)
        	MemberBook memberBook = member.getMemberNormalBookByItemId(postgres, drmObject.getItem().getId());
        	if(memberBook != null) {
        		memberBook.setLastUpdated(CommonUtil.zonedTime());
            	postgres.persist(memberBook);
        	}
        	
            postgres.getTransaction().commit();

            // Response
            MemberBookDRMResultTrue records = new MemberBookDRMResultTrue();
            records.setResult(true);
            jsonStr = toJson(records);
        } catch (ServerException ex) {
            if (ex.getError_code().endsWith("999")) {
                logger.error(ex);;
            }

            throw ex;
        } catch (Exception ex) {
            logger.error(ex);;
            throw new ServerException("id_err_999", "status:"+memberDrmLogDTO.getStatus()+",Unknown Exception " + ex.getMessage());
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        return jsonStr;
    }
    
    public String delDRM(EntityManager postgres, final MemberDrmLogDTO memberDrmLogDTO) throws ServerException, UnsupportedOperationException, IOException {

        String jsonStr = "";
        
        try {
            postgres.getTransaction().begin();
            logger.info("權限刪除開始 : delDRM start");
            
            Member member = Member.getMemberByBMemberId(postgres, memberDrmLogDTO.getBooksMemberId(), Member.MEMBERTYPE_NORMALREADER);
            Item item = Item.getItemByItemId(postgres, memberDrmLogDTO.getItemId());

            if (null == item) {
                ServerException e = new ServerException("id_err_303", "Book not found.");
                throw e;
            }
            
            // Check child items exists.
            if (item.getChilds().size() > 0) {
                List<Item> childItems = item.getChildItems(postgres);
                if (childItems.size() <= 0) {
                    throw new ServerException("id_err_501", "child items not exist.");
                }
            }
            
            // find member book DRM.
            MemberDrmLog drmObject = MemberDrmLog.getMemberDrmLogByTransactionIdWithTypeAndSubmitId(postgres, member, item, memberDrmLogDTO.getTransactionId(), Integer.valueOf(memberDrmLogDTO.getType()),memberDrmLogDTO.getSubmitId());
            
            if (null == drmObject) {
                throw new ServerException("id_err_501",
                        " DRM not exists. "
                        + "\nMember="+ member.getBmemberId()
                        + "\nItem=" + item.getId()
                        + "\nTransaction_id=" + memberDrmLogDTO.getTransactionId()
                        );
            }

            //刪除 mapping表關係
            ZonedDateTime nowDateTime = CommonUtil.currentDate();
            ZonedDateTime expireDateTime = CommonUtil.convertOldDateToLocalDateTime(drmObject.getReadExpireTime());
            if (expireDateTime.isAfter(nowDateTime)||drmObject.getType() == 1||drmObject.getType() == 6) {
                //未過期 將 DRM 關係刪除
                for (MemberBookDrmMapping memberBookDrmMapping : drmObject.getMemberBookDrmMappings()) {
                    postgres.remove(memberBookDrmMapping);
                }
            }
              
            //將DRM設定為 -1 (刪除DRM)
            drmObject.setStatus(-1);
            drmObject.setLastUpdated(CommonUtil.zonedTime());
            postgres.persist(drmObject);

            //檢查如果memberbook 不存在DRM則刪除
            // Check existence of member book.(If member book drm not exist or expired, delete it.)
            MemberBook memberBook = member.getMemberNormalBookByItemId(postgres,item.getId());
            
            if (null != memberBook) {
                
                boolean isNeedDelMemberBook = false;
                if(memberBook.getMemberDrmLogs().isEmpty()){
                    isNeedDelMemberBook = true;
                }else{
                	List<MemberDrmLog> drmList = new ArrayList<MemberDrmLog>();
                    MemberDrmLog longestMemberDrmLog = DRMLogicUtility.getLongestMemberDrmLogAndList(memberBook.getMemberDrmLogs(), drmList);
                    if(longestMemberDrmLog!=null){
                        ZonedDateTime longestExpireDateTime = CommonUtil.convertOldDateToLocalDateTime(longestMemberDrmLog.getReadExpireTime());
                        if (nowDateTime.isAfter(longestExpireDateTime)) {
                        	//最大授權過期，整本書及授權皆須刪除，則不用再檢查月租授權
                            isNeedDelMemberBook = true;
                        }else {
                        	//檢查是否有月租授權過期，因還有授權沒有過期，所以不能整本書刪除(因最大授權沒有過期，所以不能整本書刪除，只刪除該授權及mapping)
                        	for(MemberDrmLog drmLog : drmList) {
                        		
                        		//若有有月租授權過期，刪除過期授權及mapping，不刪除書籍
                        		if(drmLog.getType() == 6 && nowDateTime.isAfter(CommonUtil.convertOldDateToLocalDateTime(drmLog.getReadExpireTime()))) {
	                				// 過期 將 DRM 關係刪除
                	                for (MemberBookDrmMapping memberBookDrmMapping : drmLog.getMemberBookDrmMappings()) {                    
                	                    postgres.remove(memberBookDrmMapping);
                	                }

                	                // 將DRM設定為 -1 (刪除DRM)
                	                drmLog.setStatus(-1);
                	                drmLog.setLastUpdated(CommonUtil.zonedTime());
                	                postgres.persist(drmLog);
                        		}
                        	}
                        }
                    }
                }
                
                if(isNeedDelMemberBook){
                    memberBook.markAsDelete();
                    List<MemberBook> childMemberBooks = new ArrayList<>();
                    childMemberBooks = member.listChildMemberBooksByItemId(postgres, item.getChilds());
                    for (MemberBook childMemberBook : childMemberBooks) {
                        childMemberBook.markAsDelete();
                        postgres.persist(childMemberBook);
                    }
                    postgres.persist(memberBook);
                }else {
                	//刪除書籍權限後，更新memberBook最後更新時間(避免取不到買斷或月租書籍)
                	memberBook.setLastUpdated(CommonUtil.zonedTime());
                	postgres.persist(memberBook);
                }
                
            }
            
            logger.info("權限刪除結束 : delDRM end");
            postgres.getTransaction().commit();

            // Response
            MemberBookDRMResultTrue records = new MemberBookDRMResultTrue();
            records.setResult(true);
            jsonStr = toJson(records);
        } catch (ServerException ex) {
            logger.error(ex);
            throw ex;
        } catch (Exception ex) {
            logger.error(ex);
            throw new ServerException("id_err_999", ex.getMessage());
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        return jsonStr;
    }
    
    public boolean delDRMifExpire(EntityManager postgres, Long memberDrmLogId) {

        boolean isDeleted = false;
        
        
        try {

            MemberDrmLog memberDrmLog =MemberDrmLog.getMemberDrmLogById(postgres, memberDrmLogId);
            
            ZonedDateTime nowDateTime = CommonUtil.currentDate();
            
            if (nowDateTime.isAfter(CommonUtil.convertOldDateToLocalDateTime(memberDrmLog.getReadExpireTime()))) {

                
                postgres.getTransaction().begin();
                Member member = memberDrmLog.getMember();
                Item item = Item.getItemByItemId(postgres, memberDrmLog.getItem().getId());

                if (null == item) {
                    ServerException e = new ServerException("id_err_303", "Book not found.");
                    throw e;
                }

                // Check child items exists.
                if (item.getChilds().size() > 0) {
                    List<Item> childItems = item.getChildItems(postgres);
                    if (childItems.size() <= 0) {
                        throw new ServerException("id_err_501", "child items not exist.");
                    }
                }

                // 過期 將 DRM 關係刪除
                for (MemberBookDrmMapping memberBookDrmMapping : memberDrmLog.getMemberBookDrmMappings()) {
                    
                    // Check existence of member book.(If member book drm not exist or expired, delete it.)
                    MemberBook memberBook = memberBookDrmMapping.getMemberBook();
                    if (null != memberBook) {
                        //將書籍設定為刪除
                        memberBook.markAsDelete();
                        postgres.persist(memberBook);
                    }
                    
                    postgres.remove(memberBookDrmMapping);
                }

                // 將DRM設定為 -1 (刪除DRM)
                memberDrmLog.setStatus(-1);
                memberDrmLog.setLastUpdated(CommonUtil.zonedTime());
                postgres.persist(memberDrmLog);
                
                postgres.getTransaction().commit();
                
                isDeleted = true;
            }
        } catch (ServerException ex) {
            logger.error(ex);;
        } catch (Exception ex) {logger.error(ex);
            logger.error(ex);;
        } finally {
            if (postgres.getTransaction().isActive()) {
                postgres.getTransaction().rollback();
            }
        }

        
        return isDeleted;
    }
    
    public boolean delEplanDRMifExpire(EntityManager postgres, MemberBook memberBook, List<MemberDrmLog> drmList) {
    	boolean hasEplanDrmDeleted = false; //是否有刪除月租權限
    	boolean hasEplanDrm = false; //是否還有月租權限
    	boolean isDeleteAllEplanDrm = false; //是否有刪除，並刪除所有月租權限
    	
    	try {	
	    	postgres.getTransaction().begin();
//	    	logger.info("月租權限刪除開始 delEplanDRMifExpire start");
	    	
			//檢查是否有月租授權過期，因還有授權沒有過期，所以不能整本書刪除(因最大授權沒有過期，所以不能整本書刪除，只刪除該授權及mapping)
			for (MemberDrmLog drmLog : drmList) {
				boolean isDeleted = false;
				
				MemberDrmLog memberDrmLog =MemberDrmLog.getMemberDrmLogById(postgres, drmLog.getId());
				
				ZonedDateTime nowDateTime = CommonUtil.currentDate();
				//月租授權到期則刪除該授權及mapping
	            if (memberDrmLog.getType() == 6) {
	            	if(nowDateTime.isAfter(CommonUtil.convertOldDateToLocalDateTime(memberDrmLog.getReadExpireTime()))) {
	            		//刪除過期的月租授權
	            		Item item = Item.getItemByItemId(postgres, memberDrmLog.getItem().getId());

		                if (null == item) {
		                    ServerException e = new ServerException("id_err_303", "Book not found.");
		                    throw e;
		                }

		                // Check child items exists.
		                if (item.getChilds().size() > 0) {
		                    List<Item> childItems = item.getChildItems(postgres);
		                    if (childItems.size() <= 0) {
		                        throw new ServerException("id_err_501", "child items not exist.");
		                    }
		                }

		                // 過期 將 DRM 關係刪除
		                for (MemberBookDrmMapping memberBookDrmMapping : memberDrmLog.getMemberBookDrmMappings()) {                    
		                    postgres.remove(memberBookDrmMapping);
		                }

		                // 將DRM設定為 -1 (刪除DRM)
		                memberDrmLog.setStatus(-1);
		                memberDrmLog.setLastUpdated(CommonUtil.zonedTime());
		                postgres.persist(memberDrmLog);
		                
		                isDeleted = true;
	            	}else {
	            		//還有未過期的月租授權
	            		hasEplanDrm = true;
	            	}
	            }
	            
				if(isDeleted) {
					hasEplanDrmDeleted = true;
				}
			}
			
			//若有刪除權限，則須更新memberBook的最後更新時間
			if(hasEplanDrmDeleted) {
				memberBook.setLastUpdated(CommonUtil.zonedTime());
	            postgres.persist(memberBook);
			}
			
//			logger.info("月租權限刪除結束 delEplanDRMifExpire end");
			postgres.getTransaction().commit();
			
			//此次回收有刪除月租授權，並將所有月租授權刪除
			if(hasEplanDrmDeleted && !hasEplanDrm) {
				isDeleteAllEplanDrm = true;
			}
			
    	} catch (ServerException ex) {
    		logger.error(ex);;
    	} catch (Exception ex) {logger.error(ex);
    		logger.error(ex);
    	} finally {
    		if (postgres.getTransaction().isActive()) {
    			postgres.getTransaction().rollback();
    		}
    	}
    	
    	return isDeleteAllEplanDrm;
    }

    private List<MemberDrmLog> listAllMemberDRMLog(EntityManager postgres,Member member,String transactionId, Item item,int type , String submitId) {
        return postgres
                .createNamedQuery("MemberDrmLog.findByAllMemberAndItemAndTransactionIdWithTypeAndSubmitId", MemberDrmLog.class)
                .setParameter("member", member)
                .setParameter("item", item)
                .setParameter("transactionId", transactionId)
                .setParameter("type", type)
                .setParameter("submitId", submitId)
                .getResultList();
    }
    
    private boolean checkMappingNotExist(EntityManager postgres, Long memberBookId, Long drmLogId){
    	int count = (int) (long)postgres.createNativeQuery("select  count(mbdm.*) from member_book_drm_mapping mbdm WHERE mbdm.member_book_id = " + memberBookId + " and mbdm.member_book_drm_log_id = " + drmLogId).getSingleResult();
    	return count == 0 ;
    }

    private MemberBookDRMResultTrue genResponse(EntityManager postgres,final String memberId, Item item,final String drmType,final String viewerBaseURI) {
        MemberBookDRMResultTrue ret = new MemberBookDRMResultTrue();

        // SomeDay has more child = single link url is not working.
//      if (item.getChilds() != null && item.getChilds().size() > 0) {
//          // Add member book DRMs.
//          List<Item> childItems = item.getChildItems(postgres);
//
//          for (Item childItem : childItems) {
//                ret.setLink_url(getReturnUrl(postgres, memberId, childItem, drmType, viewerBaseURI));
//          }
//      } else {
            ret.setLink_url(getReturnUrl(postgres, memberId, item, drmType, viewerBaseURI));
//      }

        return ret;
    }
    
    private MemberBookDRMResultTrue genSerialResponse(EntityManager postgres, final String memberId, Item item, final String drmType, final String viewerBaseURI) {
        MemberBookDRMResultTrue ret = new MemberBookDRMResultTrue();

        // SomeDay has more child = single link url is not working.
        if (item.getChilds() != null && item.getChilds().size() > 0) {
            // Add member book DRMs.
            List<Item> childItems = item.getChildItems(postgres);

            for (Item childItem : childItems) {
                ret.setLink_url(getReturnUrl(postgres, memberId, childItem, drmType, viewerBaseURI));
            }
        } else {
            ret.setLink_url(getReturnUrl(postgres, memberId, item, drmType, viewerBaseURI));
        }

        return ret;
    }

    private MemberDrmLog updateDrm(MemberDrmLog drmObject ,final MemberDrmLogDTO memberDrmLogDTO ) {
        if (StringUtils.isNotBlank(memberDrmLogDTO.getDownloadExpireTime())) {
            drmObject.setDownloadExpireTime(memberDrmLogDTO.getDownloadExpireTime());
        }

        if (StringUtils.isNotBlank(memberDrmLogDTO.getReadDays())) {
            drmObject.setReadDays(Integer.valueOf(memberDrmLogDTO.getReadDays()));
        }
        
        if (StringUtils.isNotBlank(memberDrmLogDTO.getReadStartTime())) {
            drmObject.setReadStartTime(memberDrmLogDTO.getReadStartTime());
        }
        
        if (StringUtils.isNotBlank(memberDrmLogDTO.getReadExpireTime())) {
            drmObject.setReadExpireTime(memberDrmLogDTO.getReadExpireTime());
        }
        drmObject.setType(Integer.valueOf(memberDrmLogDTO.getType()));
        drmObject.setTransactionId(memberDrmLogDTO.getTransactionId());
        drmObject.setLastUpdated(CommonUtil.zonedTime());
        return drmObject;
    }

    private MemberDrmLog addDRMData(EntityManager postgres,Member member, Item item,final MemberDrmLogDTO memberDrmLogDTO) throws ServerException {
        MemberDrmLog drmObject = new MemberDrmLog();

        drmObject.setItem(item);
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        if (StringUtils.isNotBlank(memberDrmLogDTO.getDownloadExpireTime())) {
            drmObject.setDownloadExpireTime(memberDrmLogDTO.getDownloadExpireTime());
        }

        if (StringUtils.isNotBlank(memberDrmLogDTO.getReadDays())) {
            drmObject.setReadDays(Integer.valueOf(memberDrmLogDTO.getReadDays()));
        }
        
        if (StringUtils.isNotBlank(memberDrmLogDTO.getReadStartTime())) {
            drmObject.setReadStartTime(memberDrmLogDTO.getReadStartTime());
        }else{
            drmObject.setReadStartTime(sdf.format(new Date()));
        }
        
        if (StringUtils.isNotBlank(memberDrmLogDTO.getReadExpireTime())) {
            drmObject.setReadExpireTime(memberDrmLogDTO.getReadExpireTime());
        }else{
            if(StringUtils.isNotBlank(memberDrmLogDTO.getReadDays())) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.set(Calendar.DATE, cal.get(Calendar.DATE)+Integer.valueOf(memberDrmLogDTO.getReadDays()));
                drmObject.setReadExpireTime(sdf.format(cal.getTime()));
            }else {
                drmObject.setReadExpireTime("2099/12/31 23:59:59");
            }
        }
        
        drmObject.setType(Integer.valueOf(memberDrmLogDTO.getType()));
        drmObject.setTransactionId(memberDrmLogDTO.getTransactionId());
        drmObject.setSubmitId(memberDrmLogDTO.getSubmitId());
        drmObject.setStatus(1);
        drmObject.setLastUpdated(CommonUtil.zonedTime());

        member.addMemberDrmLog(drmObject);

        postgres.persist(drmObject);
        return drmObject;
    }

    private MemberBook createOrFindMemberBook(EntityManager postgres, Member member, Item item) throws ServerException {
        MemberBook mb = null;
        Boolean isTrial = false;

        try {
            List<BookFile> bookFiles = BookFile.findByItemIdAndTrial(postgres, member, item.getId(), isTrial);

            if (null == bookFiles || bookFiles.isEmpty()) {
                return null;
            }

            mb = member.getMemberNormalBookByItemId(postgres, item.getId());

            if (null == mb) {

                mb = new MemberBook();

                if (null == bookFiles || (bookFiles.isEmpty() && item.getChilds().isEmpty())) {
                    throw new ServerException("id_err_301", " no corrosponding type found " + item.getId() + " trial flag: " + isTrial);
                }

                for (BookFile bookFile : bookFiles) {
                    mb.updateByBookFile(bookFile);
                    if (!member.isNormalReader())
                        mb.setCreateTime(bookFile.getCreateTime());
                }
                
                mb.setDefaultReadList(mb);
            }
            
            mb.setLastUpdated(CommonUtil.zonedTime());
            mb.setCreateTime(new Date());
            mb.setAction("update");
            member.addMemberBook(mb);
            postgres.persist(mb);
            postgres.flush();

        } catch (ServerException ex) {
            logger.error(ex);;
            throw ex;
        } catch (Exception ex) {
            logger.error(ex);
            throw new ServerException("id_err_999", ex.getMessage());
        }
        return mb;
    }

    private void pushRescanDRMJob(EntityManager postgres, String itemId, long memberId) {
        try {

            logger.info("[Book] rescanItem: " + itemId);
            new RescanDRMHandler().rescanMemberDRM(postgres, itemId,memberId);

        } catch (Exception ex) {
            logger.error(ex);
        }
    }
    
    private List<MemberEplanDrmLog> listMemberEplanDRMLogDesc(EntityManager postgres,Member member, Eplan eplan, String transactionId) {
        return postgres
                .createNamedQuery("MemberEplanDrmLog.findByMemberAndEplanAndTransactionIdDesc", MemberEplanDrmLog.class)
                .setParameter("member", member)
                .setParameter("eplan", eplan)
                .setParameter("transactionId", transactionId)
                .getResultList();
    }

    private String getReturnUrl(EntityManager postgres, String memberId, Item item, String drmType, String viewerBaseURI) {
        String returnUrl = "";
        String cur_format = "";
        String cur_version = "";
        String cur_type = "";
        boolean isTrial = drmType.equals("4");

        String queryString = "SELECT mb.* FROM member_book mb"
                + " INNER JOIN book_file bf ON mb.book_file_id = bf.id"
                + " WHERE mb.member_id = " + memberId + ""
                + " AND mb.item_id = '" + StringHelper.escapeSQL(item.getId()) + "'"
                + " AND bf.is_trial = " + isTrial + "";

        @SuppressWarnings("unchecked")
        List<MemberBook> memberBooks = postgres.createNativeQuery(queryString, MemberBook.class).getResultList();

//      List<MemberBook> memberBooks = postgres.createNamedQuery("MemberBook.findAllByMemberIdAndItem", MemberBook.class)
//              .setParameter("memberId", member.getId())
//              .setParameter("item", item)
//              .getResultList();

        for (MemberBook memberBook : memberBooks) {
            if (memberBook.getBookFile().getFormat().toLowerCase().equals(BaseBookInfo.BookFormatPDF)) {
                cur_format = BaseBookInfo.BookFormatPDF;
            } else {
                cur_format = "epub";
            }

            cur_version = memberBook.getBookFile().getVersion();

            // type: 1=一般電子書, 2=贈品, 3=免費領取, 4=試閱收藏, 5=租賃, 6=吃到飽, 7=章節
//          if (true == memberBook.getBookFile().getIsTrial()) {
//              cur_type = "&type=trial";
//          }

            if(memberBook.getBookType().equalsIgnoreCase("audiobook")||memberBook.getBookType().equalsIgnoreCase("mediabook")) {
                returnUrl = viewerBaseURI + "av" + "/?book_uni_id=" + memberBook.getBookUniId() + "&cur_version=" + cur_version + cur_type;
            }else {
                returnUrl = viewerBaseURI + cur_format + "/web/?book_uni_id=" + memberBook.getBookUniId() + "&cur_version=" + cur_version + cur_type;
            }
            
        }

        return returnUrl;
    }

    private void writeInternalLog(String para, String url) throws UnsupportedOperationException, IOException {
        String paras = "servletName=MemberBookDRMServlet&" + para;
        String logURL = url + "?" + paras;

        try {
            CommonUtil.httpClient(logURL);
        } catch (Exception ex) {
            logger.error(ex);;
        }
    }

    class MemberBookDRMResultTrue {
        String read_expire_time;
        String link_url;
        Boolean result;

        public String getRead_expire_time() {
            return read_expire_time;
        }

        public void setRead_expire_time(String read_expire_time) {
            this.read_expire_time = read_expire_time;
        }
        
        public String getLink_url() {
            return link_url;
        }

        public void setLink_url(String link_url) {
            this.link_url = link_url;
        }

        public Boolean getResult() {
            return result;
        }

        public void setResult(Boolean result) {
            this.result = result;
        }
    }

    class MemberBookDRMResultError {
        String error_code;
        String error_message;

        public String getError_code() {
            return error_code;
        }

        public void setError_code(String error_code) {
            this.error_code = error_code;
        }

        public String getError_message() {
            return error_message;
        }

        public void setError_message(String error_message) {
            this.error_message = error_message;
        }
    }
}