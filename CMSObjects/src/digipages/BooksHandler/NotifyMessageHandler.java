package digipages.BooksHandler;

import static org.boon.Boon.toJson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import model.Member;
import model.MemberDevice;
import model.MessageQueue;

public class NotifyMessageHandler {
	
	private static DPLogger logger =DPLogger.getLogger(NotifyMessageHandler.class.getName());
	
	private String messageId = "";
	private String messageContent = "";
	private String sendStartDate = "";
	private String sendExpireDate = "";
	private String sendTime = "";
	private String expireTime = "";
	private String targetId = "";
	private String fileUri = "";
	private int splitNum = 3000;
	private String updatedTime = "";
	private String sessionToken = "";

	public String addNotifyMessage(EntityManager postgres, String pushApiUrl) throws ServerException, UnsupportedOperationException, IOException {
		String jsonStr = "";

		try {
			MessageQueue mqObject = new MessageQueue();

			mqObject.setMessageId(messageId);
			mqObject.setContent(messageContent);

			if (null != sendStartDate && !sendStartDate.isEmpty()) {
				if (isValidDate(sendStartDate)) {
					if (sendExpireDate.equals("") || sendTime.equals("")) {
						throw new ServerException("id_err_711", "push datetime empty");
					} else {
						mqObject.setStartDate(sendStartDate);
					}
				} else {
					throw new ServerException("id_err_708", "wrong datetime format");
				}
			}

			if (null != sendExpireDate && !sendExpireDate.isEmpty()) {
				if (isValidDate(sendExpireDate)) {
					if (sendStartDate.equals("") || sendTime.equals("")) {
						throw new ServerException("id_err_711", "push datetime empty");
					} else {
						mqObject.setExpireDate(sendExpireDate);
					}
				} else {
					throw new ServerException("id_err_708", "wrong datetime format");
				}
			}

			if (null != sendTime && !sendTime.isEmpty()) {
				if (isValidTime(sendTime)) {
					if (sendStartDate.equals("") || sendExpireDate.equals("")) {
						throw new ServerException("id_err_711", "push datetime empty");
					} else {
						mqObject.setSendTime(sendTime);
					}
				} else {
					throw new ServerException("id_err_709", "wrong time format");
				}
			}
			
			if (null != expireTime && !expireTime.isEmpty()) {
				if (isValidDate(expireTime)) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss"); 
					mqObject.setExpireTime(sdf.parse(expireTime));
				} else {
					throw new ServerException("id_err_709", "wrong expire_time yyyy/MM/dd hh:mm:ss format");
				}
			} else {
				throw new ServerException("id_err_709", "wrong expire_time yyyy/MM/dd hh:mm:ss format");
			}

			if (targetId.equals("*") && !fileUri.equals("")) {
				throw new ServerException("id_err_710", "wrong push notification target");
			} else if (targetId.equals("") && !fileUri.startsWith("http")) {
				throw new ServerException("id_err_710", "wrong push notification target");
			} else if (!targetId.equals("") && !fileUri.equals("")) {
				throw new ServerException("id_err_710", "wrong push notification target");
			}

			mqObject.setTargetId(targetId);
			mqObject.setFileUri(fileUri);
			mqObject.setStatus(9);
			mqObject.setExpandStatus(0);
			mqObject.setOsType("source");
			mqObject.setMsgMessageId(messageId);
			mqObject.setLastUpdated(CommonUtil.zonedTime());

			postgres.getTransaction().begin();
			postgres.persist(mqObject);
			postgres.getTransaction().commit();

			MessageQueue messageQueue = (MessageQueue) postgres.createNativeQuery("SELECT * FROM message_queue WHERE message_id = '" + messageId + "' AND os_type = 'source' AND msg_message_id = '" + messageId + "' AND status = 9 AND expand_status = 0", MessageQueue.class).getSingleResult();

			if (null != messageQueue) {
				// initiate schedule
				// initiateSchedule(postgres, messageQueue);

				// Response
				NotifyMessageResultTrue records = new NotifyMessageResultTrue();
				records.setResult(true);
				records.setPush_notification_id(messageQueue.getId().toString());
				jsonStr = toJson(records);
			} else {
				// Response
				NotifyMessageResultError records = new NotifyMessageResultError();
				records.setError_code("id_err_701");
				records.setError_message("Message not found");
				jsonStr = toJson(records);
			}
		} catch (ServerException e) {
			throw e;
		} catch (Exception ex) {
			logger.error(ex);;
			throw new ServerException("id_err_999", "Unknown Error " + ex.getMessage());
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		return jsonStr;
	}

	public String modifyNotifyMessage(EntityManager postgres, String pushApiUrl) throws ServerException, UnsupportedOperationException, IOException {
		String jsonStr = "";

		try {
			List<MessageQueue> messageQueues = postgres.createNamedQuery("MessageQueue.findAllByMessageId", MessageQueue.class).setParameter("messageId", messageId).getResultList();

			if (messageQueues.size() > 0) {
				MessageQueue mqObject = postgres.createNamedQuery("MessageQueue.findAllByMessageId", MessageQueue.class).setParameter("messageId", messageId).getSingleResult();

				mqObject.setContent(messageContent);

				if (null != sendStartDate && !sendStartDate.isEmpty()) {
					if (isValidDate(sendStartDate)) {
						if (sendExpireDate.equals("") || sendTime.equals("")) {
							throw new ServerException("id_err_711", "push datetime empty");
						} else {
							mqObject.setStartDate(sendStartDate);
						}
					} else {
						throw new ServerException("id_err_708", "wrong datetime format");
					}
				}

				if (null != sendExpireDate && !sendExpireDate.isEmpty()) {
					if (isValidDate(sendExpireDate)) {
						if (sendStartDate.equals("") || sendTime.equals("")) {
							throw new ServerException("id_err_711", "push datetime empty");
						} else {
							mqObject.setExpireDate(sendExpireDate);
						}
					} else {
						throw new ServerException("id_err_708", "wrong datetime format");
					}
				}

				if (null != sendTime && !sendTime.isEmpty()) {
					if (isValidTime(sendTime)) {
						if (sendStartDate.equals("") || sendExpireDate.equals("")) {
							throw new ServerException("id_err_711", "push datetime empty");
						} else {
							mqObject.setSendTime(sendTime);
						}
					} else {
						throw new ServerException("id_err_709", "wrong time format");
					}
				}
				
				if (null != expireTime && !expireTime.isEmpty()) {
					if (isValidDate(expireTime)) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
						mqObject.setExpireTime(sdf.parse(expireTime));
					} else {
						throw new ServerException("id_err_709", "wrong time format");
					}
				}

				if (targetId.equals("*") && !fileUri.equals("")) {
					throw new ServerException("id_err_710", "wrong push notification target");
				} else if (targetId.equals("") && !fileUri.startsWith("http")) {
					throw new ServerException("id_err_710", "wrong push notification target");
				} else if (!targetId.equals("") && !fileUri.equals("")) {
					throw new ServerException("id_err_710", "wrong push notification target");
				}

				mqObject.setTargetId(targetId);
				mqObject.setFileUri(fileUri);
				mqObject.setStatus(9);
				mqObject.setExpandStatus(0);
				mqObject.setLastUpdated(CommonUtil.zonedTime());

				postgres.getTransaction().begin();
				postgres.persist(mqObject);
				postgres.getTransaction().commit();

				@SuppressWarnings("unchecked")
				List<MessageQueue> delMessageQueues = postgres.createNativeQuery("SELECT * FROM message_queue WHERE msg_message_id = '" + messageId + "' AND os_type <> 'source'", MessageQueue.class).getResultList();

				if (delMessageQueues.size() > 0) {
					for (MessageQueue delMessageQueue : delMessageQueues) {
						if (delMessageQueue.getMsgMessageId().equals(messageId)) {
							postgres.getTransaction().begin();
							postgres.merge(delMessageQueue);
							postgres.remove(delMessageQueue);
							postgres.flush();
							postgres.getTransaction().commit();
						}
					}
				}

				MessageQueue messageQueue = (MessageQueue) postgres.createNativeQuery("SELECT * FROM message_queue WHERE message_id = '" + messageId + "' AND os_type = 'source' AND msg_message_id = '" + messageId + "' AND status = 9 AND expand_status = 0", MessageQueue.class).getSingleResult();

				if (null != messageQueue) {
					// initiate schedule
					// initiateSchedule(postgres, messageQueue);

					// Response
					NotifyMessageResultTrue records = new NotifyMessageResultTrue();
					records.setResult(true);
					records.setPush_notification_id(mqObject.getId().toString());
					jsonStr = toJson(records);
				} else {
					// Response
					NotifyMessageResultError records = new NotifyMessageResultError();
					records.setError_code("id_err_701");
					records.setError_message("Message not found");
					jsonStr = toJson(records);
				}
			} else {
				// Response
				NotifyMessageResultError records = new NotifyMessageResultError();
				records.setError_code("id_err_701");
				records.setError_message("Message not found");
				jsonStr = toJson(records);
			}
		} catch (ServerException e) {
			throw e;
		} catch (Exception ex) {
			logger.error(ex);;
			throw new ServerException("id_err_999", "Unknown Error " + ex.getMessage());
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		return jsonStr;
	}

	public String deleteNotifyMessage(EntityManager postgres) throws ServerException, UnsupportedOperationException, IOException {
		String jsonStr = "";

		try {
			@SuppressWarnings("unchecked")
			List<MessageQueue> messageQueues = postgres.createNativeQuery("SELECT * FROM message_queue WHERE msg_message_id = '" + messageId + "'", MessageQueue.class).getResultList();

			if (messageQueues.size() > 0) {
				for (MessageQueue messageQueue : messageQueues) {
					if (messageQueue.getMsgMessageId().equals(messageId)) {
						postgres.getTransaction().begin();
						postgres.merge(messageQueue);
						postgres.remove(messageQueue);
						postgres.flush();
						postgres.getTransaction().commit();
					}
				}

				// Response
				NotifyMessageResultTrue records = new NotifyMessageResultTrue();
				records.setResult(true);
				jsonStr = toJson(records);
			} else {
				// Response
				NotifyMessageResultError records = new NotifyMessageResultError();
				records.setError_code("id_err_701");
				records.setError_message("Message not found");
				jsonStr = toJson(records);
			}
		} catch (Exception ex) {
			logger.error(ex);;
			throw new ServerException("id_err_999", "Unknown Error " + ex.getMessage());
		} finally {
			if (postgres.getTransaction().isActive()) {
				postgres.getTransaction().rollback();
			}
		}

		return jsonStr;
	}

	private void initiateSchedule(EntityManager postgres, MessageQueue sourceMessageQueue) {
		String sourceMessageId = sourceMessageQueue.getMsgMessageId();

		MessageQueue messageQueue = (MessageQueue) postgres.createNativeQuery("SELECT * FROM message_queue WHERE message_id = '" + sourceMessageId + "' AND os_type = 'source' AND msg_message_id = '" + sourceMessageId + "' AND status = 9 AND expand_status = 0", MessageQueue.class).getSingleResult();

		if (null != messageQueue) {
			if ((null == sendStartDate || sendTime.isEmpty()) && (null == sendExpireDate || sendExpireDate.isEmpty()) && (null == sendTime || sendTime.isEmpty())) {
				String messageId = "";
				String messageContent = "";
				String sendStartDate = "";
				String sendExpireDate = "";
				String sendTime = "";
				String fileUri = "";
				Date expireTime = null;
				try {
					MessageQueue mainMessageQueue = (MessageQueue) postgres.createNativeQuery("SELECT * FROM message_queue WHERE id = " + messageQueue.getId(), MessageQueue.class).getSingleResult();

					if (null != mainMessageQueue) {
						postgres.getTransaction().begin();

						mainMessageQueue.setStartDate(null);
						mainMessageQueue.setExpireDate(null);
						mainMessageQueue.setSendTime(null);
						mainMessageQueue.setExpireTime(null);
						postgres.persist(mainMessageQueue);
						postgres.getTransaction().commit();
					}
				} catch (Exception ex) {
					logger.error("Error: Failed to change main message");
				}

				// schedule expand
				String curLocalDateTime = getCurLocalDateTimeStr("UTC+8");
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime nowDateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
				String nowDateTimeStr = String.valueOf(nowDateTime.getYear()) + "/" + String.format("%02d", nowDateTime.getMonthValue()) + "/" + String.format("%02d", nowDateTime.getDayOfMonth()) + " " + String.format("%02d", nowDateTime.getHour()) + ":" + String.format("%02d", nowDateTime.getMinute()) + ":00";
				String nowDateStr = String.valueOf(nowDateTime.getYear()) + "/" + String.format("%02d", nowDateTime.getMonthValue()) + "/" + String.format("%02d", nowDateTime.getDayOfMonth());
				String nearestTimeStr = getNearestTime(nowDateTimeStr);

				messageId = messageQueue.getMsgMessageId();
				messageContent = messageQueue.getContent();
				sendStartDate = nowDateStr + " 00:00:00";
				sendExpireDate = nowDateStr + " 23:59:59";
				sendTime = nearestTimeStr;
				fileUri = messageQueue.getFileUri();
				expireTime = messageQueue.getExpireTime();

				try {
					// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
					changeExpandStatus(postgres, messageQueue, 1);

					// create realtime schedule
					createScheduleJob(postgres, messageId, messageContent, sendStartDate, sendExpireDate, sendTime,expireTime, fileUri);

					// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
					changeExpandStatus(postgres, messageQueue, 9);
				} catch (Exception ex) {
					// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
					changeExpandStatus(postgres, messageQueue, -1);
				}

				// create message group from schedule job
				String queryScheduleJob = "SELECT * FROM message_queue"
						+ " WHERE msg_message_id = '" + messageId + "'"
						+ " AND os_type = 'schedule'"
						+ " AND status = 9"
						+ " AND expand_status = 0"
						+ " ORDER BY udt::varchar::timestamp ASC";

				@SuppressWarnings("unchecked")
				List<MessageQueue> scheduleJobs = postgres.createNativeQuery(queryScheduleJob, MessageQueue.class).getResultList();

				if (scheduleJobs.size() > 0) {
					for (MessageQueue scheduleJob : scheduleJobs) {
						// schedule message
						scheduleExpandJob(postgres, scheduleJob);
					}
				}
			} else {
				String messageId = messageQueue.getMsgMessageId();
				String messageContent = messageQueue.getContent();
				String sendStartDate = messageQueue.getStartDate();
				String sendExpireDate = messageQueue.getExpireDate();
				String sendTime = messageQueue.getSendTime();
				String fileUri = messageQueue.getFileUri();
				Date expireTime = null;
				try {
					// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
					changeExpandStatus(postgres, messageQueue, 1);

					// create reservation schedule
					createScheduleJob(postgres, messageId, messageContent, sendStartDate, sendExpireDate, sendTime,expireTime, fileUri);

					// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
					changeExpandStatus(postgres, messageQueue, 9);
				} catch (Exception ex) {
					// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
					changeExpandStatus(postgres, messageQueue, -1);
				}
			}
		} else {
			logger.error("Error: Failed to initiate schedule");
		}
	}

	private void createScheduleJob(EntityManager postgres, String messageId, String messageContent, String sendStartDate, String sendExpireDate, String sendTime,Date expireTime, String fileUri) {
		String startDate = sendStartDate;
		String expireDate = sendExpireDate;

		List<String> dateList = new ArrayList<String>();

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Date sDate = format.parse(startDate);
			Date eDate = format.parse(expireDate);

			Calendar c = Calendar.getInstance();
			c.setTime(sDate);

			Calendar e = Calendar.getInstance();
			e.setTime(eDate);

			dateList.add(format.format(sDate));

			if (sDate.before(eDate)) {
				while (true) {
					c.add(Calendar.DATE, 1);

					dateList.add(format.format(c.getTime()));

					if (format.format(c.getTime()).equals(format.format(e.getTime()))) {
						break;
					}
				}
			}

			int groupNum = 1;

			for (String tmpDate : dateList) {
				MessageQueue mqObject = new MessageQueue();

				mqObject.setMessageId(messageId + "_schedule_" + String.format("%04d", groupNum));
				mqObject.setContent(messageContent);
				mqObject.setStartDate(tmpDate + " 00:00:00");
				mqObject.setExpireDate(tmpDate + " 23:59:59");
				mqObject.setSendTime(sendTime);
				mqObject.setExpireTime(expireTime);
				mqObject.setTargetId(targetId);
				mqObject.setFileUri(fileUri);
				mqObject.setStatus(9);
				mqObject.setExpandStatus(0);
				mqObject.setOsType("schedule");
				mqObject.setMsgMessageId(messageId);
				mqObject.setLastUpdated(CommonUtil.zonedTime());

				postgres.getTransaction().begin();
				postgres.persist(mqObject);
				postgres.getTransaction().commit();

				groupNum++;
			}
		} catch (ParseException pe) {
			logger.error(pe);
		}
	}

	private void scheduleExpandJob(EntityManager postgres, MessageQueue messageQueue) {
		String messageId = messageQueue.getMsgMessageId();
		String messageContent = messageQueue.getContent();
		String sendStartDate = messageQueue.getStartDate();
		String sendExpireDate = messageQueue.getExpireDate();
		String sendTime = messageQueue.getSendTime();
		Date expireTime = messageQueue.getExpireTime();
		String fileUri = messageQueue.getFileUri();
		String msgMessageId = messageQueue.getMessageId();
		int splitNum = 3000;

		// schedule expand
		if (!messageQueue.getTargetId().equals("*")) {
			String fileURL = messageQueue.getFileUri();
			InputStream is = null;

			try {
				HttpURLConnection urlConnection = (HttpURLConnection) new URL(fileURL).openConnection();
				is = urlConnection.getInputStream();

				BufferedReader in = new BufferedReader(new InputStreamReader(is));

				String lines;

				List<String> bookMemberIds = new ArrayList<String>();

				while ((lines = in.readLine()) != null) {
					bookMemberIds.add(lines);
				}

				if (bookMemberIds.size() > 0) {
					// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
					changeExpandStatus(postgres, messageQueue, 1);

					List<String> memberDevicesArrayList = new ArrayList<String>();

					for (String bookMemberId : bookMemberIds) {
						Member member = Member.getMemberByBMemberId(postgres, bookMemberId, Member.MEMBERTYPE_NORMALREADER);

						if (null != member) {
							memberDevicesArrayList.add(String.valueOf(member.getId()));
						}
					}

					if (memberDevicesArrayList.size() > 0) {
						// create push group for android,ios devices
						createPushGroup(postgres, messageId, messageContent, sendStartDate, sendExpireDate, sendTime,expireTime, fileUri, memberDevicesArrayList, msgMessageId, splitNum);

						// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
						changeExpandStatus(postgres, messageQueue, 9);
					} else {
						// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
						changeExpandStatus(postgres, messageQueue, -1);
					}
				}
			} catch (IOException e) {
				logger.info("Error: Failed to get properties data");
			} catch (Exception ex) {
				// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
				changeExpandStatus(postgres, messageQueue, -1);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						logger.info("Error: InputStream IO Exception");
					}
				}
			}
		} else {
			try {
				// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
				changeExpandStatus(postgres, messageQueue, 1);

				messageId = messageQueue.getMsgMessageId();
				messageContent = messageQueue.getContent();
				sendStartDate = messageQueue.getStartDate();
				sendExpireDate = messageQueue.getExpireDate();
				sendTime = messageQueue.getSendTime();
				fileUri = messageQueue.getFileUri();

				// create push group for android,ios devices
				createPushGroup(postgres, messageId, messageContent, sendStartDate, sendExpireDate, sendTime,expireTime, fileUri, null, msgMessageId, splitNum);

				// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
				changeExpandStatus(postgres, messageQueue, 9);
			} catch (Exception ex) {
				// Update Expand Status: 0:尚未展開,1:正在展開,9:展開完成,-1:展開失敗
				changeExpandStatus(postgres, messageQueue, -1);
			}
		}
	}

	private void createPushGroup(EntityManager postgres, String messageId, String messageContent, String sendStartDate, String sendExpireDate, String sendTime,Date expireTime, String fileUri, List<String> memberDevicesArrayList, String msgMessageId, int splitNum) {
		String deviceIds = "";
		String idsFilter = "";

		if (null != memberDevicesArrayList) {
			String[] memberDevicesArr = new String[memberDevicesArrayList.size()];
			deviceIds = arrayConverter(memberDevicesArrayList.toArray(memberDevicesArr));
			idsFilter = "id IN (" + deviceIds + ") AND ";
		} else {
			idsFilter = "";
		}

		String androidDevicesQueryString = "SELECT DISTINCT ON (device_token) * FROM member_device"
				+ " WHERE " + idsFilter + "os_type = 'android'"
				+ " AND device_token IS NOT NULL"
				+ " AND device_token <> 'null'"
				+ " ORDER BY device_token, udt::varchar::timestamp DESC";

		@SuppressWarnings("unchecked")
		List<MemberDevice> androidDevices = postgres.createNativeQuery(androidDevicesQueryString, MemberDevice.class).getResultList();

		if (androidDevices.size() > 0) {
			List<String> arrayList = new ArrayList<String>();

			for (MemberDevice tmpDevices : androidDevices) {
				arrayList.add(String.valueOf(tmpDevices.getId()));
			}

			String osType = "android";
			String targetIds = "";
			int groupNum = 1;

			for (int start = 0; start < arrayList.size(); start += splitNum) {
				int end = Math.min(start + splitNum, arrayList.size());
				targetIds += arrayList.subList(start, end) + ",";

				if (3000 == splitNum) {
					if (targetIds.endsWith(",")) {
						targetIds = targetIds.substring(0, targetIds.length() - 1);
					}

					MessageQueue mqObject = new MessageQueue();

					mqObject.setMessageId(messageId + "_job_" + osType + "_" + String.format("%04d", groupNum));
					mqObject.setContent(messageContent);
					mqObject.setStartDate(sendStartDate);
					mqObject.setExpireDate(sendExpireDate);
					mqObject.setSendTime(sendTime);
					mqObject.setExpireTime(expireTime);
					mqObject.setTargetId(targetIds);
					mqObject.setFileUri(fileUri);
					mqObject.setStatus(0); // 0: 尚未推播, 1: 正在推播, 9: 推播完成, -1: 發送失敗
					mqObject.setOsType(osType); // ios, android
					mqObject.setMsgMessageId(messageId);
					mqObject.setExpandStatus(0);
					mqObject.setLastUpdated(CommonUtil.zonedTime());

					postgres.getTransaction().begin();
					postgres.persist(mqObject);
					postgres.getTransaction().commit();

					targetIds = "";
				}

				groupNum++;
			}
		}

		String iosDevicesQueryString = "SELECT DISTINCT ON (device_token) * FROM member_device"
				+ " WHERE " + idsFilter + "os_type = 'ios'"
				+ " AND device_token IS NOT NULL"
				+ " AND device_token <> 'null'"
				+ " ORDER BY device_token, udt::varchar::timestamp DESC";

		@SuppressWarnings("unchecked")
		List<MemberDevice> iosDevices = postgres.createNativeQuery(iosDevicesQueryString, MemberDevice.class).getResultList();

		if (iosDevices.size() > 0) {
			List<String> arrayList = new ArrayList<String>();

			for (MemberDevice tmpDevices : iosDevices) {
				arrayList.add(String.valueOf(tmpDevices.getId()));
			}

			String osType = "ios";
			String targetIds = "";
			int groupNum = 1;

			for (int start = 0; start < arrayList.size(); start += splitNum) {
				int end = Math.min(start + splitNum, arrayList.size());
				targetIds += arrayList.subList(start, end) + ",";

				if (3000 == splitNum) {
					if (targetIds.endsWith(",")) {
						targetIds = targetIds.substring(0, targetIds.length() - 1);
					}

					MessageQueue mqObject = new MessageQueue();

					mqObject.setMessageId(messageId + "_job_" + osType + "_" + String.format("%04d", groupNum));
					mqObject.setContent(messageContent);
					mqObject.setStartDate(sendStartDate);
					mqObject.setExpireDate(sendExpireDate);
					mqObject.setSendTime(sendTime);
					mqObject.setTargetId(targetIds);
					mqObject.setFileUri(fileUri);
					mqObject.setStatus(0); // 0: 尚未推播, 1: 正在推播, 9: 推播完成, -1: 發送失敗
					mqObject.setOsType(osType); // ios, android
					mqObject.setMsgMessageId(messageId);
					mqObject.setExpandStatus(0);
					mqObject.setLastUpdated(CommonUtil.zonedTime());

					postgres.getTransaction().begin();
					postgres.persist(mqObject);
					postgres.getTransaction().commit();

					targetIds = "";
				}

				groupNum++;
			}
		}
	}

	public boolean isValidDate(String inDate) {
		if (inDate == null) {
			return false;
		}

		// Set the format to use as a constructor argument.
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		if (inDate.trim().length() != dateFormat.toPattern().length()) {
			return false;
		}

		dateFormat.setLenient(false);

		try {
			// Parse the inDate parameter.
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}

		return true;
	}

	public boolean isValidTime(String inTime) {
		if (inTime == null) {
			return false;
		}

		// Set the format to use as a constructor argument.
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

		if (inTime.trim().length() != dateFormat.toPattern().length()) {
			return false;
		}

		dateFormat.setLenient(false);

		try {
			// Parse the inTime parameter.
			dateFormat.parse(inTime.trim());
		} catch (ParseException pe) {
			return false;
		}

		return true;
	}

	public static String arrayConverter(String[] arrayStr) {
		StringBuilder sb = new StringBuilder();

		for (String st : arrayStr) {
			sb.append(st).append(',');
		}

		if (arrayStr.length != 0) {
			sb.deleteCharAt(sb.length() - 1);
		}

		return sb.toString();
	}

	public static String getNearestTime(String strDate) {
		Date dateToReturn = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

		try {
			dateToReturn = (Date) dateFormat.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(dateToReturn);
		int minute = cal.get(Calendar.MINUTE);
		minute = minute % 5;

		if (minute != 0) {
			int minuteToAdd = 5 - minute;
			cal.add(Calendar.MINUTE, minuteToAdd);
		}

		return timeFormat.format(cal.getTime());
	}

	public static String getCurLocalDateTimeStr(String timeZone) {
		if (timeZone == null || timeZone.length() < 1) {
			timeZone = "UTC+8";
		}

		ZoneId zoneId = ZoneId.of(timeZone);
		ZonedDateTime zdt = ZonedDateTime.now(zoneId);

		return zdt.toString();
	}

	private boolean changeExpandStatus(EntityManager postgres, MessageQueue tmpMessageQueues, Integer statusCode) {
		if (null != statusCode) {
			MessageQueue messageQueue = (MessageQueue) postgres.createNativeQuery("SELECT * FROM message_queue WHERE id = " + tmpMessageQueues.getId(), MessageQueue.class).getSingleResult();

			postgres.getTransaction().begin();
			messageQueue.setExpandStatus(statusCode);
			postgres.persist(messageQueue);
			postgres.getTransaction().commit();

			logger.info("changeStatus: " + statusCode);

			return true;
		} else {
			return false;
		}
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getSendStartDate() {
		return sendStartDate;
	}

	public void setSendStartDate(String sendStartDate) {
		this.sendStartDate = sendStartDate;
	}

	public String getSendExpireDate() {
		return sendExpireDate;
	}

	public void setSendExpireDate(String sendExpireDate) {
		this.sendExpireDate = sendExpireDate;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getFileUri() {
		return fileUri;
	}

	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}

	public Integer getSplitNum() {
		return splitNum;
	}

	public void setSplitNum(Integer splitNum) {
		this.splitNum = splitNum;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
}

class NotifyMessageResultTrue {
	Boolean result;
	String push_notification_id;

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getPush_notification_id() {
		return push_notification_id;
	}

	public void setPush_notification_id(String push_notification_id) {
		this.push_notification_id = push_notification_id;
	}
}

class NotifyMessageResultError {
	String error_code;
	String error_message;

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}

class PushDataForLambda {
	String message_id;

	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}
}

class PushDataForServlet {
	private String message_id;

	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}
}