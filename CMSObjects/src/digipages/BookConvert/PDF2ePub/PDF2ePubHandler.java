package digipages.BookConvert.PDF2ePub;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.persistence.EntityManagerFactory;

import org.apache.commons.io.IOUtils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.google.gson.Gson;

import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.PdfUtility;
import digipages.BookConvert.utility.RequestData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.Ec2ResourceException;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class PDF2ePubHandler implements CommonJobHandlerInterface {
    public static DPLogger logger = DPLogger.getLogger(PDF2ePubHandler.class.getName());
	private static Gson gson = new Gson();
    protected LogToS3 convertingLogger;
    protected LogToS3 checkResultLogger;
	private String logFolder;
	private Object originalFolder;
	private Object trialFolder;
	private String message="Done";
	private DataMessage dataMsg;
	private AmazonS3 s3Client;
	private RequestData requestData;
	private PDF2ePubCore pdf2ePubCore;
	private String backupPdf2EpubLoc;
	private Properties conf;

	@Override
	public String mainHandler(Properties conf,ScheduleJob job) throws ServerException,JobRetryException {
//		CommonUtil.delDirs("/tmp/");
		this.conf=conf;
		initEnv(conf,job);
		File targetePubDir=null;
		File pdfFile=null;
		
		//flatten and conver to epub, if fail report fail with message.
		try {
			
			File pdfDir=CommonUtil.downloadPDFFromS3(dataMsg);
			pdfFile = new File(pdfDir.getAbsoluteFile() + "/Source.pdf");
			checkPDFV13(pdfFile);
			
			logger.debug("start PDF2ePub, reading PDF " + pdfFile.getAbsolutePath());
			targetePubDir = genePub(pdfFile);
			//compressThumbnail(targetePubDir);
			
			// upload to S3 (epub Dir)
			CannedAccessControlList permission = requestData.getIsTrial() ? 
	                CannedAccessControlList.PublicRead : CannedAccessControlList.AuthenticatedRead;
			
			String tmpS3Loc = dataMsg.getTargetBucket()+ "/" + dataMsg.getFolder(); 
					
			CommonUtil.uploadToS3(s3Client, targetePubDir, tmpS3Loc, permission,targetePubDir.getPath());
			logger.debug("Uploading file to: " + tmpS3Loc);
			
			clearPDFFile(s3Client);
			
			// Generate Compressed ePub and UploadTo S3
			long totalSize=extraBackupEpub(targetePubDir, permission);
			dataMsg.setSize(String.valueOf(totalSize));
		} catch (JobRetryException | Ec2ResourceException e)
		{
			throw e;
		}
		catch (ServerException e)
		{
			logger.error("PDF Convert Exception", e);
			pdfFailHandler(job,e);
		} catch (Exception e) {
			logger.error("Unexcepted Exception", e);
			pdfFailHandler(job,e);
		}
		finally{
			// delete after upload
			if (null!=targetePubDir && targetePubDir.exists()){
//				CommonUtil.delDirs(targetePubDir);
			}
			if(dataMsg.getResult()) {
				scheduleThumbnail(dataMsg, requestData);
			}else {
				notifyAPI(dataMsg, requestData);
			}
			convertingLogger.uploadLog();
			checkResultLogger.uploadLog();
		}
		return "FlattenPDF finished.";
	}
	


	/**
	 * path = /THUMBNAIL/all_thumbnails.zip
	 * @param targetePubDir
	 */
	private void compressThumbnail(File targetePubDir) {

		String thumbnailDir = targetePubDir.getAbsolutePath() + "/THUMBNAIL/";
		String zipFile = "/tmp/all_thumbnails.zip";
		try {
			pack(thumbnailDir,zipFile);
			new File (zipFile).renameTo(new File(thumbnailDir + "all_thumbnails.zip"));
		} catch (IOException e) {
			logger.error("Error compressing thumbnail",e);
			convertingLogger.error("Error compress thumbnail zip.");
		}
		
	}




	private void checkPDFV13(File pdfFile) {
		if (PdfUtility.isPDFV13(pdfFile))
		{
			convertingLogger.warn("PDF Version 1.3 or below detected.");
			checkResultLogger.warn("PDF Version 1.3 or below detected.");
			logger.debug("Converting Logger uploaded PDF V1.3 warning.");
			return;
		}
		logger.debug("ConvertingLogger skipped, PDF Version is Ok (1.4-1.7).");
	}




	private void clearPDFFile(AmazonS3 s3Client) {
		String rootFolder = dataMsg.getFolder();
		String pdfFileName = dataMsg.getBookFileName();
		String targetBucket = dataMsg.getTargetBucket();
		logger.debug(" deleting pdf rootFolder: " + rootFolder + ", pdfFileName: " + pdfFileName + ", targetBucket: "
				+ targetBucket);
		String key = rootFolder + "/PDF/" + pdfFileName;
		
		s3Client.deleteObject(new DeleteObjectRequest(targetBucket, key));
		
		
		
	}




	private long extraBackupEpub(File targetePubDir, CannedAccessControlList permission) throws ServerException {
		long totalSize=0;
		try {
			
			CommonUtil.downloadDirFromS3(dataMsg.getTargetBucket(), dataMsg.getFolder(), 
					s3Client, targetePubDir.getAbsolutePath() + "/");
			totalSize = CommonUtil.calcFileSize(targetePubDir.getAbsolutePath() + "/");
			File epubFile = compressEPub(targetePubDir, requestData.getBook_file_id());
			//new EPubLoc
			String targetS3Loc = getTargetS3Loc();
			CommonUtil.uploadToS3(s3Client, epubFile, targetS3Loc, permission, epubFile.getPath());
//			epubFile.delete();
		} catch (Exception e)
		{
			logger.error("Fail to generate/upload ePub (from pdf)",e);
			throw new ServerException("id_err_999","Fail to generate/upload ePub (from pdf) please check config.");
		}
		return totalSize;
	}




	/**
	 * wendy request: by item id, directory "E05/015/xxxx.epub"
	 * @return
	 */
	private String getTargetS3Loc() {
		String itemid = requestData.getItem();
		String dirPadding = itemid.substring(0, 3) + "/" 
				+itemid.substring(3, 6) + "/" + itemid;
		return backupPdf2EpubLoc + dirPadding +"/" + requestData.getBook_file_id() + ".epub";
	}





	public static File compressEPub(File targetePubDir, String bookFileId) throws IOException {
		String targetEpubPath = "/tmp/" + bookFileId + ".epub";
		
		pack(targetePubDir.getPath(), targetEpubPath);
		
		return new File(targetEpubPath);
	}
	
	public static void pack(String sourceDirPath, String zipFilePath) throws IOException {
		Path p = Files.createFile(Paths.get(zipFilePath));
		 try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
				writeMimeType(zs);
		        Path pp = Paths.get(sourceDirPath);
		        Files.walk(pp)
		          .filter(path -> !Files.isDirectory(path))
		          .forEach(path -> {
		        	  if (path.endsWith("mimetype")){
		        		  // skip mimetype
		        	  }else{
			              ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString().replaceAll("\\\\", "/"));
			              try {
			                  zs.putNextEntry(zipEntry);
			                  try (FileInputStream fs = new FileInputStream(path.toFile())) 
			                  { IOUtils.copy(fs, zs); } 
			                  zs.closeEntry();
			            } catch (Exception e) {
			                logger.error(e);
			            }
		              }
		          });
		    }
	}

	private static void writeMimeType(ZipOutputStream zip) throws IOException {
	    byte[] content = "application/epub+zip".getBytes("UTF-8");
	    ZipEntry entry = new ZipEntry("mimetype");
	    entry.setMethod(ZipEntry.STORED);
	    entry.setSize(20);
	    entry.setCompressedSize(20);
	    entry.setCrc(0x2CAB616F); // pre-computed
	    zip.putNextEntry(entry);
	    zip.write(content);
	    zip.closeEntry();
	}



	private File genePub(File origFile) throws JobRetryException, IOException, InterruptedException, ServerException{
    	long startTime = System.currentTimeMillis();
    	
    	File targetFile=pdf2ePubCore.convertToePub(origFile,convertingLogger, checkResultLogger);
    		    
        // delete orig if convert success.
        if (targetFile!=origFile){
        	logger.debug("file convert success.");
//	        if (!origFile.delete())
//	        {
//	    		convertingLogger.warn("File delete fail. " + origFile.getAbsolutePath());
//	        }
        }
        
        this.message = "End PDF2dPubHandler spend " + (System.currentTimeMillis() - startTime) + "ms";
        logger.info(this.message);
        return targetFile;
	}



	// fail, need to report back to NotifyAPI
	private void pdfFailHandler(ScheduleJob job, Exception e) {
		String errMessage = e.getMessage();
		if (e instanceof ServerException)
		{
			errMessage = ((ServerException) e).getError_message();
		}
		logger.error("Fail to flattenPDF with Exception "
				 + errMessage);
		job.setErrMessage(errMessage);
		job.setStatus(-1);
		job.setFinishTime(new Date());
		
		dataMsg.setResult(false);
		
		dataMsg.appendErrorMessage(errMessage);
		
	}


	private void initEnv(Properties conf,ScheduleJob job) {
		String originalBucket = conf.getProperty("originalBucket");
        logFolder = conf.getProperty("logFolder");
        this.originalFolder = conf.getProperty("originalFolder");
        this.trialFolder = conf.getProperty("trialFolder");
        this.backupPdf2EpubLoc = conf.getProperty("backupPdf2EpubLoc");

		// prepare 
		this.dataMsg = job.getJobParam(DataMessage.class);
		this.requestData = gson.fromJson(dataMsg.getRequestData(), RequestData.class);

		this.s3Client = AmazonS3ClientBuilder.defaultClient();
		
        String convertedLogPath = getLogFolder(requestData) + "/" + "converted.log";
        this.convertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
        
        String checkResultLogPath = getLogFolder(requestData) + "/" + "CheckResult.log";
        this.checkResultLogger = new LogToS3(s3Client,originalBucket,checkResultLogPath);
        
        
        String fileName=dataMsg.getBookFileName();
		pdf2ePubCore = new PDF2ePubCore();
		pdf2ePubCore.setDPI(extractDPI(fileName,pdf2ePubCore));
		pdf2ePubCore.setFactor(extractResFactor(fileName));
		pdf2ePubCore.setColorSpace(extractColorSpace(fileName));
		pdf2ePubCore.setRenderByIM(extractRenderByIM(fileName));
		pdf2ePubCore.setRenderByPSfit(extractRenderByPSfit(fileName));
		pdf2ePubCore.setRenderByPDFfit(extractRenderByPDFfit(fileName));
		pdf2ePubCore.setRenderByPDFbox(extractRenderByPDFbox(fileName));
		if(fileName.contains("_")) {
			pdf2ePubCore.setQualitySpecific(extractQuality(fileName));
			pdf2ePubCore.setQualitySpecificPagesList(extractQualitySpecificPage(fileName));
			pdf2ePubCore.setQualitySpecificAll(extractQualityAll(fileName));
		}
		
	}

	private List<String> extractQualitySpecificPage(String fileName) {
		
		String parserString = fileName.substring(fileName.indexOf("_")).toLowerCase();
		List<String> pages = new ArrayList<String>();		
		try {
			
			Pattern pattern = Pattern.compile("(p\\d+)");
			
			Matcher matcher = pattern.matcher(parserString);
			
			while (matcher.find()) {
	            pages.add(matcher.group());
	        }
		
		} catch (Exception e) {
			convertingLogger.debug("error to extractQualityAll");
		}
		return pages;
	}
	
	private int extractQuality(String fileName) {
		
		int quality = 0;
		
		try {
		
			String parserString = fileName.substring(fileName.indexOf("_")).toLowerCase();
			
			Pattern pattern = Pattern.compile("q(\\d+)");
			
			Matcher matcher = pattern.matcher(parserString);
			
			while (matcher.find()) {
				quality = Integer.valueOf(matcher.group().replaceAll("\\D+",""));
				convertingLogger.info("quality set "+quality);
				break;
	        }
		
		} catch (Exception e) {
			convertingLogger.debug("error to extractQuality");
		}
		return quality;
	}
	
	private boolean extractQualityAll(String fileName) {
		
		boolean qualityAll = false;
		try {
			String parserString = fileName.substring(fileName.indexOf("_")).toLowerCase();
			
			if(parserString.contains("pall")) {
				qualityAll = true;
			}	
		} catch (Exception e) {
			convertingLogger.debug("error to extractQualityAll");
		}
		
		
		return qualityAll;
	}
	
	private String extractColorSpace(String fileName) {
		String colorSpace="sRGB"; //default sRGB
		logger.debug("Check ColorSpace:"+ fileName);
		if (fileName.contains("CMYK")){
			colorSpace="CMYK";
		}
		if (fileName.contains("sRGB"))
		{
			colorSpace="sRGB";
		}
		return colorSpace;
	}

	private Boolean extractRenderByIM(String fileName)
	{
		Boolean ret=false;
		if (fileName.toLowerCase().contains("renderbyim"))
		{
			ret=true;
		}
		return ret;
	}
	
	private Boolean extractRenderByPSfit(String fileName)
	{
		Boolean ret=false;
		if (fileName.toLowerCase().contains("psfit"))
		{
			ret=true;
		}
		return ret;
	}
	
	private Boolean extractRenderByPDFfit(String fileName)
	{
		Boolean ret=false;
		if (fileName.toLowerCase().contains("renderfit"))
		{
			ret=true;
		}
		return ret;
	}
	
	private Boolean extractRenderByPDFbox(String fileName)
	{
		Boolean ret=false;
		if (fileName.toLowerCase().contains("renderbox"))
		{
			ret=true;
		}
		return ret;
	}
	
	private Boolean extractRenderByDPI(String fileName)
	{
		Boolean ret=false;
		if (fileName.toLowerCase().contains("dpi"))
		{
			ret=true;
		}
		return ret;
	}
	
	
	public static float extractResFactor(String fileName) {
		float resFactor=2f; 
		String constResFactorStr="resFactor";
		if (!fileName.contains(constResFactorStr))
		{
			return resFactor;
		}
		String fNamePart1=fileName.split("\\.")[0];
		String []seperated=fNamePart1.split("_");
		for (String tmpItem:seperated)
		{
			if (tmpItem.startsWith(constResFactorStr))
			{
				try {
					resFactor = Float.parseFloat(tmpItem.substring(9));
					break;
				}catch (Exception e){
					// do nothing.
				}
			}
		}

		return resFactor;
	}

	/**
	 * 
	 * @param fileName
	 * @return dpi
	 */
	public static float extractDPI(String fileName,PDF2ePubCore pdf2ePubCore) {
		float retDpi=450f; 
		String constDPIStr="dpi";
		if (!fileName.contains(constDPIStr))
		{
			return retDpi;
		}
		String fNamePart1=fileName.split("\\.")[0];
		String []seperated=fNamePart1.split("_");
		for (String tmpItem:seperated)
		{
			if (tmpItem.startsWith(constDPIStr))
			{
				try {
					retDpi = Float.parseFloat(tmpItem.substring(3));
					pdf2ePubCore.setRenderByDPI(true);
					break;
				}catch (Exception e){
					// do nothing.
				}
			}
		}

		return retDpi;
	}

   
	/**
	 * handleThumbnail by ec2/lambda (new)
	 * @throws Exception
	 */
	private void scheduleThumbnail(DataMessage dataMessage, RequestData requestData2) throws ServerException {
		// Trigger Thumbnail function
		try {
			ScheduleJob job = new ScheduleJob();
			//
	        dataMessage.setCmsToken(dataMessage.getCmsToken());
	        dataMessage.setBookUniId(dataMessage.getBookUniId());

			job.setJobParam(dataMessage);
			job.setJobName("FixedlayoutThumbnail");
			job.setJobRunner(1);
			job.setShouldFinishTime(CommonUtil.nextNMinutes(60));
			job.setJobGroup(requestData2.getBook_file_id());
			ScheduleJobManager.setConfig(conf);
			ScheduleJobManager.addJob(job);
		} catch (Exception e) {
			logger.error("Unexcepted when notify API", e);
			throw new ServerException("id_err_999", "Unexcepted when scheduleThumbnail");
		}
	}   
	
	
	
	private static void notifyAPI(DataMessage dataMessage, RequestData requestData2) throws ServerException
	{
		try {			
			ScheduleJob sj = new ScheduleJob();
			sj.setJobParam(dataMessage);
			sj.setJobName(NotifyAPIHandler.class.getName());
//			sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
			sj.setJobGroup(requestData2.getBook_file_id());
			ScheduleJobManager.addJob(sj);
		} catch (Exception e) {
			logger.error("Unexcepted when notify API",e);
			throw new ServerException("id_err_999","Unexcepted when notify API");
		}
	}


	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// don't need DB connection
	}
	
	
    protected String getLogFolder(RequestData lambdaRequestData) {
        String folderPath;
        
        
        
        folderPath = logFolder + "/" + (lambdaRequestData.getIsTrial() ? trialFolder : originalFolder) +
                "/" + lambdaRequestData.getItem() + "/" + lambdaRequestData.getBook_file_id();
        
        return folderPath;
    }

	/**
	 * default is Ec2, no other possible.
	 * @param runMode
	 * @return
	 */
	public static RunMode getRetryRunMode(RunMode runMode) {
		if (runMode==RunMode.LambdaMode)
		{
			return RunMode.ExtEc2Mode;
		}
		return RunMode.ExtEc2Mode;
	}


	
	public static RunMode getInitRunMode(ScheduleJob job) {
		
		return RunMode.ExtEc2Mode;
		
	}
	
	
	/**
	 * for test purpose
	 * @param args
	 */
	public static void main(String args[]){
		try {
			
			File pdfFile = new File("/root/test.pdf");
			PDF2ePubHandler handler = new PDF2ePubHandler();
			handler.tmpInitEnv();
			File targetePubDir = handler.genePub(pdfFile);
			logger.info("convert done.");
		} catch (Exception e) {
			
			logger.error("fail",e);
		}
	}



	/**
	 * for test purpose, run locally.
	 */
	private void tmpInitEnv() {
		String originalBucket = "";
        logFolder = "";
        this.originalFolder = "";
        this.trialFolder = "";
        this.backupPdf2EpubLoc = "";

		// prepare 
		this.dataMsg = null;
		this.requestData = null;

		this.s3Client = null;
		
        String convertedLogPath = "";
        this.convertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);

        String fileName="";
		pdf2ePubCore = new PDF2ePubCore();
		pdf2ePubCore.setDPI(extractDPI(fileName,pdf2ePubCore));
		pdf2ePubCore.setFactor(extractResFactor(fileName));
		pdf2ePubCore.setColorSpace(extractColorSpace(fileName));
		
	}




}
