package digipages.BookConvert.PDF2ePub;

import java.io.File;
import java.io.IOException;

import digipages.common.DPLogger;

public class PDF2ePubPageRunner implements Runnable {
	private File procPDF;
	private PDF2ePubCore core;
	private String targetPath;
	private int quality=92;
	private Exception failException;
	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	private static DPLogger logger = DPLogger.getLogger(PDF2ePubPageRunner.class.getName());
	
	public PDF2ePubPageRunner(PDF2ePubCore core,File pdfFile,String targetPath, int quality) {
		this.procPDF = pdfFile;
		this.core=core;
		this.targetPath=targetPath;
		this.quality = quality;
	}

	@Override
	public void run() {
		try {
			mainProcess();
		}
		catch (Exception e)
		{
			logger.error("Unexcepted exception ",e);
			this.setFailException(e);
		}
	}

	public void mainProcess() {
		try{
			// remove unused 
//			core.extractPDF(procPDF," -dFILTERIMAGE -dFILTERVECTOR ",".txtOnly");
			// attention: background is used to determine dimension, don't skip it.
			core.extractPDF(procPDF," -dFILTERTEXT ",".background");
//			core.extractPDF(procPDF," -dFILTERIMAGE -dFILTERTEXT ",".vectorOnly");
			core.processSinglePage(targetPath,procPDF, quality);
		} catch (Throwable e)
		{
			logger.error("Fail to process File",e);
			core.setSuccess(false);
		}
	}

	public Exception getFailException() {
		return failException;
	}

	public void setFailException(Exception failException) {
		this.failException = failException;
	}

}
