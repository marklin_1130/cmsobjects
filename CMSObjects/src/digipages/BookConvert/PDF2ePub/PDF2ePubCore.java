package digipages.BookConvert.PDF2ePub;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImagingOpException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.imgscalr.Scalr;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import com.google.gson.Gson;

import digipages.BookConvert.FlattenPDF.PageDimension;
import digipages.BookConvert.utility.CmdExecutor;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.PdfUtility;
import digipages.CommonJobHandler.Runner.Ec2ResourceException;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;

public class PDF2ePubCore {
	private static DPLogger logger = DPLogger.getLogger(PDF2ePubCore.class.getName());
	private static int timeoutMinutes = 60;
	private boolean success = true;
	private static LogToS3 converterLogger;
	private float dpi=450;
	private float resFactor=2;
	private int quality = 90;
	private int qualitySpecific = 0;
	private boolean qualitySpecificAll = false;
	private List<String> qualitySpecificPagesList = new ArrayList<String>();
	private boolean usePS2PDF = false;
	private String colorSpace = "sRGB"; //default 
	private int targetPages;
	private long targetFilieSize;
	private boolean imgOnlyFlatten;
	private static Gson gson = new Gson();
	private static LogToS3 checkResultLogger;
	private int pageCnt=0;
	private boolean renderByIM=false;
	private boolean renderByDPI=false;
	private boolean renderByPSfit=false;
	private boolean renderByPDFfit=false;
	private boolean renderByPDFbox=false;
	


	public File convertToePub(File origFile, LogToS3 convertingLogger, LogToS3 checkResultLogger) throws IOException, InterruptedException, ServerException {
		PDF2ePubCore.converterLogger = convertingLogger;
		PDF2ePubCore.checkResultLogger = checkResultLogger;
		File pagedPDFDir = splitPDFPages(origFile);
		// update/determine quality related
		determineQualityParameter(origFile);
		File epubGenerated = generateEPub(pagedPDFDir);

		return epubGenerated;
	}
	
	// by sepearate pdf file, generate epub
	private File generateEPub(File srcPdfDir) throws InterruptedException, ServerException {
		logger.debug("generateEPub, srcPdfDir=" + srcPdfDir.getAbsolutePath());
		String targetPath = srcPdfDir.getAbsolutePath() + ".ePub";
		File targetDir = new File(targetPath);
		targetDir.mkdirs();
		
//		int threadLimit = Runtime.getRuntime().availableProcessors();
		int threadLimit = 3;
		logger.debug("start threads with limit " + threadLimit);
		
		ExecutorService executor = Executors.newFixedThreadPool(threadLimit);
		List <PDF2ePubPageRunner> runners=new ArrayList<>();
		
		converterLogger.info("Splitting pdf number:" + srcPdfDir.listFiles().length);
		converterLogger.info("Splitting pdf pageCnt:" + pageCnt);
		
		for (File procPDF : srcPdfDir.listFiles()) {
			converterLogger.info("Splitting pdf :" + procPDF.getAbsolutePath());
		}
		
		for (File procPDF : srcPdfDir.listFiles()) {
			if (!procPDF.getName().endsWith(".pdf")) {
				continue;
			}
			
			if(qualitySpecific > 0 && qualitySpecificAll) {
				PDF2ePubPageRunner tmpRunner = new PDF2ePubPageRunner(this, procPDF, targetPath, qualitySpecific);
//				tmpRunner.mainProcess();
				runners.add(tmpRunner);
				executor.execute(tmpRunner);
			}else {
				
				Integer pageNumber = Integer.valueOf(procPDF.getName().replaceAll("\\D+",""));
				boolean hasRunner = false;
				
				//判斷是否有指定頁數品質，如果有則抓出頁數給予特定參數執行
				if(qualitySpecificPagesList!=null && !qualitySpecificPagesList.isEmpty()) {
					for (String pageNumberString : qualitySpecificPagesList) {
						Integer targerPageNumber = Integer.valueOf(pageNumberString.replaceAll("\\D+",""));
						if(pageNumber == targerPageNumber) {
							PDF2ePubPageRunner tmpRunner = new PDF2ePubPageRunner(this, procPDF, targetPath, qualitySpecific);
//							tmpRunner.mainProcess();
							runners.add(tmpRunner);
							executor.execute(tmpRunner);
							hasRunner = true;
							break;
						}
					}
				}
				if(!hasRunner) {
					PDF2ePubPageRunner tmpRunner = new PDF2ePubPageRunner(this, procPDF, targetPath, quality);
//					tmpRunner.mainProcess();
					runners.add(tmpRunner);
					executor.execute(tmpRunner);
				}
			}
		}
		executor.shutdown();
		executor.awaitTermination(timeoutMinutes, TimeUnit.MINUTES);
		
		// check any Exception in child
		for (PDF2ePubPageRunner tmpRunner:runners)
		{
			Exception e = tmpRunner.getFailException();
			if (null==e){
				continue;
			}
			throw new Ec2ResourceException("id_err_",e.getMessage());
		}
		// check total file number matched !!
		checkPageNumberMatched(targetDir,pageCnt);
		
		return targetDir;
	}

	/**
	 * targetDir:
	 * html + jpg
	 * 
	 * @param targetDir
	 * @param pageCnt2
	 */
	private void checkPageNumberMatched(File targetDir, int pageCnt2)  throws ServerException{
		int htmlCnt =0;
		int jpgCnt=0;
		
		jpgCnt+=countChildFileType(targetDir,".jpg");
		htmlCnt+=countChildFileType(targetDir,".xhtml");

		// thumbnails.
		if (jpgCnt == htmlCnt*2){
			jpgCnt = jpgCnt/2;
		}
		
		if(htmlCnt ==0 && htmlCnt ==0) {
		    String msg = "Convert check Fail: 無法轉檔成功";
            logger.error(msg);
            throw new ServerException("id_err_305",msg);
		}
		
		if (htmlCnt!=pageCnt2 || jpgCnt!=pageCnt2){
			String msg = "Convert check Fail: orig=" + pageCnt2 + ", jgp pages=" + jpgCnt + " html pages=" + htmlCnt;
			logger.error(msg);
			throw new ServerException("id_err_305",msg);
		}
		logger.debug("check page count finished.");
	}

	/**
	 * 
	 * @param targetDir
	 * @param string
	 * @return
	 */
	private static int countChildFileType(File targetFile, String ext) {
		int ret = 0;
		if (targetFile.isFile() && targetFile.getName().endsWith(ext))
		{
			return 1;
		}
		if (!targetFile.isDirectory())
			return 0;
		for (File childFile:targetFile.listFiles())
		{
			ret += countChildFileType(childFile,ext);
		}
		return ret;
	}

	/**
	 * pdftk /tmp/123/xxxx.pdf brust output /tmp/123/xxx.paged/paged_%04d.pdf
	 * 
	 * @param origPDF
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private File splitPDFPages(File origPDF) throws IOException, InterruptedException {
		File targetPDFDir = new File(origPDF.getAbsolutePath() + ".paged");
		targetPDFDir.mkdirs();

		StringBuilder sb = new StringBuilder();
		sb.append("pdftk ").append(origPDF.getAbsolutePath()).append(" burst output ").append(targetPDFDir)
				.append("/page_%04d.pdf");
		converterLogger.info("Splitting pdf By command:" + sb.toString());
		CmdExecutor cmd = new CmdExecutor();
		cmd.run(sb.toString());
		String tmpResult = cmd.getStdOut();
		String tmpErr = cmd.getStdErr();
		converterLogger.info(" splitPDF Result :" + tmpResult);
		converterLogger.info("splitPDF Error :" + tmpErr);
		logger.info("split PDF result: " + origPDF.getName() + tmpResult);
		return targetPDFDir;
	}
	
	// according to page, change quality
	private void determineQualityParameter(File origFile) {
		if (null==origFile) {
			return;
		}
		try (PDDocument pdfDoc = PDDocument.load(origFile)){
			pageCnt=pdfDoc.getNumberOfPages();
			
			calcQualityByPage(pageCnt);
			
			pdfDoc.close();
		} catch (IOException e) {
			logger.error("Unexcepted Error loading file: " + origFile.getName(),e);
		}
	}

	
	/**
	 * gs -o cool-215-imgOnly.pdf -sDEVICE=pdfwrite -dFILTERTEXT cool-215.pdf
	 * 
	 * @param origFile
	 * @param imgOnlyPara
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public File extractPDF(File origFile, String para, String ext) throws IOException, InterruptedException {
		String targetPath = origFile.getAbsolutePath() + ext;
		String cmdStr = "gs -o " + targetPath + " -sDEVICE=pdfwrite " + para + " " + origFile.getAbsolutePath();
		logger.debug("extractPDF cmd:" + cmdStr);
		CmdExecutor cmdRunner = new CmdExecutor();
		cmdRunner.run(cmdStr);
		converterLogger.info(" extractPDF Result" + cmdRunner.getStdOut());
		converterLogger.info(" extractPDF error message: " + cmdRunner.getStdErr());
		logger.info("extractPDF result:" + cmdRunner.getStdOut());
		String stdOut = cmdRunner.getStdOut();
		// error handle retry
		if (stdOut.contains("Error reading a content stream.") && !stdOut.contains("were repaired")) {
			logger.error("Faile reading content stream:" + cmdRunner.getStdErr());
			new File(targetPath).delete();
		}

		return new File(targetPath);
	}
	/**
	 * 
	 * @param pageCnt
	 */
	private void calcQualityByPage(int pageCnt) {
		logger.debug("Calculate Quality by pages: " + pageCnt);
		if (pageCnt>350)
		{
			quality = 85;
			dpi=300;
			resFactor=1.5f;
			logger.debug("Using parameter: jpg quality=" + quality + " dpi=" + dpi + " resFactor=" +resFactor);
		}
		
	}

	public void setFactor(float resfactor) {
		this.resFactor=resfactor;
		
	}

	public void setDPI(float extractDPI) {
		this.dpi=extractDPI;
		
	}

	public void setSuccess(boolean b) {
		this.success=b;
		
	}

	public File processSinglePage(String targetPath, File srcPdf,
			int quality) throws Throwable {
		
		PageDimension dimension = genDimension(srcPdf.getPath());
		File jpgFile = null;
		try {
			jpgFile = pdfToJpg(srcPdf, dimension, quality);
			
			if(jpgFile==null || !jpgFile.exists()) {
				converterLogger.error("PDF處理失敗:" + srcPdf.getName());
			}
			
			converterLogger.info("jpgFile done:" + jpgFile.getName());
			
		} catch (Exception e) {
			converterLogger.warn("jpgFile fail:"+e.getMessage());
		}

		jpgFile = moveToImgDir(targetPath, jpgFile);
		
		// move to special handler
//		File thumbnail = pdfToThumbnail(srcPdf,false);
//		thumbnail = moveToThumbnailDir(targetPath, thumbnail);

		return jpgPdf2Html(targetPath, srcPdf, jpgFile);
	}
	
	/**
	 * fix at 210x280
	 * @param pdfFile
	 * @param skipAlpha
	 * @return
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	private File pdfToThumbnail(File pdfFile, boolean skipAlpha) throws IOException, InterruptedException {
		PDDocument pdf = PDDocument.load(pdfFile);
		String pdfSrcPath = pdfFile.getAbsolutePath();
		String targetJpgPath = pdfSrcPath.replaceAll("\\.pdf$", "") + ".jpg";

		StringBuilder sb = new StringBuilder();
		logger.debug("Using ColorSpace:" + colorSpace);
		
		// don't use sRGB or CMYK, let it choose by default.
		String colorSpec = " -colorspace " + colorSpace;
		if (null == colorSpace || colorSpace.isEmpty())
			colorSpec="";
		String alphaRemove=" -alpha remove ";
		if (skipAlpha)
			alphaRemove="";
		sb.append("convert -density ").append(dpi).append(" ")
				.append(" -colorspace sRGB ") // source ColorSpace
				.append(pdfSrcPath).append(" -background white ")
				.append(alphaRemove)
				.append(" -resize ").append("210x280")
				// .append(" -define pdf:use-cropbox=true ") // force corpbox (default is trimbox)
				// without corp, output pdf will be in a smaller region.
				.append(" -quality ").append(quality).append(" ")
				.append(" -colorspace sRGB ") // dest colorspace
				.append(" ").append(targetJpgPath);
		CmdExecutor cmd = new CmdExecutor();
		cmd.run(sb.toString());
		String tmpResult = cmd.getStdOut();
		String tmpErr = cmd.getStdErr();
		converterLogger.info(sb.toString() + " Error message:" + tmpErr);
		converterLogger.info(sb.toString() + " Result message:" + tmpResult);
		logger.info("Executing Pdf2Thumbnail: " + sb.toString());
		logger.info("PdfToJpg result=" + tmpResult + " Err:" + tmpErr);
		
		// special case skip alpha remove.
		if (tmpErr.contains("UnrecognizedAlphaChannelType")){
			return pdfToThumbnail(pdfFile,true);
		}
		
		return new File(targetJpgPath);
	}

	/**
	 * 
	 * @param targetPath
	 * @param thumbnail (page_0012.jpg)
	 * @return new targetPath (thumbnail0001.jpg)
	 * @throws IOException
	 */
	private File moveToThumbnailDir(String targetPath, File thumbnail) throws IOException {
		String targetName = thumbnail.getName().replaceAll("page_", "thumbnail");
		String targetThumbFilePath = targetPath + "/THUMBNAIL/" + targetName;
		String parentPath = targetPath + "/THUMBNAIL/";
		File targetDir = new File(parentPath);
		
		if (!targetDir.exists())
			targetDir.mkdirs();
		
		File targetFile = new File(targetThumbFilePath);
		
		if (!thumbnail.renameTo(targetFile))
		{
			throw new IOException("Fail to move file " + thumbnail.getPath() + " " + thumbnail.exists() + " to " +targetFile.getPath());
		}
		
		return targetFile;
	}

	public File jpgPdf2Html(String targetPath,File srcPdf, File jpgFile) throws InvalidPasswordException, IOException {
		logger.debug("jpgPdf2Html, targetPath:" + targetPath + " srcPDF:" + srcPdf.getPath() + " using jpg:" + jpgFile.getPath() );
		String text = "";
		try {
			text = PdfUtility.extractText(srcPdf);
		}catch (Exception e)
		{
			String warnMessage="Cannot extract Text from page:" + srcPdf.getName();
			logger.warn(warnMessage);
			checkResultLogger.warn(warnMessage);
		}
		String targetHtmlPath = targetPath + "/OEBPS/book/" + jpgFile.getName().replaceAll(".jpg", ".xhtml");
		File targetHtmlFile =generateTargetHtml(targetHtmlPath, text, jpgFile);
//		srcPdf.delete();
		return targetHtmlFile;
	}

	private File generateTargetHtml(String targetHtmlPath, String text, File jpgFile) throws IOException {
        logger.debug("generateTargetHtml start");
        String templateFileName = "FixedLayoutTemplate/OEBPS/book/FixedLayoutTemplate.xhtml";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(templateFileName);
        Document htmlDoc = Jsoup.parse(IOUtils.toString(inputStream, "utf-8"), "", Parser.xmlParser());
        inputStream.close();
        // title
        String title =genTitleByFname(jpgFile.getName());
        
        Elements elms=htmlDoc.head().getElementsByAttributeValue("name","viewport");
        elms.attr("content",genJpgWidthHeight(jpgFile));
        htmlDoc.getElementsByTag("title").empty();
        htmlDoc.getElementsByTag("title").append(title);
        // image
        htmlDoc.getElementById("bgImage").attr("src","../images/" + jpgFile.getName());
        // text
        htmlDoc.getElementById("pageText").appendText(text);
        htmlDoc.outputSettings().charset("UTF-8");
        htmlDoc.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
        htmlDoc.outputSettings().escapeMode(EscapeMode.xhtml);
        File targetHtml = new File(targetHtmlPath);
        File parentDir = targetHtml.getParentFile();
        if (!parentDir.exists())
        	parentDir.mkdirs();
        CommonUtil.WriteFile(targetHtml,htmlDoc.toString());
        
		return targetHtml;
	}

	private String genTitleByFname(String name) {
		
		return name.replace(".jpg", "");
	}

	/**
	 * sample width=1576, height=2048
	 * @param jpgFile
	 * @return
	 * @throws IOException 
	 */
	public static String genJpgWidthHeight(File jpgFile) throws IOException {
		BufferedImage bimg = ImageIO.read(jpgFile);
		int width          = bimg.getWidth();
		int height         = bimg.getHeight();
		String ret ="width="+width+", height=" + height;
		logger.debug("genJpgWidthHeibht: " + ret);
		return ret;
	}

	/*
	 * make directory if not exists.
	 */
	private static File moveToImgDir(String targetPath, File jpgFile) throws IOException {
		
		String targetJpgPath = targetPath + "/OEBPS/images/" + jpgFile.getName();
		String parentPath = targetPath + "/OEBPS/images/";
		File targetDir = new File(parentPath);
		
		if (!targetDir.exists())
			targetDir.mkdirs();
		
		File targetFile = new File(targetJpgPath);
		
		if (!jpgFile.renameTo(targetFile))
		{
			throw new IOException("Fail to move file " + jpgFile.getPath() + " " + jpgFile.exists() + " to " +targetFile.getPath());
		}
		
		return targetFile;
	}

	private PageDimension genDimension(String srcPath) {
		PageDimension dimension=null;
		try {
			// attention: background is used to determine dimension, don't skip it.
			File bkgroundFile = new File(srcPath + ".background");
			logger.debug("Deter dimension by Background file: " + bkgroundFile.getAbsolutePath());
			dimension = new PageDimension(bkgroundFile, resFactor);
			
			converterLogger.info("PageDimension info:"+dimension.getBigWidth()+ "|"+dimension.getBigHeight()+"|"+ dimension.getSmallWidth()+"|"+dimension.getSmallHeight());
			
		} catch (Throwable e) {
			logger.warn("Page Background dimension not exist, using orig pdf.");
		}
		return dimension;
	}
	
	private File pdfToJpg(File srcPdf, PageDimension dimension, int quality2) throws Throwable {
		return pdfToJpg(srcPdf, dimension, quality2,false , 1);
	}

	/*
	 * convert -density 300 -background white -alpha remove -resize
	 * 
	 */
	private File pdfToJpg(File pdfFile, PageDimension dimension,int quality, boolean skipAlpha , int retryCount) throws Throwable {

		retryCount= retryCount++;
		
		if(retryCount>10) {
			return null;
		}
		
		PDDocument pdf = PDDocument.load(pdfFile);
		String pdfSrcPath = pdfFile.getAbsolutePath();
		String targetJpgPath = pdfSrcPath.replaceAll("\\.pdf$", "") + ".jpg";
		PDPage page = pdf.getPage(0);
		
		// below 2 parameter deter the size/detail of pdf
		if (dimension == null) {
			logger.debug("using dimension from local file:");
			dimension = new PageDimension(page, resFactor);
		}
		// logger.debug(PageDimension.dumpDimension(dimension.getRectanble()));
		StringBuilder sb = new StringBuilder();
		logger.debug("Using ColorSpace:" + colorSpace);
		
		// don't use sRGB or CMYK, let it choose by default.
		String colorSpec = " -colorspace " + colorSpace;
		if (null == colorSpace || colorSpace.isEmpty())
			colorSpec="";
		String alphaRemove=" -alpha remove ";
		if (skipAlpha) {
			alphaRemove="";
		}
		
		if(renderByDPI) {
			//DPI , resize 
			logger.debug("pdfToJpg useing GS");
			sb.append("gs "
					+ "-r"+dpi+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
//					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPSFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
			
		}else if (renderByIM){
			logger.debug("pdfToJpg useing IM");
			sb.append("convert -density ").append(dpi).append(" ")
				.append(" -colorspace sRGB ") // source ColorSpace
				.append(pdfSrcPath).append(" -background white ")
				.append(alphaRemove)
				.append(" -resize ").append(dimension.getBigWidth()).append("x")
				.append(dimension.getBigHeight())
				// .append(" -define pdf:use-cropbox=true ") // force corpbox (default is trimbox)
				// without corp, output pdf will be in a smaller region.
				.append(" -crop ").append(dimension.getSmallWidth()).append("x").append(dimension.getSmallHeight())
//				.append("+").append("0").append("+").append("0").append(" +repage ")
				.append("+").append(dimension.getSmallX()).append("+").append(dimension.getSmallY())
				.append(" -quality ").append(quality).append(" ")
//				.append(" -colorspace sRGB ").append(" -trim ") // dest colorspace0
				.append(" ").append(targetJpgPath);
		}else if (renderByPSfit){
			logger.debug("pdfToJpg useing GS dPSFitPage scale");
			sb.append("gs "
					+ "-r"+"300"+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
//					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPSFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
		}else if (renderByPDFfit){
			logger.debug("pdfToJpg useing GS dPSFitPage scale");
			sb.append("gs "
//					+ "-r"+dpi+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPDFFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
		}else if (renderByPDFbox){
			logger.debug("pdfToJpg useing PDFbox");
			PDFRenderer pdfRenderer = new PDFRenderer(pdf);
			BufferedImage bim = pdfRenderer.renderImageWithDPI(0, 300);
			converterLogger.info("scale image info:"+targetJpgPath+ "|"+dimension.getSmallWidth()+"|"+ dimension.getSmallHeight());
			ImageIOUtil.writeImage(bim, targetJpgPath, 300);//指定圖檔位址及DPI
			
		}else{
			logger.debug("pdfToJpg useing PSFitPage");
			sb.append("gs "
					+ "-r"+"300"+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
//					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPSFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
		}
		
		pdf.close();
		
		String tmpErr = "";
		if (!renderByPDFbox) {
			CmdExecutor cmd = new CmdExecutor();
			cmd.run(sb.toString());
			String tmpResult = cmd.getStdOut();
			tmpErr = cmd.getStdErr();
			converterLogger.info(sb.toString() + " Error message:" + tmpErr);
			converterLogger.info(sb.toString() + " Result message:" + tmpResult);
			logger.info("Executing pdfToJpg: " + sb.toString());
			logger.info("PdfToJpg result=" + tmpResult + " Err:" + tmpErr);
			logger.debug(dimension.toString());
		}
		
		File targetJpgFile = new File(targetJpgPath);
		
		boolean isScaleFail = false;
		
		try {
			if(!renderByPDFfit&&!renderByIM) {
				if(!targetJpgFile.exists()) {
					converterLogger.warn("targetJpgFile not exists "+targetJpgPath );
				}
				File imgFile = new File(targetJpgPath);
				converterLogger.info("scale image start:"+imgFile.getAbsolutePath());
				BufferedImageOp bufferedImageOpArray = null;
				BufferedImage scaledImage = Scalr.resize(ImageIO.read(imgFile), Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, dimension.getSmallWidth(), dimension.getSmallHeight(), bufferedImageOpArray);
				converterLogger.info("scale image info:"+targetJpgPath+ "|"+dimension.getSmallWidth()+"|"+ dimension.getSmallHeight());
				ImageIO.write(scaledImage, "jpg", imgFile);
				converterLogger.info("scale image end:"+targetJpgPath);
			}
		}
		catch (IllegalArgumentException ie) {
			converterLogger.warn("Scalr IllegalArgumentException error: "+ ie.getMessage());
			converterLogger.warn("Scalr IllegalArgumentException error: "+ targetJpgPath );
			isScaleFail = true;
		}
		catch (ImagingOpException ioe) {
			converterLogger.warn("Scalr ImagingOpException error: "+ ioe.getMessage());
			converterLogger.warn("Scalr ImagingOpException error: "+ targetJpgPath );
			isScaleFail = true;
		}
		catch (Throwable e) {
			converterLogger.warn("Scalr Throwable error: "+ e.getMessage());
			converterLogger.warn("Scalr Throwable error: "+ targetJpgPath );
			isScaleFail = true;
		}
		
		// special case skip alpha remove.
		if (isScaleFail || (tmpErr.contains("UnrecognizedAlphaChannelType") && !targetJpgFile.exists())){
			renderByPDFfit = true;
			renderByPSfit = false;
			renderByIM = false;
			renderByDPI = false;
			renderByPDFbox = false;
			return pdfToJpg(pdfFile, dimension, quality, true ,retryCount);
		}
		
		return new File(targetJpgPath);
	}
	
	private File pdfToJpgByDpi(File pdfFile, PageDimension dimension,int quality, boolean skipAlpha , int lowdpi ) throws IOException, InterruptedException {

		PDDocument pdf = PDDocument.load(pdfFile);
		String pdfSrcPath = pdfFile.getAbsolutePath();
		String targetJpgPath = pdfSrcPath.replaceAll("\\.pdf$", "") + ".jpg";
		PDPage page = pdf.getPage(0);
		pdf.close();
		// below 2 parameter deter the size/detail of pdf
		if (dimension == null) {
			logger.debug("using dimension from local file:");
			dimension = new PageDimension(page, resFactor);
		}
		// logger.debug(PageDimension.dumpDimension(dimension.getRectanble()));
		StringBuilder sb = new StringBuilder();
		logger.debug("Using ColorSpace:" + colorSpace);
		
		// don't use sRGB or CMYK, let it choose by default.
		String colorSpec = " -colorspace " + colorSpace;
		if (null == colorSpace || colorSpace.isEmpty())
			colorSpec="";
		String alphaRemove=" -alpha remove ";
		if (skipAlpha) {
			alphaRemove="";
		}
		if(renderByDPI) {
			//DPI , resize 
			logger.debug("pdfToJpg useing GS");
			sb.append("gs "
					+ "-r"+lowdpi+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
//					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPSFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
			
		}else if (renderByIM){
			logger.debug("pdfToJpg useing IM");
			sb.append("convert -density ").append(dpi).append(" ")
				.append(" -colorspace sRGB ") // source ColorSpace
				.append(pdfSrcPath).append(" -background white ")
				.append(alphaRemove)
				.append(" -resize ").append(dimension.getBigWidth()).append("x")
				.append(dimension.getBigHeight())
				// .append(" -define pdf:use-cropbox=true ") // force corpbox (default is trimbox)
				// without corp, output pdf will be in a smaller region.
				.append(" -crop ").append(dimension.getSmallWidth()).append("x").append(dimension.getSmallHeight())
//				.append("+").append("0").append("+").append("0").append(" +repage ")
				.append("+").append(dimension.getSmallX()).append("+").append(dimension.getSmallY())
				.append(" -quality ").append(quality).append(" ")
//				.append(" -colorspace sRGB ").append(" -trim ") // dest colorspace0
				.append(" ").append(targetJpgPath);
		}else if (renderByPSfit){
			logger.debug("pdfToJpg useing GS dPSFitPage scale");
			sb.append("gs "
					+ "-r"+"300"+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
//					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPSFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
		}else if (renderByPDFfit){
			logger.debug("pdfToJpg useing GS dPSFitPage scale");
			sb.append("gs "
//					+ "-r"+dpi+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPDFFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
		}else{
			logger.debug("pdfToJpg useing PSFitPage");
			sb.append("gs "
					+ "-r"+"300"+" " // dpi is not needed, replace by -g resolution
					+ " -sDEVICE=jpeg "
//					+ " -g" + dimension.getBigWidth() + "x" + dimension.getBigHeight()
					+ " -dNOPAUSE -dBATCH -q " // for batch and quite output
					+ " -dUseCropBox " // cropBox prevent white border
					+ " -dPSFitPage " // scale to fit the page
					+ " -dTextAlphaBits=4 -dGraphicsAlphaBits=4 " // antialias (make font smooth)
					+ " -dJPEGQ="+quality
					+ " -sOutputFile="+targetJpgPath 
					+ " " +pdfSrcPath);
		}
		CmdExecutor cmd = new CmdExecutor();
		cmd.run(sb.toString());
		String tmpResult = cmd.getStdOut();
		String tmpErr = cmd.getStdErr();
		converterLogger.info(sb.toString() + " Error message:" + tmpErr);
		converterLogger.info(sb.toString() + " Result message:" + tmpResult);
		logger.info("Executing pdfToJpg: " + sb.toString());
		logger.info("PdfToJpg result=" + tmpResult + " Err:" + tmpErr);
		logger.debug(dimension.toString());
		
		if(!renderByPDFfit) {
			File targetJpgFile = new File(targetJpgPath);
			converterLogger.info("scale image start");
			File imgFile = new File(targetJpgPath);
			BufferedImageOp bufferedImageOpArray = null;
			BufferedImage scaledImage = Scalr.resize(ImageIO.read(imgFile), Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, dimension.getBigWidth(), dimension.getBigHeight(), bufferedImageOpArray);
			converterLogger.info("scale image :"+targetJpgPath+ "|"+dimension.getBigWidth()+"|"+ dimension.getBigHeight());
			ImageIO.write(scaledImage, "jpg", imgFile);
			converterLogger.info("scale image end");
		}
		return new File(targetJpgPath);
	}

	public String getColorSpace() {
		return colorSpace;
	}

	public void setColorSpace(String colorSpace) {
		logger.debug("setting colorSpace:" + colorSpace);
		this.colorSpace = colorSpace;
	}
	
	/**
	 * for test only.
	 * @param args
	 */
	public static void main(String [] args)
	{
		try {
			
	        
	        Document htmlDoc = Jsoup.parse("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
	        		"<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\">\n" + 
	        		"    <head>\n" + 
	        		"        <title>iBooks Demo</title>\n" + 
	        		"        <meta name=\"viewport\" content=\"width=1576, height=2048\" /> \n" + 
	        		"    </head>\n" + 
	        		"    <body>\n" + 
	        		"        <div class=\"backgroundImage\">\n" + 
	        		"            <img id=\"bgImage\" src=\"../images/page01.jpg\"/> \n" + 
	        		"        </div>\n" + 
	        		"        <span id=\"pageText\" style=\"display:none\">\n" + 
	        		"        </span>\n" + 
	        		"    </body> \n" + 
	        		"</html>");

	        Elements elms=htmlDoc.head().getElementsByAttributeValue("name","viewport");
	        logger.debug("Getting viewPort Line:" + elms);
	        elms.attr("content","1234.566");
	        logger.debug("viewPort changed:" + elms);
	        logger.debug("result html:" + htmlDoc.toString());
		}catch (Exception e)
		{
			logger.error("Error",e);
		}
	}

	public boolean isRenderByIM() {
		return renderByIM;
	}

	public void setRenderByIM(boolean renderByIM) {
		this.renderByIM = renderByIM;
	}

	public int getQualitySpecific() {
		return qualitySpecific;
	}

	public void setQualitySpecific(int qualitySpecific) {
		this.qualitySpecific = qualitySpecific;
	}

	public List<String> getQualitySpecificPagesList() {
		return qualitySpecificPagesList;
	}

	public void setQualitySpecificPagesList(List<String> qualitySpecificPagesList) {
		this.qualitySpecificPagesList = qualitySpecificPagesList;
	}

	public boolean isQualitySpecificAll() {
		return qualitySpecificAll;
	}

	public void setQualitySpecificAll(boolean qualitySpecificAll) {
		this.qualitySpecificAll = qualitySpecificAll;
	}
	
	private static InputStream resizeImage(InputStream uploadedInputStream, String fileName, int width, int height) {

        try {
            BufferedImage image = ImageIO.read(uploadedInputStream);
            Image originalImage= image.getScaledInstance(width, height, Image.SCALE_DEFAULT);

            int type = ((image.getType() == 0) ? BufferedImage.TYPE_INT_ARGB : image.getType());
            BufferedImage resizedImage = new BufferedImage(width, height, type);

            Graphics2D g2d = resizedImage.createGraphics();
            g2d.drawImage(originalImage, 0, 0, width, height, null);
            g2d.dispose();
            g2d.setComposite(AlphaComposite.Src);
            g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            ImageIO.write(resizedImage, fileName.split("\\.")[1], byteArrayOutputStream);
            return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            // Something is going wrong while resizing image
            return uploadedInputStream;
        }
    }

	public boolean isRenderByDPI() {
		return renderByDPI;
	}

	public void setRenderByDPI(boolean renderByDPI) {
		this.renderByDPI = renderByDPI;
	}

	public boolean isRenderByPSfit() {
		return renderByPSfit;
	}

	public void setRenderByPSfit(boolean renderByPSfit) {
		this.renderByPSfit = renderByPSfit;
	}

	public boolean isRenderByPDFfit() {
		return renderByPDFfit;
	}

	public void setRenderByPDFfit(boolean renderByPDFfit) {
		this.renderByPDFfit = renderByPDFfit;
	}

	public boolean isRenderByPDFbox() {
		return renderByPDFbox;
	}

	public void setRenderByPDFbox(boolean renderByPDFbox) {
		this.renderByPDFbox = renderByPDFbox;
	}

}

