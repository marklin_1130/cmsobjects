package digipages.BookConvert.lossless;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManagerFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;

import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.PDF2ePub.PDF2ePubHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class LosslessCompressionHandler implements CommonJobHandlerInterface {

	public static DPLogger logger = DPLogger.getLogger(LosslessCompressionHandler.class.getName());
	private static Gson gson = new Gson();
	protected LogToS3 convertingLogger;
	protected LogToS3 checkResultLogger;
	private String logFolder;
	private Object originalFolder;
	private Object trialFolder;
	private DataMessage dataMsg;
	private AmazonS3 s3Client;
	private RequestData requestData;
	private String backupPdf2EpubLoc;
	private Properties conf;
	private static DocumentBuilderFactory dbFactory;
	private static DocumentBuilder dBuilder;
	private static String Tag = "[Lossless]";
	private DataMessage dataMessage;
	String originalBucket;

	
	
	static {
		try {
			dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setNamespaceAware(true);
			dbFactory.setValidating(false);
			dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (Exception e) {
			System.out.println("Document parser init error");
		}
	}

	public static void main(String[] args) {

		try {

			if (args.length == 0 || StringUtils.isBlank(args[0])) {
				System.out.println("need s3 buckt parameter ex: s3private-ebook.books.com.tw");
				System.exit(0);
			}

			if (StringUtils.isBlank(args[1])) {
				System.out.println("need folder parameter ex: converted/513B4C/9611619");
				System.exit(0);
			}

			String targetBucket = args[0];
			String folder = args[1];

			List<File> pngFiles = new ArrayList<File>();
			List<File> jpgFiles = new ArrayList<File>();

			AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();

			String downloadEpubFolderPath = "/tmp/Epub_" + UUID.randomUUID().toString() + File.separator;
			File epubFolder = CommonUtil.downloadDirFromS3(targetBucket, folder, s3Client, downloadEpubFolderPath);
			Path filePath = Paths.get(downloadEpubFolderPath);
			Collection<File> allFiles = listFileTree(epubFolder);

			long orgEpubSize = Files.walk(filePath).mapToLong(p -> p.toFile().length()).sum();
			orgEpubSize = orgEpubSize / 1024;

			for (File file : allFiles) {
				if (file.getName().toLowerCase().contains(".png")) {
					pngFiles.add(file);
				}
				if (file.getName().toLowerCase().contains(".jpg")) {
					jpgFiles.add(file);
				}
			}

			pngLossless(pngFiles);

			jpgLossless(jpgFiles);

			long compressionEpubSize = Files.walk(filePath).mapToLong(p -> p.toFile().length()).sum();
			compressionEpubSize = compressionEpubSize / 1024;

			System.out.println("orgEpubSize:" + orgEpubSize + " kb");
			System.out.println("compressionEpubSize:" + compressionEpubSize + " kb");
			System.out.println("compression size result:" + (orgEpubSize - compressionEpubSize) + " kb");

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}

		System.exit(0);
	}

	@Override
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {

		initEnv(config, job);
		File targetePubDir = null;
		File targetEpub = null;
		List<File> pngFiles = new ArrayList<File>();
		List<File> jpgFiles = new ArrayList<File>();
		Integer reducePercentage = 0;
		try {

			logger.info(Tag + "message: " + job.toGsonStr());
			dataMessage = job.getJobParam(DataMessage.class);
			requestData = gson.fromJson(dataMessage.getRequestData(), RequestData.class);
			String message = gson.toJson(dataMessage);

			// 電子書檔案位置 傳入Folder ex: converted/513B4C/9611619

			String downloadEpubFolderPath = "/tmp/Epub_" + requestData.getBook_file_id() + File.separator;
			File epubFolder = CommonUtil.downloadDirFromS3(dataMsg.getTargetBucket(), dataMsg.getFolder(), s3Client, downloadEpubFolderPath);
			Path filePath = Paths.get(downloadEpubFolderPath);
			Long orgEpubSize = Files.walk(filePath).mapToLong(p -> p.toFile().length()).sum();
			orgEpubSize = orgEpubSize / 1024;

			convertingLogger.log(GenericLogLevel.Info, "downloaded book:" + dataMsg.getBookUniId() + "folder:" + dataMsg.getFolder(),
					LosslessCompressionHandler.class.getSimpleName());

			Collection<File> allFiles = listFileTree(epubFolder);

			for (File file : allFiles) {
				if (file.getName().toLowerCase().contains(".png")) {
					convertingLogger.log(GenericLogLevel.Info, "pngLossless :" + file.getName(), LosslessCompressionHandler.class.getSimpleName());
					pngFiles.add(file);
				}
				if (file.getName().toLowerCase().contains(".jpg")) {
					convertingLogger.log(GenericLogLevel.Info, "jpgLossless :" + file.getName(), LosslessCompressionHandler.class.getSimpleName());
					jpgFiles.add(file);
				}
			}

			pngLossless(pngFiles);

			jpgLossless(jpgFiles);

			long compressionEpubSize = Files.walk(filePath).mapToLong(p -> p.toFile().length()).sum();

			compressionEpubSize = compressionEpubSize / 1024;

			reducePercentage = ((int) ((orgEpubSize - compressionEpubSize) * (100.0f / orgEpubSize.floatValue())));

			convertingLogger.log(GenericLogLevel.Info, "png :" + pngFiles.size(), LosslessCompressionHandler.class.getSimpleName());
			convertingLogger.log(GenericLogLevel.Info, "jpg :" + jpgFiles.size(), LosslessCompressionHandler.class.getSimpleName());
			convertingLogger.log(GenericLogLevel.Info, "org size :" + orgEpubSize + ",compress size:" + compressionEpubSize + ",reduce size:" + (orgEpubSize - compressionEpubSize) + " kb",LosslessCompressionHandler.class.getSimpleName());
			convertingLogger.log(GenericLogLevel.Info, "reduce :" + reducePercentage + "%", LosslessCompressionHandler.class.getSimpleName());

			System.out.println("orgEpubSize:" + orgEpubSize + " kb");
			System.out.println("compressionEpubSize:" + compressionEpubSize + " kb");
			System.out.println("compression size result:" + (orgEpubSize - compressionEpubSize) + " kb");

			// upload to S3 (epub Dir)
			CannedAccessControlList permission = requestData.getIsTrial() ? CannedAccessControlList.PublicRead : CannedAccessControlList.AuthenticatedRead;

			String tmpS3Loc = dataMsg.getTargetBucket() + "/" + dataMsg.getFolder();

			CommonUtil.uploadToS3(s3Client, epubFolder, tmpS3Loc, permission, epubFolder.getPath());
			System.out.println("Uploading file to: " + tmpS3Loc);
			
			notifyAPI(dataMessage);

		} catch (Exception e) {
			convertingLogger.log(GenericLogLevel.Info, "lossless error :" +e.toString(), LosslessCompressionHandler.class.getSimpleName());
			e.printStackTrace();
		} finally {
			convertingLogger.uploadLog();
		}

		return reducePercentage.toString();
	}

	private static void pngLossless(List<File> pngFiles) {

		String pngLosslessCommand = "optipng -strip all -k -o7 -silent -clobber ";

		for (File file : pngFiles) {
			Path source = Paths.get(file.getAbsolutePath());
			Path backup = Paths.get(file.getAbsolutePath() + ".bak");
			try {

				try {
					Files.copy(source, backup, StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e1) {
					e1.printStackTrace();
					continue;
				}

				System.out.println("execute :" + pngLosslessCommand + file.getAbsolutePath());

				execToString(pngLosslessCommand + file.getAbsolutePath());

				FileUtils.forceDelete(backup.toFile());

			} catch (Exception e) {
				e.printStackTrace();
				try {
					Files.copy(backup, source, StandardCopyOption.REPLACE_EXISTING);
					FileUtils.forceDelete(backup.toFile());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

	}

	private static void jpgLossless(List<File> jpgFiles) {

		String jpgLosslessCommand = "jpegtran -copy none -optimize -progressive -outfile ";

		for (File file : jpgFiles) {
			Path source = Paths.get(file.getAbsolutePath());
			Path backup = Paths.get(file.getAbsolutePath() + ".bak");
			try {

				try {
					Files.copy(source, backup, StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e1) {
					e1.printStackTrace();
					continue;
				}

				System.out.println("execute :" + jpgLosslessCommand + file.getAbsolutePath() + " " + file.getAbsolutePath());

				execToString(jpgLosslessCommand + file.getAbsolutePath() + " " + file.getAbsolutePath());

				FileUtils.forceDelete(backup.toFile());
			} catch (Exception e) {
				e.printStackTrace();
				try {
					Files.copy(backup, source, StandardCopyOption.REPLACE_EXISTING);
					FileUtils.forceDelete(backup.toFile());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	protected static Long calculateTotalSize(AmazonS3 s3Client, DataMessage dataMessage) {
		Long totalSize= 0L;
		Integer totalItems = 0;
		try {
		ObjectListing objects = s3Client.listObjects(dataMessage.getTargetBucket(), dataMessage.getFolder() + "/");
		// skip thumbnail
		do {
			for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
				if (objectSummary.getKey().toLowerCase().contains("/thumbnail/")){
					continue;
				}
				totalSize += objectSummary.getSize();
				totalItems++;
			}
			objects = s3Client.listNextBatchOfObjects(objects);
		} while (!objects.getObjectSummaries().isEmpty());
		logger.info(Tag + "In " + dataMessage.getFolder() + "/ totalSize:" + totalSize + ", totalItems:" + totalItems);
	    
        } catch (Exception e) {
        }
		return totalSize;
	}

	private void notifyAPI(DataMessage dataMessage) throws Exception {
		String tag = "[NotifyAPI]";
		Long startTime = System.currentTimeMillis();

		convertingLogger.log(GenericLogLevel.Info, "Trigger NotifyAPI", LosslessCompressionHandler.class.getSimpleName());

		dataMessage.setLosslessNotify(true);

		ScheduleJob sj = new ScheduleJob();
		sj.setJobParam(dataMessage);
		sj.setJobGroup(requestData.getBook_file_id()+"loss");
		sj.setJobName(NotifyAPIHandler.class.getName());
		sj.setJobRunner(1);
		// sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
		ScheduleJobManager.addJob(sj);

	}

	public static String execToString(String command) throws Exception {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			CommandLine commandline = CommandLine.parse(command);
			DefaultExecutor exec = new DefaultExecutor();
			PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
			exec.setStreamHandler(streamHandler);
			exec.execute(commandline);
		} catch (Exception e) {
			throw e;
		}
		return (outputStream.toString());
	}

	private void initEnv(Properties conf, ScheduleJob job) {
		String originalBucket = conf.getProperty("originalBucket");
		logFolder = conf.getProperty("logFolder");
		this.originalFolder = conf.getProperty("originalFolder");
		this.trialFolder = conf.getProperty("trialFolder");
		this.backupPdf2EpubLoc = conf.getProperty("backupPdf2EpubLoc");

		// prepare
		this.dataMsg = job.getJobParam(DataMessage.class);
		this.requestData = gson.fromJson(dataMsg.getRequestData(), RequestData.class);

		this.s3Client = AmazonS3ClientBuilder.defaultClient();

		String convertedLogPath = getLogFolder(requestData) + "/" + "converted.log";
		this.convertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);

		String checkResultLogPath = getLogFolder(requestData) + "/" + "CheckResult.log";
		this.checkResultLogger = new LogToS3(s3Client, originalBucket, checkResultLogPath);

	}

	protected String getLogFolder(RequestData lambdaRequestData) {
		String folderPath;

		folderPath = logFolder + "/" + (lambdaRequestData.getIsTrial() ? trialFolder : originalFolder) + "/" + lambdaRequestData.getItem() + "/"
				+ lambdaRequestData.getBook_file_id();

		return folderPath;
	}

	public static Collection<File> listFileTree(File dir) {
		Set<File> fileTree = new HashSet<File>();
		if (dir == null || dir.listFiles() == null) {
			return fileTree;
		}
		for (File entry : dir.listFiles()) {
			if (entry.isFile())
				fileTree.add(entry);
			else
				fileTree.addAll(listFileTree(entry));
		}
		return fileTree;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// don't need DB connection
	}

	/**
	 * default retry mode = Ec2.
	 * @param runMode
	 * @return
	 */
	public static RunMode getRetryRunMode(RunMode runMode) {
		return RunMode.ExtEc2Mode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.ExtEc2Mode;
	}

	
}
