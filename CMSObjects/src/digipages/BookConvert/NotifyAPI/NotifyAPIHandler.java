package digipages.BookConvert.NotifyAPI;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.lossless.LosslessCompressionHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.GenericLogRecord;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.ResponseData;
import digipages.BookConvert.utility.ResponseData.ResultData;
import digipages.BookConvert.utility.Utility;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

/** 
 * lambda version
 * @author Eric.CL.Chou
 *
 */
public class NotifyAPIHandler implements CommonJobHandlerInterface{
	private static final String MANIFEST = "manifest";
    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;
    {
	    dbFactory = DocumentBuilderFactory.newInstance();
	    dbFactory.setNamespaceAware(true);
	    try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.error("Fail to build dbuilder",e);
		}
    }
    String originalFolder;
	String trialFolder;
	String originalBucket;
	String URL_BookProcessResult;
	String S3Bucket;
	String S3Key;
	CannedAccessControlList s3Permission;
	private ResultData resultData;
	private Long totalSize;
	private static String Tag = "[NotifyAPI]";
	private final String error_code_converting_fail = "id_err_305";
	private DataMessage dataMessage; 
	private RequestData requestData;
	private AmazonS3 s3Client;
	private Boolean thumbnailResult = true;
	private String thumbnailErrorMessage = "";
	private static Gson gson= new GsonBuilder().create();
	private LogToS3 ConvertingLogger;
	private Long StartTime;
	
	
	private static final DPLogger logger = DPLogger.getLogger(NotifyAPIHandler.class.getName());
	


	@Override
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
		try {
			buildParameters(config);
//			CommonUtil.delDirs("/tmp");
	        StartTime = System.currentTimeMillis();
	        
			logger.info(Tag + "message: " + job.toGsonStr());
			dataMessage = job.getJobParam(DataMessage.class);
			
			if( (dataMessage.getLosslessNotify()==null || !dataMessage.getLosslessNotify()) && dataMessage.getResult()) {
				ScheduleJob sj = new ScheduleJob();
				sj.setJobParam(dataMessage);
				sj.setJobGroup(job.getJobGroup());
				sj.setJobName(LosslessCompressionHandler.class.getName());
				sj.setJobRunner(2);
				ScheduleJobManager.addJob(sj);
				return "LosslessCompressionHandler start.";
			}
			
			
			requestData = gson.fromJson(dataMessage.getRequestData(), RequestData.class);
			String message = gson.toJson(dataMessage); 
	

	
			String convertedLogPath = dataMessage.getLogFolder() + "/converted.log";
			ConvertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
			ConvertingLogger.log(Tag, "DataMessage: " + message + "\r\n" + "RequestData: " + dataMessage.getRequestData());
	
			s3Permission = requestData.getIsTrial() ? CannedAccessControlList.PublicRead
					: CannedAccessControlList.AuthenticatedRead;

			// only success and pdf/fixedlayout need update opf Thumbnail.  
			if (dataMessage.getResult() && dataMessage.getThumbnails()!=null &&
					("pdf".equalsIgnoreCase(requestData.getFormat())||
					 "fixedlayout".equalsIgnoreCase(requestData.getFormat()))){
				updateOpfThumbnail(job);
			}
			// Calculate folder size
			totalSize =calculateTotalSize(s3Client,dataMessage);
	
		
			ResponseData responseData = generateResponse();
			
			int reTryCount=0;
            try {
                sendPostRequest(responseData);
            } catch (Exception e) {
                while (reTryCount < 3) {
                    reTryCount++;
                    sendPostRequest(responseData);
                }
            }
            
		}catch (ServerException | JobRetryException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			logger.error("Unexcepted Error Notify API",e);
			try {
				sendPostRequest(generateFailResponse("Unexcepted error " + e.getMessage()));
			} catch (Exception ioex){
				logger.error("Fail to send Server ",ioex);
			}
			throw new ServerException("id_err_999","Unexcepted");
		}
		return "Notify API Handler Done";

	}


	private ResponseData generateFailResponse(String errorMessage) throws Exception {
		ResponseData ret=generateResponse();
		resultData = (ResultData) ret.getR_data();
		resultData.setResult(false);
		resultData.setError_message(errorMessage);
		return ret;
	}


	private void updateOpfThumbnail(ScheduleJob job) throws ServerException {
        try {
			DataMessage dataMsg = job.getJobParam(DataMessage.class);
			this.s3Client = AmazonS3ClientBuilder.defaultClient();
			String basePath = "/tmp/" + dataMsg.getFolder() + "/";
			File f = new File(basePath+"META-INF/");
			if (!f.exists()){
				f.mkdirs();
			}
				
			// download container.xml
			Utility.downloadFromS3(s3Client, dataMsg.getTargetBucket(), 
					dataMsg.getFolder()+"/META-INF/container.xml", basePath+"META-INF/container.xml");
			// find opf path
			String opfPath = CommonUtil.findRootOpfFile(basePath);
			String relativeOpfPath = opfPath.substring(basePath.length());
			File opfDir = new File(new File(opfPath).getParent());
			if (!opfDir.exists()){
				opfDir.mkdirs();
			}
			// download from s3
			File opfFile = Utility.downloadFromS3(s3Client, dataMsg.getTargetBucket(), 
					dataMsg.getFolder() + "/"+ relativeOpfPath, opfPath);
			
			Document opfDoc = dBuilder.parse(opfFile);
			Node manifest = opfDoc.getElementsByTagNameNS("*", MANIFEST).item(0);
			
			// already has thumbnail in opf (pdf)
			if (CommonUtil.loadFile2String(opfFile).contains("THUMBNAIL/thumbnail"))
			{
				return;
			}
			
			// don't include zip itself.
			if(dataMsg.getThumbnails()!=null) {
				for (Integer page : dataMsg.getThumbnails()) {
					String pageNumber=Integer.toString(page);
					pageNumber = CommonUtil.paddingLeft(pageNumber,4,'0');
					Element item=opfDoc.createElement("item");
					item.setAttribute("href", "../THUMBNAIL/thumbnail" + pageNumber + 
							".jpg");
					item.setAttribute("id","thumbnail_" + pageNumber);
					item.setAttribute("media-type", "image/jpeg");
					manifest.appendChild(item);
				}
			}
	       
			Utility.writeXml(opfDoc, opfPath);
			s3Permission = requestData.getIsTrial() ? CannedAccessControlList.PublicRead
					: CannedAccessControlList.AuthenticatedRead;

			CommonUtil.uploadToS3(s3Client, opfFile,
					dataMsg.getTargetBucket() + "/" + dataMsg.getFolder() +  "/" + relativeOpfPath
					, s3Permission, dataMsg.getFolder());
			CommonUtil.delDirs(basePath);
		} catch (Exception e) {
			logger.error("Fail to fix opf about thumbnail.",e);
			throw new ServerException("Unexcepted error", e.getMessage());
		}
	}





	private int countTotalThumbnails(String path) {
		File f =new File(path);
		f.listFiles();
		return f.list().length-1;
	}



	private ResponseData generateResponse() throws Exception {
		// Add CheckResult.log to error message.
		
		ResponseData responseData = new ResponseData();
		resultData = responseData.new ResultData();
		StringBuilder errorMessageBuilder =new StringBuilder();
		if (null!=dataMessage.getErrorMessage())
			errorMessageBuilder.append(dataMessage.getErrorMessage());
		ObjectListing objects = s3Client.listObjects(originalBucket, dataMessage.getLogFolder());
		String checkResultPath = dataMessage.getLogFolder() + "/CheckResult.log";
		logger.debug("checkResultPath:" + checkResultPath);
		for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
			logger.debug("checking Object in S3:" + objectSummary.getKey());
			if (objectSummary.getKey().equals(checkResultPath)) {
				logger.debug("Found LogFile, try to load:" + objectSummary.getKey());
				S3Bucket = originalBucket;
				S3Key = objectSummary.getKey();
				String logPath = "/tmp/tempLog.log";
				File logFile = Utility.downloadFromS3(s3Client, S3Bucket, S3Key, logPath);
				String logContent = loadCheckResult(logFile);
				
				if (errorMessageBuilder.length()==0) {
					errorMessageBuilder.append(logContent);
				} else {
					errorMessageBuilder
					.append("\r\n")
					.append(logContent);
				}

				logFile.delete();
			}
		}

		if (!thumbnailResult) {
			errorMessageBuilder.append("Thumbnail converting fail.\r\n")
				.append( thumbnailErrorMessage);
		}

		// remove \t (which is not useful in final output)
		if (errorMessageBuilder.length()>0 && errorMessageBuilder.indexOf("\t")>-1) {
			replaceAll(errorMessageBuilder,"\t"," ");
		}


		// ResultData initialize.
		// resultData.setResult(dataMessage.getResult() & thumbnailResult);
		resultData.setResult(dataMessage.getResult());
		if (!dataMessage.getResult()) {
			resultData.setError_code(error_code_converting_fail);
		}
		logger.debug("Sending ErrorMessage:" + errorMessageBuilder.toString());
		resultData.setError_message(errorMessageBuilder.toString().trim());
		resultData.setChecksum(dataMessage.getChecksum());
		resultData.setVersion(requestData.getVersion());
		resultData.setSize(String.valueOf(totalSize));
		//#19
		resultData.setMedia(dataMessage.getMedia());
		// String r_data = gson.toJson(resultData);

		// ResponseData initialize.
		responseData.setItem(requestData.getItem());
		responseData.setCall_type(requestData.getCall_type());
		responseData.setProcessing_id(requestData.getBook_file_id());
		if(StringUtils.isNotBlank(dataMessage.getFolder())) {
		responseData.setFile_url(
				dataMessage.getFolder().replace(requestData.getIsTrial() ? trialFolder : originalFolder, ""));
		}
		if (!requestData.getIsTrial()) {
			URL source = new URL(requestData.getEfile_cover_url());
			String cover_name = source.getFile().contains("/")
					? source.getFile().substring(source.getFile().lastIndexOf("/") + 1) : source.getFile();
			responseData.setFile_cover_url(dataMessage.getFolder().replace(originalFolder, "") + "/" + cover_name);
		}
		responseData.setReturn_file_num(requestData.getReturn_file_num());
		responseData.setR_data(resultData);
		return responseData;
	}
	
	private String loadCheckResult(File logFile) throws IOException {
		InputStream objectData = new FileInputStream(logFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(objectData));
		String line;
		String extraLog = "";
		
		Map <GenericLogLevel,List<GenericLogRecord>> records= new HashMap<>();
		while ((line = br.readLine()) != null) {
			try {
				GenericLogRecord tmpRec = GenericLogRecord.fromString(line);
				List <GenericLogRecord> tmpList = records.get(tmpRec.getLevel());
				if (null==tmpList)
				{
					tmpList = new ArrayList<>();
					records.put(tmpRec.getLevel(),tmpList);
				}
				tmpList.add(tmpRec);
			}catch (Exception e)
			{
				logger.info("skipping old info dected:" + line );
				extraLog += line + "\r\n";
			}
		}
		objectData.close();
		StringBuilder outlog= new StringBuilder();
		// output by level error, 
		appendLog(outlog, records.get(GenericLogLevel.Fatal));
		appendLog(outlog, records.get(GenericLogLevel.Error));
		appendLog(outlog, records.get(GenericLogLevel.Warn));
//		appendLog(outlog, records.get(GenericLogLevel.Info));
//		appendLog(outlog, records.get(GenericLogLevel.Debug));
		logger.debug("outLog=" + outlog.toString());
		if (outlog.length()>0){
			return outlog.toString();
		}else
			return extraLog;
	}





	/**
	 * 
	 * @param outlog
	 * @param list
	 */
	private static void appendLog(StringBuilder outlog, List<GenericLogRecord> list) {
		if (null==list)
			return;
		for (GenericLogRecord tmprec:list)
		{
			outlog
//			.append(tmprec.getSource())
//			.append(":")
			.append(tmprec.getLevel().toString())
			.append(":")
			.append(tmprec.getMessage())
			.append("\r\n");
		}
	}





	public static void replaceAll(StringBuilder builder, String from, String to)
	{
	    int index = builder.indexOf(from);
	    while (index != -1)
	    {
	        builder.replace(index, index + from.length(), to);
	        index += to.length(); // Move to the end of the replacement
	        index = builder.indexOf(from, index);
	    }
	}

	private void sendPostRequest(ResponseData responseData) throws IOException {
		/* [+] Send http request (POST) */
		logger.info(Tag + "Sending 'POST' request to URL:" + URL_BookProcessResult);
		/*
		 * if(URL_BookProcessResult.contains("https")) {
		 * javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier( new
		 * javax.net.ssl.HostnameVerifier(){ public boolean verify(String
		 * hostname, javax.net.ssl.SSLSession sslSession) { return true; } } );
		 * 
		 * TrustManager[] trustAllCerts = new TrustManager[]{ new
		 * X509TrustManager() { public java.security.cert.X509Certificate[]
		 * getAcceptedIssuers() { return null; } public void checkClientTrusted(
		 * java.security.cert.X509Certificate[] certs, String authType) { }
		 * public void checkServerTrusted( java.security.cert.X509Certificate[]
		 * certs, String authType) { } } }; SSLContext sc =
		 * SSLContext.getInstance("SSL"); sc.init(null, trustAllCerts, new
		 * java.security.SecureRandom());
		 * HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		 * }
		 */
		// Send post request
		URL obj = new URL(URL_BookProcessResult);
		HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");

		String jsonData = gson.toJson(responseData);
		connection.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.writeBytes(URLEncoder.encode(jsonData,"UTF-8"));
		wr.flush();
		wr.close();

		int responseCode = connection.getResponseCode();
		logger.info(Tag + "Response Code:" + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuffer response = new StringBuffer();

		int BUFFER_SIZE = 1024;
		char[] buffer = new char[BUFFER_SIZE]; // or some other size,
		int charsRead = 0;
		while ((charsRead = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
			response.append(buffer, 0, charsRead);
		}

		logger.info(Tag + "response:" + response.toString());

		in.close();
		connection.disconnect();
		/* [-] Send http request (POST) */
		ConvertingLogger.log(GenericLogLevel.Info,"ResponseData:" + gson.toJson(responseData) + "\r\n" + "URL:" + URL_BookProcessResult
				+ "\r\n" + "ResponseCode:" + responseCode + "\r\n" + "Response:" + response.toString() + "\r\n"
				+ "End NotifyAPI spend " + (System.currentTimeMillis() - StartTime) + "ms",
				NotifyAPIHandler.class.getName());
		ConvertingLogger.uploadLog();
	}

	protected static Long calculateTotalSize(AmazonS3 s3Client, DataMessage dataMessage) {
		Long totalSize= 0L;
		Integer totalItems = 0;
		try {
		ObjectListing objects = s3Client.listObjects(dataMessage.getTargetBucket(), dataMessage.getFolder() + "/");
		// skip thumbnail
		do {
			for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
				if (objectSummary.getKey().toLowerCase().contains("/thumbnail/")){
					continue;
				}
				totalSize += objectSummary.getSize();
				totalItems++;
			}
			objects = s3Client.listNextBatchOfObjects(objects);
		} while (!objects.getObjectSummaries().isEmpty());
		logger.info(Tag + "In " + dataMessage.getFolder() + "/ totalSize:" + totalSize + ", totalItems:" + totalItems);
	    
        } catch (Exception e) {
        }
		return totalSize;
	}



	public void buildParameters(Properties properties) throws IOException {
		originalFolder = properties.getProperty("originalFolder") + "/";
		trialFolder = properties.getProperty("trialFolder") + "/";
		originalBucket = properties.getProperty("originalBucket");
		URL_BookProcessResult = properties.getProperty("URL_BookProcessResult");

		// Create S3 Client
        this.s3Client = AmazonS3ClientBuilder.defaultClient();		
	}




	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// no DB needed.
		
	}
	
	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.ExtEc2Mode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		
		Gson gson = new Gson();
		DataMessage message=job.getJobParam(DataMessage.class);
		String reqData=message.getRequestData();
		RequestData req = gson.fromJson(reqData, RequestData.class);
		String format=req.getFormat();
		logger.debug("Format: " + format);
		if ("pdf".equalsIgnoreCase(format)){
			return RunMode.ExtEc2Mode;
		}
		return RunMode.LambdaMode;
	}

}
