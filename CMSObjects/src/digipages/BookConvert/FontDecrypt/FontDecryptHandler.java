package digipages.BookConvert.FontDecrypt;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;

import com.google.gson.Gson;

import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.utility.CmdExecutor;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;

import model.ScheduleJob;

public class FontDecryptHandler implements CommonJobHandlerInterface {
	private static final String PATHSEP = "/";
	public static DPLogger logger = DPLogger.getLogger(FontDecryptHandler.class.getName());
	private static Gson gson = new Gson();
	protected LogToS3 convertingLogger;
	protected LogToS3 checkResultLogger;
	private String logFolder;
	private Object originalFolder;
	private Object trialFolder;
	private DataMessage dataMsg;
	private AmazonS3 s3Client;
	private RequestData requestData;
	private String epubFolderPath;
	private File epubFolder;
	private String bookUniqueIdentifier;
	protected CannedAccessControlList Permission;

	@Override
	public String mainHandler(Properties conf, ScheduleJob job) throws ServerException, JobRetryException {
		initEnv(conf, job);

		// decrypt epub fonts, if fail report fail with message.
		try {
			// get data from S3
			CommonUtil.downloadDirFromS3(dataMsg.getTargetBucket(), dataMsg.getFolder(), s3Client, epubFolderPath);
			// OPF / dc:identifier
			epubOPFUniqueIdentifierInfoRecu(epubFolder);

			logger.info("bookUniqueIdentifier: " + bookUniqueIdentifier);

			new File(epubFolderPath + "tmpfont").mkdir();

			fontDecryptRecu(epubFolder, bookUniqueIdentifier);

			dataMsg.setLogFolder(getLogFolder(requestData));
			dataMsg.setResult(true);
		} catch (Exception e) {
			logger.error("Unexcepted Exception", e);
			fontDecryptFailHandler(job, e);
		} finally {
			notifyAPI(dataMsg, requestData);
			convertingLogger.uploadLog();
			checkResultLogger.uploadLog();
		}

		return "FontDecrypt finished.";
	}

	private void initEnv(Properties conf, ScheduleJob job) {
		String originalBucket = conf.getProperty("originalBucket");
		logFolder = conf.getProperty("logFolder");
		this.originalFolder = conf.getProperty("originalFolder");
		this.trialFolder = conf.getProperty("trialFolder");

		// prepare
		this.dataMsg = job.getJobParam(DataMessage.class);
		this.requestData = gson.fromJson(dataMsg.getRequestData(), RequestData.class);
		this.s3Client = AmazonS3ClientBuilder.defaultClient();

		String convertedLogPath = getLogFolder(requestData) + "/" + "converted.log";
		this.convertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);

		String checkResultLogPath = getLogFolder(requestData) + "/" + "CheckResult.log";
		this.checkResultLogger = new LogToS3(s3Client, originalBucket, checkResultLogPath);

		this.Permission = requestData.getIsTrial() ? CannedAccessControlList.PublicRead : CannedAccessControlList.AuthenticatedRead;

		epubFolderPath = "/tmp/Epub_" + requestData.getBook_file_id() + PATHSEP;
		epubFolder = new File(epubFolderPath);

		logger.info("epubFolderPath: " + epubFolderPath);
		logger.info("epubFolder: " + epubFolder.getPath());
		logger.info("S3 permission: " + Permission);
	}

	// fail, need to report back to NotifyAPI
	private void fontDecryptFailHandler(ScheduleJob job, Exception e) {
		String errMessage = e.getMessage();

		if (e instanceof ServerException) {
			errMessage = ((ServerException) e).getError_message();
		}

		logger.error("Fail to font decrypt with Exception " + errMessage);

		job.setErrMessage(errMessage);
		job.setStatus(-1);
		job.setFinishTime(new Date());

		dataMsg.setResult(false);
		dataMsg.appendErrorMessage(errMessage);
	}

	/**
	 * get opf unique-identifier info
	 * 
	 * @param currentFile
	 * @return
	 */
	private String epubOPFUniqueIdentifierInfoRecu(File currentFile) {
		String uniqueIdentifier = "";

		if (currentFile.isDirectory()) {
			String[] child = currentFile.list();

			for (int i = 0; i < child.length; i++) {
				epubOPFUniqueIdentifierInfoRecu(new File(currentFile, child[i]));
			}
		}

		String fileName = currentFile.getPath();
		String extension = "";

		if (fileName.contains(".")) {
			extension = fileName.substring(fileName.lastIndexOf('.') + 1);
		}

		if (extension.equalsIgnoreCase("opf")) {
			try {
				File opfFile = new File(currentFile.getPath());

				logger.info("opfFile: " + opfFile);
				logger.info("opfFilePath: " + currentFile.getPath());
				logger.info("opfFileLength: " + opfFile.length());

				InputStream xml = new FileInputStream(new File(currentFile.getPath()));
				Document doc = Jsoup.parse(xml, "utf-8", "", org.jsoup.parser.Parser.xmlParser());
				xml.close();

				for (Element e : doc.select("package")) {
					logger.info("unique-identifier:" + e.attr("unique-identifier"));

					for (Element ele : e.select("metadata")) {
						List<Node> nodeList = ele.childNodes();

						for (Node n : nodeList) {
							if (n.nodeName().equals("dc:identifier") && n.attr("id").equals(e.attr("unique-identifier"))) {
								uniqueIdentifier = ((TextNode) n.childNode(0)).text();
								bookUniqueIdentifier = ((TextNode) n.childNode(0)).text();

								logger.info("dc:identifier:" + ((TextNode) n.childNode(0)).text());
							}
						}
					}
				}
			} catch (FileNotFoundException e) {
				logger.error("FileNotFoundException ", e);
			} catch (IOException e) {
				logger.error("IOException ", e);
			}
		}

		return uniqueIdentifier;
	}

	/**
	 * check local dir if font need decrypt
	 * 
	 * @param epubFolder
	 * @return
	 */
	private void fontDecryptRecu(File currentFile, String obfuscationKey) {
		if (currentFile.isDirectory()) {
			String[] child = currentFile.list();

			for (int i = 0; i < child.length; i++) {
				fontDecryptRecu(new File(currentFile, StringEscapeUtils.escapeHtml4(child[i])), obfuscationKey);
			}
		}

		String fileName = currentFile.getPath();
		String extension = "";

		if (fileName.contains(".")) {
			extension = fileName.substring(fileName.lastIndexOf('.') + 1);
		}

		if (extension.equalsIgnoreCase("ttf") || extension.equalsIgnoreCase("ttc") || extension.equalsIgnoreCase("otf") || extension.equalsIgnoreCase("woff") || extension.equalsIgnoreCase("dfont")) {
			try {
				String fontName = currentFile.getPath().replace("/tmp/Epub_" + requestData.getBook_file_id() + PATHSEP, "");

				// Download font from S3
				String s3cmd = "aws s3 cp s3://" + dataMsg.getTargetBucket() + "/" + dataMsg.getFolder() + "/" + fontName + " " + currentFile.getPath();
				logger.debug("Downloading font from S3: cmd=" + s3cmd);
				Runtime.getRuntime().exec(s3cmd);

				// delete S3 font file (epub Dir)
				delS3FontFile(s3Client, currentFile.getPath());

				// String fontFilename = FilenameUtils.getName(currentFile.getPath());
				// File resultFontFile = new File(epubFolderPath + "tmpfont", fontFilename);

				String fontFilename = FilenameUtils.getName(currentFile.getPath());
				File resultFontFile = null;

				File obfuscatedDir = new File(epubFolderPath, "tmpfont");
				resultFontFile = new File(obfuscatedDir, fontFilename);

				// if (currentFile != null && resultFontFile.getAbsolutePath().equals(currentFile.getAbsolutePath())) {
				// System.err.println("[ERROR] Input and destination font file are the same. Cannot continue.");
				// }

				System.out.println("Obfuscating font " + currentFile + " ...");

				// resultFontFile.getParentFile().mkdirs();
				InputStream inStream = new FileInputStream(currentFile);
				FileOutputStream outStream = new FileOutputStream(resultFontFile);

				// IDPF Algorithm
				obfuscateFont(inStream, outStream, obfuscationKey, "");
				System.out.println("Using IDPF obfuscation key \"" + obfuscationKey + "\"");
				System.out.println("Font obfuscated to " + resultFontFile.getAbsolutePath());

				inStream.close();
				outStream.close();

				if (fontIsEncrypted(new File(resultFontFile.getAbsolutePath()))) {
					File fail_file = new File(resultFontFile.getAbsolutePath());
					boolean result = Files.deleteIfExists(fail_file.toPath());
					System.out.println("delete result: " + result);

					if (result) {
						// Adobe Algorithm
						InputStream inStreamAdobe = new FileInputStream(currentFile);
						FileOutputStream outStreamAdobe = new FileOutputStream(resultFontFile);

						obfuscateFont(inStreamAdobe, outStreamAdobe, obfuscationKey, "adobe");
						System.out.println("Using Adobe obfuscation key \"" + obfuscationKey + "\"");
						System.out.println("Font obfuscated to " + resultFontFile.getAbsolutePath());

						inStreamAdobe.close();
						outStreamAdobe.close();
					}
				}

				// Upload font to S3
				String ups3cmd = "aws s3 cp " + epubFolderPath + "tmpfont/" + currentFile.getName() + " s3://" + dataMsg.getTargetBucket() + "/" + dataMsg.getFolder() + "/" + fontName;

				logger.debug("Uploading font to S3: cmd=" + ups3cmd);

				Runtime.getRuntime().exec(ups3cmd);
			} catch (FileNotFoundException e) {
				logger.error("FileNotFoundException ", e);
			} catch (IOException e) {
				logger.error("IOException ", e);
			} catch (Exception e) {
				logger.error("Exception ", e);
			}
		}
	}

	private void fontDecryptRecuBak(File currentFile, String uniqueIdentifier) {
		if (currentFile.isDirectory()) {
			String[] child = currentFile.list();

			for (int i = 0; i < child.length; i++) {
				fontDecryptRecuBak(new File(currentFile, StringEscapeUtils.escapeHtml4(child[i])), uniqueIdentifier);
			}
		}

		String fileName = currentFile.getPath();
		String extension = "";

		if (fileName.contains(".")) {
			extension = fileName.substring(fileName.lastIndexOf('.') + 1);
		}

		if (!currentFile.getPath().contains("tmpfont") && (extension.equalsIgnoreCase("ttf") || extension.equalsIgnoreCase("ttc") || extension.equalsIgnoreCase("otf") || extension.equalsIgnoreCase("woff") || extension.equalsIgnoreCase("dfont"))) {
			try {
				String fontName = currentFile.getPath().replace("/tmp/Epub_" + requestData.getBook_file_id() + PATHSEP, "");

				// Download font from S3
				String s3cmd = "aws s3 cp s3://" + dataMsg.getTargetBucket() + "/" + dataMsg.getFolder() + "/" + fontName + " " + currentFile.getPath();
				logger.debug("Downloading font from S3: cmd=" + s3cmd);
				Runtime.getRuntime().exec(s3cmd);

				// delete S3 font file (epub Dir)
				delS3FontFile(s3Client, currentFile.getPath());

				String cmd = "/usr/local/bin/fontforge -lang=py -script /usr/local/converter/glyphIgo.py obfuscate -f " + currentFile.getPath() + " -i '" + uniqueIdentifier + "' -o " + epubFolderPath + "tmpfont/" + currentFile.getName();
				logger.debug("Starting FontDecrypt: cmd=" + cmd);

				File tempScript = createTempScript(epubFolderPath, currentFile, uniqueIdentifier);
				logger.debug("tempScript file: " + tempScript.getPath());
				logger.debug("python: python /usr/local/converter/runCmd.py " + tempScript.getPath());

				CmdExecutor executor = new CmdExecutor();
				executor.run("python /usr/local/converter/runCmd.py " + tempScript.getPath());

				logger.debug("FontDecrypt Out result:" + executor.getStdOut());
				logger.debug("FontDecrypt Err result:" + executor.getStdErr());

				String totalMsg = executor.getStdOut();
				boolean fontProcess = false;

				totalMsg = totalMsg.replaceAll("\r", "\n");
				totalMsg = totalMsg.replaceAll("\n\n", "\n");

				for (String tmpLine : totalMsg.split("\n")) {
					// Check FontDecrypt output
					if (tmpLine.toLowerCase().contains("(de)obfuscated")) {
						fontProcess = true;
						break;
					}
				}

				if (fontProcess) {
					logger.info("font is decrypted");
				} else {
					logger.warn("font is not decrypted: s3://" + dataMsg.getTargetBucket() + "/" + dataMsg.getFolder() + "/" + fontName);
				}

				// Upload font to S3
				String ups3cmd = "aws s3 cp " + epubFolderPath + "tmpfont/" + currentFile.getName() + " s3://" + dataMsg.getTargetBucket() + "/" + dataMsg.getFolder() + "/" + fontName;

				logger.debug("Uploading font to S3: cmd=" + ups3cmd);

				Runtime.getRuntime().exec(ups3cmd);
			} catch (FileNotFoundException e) {
				logger.error("FileNotFoundException ", e);
			} catch (IOException e) {
				logger.error("IOException ", e);
			} catch (Exception e) {
				logger.error("Exception ", e);
			}
		}
	}

	private static boolean fontIsEncrypted(File currentFile) {
		// use try catch to determine font encryption.
		InputStream inStream = null; 

		try {
			System.out.println("Hit possible font: " + currentFile.getName());
			inStream = new FileInputStream(currentFile);
			Font font = Font.createFont(Font.TRUETYPE_FONT, new BufferedInputStream(inStream));
			System.out.println("FontName:" + font.getFontName() + " NumGlyphs:" + font.getNumGlyphs());
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException ", e);
		} catch (FontFormatException e) {
			return true;
		} catch (IOException e) {
			logger.error("IOException ", e);
		} finally {
			try {
				inStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	private static byte[] makeXORMask(String opfUID) {
		if (opfUID == null) {
			return null;
		}

		ByteArrayOutputStream mask = new ByteArrayOutputStream();

		/**
		 * This starts with the "unique-identifier", strips the whitespace, and applies SHA1 hash
		 * giving a 20 byte key that we can apply to the font file.
		 * 
		 * See: http://www.idpf.org/epub/30/spec/epub30-ocf.html#fobfus-keygen
		 **/
		try {
			Security.addProvider(new com.sun.crypto.provider.SunJCE());
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			String temp = opfUID.trim();
			sha.update(temp.getBytes("UTF-8"), 0, temp.length());
			mask.write(sha.digest());
		} catch (NoSuchAlgorithmException e) {
			System.err.println("No such Algorithm (really, did I misspell SHA-1?");
			System.err.println(e.toString());
			return null;
		} catch (IOException e) {
			System.err.println("IO Exception. check out mask.write...");
			System.err.println(e.toString());
			return null;
		}

		if (mask.size() != 20) {
			System.err.println("makeXORMask should give 20 byte mask, but isn't");
			return null;
		}

		return mask.toByteArray();
	}

	private static byte[] makeAdobeXORMask(String opfUID) {
		if (opfUID == null) {
			return null;
		}

		ByteArrayOutputStream mask = new ByteArrayOutputStream();
		int acc = 0;
		int len = opfUID.length();

		for (int i = 0; i < len; i++) {
			char c = opfUID.charAt(i);
			int n;

			if ('0' <= c && c <= '9') {
				n = c - '0';
			} else if ('a' <= c && c <= 'f') {
				n = c - ('a' - 10);
			} else if ('A' <= c && c <= 'F') {
				n = c - ('A' - 10);
			} else {
				continue;
			}

			if (acc == 0) {
				acc = 0x100 | (n << 4);
			} else {
				mask.write(acc | n);
				acc = 0;
			}
		}

		if (mask.size() != 16) {
			return null;
		}

		return mask.toByteArray();
	}

	/** Implements the Obfuscation Algorithm from
	 * http://www.openebook.org/doc_library/informationaldocs/FontManglingSpec.html
	 **/
	public static void obfuscateFont(InputStream in, OutputStream out, String obfuscationKey, String obfuscationMode) throws IOException {
		byte[] mask;

		if (obfuscationMode.equalsIgnoreCase("adobe")) {
			// mask = adobeKeyFromIdentifier(obfuscationKey);
			mask = makeAdobeXORMask(obfuscationKey);
		} else {
			mask = makeXORMask(obfuscationKey);
		}

		System.out.printf("The key being used:\n");

		for (int n = 0; n < mask.length; n++) {
			System.out.printf("%2x ", mask[n]);
		}

		System.out.println();

		try {
			byte[] buffer = new byte[4096];
			int len;
			boolean first = true;

			while ((len = in.read(buffer)) > 0) {
				if (first && mask != null) {
					first = false;

					for (int i = 0; i < 1040; i++) {
						buffer[i] = (byte) (buffer[i] ^ mask[i % mask.length]);
					}
				}

				out.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		out.close();
	}

	/**
	 * Encrypt the supplied string using a SHA-1 Hash. This could
	 * be stronger by using a salt, etc. but this is lightweight
	 * mangling, so hardly worth it.
	 * 
	 * @param x
	 * @return
	 */
	public static byte[] encrypt(String x) {
		try {
			java.security.MessageDigest d = null;
			d = java.security.MessageDigest.getInstance("SHA-1");
			d.reset();
			d.update(x.getBytes());

			return d.digest();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Generate the IDPF key from the supplied unique ID
	 * 
	 * @param uuid
	 * @return
	 */
	public static byte[] idpfKeyFromIdentifier(String uuid) {
		String cleanUUID = uuid.replaceAll(" ", "");
		cleanUUID = cleanUUID.replaceAll("\t", "");
		cleanUUID = cleanUUID.replaceAll("\n", "");
		cleanUUID = cleanUUID.replaceAll("\r", "");

		return encrypt(cleanUUID);
	}

	/**
	 * Create the Adobe key by cleaning out cruft and returning
	 * the hex values as a byte array
	 * 
	 * @param uuid
	 * @return
	 */
	public static byte[] adobeKeyFromIdentifier(String uuid) {
		String cleanUUID = uuid.replaceAll("urn:uuid", "");
		cleanUUID = cleanUUID.replaceAll("-", "");
		cleanUUID = cleanUUID.replaceAll(":", "");
		String encodedUUID = DatatypeConverter.printHexBinary(cleanUUID.getBytes());

		return DatatypeConverter.parseHexBinary(encodedUUID);
	}

	public File createTempScript(String epubFolderPath, File currentFile, String uniqueIdentifier) throws IOException {
		File tempScript = File.createTempFile("script", ".txt");
		Writer streamWriter = new OutputStreamWriter(new FileOutputStream(tempScript));
		PrintWriter printWriter = new PrintWriter(streamWriter);
		printWriter.println("/usr/local/bin/fontforge,-lang=py,-script,/usr/local/converter/glyphIgo.py,obfuscate,-f," + currentFile.getPath() + ",-i," + uniqueIdentifier + ",-o," + epubFolderPath + "tmpfont/" + currentFile.getName());
		printWriter.close();

		return tempScript;
	}

	private void delS3FontFile(AmazonS3 s3Client, String epubFileName) {
		String rootFolder = dataMsg.getFolder();
		String fileName = epubFileName.replace("/tmp/Epub_" + requestData.getBook_file_id() + PATHSEP, "");
		String targetBucket = dataMsg.getTargetBucket();
		logger.debug("deleting epub rootFolder: " + rootFolder + ", epubFileName: " + fileName + ", targetBucket: " + targetBucket);
		String key = rootFolder + "/" + fileName;
		logger.debug("key: " + key);

		s3Client.deleteObject(new DeleteObjectRequest(targetBucket, key));
	}

	private static void notifyAPI(DataMessage dataMessage, RequestData requestData2) throws ServerException {
		try {
			ScheduleJob sj = new ScheduleJob();
			sj.setJobParam(dataMessage);
			sj.setJobName(NotifyAPIHandler.class.getName());
			sj.setJobGroup(requestData2.getBook_file_id());
			ScheduleJobManager.addJob(sj);
		} catch (Exception e) {
			logger.error("Unexcepted when notify API", e);
			throw new ServerException("id_err_999", "Unexcepted when notify API");
		}
	}

	protected String getLogFolder(RequestData lambdaRequestData) {
		String folderPath;

		folderPath = logFolder + "/" + (lambdaRequestData.getIsTrial() ? trialFolder : originalFolder) + "/" + lambdaRequestData.getItem() + "/" + lambdaRequestData.getBook_file_id();

		return folderPath;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// don't need DB connection
	}

	/**
	 * default is Ec2, no other possible.
	 * 
	 * @param runMode
	 * @return
	 */
	public static RunMode getRetryRunMode(RunMode runMode) {
		if (runMode == RunMode.LambdaMode) {
			return RunMode.ExtEc2Mode;
		}

		return RunMode.ExtEc2Mode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.ExtEc2Mode;
	}
}