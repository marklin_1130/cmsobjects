
package digipages.BookConvert.FontDecrypt.opentype;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.io.FontDataOutputStream;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static digipages.BookConvert.FontDecrypt.io.FontDataOutputStream.OPEN_TYPE_CHARSET;
import static org.slf4j.LoggerFactory.getLogger;

public class ControlValueTable extends OpenTypeTable {
    private static final Logger log = getLogger(ControlValueTable.class);
    private List<Short> values = new LinkedList<Short>();

    public String getTableType() {
        return "cvt ";
    }

    public void readData(byte[] data) throws IOException {
        FontDataInputStream input = new FontDataInputStream(data);
        while (input.available() >= 2)
            values.add(input.readShort());

        if (input.available() == 1) {
            log.info("original cvt table data length not divisble by two, adding 1 byte padding.");
            values.add((short) input.readByte());
        }
    }

    protected byte[] generateUnpaddedData() throws IOException {
        FontDataOutputStream out = new FontDataOutputStream(OPEN_TYPE_CHARSET);
        for (Short valueOn : values)
            out.writeShort(valueOn);

        return out.toByteArray();
    }

    public List<Short> getValues() {
        return values;
    }

    public Short getValue(Long index) throws CvtValueNotFoundException {
        if (index > values.size())
            throw new CvtValueNotFoundException();

        return values.get(index.intValue());
    }

    public static class CvtValueNotFoundException extends IOException {
    }
}
