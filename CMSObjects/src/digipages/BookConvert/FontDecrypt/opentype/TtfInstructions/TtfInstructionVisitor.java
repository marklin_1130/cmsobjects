

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions;

import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.*;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control.IfInstruction;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control.ElseInstruction;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control.EndFunctionInstruction;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control.EndIfInstruction;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control.FunctionDefInstruction;

import java.io.IOException;

public interface TtfInstructionVisitor {
    void visitGeneric(TtfInstruction instruction) throws IOException;

    void visit(IfInstruction instruction) throws IOException;

    void visit(ElseInstruction instruction) throws IOException;

    void visit(EndIfInstruction instruction) throws IOException;

    void visit(FunctionDefInstruction instruction) throws IOException;

    void visit(EndFunctionInstruction instruction) throws IOException;
}
