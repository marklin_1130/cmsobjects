package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetProjectionVectorToLineInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x06, 0x07};
    }

    boolean isSetPerpendicularToLine = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x07)
            isSetPerpendicularToLine = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        Long pointId2 = stack.popUint32();
        Long pointId1 = stack.popUint32();
        // todo graphics state handeling
    }
}
