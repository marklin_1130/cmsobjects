

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class OrInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x5B};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Number e1 = stack.popNumber();
        Number e2 = stack.popNumber();

        boolean result = e1.longValue() > 0 || e2.longValue() > 0;
        stack.push(boolToUint32(result));
    }
}
