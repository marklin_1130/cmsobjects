

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

import static digipages.BookConvert.FontDecrypt.FontVerterUtils.isBitSet;

public class MoveDirectRelativePointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0xC0, 0xDF};
    }

    boolean resetRp0 = false;
    boolean keepDistanceGreaterThanMin = false;
    boolean roundDistance = false;
    short engineDistanceType = 0;

    public void read(FontDataInputStream in) throws IOException {
        // 32 values between the 0xC0 and 0xDF instruction code range are params and fit into a byte
        byte flags = (byte) (code - 0xC0);
        resetRp0 = isBitSet(0, flags);
        keepDistanceGreaterThanMin = isBitSet(1, flags);
        roundDistance = isBitSet(2, flags);

        engineDistanceType = (short) (flags >> 3);
    }

    public void execute(InstructionStack stack) throws IOException {
        Long pointId = stack.popUint32();

        // todo graphics state handeling
    }

}
