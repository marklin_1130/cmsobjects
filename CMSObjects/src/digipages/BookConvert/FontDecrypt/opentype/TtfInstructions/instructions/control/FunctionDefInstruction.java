

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfInstructionVisitor;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class FunctionDefInstruction extends TtfInstruction {
    private Integer functionId = 0;

    public int[] getCodeRanges() {
        return new int[]{0x2C};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        functionId = stack.popNumber().intValue();
    }

    public Integer getFunctionId() {
        return functionId;
    }

    @Override
    public void accept(TtfInstructionVisitor visitor) throws IOException {
        visitor.visit(this);
    }
}
