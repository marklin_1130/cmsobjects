

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class ReadStoreInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x43};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long index = stack.popUint32();

        Long value = vm.getStorageAreaValue(index);
        if (value == null) {
            // unsure if non itialized storage area val is fine or if something that could effect other font libs
            log.warn("ReadStore storage area index not initialized");
            value = 0L;
        }
        stack.push(value);
    }
}
