

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MoveDirectAbsolutePointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x2E, 0x2F};
    }

    boolean roundValue = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x2F)
            roundValue = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        Long pointId = stack.popUint32();

        // todo graphics state handeling
    }
}
