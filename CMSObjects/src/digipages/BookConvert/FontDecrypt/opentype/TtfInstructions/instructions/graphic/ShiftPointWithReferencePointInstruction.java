package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class ShiftPointWithReferencePointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x32, 0x33};
    }

    public boolean useRp2 = true;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x33)
            useRp2 = false;
    }

    public void execute(InstructionStack stack) throws IOException {
        Long pointId1 = stack.popUint32();
        Long pointId2 = stack.popUint32();
        Long ploopvalue = stack.popUint32();
        // todo graphics state handeling
    }
}
