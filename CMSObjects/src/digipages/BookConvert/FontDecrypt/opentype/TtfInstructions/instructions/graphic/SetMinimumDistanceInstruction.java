package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetMinimumDistanceInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x1A};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Float minDistance = stack.popF26Dot6();
        vm.getGraphicsState().minimumDistance = minDistance;
    }
}
