

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MoveIndirectAbsolutePointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x3E, 0x3F};
    }

    boolean roundDistance = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x3F)
            roundDistance = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        Float cvtEntry = stack.popF26Dot6();
        Long pointId = stack.popUint32();

        // todo graphics state handeling
    }
}
