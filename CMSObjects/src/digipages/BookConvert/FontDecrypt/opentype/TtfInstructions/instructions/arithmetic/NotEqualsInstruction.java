

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class NotEqualsInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x55};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Object e1 = stack.pop();
        Object e2 = stack.pop();

        stack.push(boolToUint32(!e1.equals(e2)));
    }
}
