package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.awt.geom.Point2D;
import java.io.IOException;

public class SetFreedomVectorToProjectionVectorInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x0E};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        vm.getGraphicsState().freedomVector = (Point2D.Double) vm.getGraphicsState().projectionVector.clone();
    }
}
