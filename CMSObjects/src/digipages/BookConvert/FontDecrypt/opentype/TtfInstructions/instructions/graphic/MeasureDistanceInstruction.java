

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MeasureDistanceInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x49, 0x4A};
    }

    boolean gridFittedOutline = true;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x4A)
            gridFittedOutline = false;

    }

    public void execute(InstructionStack stack) throws IOException {
        Long pointId2 = stack.popUint32();
        Long pointId1 = stack.popUint32();

        // todo graphics state to grab actual point values
        Float distance = 0.0f;
        stack.push(distance);
    }
}
