

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfInstructionVisitor;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class IfInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x58};
    }

    public boolean shouldExecute;

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Integer condition = stack.popNumber().intValue();
        shouldExecute = condition.equals(1);
    }

    @Override
    public void accept(TtfInstructionVisitor visitor) throws IOException {
        visitor.visit(this);
    }
}
