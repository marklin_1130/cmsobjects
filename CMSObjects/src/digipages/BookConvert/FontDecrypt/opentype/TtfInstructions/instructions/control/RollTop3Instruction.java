

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class RollTop3Instruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x8A};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Object a = stack.pop();
        Object b = stack.pop();
        Object c = stack.pop();

        stack.push(b);
        stack.push(a);
        stack.push(c);
    }
}
