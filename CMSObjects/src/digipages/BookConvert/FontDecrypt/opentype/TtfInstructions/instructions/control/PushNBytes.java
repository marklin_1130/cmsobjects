

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class PushNBytes extends TtfInstruction {
    private byte numBytes;
    private byte[] bytes;

    public int[] getCodeRanges() {
        return new int[]{0x40};
    }

    public void read(FontDataInputStream in) throws IOException {
        numBytes = in.readByte();
        bytes = in.readBytes(numBytes);
    }

    public void execute(InstructionStack stack) throws IOException {
        // bytes or unsigned
        for (byte byteOn : bytes)
            stack.push((int) byteOn & 0xFF);
    }
}
