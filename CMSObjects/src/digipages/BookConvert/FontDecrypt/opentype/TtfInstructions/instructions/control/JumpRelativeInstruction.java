

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class JumpRelativeInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x1C};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Integer offset = (Integer) stack.pop();
        vm.jumpOffset = offset;
    }
}
