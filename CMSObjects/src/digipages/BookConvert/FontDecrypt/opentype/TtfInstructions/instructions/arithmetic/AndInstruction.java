

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class AndInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x5A};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Number e1 = stack.popNumber();
        Number e2 = stack.popNumber();

        // what is with the ttf spec with this AND result of a 32 uint??
        boolean result = e1.doubleValue() > 0 && e2.doubleValue() > 0;
        stack.push(boolToUint32(result));
    }
}
