

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class CIndexInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x25};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        // Spec: Copy the INDEXed element to the top of the stack
        Integer index = stack.popInt32();

        Object kthElement = stack.get(index);
        stack.push(kthElement);
    }
}
