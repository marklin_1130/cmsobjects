

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class PushBytes extends TtfInstruction {
    private int numBytes;
    private byte[] bytes;

    public int[] getCodeRanges() {
        return new int[]{0xB0, 0xB7};
    }

    public void read(FontDataInputStream in) throws IOException {
        numBytes = code - 0xB0 + 1;
        if (in.available() < numBytes)
            throw new IOException("Num PushBytes greater than available input stream data.");

        bytes = in.readBytes(numBytes);
    }

    public void execute(InstructionStack stack) throws IOException {
        for (byte byteOn : bytes)
            stack.push((int) byteOn & 0xFF);
    }
}
