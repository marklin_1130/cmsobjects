

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class AbsoluteValue extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x64};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        double n = stack.popF26Dot6();
        stack.push((float) Math.abs(n));
    }
}
