

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class AddInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x60};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        float n1 = stack.popF26Dot6();
        float n2 = stack.popF26Dot6();

        Float result = n1 + n2;
        stack.push(result);
    }
}
