

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeltaC3Instruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x75};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long numExceptionPairs = stack.popUint32();

        List<Long[]> cvtEntryPairs = new ArrayList<Long[]>();
        for (long i = 0; i < numExceptionPairs; i++) {
            Long cvtEntryNum = stack.popUint32();
            Long exceptionNum = stack.popUint32();

            cvtEntryPairs.add(new Long[]{cvtEntryNum, exceptionNum});
        }

        // todo should manipulate CVT table somehow after pops
        // Spec The DELTAC3[] instruction is exactly the same as the DELTAC1 instruction
        // except for operating at pixel per em sizes beginning with the
    }
}
