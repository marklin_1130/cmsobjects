

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class GetProjectionVectorInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x0C};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        // todo needs to graphical state handeling for projection vector
        Float x = 1f;
        Float y = 1f;

        stack.push(x);
        stack.push(y);
    }
}
