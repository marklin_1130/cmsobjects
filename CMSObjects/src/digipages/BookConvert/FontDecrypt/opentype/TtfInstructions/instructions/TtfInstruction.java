

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfInstructionVisitor;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfVirtualMachine;
import org.slf4j.Logger;

import java.io.IOException;

import static org.slf4j.LoggerFactory.getLogger;

public abstract class TtfInstruction {
    protected static final Logger log = getLogger(TtfInstruction.class);

    public int code;
    public TtfVirtualMachine vm;

    public abstract int[] getCodeRanges();

    public abstract void read(FontDataInputStream in) throws IOException;

    public abstract void execute(InstructionStack stack) throws IOException;

    public void accept(TtfInstructionVisitor visitor) throws IOException {
        visitor.visitGeneric(this);
    }

    public boolean doesMatch(int code) {
        int[] range = getCodeRanges();
        if (getCodeRanges().length == 1)
            return code == range[0];
        else
            return code >= range[0] && code <= range[1];
    }

    protected static Long boolToUint32(boolean value) {
        Long uIntResult = 0L;
        if (value)
            uIntResult = 1L;

        return uIntResult;
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}
