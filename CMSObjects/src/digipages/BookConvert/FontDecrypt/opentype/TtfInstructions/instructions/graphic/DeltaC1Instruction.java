

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeltaC1Instruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x73};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long numExceptionPairs = stack.popUint32();

        List<Long[]> cvtEntryPairs = new ArrayList<Long[]>();
        for (long i = 0; i < numExceptionPairs; i++) {
            Long cvtEntryNum = stack.popUint32();
            Long exceptionNum = stack.popUint32();

            cvtEntryPairs.add(new Long[]{cvtEntryNum, exceptionNum});
        }

        // todo should manipulate CVT table somehow after pops, for our current purposes dunno if that
        // actually needs to be run
    }
}
