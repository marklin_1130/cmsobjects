

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class InterpolatePointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x39};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long p1 = stack.popUint32();
        Long p2 = stack.popUint32();
        Long ploop = stack.popUint32();
        // todo graphical state
    }
}
