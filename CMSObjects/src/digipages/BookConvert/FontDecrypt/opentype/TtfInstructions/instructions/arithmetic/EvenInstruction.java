

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class EvenInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x57};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Float e1 = stack.popF26Dot6();

        boolean isEven = (e1 % 2) == 0;

        stack.push(boolToUint32(isEven));
    }
}
