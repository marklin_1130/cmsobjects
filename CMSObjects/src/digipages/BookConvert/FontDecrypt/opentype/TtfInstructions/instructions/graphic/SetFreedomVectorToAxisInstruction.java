

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetFreedomVectorToAxisInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x04, 0x05};
    }

    public boolean isXAxis = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x05)
            isXAxis = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        if (isXAxis)
            vm.getGraphicsState().freedomVector.x = 0;
        else
            vm.getGraphicsState().freedomVector.y = 0;
    }
}
