package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetReferencePoint2Instruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x12};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        vm.getGraphicsState().referencePoint2Id = stack.popUint32();
    }
}
