

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

import static digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.graphicsengine.RoundSettings.RoundState.*;

public class RoundDownToGridInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x7D};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        vm.getGraphicsState().roundState = ROUND_DOWN_TO_GRID;
    }
}
