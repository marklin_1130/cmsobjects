

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class NegateInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x65};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Float num = stack.popF26Dot6();
        stack.push(-num);
    }
}
