

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class IdefInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x89};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Number opCode = stack.popEint8();
    }
}
