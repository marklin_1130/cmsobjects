

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class ShiftZoneWithReferencePointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x36, 0x37};
    }

    public boolean useRp2 = true;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x37)
            useRp2 = false;
    }

    public void execute(InstructionStack stack) throws IOException {
        Long zoneId = stack.popUint32();
        // todo graphics state handeling
    }
}
