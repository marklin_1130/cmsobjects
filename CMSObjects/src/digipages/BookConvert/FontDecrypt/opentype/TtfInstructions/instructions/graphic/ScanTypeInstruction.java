

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

import static digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfGraphicsState.ScanConverterMode;

public class ScanTypeInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x8D};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        int type = stack.popNumber().intValue();
        vm.getGraphicsState().scanConverterMode = ScanConverterMode.fromValue(type);
    }
}
