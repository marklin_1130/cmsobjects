

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MultiplyInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x63};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        float n1 = stack.popF26Dot6();
        float n2 = stack.popF26Dot6();

        // Spec a little odd: The division takes place in the following fashion,
        // n1 is shifted left by six bits and then divided by 2.
        stack.push((float)((n1 * n2) / 64.0));
    }
}
