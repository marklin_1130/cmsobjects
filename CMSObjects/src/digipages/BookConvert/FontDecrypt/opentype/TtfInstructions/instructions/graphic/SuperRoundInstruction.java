

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.graphicsengine.RoundSettings;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SuperRoundInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x76};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long flags = stack.popUint32();

        vm.getGraphicsState().roundState = RoundSettings.RoundState.SUPER_ROUND;
        vm.getGraphicsState().roundSettings.updateForFlags(flags);
    }
}
