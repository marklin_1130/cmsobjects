

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MoveElementInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x26};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Integer index = stack.popInt32();

        Object kthElement = stack.get(index);
        stack.remove(kthElement);
        stack.push(kthElement);
    }
}
