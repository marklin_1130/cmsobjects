

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class LoopCallInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x2A};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Integer functionId = (Integer) stack.pop();
        Number loopCount = stack.popNumber();

        for (int i = 0; i < loopCount.longValue(); i++) {
            // todo call function from maxp table
        }
    }
}
