

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class ReadCvtEntryInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x45};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long index = stack.popUint32();

        // I have CVT table values as shorts but RCVT spec says to push a float. probabley
        // needs investigation or will be resolved with further ttf interpretor work
        Float cvtValue = Float.valueOf(vm.getGraphicsState().getCvtValue(index));

        stack.push(cvtValue);
    }
}
