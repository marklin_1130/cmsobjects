

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeltaP1Instruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x5D};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long numPairs = stack.popUint32();

        List<Long[]> pointExceptionPairs = new ArrayList<Long[]>();
        for (long i = 0; i < numPairs; i++) {
            Long pointNum = stack.popUint32();
            Long exceptionNum = stack.popUint32();

            pointExceptionPairs.add(new Long[]{pointNum, exceptionNum});
        }

        // todo should manipulate CVT table somehow after pops, for our current purposes dunno if that
        // actually needs to be run
    }
}
