

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class FlipPointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x80};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long p1 = stack.popUint32();
        Long p2 = stack.popUint32();
        Long ploopValue = stack.popUint32();
        // modifies graphical state stuff, not yet there with graphical state handeling
    }
}
