

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MoveStackIndirectRelPoint extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x3A, 0x3B};
    }

    boolean resetRp0= false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x3B)
            resetRp0 = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        Float distance = stack.popF26Dot6();
        Long pointId = stack.popUint32();

        // todo graphics state handeling
    }
}
