package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetProjectionVectorToAxisInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x02, 0x03};
    }

    public boolean isXAxis = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x03)
            isXAxis = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        if (isXAxis)
            vm.getGraphicsState().projectionVector.x = 0;
        else
            vm.getGraphicsState().projectionVector.y = 0;
    }
}
