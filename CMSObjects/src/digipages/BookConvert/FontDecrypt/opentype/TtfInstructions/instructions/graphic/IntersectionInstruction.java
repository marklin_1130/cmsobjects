

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class IntersectionInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x0F};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long line1Start = stack.popUint32();
        Long line1End = stack.popUint32();
        Long line2Start = stack.popUint32();
        Long line2End = stack.popUint32();

        Long movePoint = stack.popUint32();
        // todo graphical state
    }
}
