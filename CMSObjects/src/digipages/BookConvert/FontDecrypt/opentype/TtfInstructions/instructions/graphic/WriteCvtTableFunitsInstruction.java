

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class WriteCvtTableFunitsInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x70};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long num = stack.popUint32();
        Long index = stack.popUint32();

        vm.getGraphicsState().cvtValues.set(index.intValue(), num.shortValue());
    }
}
