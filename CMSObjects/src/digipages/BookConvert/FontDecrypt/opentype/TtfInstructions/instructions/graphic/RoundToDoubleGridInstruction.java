

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.graphicsengine.RoundSettings;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class RoundToDoubleGridInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x3D};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        vm.getGraphicsState().roundState = RoundSettings.RoundState.ROUND_TO_DOUBLE_GRID;
    }
}
