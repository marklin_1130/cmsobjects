

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class PushWords extends TtfInstruction {
    private short[] words;

    public int[] getCodeRanges() {
        return new int[]{0xB8, 0xBF};
    }

    public void read(FontDataInputStream in) throws IOException {
        int numWords = code - 0xB8 + 1;
        words = new short[numWords];

        for (int i = 0; i < numWords; i++)
            words[i] = in.readShort();
    }

    public void execute(InstructionStack stack) throws IOException {
        for (short wordOn : words)
            stack.push((int) wordOn);
    }
}
