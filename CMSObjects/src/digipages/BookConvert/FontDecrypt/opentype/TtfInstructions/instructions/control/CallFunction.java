

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfFunction;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class CallFunction extends TtfInstruction {
    private int functionId;

    public int[] getCodeRanges() {
        return new int[]{0x2B};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        functionId = stack.popNumber().intValue();
        TtfFunction func = vm.getFunction(functionId);
        vm.execute(func.getInstructions());
    }

    public String toString() {
        return super.toString() + " Function ID: " + functionId;
    }
}
