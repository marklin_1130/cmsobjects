

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MeasurePointSizeInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x4C};
    }

    boolean gridFittedOutline = true;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x4A)
            gridFittedOutline = false;

    }

    public void execute(InstructionStack stack) throws IOException {
        Short pointSize = 48;
        stack.push(pointSize);
    }
}
