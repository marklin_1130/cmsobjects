

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class GreaterThanInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x52};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Number e2 = stack.popNumber();
        Number e1 = stack.popNumber();
        boolean result = e1.doubleValue() > e2.doubleValue();

        stack.push(boolToUint32(result));
    }
}
