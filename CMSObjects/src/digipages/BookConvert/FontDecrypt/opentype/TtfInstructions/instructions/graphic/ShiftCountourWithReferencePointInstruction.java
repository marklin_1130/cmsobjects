package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class ShiftCountourWithReferencePointInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x34, 0x35};
    }

    public boolean useRp2 = true;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x35)
            useRp2 = false;
    }

    public void execute(InstructionStack stack) throws IOException {
        Long contourId = stack.popUint32();
        // todo graphics state handeling
    }
}
