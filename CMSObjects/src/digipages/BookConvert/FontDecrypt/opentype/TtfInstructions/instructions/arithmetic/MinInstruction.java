

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class MinInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x8C};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Number e2 = stack.popNumber();
        Number e1 = stack.popNumber();
        Number result = Math.min(e2.doubleValue(), e1.doubleValue());

        stack.push(result);
    }
}
