

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class DuplicateInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x20};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        if (stack.size() == 0)
            return;

        Object e = stack.pop();

        stack.push(e);
        stack.push(e);
    }
}
