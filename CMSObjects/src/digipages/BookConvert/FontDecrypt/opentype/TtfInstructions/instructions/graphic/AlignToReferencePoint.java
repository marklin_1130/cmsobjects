

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class AlignToReferencePoint extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x3C};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        long point1Id = stack.popUint32();
        long point2Id = stack.popUint32();
        long ploopValue = stack.popUint32();
    }
}
