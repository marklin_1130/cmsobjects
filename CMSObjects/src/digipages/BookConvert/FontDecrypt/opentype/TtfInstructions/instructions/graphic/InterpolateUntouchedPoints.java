

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class InterpolateUntouchedPoints extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x30, 0x31};
    }

    boolean interpolateInYDirection = true;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x31)
            interpolateInYDirection = false;
    }

    public void execute(InstructionStack stack) throws IOException {
        // todo graphical state
    }
}
