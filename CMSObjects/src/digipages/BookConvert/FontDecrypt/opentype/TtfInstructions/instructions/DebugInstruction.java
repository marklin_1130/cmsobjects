

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;

import java.io.IOException;

public class DebugInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x4F};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long n = stack.popUint32();
        // todo, in debugger versions of vm should invoke the font developers debugger not sure
        // how to do this
    }
}
