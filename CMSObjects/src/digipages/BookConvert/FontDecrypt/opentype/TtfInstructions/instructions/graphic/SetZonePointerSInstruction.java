package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfVirtualMachine.TtfVmRuntimeException;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetZonePointerSInstruction extends TtfInstruction {

    public int[] getCodeRanges() {
        return new int[]{0x16};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Number zoneObj = stack.popNumber();
        if (!(zoneObj instanceof Long))
            log.warn("SetZonePointer Expected Uint32 but was int");
        Long zone = zoneObj.longValue();

        if (zone > 1)
            throw new TtfVmRuntimeException(
                    "SetZonePointerS popped zone number must 0 (twilight zone) or 1 (glyph zone)");

        vm.getGraphicsState().zone0Id = zone;
        vm.getGraphicsState().zone1Id = zone;
        vm.getGraphicsState().zone2Id = zone;
    }
}
