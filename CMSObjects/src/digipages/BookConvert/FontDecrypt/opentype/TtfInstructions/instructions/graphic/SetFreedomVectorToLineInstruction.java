

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetFreedomVectorToLineInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x08, 0x09};
    }

    boolean isSetPerpendicularToLine = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x09)
            isSetPerpendicularToLine = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        Long pointId2 = stack.popUint32();
        Long pointId1 = stack.popUint32();
        // todo graphics state handeling
    }
}
