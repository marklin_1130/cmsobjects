

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class WriteCvtTablePixelUnitsInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x44};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        float num = stack.popF26Dot6();
        Long index = stack.popUint32();

        // todo figure out type conversion to pixel units!
        vm.getGraphicsState().cvtValues.set(index.intValue(), (short) num);
    }
}
