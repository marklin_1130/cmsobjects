

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfGraphicsState;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class FreedomAndProjectToAxisInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x00, 0x01};
    }

    public boolean isXAxis = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x01)
            isXAxis = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        TtfGraphicsState state = vm.getGraphicsState();
        if (isXAxis)
            state.projectionVector.x = state.freedomVector.x = 0;
        else
            state.projectionVector.y = state.freedomVector.y = 0;
    }
}
