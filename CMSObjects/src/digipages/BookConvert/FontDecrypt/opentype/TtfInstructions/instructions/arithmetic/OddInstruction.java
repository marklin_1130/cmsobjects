

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.arithmetic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class OddInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x56};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Float e1 = stack.popF26Dot6();
        boolean isOdd = (e1 % 2) == 1;

        stack.push(boolToUint32(isOdd));
    }
}
