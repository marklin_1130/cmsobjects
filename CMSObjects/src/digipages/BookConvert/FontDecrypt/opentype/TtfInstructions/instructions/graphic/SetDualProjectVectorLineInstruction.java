

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetDualProjectVectorLineInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x86, 0x87};
    }

    private boolean isPerpendicularToLine = false;

    public void read(FontDataInputStream in) throws IOException {
        if (code == 0x87)
            isPerpendicularToLine = true;
    }

    public void execute(InstructionStack stack) throws IOException {
        Long point2Id = stack.popUint32();
        Long point1Id = stack.popUint32();

        // todo graphics state handeling
    }
}
