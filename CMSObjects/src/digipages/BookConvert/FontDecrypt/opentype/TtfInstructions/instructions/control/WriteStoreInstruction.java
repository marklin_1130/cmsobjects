

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.control;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class WriteStoreInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x42};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Long value = stack.popUint32();
        Long index = stack.popUint32();

        vm.setStorageAreaValue(index, value);
    }
}
