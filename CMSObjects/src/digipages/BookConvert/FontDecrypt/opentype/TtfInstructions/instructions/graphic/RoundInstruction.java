

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class RoundInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x68, 0x6B};
    }

    private short engineType = 0;

    public void read(FontDataInputStream in) throws IOException {
        engineType = (short) (code - 0x6C);
    }

    public void execute(InstructionStack stack) throws IOException {
        Float pixelCoordinate = stack.popF26Dot6();
        pixelCoordinate = vm.getGraphicsState().round(pixelCoordinate);

        stack.push(pixelCoordinate);
    }
}
