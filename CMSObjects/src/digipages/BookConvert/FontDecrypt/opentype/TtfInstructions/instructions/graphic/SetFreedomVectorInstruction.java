

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;

public class SetFreedomVectorInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x0B};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        Float y = stack.popF26Dot6();
        Float x = stack.popF26Dot6();

        vm.getGraphicsState().freedomVector.x = x;
        vm.getGraphicsState().freedomVector.y = y;
    }
}
