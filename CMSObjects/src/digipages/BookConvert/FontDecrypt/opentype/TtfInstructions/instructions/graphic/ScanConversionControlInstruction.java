

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.graphic;

import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.InstructionStack;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfGraphicsState.ScanDropoutMode;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;
import java.util.List;

import static digipages.BookConvert.FontDecrypt.FontVerterUtils.isBitSet;
import static digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfGraphicsState.ScanDropoutMode.*;

public class ScanConversionControlInstruction extends TtfInstruction {
    public int[] getCodeRanges() {
        return new int[]{0x85};
    }

    public void read(FontDataInputStream in) throws IOException {
    }

    public void execute(InstructionStack stack) throws IOException {
        long flags = stack.popNumber().longValue();

        List<ScanDropoutMode> modes = vm.getGraphicsState().dropoutControlModes;
        modes.clear();

        if (isBitSet(8, flags))
            modes.add(TRUE_IF_PPEM_LESS_THAN_THRESHOLD);
        if (isBitSet(9, flags))
            modes.add(TRUE_IF_GLYPH_IS_ROTATED);
        if (isBitSet(10, flags))
            modes.add(TRUE_IF_GLYPH_STRETCHED);

        if (isBitSet(11, flags))
            modes.add(FALSE_UNLESS_PPEM_LESS_THAN_THRESHOLD);
        if (isBitSet(12, flags))
            modes.add(FALSE_UNLESS_STRETCHED);
        if (isBitSet(13, flags))
            modes.add(FALSE_UNLESS_STRETCHED);

        vm.getGraphicsState().droputThreshold = (flags & 0xFF);
    }
}
