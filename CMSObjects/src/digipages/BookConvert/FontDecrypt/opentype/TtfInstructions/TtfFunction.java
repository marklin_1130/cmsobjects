

package digipages.BookConvert.FontDecrypt.opentype.TtfInstructions;

import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.util.ArrayList;
import java.util.List;

public class TtfFunction {
    private List<TtfInstruction> instructions = new ArrayList<TtfInstruction>();

    public List<TtfInstruction> getInstructions() {
        return instructions;
    }

    public void addInstruction(TtfInstruction instruction) {
        instructions.add(instruction);
    }
}
