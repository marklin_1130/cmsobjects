
package digipages.BookConvert.FontDecrypt.opentype;

import digipages.BookConvert.FontDecrypt.io.FontDataInput;
import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.io.FontDataOutputStream;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

public class GlyphTable extends OpenTypeTable {
    private static final Logger log = getLogger(GlyphTable.class);

    List<TtfGlyph> glyphs = new LinkedList<TtfGlyph>();

    public String getTableType() {
        return "glyf";
    }

    protected byte[] generateUnpaddedData() throws IOException {
        FontDataOutputStream out = new FontDataOutputStream();
        for (TtfGlyph glyphOn : glyphs) {
            if (!glyphOn.isEmpty())
                out.write(glyphOn.generateData());
        }

        return out.toByteArray();
    }

    public void readData(byte[] data) throws IOException {
        super.readData(data);
        FontDataInput reader = new FontDataInputStream(data);
        Long[] offsets = font.getLocaTable().getOffsets();

        for (int i = 0; i < offsets.length - 1; i++) {
            Long offset = offsets[i];
            long length = offsets[i + 1] - offset;

            // 0 length is valid and means an empty outline for glyph
            if (length == 0) {
                glyphs.add(new TtfGlyph());
                continue;
            }

            if (offset >= data.length) {
                log.error("Invalid loca table offset, offset greater than glyf table length");
                continue;
            }

            try {
                reader.seek(offset.intValue());
                byte[] glyphData = reader.readBytes((int) length);

                TtfGlyph glyph = TtfGlyph.parse(glyphData, font);
                glyphs.add(glyph);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<TtfGlyph> getGlyphs() {
        return glyphs;
    }

    public List<TtfGlyph> getNonEmptyGlyphs() {
        List<TtfGlyph> nonEmpty = new LinkedList<TtfGlyph>();

        for (TtfGlyph glyphOn : glyphs)
            if (!glyphOn.isEmpty())
                nonEmpty.add(glyphOn);

        return nonEmpty;
    }

}
