package digipages.BookConvert.FontDecrypt.opentype;

import digipages.BookConvert.FontDecrypt.io.DataTypeSerializerException;
import digipages.BookConvert.FontDecrypt.io.DataTypeProperty;
import digipages.BookConvert.FontDecrypt.io.DataTypeBindingSerializer;

import java.nio.charset.Charset;

public class NameRecord {
    static final int NAME_RECORD_SIZE = 12;

    @DataTypeProperty(dataType = DataTypeProperty.DataType.USHORT, order = 0)
    int platformID;

    @DataTypeProperty(dataType = DataTypeProperty.DataType.USHORT, order = 1)
    int encodingID;

    @DataTypeProperty(dataType = DataTypeProperty.DataType.USHORT, order = 2)
    int languageID;

    @DataTypeProperty(dataType = DataTypeProperty.DataType.USHORT, order = 3)
    int nameID;

    @DataTypeProperty(dataType = DataTypeProperty.DataType.USHORT, order = 4)
    int length;

    @DataTypeProperty(dataType = DataTypeProperty.DataType.USHORT, order = 5)
    int offset;

    private String string;

    public static NameRecord createWindowsRecord(String name, OtfNameConstants.RecordType type, OtfNameConstants.Language language) {
        NameRecord record = new NameRecord(name);
        record.setNameID(type.getValue());
        record.platformID = OtfNameConstants.WINDOWS_PLATFORM_ID;
        record.encodingID = OtfNameConstants.WINDOWS_DEFAULT_ENCODING;
        record.languageID = language.getValue();

        return record;
    }

    public static NameRecord createMacRecord(String name, OtfNameConstants.RecordType type, OtfNameConstants.Language language) {
        NameRecord record = new NameRecord(name);
        record.setNameID(type.getValue());
        record.platformID = OtfNameConstants.MAC_PLATFORM_ID;
        record.encodingID = OtfNameConstants.MAC_DEFAULT_ENCODING;
        record.languageID = 0;

        return record;
    }

    private NameRecord(String name) {
        string = name;
    }

    public NameRecord() {
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getNameID() {
        return nameID;
    }

    public void setNameID(int nameID) {
        this.nameID = nameID;
    }

    public byte[] getStringData() {
        return string.getBytes(getEncoding());
    }

    public String getRawString() {
        return string;
    }

    private Charset getEncoding() {
        if (platformID == OtfNameConstants.WINDOWS_PLATFORM_ID)
            return Charset.forName("UTF-16");
        return Charset.forName("ISO_8859_1");
    }

    public void setStringData(String stringData) {
        this.string = stringData;
    }

    public byte[] getRecordData() throws DataTypeSerializerException {
        length = getStringData().length;
        DataTypeBindingSerializer serializer = new DataTypeBindingSerializer();
        return serializer.serialize(this);
    }
}
