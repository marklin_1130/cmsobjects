
package digipages.BookConvert.FontDecrypt.opentype;

import digipages.BookConvert.FontDecrypt.cff.CffFontAdapter;
import digipages.BookConvert.FontDecrypt.cff.CffFontAdapter.CffGlyph;
import digipages.BookConvert.FontDecrypt.io.FontDataInputStream;
import digipages.BookConvert.FontDecrypt.io.FontDataOutputStream;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

public class HorizontalMetricsTable extends OpenTypeTable {
    private static final Logger log = getLogger(HorizontalMetricsTable.class);
    private int[] advanceWidths;

    private short[] leftSideBearings;
    private Short[] nonHorizontalLeftSideBearing;

    public String getTableType() {
        return "hmtx";
    }

    public void readData(byte[] data) throws IOException {
        FontDataInputStream reader = new FontDataInputStream(data);

        int numHMetrics = font.getHhea().numberOfHMetrics;
        advanceWidths = new int[numHMetrics];
        leftSideBearings = new short[numHMetrics];

        for (int i = 0; i < numHMetrics; i++) {
            advanceWidths[i] = reader.readUnsignedShort();
            leftSideBearings[i] = reader.readShort();
        }

        LinkedList<Short> nonHorzBearings = new LinkedList<Short>();
        while (reader.available() >= 2)
            nonHorzBearings.add(reader.readShort());

        nonHorizontalLeftSideBearing = nonHorzBearings.toArray(new Short[nonHorzBearings.size()]);
    }

    protected byte[] generateUnpaddedData() throws IOException {
        FontDataOutputStream writer = new FontDataOutputStream(FontDataOutputStream.OPEN_TYPE_CHARSET);

        for (int i = 0; i < advanceWidths.length; i++) {
            writer.writeUnsignedShort(advanceWidths[i]);
            writer.writeShort(leftSideBearings[i]);
        }
        for (Short bearingOn : nonHorizontalLeftSideBearing)
            writer.writeUnsignedShort(bearingOn);

        return writer.toByteArray();
    }

    public static HorizontalMetricsTable createDefaultTable(OpenTypeFont font) {
        HorizontalMetricsTable table = new HorizontalMetricsTable();
        table.font = font;

        table.nonHorizontalLeftSideBearing = new Short[]{};
        table.leftSideBearings = new short[]{};
        table.advanceWidths = new int[]{};

        return table;
    }


    void normalize() throws IOException {
        if (advanceWidths == null) {
            leftSideBearings = new short[]{0};
            advanceWidths = new int[]{1000};
        }

        // todo for ttf type
        if (font.isCffType()) {
            CffFontAdapter cff = font.getCffTable().getCffFont();
            List<CffGlyph> glyphs = cff.getGlyphs();

            // must start with the .notdef entry otherwise removed
            if (glyphs.get(0).getLeftSideBearing() != 0)
                glyphs.add(0, cff.createGlyph());

            advanceWidths = new int[glyphs.size()];
            leftSideBearings = new short[glyphs.size()];

            for (int i = 0; i < glyphs.size(); i++) {
                CffGlyph glyphOn = glyphs.get(i);
                advanceWidths[i] = glyphOn.getAdvanceWidth();
                leftSideBearings[i] = (short) glyphOn.getLeftSideBearing();
            }
        }

        if (font.getCmap() != null)
            loadMetrics();
    }

    private void loadMetrics() {
        if (isFromParsedFont)
            return;

        int lsbArrCount = font.getCmap().getGlyphCount() - advanceWidths.length;
        if (lsbArrCount > 0) {
            nonHorizontalLeftSideBearing = new Short[lsbArrCount];
            for (int i = 0; i < lsbArrCount; i++)
                nonHorizontalLeftSideBearing[i] = 1;
        } else
            nonHorizontalLeftSideBearing = new Short[]{};
    }

    public int[] getAdvanceWidths() {
        return advanceWidths;
    }

    public short[] getLeftSideBearings() {
        return leftSideBearings;
    }
}
