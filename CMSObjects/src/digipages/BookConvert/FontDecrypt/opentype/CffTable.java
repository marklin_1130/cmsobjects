
package digipages.BookConvert.FontDecrypt.opentype;

import digipages.BookConvert.FontDecrypt.cff.CffFontAdapter;

import java.io.IOException;
import java.util.List;

public class CffTable extends OpenTypeTable {
    private byte[] data;
    private CffFontAdapter cff;

    public CffTable(byte[] data) throws IOException {
        readData(data);
    }

    public CffTable() {
    }

    protected byte[] generateUnpaddedData() {
        return data;
    }

    public String getTableType() {
        return "CFF ";
    }

    public void readData(byte[] data) throws IOException {
        this.data = data;
        cff = new CffFontAdapter();
        cff.read(data);
    }

    public List<CffFontAdapter.CffGlyph> getGlyphs() throws IOException {
        return getCffFont().getGlyphs();
    }

    public CffFontAdapter getCffFont() {
        return cff;
    }
}
