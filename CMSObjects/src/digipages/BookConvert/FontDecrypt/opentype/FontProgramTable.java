
package digipages.BookConvert.FontDecrypt.opentype;

import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.TtfInstructionParser;
import digipages.BookConvert.FontDecrypt.opentype.TtfInstructions.instructions.TtfInstruction;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class FontProgramTable extends OpenTypeTable {
    private byte[] rawInstructions = new byte[0];
    private List<TtfInstruction> instructions = new LinkedList<TtfInstruction>();

    public String getTableType() {
        return "fpgm";
    }

    public void readData(byte[] data) throws IOException {
        rawInstructions = data;

        try {
            TtfInstructionParser parser = new TtfInstructionParser();
            instructions = parser.parse(rawInstructions);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    protected byte[] generateUnpaddedData() throws IOException {
        return rawInstructions;
    }

    public List<TtfInstruction> getInstructions() {
        return instructions;
    }
}
