package digipages.BookConvert.FontDecrypt.validator;

public class FontValidationException extends Exception {
    public FontValidationException(String message) {
        super(message);
    }
}