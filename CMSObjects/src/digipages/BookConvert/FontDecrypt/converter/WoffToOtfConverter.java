package digipages.BookConvert.FontDecrypt.converter;

import org.apache.commons.lang3.StringUtils;

import digipages.BookConvert.FontDecrypt.FVFont;
import digipages.BookConvert.FontDecrypt.opentype.OpenTypeFont;
import digipages.BookConvert.FontDecrypt.opentype.OpenTypeTable;
import digipages.BookConvert.FontDecrypt.woff.WoffFont;
import digipages.BookConvert.FontDecrypt.woff.WoffTable;

import java.io.IOException;

public class WoffToOtfConverter implements FontConverter {
    OpenTypeFont otfFont;
    WoffFont woffFont;

    public FVFont convertFont(FVFont font) throws IOException {
        this.woffFont = (WoffFont) font;

        otfFont = new OpenTypeFont();
        try {
            readTables();
        } catch (Exception e) {
            throw new IOException(e);
        }
        otfFont.finalizeFont();

        return otfFont;
    }

    private void readTables() throws IOException, InstantiationException, IllegalAccessException {
        for (WoffTable tableOn : woffFont.getTables()) {
            OpenTypeTable.OtfTableRecord record = new OpenTypeTable.OtfTableRecord();

            record.recordName = tableOn.getTag();
            if (record.recordName.length() < 4)
                record.recordName = record.recordName + StringUtils.repeat(" ", 4 - record.recordName.length());
            record.originalData = tableOn.getTableData();

            OpenTypeTable table = OpenTypeTable.createFromRecord(record, otfFont);
            table.isFromParsedFont = true;

            otfFont.addTable(table);
        }

        // have to order by dependant tables before doing table reads
        otfFont.orderTablesByDependencies();
        for (OpenTypeTable tableOn : otfFont.getTables())
            tableOn.readData(tableOn.record.originalData);

    }
}
