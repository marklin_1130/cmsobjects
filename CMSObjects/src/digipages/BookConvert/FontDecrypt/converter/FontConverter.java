package digipages.BookConvert.FontDecrypt.converter;

import digipages.BookConvert.FontDecrypt.FVFont;

import java.io.IOException;

public interface FontConverter {
    FVFont convertFont(FVFont font) throws IOException;
}