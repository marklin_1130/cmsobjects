package digipages.BookConvert.FontDecrypt.converter;

import digipages.BookConvert.FontDecrypt.FVFont;
import digipages.BookConvert.FontDecrypt.eot.EotFont;

import java.io.IOException;

public class EotToOpenTypeConverter implements FontConverter {
    public FVFont convertFont(FVFont font) throws IOException {
        EotFont eotFont = (EotFont) font;
        return eotFont.getEmbeddedFont();
    }
}