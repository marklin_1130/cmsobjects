package digipages.BookConvert.FontDecrypt.converter;

import digipages.BookConvert.FontDecrypt.FVFont;
import digipages.BookConvert.FontDecrypt.eot.EotFont;
import digipages.BookConvert.FontDecrypt.opentype.OpenTypeFont;

import java.io.IOException;

public class OpenTypeToEotConverter implements FontConverter {
    public FVFont convertFont(FVFont font) throws IOException {
        OpenTypeFont otf = (OpenTypeFont) font;
        EotFont eotFont = new EotFont();

        eotFont.setFont(otf);
        return eotFont;
    }
}
