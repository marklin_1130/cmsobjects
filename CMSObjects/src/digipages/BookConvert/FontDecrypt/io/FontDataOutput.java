package digipages.BookConvert.FontDecrypt.io;

import java.io.Closeable;
import java.io.DataOutput;
import java.io.Flushable;
import java.io.IOException;

public interface FontDataOutput extends DataOutput, Flushable, Closeable {
    byte[] toByteArray();

    void writeString(String string) throws IOException;

    void writeUnsignedShort(int num) throws IOException;

    void writeUnsignedInt(int num) throws IOException;

    void writeUnsignedInt8(int num) throws IOException;

    void write32Fixed(float num) throws IOException;

    int currentPosition();
}