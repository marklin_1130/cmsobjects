package digipages.BookConvert.FontDecrypt.io;

import java.io.DataInput;
import java.io.IOException;

public interface FontDataInput extends DataInput {
    long readUnsignedInt() throws IOException;

    String readString(int length) throws IOException;

    byte[] readBytes(int length) throws IOException;

    void seek(int offset);

    // converted from pseduo C like reader code from woff spec
    int readUIntBase128() throws IOException;

    float readFixed32() throws IOException;

    int getPosition();

    int[] readSplitBits(int numUpperBits) throws IOException;

    int[] readUnsignedShortArray(int length) throws IOException;
}