package digipages.BookConvert.CoverProcessing;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.io.FileUtils;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.GetPropertyValues;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.ResponseData;
import digipages.BookConvert.utility.Utility;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class CoverProcessingHandler implements CommonJobHandlerInterface {
    private String trialBucket;
    private String URL_CoverProcessResult;
    private AmazonS3 s3Client;
    private static DPLogger logger = DPLogger.getLogger(CoverProcessingHandler.class.getName());
    		
    
	public CoverProcessingHandler() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
	       String Tag = "[CoverProcessing]";

	        Long StartTime = System.currentTimeMillis();
	        
	        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
	        logger.info(Tag + "Start CoverProcessing at " + sdFormat.format(new Date()));
	        
	        try {
	            CommonUtil.delDirs("/tmp/");
	            
	            // Get config
	            GetPropertyValues properties = new GetPropertyValues();
	            trialBucket = properties.getPropValues("trialBucket");
	            URL_CoverProcessResult = properties.getPropValues("URL_CoverProcessResult");

	            // Create S3 Client & SNS Client
	            this.s3Client = AmazonS3ClientBuilder.defaultClient();
	            
	            // Get RequestData from message
	            Gson gson = new GsonBuilder().create();
	            DataMessage dataMessage = job.getJobParam(DataMessage.class);
	            RequestData requestData = gson.fromJson(dataMessage.getRequestData(), RequestData.class);
	            
	            /*[+] Cover resize */
	            final float MAX_WIDTH = 414;
	            final float MAX_HEIGHT = 554;
	            String JPG_TYPE = "jpg";
	            String JPG_MIME = "image/jpeg";
	            
	            String hashCode = Utility.encrypt(requestData.getItem() + requestData.getRequest_time());        
	            String folderPath = "cover/" + hashCode.substring(hashCode.length() - 6) + "/" +
	                    requestData.getBook_file_id(); // {hash_code}/{book_file_id}
	            String downloadUrl = requestData.getEfile_cover_url();
	            
	            // Download file from URL
	            URL source = new URL(downloadUrl);
	            String fileName = CommonUtil.extractFileNameFromURL(source);
	            File downloadFile = new File("/tmp/" + fileName);
	            
	            // append random number in tail for no-cache
	            source = new URL(downloadUrl + "?rand=" + UUID.randomUUID().toString());
	            CommonUtil.downloadURLToFileNoCache(source,downloadFile);

	            // Read the source image
	            BufferedImage srcImage = ImageIO.read(downloadFile);
	            int srcHeight = srcImage.getHeight();
	            int srcWidth = srcImage.getWidth();
	            
	            // Infer the scaling factor to avoid stretching the image unnaturally
	            float scalingFactor = Math.min(MAX_WIDTH / srcWidth, MAX_HEIGHT / srcHeight);
	            int width = (int) (scalingFactor * srcWidth);
	            int height = (int) (scalingFactor * srcHeight);
	            
	            BufferedImage resizedImage = new BufferedImage(width, height,
	                    BufferedImage.TYPE_INT_RGB);
	            Graphics2D g = resizedImage.createGraphics();
	            
	            // Fill with white before applying semi-transparent (alpha) images
	            g.setPaint(Color.white);
	            g.fillRect(0, 0, width, height);
	            // Simple bilinear resize
	            // If you want higher quality algorithms, check this link:
	            // https://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
	            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	            g.drawImage(srcImage, 0, 0, width, height, null);
	            g.dispose();
	            
	            // Re-encode image to target format
	            ByteArrayOutputStream os = new ByteArrayOutputStream();
	            ImageIO.write(resizedImage, JPG_TYPE, os);
	            InputStream is = new ByteArrayInputStream(os.toByteArray());
	            // Set Content-Length and Content-Type
	            ObjectMetadata meta = new ObjectMetadata();
	            meta.setContentLength(os.size());
	            meta.setContentType(JPG_MIME);
	            
	            // Uploading to S3
	            logger.info(Tag + "Uploading to Bucket:" + trialBucket + ", Path:" + folderPath + "/" + fileName);
	            s3Client.putObject(new PutObjectRequest(trialBucket, folderPath + "/" + fileName, is, meta)
	                    .withCannedAcl(CannedAccessControlList.PublicRead));

	            
	            is.close();
	            os.close();
	            downloadFile.delete();
	            /*[-] Cover resize */
	            
	            /*[+] Notify CoverProcessResult API  */
	            ResponseData responseData = new ResponseData();            
	            responseData.setItem(requestData.getItem());
	            responseData.setFile_cover_url(folderPath.replace("cover/", "") + "/" + fileName);
	            responseData.setProcessing_id(requestData.getBook_file_id());
	            logger.info(Tag + "responseData:" + gson.toJson(responseData));
	            logger.info(Tag + "Sending 'POST' request to URL:" + URL_CoverProcessResult);
	            
	            URL obj = new URL(URL_CoverProcessResult);            
	            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

	            //add reuqest header
	            connection.setRequestMethod("POST");
	            connection.setRequestProperty("Content-Type", "application/json");

	            String urlParameters = gson.toJson(responseData);
	            
	            // Send post request
	            connection.setDoOutput(true);
	            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
	            wr.writeBytes(urlParameters);
	            wr.flush();
	            wr.close();

	            int responseCode = connection.getResponseCode();
	            logger.info(Tag + "Response Code:" + responseCode);

	            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	            StringBuffer response = new StringBuffer();

	            int BUFFER_SIZE=1024;
	            char[] buffer = new char[BUFFER_SIZE]; // or some other size, 
	            int charsRead = 0;
	            while ( (charsRead  = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
	                response.append(buffer, 0, charsRead);
	            }
	            
	            logger.info(Tag + "response:" + response.toString());
	            
	            in.close();
	            connection.disconnect();
	            /*[-] Notify CoverProcessResult API  */
	            
	        } catch(Exception e) {
	        	logger.error("Unexcepted exception handling cover",e);
	        }
	        
	        logger.info(Tag + "End CoverProcessing spend " + (System.currentTimeMillis() - StartTime) + "ms, at " + sdFormat.format(new Date()));        
	        return "Cover Processing Done";        
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// no needed to use database
	}
	/**
	 * default is Ec2, no other possible.
	 * @param runMode
	 * @return
	 */
	public static RunMode getRetryRunMode(RunMode runMode) {
		if (runMode==RunMode.LambdaMode)
		{
			return RunMode.ExtEc2Mode;
		}
		return RunMode.ExtEc2Mode;
	}


	
	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LambdaMode;
	}
}
