package digipages.BookConvert.FlattenPDF;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

public class PageDimension {

	private static PageRangeDector pagerangeDector = new PageRangeDector();

	private PDPage myPage;
	private PDRectangle rectangle;
	private int bigWidth ;
	private int bigHeight ;
	private int smallWidth;
	private int smallHeight;
	private int smallX ;
	private int smallY ;
	/**
	 * 
	 * @param page
	 * @param resFactor
	 */
	public PageDimension(PDPage page, double resFactor) {
		this.myPage=page;
		
		// this is outline
		
		// adjust to ipad2 resolution if too low ?!
//		PDRectangle target=findMaxInnerBox(page);
		PDRectangle target=page.getMediaBox();
		this.rectangle=target;
		double newResFactor=adjustResfactor(page.getCropBox(),resFactor);
		bigWidth = (int) Math.round(target.getWidth()*newResFactor); 
		bigHeight = (int) Math.round(target.getHeight()*newResFactor);
		
		//移除該設定這只針對GS作調整，一般會使用IM，對超過2048得圖片會影響品質而模糊
        // 如果width比height高,產升圖片會自動旋轉
        // ipad2 res = 1536x2048
//        if(bigHeight > 2048) {
//            bigHeight = 2048;
//        }
//        if (bigWidth >= bigHeight) {
//            bigWidth = (bigHeight/4)*3;
//        }
//        if (bigWidth >= 1536) {
//            bigWidth = 1536;
//        }
//		if (isFull){
//			target =page.getMediaBox();
//			target.setLowerLeftX(0f);
//			target.setLowerLeftY(0f);
//			System.out.println("Eric Debug Using MediaBox" + dumpDimension(target));
//		}else
//		{
//			System.out.println("Eric Debug Using InnerBox");
//		}
		target = page.getCropBox();
		// this is the inner printable region.
		smallWidth = (int) Math.round(target.getWidth()*newResFactor);
		smallHeight = (int) Math.round(target.getHeight()*newResFactor);
//        if (smallWidth >= smallHeight) {
//            smallWidth = (smallHeight/4)*3;
//        }
//        if (smallWidth >= 1536) {
//            smallWidth = 1536;
//        }
		smallX = (int) Math.round(target.getLowerLeftX()*newResFactor);
		// smallY is from top, but pdf is from bottom
		smallY = (int) Math.round(bigHeight-smallHeight-target.getLowerLeftY()*newResFactor);
	}
	
	public PageDimension(File bkgroundFile, float resFactor) {
		try {
		PDDocument doc = PDDocument.load(bkgroundFile);
		
		this.myPage=doc.getPage(0);
		
		// this is outline
		
		// adjust to ipad2 resolution if too low ?!
		PDRectangle target=myPage.getMediaBox();
		this.rectangle=target;
		double newResFactor=adjustResfactor(myPage.getCropBox(),resFactor);
		bigWidth = (int) Math.round(target.getWidth()*newResFactor); 
		bigHeight = (int) Math.round(target.getHeight()*newResFactor);
		
//		if (isFull){
//			target =page.getMediaBox();
//			target.setLowerLeftX(0f);
//			target.setLowerLeftY(0f);
//			System.out.println("Eric Debug Using MediaBox" + dumpDimension(target));
//		}else
//		{
//			System.out.println("Eric Debug Using InnerBox");
//		}
		target = myPage.getCropBox();
		// this is the inner printable region.
		smallWidth = (int) Math.round(target.getWidth()*newResFactor);
		smallHeight = (int) Math.round(target.getHeight()*newResFactor);
		smallX = (int) Math.round(target.getLowerLeftX()*newResFactor);
		// smallY is from top, but pdf is from bottom
		smallY = (int) Math.round(bigHeight-smallHeight-target.getLowerLeftY()*newResFactor);
		doc.close();
		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public PDRectangle getRectanble()
	{
		return rectangle;
	}
	
	
	private PDRectangle findMaxInnerBoxOld(PDPage page) {
		List <PDRectangle> dimensions = new ArrayList<>();
		dimensions.add(page.getArtBox());
		dimensions.add(page.getBBox());
		dimensions.add(page.getBleedBox());
		dimensions.add(page.getCropBox());
		dimensions.add(page.getTrimBox());
		PDRectangle max = dimensions.get(0);
		for (PDRectangle tmpDimen:dimensions)
		{
			if (tmpDimen.getHeight() > max.getHeight()
					|| tmpDimen.getWidth() > max.getWidth())
			{
				max = tmpDimen;
			}
		}
		return max;
	}
	
	private static PDRectangle findMaxInnerBox(PDPage page) {
		
		return pagerangeDector.getRect(page);

	}
	
	
	/**
	 * if artbox is different , use full range
	 * else adjust by max
	 * 
	 * @param page
	 * @return
	 */
	private static PDRectangle findMaxInnerBoxOld2(PDPage page) {
		
		// special case artbox different.
		if (page.getArtBox().getHeight() != page.getTrimBox().getHeight() 
				|| page.getArtBox().getWidth() !=page.getTrimBox().getWidth())
		{
			return page.getMediaBox();
		}
		
		List <PDRectangle> dimensions = new ArrayList<>();
		dimensions.add(page.getArtBox());
		dimensions.add(page.getBBox());
		dimensions.add(page.getBleedBox());
		dimensions.add(page.getCropBox());
		dimensions.add(page.getTrimBox());
		PDRectangle max = dimensions.get(0);
		for (PDRectangle tmpDimen:dimensions)
		{
			if (tmpDimen.getHeight() > max.getHeight()
					|| tmpDimen.getWidth() > max.getWidth())
			{
				max = tmpDimen;
			}
		}
		return max;
	}

	/**
	 * target resfactor = based on ipad 2048x1536
	 * ex: resfactor 2 = 2048x1536
	 *               1 = 1024x768
	 * calc by small aspect edge
	 * @param page
	 * @param resFactor
	 * @return adjusted resfactor
	 */
	private double adjustResfactor(PDRectangle dimension, double resFactor) {
		double width = dimension.getWidth();
		double height = dimension.getHeight();
		
		double longEdge = Math.max(width, height);
//		double shortEdge = Math.min(width, height);
		
		double ret = 1024/longEdge * resFactor;
		
		return ret;
	}
	


	public int getBigWidth() {
		return bigWidth;
	}

	public void setBigWidth(int bigWidth) {
		this.bigWidth = bigWidth;
	}

	public int getBigHeight() {
		return bigHeight;
	}

	public void setBigHeight(int bigHeight) {
		this.bigHeight = bigHeight;
	}

	public int getSmallWidth() {
		return smallWidth;
	}

	public void setSmallWidth(int smallWidth) {
		this.smallWidth = smallWidth;
	}

	public int getSmallHeight() {
		return smallHeight;
	}

	public void setSmallHeight(int smallHeight) {
		this.smallHeight = smallHeight;
	}

	public int getSmallX() {
		return smallX;
	}

	public void setSmallX(int smallX) {
		this.smallX = smallX;
	}

	public int getSmallY() {
		return smallY;
	}

	public void setSmallY(int smallY) {
		this.smallY = smallY;
	}
	
	public static String dumpPageDimension(PDPage page) {
		
		String mediaBox = "MediaBox=" + dumpDimension(page.getMediaBox());
		String artBox = "ArtBox  =" +dumpDimension(page.getArtBox());
		String bBox="Bbox    ="+dumpDimension(page.getBBox());
		String bleedBox="BleedBox="+dumpDimension(page.getBleedBox());
		String cropBox="CropBox ="+dumpDimension(page.getCropBox());
		String trimBox="TrimBox ="+dumpDimension(page.getTrimBox());
				
		return mediaBox + "\n" 
			+ artBox + "\n" 
			+ bBox + "\n" 
			+ bleedBox + "\n" 
			+ cropBox + "\n" 
			+ trimBox + "\n";
	}

	public static String dumpDimension(PDRectangle dimension) {
		String ret= 
				String.format("%3.2f",dimension.getWidth()) + "," +
				String.format("%3.2f",dimension.getHeight()) + ": " +
				String.format("%3.2f",dimension.getLowerLeftX()) + "," +
				String.format("%3.2f",dimension.getLowerLeftY()) + "," +
				String.format("%3.2f",dimension.getUpperRightX()) + "," +
				String.format("%3.2f",dimension.getUpperRightY()) ;
		return ret;
	}


	
}
