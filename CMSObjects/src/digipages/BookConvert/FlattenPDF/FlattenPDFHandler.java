package digipages.BookConvert.FlattenPDF;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

import com.amazonaws.services.s3.*;
import com.amazonaws.services.s3.model.*;
import com.google.gson.Gson;

import digipages.BookConvert.BookMain.PdfBook;
import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class FlattenPDFHandler implements CommonJobHandlerInterface {
    private static DPLogger logger = DPLogger.getLogger(FlattenPDFHandler.class.getName());
	private static Gson gson = new Gson();
    protected LogToS3 convertingLogger;
	private String logFolder;
	private boolean strictMode=true;
	private Object originalFolder;
	private Object trialFolder;
	private String message="Done";
	private DataMessage dataMsg;
	private AmazonS3 s3Client;
	private RequestData requestData;
	private boolean skipFlatten=false;
	FlattenPDFCore flattenCore = new FlattenPDFCore();
	private int maxResultPDFMB=200;

	@Override
	public String mainHandler(Properties conf,ScheduleJob job) throws ServerException,JobRetryException {
		
		initEnv(conf,job);
		File targetFile=null;
		File pdfFile=null;
		
		//flatten pdf and backup to S3 (replace), if fail report fail with message.
		try {
			File pdfDir=CommonUtil.downloadPDFFromS3(dataMsg);
			pdfFile = new File(pdfDir.getAbsoluteFile() + "/Source.pdf");
			targetFile=flattenPdf(pdfFile);

			String tmpS3Loc = dataMsg.getTargetBucket()+ "/" + dataMsg.getFolder()+ "/PDF/" + 
			dataMsg.getBookFileName();
			CannedAccessControlList permission = requestData.getIsTrial() ? 
	                CannedAccessControlList.PublicRead : CannedAccessControlList.AuthenticatedRead;

			chkPdfDimensionDist(targetFile);
			chkPdfFileSize(targetFile);
			int pdfAvgObjCntLimit = Integer.parseInt(conf.getProperty("pdfAvgObjCntLimit","200"));
			checkPDFObjectCnt(targetFile, pdfAvgObjCntLimit);
			backUpToS3(s3Client, targetFile, tmpS3Loc, permission);

		} catch (JobRetryException e)
		{
			throw e;
		} catch (ServerException e)
		{
			logger.error("PDF Convert Exception", e);
			pdfFailHandler(job, flattenCore,e);
		} catch (Exception e) {
			logger.error("Unexcepted Exception", e);
			pdfFailHandler(job, flattenCore,e);
		}
		finally{
			// delete after upload
			if (null!=targetFile && targetFile.exists())
				targetFile.delete();
			notifyAPI(dataMsg, requestData);
			convertingLogger.uploadLog();
		}
		return "FlattenPDF finished.";
	}
	
	private void chkPdfFileSize(File pdfFile) {
		long startTime = System.currentTimeMillis();
    	try {
    		PDDocument pdf = PDDocument.load(pdfFile);
    		int pageCount=pdf.getNumberOfPages();
    		long avgPageSize = (pdfFile.length()/pageCount)/1024; // KB
    		if (pdfFile.length()>(maxResultPDFMB*1024*1024)){
    			String msg = "Target PDF Size too big:" + pdfFile.length() + ", Avg PageSize: " + avgPageSize + "KB, Page count: " + pageCount;
				dataMsg.appendErrorMessage(msg);
				convertingLogger.warn(msg);
    		}
    		// don't block too big pdf file..
//    		if (strictMode && pdfFile.length()>(maxResultPDFMB*1024*1024))
//    		{
//				dataMsg.setResult(false);
//    		}
    		pdf.close();
    	} catch (Exception ex)
    	{
    		String errMsg = "Reading pdf format error:" + pdfFile.getAbsolutePath();
    		dataMsg.setResult(false);
    		dataMsg.appendErrorMessage(errMsg + ex.getMessage());
    		logger.error(errMsg,ex);
			convertingLogger.error(errMsg +  ex.getMessage());

    	} 

        convertingLogger.info("End chkPdfFileSize spend " + 
        (System.currentTimeMillis() - startTime) + "ms");
		
	}

	// fail, need to report back to NotifyAPI
	private void pdfFailHandler(ScheduleJob job, FlattenPDFCore core,Exception e) {
		String errMessage = e.getMessage();
		if (e instanceof ServerException)
		{
			errMessage = ((ServerException) e).getError_message();
		}
		logger.error("Fail to flattenPDF with Exception "
				 + errMessage);
		job.setErrMessage(errMessage);
		job.setStatus(-1);
		job.setFinishTime(new Date());
		
		dataMsg.setResult(false);
		
		dataMsg.appendErrorMessage(errMessage);
		
	}

	/**
     * flatten pdf and replace old file. (old file is removed)
     * 
     * @throws Exception
     */
    public File flattenPdf(File origFile) throws Exception
    {
   	
    	if (skipFlatten){
    		return origFile;
    	}
    	
    	long startTime = System.currentTimeMillis();
    	
    	File targetFile=flattenCore.flattenPDF(origFile,convertingLogger);
    	
        
        
        // check for space out
    	checkSpace(origFile,targetFile);
	    
        // delete orig if convert success.
        if (targetFile!=origFile){
        	logger.debug("file convert success.");
	        if (!origFile.delete())
	        {
	    		convertingLogger.warn("File delete fail. " + origFile.getAbsolutePath());
	        }
        }
        
        logger.info("End flattenPdf spend " + (System.currentTimeMillis() - startTime) + "ms");
        this.message = "End flattenPdf spend " + (System.currentTimeMillis() - startTime) + "ms";
        return targetFile;
    }

	private static void checkSpace(File origFile, File targetFile) throws FileSystemException {
        if (origFile.getFreeSpace()<1000*1024L) // space smaller than 100k
        {
        	origFile.delete();
        	targetFile.delete();
        	throw new FileSystemException("No Space left on defice, try to use different approach. left=" + origFile.getFreeSpace());
        }
	}

	private void initEnv(Properties conf,ScheduleJob job) {
		String originalBucket = conf.getProperty("originalBucket");
        logFolder = conf.getProperty("logFolder");
        this.originalFolder = conf.getProperty("originalFolder");
        this.trialFolder = conf.getProperty("trialFolder");

		// prepare 
		this.dataMsg = job.getJobParam(DataMessage.class);
		this.requestData = gson.fromJson(dataMsg.getRequestData(), RequestData.class);

		this.s3Client = AmazonS3ClientBuilder.defaultClient();
		
        String convertedLogPath = getLogFolder(requestData) + "/" + "converted.log";
        this.convertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
        this.strictMode=Boolean.parseBoolean(conf.getProperty("CheckPDFThumbnailSize","true"));
        this.maxResultPDFMB = Integer.parseInt(conf.getProperty("maxResultPDFMB","200"));
        String fileName=dataMsg.getBookFileName();
        // skip flatten = no check.
		if (fileName.contains("skipflatten"))
		{
			skipFlatten=true;
			strictMode=false;
		}
		
		flattenCore.setDPI(extractDPI(fileName));
		flattenCore.setFactor(extractResFactor(fileName));
		flattenCore.setUsePS2PDF(extractUsePS2PDF(fileName));
		flattenCore.setImgOnlyFlatten(extractImgOnlyFlatten(fileName));
	}

	private boolean extractImgOnlyFlatten(String fileName) {
		return true;
//		if (fileName.contains("ImgOnly"))
//			return true;
//		return false;
	}

	private boolean extractUsePS2PDF(String fileName) {
		if (fileName.contains("PS2PDF"))
			return true;
		return false;
	}

	public static float extractResFactor(String fileName) {
		float resFactor=2f; 
		String constResFactorStr="resFactor";
		if (!fileName.contains(constResFactorStr))
		{
			return resFactor;
		}
		String fNamePart1=fileName.split("\\.")[0];
		String []seperated=fNamePart1.split("_");
		for (String tmpItem:seperated)
		{
			if (tmpItem.startsWith(constResFactorStr))
			{
				try {
					resFactor = Float.parseFloat(tmpItem.substring(9));
					break;
				}catch (Exception e){
					// do nothing.
				}
			}
		}

		return resFactor;
	}

	/**
	 * 
	 * @param fileName
	 * @return dpi
	 */
	public static float extractDPI(String fileName) {
		float retDpi=450f; 
		String constDPIStr="dpi";
		if (!fileName.contains(constDPIStr))
		{
			return retDpi;
		}
		String fNamePart1=fileName.split("\\.")[0];
		String []seperated=fNamePart1.split("_");
		for (String tmpItem:seperated)
		{
			if (tmpItem.startsWith(constDPIStr))
			{
				try {
					retDpi = Float.parseFloat(tmpItem.substring(3));
					break;
				}catch (Exception e){
					// do nothing.
				}
			}
		}

		return retDpi;
	}

	/**
     * calc object count (single page 200)
	 * @throws IOException 
     * @throws Exception
     */
    private void checkPDFObjectCnt(File pdfFile, int pdfPageObjCntLimit) throws ServerException
    {
    	long startTime = System.currentTimeMillis();
    	try {
	    	PDDocument sourceDocument = PDDocument.load(pdfFile);
			int totalPage= sourceDocument.getNumberOfPages();
			boolean fail=false;
			StringBuilder sb=new StringBuilder();
			for (int i=0;i< totalPage; i++)
			{
				PDPage tempPage = sourceDocument.getPage(i);

				int objCnt = PdfBook.countPageElements(tempPage);
				if (objCnt> pdfPageObjCntLimit)
				{
					StringBuilder x=new StringBuilder();
					x.append("PDF too many object in single page, ")
					.append(pdfFile.getAbsolutePath())
					.append(" page:").append(i+1)
					.append(" count=").append(objCnt)
					.append("\r\n");
					sb.append(x.toString());
					fail=true;
					convertingLogger.error(
							x.toString());
				}
			}
			sourceDocument.close();
			if (fail && strictMode){
				dataMsg.setResult(false);
				dataMsg.appendErrorMessage(sb.toString());
				throw new ServerException("id_err_315",sb.toString());
			}
    	} catch (IOException ioex)
    	{
    		logger.error("Reading pdf format error:" + pdfFile.getAbsolutePath(),ioex);
			convertingLogger.error("Reading pdf format error:" +  ioex.getMessage());
    		throw new ServerException("id_err_315",ioex.getMessage());
    	}

        convertingLogger.info(
        		"End checkPDFObjectCnt spend " + (System.currentTimeMillis() - startTime) + "ms");
    }
    
    
	/**
     * check PdfSize for consistant.
	 * @throws IOException 
     * @throws Exception
     */
    private void chkPdfDimensionDist(File pdfFile) 
    {
    	long startTime = System.currentTimeMillis();
    	
    	try {
    		PDRectangle prevSize=null;
	    	PDDocument sourceDocument = PDDocument.load(pdfFile);
			int totalPage= sourceDocument.getNumberOfPages();
			boolean fail=false;
			StringBuilder sb = new StringBuilder();
			for (int i=0;i< totalPage; i++)
			{
				PDPage tempPage = sourceDocument.getPage(i);
				PDRectangle curSize = tempPage.getMediaBox();
				
				// last file is not compared.
				if (i<totalPage-1 && diffTooMuch(prevSize, curSize))
				{
					StringBuilder x= new StringBuilder();
					x.append("PDF page size mismatch , ") 
					.append(pdfFile.getAbsolutePath())
					.append(" page:").append(i+1)
					.append("\r\n");
					sb.append(x.toString());
					fail=true;
					convertingLogger.error(x.toString());
				}
				prevSize=curSize;
			}
			sourceDocument.close();
			if (strictMode && fail){
				dataMsg.setResult(false);
				dataMsg.appendErrorMessage(sb.toString());
			}
    	} catch (Exception ex)
    	{
    		String errMsg = "Reading pdf format error:" + pdfFile.getAbsolutePath();
    		dataMsg.setResult(false);
    		dataMsg.appendErrorMessage(errMsg + ex.getMessage());
    		logger.error(errMsg,ex);
			convertingLogger.error(errMsg +  ex.getMessage());

    	} 

        convertingLogger.info("End chkPdfDimensionDist spend " + 
        (System.currentTimeMillis() - startTime) + "ms");
    }
    
	
	
    /**
     * torlance 5% => 50%
     * @param prevSize
     * @param curSize
     * @return
     */
	private static boolean diffTooMuch(PDRectangle prevSize, PDRectangle curSize) {
		if (prevSize==null || curSize==null)
			return false;
		float curHeight=curSize.getHeight();
		if (Math.abs(prevSize.getHeight() - curSize.getHeight())/curHeight>0.50)
		{
			return true;
		}

		float curWidth = curSize.getWidth();
		if (Math.abs(prevSize.getWidth() - curSize.getWidth())/curWidth >0.50)
		{
			return true;
		}
		
		return false;
	}

	protected static void backUpToS3(AmazonS3 s3Client, File bookFile, String targetS3URI, CannedAccessControlList permission) {
        
        List <String> s3Info=CommonUtil.splitS3URI(targetS3URI);

        // delete orig
//        DeleteObjectRequest delRq = new DeleteObjectRequest(s3Info.get(0), s3Info.get(1));
//        s3Client.deleteObject(delRq);

        
        PutObjectRequest putObjectRequest = new 
        		PutObjectRequest(s3Info.get(0),s3Info.get(1), bookFile)
                .withCannedAcl(permission);
        
        PutObjectResult r= s3Client.putObject(putObjectRequest);
        
        logger.debug("Send back to S3, with message"+r.toString());
	}
	
	private static void notifyAPI(DataMessage dataMessage, RequestData requestData2) throws ServerException
	{
		try {			
			ScheduleJob sj = new ScheduleJob();
			sj.setJobParam(dataMessage);
			sj.setJobName(NotifyAPIHandler.class.getName());
//			sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
			sj.setJobGroup(requestData2.getBook_file_id());
			ScheduleJobManager.addJob(sj);
		} catch (Exception e) {
			logger.error("Unexcepted when notify API",e);
			throw new ServerException("id_err_999","Unexcepted when notify API");
		}
	}


	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// don't need DB connection
	}
	
	
	
	
	private static boolean errMessageIsCritical(String errResult) {
    	errResult=errResult.toLowerCase();
    	// in most case, repaired, ignored error is consider non-fatal.
    	if (errResult.contains("repaired") || errResult.contains("ignored") ||
    			errResult.contains("Output may be incorrect."))
    		return false;
    	// staill have error ?! critical
    	if (errResult.contains("error"))
    		return true;
    	// default ok.
    	return false;
	}

	
    protected String getLogFolder(RequestData lambdaRequestData) {
        String folderPath;
        
        
        
        folderPath = logFolder + "/" + (lambdaRequestData.getIsTrial() ? trialFolder : originalFolder) +
                "/" + lambdaRequestData.getItem() + "/" + lambdaRequestData.getBook_file_id();
        
        return folderPath;
    }

	/**
	 * default is Ec2, no other possible.
	 * @param runMode
	 * @return
	 */
	public static RunMode getRetryRunMode(RunMode runMode) {
		if (runMode==RunMode.LambdaMode)
		{
			return RunMode.ExtEc2Mode;
		}
		return RunMode.ExtEc2Mode;
	}


	
	public static RunMode getInitRunMode(ScheduleJob job) {
		
		return RunMode.ExtEc2Mode;
		
	}
	

}
