package digipages.BookConvert.FlattenPDF;

import java.io.IOException;

import java.util.List;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.contentstream.operator.DrawObject;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.contentstream.PDFStreamEngine;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.contentstream.operator.state.Concatenate;
import org.apache.pdfbox.contentstream.operator.state.Restore;
import org.apache.pdfbox.contentstream.operator.state.Save;
import org.apache.pdfbox.contentstream.operator.state.SetGraphicsStateParameters;
import org.apache.pdfbox.contentstream.operator.state.SetMatrix;

public class PageRangeDector extends PDFStreamEngine {
	private static PDRectangle calcRect;
	private PDPage curPage=null;
	/**
	 * Default constructor.
	 *
	 * @throws IOException
	 *             If there is an error loading text stripper properties.
	 */
	public PageRangeDector()  {
		addOperator(new Concatenate());
		addOperator(new DrawObject());
		addOperator(new SetGraphicsStateParameters());
		addOperator(new Save());
		addOperator(new Restore());
		addOperator(new SetMatrix());
	}

	public static void main(String[] args) throws IOException {

		try (PDDocument document = PDDocument.load(new File("/tmp/money498_orig.pdf"))) {
			PageRangeDector processor = new PageRangeDector();
			int pageNum = 0;
			for (PDPage page : document.getPages()) {
				pageNum++;
				System.out.println("Processing page: " + pageNum);
//				processor.setCurPage(page);
//				processor.processPage(page);
				System.out.println(PageDimension.dumpDimension(processor.getRect(page)));
				
			}
			document.close();
		}
	}
	
	public PDRectangle getRect(PDPage page)
	{
		setCurPage(page);
		try{
			processPage(page);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return calcRect;
	}



	private void setCurPage(PDPage page) {
		this.curPage=page;
		calcRect = page.getTrimBox();
	}

	/**
	 * This is used to handle an operation.
	 *
	 * @param operator
	 *            The operation to perform.
	 * @param operands
	 *            The list of arguments.
	 *
	 * @throws IOException
	 *             If there is an error processing the operation.
	 */
	@Override
	protected void processOperator(Operator operator, List<COSBase> operands) throws IOException {
		String operation = operator.getName();
		if ("Do".equals(operation)) {
			COSName objectName = (COSName) operands.get(0);
			PDXObject xobject=null;
			try {
				xobject = getResources().getXObject(objectName);
			}catch (Exception e)
			{
				System.out.println("skipping object:" + objectName + ", reason:" + e.getMessage());
				super.processOperator(operator, operands);
				return;
			}
			if (xobject instanceof PDImageXObject) {
				PDImageXObject image = (PDImageXObject) xobject;
				

				Matrix ctmNew = getGraphicsState().getCurrentTransformationMatrix();
				
				adjustOuterRange(ctmNew);
//				PDRectangle margin = curPage.getBleedBox();
//				System.out.println("bleedbox XY   :" + margin.getLowerLeftX() + "," + margin.getLowerLeftY());
//				System.out.println("bleedbox maxXY:" + (margin.getLowerLeftX()+ margin.getWidth()) 
//						+ "," + (margin.getLowerLeftY() + margin.getHeight()));
//				margin = curPage.getMediaBox();
//				System.out.println("mediabox XY   :" + margin.getLowerLeftX() + "," + margin.getLowerLeftY());
//				System.out.println("mediabox maxXY:" + (margin.getLowerLeftX()+ margin.getWidth()) 
//						+ "," + (margin.getLowerLeftY() + margin.getHeight()));
//				
//				// position in user space units. 1 unit = 1/72 inch at 72 dpi
//				System.out.println("position in PDF = " + ctmNew.getTranslateX() + ", " + ctmNew.getTranslateY()
//						+ " in user space units");
//				System.out.println("max xy          = " + (ctmNew.getTranslateX() + imageXScale) + ", " + (ctmNew.getTranslateY() + imageYScale) + " in user space units");
//				// raw size in pixels
//				System.out.println("raw image size  = " + imageWidth + ", " + imageHeight + " in pixels");
//				// displayed size in user space units
//				System.out.println("displayed size  = " + imageXScale + ", " + imageYScale + " in user space units");
//				// displayed size in inches at 72 dpi rendering
//				imageXScale /= 72;
//				imageYScale /= 72;
//				System.out.println(
//						"displayed size  = " + imageXScale + ", " + imageYScale + " in inches at 72 dpi rendering");
//				// displayed size in millimeters at 72 dpi rendering
//				imageXScale *= 25.4;
//				imageYScale *= 25.4;
//				System.out.println("displayed size  = " + imageXScale + ", " + imageYScale
//						+ " in millimeters at 72 dpi rendering");
//				System.out.println("MediaBox Out:" + outRange(curPage.getMediaBox(), ctmNew));
//				System.out.println("BleedBox Out:" + outRange(curPage.getBleedBox(), ctmNew));
//				System.out.println();
			} else if (xobject instanceof PDFormXObject) {
				PDFormXObject form = (PDFormXObject) xobject;
				showForm(form);
			}
		} else {
			super.processOperator(operator, operands);
		}
	}
	
	private void adjustOuterRange(Matrix imageDimension) {
		if (imageDimension.getTranslateX() < calcRect.getLowerLeftX())
		{
			calcRect.setLowerLeftX(imageDimension.getTranslateX());
		}
		if (imageDimension.getTranslateY() < calcRect.getLowerLeftY())
		{
			calcRect.setLowerLeftY(imageDimension.getTranslateY());
		}
		
		if (imageDimension.getTranslateX() + imageDimension.getScalingFactorX() > calcRect.getUpperRightX()){
			calcRect.setUpperRightX(imageDimension.getTranslateX() + imageDimension.getScalingFactorX());
		}
		if (imageDimension.getTranslateY() + imageDimension.getScalingFactorY() > calcRect.getUpperRightY()){
			calcRect.setUpperRightY(imageDimension.getTranslateY() + imageDimension.getScalingFactorY());
		}
		
		
	}

	private static boolean outRange(PDRectangle boxRect, Matrix imageDimension) {
		if (imageDimension.getTranslateX() < boxRect.getLowerLeftX())
			return true;
		if (imageDimension.getTranslateY() < boxRect.getLowerLeftY())
			return true;
		if (imageDimension.getTranslateX() + imageDimension.getScalingFactorX() > boxRect.getUpperRightX())
			return true;
		if (imageDimension.getTranslateY() + imageDimension.getScalingFactorY() > boxRect.getUpperRightY())
			return true;
		return false;
	}


	private boolean outRange(PDPage curPage2, Matrix ctmNew) {
		PDRectangle range = curPage2.getTrimBox();
		return outRange(range,ctmNew);
	}

}
