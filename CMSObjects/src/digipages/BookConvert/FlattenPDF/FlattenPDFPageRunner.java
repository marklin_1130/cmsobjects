package digipages.BookConvert.FlattenPDF;

import java.io.File;

import digipages.common.DPLogger;

public class FlattenPDFPageRunner implements Runnable {
	private File procPDF;
	private FlattenPDFCore core;
	private String targetPath;
	private int quality=92;
	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

	private static DPLogger logger = DPLogger.getLogger(FlattenPDFPageRunner.class.getName());
	
	public FlattenPDFPageRunner(FlattenPDFCore core,File pdfFile,String targetPath, int quality) {
		this.procPDF = pdfFile;
		this.core=core;
		this.targetPath=targetPath;
		this.quality = quality;
	}

	@Override
	public void run() {
		try {
			mainProcess();
		}catch (Exception e)
		{
			logger.error("Unexcepted exception ",e);
			// TODO what to do ?
		}
	}

	private void mainProcess() {
		try{
			core.extractPDF(procPDF," -dFILTERIMAGE -dFILTERVECTOR ",".txtOnly");
			core.extractPDF(procPDF," -dFILTERTEXT ",".background");
			core.extractPDF(procPDF," -dFILTERIMAGE -dFILTERTEXT ",".vectorOnly");
			core.flattenFile(targetPath,procPDF, quality);
		} catch (Exception e)
		{
			// fail = 
			core.setSuccess(false);
		}
	}

}
