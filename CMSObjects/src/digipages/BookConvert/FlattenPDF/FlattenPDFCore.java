package digipages.BookConvert.FlattenPDF;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.action.PDAction;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionNamed;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageFitWidthDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineNode;

import com.google.gson.Gson;

import digipages.BookConvert.utility.CmdExecutor;
import digipages.BookConvert.utility.LogToS3;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;

public class FlattenPDFCore {
	private static DPLogger logger = DPLogger.getLogger(FlattenPDFCore.class.getName());
	private static int timeoutMinutes = 60;
	private boolean success = true;
	private static LogToS3 converterLogger;
	private float dpi;
	private float resFactor;
	private int quality = 92;
	private boolean usePS2PDF = false;
	private int targetPages;
	private long targetFilieSize;
	private boolean imgOnlyFlatten;
	private static Gson gson = new Gson();

	public boolean getSuccess() {
		return success;
	}

	/**
	 * flatten PDF File with split/merge.
	 * 
	 * @param origFile
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ServerException
	 */
	public File flattenPDF(File origFile, LogToS3 converterLogger)
			throws IOException, InterruptedException, ServerException {
		FlattenPDFCore.converterLogger = converterLogger;
		File pagedPDFDir = splitPDFPages(origFile);
		// update/determine quality related
		determineQualityParameter(origFile);
		File flattenedDir = flattenDir(pagedPDFDir);
		File hiResFlattenPDF = mergePDFDir(flattenedDir);
		checkTimeoutByPageCompare(origFile, hiResFlattenPDF);
		copyBookmarks(origFile, hiResFlattenPDF);

		// CommonUtil.delDirs(pagedPDFDir);
		// CommonUtil.delDirs(flattenedDir);

		return hiResFlattenPDF;
	}

	private void determineQualityParameter(File origFile) {
		// according to page, change quality
		if (null==origFile) return;
		try {
			PDDocument pdfDoc = PDDocument.load(origFile);
			int pageCnt=pdfDoc.getNumberOfPages();
			
			calcQualityByPage(pageCnt);
			
			pdfDoc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param pageCnt
	 */
	private void calcQualityByPage(int pageCnt) {
		logger.debug("Calculate Quality by pages: " + pageCnt);
		if (pageCnt>350)
		{
			quality = 85;
			dpi=300;
			resFactor=1.5f;
			logger.debug("Using parameter: jpg quality=" + quality + " dpi=" + dpi + " resFactor=" +resFactor);
		}
		
	}

	/**
	 * timeout = source page cnt <> targetPageCnt
	 * 
	 * @param origFile
	 * @param targetFile
	 * @throws ServerException
	 */
	private void checkTimeoutByPageCompare(File origFile, File targetFile) throws ServerException {
		PDDocument sourceDocument = null;
		PDDocument targetDocument = null;

		try {
			sourceDocument = PDDocument.load(origFile);
			targetDocument = PDDocument.load(targetFile);

			if (sourceDocument.getNumberOfPages() != targetDocument.getNumberOfPages()) {
				logger.warn("Page compare different: src=" + sourceDocument.getNumberOfPages() + ", target="
						+ targetDocument.getNumberOfPages());
				if (false)
					throw new ServerException("id_err_312",
							"Convert fail to collect pages, timeout :" + timeoutMinutes
									+ " minutes, please use larger instance to convert this file."
									+ sourceDocument.getNumberOfPages() + ":" + targetDocument.getNumberOfPages());
			}
		} catch (ServerException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Unexcepted Exception", e);
			throw new ServerException("id_err_999", "Unexcepted Exception:" + e.getMessage());
		} finally {
			try {
				if (null != sourceDocument)
					sourceDocument.close();
				if (null != targetDocument)
					targetDocument.close();
			} catch (Exception e) {
				// closing fail is ok
			}
		}
	}

	/**
	 * copy the orig pdf index(bookmark) to target
	 * 
	 * @param origFile
	 * @param targetPDF
	 * @throws IOException
	 */
	public static void copyBookmarks(File origFile, File targetPDF) throws IOException {

		PDDocument sourceDocument = PDDocument.load(origFile);
		PDDocumentCatalog srcCatalog = sourceDocument.getDocumentCatalog();
		PDDocumentOutline srcOutline = srcCatalog.getDocumentOutline();

		PDDocument targetDocument = PDDocument.load(targetPDF);
		// set DocumentOutline will clone content to target. (target size = 2x)
		// targetDocument.getDocumentCatalog().setDocumentOutline(srcOutline);

		PDDocumentOutline targetOutline = new PDDocumentOutline();
		targetDocument.getDocumentCatalog().setDocumentOutline(targetOutline);

		// names seem not need to copy ?!
		// PDDocumentNameDictionary srcNames =
		// sourceDocument.getDocumentCatalog().getNames();
		// targetDocument.getDocumentCatalog().setNames(srcNames);

		copyOutLineNode(targetDocument, srcOutline, targetOutline, "");

		targetDocument.save(targetPDF);
		sourceDocument.close();
		targetDocument.close();
	}

	public static void printBookmark(PDOutlineNode bookmark, String indentation) throws IOException {
		if (bookmark == null)
			return;
		PDOutlineItem current = bookmark.getFirstChild();
		while (current != null) {
			logger.info(indentation + current.getTitle());
			printBookmark(current, indentation + "    ");
			current = current.getNextSibling();
		}
	}

	private static void copyOutLineNode(PDDocument targetDocument, PDOutlineNode srcOutline,
			PDOutlineNode targetOutline, String indentation) throws IOException {
		if (srcOutline == null)
			return;

		PDOutlineItem current = srcOutline.getFirstChild();
		while (current != null) {
			PDOutlineItem targetItem = cloneOUtlineItem(targetDocument, current);
			targetOutline.addLast(targetItem);
			// logger.info(indentation + current.getTitle());

			copyOutLineNode(targetDocument, current, targetItem, indentation + "    ");
			current = current.getNextSibling();
		}

	}

	private static PDOutlineItem cloneOUtlineItem(PDDocument targetDocument, PDOutlineItem srcItem) throws IOException {
		PDOutlineItem ret = new PDOutlineItem();
		ret.setItalic(srcItem.isItalic());
		ret.setTextColor(srcItem.getTextColor());
		ret.setTitle(srcItem.getTitle());

		PDAction action = clonePDAction(targetDocument, srcItem.getAction());
		ret.setAction(action);

		try {
			PDDestination srcDest = srcItem.getDestination();
			PDPageDestination targetPageDest = clonePDPageDest(targetDocument, srcDest);
			ret.setDestination(targetPageDest);
		} catch (Exception e) {
			// skip items that don't support
			logger.warn("PDF contains malformed destinations " + srcItem.getTitle());
		}

		return ret;
	}

	private static PDAction clonePDAction(PDDocument targetDocument, PDAction srcAction) throws IOException {
		if (srcAction == null)
			return null;

		PDAction targetAction = null;
		// what I cannot process.
		if (srcAction instanceof PDActionGoTo) {
			targetAction = cloneActionGoTo(targetDocument, (PDActionGoTo) srcAction);
		} else {
			targetAction = cloneActionOther((PDActionNamed) srcAction);
		}

		return targetAction;
	}

	private static PDAction cloneActionOther(PDActionNamed srcAction) {

		logger.debug("warn: action not implemented yet:" + gson.toJson(srcAction));
		return null;
	}

	private static PDAction cloneActionGoTo(PDDocument targetDocument, PDActionGoTo srcAction) throws IOException {
		PDActionGoTo targetAction = new PDActionGoTo();
		try {
			PDPageDestination targetPage = clonePDPageDest(targetDocument, srcAction.getDestination());
			targetAction.setDestination(targetPage);
		} catch (Exception e) {
			logger.warn("Unexcepted Clone ActionGoto" + srcAction.getDestination().toString() + " Setting to page 0");
			PDPageDestination targetPageDest = new PDPageFitWidthDestination();
			PDPage targetPag = targetDocument.getPage(1);
			targetPageDest.setPage(targetPag);
			targetAction.setDestination(targetPageDest);
		}
		return targetAction;
	}

	private static PDPageDestination clonePDPageDest(PDDocument targetDocument, PDDestination srcDest) {
		if (srcDest == null)
			return null;
		if (!(srcDest instanceof PDPageDestination)) {
			logger.debug("Warning: curItem is not a pdPage " + srcDest.getClass().getName());
			return null;
		}
		PDPageDestination srcPageDest = (PDPageDestination) srcDest;
		int pageNo = srcPageDest.retrievePageNumber();
		PDPage targetPag = targetDocument.getPage(pageNo);

		PDPageDestination targetPageDest = new PDPageFitWidthDestination();
		targetPageDest.setPage(targetPag);
		return targetPageDest;
	}

	/**
	 * flatten all directory by
	 * 
	 * @param extractAllDir
	 * @return
	 * @throws InterruptedException
	 * @throws ServerException
	 */
	private File flattenDir(File srcDir) throws InterruptedException, ServerException {

		String targetPath = srcDir.getAbsolutePath() + ".flattened";
		File targetDir = new File(targetPath);
		targetDir.mkdirs();
		long startTime = System.currentTimeMillis();
		int threadLimit = Runtime.getRuntime().availableProcessors();
		logger.debug("start threads with limit " + threadLimit);
		ExecutorService executor = Executors.newFixedThreadPool(threadLimit);
		
		for (File procPDF : srcDir.listFiles()) {
			if (!procPDF.getName().endsWith(".paged.pdf"))
				continue;

			executor.execute(new FlattenPDFPageRunner(this, procPDF, targetPath, quality));

		}
		executor.shutdown();
		executor.awaitTermination(timeoutMinutes, TimeUnit.MINUTES);
		checkTimeoutByTime(startTime);
		return targetDir;
	}

	private void checkTimeoutByTime(long startTime) throws ServerException {
		long currentTime = System.currentTimeMillis();
		long duration = currentTime - startTime;

		if (duration > (timeoutMinutes - 1) * 60 * 60 * 1000L)
			throw new ServerException("id_err_313",
					"Timeout When flatten PDF, please consider use faster machine to do the work.");
	}

	/**
	 * main processing
	 * 
	 * @param targetPath
	 * @param curPdfPage
	 * @throws IOException
	 * @throws InterruptedException
	 */
	void flattenFile(String targetPath, File curPdfPage, int quality) throws IOException, InterruptedException {
		String srcPdfPath = curPdfPage.getAbsolutePath();
		File vectorFile = new File(srcPdfPath + ".vectorOnly");
		if (hasOnlyText(curPdfPage)) {
			flattenByPs2PDF(srcPdfPath, targetPath);
		} else {
			if (isVectorBig(vectorFile))
				flattenByTxtAndBackground(srcPdfPath, targetPath, quality);
			else
				flattenByPs2PDF(srcPdfPath, targetPath);
		}
	}

	private boolean hasOnlyText(File curPdfPage) {
		String srcPdfPath = curPdfPage.getAbsolutePath();
		File bkgroundFile = new File(srcPdfPath + ".background");
		File vectorFile = new File(srcPdfPath + ".vectorOnly");

		if (bkgroundFile.exists())
			return false;
		if (vectorFile.exists())
			return false;
		return true;
	}

	// 40k vector file is too big, but this is not tested yet.
	private boolean isVectorBig(File vectorFile) {
		if (usePS2PDF) {
			// return vectorFile.length() > 40000L;
			return vectorFile.length() > 10000L; // lower to 10k vector
		} else {
			return true;
		}
	}

	/**
	 * in progress, not fullly tested.
	 * 
	 * @param srcPath
	 * @param targetPath
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void flattenByPs2PDF(String srcPath, String targetPath) throws IOException, InterruptedException {
		File srcFile = new File(srcPath);
		String targetFileName = targetPath + "/" + srcFile.getName() + ".txtOnly.merged.pdf";
		String cmd = "ps2pdf "
				// + " -dFIXEDMEDIA "
				// + " -dPDFFitPage "
				// + " -dAutoRotatePages=/PageByPage "
				+ " -dAutoRotatePages=/None " + " -dEmbedAllFonts=true " + " -dCompatibilityLevel=1.4 "
				+ " -dPDFSETTINGS=/printer " + " -dSubsetFonts=false " + " -r300x300 " + srcFile.getAbsolutePath() + " "
				+ targetFileName;
		logger.debug("ps2pdf command: " + cmd);
		CmdExecutor cmdExec = new CmdExecutor();
		cmdExec.run(cmd);
		String result = cmdExec.getStdOut();
		String errResult = cmdExec.getStdErr();

		converterLogger.info("ps2pdf result: " + result);
		converterLogger.info("ps2pdf error: " + errResult);
		logger.info("ps2pdf result: " + result);
		logger.info("ps2pdf error: " + errResult);

	}

	/**
	 * if fail to get text file or img background. try to use full pdf image.
	 * 
	 * @param srcPdfPath
	 * @param targetPath
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void flattenByTxtAndBackground(String srcPdfPath, String targetPath, int quality)
			throws IOException, InterruptedException {

		File bkgroundFile = new File(srcPdfPath + ".background");
		File txtPdf = new File(srcPdfPath + ".txtOnly");

		if (imgOnlyFlatten || !(bkgroundFile.exists() && txtPdf.exists())) {
			// only Image option is enabled.
			// has no txt => build only jpg.

			PageDimension dimension = null;
			try {
				dimension = new PageDimension(bkgroundFile, resFactor);
			} catch (Exception e) {
				logger.warn("Page Background dimension not exist, using orig pdf.");
			}
			File imgFlattenPdf = jpgFlattenPDF(new File(srcPdfPath), dimension, quality);
			// jpgToPDF(imgFlattenJpg);
			// File imgFlattenPdf = new File(imgFlattenJpg.getAbsolutePath() +
			// ".pdf");
			File targetFile = new File(targetPath + "/" + txtPdf.getName() + ".merged.pdf");
			imgFlattenPdf.renameTo(targetFile);
			logger.debug("flattened by img only :" + targetFile.getName());
		} else {
			// normal step: have background file + txt file
			File imgFlattenPdf = jpgFlattenPDF(bkgroundFile, null, quality);
			File txtImgFlattenPdf = mergePDFBackground(txtPdf, imgFlattenPdf);
			File targetFile = new File(targetPath + "/" + txtImgFlattenPdf.getName());
			txtImgFlattenPdf.renameTo(targetFile);
			logger.debug("flattened by img and txt :" + targetFile.getName());
		}

	}

	private File jpgFlattenPDF(File srcPDF, PageDimension dimension, int quality) throws IOException, InterruptedException {
		pdfToJpg(srcPDF, dimension, quality);
		File jpgFile = new File(srcPDF.getAbsoluteFile() + ".jpg");
		jpgToPDF(jpgFile);

		return new File(jpgFile.getAbsolutePath() + ".pdf");
	}

	/**
	 * pdftk cool-215-noimg.pdf multibackground cool-215-jpg.pdf output
	 * cool-215-imflatten.pdf
	 * 
	 * @param txtOnlyPDF
	 * @param flattenImgOnlyPDF
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private File mergePDFBackground(File txtOnlyPDF, File backgroundPDFFile) throws IOException, InterruptedException {
		String targetPath = txtOnlyPDF.getAbsolutePath() + ".merged.pdf";
		String cmdStr = "pdftk " + txtOnlyPDF.getAbsolutePath() + " multibackground "
				+ backgroundPDFFile.getAbsolutePath() + " output " + targetPath;
		CmdExecutor cmd = new CmdExecutor();
		cmd.run(cmdStr);
		converterLogger.info("MergePDFBackground cmd: " + cmdStr);
		converterLogger.info("MergePDFBackground result: " + cmd.getStdOut());
		converterLogger.info("MergePDFBackground error: " + cmd.getStdErr());
		logger.debug("mergePDF cmd: " + cmdStr);
		logger.debug("mergePDF result: " + cmd.getStdOut());
		return new File(targetPath);
	}

	/**
	 * pdftk *.pdf output /tmp/cool-215-remake.pdf
	 * 
	 * @param targetPDFDir
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private File mergePDFDir(File seperatedPDFDir) throws IOException, InterruptedException {
		logger.debug("mergePDFDir:" + seperatedPDFDir.getAbsolutePath());
		String path = seperatedPDFDir.getAbsolutePath() + ".mergesum.pdf";
		if (path == null || path.isEmpty()) {
			converterLogger.error("MergePDF Fail, \npath NotExists.");
			success = false;
			return null;
		}

		String cmd = buildMergePDFCmd(seperatedPDFDir, path);

		CmdExecutor executor = new CmdExecutor();
		executor.run(cmd);
		converterLogger.info("MergepdfDir result:" + executor.getStdOut());
		converterLogger.info("MergePdfDir error message: " + executor.getStdErr());
		logger.debug("merge pdf result:" + executor.getStdOut());
		logger.debug("merge pdf Err result:" + executor.getStdErr());

		File retFile = new File(path);
		if (!retFile.exists()) {
			converterLogger.error("\n MergePdfDir Fail: Targetfile not exists.");
			success = false;
			return null;
		}
		targetFilieSize = retFile.length();
		return retFile;
	}

	/*
	 * notice: don't use command pdftk *.pdf output xx because * is expand by
	 * sh/bash, which in this case not apply (only run pdftk)
	 */
	private String buildMergePDFCmd(File seperatedPDFDir, String path) {
		// build Command
		StringBuilder sb = new StringBuilder();
		sb.append("pdftk ");
		// sort in case of strange order.
		File[] files = seperatedPDFDir.listFiles();
		Arrays.sort(files);
		int i = 0;
		for (File tmpf : files) {
			if (!tmpf.getName().endsWith(".pdf"))
				continue;
			sb.append(tmpf.getAbsolutePath()).append(" ");
			i++;
		}
		targetPages = i;
		sb.append(" output ").append(path);
		logger.debug("Execute command: " + sb.toString());
		return sb.toString();
	}

	/**
	 * gs -o cool-215-imgOnly.pdf -sDEVICE=pdfwrite -dFILTERTEXT cool-215.pdf
	 * 
	 * @param origFile
	 * @param imgOnlyPara
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public File extractPDF(File origFile, String para, String ext) throws IOException, InterruptedException {
		String targetPath = origFile.getAbsolutePath() + ext;
		String cmdStr = "gs -o " + targetPath + " -sDEVICE=pdfwrite " + para + " " + origFile.getAbsolutePath();
		logger.debug("extractPDF cmd:" + cmdStr);
		CmdExecutor cmdRunner = new CmdExecutor();
		cmdRunner.run(cmdStr);
		converterLogger.info(" extractPDF Result" + cmdRunner.getStdOut());
		converterLogger.info(" extractPDF error message: " + cmdRunner.getStdErr());
		logger.info("extractPDF result:" + cmdRunner.getStdOut());
		String stdOut = cmdRunner.getStdOut();
		// error handle retry
		if (stdOut.contains("Error reading a content stream.") && !stdOut.contains("were repaired")) {
			logger.error("Faile reading content stream:" + cmdRunner.getStdErr());
			new File(targetPath).delete();
		}

		return new File(targetPath);
	}

	/**
	 * pdftk xxxx.pdf brust output /tmp/orig_%03d.paged.pdf
	 * 
	 * @param origPDF
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private File splitPDFPages(File origPDF) throws IOException, InterruptedException {
		File targetPDFDir = new File(origPDF.getAbsolutePath() + ".paged");
		targetPDFDir.mkdirs();

		StringBuilder sb = new StringBuilder();
		sb.append("pdftk ").append(origPDF.getAbsolutePath()).append(" burst output ").append(targetPDFDir)
				.append("/orig_%03d.paged.pdf");
		logger.debug("Splitting pdf By command:" + sb.toString());
		CmdExecutor cmd = new CmdExecutor();
		cmd.run(sb.toString());
		String tmpResult = cmd.getStdOut();
		String tmpErr = cmd.getStdErr();
		converterLogger.info(" splitPDF Result :" + tmpResult);
		converterLogger.info("splitPDF Error :" + tmpErr);
		logger.info("split PDF result: " + origPDF.getName() + tmpResult);
		return targetPDFDir;
	}

	/*
	 * convert -density 300 -background white -alpha remove -resize
	 * 
	 */
	private String pdfToJpg(File pdfFile, PageDimension dimension,int quality) throws IOException, InterruptedException {

		PDDocument pdf = PDDocument.load(pdfFile);
		String pdfSrcPath = pdfFile.getAbsolutePath();
		PDPage page = pdf.getPage(0);
		pdf.close();
		// below 2 parameter deter the size/detail of pdf
		if (dimension == null) {
			logger.debug("using dimension from local file:");
			dimension = new PageDimension(page, resFactor);
		}
		// logger.debug(PageDimension.dumpDimension(dimension.getRectanble()));
		// without corp, output pdf will be in a smaller region.
		StringBuilder sb = new StringBuilder();
		sb.append("convert -density ").append(dpi).append(" ").append(pdfSrcPath).append(" -background white ")
				.append(" -alpha remove ").append(" -resize ").append(dimension.getBigWidth()).append("x")
				.append(dimension.getBigHeight())
				// .append(" -colorspace CMYK ") // avoide cmyk output, but make
				// output less colorful.
				// .append(" -define pdf:use-cropbox=true ")
				.append(" -crop ").append(dimension.getSmallWidth()).append("x").append(dimension.getSmallHeight())
				.append("-").append("10").append("-").append("10").append(" +repage ")
				// .append(" -crop
				// ").append(dimension.getBigWidth()).append("x").append(dimension.getBigHeight())
				// .append("+").append("0").append("+").append("0")
				.append(" -quality ").append(quality).append(" ").append(pdfSrcPath + ".jpg");
		CmdExecutor cmd = new CmdExecutor();
		cmd.run(sb.toString());
		String tmpResult = cmd.getStdOut();
		String tmpErr = cmd.getStdErr();
		converterLogger.info(sb.toString() + " Error message:" + tmpErr);
		converterLogger.info(sb.toString() + " Result message:" + tmpResult);
		logger.info("Executing pdfToJpg: " + sb.toString());
		logger.info("PdfToJpg result=" + tmpResult);
		logger.debug(dimension.toString());
		return tmpResult + "\n\n";
	}

	/**
	 * convert -bordercolor white -background white -border 0 -trim *.jpg
	 * pictures.pdf Using Thread to
	 * 
	 * @param jpgFile
	 * @return
	 * @throws InterruptedException
	 */
	private String jpgToPDF(File jpgFile) throws IOException, InterruptedException {

		String targetPath = jpgFile.getAbsolutePath() + ".pdf";
		String cmdStr = "convert -bordercolor white -background white -border 0 -density 150 "
				+ jpgFile.getAbsolutePath() + " " + targetPath;
		CmdExecutor cmd = new CmdExecutor();
		cmd.run(cmdStr);

		String tmpResult = cmd.getStdOut();
		String tmpErr = cmd.getStdErr();
		converterLogger.info(cmdStr + " Error message:" + tmpErr);
		converterLogger.info(cmdStr + " Result message:" + tmpResult);

		logger.debug("jpg to pdf: " + cmdStr);
		logger.debug("jpgToPDF: " + jpgFile.getName() + ":" + tmpResult);
		return tmpResult;
	}

	public void setDPI(float dpi) {
		this.dpi = dpi;

	}

	public void setFactor(float resFactor) {
		this.resFactor = resFactor;

	}

	public void setSuccess(boolean b) {
		this.success = b;

	}

	/**
	 * for local run/test purpose
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			File source = new File("/tmp/window.pdf");
			File target = new File("/tmp/window_target.pdf");
			FlattenPDFCore.copyBookmarks(source, target);

		} catch (Exception e) {
			logger.error("Unexcepted error:", e);
		}
	}

	public boolean isUsePS2PDF() {
		return usePS2PDF;
	}

	public void setUsePS2PDF(boolean usePS2PDF) {
		this.usePS2PDF = usePS2PDF;
	}

	public boolean isImgOnlyFlatten() {
		return imgOnlyFlatten;
	}

	public void setImgOnlyFlatten(boolean imgOnlyFlatten) {
		this.imgOnlyFlatten = imgOnlyFlatten;
	}
	
	public int getQuality() {
		return quality;
	}

	public void setQuality(int quality) {
		this.quality = quality;
	}

}
