package digipages.BookConvert.Thumbnail;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.persistence.EntityManagerFactory;

import org.apache.pdfbox.pdmodel.PDDocument;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

//import digipages.BookConvert.FlattenPDF.FlattenPDFHandler;
import digipages.BookConvert.PDF2ePub.PDF2ePubHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.SimpleStreamToString;
import digipages.BookConvert.utility.Utility;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class PdfThumbnailHandler implements CommonJobHandlerInterface{
	private static String Tag = "[PdfThumbnail]";
	private static  Gson gson = new GsonBuilder().create();
	private static DPLogger logger = DPLogger.getLogger(PdfThumbnailHandler.class.getName());
	AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
	LogToS3 checkResultLogger;
	String originalBucket;
	private Object logFolder;
	private Object trialFolder;
	private Object originalFolder;

	public PdfThumbnailHandler() {
	}



	@Override
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
		CommonUtil.delDirs("/tmp");
		DataMessage message=null;
		RequestData requestData=null;
		originalBucket= config.getProperty("originalBucket");
        this.logFolder = config.getProperty("logFolder");
        this.trialFolder = config.getProperty("trialFolder");
        this.originalFolder = config.getProperty("originalFolder");
		try {
			message = job.getJobParam(DataMessage.class);
			logger.info(Tag + "message: " + gson.toJson(message));
			
//			AWSCredentials s3Credentials=getS3Cred(config);
			File pdfDir=CommonUtil.downloadPDFFromS3(message);
		
			requestData = gson.fromJson(message.getRequestData(), RequestData.class);
	        String folderPath ;
	        String checkResultgPath;
			folderPath = logFolder + "/" 
	        		+ (requestData.getIsTrial() ? trialFolder : originalFolder) +
	                "/" + requestData.getItem() + "/" + requestData.getBook_file_id();
			checkResultgPath = folderPath + "/" + "CheckResult.log";
			checkResultLogger = new LogToS3(s3Client, originalBucket, checkResultgPath);
		    long convertStartTime = System.currentTimeMillis();
		    // ImageMagick => use thumbnail option to shrink file
		    toThumbnail(config, pdfDir);
		    logger.info(Tag + "convertTime:" + (System.currentTimeMillis()-convertStartTime) + "ms" );
		
		    CannedAccessControlList permission = requestData.getIsTrial() ? 
		            CannedAccessControlList.PublicRead : CannedAccessControlList.AuthenticatedRead;
		    
		    String picDir = pdfDir.getAbsolutePath() + "/thumbnails";
		    logger.info(Tag+"upload To S3:" + picDir + " To " + message.getTargetBucket() + " " +message.getFolder());
		    
		    uplodadThumbnailsToS3(s3Client,message.getTargetBucket(), message.getFolder(), picDir, permission);
		    
		    
		    String thumbnailErrMsg=checkThumbnailSizeConsist(picDir).trim();
		    logger.info(Tag+ thumbnailErrMsg);
		    message.appendErrorMessage(thumbnailErrMsg);
		    
		    fixMissingThumbnails(message,permission);
		    zipThumbnails(message, permission);
		    
		    

		    pdfDir.delete();
		}catch (JobRetryException ex)
		{
			throw ex;
		}catch (Exception e)
		{
			// any other exception
			logger.error("Unexcepted error handling thumbnail.",e);
		}

	    try {
			schedulePDF2ePub(message,requestData);
		} catch (Exception e) {
			logger.error("Fail to issue Job flattenPDF ",e);
			throw new ServerException("id_err_999","Fail to issue job FlattenPDF");
		}
		
		return "PDFThumbnail Finished.";
	}
	private void fixMissingThumbnails(DataMessage dataMessage, CannedAccessControlList permission) throws Exception {
		
		/* [+] Count & record the number of pdf thumbnail */
		if (dataMessage.getBookFileName().contains(".pdf") && dataMessage.getResult()) {
			String thumbnailLog = "";

			ObjectListing objectList = s3Client.listObjects(dataMessage.getTargetBucket(),
					dataMessage.getFolder() + "/THUMBNAIL/");
			List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();

			while (objectList.isTruncated()) {
				objectList = s3Client.listNextBatchOfObjects(objectList);
				objectSummaries.addAll(objectList.getObjectSummaries());
			}
			logger.info(Tag + "In " + dataMessage.getFolder() + "/THUMBNAIL/ objectSummaries Size:"
					+ objectSummaries.size());

			String pdfPath = dataMessage.getFolder() + "/PDF/" + dataMessage.getBookFileName();

			RequestData requestData = gson.fromJson(dataMessage.getRequestData(), RequestData.class);

			String S3Bucket = dataMessage.getTargetBucket();
			String S3Key = pdfPath;
			String localPdfPath = "/tmp/PDF_" + requestData.getBook_file_id() + ".pdf";

			File pdfFile = Utility.downloadFromS3(s3Client, S3Bucket, S3Key, localPdfPath);
			InputStream bookStream = new FileInputStream(pdfFile);
			PDDocument sourceDocument = PDDocument.load(bookStream);

			int totalPage = sourceDocument.getNumberOfPages();

			// Get default failed thumbnail
			String failThumbnailName = "fail_thumbnail.jpg";
			InputStream tempFile = getClass().getClassLoader().getResourceAsStream(failThumbnailName);
			BufferedImage failThumbnail = ImageIO.read(tempFile);
			ByteArrayOutputStream failedImageOutputStream = new ByteArrayOutputStream();
			ImageIO.write(failThumbnail, "jpg", failedImageOutputStream);
			InputStream failedImageInputStream = new ByteArrayInputStream(failedImageOutputStream.toByteArray());
			String thumbnailLogPath = dataMessage.getLogFolder() + "/thumbnail.log";
			LogToS3 ThumbnailLogger = new LogToS3(s3Client, originalBucket, thumbnailLogPath);

			for (Integer i = 1; i <= totalPage; i++) {
				String number = "";
				Boolean isExist = false;

				for (int j = 1; j < objectSummaries.size(); j++) {
					S3ObjectSummary summary = objectSummaries.get(j);
					String fileName = summary.getKey();
					fileName = fileName.substring(fileName.lastIndexOf("/") + 1);

					if (fileName.contains("thumbnail") && fileName.contains(".jpg")) {
						number = fileName.replace("thumbnail", "").replace(".jpg", "");
						if (i.equals(Integer.valueOf(number))) {
							isExist = true;
							break;
						}
					}
				}

				String thumbnailNumber = "";

				if (i < 1000) {
					switch (i.toString().length()) {
					case 1:
						thumbnailNumber = "thumbnail000" + i;
						break;
					case 2:
						thumbnailNumber = "thumbnail00" + i;
						break;
					case 3:
						thumbnailNumber = "thumbnail0" + i;
						break;
					}
				}

				if (isExist) {
					ThumbnailLogger.log(GenericLogLevel.Info,thumbnailNumber + "complete",PdfThumbnailHandler.class.getName());
				} else {
					ThumbnailLogger.log(GenericLogLevel.Info,thumbnailNumber + "fail",PdfThumbnailHandler.class.getName());
					String thumbnailErrorMessage = thumbnailNumber + " failed.\r\n";
					logger.warn(Tag + thumbnailNumber + " failed.");
					checkResultLogger.log(GenericLogLevel.Warn,thumbnailErrorMessage,PdfThumbnailHandler.class.getName());
					checkResultLogger.uploadLog();
					// Upload default failed thumbnail.
					ObjectMetadata meta = new ObjectMetadata();
					meta.setContentLength(failedImageOutputStream.size());
					meta.setContentType("image/jpeg");
					failedImageInputStream.reset();
					PutObjectRequest putObjectRequest = new PutObjectRequest(dataMessage.getTargetBucket(),
							dataMessage.getFolder() + "/THUMBNAIL/" + thumbnailNumber + ".jpg", failedImageInputStream,
							meta).withCannedAcl(permission);
					s3Client.putObject(putObjectRequest);
				}
			}
			failedImageOutputStream.close();
			failedImageInputStream.close();
			tempFile.close();
			
			ThumbnailLogger.uploadLog();

			sourceDocument.close();
			bookStream.close();
			
		}
		/* [-] Count & record the number of pdf thumbnail */

		
	}

	private void zipThumbnails(DataMessage dataMessage, CannedAccessControlList permission ) throws Exception {
		/* [+] Zip PDF thumbnail and upload to S3 */
	   

		if (dataMessage.getBookFileName().contains(".pdf") && dataMessage.getResult()) {
			String zipFolder = "/tmp/ZipPic";
			new File(zipFolder).mkdirs();
			String zipFileName = "all_thumbnails.zip";
			ObjectListing objectList = s3Client.listObjects(dataMessage.getTargetBucket(),
					dataMessage.getFolder() + "/THUMBNAIL/");
			List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();

			// Download all thumbnail
			while (objectList.isTruncated()) {
				objectList = s3Client.listNextBatchOfObjects(objectList);
				objectSummaries.addAll(objectList.getObjectSummaries());
			}

			String downloadFolderPath = "/tmp/S3Download/thumbnail/";
			File downloadFolder = new File(downloadFolderPath);
			for (int i = 0; i < objectSummaries.size(); i++) {
				S3ObjectSummary summary = objectSummaries.get(i);
				String key = summary.getKey();

				if (key.lastIndexOf("/") != key.length() - 1) {
					String fileName = key.substring(key.lastIndexOf("/") + 1);

					if (fileName.contains(".jpg")) {

						String tempFilePath = downloadFolderPath + fileName;
						String tempFolderPath = tempFilePath.substring(0, tempFilePath.lastIndexOf("/") + 1);

						File tempfolder = new File(tempFolderPath);
						if (!tempfolder.exists()) {
							tempfolder.mkdirs();
						}

						String S3Bucket = summary.getBucketName();
						String S3Key = summary.getKey();
						Utility.downloadFromS3(s3Client, S3Bucket, S3Key, tempFilePath);
					}
				}
			}

			// Zip file
			byte[] buffer = new byte[1024];
			File zipFile = new File(zipFolder + File.separator + zipFileName);
			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);
			List<File> fileList = Utility.listAllFile(downloadFolder);

			for (File file : fileList) {
				if (file.isFile() && file.getName().contains(".jpg")) {
					ZipEntry ze = new ZipEntry(file.getPath().replace(downloadFolderPath, ""));
					zos.putNextEntry(ze);

					InputStream in = new FileInputStream(file);
					int len;
					while ((len = in.read(buffer)) > 0) {
						zos.write(buffer, 0, len);
					}

					in.close();
				}
			}

			zos.closeEntry();
			zos.close();
			fos.close();
			
			// upload zip file
			PutObjectRequest putObjectRequest = new PutObjectRequest(dataMessage.getTargetBucket(),
					dataMessage.getFolder() + "/THUMBNAIL/" + zipFileName, zipFile).withCannedAcl(permission);
			s3Client.putObject(putObjectRequest);
			zipFile.delete();
			Utility.deleteDir(downloadFolder);
		}
		/* [-] Zip PDF thumbnail and upload to S3 */
		
	}


	private void schedulePDF2ePub(DataMessage message, RequestData requestData) throws ServerException,IOException {
		ScheduleJob sj = new ScheduleJob();
		sj.setJobName(PDF2ePubHandler.class.getName());
		sj.setJobRunner(RunMode.ExtEc2Mode.getValue());
		sj.setJobParam(message);
		sj.setJobGroup(requestData.getBook_file_id());
		logger.debug("PDFThumbnailHandler sending Job " + sj.toGsonStr());
//		sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
		ScheduleJobManager.addJob(sj);
	}



	public static void uplodadThumbnailsToS3(AmazonS3 s3Client,
			String targetBucket, 
			String targetFolder, 
			String picDir, 
			CannedAccessControlList permission) throws ServerException {
        // Upload image to S3
		
		File dir = new File(picDir);
		int padding = calcPadding(dir.listFiles().length);
		StringBuilder sb = new StringBuilder();
		for (File child: dir.listFiles())
		{
	        String pageNumber = getNumFromFileName(padding,child.getName());
	        String targetPath =targetFolder + "/THUMBNAIL/thumbnail" + pageNumber + ".jpg"; 
	        PutObjectRequest rq = new PutObjectRequest(
	        		targetBucket, targetPath, child);
	        rq.withCannedAcl(permission);
	        s3Client.putObject(rq);
	        sb.append("[send file to S3]: ").append(targetBucket).append(" ").append(targetPath).append("\r\n");
		}
        logger.info(sb.toString());
	}

	
	/**
	 * check if thumbnail size is similar, if not matched, return Error message
	 * @param thumbDir
	 * @return which page is different from previous page
	 */
	public static String  checkThumbnailSizeConsist(String thumbDir) {
		File f = new File(thumbDir);
		Dimension previousSize=null;
		StringBuilder sb = new StringBuilder();
		File[] files = f.listFiles();
		Arrays.sort(files);
		
		// different File: (skip last)
		for (int i=0; i< files.length-1; i++)
		{
			File thumbnailFile = files[i];
			
			// first
			Dimension curSize = getSize(thumbnailFile);
			if (previousSize==null || curSize==null) // skip first, skip wrong format.
			{
				previousSize = curSize;
				continue;
			}
			
			if (dimensionSimilar(previousSize,curSize))
			{
				previousSize = curSize;
				continue;
			}else
			{
				sb.append("\r\nWarning: Thumbnail dimension mismatch. Please check pdf format. ") 
						.append(thumbnailFile.getName());
				break;
			}
		}
		
		return sb.toString(); 
		
	}
	

	private static boolean dimensionSimilar(Dimension previousSize, Dimension curSize) {
		double widthDiff=Math.abs(previousSize.getWidth()-curSize.getWidth());
		double heightDiff = Math.abs(previousSize.getHeight()-curSize.getHeight());
		
		if (widthDiff/curSize.getWidth()>0.10)
			return false;
		if (heightDiff/curSize.getHeight() >0.10)
			return false;
		
		return true;
	}



	private static Dimension getSize(File thumbnailFile) {
		Dimension d = null;
		BufferedImage img = null;
		try {
			img = ImageIO.read(thumbnailFile);
			d = new Dimension(img.getWidth(), img.getHeight());
		} catch (javax.imageio.IIOException iio)
		{
			logger.info("warning unsupported thumbnail jpg format " + thumbnailFile.getName());
		}
		catch (Exception e) {
			logger.error("Error handling thumbnail " + thumbnailFile.getName() ,e);
		}
		return d;
	}

	//
	public static int calcPadding(int length) {
		int padding=4;
		int log = ((int) Math.floor(Math.log10(length)))+1;
		if (log>padding)
		{
			padding = log;
		}

		return padding;
	}

	/**
	 * input format: thumbnail-1.jpg
	 * @param name 
	 * @return 001
	 */
	public static String getNumFromFileName(int padding,String name) {
		String ret = "001";
		
		String x = name.replace("thumbnail-", "").replace(".jpg", "");
		int num=0;
		try{
			num=Integer.parseInt(x);
		}catch (Exception e)
		{}
		// 1 based.
		x = String.valueOf( num+1);
		ret = padding(padding,x);
		return ret;
	}

	
	private static String padding(int padding, String x) {
		StringBuilder sb = new StringBuilder();
		sb.append(x);
		while (sb.length()<padding)
		{
			sb.insert(0, "0");
		}
		return sb.toString();
	}
	


	private void toThumbnail(Properties config, File dirPath) throws ServerException, JobRetryException{
        Process p;
        // background white, alpha remove prevent black background thumbnails.
        final Float maxWidth = Float.valueOf(config.getProperty("MAX_WIDTH","210"));
        final Float maxHeight = Float.valueOf(config.getProperty("MAX_HEIGHT","280"));
        File thumbnailDir= new File(dirPath.getAbsolutePath() + "/thumbnails/");
		thumbnailDir.mkdirs();
        
        String cmd = "convert -thumbnail "+maxWidth+"x"+maxHeight+
        		" -background white -alpha remove -colorspace sRGB -define pdf:use-cropbox=true " 
        		+ dirPath.getAbsolutePath() + "/Source.pdf" + 
        		" " + dirPath.getAbsolutePath() + "/thumbnails/thumbnail.jpg";
        
        logger.info(Tag + "Command="+ cmd);
        try {
			p = Runtime.getRuntime().exec(cmd);
	        SimpleStreamToString errResult = new SimpleStreamToString(p.getErrorStream());
	        SimpleStreamToString result = new SimpleStreamToString(p.getInputStream());
	        result.start();
	        errResult.start();
	        boolean succ=p.waitFor(30,TimeUnit.MINUTES);
	        String errLine=errResult.getMessage();
	        String line = result.getMessage();
	        int fileCnt = thumbnailDir.listFiles().length; 
	        if ((errLine!=null && 
	        		!errLine.isEmpty() && 
	        		(errLine.contains("error") && !errLine.contains("may be incorrect"))) 
	        		|| !succ || fileCnt < 3)
	        {
	        	logger.error("Fail to convert Thumbnail, message = " + line + " errMessage=" + errLine);
	        	throw new JobRetryException("Thumbnail Fail, retrying");
	        }
	        result.close();
	        errResult.close();
	        p.getInputStream().close();
		} catch (IOException|InterruptedException e) {
			logger.error("Fail to Run Thumbnail, with error:",e);
			throw new ServerException("id_err_999","Fail to Run Thumbail with Error" + e.getMessage());
		}
        
	}



	public static String readProcessResult(Process p) throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
        String line;
        while((line = reader.readLine()) != null) {
            logger.info(Tag + "Result="+ line);
        }
        reader.close();
        
        if (line==null)
        	line="";
		return line;
	}
	
	public static String readProcessError(Process p) throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(p.getErrorStream(), "UTF-8"));
        String line ;
        while((line = reader.readLine()) != null) {
            logger.info(Tag + "Result="+ line);
        }
        reader.close();
        if (line==null)
        	line="";
		return line;
	}



	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		//not used.
	}
	
	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		
		return RunMode.ExtEc2Mode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		
		Gson gson = new Gson();
		DataMessage message= job.getJobParam(DataMessage.class);
		String reqData=message.getRequestData();
		RequestData req = gson.fromJson(reqData, RequestData.class);
		String format=req.getFormat();
		logger.debug("Format: " + format);
		if ("pdf".equalsIgnoreCase(format)){
			return RunMode.ExtEc2Mode;
		}
		return RunMode.LambdaMode;
	}
	
	// for test purpose 
	public static void main(String args[])
	{
		if (args.length<1)
		{
			logger.error("no path defined.");
		}
		String path = args[0];
		
		logger.debug(checkThumbnailSizeConsist(path));
	}

}
