package digipages.BookConvert.ElasticSearch;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.google.common.base.Supplier;

import digipages.BookConvert.utility.GetPropertyValues;
import digipages.BookConvert.utility.RequestData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
//elasticsearch
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.indices.CreateIndex;
import model.ScheduleJob;
import vc.inreach.aws.request.AWSSigner;
import vc.inreach.aws.request.AWSSigningRequestInterceptor;

/**
 * still not all set (ElasticSearch Server not ready)
 * @author Eric.CL.Chou
 *
 */
public class ElasticSearch implements CommonJobHandlerInterface {
	
    //deployer
    private AmazonS3 s3Client;
    private String ElasticSearchServer;
    private static final DPLogger logger = DPLogger.getLogger(ElasticSearch.class.getName());
    
	@Override
	/**
	 * 
	 * @param config
	 * @param job
	 * @return
	 * @throws ServerException
	 * @throws JobRetryException
	 */
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
        try{
            GetPropertyValues properties = new GetPropertyValues();
            ElasticSearchServer = properties.getPropValues("ElasticSearchServer");

            RequestData requestData = job.getJobParam( RequestData.class);

            String srcBucket =requestData.getEfile_url();
            
            String srcKey ="";
            if (requestData.getIsTrial() && requestData.getPreview_type().equals("3")) {
                srcKey = requestData.getPreview_content();
            } else {
                if (!requestData.getEfile_nofixed_name().isEmpty()) {
                    srcKey = requestData.getEfile_nofixed_name();
                }
            }
            logger.info("srcKey: " + srcKey + " srcBucket: " + srcBucket);
            
            // Create S3 Client
            s3Client = AmazonS3ClientBuilder.defaultClient();
            // Download the Object from S3 into a stream 
            Map<String, String> map = new HashMap<>();
            String bucket;
            String path;
            
            bucket = requestData.getEfile_url().substring(0, requestData.getEfile_url().indexOf('/'));
            path = requestData.getEfile_url().substring(requestData.getEfile_url().indexOf('/') + 1 ,requestData.getEfile_url().length());
            map.put("bucket", bucket);
            map.put("path", path);
            
            S3Object s3Object = s3Client.getObject(new GetObjectRequest(map.get("bucket"),
                    map.get("path") + srcKey));
            
            S3ObjectInputStream infile = s3Object.getObjectContent();  
            
//            EpubReader epubReader = new EpubReader();
            
            Supplier<LocalDateTime> clock = () -> LocalDateTime.now();
            final AWSSigner awsSigner = new AWSSigner(InstanceProfileCredentialsProvider.getInstance(), "ap-northeast-1", "es", clock);
            final AWSSigningRequestInterceptor requestInterceptor = new AWSSigningRequestInterceptor(awsSigner);
            final JestClientFactory factory = new JestClientFactory()             
            {
                @Override
                protected HttpClientBuilder configureHttpClient(HttpClientBuilder builder) {
                    builder.addInterceptorLast(requestInterceptor);
                    return builder;
                }
                @Override
                protected HttpAsyncClientBuilder configureHttpClient(HttpAsyncClientBuilder builder) {
                    builder.addInterceptorLast(requestInterceptor);
                    return builder;
                }
            };            
            

            factory.setHttpClientConfig(new HttpClientConfig
                                   .Builder(ElasticSearchServer)
                                   .multiThreaded(true)
                                   .build());
            JestClient client = factory.getObject();        
                       
            client.execute(new CreateIndex.Builder("books").build());
            /*
            Settings settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", "122337750860:test").build();
            Client client = new TransportClient(settings)        
                            .addTransportAddress(new InetSocketTransportAddress("54.65.236.193", 9300));
            */
            //
//            Book testBook = epubReader.readEpub(infile);
//            
//            Spine spine = testBook.getSpine(); 
//            List<SpineReference> spineList = spine.getSpineReferences() ;
//            int count = spineList.size();
//            for (int i=0;i<count;i++){
//                String curCFI="epubcfi(/6/";
//                curCFI=curCFI+(i+1)*2+"!/";
//                
//                Resource res =spine.getResource(i);
//                StringBuilder sb = readContent(res);
//                Document doc = Jsoup.parse(sb.toString());
//                DOMParser.searchNote(doc.children(),curCFI,client,requestData.getBook_file_id());
//                
//            }

        	infile.close();
        }catch (Exception e){
        	logger.error("Unexcepted Error in ElasticSearch",e);
        }
        return "ElasticSearch index ok";
    }


//	private StringBuilder readContent(Resource res) throws IOException {
//        StringBuilder sb = new StringBuilder();
//        String linez;
//
//        InputStream is = res.getInputStream();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));
//        
//        while ((linez = reader.readLine()) != null) {
//        	logger.debug("read line: "+linez);
//           	sb.append(linez);
//        }
//        is.close();
//		return sb;
//	}


	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// no DB access needed.
	}
	
	/**
	 * 
	 * @param runMode
	 * @return
	 * @throws Exception
	 */
	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.ExtEc2Mode;
	}

	
	public static RunMode getInitRunMode(ScheduleJob job) {
		//TODO fill me.
		return RunMode.LambdaMode;
	}
}