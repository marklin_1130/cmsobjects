package digipages.BookConvert.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;

public class GenericLogRecord {
    private String time; 
	private GenericLogLevel level; // FATAL, ERROR, RETRY, WARNING, Info, Debug 
	private String message; // main Message, ex: fail to generate thumb nail in aaa.pdf page 123
	private String source; // ClassName (ex: PdfThumbnailHandler, FlattenPDFHandler ...)
	static Gson gson = new Gson();

	@Override
	public String toString()
	{
		return gson.toJson(this);
	}
	
	public GenericLogRecord(GenericLogLevel level, String message, String source)
	{
	    this.time = new SimpleDateFormat("yyyy/MM/dd/ HH:mm:ss.SSS").format(new Date());
		this.level=level;
		this.message=message;
		this.source=source;
	}
	
	public static GenericLogRecord fromString(String jsonStr)
	{
		return gson.fromJson(jsonStr, GenericLogRecord.class);
	}

	public GenericLogLevel getLevel() {
		return level;
	}

	public void setLevel(GenericLogLevel level) {
		this.level = level;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

    public String getTime() {
        return time;
    }

    public static Gson getGson() {
        return gson;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public static void setGson(Gson gson) {
        GenericLogRecord.gson = gson;
    }

}
