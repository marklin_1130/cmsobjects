package digipages.BookConvert.utility;

import java.util.Set;

public class ResponseData {
    private String item;
    private String call_type;
    private String processing_id;
    private String file_url;
    private String file_cover_url;
    private int return_file_num;
    private Object r_data;
    
    public String getFile_cover_url() {
        return file_cover_url;
    }
    public void setFile_cover_url(String file_cover_url) {
        this.file_cover_url = file_cover_url;
    }
    
    public String getFile_url() {
        return file_url;
    }
    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }
    
    public String getItem() {
        return item;
    }
    public void setItem(String item) {
        this.item = item;
    }

    public String getCall_type() {
        return call_type;
    }
    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getProcessing_id() {
        return processing_id;
    }
    public void setProcessing_id(String processing_id) {
        this.processing_id = processing_id;
    }

    public int getReturn_file_num() {
        return return_file_num;
    }
    public void setReturn_file_num(int return_file_num) {
        this.return_file_num = return_file_num;
    }

    public Object getR_data() {
        return r_data;
    }
    public void setR_data(Object r_data) {
        this.r_data = r_data;
    }

    public class ResultData {
        private Boolean result;
        private String size;
        private String version;
        private String checksum;
        private String error_code;
        private String error_message;
        private Set<String> media;
        
        public Set<String> getMedia() {
            return media;
        }
        public void setMedia(Set<String> media) {
            this.media = media;
        }
        public String getChecksum() {
            return checksum;
        }
        public void setChecksum(String checksum) {
            this.checksum = checksum;
        }
        
        public Boolean getResult() {
            return result;
        }
        public void setResult(Boolean result) {
            this.result = result;
        }
        
        public String getSize() {
            return size;
        }
        public void setSize(String size) {
            this.size = size;
        }
        
        public String getVersion() {
            return version;
        }
        public void setVersion(String version) {
            this.version = version;
        }
        
        public String getError_code() {
            return error_code;
        }
        public void setError_code(String error_code) {
            this.error_code = error_code;
        }
        
        public String getError_message() {
            return error_message;
        }
        public void setError_message(String error_message) {
            this.error_message = error_message;
        }
    }
}