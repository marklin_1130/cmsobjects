package digipages.BookConvert.utility.vod;

public class MediabookConvertDataMessage {
	private String itemId;
    private String targetBucket;
    private String folder;
    private String requestData;
    private Boolean result;
    private String errorMessage="";
    private String logFolder;
	private int bookFileId;
	private int bookFileTrialId;
	public String getTargetBucket() {
		return targetBucket;
	}
	public void setTargetBucket(String targetBucket) {
		this.targetBucket = targetBucket;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getRequestData() {
		return requestData;
	}
	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}
	public Boolean getResult() {
		return result;
	}
	public void setResult(Boolean result) {
		this.result = result;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getLogFolder() {
		return logFolder;
	}
	public void setLogFolder(String logFolder) {
		this.logFolder = logFolder;
	}
	public int getBookFileId() {
		return bookFileId;
	}
	public void setBookFileId(int bookFileId) {
		this.bookFileId = bookFileId;
	}
	public int getBookFileTrialId() {
		return bookFileTrialId;
	}
	public void setBookFileTrialId(int bookFileTrialId) {
		this.bookFileTrialId = bookFileTrialId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

}