package digipages.BookConvert.utility.vod;

import java.util.List;

import com.google.gson.annotations.Expose;

import model.MediabookAttach;
import model.MediabookChapter;

public class MediaBookRequestData {
	@Expose
	private String author;
	@Expose
	private String book_file_id;
	@Expose
	private String c_title;
	@Expose
	private String o_title;
	@Expose
	private String call_type;
	@Expose
	private int count_TimeoutMointor;
	@Expose
	private String efile_cover_url;
	@Expose
	private Boolean forceCheck = false;
	@Expose
	private String format;
	@Expose
	private String intro;
	@Expose
	private String isbn;
	@Expose
	private String issn;
	@Expose
	private String item;
	@Expose
	private String language;
	@Expose
	private String preview_content;
	@Expose
	private String preview_type;
	@Expose
	private String publish_date;
	@Expose
	private String publisher_b_id;
	@Expose
	private String publisher_name;
	@Expose
	private String request_time;
	@Expose
	private int return_file_num;
	@Expose
	private String s3Key;
	@Expose
	private String s3Secret;
	@Expose
	private String status;
	@Expose
	private String version;
	@Expose
	private String type;
	@Expose
	private List<MediabookChapter> mediabookChapterList;
	@Expose
	private List<MediabookAttach> mediabookAttachList;
	@Expose
	private Long convertId;
	@Expose
	private String package_flag;
	@Expose
	private String package_type;
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getBook_file_id() {
		return book_file_id;
	}

	public void setBook_file_id(String book_file_id) {
		this.book_file_id = book_file_id;
	}

	public String getC_title() {
		return c_title;
	}

	public void setC_title(String c_title) {
		this.c_title = c_title;
	}

	public String getO_title() {
		return o_title;
	}

	public void setO_title(String o_title) {
		this.o_title = o_title;
	}

	public String getCall_type() {
		return call_type;
	}

	public void setCall_type(String call_type) {
		this.call_type = call_type;
	}

	public int getCount_TimeoutMointor() {
		return count_TimeoutMointor;
	}

	public void setCount_TimeoutMointor(int count_TimeoutMointor) {
		this.count_TimeoutMointor = count_TimeoutMointor;
	}

	public String getEfile_cover_url() {
		return efile_cover_url;
	}

	public void setEfile_cover_url(String efile_cover_url) {
		this.efile_cover_url = efile_cover_url;
	}

	public Boolean getForceCheck() {
		return forceCheck;
	}

	public void setForceCheck(Boolean forceCheck) {
		this.forceCheck = forceCheck;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPreview_content() {
		return preview_content;
	}

	public void setPreview_content(String preview_content) {
		this.preview_content = preview_content;
	}

	public String getPreview_type() {
		return preview_type;
	}

	public void setPreview_type(String preview_type) {
		this.preview_type = preview_type;
	}

	public String getPublish_date() {
		return publish_date;
	}

	public void setPublish_date(String publish_date) {
		this.publish_date = publish_date;
	}

	public String getPublisher_b_id() {
		return publisher_b_id;
	}

	public void setPublisher_b_id(String publisher_b_id) {
		this.publisher_b_id = publisher_b_id;
	}

	public String getPublisher_name() {
		return publisher_name;
	}

	public void setPublisher_name(String publisher_name) {
		this.publisher_name = publisher_name;
	}

	public String getRequest_time() {
		return request_time;
	}

	public void setRequest_time(String request_time) {
		this.request_time = request_time;
	}

	public int getReturn_file_num() {
		return return_file_num;
	}

	public void setReturn_file_num(int return_file_num) {
		this.return_file_num = return_file_num;
	}

	public String getS3Key() {
		return s3Key;
	}

	public void setS3Key(String s3Key) {
		this.s3Key = s3Key;
	}

	public String getS3Secret() {
		return s3Secret;
	}

	public void setS3Secret(String s3Secret) {
		this.s3Secret = s3Secret;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<MediabookChapter> getMediabookChapterList() {
		return mediabookChapterList;
	}

	public void setMediabookChapterList(List<MediabookChapter> mediabookChapterList) {
		this.mediabookChapterList = mediabookChapterList;
	}

	public List<MediabookAttach> getMediabookAttachList() {
		return mediabookAttachList;
	}

	public void setMediabookAttachList(List<MediabookAttach> mediabookAttachList) {
		this.mediabookAttachList = mediabookAttachList;
	}

    public Long getConvertId() {
        return convertId;
    }

    public void setConvertId(Long convertId) {
        this.convertId = convertId;
    }

    public String getPackage_flag() {
        return package_flag;
    }

    public void setPackage_flag(String package_flag) {
        this.package_flag = package_flag;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

}