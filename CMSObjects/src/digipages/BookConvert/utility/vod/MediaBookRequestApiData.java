package digipages.BookConvert.utility.vod;

import java.util.List;

import com.google.gson.annotations.Expose;

public class MediaBookRequestApiData {
	
//	@Expose
//	private String dynamic_profile_option;
	@Expose
	private String item_id;
	@Expose
	private String chapter_no;
	@Expose
	private String processing_id;
	@Expose
	private String convert_flow;
	@Expose
	private String chapter_name;
	@Expose
	private String chapter_input;
	@Expose
	private String preview_type;
	@Expose
	private String preview_content;
	@Expose
	private Integer preview_length;
	@Expose
	private String profile_name;
	
	@Expose
	private List<SubtitleRequestApiData> subtitle;
//    public String getDynamic_profile_option() {
//        return dynamic_profile_option;
//    }
    public String getItem_id() {
        return item_id;
    }
    public String getChapter_no() {
        return chapter_no;
    }
    public String getProcessing_id() {
        return processing_id;
    }
    public String getConvert_flow() {
        return convert_flow;
    }
    public String getChapter_name() {
        return chapter_name;
    }
    public String getChapter_input() {
        return chapter_input;
    }
    public String getPreview_type() {
        return preview_type;
    }
    public String getPreview_content() {
        return preview_content;
    }
    public Integer getPreview_length() {
        return preview_length;
    }
    public List<SubtitleRequestApiData> getSubtitle() {
        return subtitle;
    }
//    public void setDynamic_profile_option(String dynamic_profile_option) {
//        this.dynamic_profile_option = dynamic_profile_option;
//    }
    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
    public void setChapter_no(String chapter_no) {
        this.chapter_no = chapter_no;
    }
    public void setProcessing_id(String processing_id) {
        this.processing_id = processing_id;
    }
    public void setConvert_flow(String convert_flow) {
        this.convert_flow = convert_flow;
    }
    public void setChapter_name(String chapter_name) {
        this.chapter_name = chapter_name;
    }
    public void setChapter_input(String chapter_input) {
        this.chapter_input = chapter_input;
    }
    public void setPreview_type(String preview_type) {
        this.preview_type = preview_type;
    }
    public void setPreview_content(String preview_content) {
        this.preview_content = preview_content;
    }
    public void setPreview_length(Integer preview_length) {
        this.preview_length = preview_length;
    }
    public void setSubtitle(List<SubtitleRequestApiData> subtitle) {
        this.subtitle = subtitle;
    }
	public String getProfile_name() {
		return profile_name;
	}
	public void setProfile_name(String profile_name) {
		this.profile_name = profile_name;
	}
	
	
	
}