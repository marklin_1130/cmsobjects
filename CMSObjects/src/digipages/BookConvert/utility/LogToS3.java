package digipages.BookConvert.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import digipages.BookConvert.BookMain.CheckResult;
import digipages.common.DPLogger;


/**
 * log into S3 space
 * @author Eric.CL.Chou
 *
 */
public class LogToS3 {
    private AmazonS3 s3Client;
    private String bucket;
    private String logPath;
    private static DPLogger logger=DPLogger.getLogger(LogToS3.class.getName());
    private ArrayList<String> logList=new ArrayList<>();
    private CannedAccessControlList filePermission = CannedAccessControlList.AuthenticatedRead;
    
    /**
     * 
     * @param s3Client
     * @param bucket
     * @param logPath
     */
    public LogToS3(AmazonS3 s3Client, String bucket, String logPath) {
        this.s3Client = s3Client;
        this.bucket = bucket;
        this.logPath = logPath;
    }
    
    /**
     * empty logger, just for test
     */
    public LogToS3()
    {
    	// empty, for Unit Test
    }
    
    /**
     * 
     * @param level
     * @param message
     * @param source
     */
    public void log(GenericLogLevel level, String message, String source)
    {
    	// skip empty message.
    	if (null ==message || message.isEmpty())
    		return;
    	log(new GenericLogRecord(level,message,source));
    }
    
    
    private String getStackTraceParent() {
    	String ret ;
    	StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
    	int i=0;
    	for (; i< stackTraceElements.length;i++)
    	{
    		StackTraceElement curElement=stackTraceElements[i];
    		logger.debug("Dump Trace: " + i +":"+ curElement.getClassName()
    			+ "."+ curElement.getMethodName() 
    			+ ":" + curElement.getLineNumber());

    		// skip some native thing.
    		if (curElement.getClassName().equals(Thread.class.getName()))
    			continue;
    		if (!curElement.getClassName().equals(LogToS3.class.getName())){
    			break;
    		}
    	}
    	ret = stackTraceElements[i].getClassName();
		return ret;
	}

    /**
     * info level.
     * @param message
     */
    public void info(String message)
    {
//    	String source = getStackTraceParent();
    	log(GenericLogLevel.Info,message,"");
    }
    /**
     * 
     * @param message
     * @param source
     */
    public void info(String message, String source)
    {
    	log(GenericLogLevel.Info,message,source);
    }

    /**
     * log error message.
     * @param message
     */
    public void error(String message)
    {
    	log(GenericLogLevel.Error,message,getStackTraceParent());
    }
    
    /**
     * log error message.
     * @param message
     * @param sourceName (className)
     */
    public void error(String message,String source)
    {
    	log(GenericLogLevel.Error,message,source);
    }
    
    /**
     * log debug message
     * @param message
     */
    public void debug(String message)
    {
    	log(GenericLogLevel.Debug,message,getStackTraceParent());
    }
    
    /**
     * log debug message
     * @param message
     * @param source
     */
    public void debug(String message,String source)
    {
    	log(GenericLogLevel.Debug,message,source);
    }
    
    /**
     * log fatal message
     * @param message
     */
    public void fatal(String message)
    {
    	log(GenericLogLevel.Fatal,message,getStackTraceParent());
    }
    
    /**
     * log fatal message
     * @param message
     * @param source
     */
    public void fatal(String message,String source)
    {
    	log(GenericLogLevel.Fatal,message,source);
    }

    /**
     * log warn message.
     * @param message
     */
	public void warn(String message) {
		log(GenericLogLevel.Warn,message,getStackTraceParent());
    }
	
    /**
     * log warn message.
     * @param message
     */
	public void warn(String message,String tag) {
		log(GenericLogLevel.Warn,message,tag);
    }

    private void log(GenericLogRecord record){
        logList.add(record.toString());
    }
    
    /**
     * log in info level
     * @param tag
     * @param log
     */
    public void log(String tag, String log){
    	this.info(log,tag);
    }
    
    /**
     * upload Log to S3 Bucket, if already exists
     * do a merge. (download, append, upload)
     */
    public void uploadLog() {
    	this.sortLogMsg();
    	if (null==s3Client)
    	{
    		logger.warn("Warning no S3 Client to send data.");
    		return;
    	}
        try {
        	logger.debug("uploading Log: " + logPath);
            if(isExist()) {
                String localPath = "/tmp/oldLog.log";
                File oldLog = Utility.downloadFromS3(s3Client, bucket, logPath, localPath);
                BufferedReader reader = new BufferedReader(new InputStreamReader( new FileInputStream(oldLog), "UTF-8"));
                File newLog = new File("/tmp/newLog.log");
                BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newLog), "UTF-8"));
                
                ArrayList<String> content = new ArrayList<String>();
                String buf;            
                while ((buf = reader.readLine()) != null) {
                    content.add(buf);
                }                
                content.addAll(logList);
                reader.close();
                
                for (int i = 0; i < content.size(); i++) {
                    writter.write(content.get(i) + "\r\n");
                }
                writter.close();
                
                s3Client.putObject(new PutObjectRequest(bucket, logPath, newLog)
                        .withCannedAcl(filePermission));
                
                oldLog.delete();
                newLog.delete();
            } else {
                File newLog = File.createTempFile("LogToS3", ".log");
                BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newLog), "UTF-8"));
                for (int i = 0; i < logList.size(); i++) {
                    writter.write(logList.get(i) + "\r\n");
                }
                writter.close();
                
                s3Client.putObject(new PutObjectRequest(bucket, logPath, newLog)
                        .withCannedAcl(filePermission));
                
                
                newLog.delete();
            }
        } catch (Exception e) {
        	logger.error("Fail Uploading log to S3",e);
        }
    }
    
    /**
     * upload Log to S3 Bucket, if already exists
     * do a merge. (download, append, upload)
     */
    public void uploadLogWithItemId(String itemId) {
        this.sortLogMsg();
        if (null==s3Client)
        {
            logger.warn("Warning no S3 Client to send data.");
            return;
        }
        try {
            logger.debug("uploading Log: " + logPath);
            if(isExist()) {
                String localPath = "/tmp/"+itemId+"oldLog.log";
                File oldLog = Utility.downloadFromS3(s3Client, bucket, logPath, localPath);
                BufferedReader reader = new BufferedReader(new InputStreamReader( new FileInputStream(oldLog), "UTF-8"));
                File newLog = new File("/tmp/"+itemId+"newLog.log");
                BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newLog), "UTF-8"));
                
                ArrayList<String> content = new ArrayList<String>();
                String buf;            
                while ((buf = reader.readLine()) != null) {
                    content.add(buf);
                }                
                content.addAll(logList);
                reader.close();
                
                for (int i = 0; i < content.size(); i++) {
                    writter.write(content.get(i) + "\r\n");
                }
                writter.close();
                
                s3Client.putObject(new PutObjectRequest(bucket, logPath, newLog)
                        .withCannedAcl(filePermission));
                
                oldLog.delete();
                newLog.delete();
            } else {
                File newLog = File.createTempFile("LogToS3", ".log");
                BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newLog), "UTF-8"));
                for (int i = 0; i < logList.size(); i++) {
                    writter.write(logList.get(i) + "\r\n");
                }
                writter.close();
                
                s3Client.putObject(new PutObjectRequest(bucket, logPath, newLog)
                        .withCannedAcl(filePermission));
                
                
                newLog.delete();
            }
        } catch (Exception e) {
            logger.error("Fail Uploading log to S3",e);
        }
    }
    
    private Boolean isExist() {
        ObjectListing objectList = s3Client.listObjects(
                new ListObjectsRequest().withBucketName(bucket).withPrefix(logPath));
        List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();
        
        while (objectList.isTruncated()) {
            objectList = s3Client.listNextBatchOfObjects(objectList);
            objectSummaries.addAll(objectList.getObjectSummaries());
        }

        for (S3ObjectSummary summary : objectSummaries) {
            if (summary.getKey().equals(logPath)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param src
     */
	public void addAll(CheckResult src) {
		if (src==null)
			return;
		for (GenericLogRecord log:src.listMessages())
		{
			this.log(log);
		}
	}

	public String dumpMessage() {
		
		StringBuilder sb = new StringBuilder();
		for (String tmpLog:logList)
		{
			sb.append(tmpLog).append("\n");
		}
		return sb.toString();
	}

	public void sortLogMsg () {
		
		logList.sort(new LogLevelComparator());
		
	}

}

class LogLevelComparator implements Comparator<String> {
	@Override
	public int compare(String s1, String s2) {

		int s1score = getCompareScore(s1);
		int s2score = getCompareScore(s2);
		int result = 0;
		if (s1score < s2score) {
			result = -1;
		} else if (s1score == s2score) {
			result = 0;
		} else if (s1score > s2score) {
			result = 1;
		}

		return result;
	}

	private int getCompareScore(String logmsg) {

		int score = 0;
		
		String logTop1 ="Fatal";
		String logTop1_1 = "試閱轉檔大小超過正式檔案";
		String logTop1_2 = "試閱轉檔資料夾大小";
		String logTop1_3 = "試閱處理失敗";
		String logTop2 = "script found";
		String logTop2_1 = "影音檔 js script found";
		String logTop3 = "影音檔案超過";
		String logTop4 = "Font name";
		String logTop5 = "出現vh";
		String logTop6 = "Cannot extract Text";
		String logTop7 = "PDF Version 1.3 or below detected";
		String logTop8 = "Traceback";
		String logTop9= "無法轉檔成功";
		String logTop10 = "Paragraph is too big";
		String logTop11 = "negative margin";
		String logTop12 = "negative text-indent";


		if (logmsg.contains(logTop1)||logmsg.contains(logTop1_1)||logmsg.contains(logTop1_2)||logmsg.contains(logTop1_3)) {
			score = 1;
		} else if (logmsg.contains(logTop2)||logmsg.contains(logTop2_1)) {
			score = 2;
		} else if (logmsg.contains(logTop3)) {
			score = 3;
		} else if (logmsg.contains(logTop4)) {
			score = 4;
		} else if (logmsg.contains(logTop5)) {
			score = 5;
		} else if (logmsg.contains(logTop6)) {
			score = 6;
		} else if (logmsg.contains(logTop7)) {
			score = 7;
		} else if (logmsg.contains(logTop8)) {
			score = 8;
		} else if (logmsg.contains(logTop9)) {
			score = 9;
		} else if (logmsg.contains(logTop10)) {
			score = 10;
		} else if (logmsg.contains(logTop11)) {
			score = 11;
		} else if (logmsg.contains(logTop12)) {
			score = 12;
		}
		
		return score;
	}
}
