package digipages.BookConvert.utility;

import java.util.Properties;

public class ConvertConfig {

	private Properties config;
	private String tmpBucket;

	public ConvertConfig() {
	}

	public ConvertConfig(Properties conf) {
		tmpBucket = conf.getProperty("tmpBucket", "tmpBucket");
		this.config=conf;
	}


	public String getTmpBucket() {
		
		return tmpBucket;
	}


}
