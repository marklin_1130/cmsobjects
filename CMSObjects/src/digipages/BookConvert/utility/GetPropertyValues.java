package digipages.BookConvert.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetPropertyValues {
	String result = "";
	static InputStream inputStream;
	static Properties prop = new Properties();

	public static Properties loadProperties() throws IOException {
		String propFileName = "config.properties";
		inputStream = GetPropertyValues.class.getClassLoader().getResourceAsStream(propFileName);
		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
		return prop;
	}

	public String getPropValues(String Property) throws IOException {

		String propFileName = "config.properties";
		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}

		// get the property value
		result = prop.getProperty(Property);
		inputStream.close();

		return result;
	}

	public static Properties getProperty() {
		return prop;
	}
}
