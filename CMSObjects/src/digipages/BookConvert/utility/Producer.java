package digipages.BookConvert.utility;

import java.util.concurrent.BlockingQueue;

import digipages.common.DPLogger;
import model.ScheduleJob;

public class Producer implements Runnable {
	
	private static DPLogger logger = DPLogger.getLogger(Producer.class.getName());
	
	private BlockingQueue<ScheduleJob> bq = null;

	public Producer(BlockingQueue<ScheduleJob> queue) {
		this.bq = queue;
	}

	public void run() {
		try {
			ScheduleJob scheduleJob = new ScheduleJob();

			scheduleJob.setJobName("ScheduleJob");

			logger.info("Produced: " + scheduleJob);

			bq.put(scheduleJob);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}