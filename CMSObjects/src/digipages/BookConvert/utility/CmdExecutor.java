package digipages.BookConvert.utility;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * execute Command and get Result in both stdout and stderr
 * @author Eric.CL.Chou
 *
 */
public class CmdExecutor {
	private String stdErr="";
	private String stdOut="";
	public void run(String cmd) throws IOException, InterruptedException
	{
		Process p = Runtime.getRuntime().exec(cmd);
        SimpleStreamToString errResult = new SimpleStreamToString(p.getErrorStream());
        SimpleStreamToString result = new SimpleStreamToString(p.getInputStream());
        result.start();
        errResult.start();
        boolean succ=p.waitFor(60,TimeUnit.MINUTES);
        stdErr=errResult.getMessage();
        stdOut = result.getMessage();
        result.close();
        errResult.close();
        p.getOutputStream().close();
	}
	public String getStdErr()
	{
		return stdErr;
	}
	public String getStdOut()
	{
		return stdOut;
	}
}
