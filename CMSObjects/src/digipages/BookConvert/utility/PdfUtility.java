package digipages.BookConvert.utility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;

import digipages.BookConvert.PDF2ePub.PDF2ePubHandler;
import digipages.common.DPLogger;

public class PdfUtility {
	private  static DPLogger logger =new DPLogger(PdfUtility.class.getName());
	
	public static void exportText(File pdfFile, String targetTextPath) throws InvalidPasswordException, IOException
	{
		String retTxt = extractText(pdfFile);
		
		FileWriter fw = new FileWriter(new File(targetTextPath));
		fw.append(retTxt);
		fw.close();
	}
	
	private static String handleText(String retTxt, String pattern, String replacement) {
//		if (retTxt.contains(pattern)){
//			logger.debug("find pattern:" + replacement );
//		}
		return retTxt.replaceAll(pattern, replacement);

	}
	
	public static String safeConvertPdfText(String source)
	{
		String retTxt = source;
		retTxt=handleText(retTxt,"\r\n\r\n", "XDXDchangeLine");
		retTxt=handleText(retTxt,"\r\n", "");
		// Cancel often happened in end of page. ascii 018x
		retTxt=handleText(retTxt,"", "<PageEnd>\r\n");
		// handle special case \ u0000 = special char
		retTxt=handleText(retTxt,"\\u000d", "");
		// finally remove any escape characters.
		retTxt=handleText(retTxt,"[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
		retTxt=handleText(retTxt,"XDXDchangeLine", "\r\n");
		return retTxt;
	}
	

	/**
	 * can only do single page.
	 * @param srcPdf
	 * @return
	 * @throws InvalidPasswordException
	 * @throws IOException
	 */
	public static String extractText(File srcPdf) throws InvalidPasswordException, IOException {
		PDDocument pdf=PDDocument.load(srcPdf);
		PDFTextStripper stripper = new PDFTextStripper();
		String content = stripper.getText(pdf);
		content=safeConvertPdfText(content);
		content = CommonUtil.htmlCodeToUniCode(content);
		pdf.close();
		return content;
	}

	public static boolean isPDFV13(File pdfFile) {
		String version = getPDFVersion(pdfFile);
		
		// lack version info = need warn.
		if (version.isEmpty())
			return true;
		
		return version.toLowerCase().contains("1.3");
	}

	public static String getPDFVersion(File pdfFile) {
		String version = "";
		try {
			CmdExecutor executor = new CmdExecutor();
			executor.run("pdfinfo " + pdfFile.getAbsolutePath());
			String totalMsg = executor.getStdOut();
			logger.debug("totalMessage: " + totalMsg);
			totalMsg = totalMsg.replaceAll("\r", "\n");
			totalMsg = totalMsg.replaceAll("\n\n", "\n");
			for (String tmpLine:totalMsg.split("\n"))
			{
				logger.debug("Processing pdf info Line:" + tmpLine);
				if (tmpLine.toLowerCase().contains("pdf version"))
				{
					version = tmpLine.split(":")[1].trim();
					break;
				}
			}
		} catch (Exception e1) {
			PDF2ePubHandler.logger.error("Fail getting pdf version", e1);
		}
		logger.debug("PDF Version detected:" + version);
		return version;
	}

	// for test only.
		public static void main(String [] args)
		{
			try {
				
				File f = new File("/Downloads/Uploaded_PDF2ePub/9789864530359.pdf");
	//			File f = new File("/Downloads/Kali_Revealed_1st_edition.pdf");
				exportText(f,"/tmp/9789864530359.pdf.txt");
			}catch (Exception e)
			{
				logger.error("",e);
			}
		}
}
