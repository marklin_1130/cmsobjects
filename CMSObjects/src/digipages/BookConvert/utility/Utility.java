package digipages.BookConvert.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import digipages.common.DPLogger;

public class Utility {
	
	private static DPLogger logger = DPLogger.getLogger(Utility.class.getName());
    
    // Hash function SHA-1
    public static String encrypt(String s) {
        MessageDigest sha = null;
        
        try{
            sha = MessageDigest.getInstance("SHA-1"); 
            sha.update(s.getBytes());
        }catch(Exception e){
            e.printStackTrace();
            return "";
        }
        
        return byte2hex(sha.digest());
    }
    
    public static String byte2hex(byte[] b) {
        String hash="";
        String temp="";
        
        for (int i = 0; i < b.length; i++){
            temp = Integer.toHexString(b[i] & 0XFF);
            if (temp.length() == 1) {
                hash = hash + "0" + temp;
            } else {
                hash = hash + temp;
            }
        }
        
        return hash.toUpperCase();
    }
    
    public static File downloadFromS3(AmazonS3 s3Client, String bucket, String key, String localPath) throws Exception {
        int retry = 5;
        return downloadFromS3(s3Client, bucket, key, localPath, retry);
    }
    
    private static File downloadFromS3(AmazonS3 s3Client, String bucket, String key, String localPath, int retry) throws Exception {
        String Tag = "[downloadFromS3]";
        File downloadedFile = new File(localPath);
        String folderPath = localPath.substring(0, localPath.lastIndexOf("/") + 1);
        File folder = new File(folderPath);
        if (!folder.exists()) {
            folder.mkdir();
        }
        
        try {
            logger.info(Tag + "Download from bucket:" + bucket + ", key:" + key);
            S3Object s3Object = s3Client.getObject(new GetObjectRequest(bucket, key));
            InputStream objectData = s3Object.getObjectContent();
            OutputStream out = new FileOutputStream(downloadedFile);
            
            int size = 0;
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = objectData.read(bytes)) != -1) {
                out.write(bytes, 0, read);
                size = size + read;
            }
            
            logger.info(Tag + "Download finished. File size:" + size + " location:" + downloadedFile.getAbsolutePath());
            out.close();
            objectData.close();
            s3Object.close();
        } catch (Exception e) {
        	
        	// special case key not matched => skip retry. 
        	if (e.getMessage().contains("key does not exist")){
        		throw new Exception(e.getMessage(), e);
        	}
        		
            logger.info("[downloadFromS3] Exception, retry:" + retry);
            e.printStackTrace();
            
            if (retry > 0) {
            	retry = retry -1;
                Random random = new Random();
                Long delay = (long) ((random.nextInt(15) + 5) * 1000); //Random 5~20s
                
                try {
                    Thread.sleep(delay);
                } catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                
                downloadedFile = downloadFromS3(s3Client, bucket, key, localPath, retry);
            } else {
                throw new Exception(e.getMessage(), e);
            }
        }
        
        return downloadedFile;
    }
    
    /**
     * list all child files.
     * @param folder
     * @return all files under this folder
     */
    public static List<File> listAllFile(File folder) {
        ArrayList<File> fileList = new ArrayList<>();
        if (folder==null)
        	return fileList;
        if (folder.isFile()){
        	fileList.add(folder);
        	return fileList;
        }
        for (File file : folder.listFiles()) {
            if (file.isFile()) {
                fileList.add(file);
            } else {
                fileList.addAll(listAllFile(file));
            }
        }
        
        return fileList;
    }
    
    public static Boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] child = dir.list();
            for (int i = 0; i < child.length; i++) {
                Boolean success = deleteDir(new File(dir, child[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
    

    /**
     * 
     * @param s3Client
     * @param bucket
     * @param path
     * @param file
     * @param permission
     */
    public static void uploadToS3(AmazonS3 s3Client,String bucket, String path, File file,CannedAccessControlList permission)
    {
        s3Client.putObject(new PutObjectRequest(bucket, path, file)
                .withCannedAcl(permission));
    }

	// [+] XML function
	    public static File writeXml(Document doc, String filePath) throws Exception {
	        File xml = new File(filePath);
	        if (xml.exists()) {
	            xml.delete();
	        }
	        //for local debug 
//	        TransformerFactory transformerFactory = new com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();        
			Transformer transformer = transformerFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        StreamResult result = new StreamResult(xml);
	        transformer.transform(source, result);
	
	        return xml;
	    }

}
