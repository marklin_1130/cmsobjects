package digipages.BookConvert.utility;



/**
 * common job parameter
 * @author Eric.Cl.Chou
 *
 */
import java.util.Set;
import java.util.ArrayList;
public class DataMessage {
    private String targetBucket;
    private String folder;
    private String bookFileName;
    private String requestData;
    private int convertPage;
    private String size;
    private String checksum;
    private Boolean result;
    private String errorMessage="";
    private String logFolder;
    private Set<String> media;
	private int totalPage;
	private ArrayList<Integer> thumbnails;

	// for thumbnail
	private String bookUniId;
	private int bookFileId;
	private String cmsToken;
	private String downloadToken;
	private String viewerBaseURL; // https://viewer...../viewer/
	private int pageWait; //700ms-1200ms
	
	private Boolean losslessNotify = false;
    
    public String getLogFolder() {
        return logFolder;
    }
    public void setLogFolder(String logFolder) {
        this.logFolder = logFolder;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
    
    public Boolean getResult() {
        return result;
    }
    public void setResult(Boolean result) {
        this.result = result;
    }
    
    public String getChecksum() {
        return checksum;
    }
    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }
    
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }
    
    public int getConvertPage() {
        return convertPage;
    }
    public void setConvertPage(int convertPage) {
        this.convertPage = convertPage;
    }
    
    public String getTargetBucket() {
        return targetBucket;
    }
    public void setTargetBucket(String targetBucket) {
        this.targetBucket = targetBucket;
    }
    
    public String getFolder() {
        return folder;
    }
    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getBookFileName() {
        return bookFileName;
    }
    public void setBookFileName(String bookFileName) {
        this.bookFileName = bookFileName;
    }

    public String getRequestData() {
        return requestData;
    }
    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }
    
	public void setEndPage(int totalPage) {
		this.totalPage=totalPage;
		
	}
	
	/**
	 * limit error message to constant size 
	 * @param string
	 */
	public void appendErrorMessage(String string) {
		if (null==string || string.isEmpty())
			return;
		errorMessage += string;
		checkErrMessageSize();
	}
	
	// limit to front 2000 characters
	private void checkErrMessageSize() {
        // max 2000 characters.
        if (this.errorMessage.length()>2000){
        	this.errorMessage= this.errorMessage.substring(0, 2000);
        }
	}
    public Set<String> getMedia() {
        return media;
    }
    public void setMedia(Set<String> media) {
        this.media = media;
    }
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public String getBookUniId() {
		return bookUniId;
	}
	public void setBookUniId(String bookUniId) {
		this.bookUniId = bookUniId;
	}
	public int getBookFileId() {
		return bookFileId;
	}
	public void setBookFileId(int bookFileId) {
		this.bookFileId = bookFileId;
	}
	public String getCmsToken() {
		return cmsToken;
	}
	public void setCmsToken(String cmsToken) {
		this.cmsToken = cmsToken;
	}
	public String getDownloadToken() {
		return downloadToken;
	}
	public void setDownloadToken(String downloadToken) {
		this.downloadToken = downloadToken;
	}
	public String getViewerBaseURL() {
		return viewerBaseURL;
	}
	public void setViewerBaseURL(String viewerBaseURL) {
		this.viewerBaseURL = viewerBaseURL;
	}
	public int getPageWait() {
		return pageWait;
	}
	public void setPageWait(int pageWait) {
		this.pageWait = pageWait;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ArrayList<Integer> getThumbnails() {
		return thumbnails;
	}
	public void setThumbnails(ArrayList<Integer> thumbnails) {
		this.thumbnails = thumbnails;
	}
	public Boolean getLosslessNotify() {
		return losslessNotify;
	}
	public void setLosslessNotify(Boolean losslessNotify) {
		this.losslessNotify = losslessNotify;
	}
}