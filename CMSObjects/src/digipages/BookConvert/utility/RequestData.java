package digipages.BookConvert.utility;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import model.BaseBookInfo;
import model.BookInfo;
import model.Item;
import model.MagInfo;

public class RequestData {
	private String author;
	private String book_file_id;
	private String c_title;
	private String o_title;
	private String call_type;
	private int count_TimeoutMointor;
	private String efile_cover_url;
	private String efile_nofixed_name;
	private String efile_url;
	private Boolean forceCheck = false;
	private String format;
	private String intro;
	private String isbn;
	private String issn;
	private Boolean isTrial;
	private String item;
	private String language;
	private String preview_content;
	private String preview_type;
	private String publish_date;
	private String publisher_b_id;
	private String publisher_name;
	private String request_time;
	private int return_file_num;
	private String s3Key;
	private String s3Secret;
	private String status;
	private String version;
	private String pageDirection = "default"; // Allowed values are ltr (left-to-right), rtl (right-to-left) and default.
	private String type;
	private String normalBookFileS3Location;
	private String screenText;
	private String screenXhtml;
	private String screenXhtmlPrevious;
	private String totalPage;
	private String trialPage;

	public String getAuthor() {
		return CommonUtil.stripTextFromPartialHtml(author);
	}

	public String getBook_file_id() {
		return book_file_id;
	}

	public String getC_title() {
		return CommonUtil.stripTextFromPartialHtml(c_title);
	}

	public String getCall_type() {
		return call_type;
	}

	public int getCount_TimeoutMointor() {
		return count_TimeoutMointor;
	}

	public String getEfile_cover_url() {
		return efile_cover_url;
	}

	public String getEfile_nofixed_name() {
		return efile_nofixed_name;
	}

	public String getEfile_url() {
		return efile_url;
	}

	public Boolean getForceCheck() {
		return forceCheck;
	}

	public String getFormat() {
		return format;
	}

	public String getIntro() {
		return CommonUtil.htmlCodeToUniCode(intro);
	}

	public String getIsbn() {
		return CommonUtil.htmlCodeToUniCode(isbn);
	}

	public String getIssn() {
		return CommonUtil.htmlCodeToUniCode(issn);
	}

	public Boolean getIsTrial() {
		return isTrial;
	}

	public String getItem() {
		return item;
	}

	public String getLanguage() {
		return language;
	}

	public String getPreview_content() {
		return CommonUtil.htmlCodeToUniCode(preview_content);
	}

	public String getPreview_type() {
		return preview_type;
	}

	public String getPublish_date() {
		return CommonUtil.htmlCodeToUniCode(publish_date);
	}

	public String getPublisher_b_id() {
		return publisher_b_id;
	}

	public String getPublisher_name() {
		return CommonUtil.stripTextFromPartialHtml(publisher_name);
	}

	public String getRequest_time() {
		return request_time;
	}

	public int getReturn_file_num() {
		return return_file_num;
	}

	public String getS3Key() {
		return s3Key;
	}

	public String getS3Secret() {
		return s3Secret;
	}

	public String getStatus() {
		return status;
	}

	public String getVersion() {
		return CommonUtil.htmlCodeToUniCode(version);
	}

	public void setAuthor(String author) {
		this.author = StringEscapeUtils.escapeHtml4(author);
	}

	public void setBook_file_id(String book_file_id) {
		this.book_file_id = book_file_id;
	}

	public void setC_title(String c_title) {
		this.c_title = StringEscapeUtils.escapeHtml4(c_title);
	}

	public void setCall_type(String call_type) {
		this.call_type = call_type;
	}

	public void setCount_TimeoutMointor(int count_TimeoutMointor) {
		this.count_TimeoutMointor = count_TimeoutMointor;
	}

	public void setEfile_cover_url(String efile_cover_url) {
		this.efile_cover_url = efile_cover_url;
	}

	public void setEfile_nofixed_name(String efile_nofixed_name) {
		this.efile_nofixed_name = efile_nofixed_name;
	}

	public void setEfile_url(String efile_url) {
		this.efile_url = efile_url;
	}

	public void setForceCheck(Boolean forceCheck) {
		this.forceCheck = forceCheck;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public void setIsTrial(Boolean isTrial) {
		this.isTrial = isTrial;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setPreview_content(String preview_content) {
		this.preview_content = preview_content;
	}

	public void setPreview_type(String preview_type) {
		this.preview_type = preview_type;
	}

	public void setPublish_date(String publish_date) {
		this.publish_date = publish_date;
	}

	public void setPublisher_b_id(String publisher_b_id) {
		this.publisher_b_id = publisher_b_id;
	}

	public void setPublisher_name(String publisher_name) {
		this.publisher_name = StringEscapeUtils.escapeHtml4(publisher_name);
	}

	public void setRequest_time(String request_time) {
		this.request_time = request_time;
	}

	public void setReturn_file_num(int return_file_num) {
		this.return_file_num = return_file_num;
	}

	public void setS3Key(String s3Key) {
		this.s3Key = s3Key;
	}

	public void setS3Secret(String s3Secret) {
		this.s3Secret = s3Secret;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void updateByItem(Item item) {
		setAuthor(StringUtils.trimToEmpty(item.getAuthor()));
		setPublisher_name(item.getPublisherName());
		setC_title(CommonUtil.htmlCodeToUniCode(item.getcTitle()));

		BaseBookInfo info = (BaseBookInfo) item.getInfo();

		setLanguage(info.getLanguage());
		setPublish_date(info.getPublish_date());
		setPageDirection(info.getPage_direction());
		setO_title(CommonUtil.htmlCodeToUniCode(info.getO_title()));

		// for book => isbn, author,
		switch (item.getItemType()) {
			case "magazine":
				setIssn(((MagInfo) info).getIssn());
				setIntro(CommonUtil.htmlCodeToUniCode(((MagInfo) info).getCover_story()));
				setType("magazine");
				break;

			case "book":
			case "serial":
			default:
				setIsbn(((BookInfo) info).getIsbn());
				setIntro(CommonUtil.htmlCodeToUniCode(((BookInfo) info).getIntro()));
				setType("book");
		}
	}

	public String getPageDirection() {
		return pageDirection;
	}

	public void setPageDirection(Integer pageDirection) {
		switch (pageDirection) {
			case 1:
				this.pageDirection = "rtl";
				break;
			case 0:
			default:
				this.pageDirection = "ltr";
				break;
		}
	}

	public void setO_title(String otitle) {
		this.o_title = StringEscapeUtils.escapeHtml4(otitle);
	}

	public String getO_title() {
		return CommonUtil.stripTextFromPartialHtml(o_title);
	}

    public String getNormalBookFileS3Location() {
        return normalBookFileS3Location;
    }

    public void setNormalBookFileS3Location(String normalBookFileS3Location) {
        this.normalBookFileS3Location = normalBookFileS3Location;
    }

    public String getScreenText() {
        return screenText;
    }

    public void setScreenText(String screenText) {
        this.screenText = screenText;
    }

    public String getScreenXhtml() {
        return screenXhtml;
    }

    public void setScreenXhtml(String screenXhtml) {
        this.screenXhtml = screenXhtml;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getTrialPage() {
        return trialPage;
    }

    public void setTrialPage(String trialPage) {
        this.trialPage = trialPage;
    }

    public String getScreenXhtmlPrevious() {
        return screenXhtmlPrevious;
    }

    public void setScreenXhtmlPrevious(String screenXhtmlPrevious) {
        this.screenXhtmlPrevious = screenXhtmlPrevious;
    }
}