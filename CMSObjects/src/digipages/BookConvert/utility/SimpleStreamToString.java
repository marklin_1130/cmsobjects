package digipages.BookConvert.utility;

import java.io.*;

public class SimpleStreamToString extends Thread {
	InputStream is;
	StringBuilder sb = new StringBuilder();

	public SimpleStreamToString(InputStream is) {
		this.is = is;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is,"UTF-8");
			BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException ioe) {
			// supress error
		}
	}

	public String getMessage() {
		return sb.toString();
	}
	
	@Override
	public void finalize()
	{
		try {
			is.close();
		} catch (IOException e) {
			// do nothing.
		}
	}

	public void close() {
		try {
			is.close();
		} catch (IOException e) {
			// do nothing.
		}
		
	}
}