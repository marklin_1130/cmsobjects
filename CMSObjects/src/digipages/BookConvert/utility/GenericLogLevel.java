package digipages.BookConvert.utility;

public enum GenericLogLevel {
	Debug(1), Info(2), Warn(3), Error(998), Fatal(999);
	private final int value;
	private GenericLogLevel(int value)
	{
		this.value=value;
	}
	public int getValue() {
		return value;
	}
}
