package digipages.BookConvert.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.CharacterIterator;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.crypto.KeyGenerator;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.w3c.dom.NamedNodeMap;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;

import digipages.BookConvert.BookMain.MimeType;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
public class CommonUtil {
    private static final String PATHSEP = "/";

//	static DPLogger logger = DPLogger.getLogger(CommonUtil.class.getName());
	static SimpleDateFormat oldDateTimeFormatter = new  SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static Gson gson=new Gson();
	private static DPLogger logger=DPLogger.getLogger(CommonUtil.class.getName());
	private static List<String> transSafeCharacters=new ArrayList<String>();
    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;
    static {
	    dbFactory = DocumentBuilderFactory.newInstance();
	    dbFactory.setNamespaceAware(true);
	    try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.error("Fail to build dbuilder",e);
		}
    }
    
	static {
		try{
			loadTransSafeCharacters();
		}catch(Exception e)
		{
			logger.error("Fail to load check Characters",e);
		}
	}
	
	// format: 2015/05/12T00:00:00Z+8, 2015-07-06T15:41:20+08:00, 2015-07-06T15:41:20+0800
	public static OffsetDateTime parseDateTime(String dateTimeStr) {

		if (dateTimeStr.indexOf(" ") != -1) {
			dateTimeStr = dateTimeStr.replace(" ", "T");
		}
		return OffsetDateTime.parse(standardizeIso8601(dateTimeStr));
	}
	
	// format: 2015/05/12T00:00:00Z+8, 2015-07-06T15:41:20+08:00, 2015-07-06T15:41:20+0800
	public static java.util.Date parseDateTime2JDate(String dateTimeStr) {
		OffsetDateTime offset= parseDateTime(dateTimeStr);
		
		return new Date(offset.toInstant().toEpochMilli());
	}
	
	// from 9999/12/31 00:00:00 to iso8601 (2016-03-22T10:14:53+08:00)
	// if fail to recognize, return with original date String
	public static String normalizeDateTime(String wrongFormat)
	{
		String ret = wrongFormat;
		try {
			Date d = oldDateTimeFormatter.parse(wrongFormat);
			ret = fromDateToString(d);
		}catch (Exception e)
		{
//			e.printStackTrace();
		}
		return ret;
	}

	
	// from java.util.date to  2015-07-06T15:41:20+08:00
	public static String fromDateToString(java.util.Date date)
	{
		String ret=OffsetDateTime.ofInstant(date.toInstant(), ZoneId.of("+08:00"))
				.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX:00"));
		
		// remote need clean output, cut off .123 under second.
		// 2015-07-06T15:41:20.123+08:00 => 2015-07-06T15:41:20+08:00
		ret=ret.replaceAll("\\.\\d\\d\\d", "");
		
		return ret;
	}
	
	public static Map<String, String> getURIasMap(String baseURI,String requestURI) {
		// cut out baseURI
		int loc = requestURI.indexOf(baseURI);
		int rqLength = requestURI.length();
		int baseURILength = baseURI.length(); 
		requestURI=requestURI.substring(loc + baseURILength, rqLength);
		
		String [] split=requestURI.split("/");
		Map <String,String> parameters = new HashMap<String,String>();
		
		String curKey="";
		for (int i=0; i< split.length; i++)
		{
			if (i%2==0)
			{
				curKey=split[i];
			}else
			{
				parameters.put(curKey, split[i]);
			}
		}
		return parameters;

	}
	
	public static List<String> getURIasList(String baseURI,String requestURI) {
		// cut out baseURI
		int loc = requestURI.indexOf(baseURI);
		int rqLength = requestURI.length();
		int baseURILength = baseURI.length(); 
		requestURI=requestURI.substring(loc + baseURILength, rqLength);

		
		String []split=requestURI.split("/");
		List <String> parameters = Arrays.asList(split);
		return parameters;
	}

	/**
	 * 
	 * from "2015-07-06T15:41:20+0800"  to "2015-07-06T15:41:20+08:00"
	 * convert " " to "T"
	 * @param srcDT
	 * @return
	 */
	static public String standardizeIso8601(String srcDT) {
		srcDT = srcDT.replace(' ', 'T');
		if (srcDT.lastIndexOf(':')> (srcDT.length()-4))
		{
			return srcDT;
		}
		else
		{
			String newX=srcDT.substring(0,srcDT.length()-2) + ":00";
			return newX;
		}
		
	}


	public static String zonedTime() {
		return fromDateToString(new Date());
	}

	public static boolean isEmpty(String stringValue) {
		
		return stringValue==null || stringValue.length()==0;
	}
	
	public static String getServLetURL(HttpServletRequest request) {
		String reqHead=request.getRequestURL().toString();
		reqHead=reqHead.replaceAll(request.getServletPath(), "");
		return reqHead;
	}
	
	/**
	 * input: bucketName/path/to/s3/file
	 * @param s3URI 
	 * @return [bucketName , pathKey]
	 */
	public static List<String> splitS3URI(String s3URI)
	{
		List <String> ret = new ArrayList<>();
		String split[]=s3URI.split("/");
		String bucketName = split[0];
		ret.add(bucketName);
		
		String pathKey =s3URI.replace(bucketName,"");
		while (pathKey.startsWith("/"))
		{
			pathKey=pathKey.substring(1);
		}
		ret.add(pathKey);

		return ret;
		
	}
	
	public static byte[] generateAES256Key() throws NoSuchAlgorithmException{
		KeyGenerator keygen;
		keygen=KeyGenerator.getInstance("AES");
		keygen.init(256);
		byte[] encryptionKey=keygen.generateKey().getEncoded();
		return encryptionKey ;
	}

	
	/**
	 * get tail parameter from uri, skip last empty item.
	 * @param uri
	 * @return
	 */
	public static String getTailSplitName(String uri) {
		String []tmpAry = uri.split("/");
		int lastIdx = tmpAry.length-1;
		
		if ((tmpAry[lastIdx]).length()>0)
			return tmpAry[lastIdx];
		else
			return tmpAry[lastIdx-1];
	}
	
	
	public static String inputStreamToString(InputStream inputStream, String encoding) throws IOException{
		
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream, encoding));
		
		String line ;
		StringBuilder result = new StringBuilder();
		while ((line = bufferedReader.readLine()) != null){
			String tmpTail="";
			if (!(line.endsWith("\r") || line.endsWith("\n")))
				tmpTail="\r\n";
			result.append( line).append(tmpTail);
		}
		return result.toString();
	}
	
	public static String inputStreamToString(InputStream inputStream) throws IOException{
			return inputStreamToString(inputStream,"utf-8");
		}
	
	public static String toBooksDateStr(Date lastUpdated){
		
		String dateTime ="";
		String udt = fromDateToString(lastUpdated) ;
		udt = udt.replace("-", "/")  ;
		udt = udt.replace("T", " ")  ;
		dateTime= udt.split("\\.")[0] ;
		return dateTime;
		
	}
	
	/**
	 * from 
	 * http://viewer.booksdev.digipage.info/viewer/index.html?readlist=magazine
	 * to 
	 * http://viewer.booksdev.digipage.info
	 * or 
	 * https://viewer.booksdev.digipage.info/viewer/index.html?readlist=magazine
	 * to
	 * https://viewer.booksdev.digipage.info
	 * @param referrer
	 * @return
	 */
	public static String getRequestHost(String referrer) {
		int loc =0;
		int cnt =0;
		for (int i=0;i<referrer.length();i++)
		{
			if (referrer.charAt(i)=='/')
			{
				cnt++;
			}
			if (cnt==3){
				loc = i;
				break;
			}
		}
		if (loc==0)
			loc = referrer.length();
		String ret = referrer.substring(0,loc);
		return ret;
	}
	
	//
	public static String getCurLocalDateTimeStr(String timeZone)
	{
		if (timeZone==null || timeZone.length()<1)
			timeZone = "UTC+8";
		ZoneId zoneId = ZoneId.of(timeZone);
		ZonedDateTime zdt = ZonedDateTime.now(zoneId);
		return zdt.toString();
	}
	public static String responseToStr(CloseableHttpResponse response)
			throws UnsupportedOperationException, IOException {
		String responseStr = null;
		try {
			// logger.info(response.getStatusLine());
			HttpEntity entity1 = response.getEntity();
			InputStream inp = entity1.getContent();
			Scanner scanner = new Scanner(inp, "UTF-8");
			scanner.useDelimiter("\\A");
			responseStr = scanner.next();
	
			scanner.close();
			inp.close();
			EntityUtils.consume(entity1);
		} finally {
			response.close();
		}
		return responseStr;
	}
	public static String readPutRequest(HttpServletRequest request) throws UnsupportedEncodingException, IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(request.getInputStream(), "UTF-8"));
		String line = "",result="";
		while ((line = bufferedReader.readLine()) != null) {
			result += line;
		}
		bufferedReader.close();
		result = URLDecoder.decode(result,"UTF-8");

		return result;
	}
	public static String dumpObject(Object anObject) {
		
		
		return ToStringBuilder.reflectionToString(anObject);
	}
	// notice: performance 
	public static boolean hasDuplicate(List<String> deviceNames, String deviceName) {
		for (String tmpName:deviceNames)
		{
			if (deviceName.equals(tmpName))
			{
				return true;
			}
		}
		return false;
	}
	
	public static String genNewName(List<String> deviceNames, String curName) {
		int max=1;
		for (String tmpName:deviceNames)
		{
			if (null==tmpName)
				continue;
			
			if (tmpName.startsWith(curName))
			{
				String []x = tmpName.split("#");
				if (x.length>1)
				{
					int curNum = 0;
					/* some user may use # as his favorite device name ==> cause parseInt crash
					 * make it exception (not handle)
					 * ex: iphone#1 ==> iphone#5s (my 5s) or iphone#rita's iphone
					*/
					try {
							curNum=Integer.parseInt(x[1]);
					} catch (Exception ex)
					{
						continue;
					}
					if (curNum > max)
						max=curNum;
				}
			}
		}
		return curName + "#" + (max+1);
	}
	
	@SuppressWarnings("resource")
	public static String execCmd(String cmd) throws java.io.IOException {
        java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(cmd)
        		.getInputStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
	



	public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
	
	public static String httpClient(String url) throws UnsupportedOperationException, IOException {
		String responseStr = "";
		
		final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
		HttpGet httpGet = new HttpGet(url);
		httpclient.execute(httpGet);
		return responseStr ;
	}
	
	public static String escapeSpecialCharacter(String aText){

		// skip empty String
		if (aText==null || aText.length()<1)
			return aText;
		
	     final StringBuilder result = new StringBuilder();
	     final StringCharacterIterator iterator = new StringCharacterIterator(aText);
	     char character =  iterator.current();
	     while (character != CharacterIterator.DONE ){

	       if (character == '<' ||
				character == '!'  ||
				character == '@'  || 
				character == '#'  || 
				character == '$'  ||
				character == '%'  ||
				character == '^'  ||
				character == '&'  ||
				character == '*'  ||
				character == '('  ||
				character == ')'  ||
				character == '|'  ||
				character == '<'  ||
				character == '>'  ||
				character == '\\' ||
				character == '/'  ||
				character == '['  ||
				character == ']'  ||
				character == '{'  ||
				character == '}'  ||
				character == '"'  ||
				character == '+'  
	    		   ) {
	         result.append(""); 
	       }else{
	    	   result.append(character);  
	       }
	       character = iterator.next();
	     }
	     return result.toString();
	  }
	
	public static Date nextNMinutes(int i) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, i);
		return c.getTime();

	}
	
	/**
	 * from httpURLConnection (already put/post), get response as String.
	 * @param connection
	 * @return
	 * @throws IOException
	 */
	public static String getResponseFromConnection(HttpURLConnection connection) throws IOException {
	       BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        StringBuilder response = new StringBuilder();
	
	        int bufferSize=1024;
	        char[] buffer = new char[bufferSize]; // or some other size, 
	        int charsRead ;
	        while ( (charsRead  = in.read(buffer, 0, bufferSize)) != -1) {
	            response.append(buffer, 0, charsRead);
	        }
	        in.close();
	        return response.toString();
	}
	
	public static void delDirs(String filePath) {
//		logger.info("deleting:" + filePath);
		File f = new File(filePath);
		if (f.isDirectory())
		{
			for (File childF:f.listFiles())
			{
				delDirs(childF);
			}
		}else{
			delDirs(f);
		}
	}
	
	
    public static void delDirs(File f) {
		if (f.isDirectory())
		{
			for (File childF:f.listFiles())
			{
				delDirs(childF);
			}
		}
		if (!f.delete())
		{
			logger.info("Fail Deleting file:" +f.getAbsolutePath() + "/" + f.getName());
		}
	}

    /**
     * call s3 listObjects
     * @param srcBucket
     * @param srcFolder
     * @param s3Client
     * @return count of objects
     * @throws Exception
     */
    public static int countObjectsFromS3(String srcBucket, 
    		String srcFolder, AmazonS3 s3Client) throws Exception
    {
        String bucket = srcBucket;
        String folder = srcFolder;
        ObjectListing objectList = s3Client.listObjects(bucket,  folder);
        logger.debug( "Download source for bucket:" + bucket + ", folder:" + folder);
        
        List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();                
        while (objectList.isTruncated()) {
            objectList = s3Client.listNextBatchOfObjects(objectList);
            objectSummaries.addAll(objectList.getObjectSummaries());
        }
        
    	return objectSummaries.size();
    }
    /**
     * 
     * @param srcBucket
     * @param srcFolder
     * @param s3Client
     * @param downloadFolderPath
     * @throws Exception
     */
    public static File downloadDirFromS3(String srcBucket, 
    		String srcFolder, AmazonS3 s3Client, String downloadFolderPath) throws Exception
    {
    	
        String bucket = srcBucket;
        String folder = srcFolder;
        ObjectListing objectList = s3Client.listObjects(bucket,  folder);
        logger.debug( "Download source for bucket:" + bucket + ", folder:" + folder);
        
        List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();                
        while (objectList.isTruncated()) {
            objectList = s3Client.listNextBatchOfObjects(objectList);
            objectSummaries.addAll(objectList.getObjectSummaries());
        }
        
        // Download all file
        
        File downloadFolder = new File(downloadFolderPath);
        for (int i = 0; i < objectSummaries.size(); i++) {
            S3ObjectSummary summary = objectSummaries.get(i);
            String key = summary.getKey();
            
            if (key.lastIndexOf("/") != key.length() - 1) {
                String tempFilePath = downloadFolderPath + key.replace(folder + "/", "");
                String tempFolderPath = tempFilePath.substring(0, tempFilePath.lastIndexOf("/") + 1);
                
                File tempfolder = new File(tempFolderPath);
                if (!tempfolder.exists()) {
                    tempfolder.mkdirs();
                }
                
                Utility.downloadFromS3(s3Client, bucket, key, tempFilePath);
            }
        }
        return downloadFolder;
    }
    
	/**
	 * root file = opf file
	 * @param epubFolderPath
	 * @return path of opf file.
	 * @throws ServerException
	 */
    public static String findRootOpfFile(String epubFolderPath) throws ServerException {
    	try {
	        String containerPath = epubFolderPath + "META-INF/container.xml";
	        InputStream objectData = new FileInputStream(containerPath);        
	        org.w3c.dom.Document doc = dBuilder.parse(objectData);
	        org.w3c.dom.Node rootfile = doc.getElementsByTagName("rootfile").item(0);
	        NamedNodeMap map = rootfile.getAttributes();
	        org.w3c.dom.Node full_path = map.getNamedItem("full-path");
	        
	        String rootFileDir = epubFolderPath + full_path.getNodeValue().substring(0, full_path.getNodeValue().lastIndexOf(PATHSEP) + 1);
	        String rootFileName = full_path.getNodeValue().substring(full_path.getNodeValue().lastIndexOf(PATHSEP) + 1);
	        
	        logger.debug("[getRootFile]" + " rootFileDir: " + rootFileDir);
	        logger.debug("[getRootFile]" + "rootFileName: " + rootFileName);
	        
	        objectData.close();
	        return rootFileDir + rootFileName;
    	} catch (Exception e)
    	{
    		e.printStackTrace();
    		throw new ServerException("id_err_315","Cannot find RootFile: META-INF/container.xml");
    	}
    }
    
	/**
	 * download a single pdf file from server
	 * @param s3Credentials
	 * @param dataMessage
	 * @return
	 * @throws ServerException
	 */
	public static File downloadPDFFromS3(DataMessage dataMessage) throws ServerException
	{
		
        RequestData requestData = gson.fromJson(dataMessage.getRequestData(),RequestData.class);
        String picDir = "/tmp/PDF/"+requestData.getBook_file_id() + "/";
        File ret = new File(picDir);
		ret.mkdirs();
		try {
	        // Create S3 Client & SNS Client
//	        AmazonS3Client s3Client = new AmazonS3Client(s3Credentials);
	        AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
	        // Get PDF file
	        String bucket = dataMessage.getTargetBucket();

	        // PDF is for epub format, 
	        String key = dataMessage.getFolder() + "/PDF/" + dataMessage.getBookFileName(); 
	        
	        String pdfPath = "/tmp/PDF/" + requestData.getBook_file_id() + "/Source.pdf";
	        File pdfFile=Utility.downloadFromS3(s3Client, bucket, key, pdfPath);
		} catch (Exception e)
		{
			logger.error("DownloadFrom S3",e);
			
		}
        return ret;
	}
	
	public static String loadFile2String(File textFile) throws IOException {
		return new String(Files.readAllBytes(Paths.get(textFile.getAbsolutePath())));
	}
	
	public static String loadFile2String(String fileName) throws IOException {
		return new String(Files.readAllBytes(Paths.get(fileName)));
	}
	/**
	 * 
	 * @param htmlContent
	 * @return ist of css file paths
	 */
	public static List<String> listCssFileNames(String htmlContent) {
		List <String> ret=new ArrayList<>();
		Document doc=Jsoup.parse(htmlContent);
		Element header = doc.head();
		for (Node child:header.childNodes())
		{
			if (child.hasAttr("type") 
					&& child.attr("type").equalsIgnoreCase("text/css")
					&& child.attr("href").length()>0)
			{
				ret.add(child.attr("href"));
			}
		}
		
		
		return ret;
	}
	
	
	public static String loadCssRecursive(String newCssPath) throws IOException {
		StringBuilder sb = new StringBuilder();
		String ret = "";
		
		try {
			 ret=loadFile2String(newCssPath);
		} catch (IOException ex)
		{
			logger.warn("Warning, fail to load CSS File: " + newCssPath);
			return "";
		}
		
		sb.append(ret);
		String []tmpContent = ret.split("\n");
		for (String tmpLine:tmpContent)
		{
			if (!tmpLine.toLowerCase().contains("@import"))
			{
				continue;
			}
			String childCssFileName = tmpLine.substring(7).replaceAll("\"", "").replaceAll(";", "").trim();
			String head = new File(newCssPath).getParent();
			childCssFileName = head + "/"+childCssFileName;
			sb.append(loadCssRecursive(childCssFileName));
			
		}
		return sb.toString();
	}
	
	
	/**
	 * read html file, load all css in single String
	 * @param htmlFile
	 * @return String contains the css
	 * @throws IOException 
	 */
	public static String loadHtmlCss(File htmlFile) throws IOException {
		StringBuilder allContent = new StringBuilder();
		String htmlContent = CommonUtil.loadFile2String(htmlFile);
		List <String> cssPaths = CommonUtil.listCssFileNames(htmlContent);
		String pathhead = htmlFile.getParent();
		// path convert
		for (String cssPath:cssPaths)
		{
			String newCssPath = pathhead + "/" +cssPath;
			File cssFile = new File(newCssPath);
			if (cssFile.isDirectory()){
				logger.warn("Warning: skipping cssDirectory" + newCssPath + " SourceHtml:" + htmlFile.getName());
				continue;
			}
			String tmpCss = CommonUtil.loadCssRecursive(newCssPath);
			allContent.append(tmpCss).append("\n");
			
		}
		return allContent.toString();
	}
	
	
	/**
	 * download file with no-cache setting. 
	 * @param URLsource
	 * @param downloadFile
	 * @throws IOException
	 */
	public static void downloadURLToFileNoCache(URL source, File downloadFile) throws IOException {
        URLConnection connection=source.openConnection();
		connection.setUseCaches(false);
        connection.connect();
        InputStream input=connection.getInputStream();
        byte[] buffer = new byte[4096];
        int tmpSize;

        OutputStream output = new FileOutputStream( downloadFile );
        while ((tmpSize = input.read(buffer)) != -1) 
        {
            output.write(buffer, 0, tmpSize);
        }
        output.close();
	}
	
	/**
	 * 
	 * @param source
	 * @return fileName (remove ? and other)
	 */
	public static String extractFileNameFromURL(URL source) {
		return source.getFile().contains("/") ? 
                source.getFile().substring(source.getFile().lastIndexOf("/") + 1) :
                source.getFile();
	}
	
	public static boolean isFileXML(File f) {
		try {
			String ext = getFileExt(f);
			switch (ext)
			{
			case "xml":
			case "xhtml":
			case "opf":
				return true;
			default:
				return false;
				
			}
		} catch (Exception e)
		{
			logger.warn("Unexcepted File in checking ext format. " + f.getName(),e);
		}
		return false;
	}
	
	private static String getFileExt(File f) {
		String fileName = f.getName();
		String[] tokens = fileName.split("\\.(?=[^\\.]+$)");
		if (tokens.length>1)
			return tokens[1];
		return "";
	}
	
	
	public static void uploadToS3(AmazonS3 s3Client, File targetFile, String targetS3URI,
			CannedAccessControlList permission, String basePath) throws IOException {
		if (null==targetFile || (null==targetS3URI || targetS3URI.isEmpty()))
		{
			throw new IOException("Faile to upload S3 target, source/target is empty file=" + targetFile + " s3Path=" + targetS3URI );
		}
				
		logger.info("Uploading to S3:" + targetFile.getName()  + " target:" + targetS3URI );
		
		List <String> s3Info=CommonUtil.splitS3URI(targetS3URI);
		
        if (targetFile.isDirectory()) {
            for (File childFile:targetFile.listFiles()) {
            	uploadToS3(s3Client,childFile,
            			targetS3URI + "/" + childFile.getName() ,permission,basePath);
            }
        } else {
        	// base ?
            String fileName = targetFile.getName();
            String extension = "";
            if(fileName.contains(".")) {
                extension = fileName.substring(fileName.lastIndexOf('.'));
            }
            MimeType mimeType = (MimeType.fromExtension(extension) == null) 
                    ? MimeType.fromExtension(".txt") : MimeType.fromExtension(extension);
                    
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentType(mimeType.getType());
            
            PutObjectRequest putObjectRequest = new 
            		PutObjectRequest(s3Info.get(0),s3Info.get(1), targetFile)
                    .withCannedAcl(permission);
            
            PutObjectResult r= s3Client.putObject(putObjectRequest);
        }
	}
	
	public static void WriteFile(File targetHtml, String string) throws IOException {
		FileWriter writer = new FileWriter(targetHtml);
		writer.append(string);
		writer.close();
		
	}
	
	/**
	 * padding left of the String with character c
	 * ex: paddingLeft("10", 4, '0') = "0010"
	 * @param pageNumber
	 * @param targetLength
	 * @param c
	 * @return
	 */
	public static String paddingLeft(String pageNumber, int targetLength, char c) {
		StringBuilder ret = new StringBuilder(pageNumber);
		while (ret.length()<targetLength)
		{
			ret.insert(0, c);
		}
		return ret.toString();
	}
	   
	/**
	 * 
	 * @param string
	 * @param targetDir
	 * @throws IOException 
	 */
	public static void copyFromResource(String resourceURI, File targetDir) throws IOException {
		
		String targetFileName = extractTailPath(resourceURI);
    	String targetFilePath = targetDir + "/" + targetFileName;
		logger.debug("copying file:"  + resourceURI + " to " +targetFilePath) ;
    	
    	InputStream is = CommonUtil.class.getClassLoader().getResourceAsStream(resourceURI);
    	Files.copy(is,Paths.get(targetDir + "/" + targetFileName));
    	is.close();
	}
	
	private static String extractTailPath(String basePath) {
		if (null==basePath || basePath.isEmpty())
			return "";
		String [] splitted = basePath.split("/");
		String last =splitted[splitted.length-1]; 
		if (null==last || last.isEmpty())
		{
			last=splitted[splitted.length-2];
		}
		return last;
	}
	
	
	/*
	 * change &nbsp; to #xxx, look in characters.txt
	 * &amp; to 
	 * 
	 */
	public static String htmlCodeToUniCode(String content) {
		String retTxt=content;
		if (null==retTxt || retTxt.isEmpty())
			return "";
		if (transSafeCharacters.isEmpty())
			return retTxt;
		
		for (String strLine:transSafeCharacters){
			if (strLine==null || strLine.isEmpty())
				continue;
			String[] aArray = strLine.split(" ");
			retTxt = retTxt.replaceAll(aArray[0].trim(), aArray[1].trim());
		}
		
		return retTxt;
	}
	
	public synchronized static void loadTransSafeCharacters() {
		if (transSafeCharacters.isEmpty())
		{
			loadTranslateCharacters();
		}
	}
	
	public static void loadTranslateCharacters()
	{
		String filename = "/characters.txt";
		InputStream	is = CommonUtil.class.getResourceAsStream(filename);
		try {
			
			if (is == null){
				logger.warn("TransSafeCharacters not loaded.");
				return;
			}
				
				InputStreamReader isr = new InputStreamReader(is, "UTF-8");
				BufferedReader br = new BufferedReader(isr);
	
				String strLine;
				while ((strLine = br.readLine()) != null) {
					transSafeCharacters.add(strLine);
				}
				// Close the input stream
			br.close();
			isr.close();
			is.close();
		}catch (IOException e)
		{
			logger.error("Fail Load translate characters.",e);
		}
		
	}

	public static void main(String []args)
	{
		System.out.println(CommonUtil.htmlCodeToUniCode(" 台灣： 定價 NT$230 香港：特價 HK$55 國家地理中文網 國家地理雜誌 中文版 2 0 1 6 年 1 月 N O . 1 7 0 荒野的力量 年度主題：國家公園100年 北極融冰危機 橡膠：引爆東南亞淘金熱 兀鷲 大地清道夫 N ATIO N A L G EO G RA PH IC 國 家 地 理 雜 誌 國 家 公 園 一 百 年 ． 野 地 和 大 腦 ． 兀 鷲 ． 北 極 融 冰 ． 橡 膠 ． 女 兒 國 (N O . 170 ) 2 0 1 6 年 &nbsp; 月 1 books.com "));
	}
	

	public static long calcFileSize(String filePath) {
		
		return calcFileSizeRecu(new File(filePath));
	}
	
	/**
	 * calculate file/directory size (recursively), not include directory structure size.
	 * @param f
	 * @return total size of this file/directory
	 */
	public static long calcFileSizeRecu(File f)
	{
		if (null==f || !f.exists())
			return 0;
		
		if (f.isFile())
			return f.length();
		long total = 0;
		if (f.isDirectory())
		{
			for (File childF:f.listFiles())
			{
				total += calcFileSizeRecu(childF);
			}
		}
		return total;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String textToEPubXML(String text)
	{
		// first to html, and then to Unicode
		String x = textToHtml(text);
		return htmlCodeToUniCode(x);
	}
	
	
	public static String textToHtml(String s) {
	    StringBuilder builder = new StringBuilder();
	    boolean previousWasASpace = false;
	    for( char c : s.toCharArray() ) {
	        if( c == ' ' ) {
	            if( previousWasASpace ) {
	                builder.append("&nbsp;");
	                previousWasASpace = false;
	                continue;
	            }
	            previousWasASpace = true;
	        } else {
	            previousWasASpace = false;
	        }
	        switch(c) {
	            case '<': builder.append("&lt;"); break;
	            case '>': builder.append("&gt;"); break;
	            case '&': builder.append("&amp;"); break;
	            case '"': builder.append("&quot;"); break;
//	            case '\n': builder.append("<br>"); break;
	            // We need Tab support here, because we print StackTraces as HTML
	            case '\t': builder.append("&nbsp; &nbsp; &nbsp;"); break;  
	            default:
                    builder.append(c);
	        }
	    }
	    return builder.toString();
	}
	
	/**
	 * 
	 * @param partialHtml
	 * @return text which can be included in xml
	 */
	public static String stripTextFromPartialHtml(String partialHtml)
	{
		if (partialHtml==null || partialHtml.isEmpty())
			return "";
		Document htmlDoc = Jsoup.parse(partialHtml);
		return textToEPubXML(htmlDoc.text());
	}

}
