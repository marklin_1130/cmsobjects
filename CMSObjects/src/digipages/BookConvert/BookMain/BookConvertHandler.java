package digipages.BookConvert.BookMain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.google.gson.Gson;

import digipages.BookConvert.TimeoutMonitor.TimeoutMonitorHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.RequestData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;
import net.lingala.zip4j.exception.ZipException;

/**
 * main entry of Book Convert
 * @author Eric.CL.Chou
 *
 */
public class BookConvertHandler implements CommonJobHandlerInterface {
    private AmazonS3 s3Client;
    private static DPLogger logger = DPLogger.getLogger(BookConvertHandler.class.getName());
    static Gson gson = new Gson();

	@Override
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
        String logTag = "[BookConvertHandler]";

        ConvertingBook book = null;
        Long startTime = System.currentTimeMillis();
        
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String errorMessage="";
        try {
            // init TimeoutMonitor 
            initEnv(config);
            // Get RequestData from message
            DataMessage dataMessage = job.getJobParam(DataMessage.class);
            RequestData requestData = gson.fromJson(dataMessage.getRequestData(), RequestData.class);
            initTimeoutMonitor(requestData, job);
            
            String format = ConvertingBook.determineBookFormat(requestData.getFormat());
            CommonUtil.delDirs("/tmp/");

            if("Epub".equals(format)) {
                book = new EpubBook(s3Client, requestData,config, dataMessage);
                book.convert();
            } else if("PDF".equals(format)) {
                book = new PdfBook(s3Client, requestData,config,dataMessage);
                book.convert();
            } else {
                throw new ServerException("id_err_315","Object is not a Epub or PDF file");
            }

        }catch (JobRetryException e){
        	// just send retry. don't do things.
        	throw e;
        }catch (Exception e) {
            
            if(e instanceof ServerException) {
                String errMsg = ((ServerException)e).getError_message();
                logger.error(logTag + "Exception in BookConvertHandler: "+errMsg);
                sendNotify(book,errMsg);                
            }else {
                logger.error(logTag + "Exception in BookConvertHandler: ",e);
                errorMessage = dumpErrMessage(e, book,job);
                sendNotify(book,errorMessage);
            }
        } finally {
            if(book != null && book.ConvertingLogger != null) {
                book.ConvertingLogger.uploadLog();
            }
        }
    
        return "Converter Done";
    }


	private void sendNotify(ConvertingBook book, String errorMessage) {
        if(book != null) {
            try {
            	book.notifyAPI(false,errorMessage);
            } catch (Exception e1) {
            	logger.error("Fail sending Notify",e1);
            }
        }
	}


	private String dumpErrMessage(Exception e, ConvertingBook book, ScheduleJob job) throws JobRetryException {
		
		String errorMessage=org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
        if(e.getClass().getName() != null && 
                e.getClass().getName().contains("AmazonS3Exception") && book!=null) {
            errorMessage = "file not found or size = 0, bucket:" + book.S3Bucket 
                    + ", key:" + book.S3Key + "\r\n" + errorMessage;
            
        } else if (e.getMessage() != null &&
                e.getMessage().contains("No Space Left on Device")) {
            errorMessage = "No Space Left on Device. Please check book file size. After unzip, it may exceed size limitation." +
                    "\r\n" + errorMessage;
            if (job.getJobRunner()==RunMode.LambdaMode.getValue())
            {
            	throw new JobRetryException("Lambda Limit reached, try Ec2 instead.");
            }
        } else	if (e.getMessage() != null && e.getMessage().contains("Wrong type of referenced length object COSObject"))
    	{
            errorMessage = "Non-parseable PDF format, please re-save by Adobe Reader to fix this problem." +
                    "\r\n" + errorMessage;
    	} else if (e instanceof ZipException)
    	{
    		errorMessage = "Unzip fail: please check epub format.";
    	}
        else if (e.getStackTrace() != null) {
            Boolean containPDFBox = false;
            for(int i = 0; i < e.getStackTrace().length; i++) {
                if(e.getStackTrace()[i].toString().contains("org.apache.pdfbox")) {
                    containPDFBox = true;
                }
            }
            
            if(containPDFBox) {
                errorMessage = "PDFBox Exception. Please check PDF file is complete." + "\r\n" + errorMessage;
            }
        }else{
            errorMessage = "Book convert failed. Please check book file is complete." + "\r\n" + errorMessage;
        }
		return errorMessage;
	}


	private void initTimeoutMonitor(RequestData reqData, ScheduleJob origJob) {
		ScheduleJob job = new ScheduleJob();
		job.setJobName(TimeoutMonitorHandler.class.getName());
		job.setJobParam(reqData);
		job.setShouldFinishTime(CommonUtil.nextNMinutes(65));
		if (origJob.getJobRunner()==RunMode.LambdaMode.getValue())
		{
			// lambda is 2-4 step, which mean timeout is 25 min max
			job.setShouldStartTime(CommonUtil.nextNMinutes(25));
		}else{
			job.setShouldStartTime(CommonUtil.nextNMinutes(60));
		}
		job.setJobRunner(RunMode.LambdaMode.getValue());
		job.setJobGroup(reqData.getBook_file_id());
		try {
			ScheduleJobManager.addJob(job);
		} catch (Exception e) {
			logger.error("Fail Starting TimeoutMonitor",e);
		}
	}


	private void initEnv(Properties config) {
        // Get config
        this.s3Client = AmazonS3ClientBuilder.defaultClient();
	}


	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// No DataBase access needed
		
	}
	
	/**
	 * retry = Ec2 
	 * @param runMode
	 * @return
	 */
	public static RunMode getRetryRunMode(RunMode runMode) {
		return RunMode.ExtEc2Mode;
	}

	/**
	 * pdf = EC2 by default.
	 * @param job
	 * @return
	 */
	public static RunMode getInitRunMode(ScheduleJob job) {
		Gson gson = new Gson();
		DataMessage message= job.getJobParam(DataMessage.class);
		String reqData=message.getRequestData();
		RequestData req = gson.fromJson(reqData, RequestData.class);
		String format=req.getFormat();
		logger.debug("Format: " + format);
		if ("pdf".equalsIgnoreCase(format)){
			return RunMode.ExtEc2Mode;
		}

		
		return RunMode.LambdaMode;
	}

}