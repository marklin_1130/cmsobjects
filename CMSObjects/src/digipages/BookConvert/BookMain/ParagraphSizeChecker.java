package digipages.BookConvert.BookMain;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import digipages.BookConvert.utility.CommonUtil;
import digipages.common.DPLogger;

/**
 * check Paragraph and summary the result.
 * @author Eric.CL.Chou
 *
 */
public class ParagraphSizeChecker {
	private int tooBigParagraphCnt=0;
	private int sizelimit=0;
	private int maxEventCount=20;
	private static DPLogger logger = DPLogger.getLogger(ParagraphSizeChecker.class.getName());
	
	public ParagraphSizeChecker(int sizeLimit, int maxEventCount)
	{
		this.sizelimit=sizeLimit;
		this.maxEventCount=maxEventCount;
	}
	public String updateFile(File f, String pageProgressionDirection) throws IOException
	{
		String ret =checkBigParagraph(f, sizelimit, pageProgressionDirection); 
		return ret;
	}
	
	
	/**
	 * paragraph (div or p) should be under 625 characters (epubCheckMaxParagraph)
	 * @param checkFile
	 * @param pageProgressionDirection 
	 * @return
	 * @throws IOException 
	 */
	public String checkBigParagraph(File checkFile, int epubCheckMaxParagraph, String pageProgressionDirection) throws IOException {
		StringBuilder errMessage= new StringBuilder();
		
		// skip non html/xhtml file
		if (!checkFile.getName().endsWith("html") || checkFile.isDirectory())
			return "";
		if (!"rtl".equals(pageProgressionDirection))
			return "";
		
		// skip landscape mode html
		if (!isVerticalHtml(checkFile))
		{
			return "";
		}
		
		try {
			org.jsoup.nodes.Document doc = Jsoup.parse(checkFile,"UTF-8","");
			
			// for each div or p
			Elements paragraphs=doc.select("p");
			Elements divs = doc.select("div");
			
			paragraphs.addAll(divs);
			
			Iterator<org.jsoup.nodes.Element> it = paragraphs.iterator();
			while (it.hasNext())
			{
				int calcParagraphSize = 0;
				org.jsoup.nodes.Element e = it.next();

				// skip non-chinese chapters
				if (!isChineseChapter(e))
					continue;

				if (e.tagName().contains("p"))
				{
					calcParagraphSize = e.text().length();
				}else if (e.tagName().contains("div"))
				{
					// recursive div, p ?! skip it.
					if (!e.children().select("div").isEmpty() 
							|| !e.children().select("p").isEmpty())
					{
						continue;
					}else
					{
						calcParagraphSize = e.text().length();
					}
				}
				if (epubCheckMaxParagraph<calcParagraphSize 
						&& tooBigParagraphCnt < maxEventCount)
				{
					logger.debug("BigParagraphCnt:" + tooBigParagraphCnt + " " +checkFile.getName());
					errMessage.append("Paragraph is too big. size=")
					.append(calcParagraphSize)
					.append(" file: ")
					.append(checkFile.getName())
					.append("\r\nContent:\r\n")
					.append(e.text().substring(0,15))
					.append("\r\n");
				}
				if (epubCheckMaxParagraph<calcParagraphSize)
				{
					tooBigParagraphCnt++;
				}
			}
		} catch (IOException ex)
		{
			logger.error("IOException ",ex);
		}
		
		return errMessage.toString();
	}
	
	/**
	 * read html file
	 * check corrosponding style (load css file if necessary)
	 * check if there are any
	 * -webkit-writing-mode: vertical-rl
	 * writing-mode: vertical-rl
	 * @param htmlFile
	 * @return
	 * @throws IOException 
	 */
	public static boolean isVerticalHtml(File htmlFile) throws IOException {
		String htmlContent = CommonUtil.loadFile2String(htmlFile);
		if (cssisVerical(htmlContent))
			return true;
		List <String> cssPaths = CommonUtil.listCssFileNames(htmlContent);
		String pathhead = htmlFile.getParent();
		// path convert
		for (String cssPath:cssPaths)
		{
			String newCssPath = pathhead + "/" +cssPath;
			File cssFile = new File(newCssPath);
			if (cssFile.isDirectory()){
				logger.warn("Warning: skipping cssDirectory" + newCssPath + " SourceHtml:" + htmlFile.getName());
				continue;
			}
			String tmpCss = CommonUtil.loadCssRecursive(newCssPath);
			if (cssisVerical(tmpCss))
				return true;
		}
		return false;
	}
	

	
	private static boolean cssisVerical(String tmpCss) {
		// warning: below code cannot treat other format well. 
		return tmpCss.toLowerCase().contains("writing-mode") && 
		tmpCss.toLowerCase().contains("vertical-rl");
	}
	private boolean isChineseChapter(Element e) {
		String text = e.text();
		for (int i=0; i< text.length();i++){
			if (XHTMLChecker.isChinese(text.charAt(i)))
			{
				return true;
			}
		}
		return false;
	}
	public int getSize() {
		
		return tooBigParagraphCnt;
	}
}
