package digipages.BookConvert.BookMain;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.service.DriverCommandExecutor;
import org.openqa.selenium.remote.service.DriverService;

import digipages.BookConvert.utility.LogToS3;

public class OpenBookHelper {

    private String screenText = "";
    private String screenXhtml = "";
    private String screenXhtmlPrevious = "";
    private String totalPage = "";
    private String trialPage = "";
    private String layout = "";
    private String spread = "";
//    private String url="";

    private WebDriver webDriver;

    // private String getLibLocation(String lib) {
    // return String.format("%s/lib/%s", System.getenv("LAMBDA_TASK_ROOT"), lib);
    // }

    private File getLinuxFileFromURL() {
        URL url = this.getClass().getClassLoader().getResource("chromedriver");
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        return file;
    }

    private File getWinFileFromURL() {
        URL url = this.getClass().getClassLoader().getResource("chromedriver.exe");
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        return file;
    }

    private ChromeOptions getWinChromeOptions() {

        // File folder = new ClassPathResource("sql").getFile();

        String driverPath = "D:/chromedriver.exe";
        // String driverPath = "/opt/chromedriver_linux64/chromedriver";

        System.setProperty("webdriver.chrome.driver", driverPath);
        System.setProperty("webdriver.chrome.verboseLogging", "true");
        ChromeOptions options = new ChromeOptions();
        // options.setBinary(getLibLocation("chrome"));
        // options.setBinary(driverPath);
        // options.addArguments("--disable-gpu");
        // options.addArguments("--headless");
        options.addArguments("--window-size=320,620");
        // options.addArguments("--single-process");
        // options.addArguments("--no-sandbox");
        options.addArguments("--no-zygote");
        // options.addArguments("--disable-dev-shm-usage");
        // options.addArguments("--user-data-dir=/tmp/user-data");
        // options.addArguments("--data-path=/tmp/data-path");
        // options.addArguments("--homedir=/tmp");
        options.addArguments("--allow-file-access-from-files");
        // options.addArguments("--disk-cache-dir=/tmp/cache-dir");
        return options;
    }

    private ChromeOptions getLinuxChromeOptions() {

        // File folder = new ClassPathResource("sql").getFile();

        // String driverPath = "D:/chromedriver.exe";
        String driverPath = "/opt/chromedriver_linux64/chromedriver";

        System.setProperty("webdriver.chrome.driver", driverPath);
        System.setProperty("webdriver.chrome.verboseLogging", "false");
        ChromeOptions options = new ChromeOptions();
        // options.setBinary(getLibLocation("chrome"));
        // options.setBinary(driverPath);
        options.addArguments("--disable-gpu");
        options.addArguments("--headless");
//        options.addArguments("--disable-infobars");
        options.addArguments("--window-size=320,475");
        // options.addArguments("--single-process");
        options.addArguments("--no-sandbox");
//        options.addArguments("--app="+url);
//        System.out.println("--app="+url);
//         options.addArguments("--no-zygote");
        // options.addArguments("--disable-dev-shm-usage");
        // options.addArguments("--user-data-dir=/tmp/user-data");
        // options.addArguments("--data-path=/tmp/data-path");
        // options.addArguments("--homedir=/tmp");
        // options.addArguments("--allow-file-access-from-files");
        // options.addArguments("--disk-cache-dir=/tmp/cache-dir");
        return options;
    }

    public WebDriver startDeriver() {

        webDriver = new ChromeDriver(getLinuxChromeOptions());
        // webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        // webDriver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        return webDriver;
    }

    public void WaitForAjax(WebDriver driver) throws InterruptedException {
    	int retryCount = 0;
        while (true) {
        	retryCount++;
            if ((Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0")) {
                break;
            }
            if(retryCount>5) {
            	break;
            }
            Thread.sleep(1000);
        }
    }

    public void checkPageIsReady(WebDriver driver) {

        JavascriptExecutor js = (JavascriptExecutor) driver;

        // Initially bellow given if condition will check ready state of page.
        if (js.executeScript("return document.readyState").toString().equals("complete")) {
            System.out.println("Page Is loaded.");
            return;
        }

        // This loop will rotate for 25 times to check If page Is ready after
        // every 1 second.
        // You can replace your value with 25 If you wants to Increase or
        // decrease wait time.
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(5000);
                System.out.println("wait for page ready " + i);
            } catch (InterruptedException e) {
            }
            // To check page ready state.
            if (js.executeScript("return document.readyState").toString().equals("complete")) {
                break;
            }
        }
    }

//    public static void main(String[] args) {
//
//        try {
//
//            int pg = (int) (Integer.valueOf(490) * (Integer.parseInt("24") * 0.01));
//            System.out.println(pg);
//            // String s3FileLocation =
//            // "s3public-auth.books.com.tw/trial-converted/1774AD/279610";
//            // String url = "/s3public-auth.books.com.tw/trial-converted/000501/256334";
//            //
//            // String bucketName = args[0];
//            // String objectKey = args[1];
//            //
//            // AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
//            // System.out.println("s3Client!=null:"+s3Client!=null);
//            // System.out.println("bucketName:"+bucketName);
//            // System.out.println("s3ObjectKey:"+objectKey);
//            // ObjectMetadata objectmetadata = s3Client.getObjectMetadata(bucketName,
//            // objectKey);
//            // long length = objectmetadata.getContentLength();
//            // System.out.println(length);
//            // OpenBookHelper ob = new OpenBookHelper();
//            // ob.openBook(s3FileLocation);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void openBook(String s3FileLocation, String percentage, LogToS3 opebookLogger) {

        boolean isSuccessOpen = false;
        int retryCount = 0;
        int totalpgRetryCount = 0;
        String TAG = "openBook";
//        url = "http://localhost:8080/CMSOpenBook/openbook/" + s3FileLocation;
        opebookLogger.log(TAG, "s3FileLocation: " + s3FileLocation + "\r\n" + "percentage: " + percentage);
        while (!isSuccessOpen && retryCount < 3) {
            try {
                retryCount++;
                opebookLogger.log(TAG, "startDeriver");
                startDeriver();
                Date startTime = new Date();
                webDriver.get("http://localhost:8080/CMSOpenBook/openbook/" + s3FileLocation);
                opebookLogger.log(TAG, "startDeriver URL :" + "http://localhost:8080/CMSOpenBook/openbook/" + s3FileLocation);
                try {
                    WaitForAjax(webDriver);
                    checkPageIsReady(webDriver);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                WebElement totalpg = null;

                while ((null == totalpg || "".equals(totalpg.getText()) || "0".equals(totalpg.getText())) && totalpgRetryCount < 15) {
                    try {
                        Thread.sleep(30000);
                        totalpg = webDriver.findElement(By.id("totalpg"));
                        System.out.println("wait for totalpg");
                        opebookLogger.log(TAG, "wait for totalpage");
                        totalpgRetryCount++;
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                }

                if(totalpg == null) {
                    throw new Exception("開書失敗 totalpg = null");
                }
                
                WebElement deviceWele = webDriver.findElement(By.id("deviceW"));
                WebElement deviceHele = webDriver.findElement(By.id("deviceH"));

                this.totalPage = totalpg.getText();
                String deviceW = deviceWele.getText();
                String deviceH = deviceHele.getText();
                opebookLogger.log(TAG, "totalPage:" + totalPage);
                opebookLogger.log(TAG, "deviceW:" + deviceW);
                opebookLogger.log(TAG, "deviceH:" + deviceH);
                WebElement currentPg = webDriver.findElement(By.id("currentpg"));

                // Y<1 ,Y=1,  Y+1=2 最少一頁+封面=2
                int pg = (int) (Integer.valueOf(totalPage) * (Integer.parseInt(percentage) * 0.01) * 0.9);
                if (pg < 1) {
                    pg = 2;
                } else {
                    // Y+1 +封面
                    pg++;
                }

                opebookLogger.log(TAG, totalPage + "* (" + percentage + " * 0.01) * 0.9 ");
                opebookLogger.log(TAG, "trialpage:" + pg);

                System.out.println("gotoPage:" + pg);
//                currentPg.sendKeys(String.valueOf(pg));
//                currentPg.sendKeys(Keys.ENTER);
                
                JavascriptExecutor js = (JavascriptExecutor) webDriver;
                
                for (int i = 0; i < pg; i++) {
                    
                    js.executeScript("_book.gotoNext()");
                    opebookLogger.log(TAG, "翻頁:" + i);
                    try {
                        try {
                            Thread.sleep(1000);
                            WaitForAjax(webDriver);
                            checkPageIsReady(webDriver);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
                
                String layout =(String) js.executeScript("return _EPUB.globalLayoutProperties.layout");
                String spread =(String) js.executeScript("return _EPUB.globalLayoutProperties.spread");
                opebookLogger.log(TAG, "layout:" + layout);
                opebookLogger.log(TAG, "spread:" + spread);
                Long spinPos = (Long) js.executeScript("return Book.showSpinePos");
                if(spinPos>=1) {
                    this.screenXhtmlPrevious = (String) js.executeScript("return Book.spine[Book.showSpinePos-1].id");
                }
                this.screenXhtml = (String) js.executeScript("return Book.spine[Book.showSpinePos].id");
                
                System.out.println(screenXhtml);
                opebookLogger.log(TAG, "screenXhtml:" + screenXhtml);
                opebookLogger.log(TAG, "getVisibleCfi");
                
                this.screenText = getTextContent(js,opebookLogger);
                
                opebookLogger.log(TAG, "screenText:" + screenText);
                // 若記下來的字不到18個，和上一頁又是同一個html，則連上一頁的字一起計算到18個字
                
                while(this.screenText.length() < 18) {
                    js.executeScript("_book.gotoPrevious()");
                    Thread.sleep(2000);
                    String screenXhtmlCurrent = (String) js.executeScript("return Book.spine[Book.showSpinePos].id");
                    if (screenXhtmlCurrent.equalsIgnoreCase(this.screenXhtml)) {
                        String screenTextPrevious = getTextContent(js,opebookLogger);
                        if(!"".equalsIgnoreCase(screenTextPrevious)) {
                            this.screenText = screenTextPrevious + this.screenText;
                        }
                    }else {
                        break;
                    }
                }

                if (this.screenText.length() > 18) {
//                    this.screenText = this.screenText.substring(this.screenText.length() - 18, this.screenText.length());
                    this.screenText = this.screenText.substring(0,18);
                }
                
                // System.out.println(webDriver.findElement(By.id("bodybox")).getText());

                this.trialPage = String.valueOf(pg);
                opebookLogger.log(TAG, "cut screenText:" + screenText);
                opebookLogger.log(TAG, "screenXhtmlPrevious:" + screenXhtmlPrevious);
                System.out.println("deviceW:" + deviceW);
                System.out.println("deviceH:" + deviceH);
                Date endTime = new Date();
                System.out.println("TotalPage:" + totalPage);
                System.out.println("Page:" + pg);
                System.out.println("screenText:" + screenText);
                System.out.println("screenXhtml:" + screenXhtml);
                System.out.println("screenXhtmlPrevious:" + screenXhtmlPrevious);
                System.out.println("loadTime:" + ((endTime.getTime() - startTime.getTime()) / 1000f));
                opebookLogger.log(TAG, "loadTime:" + ((endTime.getTime() - startTime.getTime()) / 1000f));
                if (!"".equalsIgnoreCase(totalPage) && !"".equalsIgnoreCase(trialPage)) {
                    isSuccessOpen = true;
                }

                try {
                    webDriver.close();
                    webDriver.quit();
                } catch (Exception e) {
                }
                try {
                    executeCommand("pkill -f google");
                } catch (Exception e) {
                }
                try {
                    executeCommand("rm -rf /tmp/*");
                } catch (Exception e) {
                }
            } catch (Exception e) {
                opebookLogger.log(TAG, "error:" + ExceptionUtils.getStackTrace(e));
            }
        }

    }
    
    private String getTextContent(JavascriptExecutor js,LogToS3 opebookLogger) {
    	String content="";
        try {
        	content = (String) js.executeScript("return _book.getVisibleCfi()[3].toString()");
        	content = replaceBlank(content);
        } catch (Exception e) {
        	opebookLogger.log("openBook", "抓字失敗 getVisibleCfi:" + e.getMessage());
        	e.printStackTrace();
        	
        }
        return content;
    }

    public void exitWebDriver() {

        try {
            if (OSValidator.isUnix()) {
                int portNum = getWebDriverPort(webDriver);
                System.out.println("get Port:" + portNum);
                String geckodriverPid = executeCommand("lsof -t -i:" + portNum + " -sTCP:LISTEN").replaceAll("[^\\d.]", "");
                System.out.println("ps axo pid,ppid | grep " + geckodriverPid + " |awk '{print $1}' | xargs kill -9");
                executeCommand("ps axo pid,ppid | grep " + geckodriverPid + " |awk '{print $1}' | xargs kill -9");
            } else {
                executeCommand("taskkill /f /t /im firefox.exe");
            }
            Thread.sleep(5000);
        } catch (Exception e) {
            // e.printStackTrace();
        }

        try {
            if (OSValidator.isUnix()) {
                executeCommand("rm -rf /tmp/*");
            }
            Thread.sleep(5000);
        } catch (Exception e) {
            // e.printStackTrace();
        }

        System.out.println("exit webDriver");
    }

    public Integer getWebDriverPort(WebDriver webDriver) {
        try {

            FirefoxDriver aFireFoxDriver = (FirefoxDriver) webDriver;
            DriverCommandExecutor aDriverCommandExecutor = (DriverCommandExecutor) aFireFoxDriver.getCommandExecutor();
            Field serviceField = DriverCommandExecutor.class.getDeclaredField("service");
            serviceField.setAccessible(true);
            final DriverService driverService = (DriverService) serviceField.get(aDriverCommandExecutor);
            System.out.println(driverService.getUrl().getPort());

            return driverService.getUrl().getPort();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private String executeCommand(String command) {

        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }
    private String replaceBlank(String str) {
        String dest = "";
        if (str!=null) {
            Pattern p = Pattern.compile("\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    public String getScreenText() {
        return screenText;
    }

    public void setScreenText(String screenText) {
        this.screenText = screenText;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getTrialPage() {
        return trialPage;
    }

    public void setTrialPage(String trialPage) {
        this.trialPage = trialPage;
    }

    public String getScreenXhtml() {
        return screenXhtml;
    }

    public void setScreenXhtml(String screenXhtml) {
        this.screenXhtml = screenXhtml;
    }

    public String getScreenXhtmlPrevious() {
        return screenXhtmlPrevious;
    }

    public void setScreenXhtmlPrevious(String screenXhtmlPrevious) {
        this.screenXhtmlPrevious = screenXhtmlPrevious;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getSpread() {
        return spread;
    }

    public void setSpread(String spread) {
        this.spread = spread;
    }
}

class OSValidator {

    private static String OS = System.getProperty("os.name").toLowerCase();

    public static void main(String[] args) {

        System.out.println(OS);

        if (isWindows()) {
            System.out.println("This is Windows");
        } else if (isMac()) {
            System.out.println("This is Mac");
        } else if (isUnix()) {
            System.out.println("This is Unix or Linux");
        } else if (isSolaris()) {
            System.out.println("This is Solaris");
        } else {
            System.out.println("Your OS is not support!!");
        }
    }

    public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }

    public static boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }

    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    }

    public static boolean isSolaris() {
        return (OS.indexOf("sunos") >= 0);
    }

    public static String getOS() {
        if (isWindows()) {
            return "win";
        } else if (isMac()) {
            return "osx";
        } else if (isUnix()) {
            return "uni";
        } else if (isSolaris()) {
            return "sol";
        } else {
            return "err";
        }
    }
    
}
