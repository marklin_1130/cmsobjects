package digipages.BookConvert.BookMain;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.GenericLogRecord;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.ResponseData.ResultData;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

/**
 * 電子書(#11)EPUB reflow依正確%產出試閱書-開書部分
 * 
 * @author henry cheng
 *
 */
public class TrialOpenBookHandler implements CommonJobHandlerInterface {
    String originalFolder;
    String trialFolder;
    String originalBucket;
    String URL_BookProcessResult;
    String S3Bucket;
    String S3Key;
    CannedAccessControlList s3Permission;
    private ResultData resultData;
    private Long totalSize;
    private static String TAG = "[TrialOpenBookHandler]";
    private final String error_code_converting_fail = "id_err_305";
    private DataMessage dataMessage;
    private RequestData requestData;
    private AmazonS3 s3Client;
    private static Gson gson = new GsonBuilder().create();
    private LogToS3 opebookLogger;
    private Long jobStartTime;
    private Long StartTime;
    private String logFolder;
    private String folderPath;

    private static final DPLogger logger = DPLogger.getLogger(TrialOpenBookHandler.class.getName());
    
    @Override
    public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
        jobStartTime = System.currentTimeMillis();
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        try {
            buildParameters(config);
            // CommonUtil.delDirs("/tmp");
            StartTime = System.currentTimeMillis();

            //取得job參數
            logger.info(TAG + "message: " + job.toGsonStr());
            dataMessage = job.getJobParam(DataMessage.class);
            requestData = gson.fromJson(dataMessage.getRequestData(), RequestData.class);
            String message = gson.toJson(dataMessage);

            //設定S3-log路徑
            String convertedLogPath = logFolder + "/" + trialFolder + requestData.getItem() + "/" + requestData.getBook_file_id() + "/openbook.log";
            opebookLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
            opebookLogger.log(TAG, "DataMessage: " + message + "\r\n" + "RequestData: " + dataMessage.getRequestData());
            // 計算試閱比例

            String s3FileLocation = requestData.getNormalBookFileS3Location();

            //執行開書
            if(StringUtils.isNoneBlank(requestData.getPreview_content())) {
                String percentage = requestData.getPreview_content().replace("%", "");
                String fullS3BookPath = originalBucket + "/" + originalFolder + "/" + s3FileLocation;
                OpenBookHelper openBookHelper = new OpenBookHelper();
                openBookHelper.openBook(fullS3BookPath,percentage,opebookLogger);
                requestData.setScreenText(openBookHelper.getScreenText());
                requestData.setScreenXhtml(openBookHelper.getScreenXhtml());
                requestData.setTotalPage(openBookHelper.getTotalPage());
                requestData.setTrialPage(openBookHelper.getTrialPage());
                requestData.setScreenXhtml(openBookHelper.getScreenXhtml());
                requestData.setScreenXhtmlPrevious(openBookHelper.getScreenXhtmlPrevious());
                dataMessage.setRequestData(gson.toJson(requestData));
                
                if (StringUtils.isNotBlank(openBookHelper.getScreenXhtml()) && StringUtils.isNotBlank(openBookHelper.getTotalPage()) && StringUtils.isNotBlank(openBookHelper.getTrialPage())) {
                    ScheduleJob sj = new ScheduleJob();
                    sj.setJobName(BookConvertHandler.class.getName());
                    sj.setJobGroup(requestData.getBook_file_id());
                    sj.setJobParam(dataMessage);
                    ScheduleJobManager.addJob(sj);
                }else{
                    throw new Exception(" check log:"+ convertedLogPath);
                }
            }else{
                throw new Exception("Preview_content is empty");
            }
            
            opebookLogger.log(TAG, "DataMessage: " + message + "\r\n" + "RequestData: " + dataMessage.getRequestData());

        } catch (Exception e) {
            e.printStackTrace();

            // String errorMessage = e.getClass().getName() + ": " +
            // e.getMessage();
            String errorMessage = ExceptionUtils.getStackTrace(e);
            if (e instanceof java.io.IOException && e.getMessage().contains("Wrong type of referenced length object COSObject")) {
                errorMessage = "Non-parseable PDF format, please re-save by Adobe Reader to fix this problem.";
            } else if (e instanceof ServerException) {
                errorMessage = ((ServerException) e).getError_message();
            } else if (e.getMessage().contains("No Space Left on Device")) {
                errorMessage = "No Space Left on Device. Please check book file size. After unzip, it may exceed size limitation." + "\r\n" + errorMessage;
            } else {
                errorMessage = "開書失敗 " + e.getMessage();
            }

            dataMessage.setResult(false);
            dataMessage.appendErrorMessage(errorMessage);
            opebookLogger.error(errorMessage);

            ScheduleJob sj = new ScheduleJob();
            sj.setJobName(NotifyAPIHandler.class.getName());
            sj.setJobRunner(1);
            sj.setJobParam(dataMessage);
            sj.setJobGroup(requestData.getBook_file_id());
            try {
                ScheduleJobManager.addJob(sj);
            } catch (Exception e1) {
                e1.printStackTrace();
                logger.error(TAG + " Fail to send job.", e1);
            }
            logger.info(TAG + "End TocAndOpf spend " + (System.currentTimeMillis() - jobStartTime) + "ms, at " + sdFormat.format(new Date()));
        }
        opebookLogger.uploadLog();
        return "TrialOpenBookHandler Done";

    }

    /**
     * 
     * @param outlog
     * @param list
     */
    private static void appendLog(StringBuilder outlog, List<GenericLogRecord> list) {
        if (null == list)
            return;
        for (GenericLogRecord tmprec : list) {
            outlog
                    // .append(tmprec.getSource())
                    // .append(":")
                    .append(tmprec.getLevel().toString()).append(":").append(tmprec.getMessage()).append("\r\n");
        }
    }

    public static void replaceAll(StringBuilder builder, String from, String to) {
        int index = builder.indexOf(from);
        while (index != -1) {
            builder.replace(index, index + from.length(), to);
            index += to.length(); // Move to the end of the replacement
            index = builder.indexOf(from, index);
        }
    }

    public void buildParameters(Properties properties) throws IOException {
        originalFolder = properties.getProperty("originalFolder") + "/"; // converted
        trialFolder = properties.getProperty("trialFolder") + "/";
        originalBucket = properties.getProperty("originalBucket"); // s3private-auth.books.com.tw
        URL_BookProcessResult = properties.getProperty("URL_BookProcessResult");
        logFolder = properties.getProperty("logFolder");
        // Create S3 Client
        this.s3Client = AmazonS3ClientBuilder.defaultClient();
    }

    @Override
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        // no DB needed.

    }

    public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
        return RunMode.OPENBOOK_EC2;
    }

    public static RunMode getInitRunMode(ScheduleJob job) {
        return RunMode.OPENBOOK_EC2;
    }

}
