package digipages.BookConvert.BookMain;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.utility.*;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class ConvertingBook {
	private static DPLogger logger = DPLogger.getLogger(ConvertingBook.class.getName());
    protected static String logTag = "[ConvertingBook]";
    protected BookFormat bookFormat;
    protected String bookFormatName;
    protected String originalBucket;
    protected String trialBucket;
    protected String originalFolder;
    protected String trialFolder;
    protected String logFolder;
    protected AmazonS3 s3Client;
    protected RequestData requestData;
    protected String srcBucket;
    protected String srcFolder;
    protected String srcFileName;
    protected String rootFolder;
    protected String targetBucket;
    protected String convertedLogPath;
    protected String checkSum;
    protected CannedAccessControlList Permission;
    protected LogToS3 ConvertingLogger;
    protected LogToS3 checkResultLogger;
    protected Properties properties;
    
    // bucket & path be used when AmazonS3Exception.
    protected String S3Bucket;
    protected String S3Key;
    
    protected String sourcePath;
    
    private long Maximum_Epub;
    private long Maximum_PDF;
    private long Maximum_FIXLAYOUT_Epub;
	protected long pdfPageObjCntLimit;
	protected boolean strictMode=false;
	protected int epubCheckMaxParagraph;
    protected boolean allowFixedLayout=false;
    protected String runMode="";
    protected int lambdaSizeLimitMB=80;
    protected DataMessage dataMessage;
    protected Set<String> mediaType;

    
    public ConvertingBook(AmazonS3 S3Client, RequestData requestData, Properties config, DataMessage datamessage) throws Exception {
    	this.dataMessage=datamessage;
        this.properties = config;
        this.originalBucket = properties.getProperty("originalBucket");
        logger.debug("setting originalBucket:" + originalBucket);
        this.trialBucket = properties.getProperty("trialBucket");
        logger.debug("setting trial bucket:" + trialBucket);
        this.originalFolder = properties.getProperty("originalFolder");
        this.trialFolder = properties.getProperty("trialFolder");
        this.logFolder = properties.getProperty("logFolder");
        this.runMode = properties.getProperty("runMode",String.valueOf(RunMode.LocalMode));
        this.lambdaSizeLimitMB = Integer.parseInt(properties.getProperty("lambdaSizeLimitMB","80"));
        try {
        	this.allowFixedLayout = Boolean.parseBoolean(properties.getProperty("allowFixedLayout"));
        } catch (Exception e)
        { 
        	// default = false 
        }
        this.Maximum_Epub = (new Long(properties.getProperty("Maximum_Epub"))) * 1000 * 1000; //MB to byte
        this.Maximum_PDF = (new Long(properties.getProperty("Maximum_PDF"))) * 1000 * 1000;
        this.Maximum_FIXLAYOUT_Epub = (new Long(properties.getProperty("Maximum_FIXLAYOUT_Epub","525"))) * 1000 * 1000;
        this.pdfPageObjCntLimit = (new Long(properties.getProperty("pdfPageObjCntLimit"))) * 1000 * 1000;
        this.epubCheckMaxParagraph=Integer.parseInt(properties.getProperty("EpubCheckMaxParagraph"));
        this.s3Client = S3Client;
        this.requestData = requestData;
        String checkResultgPath = getLogFolder() + "/" + "CheckResult.log";
        checkResultLogger = new LogToS3(s3Client, originalBucket, checkResultgPath);
        
        this.srcFileName = getFileName();
        Map<String, String> map = parsingUrl(requestData.getEfile_url());
        this.srcBucket = map.get("bucket");
        this.srcFolder = map.get("folder");
        this.bookFormatName = requestData.getFormat();
        setBookFormat();
        
        this.rootFolder = getRootFolder();
        this.targetBucket = requestData.getIsTrial() ? trialBucket : originalBucket;
        logger.debug("setting targetBucket:" + this.targetBucket);
        this.convertedLogPath = getLogFolder() + "/" + "converted.log";
        this.Permission = requestData.getIsTrial() ? 
                CannedAccessControlList.PublicRead : CannedAccessControlList.AuthenticatedRead;

        this.ConvertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
        ConvertingLogger.log(logTag, "rootFolder:" + rootFolder + ", logFolder:" + getLogFolder());
    }
    
    protected void convert() throws Exception {
        if (!requestData.getIsTrial()) {
            /*
            this.getBucket = this.srcBucket;
            this.getPath = this.srcFolder + "/" + this.srcFileName;
            CopyObjectRequest copyObjectRequest = new CopyObjectRequest(
                    this.getBucket, this.getPath,
                    originalBucket, "src/" + requestData.getItem() + "/" + this.srcFileName);
            copyObjectRequest.setCannedAccessControlList(s3Permission);
            s3Client.copyObject(copyObjectRequest);
            */
            
            generateCoverThumbnail();
        }
    }
    
    protected void setBookFormat() {
        String format = determineBookFormat(requestData.getFormat());
        
        if (format.equals("PDF")) {
            bookFormat = BookFormat.PDF;
        } else if (format.equals("Epub")) {
            bookFormat = BookFormat.Epub;
        } else {
            bookFormat = BookFormat.Unknow;
        }
    }
    
    public static String determineBookFormat(String input) {
        String format = "";
        
        if (input.equals("pdf")) {
            format = "PDF";
        } else if (input.equals("reflowable")
                || input.equals("fixedlayout") || input.equals("fixedphone")) {
            format = "Epub";
        } else {
            format = "Unknow";
        }
        
        return format;
    }
    

    
    protected Boolean checkBookSize() {        
        Boolean result = false;
        
        if (srcFileName.isEmpty()) {
            result = true;
        } else {
            S3Bucket = srcBucket;
            S3Key = srcFolder + "/" + srcFileName;
            String fileName = srcFileName;
            logger.debug("S3 get file:" + S3Bucket + ":" + S3Key);
            ObjectMetadata meta = s3Client.getObjectMetadata(S3Bucket, S3Key);
            long size = meta.getInstanceLength();
            
            if("fixedlayout".equalsIgnoreCase(bookFormatName)){
                if (size >= Maximum_FIXLAYOUT_Epub) {
                    SystemLog(logTag, fileName + " size is over than " + Maximum_FIXLAYOUT_Epub);
                    return false;
                } else {
                    return true;
                }
            }
            
            if (bookFormat == BookFormat.PDF) {
                if (size >= Maximum_PDF) {
                    SystemLog(logTag, fileName + " size is over than " + Maximum_PDF);
                    result = false;
                } else {
                    result = true;
                }
            } else if ((bookFormat == BookFormat.Epub)) {
                if (size >= Maximum_Epub) {
                    SystemLog(logTag, fileName + " size is over than " + Maximum_Epub);
                    result = false;
                } else {
                    result = true;
                }
            }
        }
        
        return result;
    }
    
    protected long getSrcBookSize() {        
        long size =0;
        S3Bucket = srcBucket;
        S3Key = srcFolder + "/" + srcFileName;
        String fileName = srcFileName;
        logger.debug("S3 get file:" + S3Bucket + ":" + S3Key);
        ObjectMetadata meta = s3Client.getObjectMetadata(S3Bucket, S3Key);
        size = meta.getInstanceLength();
        return size;
    }
    
    protected void getBookSource() throws Exception {
        File source = null;
        CommonUtil.delDirs("/tmp");
        if (bookFormat == BookFormat.Epub) {
            sourcePath = "/tmp/sourceBook_" + requestData.getBook_file_id() + ".epub";
            source = new File(sourcePath);
            
            if (srcFileName.isEmpty()) { // download from previous stored location.
                String bucket = srcBucket;
                String folder = srcFolder;
                ObjectListing objectList = s3Client.listObjects(bucket,  folder);
                SystemLog("[getBookSource]", "Download source for bucket:" + bucket + ", folder:" + folder);
                
                List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();                
                while (objectList.isTruncated()) {
                    objectList = s3Client.listNextBatchOfObjects(objectList);
                    objectSummaries.addAll(objectList.getObjectSummaries());
                }
                
                // Download all file
                String downloadFolderPath = "/tmp/S3Download/";
                File downloadFolder = new File(downloadFolderPath);
                for (int i = 0; i < objectSummaries.size(); i++) {
                    S3ObjectSummary summary = objectSummaries.get(i);
                    String key = summary.getKey();
                    
                    if (key.lastIndexOf("/") != key.length() - 1) {
                        String tempFilePath = downloadFolderPath + key.replace(srcFolder + "/", "");
                        String tempFolderPath = tempFilePath.substring(0, tempFilePath.lastIndexOf("/") + 1);
                        
                        File tempfolder = new File(tempFolderPath);
                        if (!tempfolder.exists()) {
                            tempfolder.mkdirs();
                        }
                        
                        Utility.downloadFromS3(s3Client, bucket, key, tempFilePath);
                    }
                }
                
                //  Zip file to .epub
                byte[] buffer = new byte[1024];
                FileOutputStream fos = new FileOutputStream(source);
                ZipOutputStream zos = new ZipOutputStream(fos);
                List<File> fileList = Utility.listAllFile(downloadFolder);
                
                for (File file : fileList) {                    
                    if (file.isFile()) {
                        ZipEntry ze = new ZipEntry(file.getPath().replace(downloadFolderPath, ""));
                        zos.putNextEntry(ze);
                        
                        InputStream in = new FileInputStream(file);
                        int len;
                        while ((len = in.read(buffer)) > 0) {
                            zos.write(buffer, 0, len);
                        }
                        
                        in.close();
                    }
                }
                
                zos.closeEntry();
                zos.close();
                fos.close();
                Utility.deleteDir(downloadFolder);
            } else {
                String key = srcFolder + "/" + srcFileName;
                source = Utility.downloadFromS3(s3Client, srcBucket, key, sourcePath);
            }
        } else if (bookFormat == BookFormat.PDF) {
            sourcePath = "/tmp/sourceBook_" + requestData.getBook_file_id() + ".pdf";
            source = new File(sourcePath);
            
            if (srcFileName.isEmpty()) {
                String bucket = srcBucket;
                String folder = srcFolder + "/PDF/";
                ObjectListing objectList = s3Client.listObjects(bucket,  folder);
                List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();
                
                while (objectList.isTruncated()) {
                    objectList = s3Client.listNextBatchOfObjects(objectList);
                    objectSummaries.addAll(objectList.getObjectSummaries());
                }
                
                for (int i = 0; i < objectSummaries.size(); i++) {
                    S3ObjectSummary summary = objectSummaries.get(i);
                    String key = summary.getKey();
                    
                    if (key.contains(".pdf")) {
                        SystemLog("[getBookSource]", "Download source for bucket:" + bucket + ", key:" + key);
                        source = Utility.downloadFromS3(s3Client, bucket, key, sourcePath);
                        srcFileName = key.substring(key.lastIndexOf("/") + 1);
                    }
                }
            } else {
                String key = srcFolder + "/" + srcFileName;
                source = Utility.downloadFromS3(s3Client, srcBucket, key, sourcePath);
            }
        }
        SystemLog("[getBookSource]", "File store to:" + sourcePath);
    }
    

	private String getFileName() {
        String fileName = "";
        
        if (requestData.getIsTrial() && requestData.getPreview_type().equals("3")) {
            fileName = requestData.getPreview_content();
        } else {
            if (requestData.getEfile_nofixed_name() != null) {
                fileName = requestData.getEfile_nofixed_name();
            }
        }
        
        SystemLog("[getFileName]", "BookFileName:" + fileName);
        
        return fileName;
    }
    
    protected String getLogFolder() {
        String folderPath;
        
        folderPath = logFolder + "/" + (requestData.getIsTrial() ? trialFolder : originalFolder) +
                "/" + requestData.getItem() + "/" + requestData.getBook_file_id();
        
        return folderPath;
    }

    private String getRootFolder() throws Exception {
        String folderPath;
        String hashCode;
        
        hashCode = Utility.encrypt(requestData.getItem() + requestData.getRequest_time());        
        folderPath = (requestData.getIsTrial() ? trialFolder : originalFolder) + "/" +
                hashCode.substring(hashCode.length() - 6) + "/" +
                requestData.getBook_file_id(); // {hash_code}/{book_file_id}
        
        return folderPath;
    }
    
    private void generateCoverThumbnail() throws ServerException {
    	try {
	        String Tag = "[generateCoverThumbnail]";
	        Long StartTime = System.currentTimeMillis();
	        ConvertingLogger.log(Tag, "Start generateCoverThumbnail()");
	        
	        final float MAX_WIDTH = 414;
	        final float MAX_HEIGHT = 554;
	        String JPG_TYPE = "jpg";
	        String JPG_MIME = "image/jpeg";
	        String folderPath = rootFolder.replace(
	                (requestData.getIsTrial() ? trialFolder : originalFolder), "cover");
	        String downloadUrl = requestData.getEfile_cover_url();
	        
	        // Download file from URL
	        URL source = new URL(downloadUrl);
	        
	        String fileName = source.getFile().contains("/") ? 
	                source.getFile().substring(source.getFile().lastIndexOf("/") + 1) :
	                source.getFile();
	        CommonUtil.delDirs("/tmp");
	        File downloadFile = new File("/tmp/" + fileName);
	        FileUtils.copyURLToFile(source, downloadFile);
	        
	        // Read the source image
	        BufferedImage srcImage = ImageIO.read(downloadFile);
	        int srcHeight = srcImage.getHeight();
	        int srcWidth = srcImage.getWidth();
	        
	        // Infer the scaling factor to avoid stretching the image unnaturally
	        float scalingFactor = Math.min(MAX_WIDTH / srcWidth, MAX_HEIGHT / srcHeight);
	        int width = (int) (scalingFactor * srcWidth);
	        int height = (int) (scalingFactor * srcHeight);
	        
	        BufferedImage resizedImage = new BufferedImage(width, height,
	                BufferedImage.TYPE_INT_RGB);
	        Graphics2D g = resizedImage.createGraphics();
	        
	        // Fill with white before applying semi-transparent (alpha) images
	        g.setPaint(Color.white);
	        g.fillRect(0, 0, width, height);
	        // Simple bilinear resize
	        // If you want higher quality algorithms, check this link:
	        // https://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
	        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	        g.drawImage(srcImage, 0, 0, width, height, null);
	        g.dispose();
	        
	        // Re-encode image to target format
	        ByteArrayOutputStream os = new ByteArrayOutputStream();
	        ImageIO.write(resizedImage, JPG_TYPE, os);
	        InputStream is = new ByteArrayInputStream(os.toByteArray());
	        // Set Content-Length and Content-Type
	        ObjectMetadata meta = new ObjectMetadata();
	        meta.setContentLength(os.size());
	        meta.setContentType(JPG_MIME);
	        
	        // Uploading to S3
	        s3Client.putObject(new PutObjectRequest(trialBucket, folderPath + "/" + fileName, is, meta)
	                .withCannedAcl(CannedAccessControlList.PublicRead));
	        
	        is.close();
	        os.close();
	        downloadFile.delete();
	        
	        ConvertingLogger.log(GenericLogLevel.Info,
	        		"End generateCoverThumbnail() spend " 
	        				+ (System.currentTimeMillis() - StartTime) + "ms"
	        		,ConvertingBook.class.getName());
    	} catch (java.net.ConnectException e)
    	{
    		throw new ServerException("id_err_315","Cannot download Cover Thumbnail from " + requestData.getEfile_cover_url());
    	}catch (Exception e)
    	{
    		logger.error("Unexcepted exception:" + e);
    		throw new ServerException("id_err_999",e.getMessage());
    	}
    }
    
    protected void notifyAPI(Boolean result, String errorMessage) throws Exception
    {
        String tag = "[NotifyAPI]";
        Long startTime = System.currentTimeMillis();
        
        SystemLog(tag, "Trigger NotifyAPI");
        
        DataMessage dataMessage = new DataMessage();
        dataMessage.setTargetBucket(targetBucket);
        dataMessage.setFolder(rootFolder);
        dataMessage.setBookFileName(srcFileName);
        dataMessage.setChecksum(checkSum);
        dataMessage.setLogFolder(getLogFolder());
        dataMessage.setResult(result);
        dataMessage.appendErrorMessage(errorMessage);
        dataMessage.setMedia(mediaType);

        
        Gson gson = new GsonBuilder().create();
        String data = gson.toJson(requestData);
        dataMessage.setRequestData(data);

        ScheduleJob sj =new ScheduleJob();
        sj.setJobParam(dataMessage);
        sj.setJobGroup(requestData.getBook_file_id()); 
        sj.setJobName(NotifyAPIHandler.class.getName());
        sj.setJobRunner(1);
//        sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
        ScheduleJobManager.addJob(sj);
        
        //delete file without blocker (sourcePath =null => Exception.)
        try {
	        File bookFile = new File(sourcePath);
	        if (bookFile.exists() && !bookFile.delete())
	        {
	        	bookFile.deleteOnExit();
	        }
	    } catch (Exception e){
	    	logger.info(logTag + " Delete file fail" + sourcePath + e.getMessage());
        }
        
        ConvertingLogger.log(GenericLogLevel.Info,
        		"RequestData: " + gson.toJson(requestData) + "\r\n" +
                "DataMessage: " + gson.toJson(dataMessage) + "\r\n" +
                "End triggerLambda() spend " + 
                (System.currentTimeMillis() - startTime) + "ms",
                ConvertingBook.class.getName());
        
        ConvertingLogger.uploadLog();
        SystemLog(tag, "DataMessage: " + gson.toJson(dataMessage));
    }
    
 
    
    protected Map<String, String> parsingUrl(String url) {
        Map<String, String> map = new HashMap<>();
        String bucket;
        String folder = "";
        
        String[] split = url.split("/");
        
        //If srcFileName is empty, url only has hashcode and processingID.
        if (srcFileName.isEmpty()) { 
            bucket = originalBucket;
            folder = "converted/" + url;
            
        } else {
            bucket = split[0];
            
            if (split.length > 1) {
                for (int i = 1; i < split.length; i++) {
                    folder = folder + split[i] + "/";
                }
                folder = folder.substring(0, folder.length() - 1);
            }
        }
        
        map.put("bucket", bucket);
        map.put("folder", folder);
        
        return map;
    }

    protected static void SystemLog(String Tag ,String str) {
        logger.info(Tag + str + "\r\n");
    }
    
    protected enum BookFormat {
        PDF, Epub, Unknow
    }

    public Set<String> getMediaType() {
        return mediaType;
    }

    public void setMediaType(Set<String> mediaType) {
        this.mediaType = mediaType;
    }
}