package digipages.BookConvert.BookMain;

import java.util.ArrayList;
import java.util.List;

import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.GenericLogRecord;

public class CheckResult {
    private Boolean result;
    private List <GenericLogRecord> messages=new ArrayList<>();
    
    @Override
    public String toString(){
    	String ret="";
    	for (GenericLogRecord rec:messages)
    	{
    		ret += rec.toString() + "\n";
    	}
    	return ret;
    }
    
    public List<GenericLogRecord> listMessages()
    {
    	return messages;
    }
    public Boolean getResult() {
        return result;
    }
    public void setResult(Boolean result) {
        this.result = result;
    }
    
    public List<GenericLogRecord> getMessages() {
        return messages;
    }
    public void addMessage(GenericLogRecord message) {
        this.messages.add(message);
    }
	public void addMessage(GenericLogLevel level, String resultMessage, String tag) {
		addMessage(new GenericLogRecord(level, resultMessage,tag));
		
	}
}