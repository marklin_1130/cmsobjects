package digipages.BookConvert.BookMain.EPubUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import digipages.common.DPLogger;

public class Manifest {
	
	private String basePath;
	private HashMap<String,Node> content=new HashMap<String,Node>();
	private static DPLogger logger = DPLogger.getLogger(Manifest.class.getName());
	
    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;

    {
        dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.warn("Not able to build DBFactory",e);
		}
    }
	
	
	public Manifest(String basePath,File contentOpfFile)

	{
		this.basePath = basePath;
		try {
			
	        InputStream objectData = new FileInputStream(contentOpfFile);
	        Document doc = dBuilder.parse(objectData);
	        Element root = doc.getDocumentElement();
	        EPubBookData.removeCommentAndEmptyText(root);
	        objectData.close();
	        
	        Node manifest = doc.getElementsByTagNameNS("*", "manifest").item(0);
	        NodeList spineNodeList = manifest.getChildNodes();
	        for (int i = 0; i < spineNodeList.getLength(); i++) {
	            Node curChild = spineNodeList.item(i);
	            String id = curChild.getAttributes().getNamedItem("id").getNodeValue();
	            content.put(id,curChild);
	        }

		}catch (Exception e)
		{
			logger.error("fail to build Spine from directory:" + contentOpfFile.getAbsolutePath(),e);
		}
	}
	
	public File getFileById(String id) {
		Node curNode = content.get(id);
		String fileName=curNode.getAttributes().getNamedItem("href").getNodeValue();
		
		return new File(basePath + fileName);
	}

}
