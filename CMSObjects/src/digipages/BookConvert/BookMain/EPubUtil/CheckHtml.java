package digipages.BookConvert.BookMain.EPubUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.helger.commons.collection.impl.ICommonsList;
import com.helger.css.ECSSVersion;
import com.helger.css.decl.CSSDeclaration;
import com.helger.css.decl.CSSSelector;
import com.helger.css.decl.CSSStyleRule;
import com.helger.css.decl.CascadingStyleSheet;
import com.helger.css.decl.ICSSSelectorMember;
import com.helger.css.reader.CSSReader;
import com.helger.css.reader.errorhandler.LoggingCSSParseErrorHandler;

import digipages.BookConvert.BookMain.CheckResult;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.LogToS3;
import digipages.common.DPLogger;

public class CheckHtml {
    private static DPLogger logger = DPLogger.getLogger(CheckHtml.class.getSimpleName());
    private static Set<String> scannedCss = new HashSet<>();
    private static Set<String> negativeMarginIssueCss = new HashSet<>();
    private static Set<String> negativeTextIndentIssueCss = new HashSet<>();
    private static Set<String> fontSizeIssueCss = new HashSet<>();
    private static Set<String> vhvwIssueCss = new HashSet<>();
    private static List<Map<String, List<String>>> issueCssList = new ArrayList<Map<String, List<String>>>();
    private static List<IssueCssRule> negativeMarginIssue = new ArrayList<IssueCssRule>();
    private static List<IssueCssRule> negativeTextIndentIssue = new ArrayList<IssueCssRule>();
    private static List<IssueCssRule> fontSizeIssue = new ArrayList<IssueCssRule>();
    private static List<IssueCssRule> vhvwSizeIssue = new ArrayList<IssueCssRule>();
    private static int textIndentErrorCount = 0;
    private static int fontSizeErrorCount = 0;
    private static int vhvwErrorCount = 0;
    public static boolean isScriptfound = false;
    
    /**
     * 
     * 2.另外也請再新增一個錯誤警告訊息，是若epub的css設定裡， 在以下的設定之後，若有［負數］值，請列出哪個設定裡有負值，並回覆錯誤訊息。
     * margin-top margin-bottom margin-right margin-left margin
     * 
     * @param htmlFile
     * @param checkResultLog
     * 
     * 20171110 CMS踢退邏輯增加：epub裡各種css裡有 Margin 為負號的書籍，例：margin-top:-0.7em
     * 20200311 CMS踢退邏輯增加：使用vh ,vw為單位警告
     */
    public static void getCss(String basepath, File srcFile, LogToS3 checkResultLog, CheckResult checkResult,String format) {
        // if (!srcFile.getName().endsWith(".xhtml"))
        // {
        // return;
        // }
        try {
//            System.out.println(srcFile.getName());
            if (StringUtils.containsIgnoreCase(srcFile.getName(), "xhtml") || StringUtils.containsIgnoreCase(srcFile.getName(), "html") || StringUtils.containsIgnoreCase(srcFile.getName(), "xml")) {

                String htmlContent = CommonUtil.loadFile2String(srcFile);

                List<String> cssFilesPath = CommonUtil.listCssFileNames(htmlContent);
                for (String cssPath : cssFilesPath) {
                    // skip scanned css
                    if (scannedCss.contains(cssPath)) {
                        continue;
                    } else {
                        scannedCss.add(cssPath);
//                        System.out.println("scanneCss" + cssPath);
                    }

                    checkCss(srcFile.getParent() + "/" + cssPath, checkResultLog, checkResult , format);

                }

            }
        } catch (Exception e) {
            logger.warn("Unexception error: ", e);
            checkResultLog.warn("Error Parsing file:" + srcFile.getName());
        }
    }

    public static void checkCssHtml(String basepath, File srcFile, LogToS3 checkResultLog, CheckResult checkResult,String fileFormat) {
        try {
//            System.out.println(srcFile.getName());
            if (StringUtils.containsIgnoreCase(srcFile.getName(), "xhtml") || StringUtils.containsIgnoreCase(srcFile.getName(), "html") || StringUtils.containsIgnoreCase(srcFile.getName(), "xml")) {

                String htmlContent = CommonUtil.loadFile2String(srcFile);

//                System.out.println(srcFile);
                Document doc = Jsoup.parse(htmlContent);

                // for loop all emement
                checkMarginElement(htmlContent, srcFile.getName(), doc, checkResultLog, checkResult);

                checkTextIndentElement(htmlContent, srcFile.getName(), doc, checkResultLog, checkResult);

                if(StringUtils.equals("reflowable",fileFormat )) {
                    checkFontSizeElement(htmlContent, srcFile.getName(), doc, checkResultLog, checkResult);
                }
                
                checkVHVWElement(htmlContent, srcFile.getName(), doc, checkResultLog, checkResult);

            }
        } catch (Exception e) {
            logger.warn("Unexception error: ", e);
            checkResultLog.warn("Error Parsing file:" + srcFile.getName());
        }
    }

    public void clearScannedCss() {
        scannedCss.clear();
    }

    /**
     * 1.若一本epub，從opf來看，第一頁裡不是只有一張圖的設定，就請要列出錯誤並踢退。 (第一頁只能有一個 svg/img, 超過或不足都要踢退) 補充：
     * reflowable => 只能有一張圖片 (多或少都踢退) fixedlayout => 一張圖片或以上
     * 
     */
    public static boolean checkEPubCover(EPubBookData epub, LogToS3 checkResultLog) {
        boolean checkPass = true;
        File coverHtmlFile = epub.getCoverFile();
        String failStr = "Fail to open epub file.";
        try {
            int imgCount = countImgSvg(coverHtmlFile);
            boolean isReflowable = epub.isReflowable();
            // #10 reflow單頁一圖置中 修改後不需要剔退 Reflowable 第一頁非圖片
            // if (isReflowable && imgCount!=1)
            // {
            // checkPass=false;
            // failStr="Reflowable need one and only one image in cover page.";
            // }
//            if (!isReflowable && imgCount < 1) {
//                checkPass = false;
//                failStr = "Fixedlayout need at lease one Image in cover page.";
//            }
        } catch (Exception e) {
            logger.error("Unexcepted Error", e);
            checkPass = false;
            checkResultLog.error(failStr + ":" + coverHtmlFile.getName());
        }
        if (!checkPass)
            checkResultLog.error(failStr + ":" + coverHtmlFile.getName());

        return checkPass;
    }

    private static int countImgSvg(File coverHtmlFile) {
        int imgCount = 0;
        try {
            Document htmlDoc = Jsoup.parse(CommonUtil.loadFile2String(coverHtmlFile));
            for (Element child : htmlDoc.body().children()) {
                imgCount += countImgSvg(child);
            }
        } catch (IOException e) {
            logger.error("Unxcepted Error", e);
        }
        return imgCount;
    }

    private static int countImgSvg(Element curElem) {
        if (curElem.tagName().equals("image") || curElem.tagName().equals("svg") || curElem.tagName().equals("img"))
            return 1;
        int imgCount = 0;
        for (Element child : curElem.children()) {
            imgCount += countImgSvg(child);
        }
        return imgCount;
    }

    // not used
    private static int countHtmlTag(String tagName, File coverHtmlFile) throws IOException {
        String content = CommonUtil.loadFile2String(coverHtmlFile);
        return StringUtils.countMatches(content, "<" + tagName);
    }

    public static void checkCss(String cssFilePath, LogToS3 checkResultLog, CheckResult checkResult,String format) {
        try {

            // String cssContent = CommonUtil.loadFile2String(cssFilePath).toLowerCase();

            File cssFile = new File(cssFilePath);
            
            if(!cssFile.exists()) {
                checkResult.addMessage(GenericLogLevel.Warn, "css file not found:" +cssFilePath,CheckHtml.class.getSimpleName());
                throw new Exception ("File not found:"+cssFilePath);
            }

            CascadingStyleSheet aCSS = CSSReader.readFromFile(cssFile, StandardCharsets.UTF_8, ECSSVersion.CSS30, new LoggingCSSParseErrorHandler());

            ICommonsList<CSSStyleRule> acssList = aCSS.getAllStyleRules();

            for (CSSStyleRule rule : acssList) {
                // CSSStyleRule rule = ((CSSStyleRule) icssTopLevelRule);
                StringBuilder sb = new StringBuilder();
                String memberString = "";
                List<String> elementTagList = new ArrayList<String>();
                List<String> classList = new ArrayList<String>();
                for (CSSSelector cssSelector : rule.getAllSelectors()) {
//                    System.out.println("@@@cssSelector:"+cssSelector.getAsCSSString());
                    String cssSelectorValue = cssSelector.getAsCSSString();
                    
                    if(cssSelectorValue.indexOf(",")>-1) {
                        
                        for (String cssItemValue : cssSelectorValue.split(",")) {
                            if(cssItemValue.indexOf(".")>-1) {
                                classList.add(cssItemValue);
                            }else {
                                elementTagList.add(cssItemValue);
                            }
                        }
                    }else {
                        if(cssSelectorValue.indexOf(".")>-1) {
                            classList.add(cssSelectorValue);
                        }else {
                            elementTagList.add(cssSelectorValue);
                        }
                        sb.append(cssSelectorValue).append(",");
                    }
                    
//                    for (ICSSSelectorMember icsselectorMember : cssSelector.getAllMembers()) {
//                        String member = icsselectorMember.getAsCSSString();
//                        if (member.startsWith(".")) {
//                            classList.add(member);
//                        } else {
//                            elementTagList.add(member);
//                        }
//                        sb.append(icsselectorMember.getAsCSSString()).append(",");
//                    }
                }

                memberString = sb.substring(0, sb.length() - 1);

                IssueCssRule aIssueMarginCssRule = new IssueCssRule();
                IssueCssRule aIssueTextIndentCssRule = new IssueCssRule();
                IssueCssRule aIssueFontSizeCssRule = new IssueCssRule();
                IssueCssRule aIssueVHVWSizeCssRule = new IssueCssRule();
                for (CSSDeclaration cssDeclaration : rule.getAllDeclarations()) {
                    String expression = cssDeclaration.getExpressionAsCSSString();
                    String property = cssDeclaration.getProperty();
//                    System.out.println(property + expression);
                    if (StringUtils.containsIgnoreCase(property, "margin")) {
                        if (StringUtils.contains(expression, "-")) {
                            int issueCssLineNumber = cssDeclaration.getSourceLocation().getFirstTokenBeginLineNumber();
                            aIssueMarginCssRule.setIssueCssMember(sb.substring(0, sb.length() - 1));
                            sb.append("issueCss:").append(property).append(expression).append("(").append(issueCssLineNumber).append(")");
                            aIssueMarginCssRule.setCssStyleRule(rule);
                            aIssueMarginCssRule.setIssueLineNumber(issueCssLineNumber);
                            aIssueMarginCssRule.setIssueCssString(sb.toString());
                            aIssueMarginCssRule.setIssueCssFileName(cssFile.getPath());
                            aIssueMarginCssRule.setElementTagList(elementTagList);
                            aIssueMarginCssRule.setClassList(classList);
                        }
                    }

                    if (StringUtils.containsIgnoreCase(property, "text-indent")) {
                        if (StringUtils.contains(expression, "-")) {
                            int issueCssLineNumber = cssDeclaration.getSourceLocation().getFirstTokenBeginLineNumber();
                            aIssueTextIndentCssRule.setIssueCssMember(sb.substring(0, sb.length() - 1));
                            sb.append("issueCss:").append(property).append(expression).append("(").append(issueCssLineNumber).append(")");
                            aIssueTextIndentCssRule.setCssStyleRule(rule);
                            aIssueTextIndentCssRule.setIssueLineNumber(issueCssLineNumber);
                            aIssueTextIndentCssRule.setIssueCssString(sb.toString());
                            aIssueTextIndentCssRule.setIssueCssFileName(cssFile.getPath());
                            aIssueTextIndentCssRule.setElementTagList(elementTagList);
                            aIssueTextIndentCssRule.setClassList(classList);
                        }
                    }

                    if(StringUtils.equals("reflowable",format )) {
                        if (StringUtils.containsIgnoreCase(property, "font-size")) {
    //                        System.out.println(expression);
                            Pattern pattern = Pattern.compile("[0-9\\.\\ ]*(large|LARGE|small|SMALL|em|EM|%)");
                            Matcher matcher = pattern.matcher(expression);
                            if (!matcher.find()&&!StringUtils.trimToEmpty(expression).equalsIgnoreCase("0")) {
                                int issueCssLineNumber = cssDeclaration.getSourceLocation().getFirstTokenBeginLineNumber();
                                aIssueFontSizeCssRule.setIssueCssMember(sb.substring(0, sb.length() - 1));
                                sb.append("issueCss:").append(property).append(expression).append("(").append(issueCssLineNumber).append(")");
                                aIssueFontSizeCssRule.setCssStyleRule(rule);
                                aIssueFontSizeCssRule.setIssueLineNumber(issueCssLineNumber);
                                aIssueFontSizeCssRule.setIssueCssString(sb.toString());
                                aIssueFontSizeCssRule.setIssueCssFileName(cssFile.getPath());
                                aIssueFontSizeCssRule.setElementTagList(elementTagList);
                                aIssueFontSizeCssRule.setClassList(classList);
                            }
                        }
                    }
                    
                    //檢查單位vh,vw
                    Pattern pattern = Pattern.compile("[0-9\\.\\ ]*(vh|vw)");
                    Matcher matcher = pattern.matcher(expression);
                    if (matcher.find()) {
                        int issueCssLineNumber = cssDeclaration.getSourceLocation().getFirstTokenBeginLineNumber();
                        aIssueVHVWSizeCssRule.setIssueCssMember(sb.substring(0, sb.length() - 1));
                        sb.append("issueCss:").append(property).append(expression).append("(").append(issueCssLineNumber).append(")");
                        aIssueVHVWSizeCssRule.setCssStyleRule(rule);
                        aIssueVHVWSizeCssRule.setIssueLineNumber(issueCssLineNumber);
                        aIssueVHVWSizeCssRule.setIssueCssString(sb.toString());
                        aIssueVHVWSizeCssRule.setIssueCssFileName(cssFile.getPath());
                        aIssueVHVWSizeCssRule.setElementTagList(elementTagList);
                        aIssueVHVWSizeCssRule.setClassList(classList);
                    }
                }

                if (aIssueMarginCssRule.getCssStyleRule() != null) {
                    negativeMarginIssue.add(aIssueMarginCssRule);
                    negativeMarginIssueCss.add(cssFile.getName());
                }
                if (aIssueTextIndentCssRule.getCssStyleRule() != null) {
                    negativeTextIndentIssue.add(aIssueTextIndentCssRule);
                    negativeTextIndentIssueCss.add(cssFile.getName());
                }
                if (aIssueFontSizeCssRule.getCssStyleRule() != null) {
                    fontSizeIssue.add(aIssueFontSizeCssRule);
                    fontSizeIssueCss.add(cssFile.getName());
                    for (String member : memberString.split(",")) {
//                        if (StringUtils.equalsIgnoreCase("p", StringUtils.trimToEmpty(member))) {
                            
                            if(fontSizeErrorCount <30) {
                                fontSizeErrorCount++;
                                checkResult.addMessage(GenericLogLevel.Error, "p font-size [css] 不是em、百分比、larger、smaller、0:" + aIssueFontSizeCssRule.getIssueCssFileName() + ":" + aIssueFontSizeCssRule.getIssueCssString(),CheckHtml.class.getSimpleName());
                            }
//                        }
                    }
                }
                if (aIssueVHVWSizeCssRule.getCssStyleRule() != null) {
                	vhvwSizeIssue.add(aIssueVHVWSizeCssRule);
                	vhvwIssueCss.add(cssFile.getName());
                    if(vhvwErrorCount <30) {
                    	vhvwErrorCount++;
                        checkResult.addMessage(GenericLogLevel.Warn, "出現vh,vw單位宣告:" + aIssueVHVWSizeCssRule.getIssueCssFileName() + ":" + aIssueVHVWSizeCssRule.getIssueCssString(),CheckHtml.class.getSimpleName());
                    }
                }
                
            }

//            for (IssueCssRule issueCssRule : negativeMarginIssue) {
//                System.out.println(issueCssRule.getIssueCssFileName() + ";" + issueCssRule.getIssueCssString());
//            }
//
//            for (IssueCssRule issueCssRule : negativeTextIndentIssue) {
//                System.out.println(issueCssRule.getIssueCssFileName() + ";" + issueCssRule.getIssueCssString());
//            }
//
//            for (IssueCssRule issueCssRule : fontSizeIssue) {
//                System.out.println(issueCssRule.getIssueCssFileName() + ";" + issueCssRule.getIssueCssString());
//            }

            // handle special seperator in css
            // cssContent = cssContent.replaceAll("\\}", "\\};");
            // cssContent = cssContent.replaceAll("\r", ";\r");
            // cssContent = cssContent.replaceAll("\n", ";\n");
            // String lines[] = cssContent.split("\\r?\\n");
            // int lineCount = 0;
            // for (String tmpLine : lines) {
            // lineCount++;
            //
            // String[] spiltStyle = tmpLine.split(";");
            // for (String string : spiltStyle) {
            // string += ";";
            //
            // // 檢查CSS Margin 有負值
            // if (isNegativeMargin(string)) {
            // tmpLine = StringUtils.trim(tmpLine);
            // checkResultLog.warn("Css margin warn:" + cssFilePath + ":" + tmpLine + "(" +
            // lineCount + ")", "CheckMarginCss");
            // }
            // // 檢查CSS TextIndent 有負值
            // if (isNegativeTextIndent(string)) {
            // tmpLine = StringUtils.trim(tmpLine);
            // // checkResultLog.error("Css text-indent error:" + cssFilePath + ":" +
            // // tmpLine,"CheckMarginCss");
            // checkResult.addMessage(GenericLogLevel.Error, "Css text-indent Error:" +
            // cssFilePath + ":" + tmpLine, CheckHtml.class.getSimpleName());
            // issueCss.add(cssFilePath);
            // int startIndex = cssContent.lastIndexOf("{", cssContent.indexOf(tmpLine));
            // // int lastIndex = cssContent.indexOf("}", cssContent.indexOf(tmpLine));
            // String cssClass = "";
            // if (cssContent.lastIndexOf("}", startIndex) > 0) {
            // cssClass = cssContent.substring(cssContent.lastIndexOf("}", startIndex),
            // startIndex);
            // } else {
            // cssClass = cssContent.substring(cssContent.lastIndexOf("\\r?\\n",
            // startIndex), startIndex);
            // }
            //
            // // String cssClassContent = cssContent.substring(startIndex, lastIndex + 1);
            // Map<String, List<String>> issueCssClass = new HashMap<String,
            // List<String>>();
            // List<String> classList = new ArrayList<String>();
            // String[] cssSpiltClass = cssClass.split(" ");
            // for (String className : cssSpiltClass) {
            // if (StringUtils.isNotBlank(className)) {
            // classList.add(StringUtils.deleteWhitespace(className).toLowerCase());
            // }
            // }
            // issueCssClass.put(cssFilePath, classList);
            // issueCssList.add(issueCssClass);
            // }
            // }
            // }

             } catch (FileNotFoundException e) {
             checkResultLog.warn("Missing css File: " + cssFilePath, "XD");
             } catch (IOException ioe) {
             checkResultLog.warn("File parsing fail:" + cssFilePath, "XD");
        } catch (Exception e) {
            logger.warn("Unexcepte", e);
        }
    }

//    public static void checkFontSizeCss(String cssFilePath, LogToS3 checkResultLog, CheckResult checkResult) {
//        try {
//            // p.?{([\s\S]+?)}
//            Pattern patternFindFontSize = Pattern.compile("(font-size:.*?\\d.*?(px|pt|PX|PT))");
//            String cssContent = CommonUtil.loadFile2String(cssFilePath);
//            // handle special seperator in css
//            // cssContent = cssContent.replaceAll("\\}", "\\};");
//            // cssContent = cssContent.replaceAll("\r", ";\r");
//            // cssContent = cssContent.replaceAll("\n", ";\n");
//            int lineCount = 0;
//            String lines[] = cssContent.split("\\r?\\n");
//            for (String css : lines) {
//                lineCount++;
//                Matcher matcher = patternFindFontSize.matcher(css);
//                if (matcher.find()) {
//                    String issueCssLine = matcher.group(1);
//                    // checkResultLog.error("Css font-size Error:" + cssFilePath + ":" + issueCss,
//                    // "checkPfontSizeCss");
//                    if(fontSizeErrorCount <30) {
//                        fontSizeErrorCount++;
//                        checkResult.addMessage(GenericLogLevel.Warn, "Css font-size Error:" + cssFilePath + ":" + issueCssLine + "(" + lineCount + ")", CheckHtml.class.getSimpleName());
//                    }
//                    // issueCss.add(cssFilePath);
//                }
//            }
//
//        } catch (FileNotFoundException e) {
//            checkResultLog.warn("Missing css File: " + cssFilePath, "XD");
//        } catch (IOException ioe) {
//            checkResultLog.warn("File parsing fail:" + cssFilePath, "XD");
//        } catch (Exception e) {
//            logger.warn("Unexcepte", e);
//        }
//    }

    private static void checkMarginElement(final String htmlContent, String filePath, Document doc, LogToS3 checkResultLog, CheckResult checkResult) throws IOException {
        
        
        
        // 檢查HEADER
        Element header = doc.head();
        for (Node child : header.childNodes()) {
            if (child.hasAttr("type") && child.attr("type").equalsIgnoreCase("text/css") && child.attr("href").length() > 0) {
                String value = child.attr("href");
                for (String issueCss : negativeMarginIssueCss) {
                    // 有使用到有問題的CSS檔案
                    if (StringUtils.containsIgnoreCase(value, issueCss)) {
                        // 檢查CSS規則tag是否被引用
                        for (IssueCssRule issueCssRule : negativeMarginIssue) {
                            for (String tag : issueCssRule.getElementTagList()) {
                                if (!doc.getElementsByTag(tag).isEmpty()) {
                                    checkResult.addMessage(GenericLogLevel.Warn, "negative margin [tag]:" + filePath +"tag:" + tag+ ":" + issueCssRule.getIssueCssFileName() +"css"+issueCssRule.getIssueCssString()+ "("+issueCssRule.getIssueLineNumber()+")", CheckHtml.class.getSimpleName());
                                }
                            }
                        }
                    }
                }
            }
        }

        Elements allElements = doc.getAllElements();
        int elementCount = 0;
        for (Element element : allElements) {
			elementCount++;            if (element.hasAttr("class")) {
                String classValue = element.className();
                for (IssueCssRule issueCssRule : negativeMarginIssue) {
                    for (String issueClass : issueCssRule.getClassList()) {
                        if (StringUtils.containsIgnoreCase(issueClass, classValue)) {
                            int lineNumber = findLineNumber(htmlContent, elementCount);
                            checkResult.addMessage(GenericLogLevel.Warn,
                                    "negative margin [class]:" + filePath + ":" + issueCssRule.getIssueCssFileName() + ":tag:" + element.tagName() + ",class:" + classValue + ":css:"+issueCssRule.getIssueCssString() + "(" + lineNumber + ")",
                                    CheckHtml.class.getSimpleName());
                        }
                    }
                }
            }

            if (element.hasAttr("style")) {
                String styleValue = element.attr("style");
                for (String tmpStyle : styleValue.split(";")) {
                    if (isNegativeMargin(tmpStyle)) {
                        styleValue = StringUtils.trim(styleValue);
                        int lineNumber = findLineNumber(htmlContent, elementCount);
                        checkResult.addMessage(GenericLogLevel.Warn, "negative margin [style]:" + filePath + ":" + "tag:" + element.tagName() + ",style:" + styleValue + ":" + "(" + lineNumber + ")",
                                CheckHtml.class.getSimpleName());
                    }
                }
            }
        }
    }

    private static void checkTextIndentElement(final String htmlContent, String filePath, Document doc, LogToS3 checkResultLog, CheckResult checkResult) {
        // 檢查HEADER
        Element header = doc.head();
        for (Node child : header.childNodes()) {
            if (child.hasAttr("type") && child.attr("type").equalsIgnoreCase("text/css") && child.attr("href").length() > 0) {
                String value = child.attr("href");
                for (String issueCss : negativeTextIndentIssueCss) {
                    // 有使用到有問題的CSS檔案
                    if (StringUtils.containsIgnoreCase(value, issueCss)) {
                        // 檢查CSS規則tag是否被引用
                        for (IssueCssRule issueCssRule : negativeTextIndentIssue) {
                            for (String tag : issueCssRule.getElementTagList()) {
                                if (!doc.getElementsByTag(tag).isEmpty()) {
                                    if(textIndentErrorCount <30) {
                                        textIndentErrorCount++;
                                        checkResult.addMessage(GenericLogLevel.Warn, "negative text-indent [tag]:" + filePath + ":" + issueCssRule.getIssueCssFileName() + ":tag:" + tag+":css:"+issueCssRule.getIssueCssString(), CheckHtml.class.getSimpleName());
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        Elements allElements = doc.getAllElements();
        int elementCount = 0;
        for (Element element : allElements) {
        	
            elementCount++;
            if (element.hasAttr("class")) {
                String classValue = element.className();
                for (IssueCssRule issueCssRule : negativeTextIndentIssue) {
                    for (String issueClass : issueCssRule.getClassList()) {
                        if (StringUtils.containsIgnoreCase(issueClass, classValue)) {
                            int lineNumber = findLineNumber(htmlContent, elementCount);
                            if(textIndentErrorCount <30) {
                                textIndentErrorCount++;
                                checkResult.addMessage(GenericLogLevel.Warn,
                                        "negative text-indent [class]:" + filePath + ":" + issueCssRule.getIssueCssFileName() + ":tag:" + element.tagName() + ",class:" + classValue +":css:"+issueCssRule.getIssueCssString()+ "(" + lineNumber + ")",
                                        CheckHtml.class.getSimpleName());
                            }
                        }
                    }
                }
            }

            if (element.hasAttr("style")) {
                String styleValue = element.attr("style");
                for (String tmpStyle : styleValue.split(";")) {
                    if (isNegativeTextIndent(tmpStyle)) {
                        styleValue = StringUtils.trim(styleValue);
                        int lineNumber = findLineNumber(htmlContent, elementCount);
                        if(textIndentErrorCount <30) {
                            textIndentErrorCount++;
                            checkResult.addMessage(GenericLogLevel.Warn, "negative text-indent [style]:" + filePath + ":" + "tag:" + element.tagName() + ",style:" + styleValue + ":" + "(" + lineNumber + ")",
                                    CheckHtml.class.getSimpleName());
                        }
                    }
                }
            }
            
        	//js check
        	if(element.tagName().equalsIgnoreCase("script")) {
        		int lineNumber = findLineNumber(htmlContent, elementCount);
        		checkResult.addMessage(GenericLogLevel.Warn,
                        "script found:" + filePath + "(" + lineNumber + ")",
                        CheckHtml.class.getSimpleName());
        		isScriptfound = true;
        	}
        	
        }
    }

    // 20171110
    // 3 若在<p>裡的font-size= 的單位為pt或px
    // (若使用 small/large，或em或rem做單位，不用處理)
    //
    // CheckHtml 增加檢查

    private static void checkFontSizeElement(final String htmlContent, String filePath, Document doc, LogToS3 checkResultLog, CheckResult checkResult) {
        
     // 檢查HEADER
        Element header = doc.head();
        for (Node child : header.childNodes()) {
            if (child.hasAttr("type") && child.attr("type").equalsIgnoreCase("text/css") && child.attr("href").length() > 0) {
                String value = child.attr("href");
                for (String issueCss : fontSizeIssueCss) {
                    // 有使用到有問題的CSS檔案
                    if (StringUtils.containsIgnoreCase(value, issueCss)) {
                        // 檢查CSS規則tag是否被引用
                        for (IssueCssRule issueCssRule : fontSizeIssue) {
                            for (String tag : issueCssRule.getElementTagList()) {
                                if (!doc.getElementsByTag(tag).isEmpty()) {
                                    if(fontSizeErrorCount <30) {
                                        fontSizeErrorCount++;
                                        checkResult.addMessage(GenericLogLevel.Error, "p font-size [tag]:" + filePath + ":" + issueCssRule.getIssueCssFileName() + ":tag:" + tag+":css:"+issueCssRule.getIssueCssString(), CheckHtml.class.getSimpleName());
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        Elements allElements = doc.getAllElements();
        int elementCount = 0;
        for (Element element : allElements) {
            elementCount++;
            if (element.hasAttr("class")) {
                String classValue = element.className();
                for (IssueCssRule issueCssRule : fontSizeIssue) {
                    for (String issueClass : issueCssRule.getClassList()) {
                        if (StringUtils.containsIgnoreCase(issueClass, classValue)) {
                            int lineNumber = findLineNumber(htmlContent, elementCount);
                            if(fontSizeErrorCount <30) {
                                fontSizeErrorCount++;
                            checkResult.addMessage(GenericLogLevel.Error,
                                    "p font-size [class]:" + filePath + ":" + issueCssRule.getIssueCssFileName() + ":tag:" + element.tagName() + ",class:" + classValue +":css:"+issueCssRule.getIssueCssString()+ "(" + lineNumber + ")",
                                    CheckHtml.class.getSimpleName());
                            }
                        }
                    }
                }
            }
            
//            Pattern pattern = Pattern.compile("(font-size:.*?\\d.*?(px|pt|PX|PT))");
            Pattern pattern = Pattern.compile("font-size:([0-9\\.\\ ]*large|[0-9\\.\\ ]*LARGE|[0-9\\.\\ ]*small|[0-9\\.\\ ]*SMALL|[0-9\\.\\ ]*em|[0-9\\.\\ ]*EM|[0-9\\.\\ ]*%|[\\ ]*0)");
            
            if (element.hasAttr("style")) {
                String styleValue = element.attr("style");
                for (String tmpStyle : styleValue.split(";")) {
                    if(tmpStyle.contains("font-size")) {
                        System.out.println("font-size check:"+tmpStyle);
                        Matcher matcher = pattern.matcher(StringUtils.normalizeSpace(tmpStyle));
                        if (!matcher.find()) {
                            int lineNumber = findLineNumber(htmlContent, elementCount);
                            if(fontSizeErrorCount <30) {
                                fontSizeErrorCount++;
                                checkResult.addMessage(GenericLogLevel.Error, "p font-size [style] 不是em、百分比、larger、smaller、0:" + filePath + ":" + "tag:" + element.tagName() + ",style:" + tmpStyle + ":" + "(" + lineNumber + ")", CheckHtml.class.getSimpleName());
//                            checkResult.addMessage(GenericLogLevel.Error, "p font-size [style]:" + filePath + ":" + "tag:" + element.tagName() + ",style:" + tmpStyle + ":" + "(" + lineNumber + ")", CheckHtml.class.getSimpleName());
                            }
                        }
                    }
                }
            }
        }
//        
//        
//        for (Element element : allElements) {
//            if (StringUtils.equalsIgnoreCase("p", element.tagName())) {
//                String styleLine = element.attr("style");
//                if (StringUtils.isNotBlank(styleLine)) {
//                    Matcher matcher = pattern.matcher(styleLine);
//                    if (matcher.find()) {
//                        // checkResultLog.error("Element style font-size Error:" + filePath + ":" +
//                        // element.tagName() + ":" + styleLine, "checkfontSizeCss");
//                        int lineNumber = findLineNumber(htmlContent, elementCount);
//                        checkResult.addMessage(GenericLogLevel.Error, "p font-size [style]:" + filePath + ":" + "tag:" + element.tagName() + ",style:" + styleLine + ":" + "(" + lineNumber + ")",
//                                CheckHtml.class.getSimpleName());
//                    }
//                }
//            }
//        }
    }
    
 // 20200311
    //  檢查單位為vh或vw
    //
    // CheckHtml 增加檢查

    private static void checkVHVWElement(final String htmlContent, String filePath, Document doc, LogToS3 checkResultLog, CheckResult checkResult) {
        
     // 檢查HEADER
        Element header = doc.head();
        for (Node child : header.childNodes()) {
            if (child.hasAttr("type") && child.attr("type").equalsIgnoreCase("text/css") && child.attr("href").length() > 0) {
                String value = child.attr("href");
                for (String issueCss : vhvwIssueCss) {
                    // 有使用到有問題的CSS檔案
                    if (StringUtils.containsIgnoreCase(value, issueCss)) {
                        // 檢查CSS規則tag是否被引用
                        for (IssueCssRule issueCssRule : vhvwSizeIssue) {
                            for (String tag : issueCssRule.getElementTagList()) {
                                if (!doc.getElementsByTag(tag).isEmpty()) {
                                    if(vhvwErrorCount <30) {
                                    	vhvwErrorCount++;
                                        checkResult.addMessage(GenericLogLevel.Warn, "出現vh,vw單位宣告:" + filePath + ":" + issueCssRule.getIssueCssFileName() + ":tag:" + tag+":css:"+issueCssRule.getIssueCssString(), CheckHtml.class.getSimpleName());
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        Elements allElements = doc.getAllElements();
        int elementCount = 0;
        for (Element element : allElements) {
            elementCount++;
            if (element.hasAttr("class")) {
                String classValue = element.className();
                for (IssueCssRule issueCssRule : vhvwSizeIssue) {
                    for (String issueClass : issueCssRule.getClassList()) {
                        if (StringUtils.containsIgnoreCase(issueClass, classValue)) {
                            int lineNumber = findLineNumber(htmlContent, elementCount);
                            if(vhvwErrorCount <30) {
                            	vhvwErrorCount++;
                            checkResult.addMessage(GenericLogLevel.Warn, "出現vh,vw單位宣告:" + filePath + ":" + issueCssRule.getIssueCssFileName() + ":tag:" + element.tagName() + ",class:" + classValue +":css:"+issueCssRule.getIssueCssString()+ "(" + lineNumber + ")",
                                    CheckHtml.class.getSimpleName());
                            }
                        }
                    }
                }
            }
            
            Pattern pattern = Pattern.compile("[0-9\\.\\ ]*(vh|vw)");
            
            if (element.hasAttr("style")) {
                String styleValue = element.attr("style");
                for (String tmpStyle : styleValue.split(";")) {
                    System.out.println("viewpoint-size check:"+tmpStyle);
                    Matcher matcher = pattern.matcher(StringUtils.normalizeSpace(tmpStyle));
                    if (matcher.find()) {
                        int lineNumber = findLineNumber(htmlContent, elementCount);
                        if(vhvwErrorCount <30) {
                        	vhvwErrorCount++;
                            checkResult.addMessage(GenericLogLevel.Warn, "出現vh,vw單位宣告:" + filePath + ":" + "tag:" + element.tagName() + ",style:" + tmpStyle + ":" + "(" + lineNumber + ")", CheckHtml.class.getSimpleName());
                        }
                    }
                }
            }
        }
    }

    private static boolean isNegativeMargin(String tmpStyle) {

        if (!tmpStyle.trim().toLowerCase().contains("margin")) {
            return false;
        }

        if (tmpStyle.trim().toLowerCase().contains("margin-")) {
            String marginCss = StringUtils.substringAfter(tmpStyle.toLowerCase(), "margin-");

            if (null == marginCss || marginCss.length() < 2)
                return false;
            try {
                return marginCss.contains("-");
            } catch (Exception e) {
                logger.warn("Unexcepted Error", e);
            }
        } else if (tmpStyle.trim().toLowerCase().contains("margin")) {
            String marginCss = StringUtils.substringAfter(tmpStyle.toLowerCase(), "margin");

            if (null == marginCss || marginCss.length() < 2)
                return false;
            try {
                return marginCss.contains("-");
            } catch (Exception e) {
                logger.warn("Unexcepted Error", e);
            }
        }

        return false;
    }

    private static boolean isNegativeTextIndent(String tmpStyle) {

        if (!tmpStyle.trim().toLowerCase().contains("text-indent")) {
            return false;
        }

        String textIndentCss = StringUtils.substringAfter(tmpStyle.toLowerCase(), "text-indent");

        if (null == textIndentCss || textIndentCss.length() < 0)
            return false;
        try {
            return textIndentCss.contains("-");
        } catch (Exception e) {
            logger.warn("Unexcepted Error", e);
        }
        return false;
    }

    private static int findLineNumber(String htmlSource, int elementNumber) {

        int lineNumber = -1;
        try (BufferedReader br = new BufferedReader(new StringReader(htmlSource))) {
            String line;
            lineNumber = 1;
            int lineEmementCount = -1;
            while ((line = br.readLine()) != null) {
                // process the line.
                // int match = StringUtils.countMatches(line, "</");
                // lineEmementCount = lineEmementCount+StringUtils.countMatches(line, "<");
                Pattern p = Pattern.compile("(<(?!\\/).+?>)");
                Matcher m = p.matcher(line);
                while (m.find()) {
                    lineEmementCount++;
                }

                if (lineEmementCount >= elementNumber) {
                    break;
                }
                lineNumber++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lineNumber;
    }

}
