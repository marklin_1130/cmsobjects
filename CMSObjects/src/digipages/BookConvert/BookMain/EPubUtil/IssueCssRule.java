package digipages.BookConvert.BookMain.EPubUtil;

import java.util.ArrayList;
import java.util.List;

import com.helger.css.decl.CSSStyleRule;

public class IssueCssRule {

    @Override
    public String toString() {
        return "IssueCssRule [cssStyleRule=" + cssStyleRule + ", issueLineNumber=" + issueLineNumber + "]";
    }

    private CSSStyleRule cssStyleRule;

    private int issueLineNumber;

    private String issueCssFileName;

    private String issueCssString;

    private String issueCssMember;

    List<String> elementTagList = new ArrayList<String>();

    List<String> classList = new ArrayList<String>();

    public CSSStyleRule getCssStyleRule() {
        return cssStyleRule;
    }

    public void setCssStyleRule(CSSStyleRule cssStyleRule) {
        this.cssStyleRule = cssStyleRule;
    }

    public int getIssueLineNumber() {
        return issueLineNumber;
    }

    public void setIssueLineNumber(int issueLineNumber) {
        this.issueLineNumber = issueLineNumber;
    }

    public String getIssueCssString() {
        return issueCssString;
    }

    public void setIssueCssString(String issueCssString) {
        this.issueCssString = issueCssString;
    }

    public String getIssueCssFileName() {
        return issueCssFileName;
    }

    public void setIssueCssFileName(String issueCssFileName) {
        this.issueCssFileName = issueCssFileName;
    }

    public String getIssueCssMember() {
        return issueCssMember;
    }

    public void setIssueCssMember(String issueCssMember) {
        this.issueCssMember = issueCssMember;
    }

    public List<String> getElementTagList() {
        return elementTagList;
    }

    public void setElementTagList(List<String> elementTagList) {
        this.elementTagList = elementTagList;
    }

    public List<String> getClassList() {
        return classList;
    }

    public void setClassList(List<String> classList) {
        this.classList = classList;
    }

}
