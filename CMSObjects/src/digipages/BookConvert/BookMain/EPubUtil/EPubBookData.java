package digipages.BookConvert.BookMain.EPubUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import digipages.BookConvert.utility.CommonUtil;
import digipages.common.DPLogger;

public class EPubBookData {
	private String epubPath;
	private String basePath;
	private File contentOpfFile;
	private File containerFile;
	private Manifest manifest;
	private Spine spine;
	private String conteinerData;
	private static DPLogger logger = DPLogger.getLogger(EPubBookData.class.getName());

    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;
    {
	    dbFactory = DocumentBuilderFactory.newInstance();
	    dbFactory.setNamespaceAware(true);
	    try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.error("Fail to build dbuilder",e);
		}
    }
	
	public EPubBookData(String epubPath) throws IOException, SAXException
	{
		this.epubPath=epubPath;
		buildEPubData();
	}
	
	public EPubBookData(File epubFolder) throws IOException, SAXException
	{
		this.epubPath=epubFolder.getAbsolutePath() + "/";
		buildEPubData();
	}

	/**
	 * 1. find conteiner.xml
	 * 2. find content.opf
	 * 3. define basePath
	 * 4. build manifest
	 * 5. build spine.
	 * @throws IOException 
	 * @throws SAXException 
	 */
	private void buildEPubData() throws IOException, SAXException {
		// container.xml file
		conteinerData = CommonUtil.loadFile2String(epubPath+ "META-INF/container.xml");
        Document doc = dBuilder.parse(new InputSource(new StringReader(conteinerData)));
        Node rootfile = doc.getElementsByTagName("rootfile").item(0);

        // contentOpf
        String tmpbasePath = epubPath + rootfile.getAttributes().getNamedItem("full-path").getNodeValue();
        contentOpfFile = new File(tmpbasePath);
        // basePath
        basePath = contentOpfFile.getParent() + "/";
        // manifest
		manifest = new Manifest(basePath,contentOpfFile);
		spine = new Spine(basePath,contentOpfFile);
		
	}

	
    public static void removeCommentAndEmptyText(Node root) {
        NodeList Nodes = root.getChildNodes();
        
        if(Nodes == null) return;
        
        // remove empty #test
        for(int i = Nodes.getLength() - 1; i >= 0; i--) {
            Node node = Nodes.item(i);
            removeCommentAndEmptyText(node);
            
            if (node instanceof Text) {
                String value = node.getNodeValue().trim();
                if (value.equals("") ) {
                   root.removeChild(node);
                }
            } else if (node instanceof org.w3c.dom.Comment) {
                root.removeChild(node);
            }
        }
    }
    
	/**
	 * 1. find first id in spine
	 * 2. find href in manifest
	 * 3. open file
	 * 
	 * @return
	 */
    public File getCoverFile()
    {
    	String coverId = spine.getCoverId();
    	return manifest.getFileById(coverId);
    }

    // judge fixedlayout.
    public boolean isReflowable() throws Exception {
    	
        Boolean result = true;
        InputStream objectData = new FileInputStream(contentOpfFile);
        Document doc = dBuilder.parse(objectData);
        NodeList metaList = doc.getElementsByTagNameNS("*", "meta");
        
        for (int i = 0; i < metaList.getLength(); i++) {
            Node meta = metaList.item(i);
            NamedNodeMap map = meta.getAttributes();
            Node propertyNode = map.getNamedItem("property");
            
            if (propertyNode != null) {
                String property = propertyNode.getNodeValue();
                
                if (property.contains("layout")) {
                    Node firstChild = meta.getFirstChild();
                    
                    if (firstChild.getNodeValue().contains("pre-paginated")) {
                        result = false;
                    }
                }
            }
        }
        
        objectData.close();
        
        return result;
    }
    
    //#69只以總html數計算，取前N%做為試閱本
    public long getTotalHtmlSize() throws IOException, SAXException {

        long totalHtml = 0;
        List<Node> spineNodeList = spine.getNodeList();
        // Calculate total file size in <spine>
        for (Node child : spineNodeList) {
            NamedNodeMap map = child.getAttributes();
            Node idref = map.getNamedItem("idref");
            Node linear = map.getNamedItem("linear");
            File targetFile=manifest.getFileById(idref.getNodeValue());
            //排除linear = yes 
            if (idref != null && ( linear==null || StringUtils.equalsIgnoreCase(linear.getNodeValue(), "yes"))) {
                totalHtml++;
//                System.out.println("count +1 :" +targetFile.getAbsolutePath());
                org.jsoup.nodes.Document targetFileDoc = Jsoup.parse(targetFile, "UTF-8");
                //排除空白頁
                if(targetFileDoc.getElementsByTag("body").text().length() < 1
                        && targetFileDoc.getElementsByTag("body").get(0).getElementsByTag("img").size() < 1 
                        && targetFileDoc.getElementsByTag("body").get(0).getElementsByTag("image").size() < 1
                        && targetFileDoc.getElementsByTag("body").get(0).getElementsByTag("svg").size() < 1) {
                    totalHtml--;
//                    System.out.println("blank -1 :" +targetFile.getAbsolutePath());
                }
            }
        }
        return totalHtml;
    }
    
    public long getTotalSize() throws IOException, SAXException
    {
    	long totalSize=0;
    	List<Node> spineNodeList=spine.getNodeList();
    	 // Calculate total file size in <spine>        
        for (Node child:spineNodeList) {
           
            NamedNodeMap map = child.getAttributes();
            Node idref = map.getNamedItem("idref");
            File targetFile=manifest.getFileById(idref.getNodeValue());
            
            totalSize += targetFile.length();
            
        }
		return totalSize;
    }

	public String getPageDirectionFromOpf() {
		return spine.getPageDirection();
	}

    public Manifest getManifest() {
        return manifest;
    }

    public void setManifest(Manifest manifest) {
        this.manifest = manifest;
    }
}
