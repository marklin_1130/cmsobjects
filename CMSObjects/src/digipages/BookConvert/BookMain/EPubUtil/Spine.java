package digipages.BookConvert.BookMain.EPubUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import digipages.common.DPLogger;

public class Spine {
	private List<Node> spineContent=new ArrayList<>();
	private String basePath;
	private String pageDirection;
    private static DPLogger logger = DPLogger.getLogger(Spine.class.getName()); 

    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;

    {
        dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.warn("Not able to build DBFactory",e);
		}
    }
	public Spine(String basePath,File contentOpfFile)
	{
		this.basePath=basePath;
		try {
			
	        InputStream objectData = new FileInputStream(contentOpfFile);
	        Document doc = dBuilder.parse(objectData);
	        Element root = doc.getDocumentElement();
	        EPubBookData.removeCommentAndEmptyText(root);
	        objectData.close();
	        
	        Node spine = doc.getElementsByTagNameNS("*", "spine").item(0);
	        pageDirection = loadPageDirection(spine);
	        NodeList spineNodeList = spine.getChildNodes();
	        for (int i = 0; i < spineNodeList.getLength(); i++) {
	            Node curChild = spineNodeList.item(i);
	            spineContent.add(curChild);
	        }

		}catch (Exception e)
		{
			logger.error("fail to build Spine from directory:" + contentOpfFile.getAbsolutePath(),e);
		}
	}
	private String loadPageDirection(Node spine) {
		if (!spine.hasAttributes())
			return "";
		String ret="";
		try {
			ret = spine.getAttributes().getNamedItem("page-progression-direction").getNodeValue();
		} catch (Exception e)
		{
			//fail to get value => consider it as empty 
		}
		return ret;
	}
	public String getCoverId()
	{
		Node coverNode = spineContent.get(0);
		NamedNodeMap nodeMap=coverNode.getAttributes();
		Node x = nodeMap.getNamedItem("idref");
		return x.getNodeValue();
	}
	public List<Node> getNodeList() {
		
		return spineContent;
	}
	public String getPageDirection() {
		return pageDirection;
	}
	
}
