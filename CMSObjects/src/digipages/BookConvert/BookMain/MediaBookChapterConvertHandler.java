package digipages.BookConvert.BookMain;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.google.gson.Gson;

import digipages.BookConvert.BookMain.ConvertingBook.BookFormat;
import digipages.BookConvert.TimeoutMonitor.TimeoutMonitorHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.vod.MediaBookRequestData;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;
import net.lingala.zip4j.exception.ZipException;

/**
 * main entry of Media Book Convert
 * @author Henry Cheng
 *
 */
public class MediaBookChapterConvertHandler {
    private static final String PATHSEP = "/";
    private static final String NO = "no";
    private AmazonS3 s3Client;
    private static DPLogger logger = DPLogger.getLogger(MediaBookChapterConvertHandler.class.getName());
    static Gson gson = new Gson();
    protected Properties properties;
    private MediaBookRequestData requestData;
    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;
    private String epubFolderPath;
    private File epubFolder;
    private String checkResultgPath;
    protected static String logTag = "[ConvertingMediaBook]";
    protected BookFormat bookFormat;
    protected String bookFormatName;
    protected String originalBucket;
    protected String trialBucket;
    protected String originalFolder;
    protected String trialFolder;
    protected String logFolder;
    protected String srcBucket;
    protected String srcFolder;
    protected String srcFileName;
    protected String rootFolder;
    protected String targetBucket;
    protected String convertedLogPath;
    protected String checkSum;
    protected CannedAccessControlList Permission;
    protected LogToS3 ConvertingLogger;
    protected LogToS3 checkResultLogger;
    // bucket & path be used when AmazonS3Exception.
    protected String S3Bucket;
    protected String S3Key;
    protected String sourcePath;
	protected boolean strictMode=false;
    protected String runMode="";
    protected DataMessage dataMessage;
    protected Set<String> mediaType;
    
    public MediaBookChapterConvertHandler() throws Exception {
    	this.s3Client = AmazonS3ClientBuilder.defaultClient();
        dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        dBuilder = dbFactory.newDocumentBuilder();
        checkResultgPath = getLogFolder() + PATHSEP + "CheckResult.log";
        this.requestData=requestData;
    }
    
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
        String logTag = "[MediaBookConvertHandler]";
        properties = config;
        ConvertingBook book = null;
        Long startTime = System.currentTimeMillis();
        
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        String errorMessage="";
        try {
            // init TimeoutMonitor 
            // Get RequestData from message
            DataMessage dataMessage = job.getJobParam(DataMessage.class);
            MediaBookRequestData requestData = gson.fromJson(dataMessage.getRequestData(), MediaBookRequestData.class);
            initTimeoutMonitor(requestData, job);
            
            String format = ConvertingBook.determineBookFormat(requestData.getFormat());
            //暫存暫時用不到
//            CommonUtil.delDirs("/tmp/");

            //根據item展開chpater轉檔資料
            for (String string : mediaType) {
            	requestData.getMediabookChapterList();
			}
//            throw new ServerException("id_err_315","Object is not a Epub or PDF file");

//        }catch (JobRetryException e){
//        	// just send retry. don't do things.
//        	throw e;
//        }
        }catch (Exception e) {
            
            if(e instanceof ServerException) {
                String errMsg = ((ServerException)e).getError_message();
                logger.error(logTag + "Exception in BookConvertHandler: "+errMsg);
                sendNotify(book,errMsg);                
            }else {
                logger.error(logTag + "Exception in BookConvertHandler: ",e);
                errorMessage = dumpErrMessage(e, book,job);
                sendNotify(book,errorMessage);
            }
        } finally {
            if(book != null && book.ConvertingLogger != null) {
                book.ConvertingLogger.uploadLog();
            }
        }
    
        return "Converter Done";
    }


	private void sendNotify(ConvertingBook book, String errorMessage) {
        if(book != null) {
            try {
            	book.notifyAPI(false,errorMessage);
            } catch (Exception e1) {
            	logger.error("Fail sending Notify",e1);
            }
        }
	}


	private String dumpErrMessage(Exception e, ConvertingBook book, ScheduleJob job) throws JobRetryException {
		
		String errorMessage=org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e);
        if(e.getClass().getName() != null && 
                e.getClass().getName().contains("AmazonS3Exception") && book!=null) {
            errorMessage = "file not found or size = 0, bucket:" + book.S3Bucket 
                    + ", key:" + book.S3Key + "\r\n" + errorMessage;
            
        } else if (e.getMessage() != null &&
                e.getMessage().contains("No Space Left on Device")) {
            errorMessage = "No Space Left on Device. Please check book file size. After unzip, it may exceed size limitation." +
                    "\r\n" + errorMessage;
            if (job.getJobRunner()==RunMode.LambdaMode.getValue())
            {
            	throw new JobRetryException("Lambda Limit reached, try Ec2 instead.");
            }
        } else	if (e.getMessage() != null && e.getMessage().contains("Wrong type of referenced length object COSObject"))
    	{
            errorMessage = "Non-parseable PDF format, please re-save by Adobe Reader to fix this problem." +
                    "\r\n" + errorMessage;
    	} else if (e instanceof ZipException)
    	{
    		errorMessage = "Unzip fail: please check epub format.";
    	}
        else if (e.getStackTrace() != null) {
            Boolean containPDFBox = false;
            for(int i = 0; i < e.getStackTrace().length; i++) {
                if(e.getStackTrace()[i].toString().contains("org.apache.pdfbox")) {
                    containPDFBox = true;
                }
            }
            
            if(containPDFBox) {
                errorMessage = "PDFBox Exception. Please check PDF file is complete." + "\r\n" + errorMessage;
            }
        }else{
            errorMessage = "Book convert failed. Please check book file is complete." + "\r\n" + errorMessage;
        }
		return errorMessage;
	}


	private void initTimeoutMonitor(MediaBookRequestData reqData, ScheduleJob origJob) {
		ScheduleJob job = new ScheduleJob();
		job.setJobName(TimeoutMonitorHandler.class.getName());
		job.setJobParam(reqData);
		job.setShouldFinishTime(CommonUtil.nextNMinutes(10));
		job.setJobRunner(RunMode.LocalMode.getValue());
		job.setJobGroup(reqData.getBook_file_id());
		try {
			ScheduleJobManager.addJob(job);
		} catch (Exception e) {
			logger.error("Fail Starting TimeoutMonitor",e);
		}
	}
	
	public static RunMode getRetryRunMode(RunMode runMode) {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
	
    protected String getLogFolder() {
        String folderPath;
        
        folderPath = logFolder + "/" + originalFolder + "/" + requestData.getItem() + "/" + requestData.getBook_file_id();
        
        return folderPath;
    }

}