package digipages.BookConvert.BookMain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Character.UnicodeBlock;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XHTMLChecker {
    private File xhtml;
    private final String Tag = "[XHTMLChecker]";

    public XHTMLChecker(File xhtml){
        this.xhtml = xhtml;
    }
    
    public Boolean tooManyChineseChars(int upperBound) throws IOException {
        int count = 0;
        
        BufferedReader reader = new BufferedReader( new FileReader(xhtml));
        char[] buf = new char[1024];
        int numRead;
        
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            for(int i = 0; i < numRead; i++) {
                count = isChinese(readData.charAt(i)) ? count + 1 : count;
            }
            
            if(count > upperBound) {
                return false;
            }
        }
        reader.close();
        
        return true;
    }
    
    public Boolean checkFileSize(long upperBound) {
        return xhtml.length() <= upperBound;
    }
    
    public List<String> checkTag() throws IOException, ParserConfigurationException, SAXException {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream listFile = classLoader.getResourceAsStream("tagList.txt");
        BufferedReader reader = new BufferedReader( new InputStreamReader(listFile,"UTF-8"));
        ArrayList<String> tagList = new ArrayList<>();
        ArrayList<String> illegalList;
        
        String buf;
        while((buf = reader.readLine()) != null) {
            tagList.add(buf);
        }
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        
        // Avoid DTD error
        dbFactory.setValidating(false);
        dbFactory.setNamespaceAware(true);
        //dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
        //dbFactory.setFeature("http://xml.org/sax/features/validation", false);
        //dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
        dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();

        Document doc = dBuilder.parse(xhtml);
        Element root = doc.getDocumentElement();
        
        illegalList = checkNodeForIllegalTag(root, tagList);
        
        reader.close();
        listFile.close();
        
        return illegalList;
    }
    
    public static Boolean isChinese(char c) {
        UnicodeBlock ub = UnicodeBlock.of(c);
        if (ub == UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS ||
                ub == UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS) {
            return true;
        }
        return false;
    }
    
    public int countChineseChars() throws IOException
    {
        int count = 0;
        
        BufferedReader reader = new BufferedReader( new FileReader(xhtml));
        char[] buf = new char[1024];
        int numRead ;
        
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            for(int i = 0; i < numRead; i++) {
                count = isChinese(readData.charAt(i)) ? count + 1 : count;
            }
            
        }
        reader.close();
        return count;
    }
    
    // checkAllNode - return a list which is including tags are not in tagList. 
    private ArrayList<String> checkNodeForIllegalTag(Node root, ArrayList<String> tagList) {
    	ArrayList<String> illegalList = new ArrayList<String>();
        Boolean isInclude = false;
        NodeList nodes = root.getChildNodes();
        
        for(int i = 0; i < tagList.size(); i++) {
            if(tagList.get(i).equals(root.getNodeName())) {
                isInclude = true;
            }
        }
        
        if(!isInclude) {
            illegalList.add(root.getNodeName());
        }
        
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            
            illegalList.addAll(checkNodeForIllegalTag(node, tagList));
        }

        return illegalList;
    }
}
