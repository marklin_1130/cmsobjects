package digipages.BookConvert.BookMain;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CopyPartRequest;
import com.amazonaws.services.s3.model.CopyPartResult;
import com.amazonaws.services.s3.model.GetObjectMetadataRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.Utility;
import digipages.BookConvert.utility.vod.MediaBookRequestApiData;
import digipages.BookConvert.utility.vod.MediaBookRequestData;
import digipages.BookConvert.utility.vod.MediabookConvertDataMessage;
import digipages.BookConvert.utility.vod.SubtitleRequestApiData;
import digipages.BooksHandler.vod.MediabookConvertNotifyHandler;
import digipages.BooksHandler.vod.MediabookResultNotifyHandler;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.CommonJobHandler.Runner.LocalRunner;
import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.MediabookAttach;
import model.MediabookChapter;
import model.MediabookConvertMapping;
import model.MediabookSubtitle;
import model.ScheduleJob;

//影音轉檔 步驟 2 處理檔案
public class MediaBookConvertHandler implements CommonJobHandlerInterface {
	private AmazonS3 s3Client;
	public static final String HANDLERNAME = "MediaBookConvertHandler";
	public static final String JOBGROUPNAME = "MEDIABOOK";
	public static final int JOBRUNNER = 0;
	private ScheduleJob scheduleJob;
	private static final DPLogger logger = DPLogger.getLogger(MediaBookConvertHandler.class.getName());
	private String itemId;
	private EntityManagerFactory emf;
	private static MediaBookConvertHandler myInstance = new MediaBookConvertHandler();
	protected static LogToS3 ConvertingLogger;
	protected LogToS3 checkResultLogger;
	private static final String logTag = "MediaBookConvertHandler";
	private String epubFolderPath;
	private String from_bucket ;
	private String src_Folder ;
	private String mediabook_process_Folder ;
	private String mediabook_ok_folder ;
	private String rootFolder;
	private static final String PATHSEP = "/";
	private CannedAccessControlList permission = CannedAccessControlList.AuthenticatedRead; 
//             CannedAccessControlList.PublicRead : CannedAccessControlList.AuthenticatedRead;
	private String targetBucket;
	private String originalFolder;
	private String trialBucket;
	
	public MediaBookConvertHandler() {
		LocalRunner.registerHandler(this);
	}

	public static void initJobHandler() {
		// not used.
	}

	public static List<ScheduleJob> queryMyJobs(EntityManager postgres) throws InterruptedException {
		// take jobs
		List<ScheduleJob> jobs;

		ScheduleJob jobParam = new ScheduleJob();
		jobParam.setUuid(UUID.randomUUID());
		jobParam.setJobName(MediaBookConvertHandler.HANDLERNAME);
		jobParam.setJobGroup(MediaBookConvertHandler.JOBGROUPNAME);
		jobParam.setJobRunner(MediaBookConvertHandler.JOBRUNNER);

		jobs = ScheduleJobManager.takeJobs(jobParam, 10);

		return jobs;
	}

	public ScheduleJob getScheduleJob() {
		return scheduleJob;
	}

	public void setScheduleJob(ScheduleJob scheduleJob) {
		this.scheduleJob = scheduleJob;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public static MediaBookConvertHandler getInstance() {
		return myInstance;
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private String getFolderDecodeFromItemId(String itemId) {
		StringBuilder folder = new StringBuilder();

//		media_tmp/
//		E07/
//		190/
//		88/
//		E071908862/

		return folder.append(itemId.substring(0, 3)).append("/").append(itemId.substring(3, 6)).append("/")
				.append(itemId.substring(6, 8)).append("/").append(itemId).append("/").toString();
	}

	@Override
	public String mainHandler(Properties conf, ScheduleJob scheduleJob) throws ServerException, JobRetryException {
		this.s3Client = AmazonS3ClientBuilder.defaultClient();
//		System.setProperty("com.amazonaws.sdk.disableEc2Metadata", AWS_EC2_METADATA_DISABLED_SYSTEM_PROPERTY);
//		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAI47WKLD65AUYRWXA", "VWU4HmYz6necoGFqEKwPNKLROPlVpZBfxXKBj1VM");
//		this.s3Client = AmazonS3ClientBuilder.standard().withRegion("ap-northeast-1")
//		                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
//		                        .build();
		
		EntityManager em = emf.createEntityManager();
		// 取出轉檔參數資料
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		MediabookConvertDataMessage dataMessage = scheduleJob.getJobParam(MediabookConvertDataMessage.class);
		MediaBookRequestData mediaBookRequestData = gson.fromJson(dataMessage.getRequestData(),
				MediaBookRequestData.class);

		String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
		String dateTimeString = String.valueOf(dateTime.getYear()) + "/"
				+ String.format("%02d", dateTime.getMonthValue()) + "/"
				+ String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour())
				+ ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

		logger.info("[LocalConsumer][mediabook] item_id: " + dataMessage.getItemId());

		setScheduleJob((ScheduleJob) scheduleJob);
		setItemId(dataMessage.getItemId());
		
		String file_cover_url = "";
		
		try {

		    Gson gsonNormal = new Gson();
		    
		    String mcmSql = "select * from mediabook_convert_mapping mcm where id = "+mediaBookRequestData.getConvertId();
		    
		    logger.info("[LocalConsumer][mediabook] mcmSql: " + mcmSql);
		    
			Boolean result=true;
			
			MediabookConvertMapping mediabookConvertMapping = (MediabookConvertMapping) em.createNativeQuery(mcmSql, MediabookConvertMapping.class).getSingleResult();
			
			trialBucket = conf.getProperty("trialBucket");
			String originalBucket = conf.getProperty("originalBucket"); // s3private-ebook.books.com.tw
			targetBucket = originalBucket;
			originalFolder = conf.getProperty("originalFolder"); // converted
			String logFolder = conf.getProperty("logFolder"); // log-space
			//		rootFolder 轉檔後的存放位置 = /converted/{hashCode}/{bookFileId}
//			rootFolder = getRootFolder(mediaBookRequestData.getItem(), mediaBookRequestData.getRequest_time(), originalFolder,mediaBookRequestData.getBook_file_id());
			rootFolder = "converted/"+dataMessage.getFolder();
			String folderPath = logFolder + "/" + originalFolder + "/" + mediaBookRequestData.getItem() + "/"+ mediaBookRequestData.getBook_file_id() + "/" + mediabookConvertMapping.getId();
			String convertedLogPath = folderPath + "/" + "converted.log";
			this.ConvertingLogger = new LogToS3(s3Client, originalBucket, convertedLogPath);
			ConvertingLogger.log(logTag, "rootFolder:" + rootFolder + ", logFolder:" + folderPath);

			 from_bucket = conf.getProperty("MediabookSrcBucket");
			 src_Folder = conf.getProperty("MediabookSrcFolder");
			 mediabook_process_Folder = conf.getProperty("MediabookProcessFolder");
			 mediabook_ok_folder = conf.getProperty("MediabookOkFolder");
			 file_cover_url = generateCoverThumbnail(mediaBookRequestData);
			 
			// 送出轉檔前檢查檔案是否都存在，如果有任一檔案不存在其他是否還需要繼續轉?
			// 先實作不存在 直接回失敗，檔案都在才送給宜沛轉檔 (展開chapter job)

			// 考慮到如果檔案數量太多S3 API操作需要花時間，因此由轉檔排程處理檔案檢查
			// 檢查全部chapter , subtitle , attach檔案
			
			//檢查完畢後 上傳attach檔案
			 
			boolean isPackageType2 = "Y".equalsIgnoreCase( mediaBookRequestData.getPackage_flag()) && "2".equalsIgnoreCase(mediaBookRequestData.getPackage_type());
	         //影音套書轉檔特別設定, 指定店內碼為試閱,進行轉檔,
            if(isPackageType2) {

                MediabookProcessResultData mediabookProcessResultData = new MediabookProcessResultData();
                mediabookProcessResultData.setFile_cover_url(file_cover_url);
                mediabookProcessResultData.setFile_url(rootFolder);
                mediabookProcessResultData.setItem(mediabookConvertMapping.getItemId());
                mediabookProcessResultData.setConvert_id(mediabookConvertMapping.getId().toString());
                mediabookProcessResultData.setCall_type(mediabookConvertMapping.getConvertType());
                
//              String data = gsonNormal.toJson(mediabookProcessResultData);
                ScheduleJob tmpJob = new ScheduleJob();
                tmpJob.setJobName(MediabookResultNotifyHandler.class.getName());
                tmpJob.setJobGroup("M"+String.valueOf(mediaBookRequestData.getConvertId()));
                tmpJob.setJobRunner(0); // for local
                tmpJob.setPriority(500);
                tmpJob.setRetryCnt(0);
                tmpJob.setStatus(0);
                tmpJob.setJobParam(mediabookProcessResultData);
                Random rand = new Random();
                int randomNum = rand.nextInt((1 - 5) + 1) + 1;
                tmpJob.setShouldStartTime(CommonUtil.nextNMinutes(randomNum));
                ScheduleJobManager.addJob(tmpJob);
                return "media book convert Finished. " + getItemId();
            }
            
			ConvertingLogger.log(logTag, "檢查檔案開始");
			List<MediabookChapter> chapterList = mediaBookRequestData.getMediabookChapterList();
			List<MediabookSubtitle> subtitleList = new ArrayList<MediabookSubtitle>();
			List<MediabookAttach> mediabookAttacList = mediaBookRequestData.getMediabookAttachList();
			List<String> filenames = new ArrayList<String> ();
			String path = getFolderDecodeFromItemId(mediaBookRequestData.getItem());
			
			StringBuilder sb = new StringBuilder();
			

	         //檔案檢查
			result = checkFileExist(sb ,result,path, chapterList, subtitleList, mediabookAttacList, filenames);
			
			if(!result) {
				//檔案檢查失敗回檔
				scheduleJob.setStatus(-1);
				scheduleJob.setErrMessage("檔案不存在:"+sb.toString());
                scheduleJob.setResultStr("轉檔失敗檔案不存在");
                MediabookProcessResultData processResultData = new MediabookProcessResultData();
                List<MediabookReturntData> rDataList = new  ArrayList<MediabookReturntData>();
                processResultData.setR_data(rDataList);
                processResultData.setConvert_result(result);
                processResultData.setCall_type(mediaBookRequestData.getCall_type());
                processResultData.setFile_cover_url(file_cover_url);
                processResultData.setItem(mediaBookRequestData.getItem());
                processResultData.setConvert_id(mediaBookRequestData.getConvertId().toString());
                processResultData.setError_message("file not exist:"+sb.toString());
                reportToBookServer(processResultData,conf.getProperty("efileProcessResult") ,conf.getProperty("bookAPIAuthKey"));
				return "media book convert Finished. " + getItemId();
			}
			
			System.setProperty("org.xml.sax.driver", "com.sun.org.apache.xerces.internal.parsers.SAXParser");
			
			//檔案移動 - 給宜沛的檔案
			for (String objectKey : filenames) {
				try {
				    String object_key = src_Folder + path + objectKey;
                    String copy_to_object_key = mediabook_process_Folder + path + dataMessage.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP + objectKey;
                    String to_bucket = from_bucket; // copy to is same bucket
					
                    boolean isObjectExist = false;
                    try {
                        isObjectExist = s3Client.doesObjectExist(to_bucket, copy_to_object_key);
                    } catch (Exception e) {
                    }
                    
                    if(!isObjectExist) {
                    // Initiate the multipart upload.
                    InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(to_bucket, copy_to_object_key);
                    InitiateMultipartUploadResult initResult = s3Client.initiateMultipartUpload(initRequest);
                    
                 // Get the object size to track the end of the copy operation.
                    GetObjectMetadataRequest metadataRequest = new GetObjectMetadataRequest(from_bucket, object_key);
                    ObjectMetadata metadataResult = s3Client.getObjectMetadata(metadataRequest);
                    long objectSize = metadataResult.getContentLength();
                    
                    // Copy the object using 5 MB parts.
                    long partSize = 5 * 1024 * 1024;
                    long bytePosition = 0;
                    int partNum = 1;
                    List<CopyPartResult> copyResponses = new ArrayList<CopyPartResult>();
                    while (bytePosition < objectSize) {
                        // The last part might be smaller than partSize, so check to make sure
                        // that lastByte isn't beyond the end of the object.
                        long lastByte = Math.min(bytePosition + partSize - 1, objectSize - 1);

                        // Copy this part.
                        CopyPartRequest copyRequest = new CopyPartRequest()
                                .withSourceBucketName(from_bucket)
                                .withSourceKey(object_key)
                                .withDestinationBucketName(to_bucket)
                                .withDestinationKey(copy_to_object_key)
                                .withUploadId(initResult.getUploadId())
                                .withFirstByte(bytePosition)
                                .withLastByte(lastByte)
                                .withPartNumber(partNum++);
                        copyResponses.add(s3Client.copyPart(copyRequest));
                        bytePosition += partSize;
                    }

                 // Complete the upload request to concatenate all uploaded parts and make the copied object available.
                    CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest(
                            to_bucket,
                            copy_to_object_key,
                            initResult.getUploadId(),
                            getETags(copyResponses));
                    s3Client.completeMultipartUpload(completeRequest);
                    
                    ConvertingLogger.log(logTag, "Multipart copy complete."+copy_to_object_key);
                    }
                    
				} catch (Exception e) {
			    	ConvertingLogger.error(logTag, "chpater檔案移動異常:"+objectKey + ",error:"+e.getMessage());
			    	result=false;
			    	sb.append(objectKey).append(",");
				}
			}
			
			// attach 附件 書籍檔案上傳-不需給宜沛 0624調整為不需上傳
//			for (MediabookAttach attach : mediabookAttacList) {
//				try {
//				    em.getTransaction().begin();
//					String object_key = src_Folder + path + attach.getAttachfileName();
//					
////					rootFolder 轉檔後的存放位置 = /converted/{hashCode}/{bookFileId}
//					s3Client.copyObject(from_bucket, object_key, targetBucket, rootFolder + PATHSEP+ attach.getAttachfileName());
//					GetObjectMetadataRequest metadataRequest = new GetObjectMetadataRequest(targetBucket, rootFolder + PATHSEP + attach.getAttachfileName());
//					final ObjectMetadata objectMetadata = s3Client.getObjectMetadata(metadataRequest);
//					Long contentLength = objectMetadata.getContentLength();
//					attach.setSize(contentLength.intValue());
//					
//					List<MediabookAttach> mediabookAttachList = em.createNamedQuery("MediabookAttach.findById", MediabookAttach.class).setParameter("id", attach.getId()).getResultList();
//					
//					if(!mediabookAttachList.isEmpty()) {
//					    //更新attch 將舊版attach 狀態設為 8 ,新版設為9上線
//					    em.createNativeQuery("update mediabook_attach set status = 8 where item_id ='"+attach.getItemId()+"' and attachfile_no ="+attach.getAttachfileNo() +" and status = 9").executeUpdate();
//    					mediabookAttachList.get(0).setSize(attach.getSize());
//    					mediabookAttachList.get(0).setFileLocation(rootFolder + PATHSEP);
//    					mediabookAttachList.get(0).setStatus(9);
//                        em.persist(mediabookAttachList.get(0));
//					}
//					 em.getTransaction().commit();
//				} catch (AmazonServiceException e) {
//			    	ConvertingLogger.error(logTag, "attach檔案上傳異常:"+attach.getAttachfileName() + ",error:"+e.getMessage());
//			    	result=false;
//			    	sb.append(attach.getAttachfileName()).append(",");
//				}
//			}
			
	         // script 文稿 書籍檔案上傳-不需給宜沛 
			
			for (MediabookChapter chapter : chapterList) {
				try {

				    if("Y".equalsIgnoreCase(chapter.getIsScript())) {
    					String object_key = src_Folder + path + chapter.getScriptFile();
    					
    //					rootFolder 轉檔後的存放位置 = /converted/{hashCode}/{bookFileId}
    					s3Client.copyObject(from_bucket, object_key, targetBucket, rootFolder + PATHSEP + chapter.getScriptFile());
    					
    					GetObjectMetadataRequest metadataRequest = new GetObjectMetadataRequest( targetBucket, rootFolder + PATHSEP + chapter.getScriptFile());
                        final ObjectMetadata objectMetadata = s3Client.getObjectMetadata(metadataRequest);
                        Long contentLength = objectMetadata.getContentLength();
//                        chapter.setSize(contentLength.intValue());
                        //文稿檔案大小回寫 , 沒有欄位可填 是否需要待確認
    
				    }
				} catch (Exception e) {
			    	ConvertingLogger.error(logTag, "script檔案上傳異常:"+chapter.getScriptFile() + ",error:"+e.getMessage());
			    	result=false;
			    	sb.append(chapter.getScriptFile()).append(",");
				}
			}
			//回寫檔案size
			
			
			
			if(!result) {
				//檔案搬移失敗回檔
				if(scheduleJob.getRetryCnt() >= 3) {
					scheduleJob.setStatus(-1);
					
//		            processResultData.setR_data(rDataList);
//		            processResultData.setConvert_result(result);
//					processResultData.setCall_type(mediaBookRequestData.getCall_type());
//					processResultData.setFile_cover_url(mediaBookRequestData.getEfile_cover_url());
//					processResultData.setItem(mediaBookRequestData.getItem());
//					processResultData.setConvert_id(mediaBookRequestData.getConvertId().toString());
//					processResultData.setError_message("檔案搬移失敗:"+sb.toString());
//					reportToBookServer(processResultData,conf.getProperty("efileProcessResult") ,conf.getProperty("bookAPIAuthKey"));
//					
					
					MediabookProcessResultData mediabookProcessResultData = new MediabookProcessResultData();
					List<MediabookReturntData> rDataList = new  ArrayList<MediabookReturntData>();
					mediabookProcessResultData.setConvert_result(false);
					mediabookProcessResultData.setR_data(rDataList);
	                mediabookProcessResultData.setFile_cover_url(file_cover_url);
	                mediabookProcessResultData.setFile_url(rootFolder);
	                mediabookProcessResultData.setItem(mediabookConvertMapping.getItemId());
	                mediabookProcessResultData.setConvert_id(mediabookConvertMapping.getId().toString());
	                mediabookProcessResultData.setCall_type(mediabookConvertMapping.getConvertType());
	                mediabookProcessResultData.setError_message("檔案搬移失敗:"+sb.toString());
	                
//	              String data = gsonNormal.toJson(mediabookProcessResultData);
	                ScheduleJob tmpJob = new ScheduleJob();
	                tmpJob.setJobName(MediabookResultNotifyHandler.class.getName());
	                tmpJob.setJobGroup("M"+String.valueOf(mediaBookRequestData.getConvertId()));
	                tmpJob.setJobRunner(0); // for local
	                tmpJob.setPriority(500);
	                tmpJob.setRetryCnt(0);
	                tmpJob.setStatus(0);
	                tmpJob.setJobParam(mediabookProcessResultData);
	                tmpJob.setShouldStartTime(CommonUtil.nextNMinutes(1));
	                ScheduleJobManager.addJob(tmpJob);
	                
	                List<MediabookChapter> mediabookChapterList = em.createNamedQuery("MediabookChapter.findByConverId", MediabookChapter.class).setParameter("convertId", mediabookConvertMapping.getId()).getResultList();
	                em.getTransaction().begin();
	                for (MediabookChapter mediabookChapter : mediabookChapterList) {
	                    mediabookChapter.setStatus(-1);
	                    em.persist(mediabookChapter);
	                }
	                em.getTransaction().commit();
					
					return "media book convert Finished. " + getItemId();
					
				}else {
					scheduleJob.setStatus(3);
					throw new JobRetryException("S3 file move fail");
				}
			}
			
			//準備資料呼叫宜沛
			if(result) {
				List<ScheduleJob> scheduleJobs = new ArrayList<>();
				
				for (MediabookChapter chapter : chapterList) {
				    
				    //只更新文稿script, status = 3 ,不需送給宜沛
				    if(chapter.getStatus() != 3) {
				    
    					// 展開chapter資料
    					MediaBookRequestApiData mediaBookRequestApiData = new MediaBookRequestApiData();
    					String to_bucket = from_bucket; // copy to is same bucket
    					String copy_to_object_folder = mediabook_process_Folder + path + dataMessage.getBookFileId() + PATHSEP + mediabookConvertMapping.getId() + PATHSEP;
    					if(StringUtils.isNotBlank(chapter.getChapterFile())) {
    					    mediaBookRequestApiData.setChapter_input("s3://"+to_bucket+ PATHSEP +copy_to_object_folder+chapter.getChapterFile());
    					}
    					mediaBookRequestApiData.setChapter_name(chapter.getChapterName());
    					mediaBookRequestApiData.setChapter_no(String.valueOf(chapter.getChapterNo()));
    					
                        // Convert_flow
                        // A:正式轉檔 B:正式和試閱轉檔C:試閱轉檔
    					if("Y".equalsIgnoreCase( mediaBookRequestData.getPackage_flag() )&& chapter.getIspreview().equalsIgnoreCase("Y")) {
    					    mediaBookRequestApiData.setConvert_flow("C");//只轉試閱
    					    if(chapter.getPreviewType().equalsIgnoreCase("2")) {
    					        mediaBookRequestApiData.setChapter_input(null);
                                mediaBookRequestApiData.setPreview_content("s3://"+to_bucket+PATHSEP+copy_to_object_folder+chapter.getPreviewContent());
                            }
                            if(chapter.getPreviewType().equalsIgnoreCase("1")) {
                                
                                if(chapter.getPreviewLength()!=null && chapter.getPreviewLength()>0) {
                                    mediaBookRequestApiData.setPreview_length(chapter.getPreviewLength());
                                }else {
                                    mediaBookRequestApiData.setPreview_length(300);
                                }
                            }
                            mediaBookRequestApiData.setPreview_type(chapter.getPreviewType());
    					}else {
    					    if(chapter.getIspreview().equalsIgnoreCase("Y")) {

                                mediaBookRequestApiData.setConvert_flow("B");
                                if(chapter.getPreviewType().equalsIgnoreCase("2")) {
                                    mediaBookRequestApiData.setPreview_content("s3://"+to_bucket+PATHSEP+copy_to_object_folder+chapter.getPreviewContent());
                                }
                                if(chapter.getPreviewType().equalsIgnoreCase("1")) {
                                    if(chapter.getPreviewLength()!=null && chapter.getPreviewLength()>0) {
                                        mediaBookRequestApiData.setPreview_length(chapter.getPreviewLength());
                                    }else {
                                        mediaBookRequestApiData.setPreview_length(300);
                                    }
                                }
                                mediaBookRequestApiData.setPreview_type(chapter.getPreviewType());
                            }else {
                                mediaBookRequestApiData.setConvert_flow("A");
                                mediaBookRequestApiData.setPreview_type("0");
                                mediaBookRequestApiData.setPreview_content("");
                            }
    					}
    					
//    					mediaBookRequestApiData.setDynamic_profile_option("");
    					mediaBookRequestApiData.setItem_id(chapter.getItemId());
    
    					//*處理序號!!
    					mediaBookRequestApiData.setProcessing_id(String.valueOf(chapter.getId()));
    					
    					List<SubtitleRequestApiData> subtitle = new ArrayList<SubtitleRequestApiData>();
    					mediaBookRequestApiData.setSubtitle(subtitle);
    					if(chapter.getSubtitles()!=null) {
        					for (MediabookSubtitle subtitleItem : chapter.getSubtitles()) {
        						SubtitleRequestApiData subtitleRequestApiData = new SubtitleRequestApiData();
        						subtitleRequestApiData.setChapter_no(String.valueOf(chapter.getChapterNo()));
        						subtitleRequestApiData.setItem_id(chapter.getItemId());
        						subtitleRequestApiData.setSubtitle_file("s3://"+to_bucket+PATHSEP+copy_to_object_folder+subtitleItem.getSubtitleFile());
        						subtitleRequestApiData.setSubtitle_id(subtitleItem.getSubtitleId());
        						subtitle.add(subtitleRequestApiData);
        					}
    					}
    					
    					//影音分別轉檔參數設定
    					if(mediaBookRequestApiData.getItem_id().contains("E07")) {
    						mediaBookRequestApiData.setProfile_name("AudioProfile");
    					} else {
    						mediaBookRequestApiData.setProfile_name("VideoProfile");
    					}
    					
    //					String mediaBookRequestApiDataJson = gsonNormal.toJson(mediaBookRequestApiData);
    //					DataMessage message = new DataMessage();
    //					chpaterScheduleJobParam jobParams = new ScheduleJobParam();
    					ScheduleJob tmpJob = new ScheduleJob();
    					tmpJob.setJobName(MediabookConvertNotifyHandler.class.getName());
    					tmpJob.setJobGroup("M"+String.valueOf(mediaBookRequestData.getConvertId())+"_"+chapter.getChapterNo());
    					tmpJob.setJobRunner(0); // for local
    					tmpJob.setPriority(500);
    					tmpJob.setRetryCnt(0);
    					tmpJob.setStatus(0);
    					tmpJob.setJobParam(mediaBookRequestApiData);
    					scheduleJobs.add(tmpJob);
    					
    				    }
				}
				
				
                List<MediabookChapter> mediabookChapterList = em.createNamedQuery("MediabookChapter.findByConverId", MediabookChapter.class).setParameter("convertId", mediabookConvertMapping.getId()).getResultList();
                
                em.getTransaction().begin();
                
                for (MediabookChapter mediabookChapter : mediabookChapterList) {
                    if(mediabookChapter.getStatus()==3) {
                        em.createNativeQuery("update mediabook_chapter set status = 8 where item_id ='"+mediabookChapter.getItemId()+"' and chapter_no ="+mediabookChapter.getChapterNo() +" and status = 9 and id <> "+mediabookChapter.getId()).executeUpdate();
                        mediabookChapter.setStatus(9);
                        mediabookChapter.setResponseMsg("更新script成功");
                    }else {
                        mediabookChapter.setStatus(1);
                    }
                    em.persist(mediabookChapter);
                }
                em.getTransaction().commit();
				
				ScheduleJobManager.addJobs(scheduleJobs);
				
//	          MediaBookChapterConvertHandler mediaBookChapterConvertHandler = new MediaBookChapterConvertHandler();
//	          mediaBookChapterConvertHandler.mainHandler(conf, scheduleJob);
	            
	            MediabookProcessResultData mediabookProcessResultData = new MediabookProcessResultData();
	            mediabookProcessResultData.setFile_cover_url(file_cover_url);
	            mediabookProcessResultData.setFile_url(rootFolder);
	            mediabookProcessResultData.setItem(mediabookConvertMapping.getItemId());
	            mediabookProcessResultData.setConvert_id(mediabookConvertMapping.getId().toString());
	            mediabookProcessResultData.setCall_type(mediabookConvertMapping.getConvertType());
	            
//	            String data = gsonNormal.toJson(mediabookProcessResultData);
	            ScheduleJob tmpJob = new ScheduleJob();
	            tmpJob.setJobName(MediabookResultNotifyHandler.class.getName());
	            tmpJob.setJobGroup("M"+String.valueOf(mediaBookRequestData.getConvertId()));
	            tmpJob.setJobRunner(0); // for local
	            tmpJob.setPriority(500);
	            tmpJob.setRetryCnt(0);
	            tmpJob.setStatus(0);
	            tmpJob.setJobParam(mediabookProcessResultData);
	            tmpJob.setShouldStartTime(CommonUtil.nextNMinutes(30));
	            ScheduleJobManager.addJob(tmpJob);
			}
			


		} catch (ServerException e) {
			logger.error("ServerException ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			ConvertingLogger.error(logTag,errors.toString());
			throw e;
		} catch (Exception e) {
			logger.error("ServerException ", e);
			StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            ConvertingLogger.error(logTag,errors.toString());
			throw new JobRetryException("id_err_999"+e.getMessage());
		} finally {
			ConvertingLogger.uploadLog();
			em.close();
		}

		return "media book convert Finished. " + getItemId();
	}

	private String getRootFolder(String itemId, String requestTime, String originalFolder, String bookFileId)
			throws Exception {
		String folderPath;
		String hashCode;
		hashCode = Utility.encrypt(itemId + requestTime);
		folderPath = originalFolder + "/" + hashCode.substring(hashCode.length() - 6) + "/" + bookFileId; // {hash_code}/{book_file_id}
		return folderPath;
	}

	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}
	
    private void uploadEpub(File dir) throws Exception {
    	logger.info("Uploading to S3:" + dir.getName());
        if (dir.isDirectory()) {
            String[] child = dir.list();
            for (int i = 0; i < child.length; i++) {
                uploadEpub(new File(dir, child[i]));
            }
        } else {
            String fileName = dir.getPath().replace(epubFolderPath, "");
            String extension = "";
            if(fileName.contains(".")) {
                extension = fileName.substring(fileName.lastIndexOf('.'));
            }
            MimeType mimeType = (MimeType.fromExtension(extension) == null) 
                    ? MimeType.fromExtension(".txt") : MimeType.fromExtension(extension);
                    
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentType(mimeType.getType());
            PutObjectRequest request = new PutObjectRequest(targetBucket, rootFolder + PATHSEP + fileName, dir).withCannedAcl(permission);
            request.setMetadata(meta);
            s3Client.putObject(request);
        }
    }
    
    private boolean checkFileExist(StringBuilder sb ,boolean result,String path ,List<MediabookChapter> chapterList ,List<MediabookSubtitle> subtitleList ,List<MediabookAttach> mediabookAttacList ,List<String> filenames) {
    	
        for (MediabookChapter mediabookChapter : chapterList) {
			try {
			    String object_key ="";
			    if(3 != mediabookChapter.getStatus()) {
			    
			        if(mediabookChapter.getSubtitles()!=null) {
	                    subtitleList.addAll(mediabookChapter.getSubtitles());
	                }
	                
			        //如果為指定試閱 可以允許chapter file為空
			        boolean isAllowChpaterEmpty = mediabookChapter.getIspreview().equalsIgnoreCase("y") && mediabookChapter.getPreviewType().equalsIgnoreCase("2");
			        
			        
			        if(isAllowChpaterEmpty) {
			            if( StringUtils.isNotBlank(mediabookChapter.getChapterFile()) ) {
			              //檢查章節檔案
	                        object_key = src_Folder + path + mediabookChapter.getChapterFile();
	                        filenames.add(mediabookChapter.getChapterFile());
	                        
	                        if(!s3Client.doesObjectExist(from_bucket, object_key)) {
	                            ConvertingLogger.error(logTag, "ChapterFile檔案不存在:"+object_key);
	                            result=false;
	                            sb.append(object_key).append(",");
	                        }
			            }
			        }else {
	                    //檢查章節檔案
	                    object_key = src_Folder + path + mediabookChapter.getChapterFile();
	                    filenames.add(mediabookChapter.getChapterFile());
	                    
	                    if(!s3Client.doesObjectExist(from_bucket, object_key)) {
	                        ConvertingLogger.error(logTag, "ChapterFile檔案不存在:"+object_key);
	                        result=false;
	                        sb.append(object_key).append(",");
	                    }
			        }
				
    				//檢查試閱檔案
                    if(mediabookChapter.getIspreview().equalsIgnoreCase("y")) {
                        if(mediabookChapter.getPreviewType().equalsIgnoreCase("2")) {
                            object_key = src_Folder + path + mediabookChapter.getPreviewContent();
                            filenames.add(mediabookChapter.getPreviewContent());
                            if(!s3Client.doesObjectExist(from_bucket, object_key)) {
                                ConvertingLogger.error(logTag, "PreviewContent檔案不存在:"+object_key);
                                result=false;
                                sb.append(object_key).append(",");
                            }
                        }
                    }
			    }
				
				if("Y".equalsIgnoreCase(mediabookChapter.getIsScript())) {
				    //檢查script檔案
	                object_key = src_Folder + path + mediabookChapter.getScriptFile();
	                filenames.add(mediabookChapter.getScriptFile());
    				if(!s3Client.doesObjectExist(from_bucket, object_key)) {
    					ConvertingLogger.error(logTag, "ScriptFile檔案不存在:"+object_key);
    					result=false;
    					sb.append(object_key).append(",");
    				}
				}
                
				
			} catch (AmazonServiceException e) {
		    	ConvertingLogger.error(logTag, "檔案存取異常:"+mediabookChapter.getChapterFile() + ",error:"+e.getMessage());
		    	result=false;
		    	sb.append(mediabookChapter.getChapterFile()).append(",");
			}
		}
		
		for (MediabookSubtitle subtitle : subtitleList) {
			try {

                //檢查字幕檔案
				String object_key = src_Folder + path + subtitle.getSubtitleFile();
				filenames.add(subtitle.getSubtitleFile());
				if(!s3Client.doesObjectExist(from_bucket, object_key)) {
					ConvertingLogger.error(logTag, "subtitle檔案不存在:"+object_key);
					result=false;
					sb.append(object_key).append(",");
				}
				
			} catch (AmazonServiceException e) {
		    	ConvertingLogger.error(logTag, "檔案存取異常:"+subtitle.getSubtitleFile() + ",error:"+e.getMessage());
		    	result=false;
		    	sb.append(subtitle.getSubtitleFile()).append(",");
			}
		}
		
		//調整不需要加密下載 CMS不用處理
//		for (MediabookAttach attach : mediabookAttacList) {
//			try {
//			  //檢查附件檔案
//				String object_key = src_Folder + path + attach.getAttachfileName();
//				if(!s3Client.doesObjectExist(from_bucket, object_key)) {
//					ConvertingLogger.error(logTag, "attach檔案不存在:"+object_key);
//					result=false;
//					sb.append(object_key).append(",");
//				}
//				
//			} catch (AmazonServiceException e) {
//		    	ConvertingLogger.error(logTag, "檔案存取異常:"+attach.getAttachfileName() + ",error:"+e.getMessage());
//		    	result=false;
//		    	sb.append(attach.getAttachfileName()).append(",");
//			}
//		}
		
		return result;
    }
    
    public static String reportToBookServer(MediabookProcessResultData post,String efileProcessResultURL ,String bookAPIAuthKey) throws IOException  {
		
		final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(50000).setConnectionRequestTimeout(50000).setSocketTimeout(50000).build();
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
		
		HttpPost httpPost = new HttpPost(efileProcessResultURL);
		//HttpPost httpPost = new HttpPost("https://ens2tu7mwtq5jeq.m.pipedream.net");
		
		httpPost.setHeader("app", "CMS");
		httpPost.setHeader("auth_id", "cms");
		httpPost.setHeader("auth_key", bookAPIAuthKey);
		httpPost.setHeader("Accept", "application/json");

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		builder.addTextBody("item", post.getItem());
		builder.addTextBody("call_type", post.getCall_type());
		builder.addTextBody("processing_id", post.getConvert_id());
		builder.addTextBody("return_file_num", String.valueOf(post.getReturn_file_num()));
		builder.addTextBody("convert_result", post.isConvert_result()? "true":"false");
		builder.addTextBody("error_message", StringUtils.trimToEmpty(post.getError_message()));
		builder.addTextBody("file_cover_url", StringUtils.trimToEmpty(post.getFile_cover_url()));
		builder.addTextBody("file_url", StringUtils.trimToEmpty(post.getFile_url()));
		builder.addTextBody("media_lengthtime", post.getMedia_lengthtime()!=null? String.valueOf(post.getMedia_lengthtime()) : "");
        builder.addTextBody("media_size", post.getMedia_size()!=null? String.valueOf(post.getMedia_size()) : "");
		String rdata = new Gson().toJson(post.getR_data());
		builder.addTextBody("r_data", URLEncoder.encode(rdata, "UTF-8"));
		HttpEntity entity = builder.build();
		httpPost.setEntity(entity);
		
		try {
        
		ConvertingLogger.log(logTag, "回覆轉檔結果:"+new Gson().toJson(post));
		   
        } catch (Exception e) {
            ConvertingLogger.log(logTag, "回覆轉檔結果:"+ExceptionUtils.getStackTrace(e));
        }
		
		IOException ioex=null;
		CloseableHttpResponse response_t =null;
		int cnt =0;
		boolean success=false;
		while (cnt<5 && success==false)
		{
			try {
				response_t = httpclient.execute(httpPost);
				success=true;
				ConvertingLogger.log(logTag, "回覆轉檔結果通知成功");
			} catch (IOException ex)
			{
				ioex=ex;
				ConvertingLogger.log(logTag, "回覆轉檔結果通知失敗"+ExceptionUtils.getStackTrace(ex));
			}
			cnt++;
		}
		
		if (success==false)
		{
			dumpTraceRoute(httpPost.getURI().getHost());
			throw ioex;
		}
		
		String responseStr = responseToStr(response_t);

		return responseStr;
	}
    
	private static void dumpTraceRoute(String address) throws UnknownHostException {
		String dumpResult = CommonUtil.traceRoute(address);
		ConvertingLogger.error("[reportToBookServer] dumpTraceRoute",dumpResult);
	}
	
	private static String responseToStr(CloseableHttpResponse response) throws UnsupportedOperationException, IOException {
		String responseStr = null;

		try {
			HttpEntity entity1 = response.getEntity();
			InputStream inp = entity1.getContent();
			Scanner scanner = new Scanner(inp, "UTF-8");
			scanner.useDelimiter("\\A");
			responseStr = scanner.next();

			scanner.close();
			inp.close();
			EntityUtils.consume(entity1);
		} finally {
			response.close();
		}

		return responseStr;
	}
    
    private String generateCoverThumbnail(MediaBookRequestData requestData) throws ServerException {

        String file_cover_url = "";
        
        try {
    	    
	        String Tag = "[generateCoverThumbnail]";
	        Long StartTime = System.currentTimeMillis();
	        ConvertingLogger.log(Tag, "Start generateCoverThumbnail()");
	        
	        String fileSystem = "/tmp/";
	        
	        final float MAX_WIDTH = 414;
	        final float MAX_HEIGHT = 554;
	        String JPG_TYPE = "jpg";
	        String JPG_MIME = "image/jpeg";
	        String folderPath = rootFolder.replace(originalFolder, "cover");
	        String downloadUrl = requestData.getEfile_cover_url();
	        
	        // Download file from URL
	        URL source = new URL(downloadUrl);
	        
	        String fileName = source.getFile().contains("/") ? 
	                source.getFile().substring(source.getFile().lastIndexOf("/") + 1) :
	                source.getFile();
	        
	        if(System.getProperty("os.name").contains("win")||System.getProperty("os.name").contains("Win")) {
	        	fileSystem = "C:/tmp/";
	        }
	        
//	        digipages.BookConvert.utility.CommonUtil.delDirs(fileSystem);
	        File downloadFile = new File(fileSystem + fileName);
	        FileUtils.copyURLToFile(source, downloadFile);
	        
	        // Read the source image
	        BufferedImage srcImage = ImageIO.read(downloadFile);
	        int srcHeight = srcImage.getHeight();
	        int srcWidth = srcImage.getWidth();
	        
	        // Infer the scaling factor to avoid stretching the image unnaturally
	        float scalingFactor = Math.min(MAX_WIDTH / srcWidth, MAX_HEIGHT / srcHeight);
	        int width = (int) (scalingFactor * srcWidth);
	        int height = (int) (scalingFactor * srcHeight);
	        
	        BufferedImage resizedImage = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
	        Graphics2D g = resizedImage.createGraphics();
	        
	        // Fill with white before applying semi-transparent (alpha) images
	        g.setPaint(Color.white);
	        g.fillRect(0, 0, width, height);
	        // Simple bilinear resize
	        // If you want higher quality algorithms, check this link:
	        // https://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
	        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	        g.drawImage(srcImage, 0, 0, width, height, null);
	        g.dispose();
	        
	        // Re-encode image to target format
	        ByteArrayOutputStream os = new ByteArrayOutputStream();
	        ImageIO.write(resizedImage, JPG_TYPE, os);
	        InputStream is = new ByteArrayInputStream(os.toByteArray());
	        // Set Content-Length and Content-Type
	        ObjectMetadata meta = new ObjectMetadata();
	        meta.setContentLength(os.size());
	        meta.setContentType(JPG_MIME);
	        
	        // Uploading to S3
	        s3Client.putObject(new PutObjectRequest(trialBucket, folderPath + "/" + fileName, is, meta).withCannedAcl(CannedAccessControlList.PublicRead));
	        
	        file_cover_url = folderPath.replace("cover/", "") + "/" + fileName;
	        
	        is.close();
	        os.close();
	        downloadFile.delete();
	        
	        ConvertingLogger.log(GenericLogLevel.Info,
	        		"End generateCoverThumbnail() spend " 
	        				+ (System.currentTimeMillis() - StartTime) + "ms"
	        		,ConvertingBook.class.getName());
    	} catch (java.net.ConnectException e)
    	{
    		ConvertingLogger.error(logTag, "Cannot download Cover Thumbnail from " + requestData.getEfile_cover_url());
    		throw new ServerException("id_err_315","Cannot download Cover Thumbnail from " + requestData.getEfile_cover_url());
    	}catch (Exception e)
    	{
    		logger.error("Unexcepted exception:" + e);
    		throw new ServerException("id_err_999",e.getMessage());
    	}
    	
    	return file_cover_url;
    }
    
 // This is a helper function to construct a list of ETags.
    private static List<PartETag> getETags(List<CopyPartResult> responses) {
        List<PartETag> etags = new ArrayList<PartETag>();
        for (CopyPartResult response : responses) {
            etags.add(new PartETag(response.getPartNumber(), response.getETag()));
        }
        return etags;
    }
}
