package digipages.BookConvert.BookMain;

import com.adobe.epubcheck.api.EPUBLocation;
import com.adobe.epubcheck.messages.Message;
import com.adobe.epubcheck.messages.Severity;
import com.adobe.epubcheck.util.PathUtil;


public class EpubCheckReport extends  com.adobe.epubcheck.util.DefaultReportImpl  {
    String errorMessage = "";
    String errorCodeList = "";
    
    public EpubCheckReport(String ePubName) {
        super(ePubName);
        
    }
    
    @Override
    public void message(Message message, EPUBLocation location, Object... args) {
        String text = formatMessage(message, location, args); 
        if(message.getSeverity().equals(Severity.ERROR) || message.getSeverity().equals(Severity.FATAL)) {
            errorMessage += text + "\r\n";
            errorCodeList += message.getID() + "\r\n";
        }        
        super.message(message, location, args);    
    }

    String formatMessage(Message message, EPUBLocation location, Object... args) {
        String fileName = (location.getPath() == null ? "" : "/" + location.getPath());
        fileName = PathUtil.removeWorkingDirectory(fileName);
        return String.format("%1$s(%2$s): %3$s%4$s(%5$s,%6$s): %7$s",
                message.getSeverity(),
                message.getID(),
                PathUtil.removeWorkingDirectory(this.getEpubFileName()),
                fileName,
                location.getLine(),
                location.getColumn(),
                fixMessage(args != null && args.length > 0 ? message.getMessage(args) : message.getMessage()));
    }
    
    String fixMessage(String message) {
        if (message == null) {
            return "";
        }
        
        return message.replaceAll("[\\s]+", " ");
    }
    
    public String getErrorMessage() {
        return this.errorMessage;
    }
    
    public String getErroCodeList() {
        return this.errorCodeList;
    }
}
