package digipages.BookConvert.BookMain;

public class MediabookReturntData {
    private Long chapter_no;
    private Boolean result;
    private String size;
    private String version;
    private String error_code;
    private String error_message;
    private Object chapter_info;
    private Object preview_info;

    public Long getChapter_no() {
        return chapter_no;
    }

    public void setChapter_no(Long chapter_no) {
        this.chapter_no = chapter_no;
    }
    public Boolean getResult() {
        return result;
    }

    public String getSize() {
        return size;
    }

    public String getVersion() {
        return version;
    }

    public String getError_code() {
        return error_code;
    }

    public String getError_message() {
        return error_message;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public Object getChapter_info() {
        return chapter_info;
    }

    public void setChapter_info(Object chapter_info) {
        this.chapter_info = chapter_info;
    }

    public Object getPreview_info() {
        return preview_info;
    }

    public void setPreview_info(Object preview_info) {
        this.preview_info = preview_info;
    }

}
