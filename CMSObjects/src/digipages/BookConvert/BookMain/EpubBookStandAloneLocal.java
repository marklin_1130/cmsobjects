package digipages.BookConvert.BookMain;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jsoup.Jsoup;
import org.red5.io.mp4.impl.MP4Reader;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.adobe.epubcheck.api.EpubCheck;

import digipages.BookConvert.BookMain.EPubUtil.CheckHtml;
import digipages.BookConvert.BookMain.EPubUtil.EPubBookData;
import digipages.BookConvert.FontDecrypt.FVFont;
import digipages.BookConvert.FontDecrypt.FontVerter;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.GenericLogRecord;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.Utility;
import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
import net.lingala.zip4j.core.ZipFile;

/**
 * extract from epubBook, this stand alone version can run on local env.
 * 
 * @author ericjou
 *
 */
public class EpubBookStandAloneLocal {
    private static final String PATHSEP = "/";
    private static final String VERSION2_0 = "2.0";
    private static final String THREE = "3";
    private static final String EMPTY_PAGE = "empty_page";
    private static final String MANIFEST = "manifest";
    private static final String EXTXHTML = ".xhtml";
    private static final String NO = "no";
    private boolean isEpubVersion3 = false;
    private static String fileSizeUpperBound;
    private String epubFolderPath;
    private File epubFolder;
    // root file = opf file
    private String rootFileName;
    private String rootFileDir;
    private String checkResultgPath;
    private LogToS3 convertingLogger;
    private static DPLogger logger=DPLogger.getLogger(EpubBookStandAloneLocal.class.getName());
    private static Set<String> mediaType = new HashSet<String>();
    private static Set<String> mediaTypeList = new HashSet<String>(Arrays.asList(new String[]{"webm","mkv","flv","vob","ogv","ogg","drc","gifv","mng","avi","mov","qt","wmv","yuv","rm","rmvb","asf","amv","mp4","m4p","m4v","mpg","mp2","mp3","mpeg","mpe","mpv","m2v","m4v","svi","3gp","mxf","3gp2","nsv","acc"}));
    //switch default is true.
    private Boolean epubCheckIsOpened = true;
    private Boolean consistencyIsOpened = true;

    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;
    private RequestData requestData;
    private String sourcePath;
    private Properties config;
    CheckResult consistencyResult = null;
    CheckResult epubCheckResult = null;
    private static String tocFileName ;

    private String bookUniqueIdentifier;

    public void setup(String sourcePath, RequestData requestData, Properties config, LogToS3 converterLogger) throws Exception {
        this.convertingLogger = converterLogger;
        this.sourcePath = sourcePath;
        dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        dbFactory.setValidating(false);
        dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        dBuilder = dbFactory.newDocumentBuilder();

        if (NO.equals(config.getProperty("EpubCheck_is_opened"))) {
            epubCheckIsOpened = false;
        }
        if (NO.equals(config.getProperty("ConsistencyCheck_is_opened"))) {
            consistencyIsOpened = false;
        }

        epubFolderPath = "R:/tmp/Epub_" + requestData.getBook_file_id() + PATHSEP;
        epubFolder = new File(epubFolderPath);
        epubFolder.mkdirs();
        fileSizeUpperBound = config.getProperty("fileSizeUpperBound", "300000");
        this.requestData = requestData;
        this.config = config;
    }

    public boolean convert() throws Exception {
        boolean result = false;
        String message = "";
        boolean inRetry = false;

        unZipEpub(sourcePath, epubFolderPath, convertingLogger);
        findRootFile(epubFolderPath);
        // check layout
        String opfPath = rootFileDir + rootFileName;
        String itemId = requestData.getItem();

        
        File rootFile = new File(opfPath);
        InputStream objectData = new FileInputStream(rootFile);
        Document doc = dBuilder.parse(objectData);
        Element root = doc.getDocumentElement();
        removeCommentAndEmptyText(root);
        objectData.close();

        Node manifest = doc.getElementsByTagNameNS("*", MANIFEST).item(0);
        NodeList manifestNodeList = manifest.getChildNodes();
        
        tocFileName = getTocPath(manifestNodeList);
        if(tocFileName.contains("/")) {
        	tocFileName= tocFileName.substring(tocFileName.indexOf("/")+1);
        }

        
        epubCheckResult = epubCheck(sourcePath);
        consistencyResult = checkConsistency(itemId, opfPath, epubFolder, convertingLogger, config, requestData);

        // Unique Identifier
        epubOPFUniqueIdentifierInfoRecu(epubFolder);
        boolean fontSucc = fontDecryptRecu(epubFolder, bookUniqueIdentifier);

        // debug code, force skip epubcheck.
        // epubCheckResult = new CheckResult();
        // epubCheckResult.setResult(true);

    		if (consistencyResult.getResult() && epubCheckResult.getResult() && fontSucc && !"reflowable".equalsIgnoreCase(requestData.getFormat())) {
    			result = true;
    			message = "";
    
    			File opfFile = new File(opfPath);
    			String epubVersion = checkEpubVersion(opfFile);
    
    			if (epubVersion.equals(VERSION2_0)) {
    				epub2ToEpub3();
    			}
    
    			if (requestData.getIsTrial() && !requestData.getPreview_type().equals(THREE)) {
    				consistencyResult = generateEpubTrial();
    				result = consistencyResult.getResult();
    				message = consistencyResult.getMessages().get(0).getMessage();
    			} else {
    				if (!requestData.getIsTrial()) {
    					getEpubCheckSum();
    				}
    			}
    		} else if (consistencyResult.getResult() && epubCheckResult.getResult() && "reflowable".equalsIgnoreCase(requestData.getFormat())){
    		    result = true;
                message = "";
    
                File opfFile = new File(opfPath);
                String epubVersion = checkEpubVersion(opfFile);
    
                if (epubVersion.equals(VERSION2_0)) {
                    epub2ToEpub3();
                }
                
                if(epubVersion.equalsIgnoreCase("3.0")) {
                	isEpubVersion3 = true;
                }
    
                if (requestData.getIsTrial() && !requestData.getPreview_type().equals(THREE)) {
                    consistencyResult = generateEpubTrial();
                    result = consistencyResult.getResult();
                    message = consistencyResult.getMessages().get(0).getMessage();
                } else {
                    if (!requestData.getIsTrial()) {
                        getEpubCheckSum();
                    }
                }
    		}else {
                result = false;
                message = "";
    		}

        logger.info("result: " + result);

        return result;
    }

    public static void unZipEpub(String srcEPubPath, String targetFolder, LogToS3 ConvertingLogger) throws Exception {
        String Tag = "[unZipEpub]";
        Long StartTime = System.currentTimeMillis();
        ConvertingLogger.log(Tag, "Start unZipEpub():" + targetFolder);

        // Save file
        // zip4j
        File bookFile = new File(srcEPubPath);
        ZipFile zipFile = new ZipFile(bookFile);
        zipFile.setFileNameCharset("GBK");
        if (!zipFile.isValidZipFile()) {
            throw new ServerException("id_err_315", "This file is not a valid ZipFile");
        }
        zipFile.extractAll(targetFolder);
        fixBomInDir(targetFolder);
        // java.util.zip
        /*
        File bookFile = new File(sourcePath);
        ZipInputStream zip = new ZipInputStream(new FileInputStream(bookFile));
        ZipEntry zipEntry = zip.getNextEntry();
        while (zipEntry != null){
            String filePath = epubFolderPath + zipEntry.getName();
            File temp = new File(filePath);
            
            if (filePath.lastIndexOf("/") == filePath.length() - 1) {
                temp.mkdirs();
            } else {
                File folder = new File(filePath.substring(0, filePath.lastIndexOf("/") + 1));
                if (!folder.exists()) {
                    folder.mkdirs();
                }
                 OutputStream out = new FileOutputStream(temp);
                 
                 byte[] buffer = new byte[1024];
                 int len;
                 while ((len = zip.read(buffer)) > 0) {
                     out.write(buffer, 0, len);
                 }
                 
                 removeBom(temp);
                 out.close();
            }
            
            zipEntry = zip.getNextEntry();
        }                            

        zip.closeEntry();
        zip.close();
         */

        ConvertingLogger.log(GenericLogLevel.Info,
        		"End unZipEpub() spend " + 
        (System.currentTimeMillis() - StartTime) + "ms", 
        EpubBookStandAloneLocal.class.getName());
    }

    private static void fixBomInDir(String targetFolder) {
        File f = new File(targetFolder);
        fixBomRecursive(f);
    }


	private static void fixBomRecursive(File f) {
		if (f.isFile() && CommonUtil.isFileXML(f))
		{
            try {
                removeBom(f);
			} catch (Exception e)
			{
                e.printStackTrace();
            }
            return;
        }
		if (f.isDirectory())
		{
			for (File child:f.listFiles())
			{
                fixBomRecursive(child);
            }
        }
    }


    // root file = opf file
    private void findRootFile(String epubFolderPath) throws ServerException {
        try {
            String containerPath = epubFolderPath + "META-INF/container.xml";
            InputStream objectData = new FileInputStream(containerPath);
            Document doc = dBuilder.parse(objectData);
            Node rootfile = doc.getElementsByTagName("rootfile").item(0);
            NamedNodeMap map = rootfile.getAttributes();
            Node full_path = map.getNamedItem("full-path");

            rootFileDir = epubFolderPath + full_path.getNodeValue().substring(0, full_path.getNodeValue().lastIndexOf(PATHSEP) + 1);
            rootFileName = full_path.getNodeValue().substring(full_path.getNodeValue().lastIndexOf(PATHSEP) + 1);

            SystemLog("[getRootFile]", "rootFileDir: " + rootFileDir);
            SystemLog("[getRootFile]", "rootFileName: " + rootFileName);

            objectData.close();
    	} catch (Exception e)
    	{
            throw new ServerException("id_err_315", "Cannot find RootFile: META-INF/container.xml");
        }
    }

    protected static void SystemLog(String Tag, String str) {
        logger.info(Tag + str + "\r\n");
    }

    private CheckResult epubCheck(String sourcePath) throws Exception {
        String tag = "[epubCheck]";
        Long startTime = System.currentTimeMillis();
        convertingLogger.log(tag, "Start epubCheck()");

        // EpubCheck
        File bookFile = new File(sourcePath);
        Boolean result = false;
        CheckResult checkResult = new CheckResult();

        String errorCode = "";
        String errorMessage;
        try {
            EpubCheckReport epubCheckReport = new EpubCheckReport(bookFile.getName());
            EpubCheck epubcheck = new EpubCheck(bookFile, epubCheckReport);
            result = epubcheck.validate();

            errorMessage = epubCheckReport.getErrorMessage();
            errorCode = epubCheckReport.getErroCodeList();
        } catch (Exception e) {
            logger.error("Unexcepted Exception ", e);
            result = false;
            String resultMessage = "EpubCheck Exception." + ((e.getMessage() != null) ? 
            		("\r\n" + ExceptionUtils.getStackTrace(e))  : "");

            checkResult.setResult(result);
            checkResult.addMessage(GenericLogLevel.Fatal, resultMessage, tag);

            return checkResult;
        }

        /* [+] Error Code Filter */
        String itemIdTop3 = requestData.getItem().substring(0, 3);
        SystemLog(tag, "ItemID top3: " + itemIdTop3);

        if (!requestData.getIsTrial() && isStrict(itemIdTop3)) {
            String publisherBid = (requestData.getPublisher_b_id() != null) ?
                     requestData.getPublisher_b_id() : ""; 
            String alwaysPassPublisher = "AlwaysPassList_Publisher.txt";
            Boolean alwaysPass = checkInAlwaysPass(publisherBid, alwaysPassPublisher);

            if (alwaysPass) {
                result = true;
            } else {
                String alwaysPassErrorCode = "AlwaysPassList_ErrorCode.txt";
                HashSet<String> alwaysPassListErrorCode = getEpubCheckList(alwaysPassErrorCode);
                HashMap<String, List<String>> publisherMap = new HashMap<>();

                String listPath = "EpubCheckList/PublisherList.txt";
                InputStream inputStream = getClass().getClassLoader().getResourceAsStream(listPath);
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                String publisher = "";
                ArrayList<String> list = new ArrayList<String>();
                Boolean startTag = false;
                while ((line = br.readLine()) != null) {
                    if (!line.isEmpty()) {
                        if (line.contains("publisher:")) {
                            if (startTag) {
                                publisherMap.put(publisher, list);
                            }

                            publisher = br.readLine();
                            startTag = false;
                            list = new ArrayList<String>();
                        } else if (line.contains("ID:")) {
                            startTag = true;
                        } else {
                            if (startTag && !line.isEmpty()) {
                                list.add(line);
                            }
                        }
                    }
                }
                publisherMap.put(publisher, list);
                br.close();
                inputStream.close();

                List<String> keyList = new ArrayList<String>();
                keyList.addAll(publisherMap.keySet());

                for (int i = 0; i < keyList.size(); i++) {
                    List<String> nameList = publisherMap.get(keyList.get(i));

                    for (int j = 0; j < nameList.size(); j++) {
                        if (publisherBid.contains(nameList.get(j))) {
                            publisherBid = keyList.get(i);
                        }
                    }
                }

                String publisherPass = "PassList_" + publisherBid + ".txt";
                SystemLog(tag, "Publisher Pass list:" + publisherPass);
                HashSet<String> publisherPassList = getEpubCheckList(publisherPass);
                publisherPassList.addAll(alwaysPassListErrorCode);

                Boolean tmpResult = true;
                String[] errorCodeList = errorCode.split("\r\n");
                String[] errorMessageList = errorMessage.split("\r\n");
                if (!errorCode.isEmpty()) {
                    for (int i = 0; i < errorCodeList.length; i++) {
                        if (publisherPassList.contains(errorCodeList[i])) {
                            // remove pass message
                            errorMessageList[i] = "";
                        } else {
                            tmpResult = false;
                        }
                    }
                }

                for (int i = 0; i < errorMessageList.length; i++) {
                    if (!errorMessageList[i].isEmpty()) {
                    	checkResult.addMessage(
                    					tmpResult? GenericLogLevel.Warn:GenericLogLevel.Error, 
                    							errorMessageList[i], 
                    					EpubBookStandAloneLocal.class.getName());
                        // resultMessage += errorMessageList[i] + "\r\n";
                    }
                }

                result = tmpResult;
            }
        } else { // trial || non E05/E06/G00 = still check BlockList
            String block = "BlockList.txt";
            HashSet<String> blockList = getEpubCheckList(block);
            String[] errorMessageList = errorMessage.split("\r\n");
            Boolean flag = true;

            for (int i = 0; i < errorMessageList.length; i++) {
                if (blockList.contains(errorMessageList[i])) {
                    flag = false;
                    // resultMessage += errorMessageList[i] + "\r\n";
                    SystemLog(tag, "Error code " + errorMessageList[i] + " contained in errorMessage.");
                }
            }

            result = flag;

            // Print all error message, not only in block list
            String alwaysPassErrorCode = "AlwaysPassList_ErrorCode.txt";
            HashSet<String> alwaysPassListErrorCode = getEpubCheckList(alwaysPassErrorCode);
            String[] errorCodeList = errorCode.split("\r\n");
            if (!errorCode.isEmpty()) {
                for (int i = 0; i < errorCodeList.length; i++) {
                    if (alwaysPassListErrorCode.contains(errorCodeList[i])) {
                        // remove pass message
                        errorMessageList[i] = "";
                    }
                }
            }

            for (int i = 0; i < errorMessageList.length; i++) {
                if (!errorMessageList[i].isEmpty()) {
                	checkResult.addMessage(
                					flag?GenericLogLevel.Error: GenericLogLevel.Warn, 
                							errorMessageList[i], 
                					EpubBookStandAloneLocal.class.getName());
//                    resultMessage += errorMessageList[i] + "\r\n"; 
                }
            }
        }
        /* [-] Error Code Filter */
        
        // check switch
        if(!epubCheckIsOpened) {
            result = true;
            SystemLog(tag, "EpubCheck is closed, EpubCheck will always pass.\r\n" 
            + checkResult.toString());
        } else {
            SystemLog(tag, result ? "EpubCheck pass.\r\n" : "EpubCheck fail.\r\n");
            // if(!result) {
            // resultMessage = "EpubCheck fail.\r\n" + resultMessage;
            // }
        }

        // CheckResult checkResult = new CheckResult();
        checkResult.setResult(result);
        // checkResult.addMessage(resultMessage);

        convertingLogger.log(GenericLogLevel.Info,
        		"result: " + result + "\r\n" +
                "End epubCheck() spend " + 
        				(System.currentTimeMillis() - startTime) + "ms",
        		EpubBookStandAloneLocal.class.getName());
        
        return checkResult;
    }

    private Boolean checkInAlwaysPass(String publisherBID, String alwaysPassPublisher) throws IOException {

        HashSet<String> passSet = loadPassList(alwaysPassPublisher);
        return passSet.contains(publisherBID);

    }

    private HashSet<String> loadPassList(String alwaysPassPublisher) throws IOException {
        final String listPath = "EpubCheckList/" + alwaysPassPublisher;
        HashSet<String> list = new HashSet<>();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(listPath);
        if (inputStream == null) {
            return list;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = br.readLine()) != null) {
            if (!line.isEmpty()) {
                list.add(line);
            }
        }

        br.close();
        inputStream.close();

        return list;
    }

    // E05, E06, G00
    private static boolean isStrict(String itemId) {
        String itemIdTop3 = itemId.substring(0, 3);
    	return "E05".equals(itemIdTop3) || 
    			"E06".equals(itemIdTop3) ||
    			"G00".equals(itemIdTop3);
    }

    /**
     * epub other check (beside EPubCheck)
     * @param itemId
     * @param opfPath
     * @param epubFolder
     * @param checkResultLogger
     * @param properties
     * @return
     * @throws Exception
     */
	public synchronized static  CheckResult checkConsistency(String itemId,String opfPath,
			File epubFolder, LogToS3 checkResultLogger, Properties properties, RequestData requestData) throws Exception {
        String tag = "[checkConsistency]";
        Boolean result = true;
        CheckResult checkResult = new CheckResult();

        Long startTime = System.currentTimeMillis();
        checkResultLogger.info(
        		"Start checkConsistency()", tag);

        try {
            File opfFile = new File(opfPath);

            result = result && checkFixedLayout(opfFile, properties, checkResult, requestData);

            checkCSS(opfFile.getParent() + "/", checkResultLogger, checkResult, requestData);
            // CMS踢退邏輯增加：
            // 1 epub裡各種css裡有 Margin 為負號的書籍，例：margin-top:-0.7em
            for (GenericLogRecord item : checkResult.getMessages()) {
                if (item.getLevel() == GenericLogLevel.Error) {
                    result = false;
                    break;
                }
            }

	        
            EPubBookData epubData = new EPubBookData(epubFolder);
            result = result && CheckHtml.checkEPubCover(epubData, checkResultLogger);
            String pageProgressionDirection = epubData.getPageDirectionFromOpf();

            
	        // Get all object
	        List<File> fileList = Utility.listAllFile(epubFolder);
	        int epubCheckMaxParagraph=Integer.parseInt(properties.getProperty("EpubCheckMaxParagraph","625"));
	        int maxBigParagraphEventCount=Integer.parseInt(properties.getProperty("MaxBigParagraphEventCount","20"));
	        ParagraphSizeChecker paraChecker = new ParagraphSizeChecker(epubCheckMaxParagraph,maxBigParagraphEventCount);
	        mediaType.clear();
	        for (File checkFile : fileList) {
	            String fileName = checkFile.getName();
	            result=result && allowFname(checkFile.getName(), checkResult, checkFile);
	            
	            
	            int checkExtensionResult = checkExtension(checkFile,checkResult);                

	            if(checkExtensionResult > 0) {
	                if(!fileName.contains("container.xml")&& checkExtensionResult != 4) {
	                    if(!checkFileEncoding(checkFile)) {
	                    	checkResult.addMessage(
	                    					GenericLogLevel.Error,
	                    					"checkFileEncoding: fail",
	                    					EpubBookStandAloneLocal.class.getName());
                            result = false;
                        }
                    }

                    if (checkExtensionResult == 2) { // Check opf file
                        ArrayList<String> missTagList = checkGeneralInformation(checkFile);
                        if (!missTagList.isEmpty()) {
                            result = false;
                            for (int i = 0; i < missTagList.size(); i++) {
	                        checkResult.addMessage(                    		
	            				GenericLogLevel.Error, 
	        					"checkGeneralInformation: fail " +  fileName + 
	        					" miss " + missTagList.get(i), 
	                            EpubBookStandAloneLocal.class.getName());
                            }
                        }

                    } else if (checkExtensionResult == 1) { // Check xhtml/html file
                        // Check whether file already add font information.
                        // checkFile = checkFontInfo(checkFile);

                        // Parsing full-path to get version
                        InputStream objectData = new FileInputStream(checkFile);
                        Document doc = dBuilder.parse(objectData);
                        if(doc.getDoctype()==null) {
                        	if(!checkFile.getName().contains(tocFileName)) {
                        	result = false;
	                        checkResult.addMessage(
		            			GenericLogLevel.Error, 
		            			"沒有宣告doc type" + checkFile.getName() + "Size:" + checkFile.length(),
		            			EpubBookStandAloneLocal.class.getName());
                        	}
                        }
                        objectData.close();
                    	
                        XHTMLChecker checker = new XHTMLChecker(checkFile);
                        if (!checker.checkFileSize(Long.valueOf(fileSizeUpperBound))) {
	                        result = false;
	                        checkResult.addMessage(
		            			GenericLogLevel.Error, 
		            			"checkFileSize: fail " + checkFile.getName() + "Size:" + checkFile.length(),
		            			EpubBookStandAloneLocal.class.getName());
                        }

                        try {
                            List<String> illegalList = checker.checkTag();
                            if (!illegalList.isEmpty()) {
                                result = false;
                                for (int i = 0; i < illegalList.size(); i++) {
		                        checkResult.addMessage(
		                        		GenericLogLevel.Error,
				                        "checkTag: fail In "+
				                        fileName + ", [" + illegalList.get(i) + 
		                        		"] is not included in tagList.xml",
                                            EpubBookStandAloneLocal.class.getName());
                                }
                            }

                            String bigParaCheckResult = paraChecker.updateFile(checkFile, pageProgressionDirection);
                            if (!bigParaCheckResult.isEmpty()) {
                                // bigParacheck doesn't affect result,
                                // result=false;
			                    checkResult.addMessage(
			                    		GenericLogLevel.Warn, 
			                    		bigParaCheckResult, EpubBookStandAloneLocal.class.getName());
                            }

		                    
	                    } catch (Exception e)
	                    {
	                    	 checkResult.addMessage(
	 	                    		GenericLogLevel.Warn, 
	 	                    		"File " + checkFile.getPath() + " format error. please check it.", EpubBookStandAloneLocal.class.getName());
                        }
                        // Add font information.
                        // addBOM(addFontInfo(checkFile));
                    }
	                
	                if(checkExtensionResult == 4) {
	                    result = false;
	                }
	                
                } else {
                    if (checkExtensionResult == -1) {
                        result = false;
	                    checkResult.addMessage(GenericLogLevel.Error, 
	                    		"This file is .htm file.", EpubBookStandAloneLocal.class.getName());                    
                    }
                }
            }
	        
//	        if (!mediaType.isEmpty()) {
//                for (String mediaString : mediaType) {
//                    if (!"mp3".equalsIgnoreCase(mediaString) && !"mp4".equalsIgnoreCase(mediaString) && !"wav".equalsIgnoreCase(mediaString)) {
//                        checkResult.addMessage(GenericLogLevel.Error, "影音檔案格式不符:" + mediaString, EpubBookStandAlone.class.getName());
//                        result = false;
//                    }
//                }
//            }

            // 20171110 電子書(#33#34)CMS轉檔邏輯修改,單一段落有>350字的書籍
	        if (paraChecker.getSize()>0)
	        {
	        	checkResult.addMessage(
	        			GenericLogLevel.Warn,
	        			"Big paragraph Summary, over "+epubCheckMaxParagraph+
	        			" chars, total count:"+paraChecker.getSize()+"",
                        EpubBookStandAloneLocal.class.getName());
                // result = false;
            }

            if ("1".equalsIgnoreCase(requestData.getPreview_type())) {
                try {
                    // 69
                    String percentage = requestData.getPreview_content().replace("%", "");
                    if (Integer.valueOf(percentage) <= 0) {
                        checkResult.addMessage(GenericLogLevel.Error, "Preview_content percentage <= 0 :" + percentage, EpubBookStandAloneLocal.class.getName());
                        result = false;
                    }
                } catch (Exception e) {
                    checkResult.addMessage(GenericLogLevel.Error, "Preview_content error check percentage value", EpubBookStandAloneLocal.class.getName());
                    result = false;
                }
            }

            // Only Check E05/E06/G00

            if (!isStrict(itemId)) {
                result = true;
            }

            boolean consistencyIsOpened = true;
            if (NO.equals(properties.getProperty("ConsistencyCheck_is_opened"))) {
                consistencyIsOpened = false;
            }
            // check switch
            if (!consistencyIsOpened) {
                result = true;
	            SystemLog(tag, "ConsistencyCheck is closed, ConsistencyCheck will always pass.\r\n"
	            + checkResult.toString());
            } else {
	            SystemLog(tag, "CheckConsistency " + (result ? "pass.\r\n" : "fail.\r\n") 
	            		+ checkResult.toString());
                // if(!result) {
                // resultMessage.append("CheckConsistency fail.\r\n").append(resultMessage);
                // }
            }
        }
        catch (SAXParseException | IndexOutOfBoundsException ex)
        {
            result = false;
            checkResult.addMessage(GenericLogLevel.Fatal, "Fail to load epub structure (opf), stop checking.", tag);
        }
        
        for (GenericLogRecord item : checkResult.getMessages()) {
            if (item.getLevel() == GenericLogLevel.Error) {
                result = false;
                break;
            }
        }

        checkResult.setResult(result);
        // checkResult.addMessage(resultMessage.toString());

        checkResultLogger.info("result: " + result + "\r\n" +
                "End checkConsistency(),  spend " + (System.currentTimeMillis() - startTime) + "ms",tag);

        return checkResult;
    }


    private static boolean checkFixedLayout(File opfFile, Properties properties, CheckResult checkResult, RequestData requestData) throws Exception {
        Boolean fileFixedLayout = !isReflowable(opfFile);
        String fileFormat = fileFixedLayout ? "fixedlayout" : "reflowable";
        String requestFormat = getFormatFromRequest(requestData.getFormat());
        if (!fileFormat.equals(requestFormat)) {
			GenericLogRecord rec = new GenericLogRecord(GenericLogLevel.Fatal, 
					"File format mismatch: file " + fileFormat + ", request: " + requestFormat,
					EpubBookStandAloneLocal.class.getName() );
            checkResult.addMessage(rec);
            return false;
        }

        return true;
    }

    private static String getFormatFromRequest(String format) {
        String ret = "reflowable";
        if (format.equals("0"))
            ret = "pdf";
        else if (format.equals("2"))
            ret = "fixedlayout";
		else
		{
            ret = format;
        }

        return ret;
    }

    /**
     * only allow "_/.\w\d" (0-9), a-z,A-Z,_, (but no space)
     * 
     * @param name
     * @param checkResult
     * @param checkFile
     * @return
     */
    public static boolean allowFname(String name, CheckResult checkResult, File checkFile) {
        boolean fileNameOk = name.matches("[\\w_\\d\\/\\.\\-\\\\]*");
        if (!fileNameOk) {
			checkResult.addMessage(GenericLogLevel.Error, "File Name Not Allowed:" + checkFile.getName() + ".",
					EpubBookStandAloneLocal.class.getName());

        }
        return fileNameOk;

    }

    /**
     * 
     * @param fileName
	 * @return
     		: Not contain in list:".htm" return -1
                                  else return 0
              Contain in list:".xhtml/html" return 1
                              ".opf" return 2
                              else return 3
	 * @throws Exception
	 */
    private static int checkExtension(File checkFile,CheckResult checkResult) throws Exception {
        int result = 0;
        String fileName = checkFile.getName();
        // Get extensionList
        ClassLoader classLoader = EpubBookStandAloneLocal.class.getClassLoader();
        InputStream listFile = classLoader.getResourceAsStream("extensionList.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(listFile, "UTF-8"));
        ArrayList<String> extensionList = new ArrayList<>();

        String buf;
        while ((buf = reader.readLine()) != null) {
            extensionList.add(buf);
        }
        reader.close();

        
        String extension;
        if (fileName.contains(".")) {
            extension = fileName.substring(fileName.lastIndexOf("."));
        } else {
            return result;
        }

        Boolean inList = false;
        for (int i = 0; i < extensionList.size(); i++) {
            if (extensionList.get(i).equals(extension)) {
                inList = true;
            }
        }

        if (inList) {
            if (extension.equals(EXTXHTML) || extension.equals(".html")) {
                result = 1;
            } else if (extension.equals(".opf")) {
                result = 2;
            } else {
                result = 3;
            }
        } else {
            if (extension.equals(".htm")) {
                result = -1;
            } else {
                result = 0;
            }
        }
        
        if(mediaTypeList.contains(extension.substring(1))) {
            mediaType.add(extension.substring(1));
            checkResult.addMessage(
                    GenericLogLevel.Info,
                    "media file found:"+fileName,
                    EpubBookStandAloneLocal.class.getName());
            
            if (!"mp3".equalsIgnoreCase(extension.substring(1)) && !"mp4".equalsIgnoreCase(extension.substring(1)) && !"wav".equalsIgnoreCase(extension.substring(1))) {
                result = 4;
                checkResult.addMessage(GenericLogLevel.Error, "影音檔案格式不支援:" + fileName, EpubBookStandAloneLocal.class.getName());
            }else {
            	
            	  if(extension.equalsIgnoreCase(".js")) {
                      checkResult.addMessage(
                              GenericLogLevel.Error,
                              "影音檔 js file found:"+fileName,
                              EpubBookStandAloneLocal.class.getName());
                  }
                
                if(CheckHtml.isScriptfound) {
                	 checkResult.addMessage(
                             GenericLogLevel.Error,
                             "影音檔 js script found:",
                             EpubBookStandAloneLocal.class.getName());
                }
                
                // Get length of file in bytes
                long fileSizeInBytes = checkFile.length();
                // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                long fileSizeInKB = fileSizeInBytes / 1024;
//                 Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                long fileSizeInMB = fileSizeInKB / 1024;
                
                if(83886080 < checkFile.length()) {
                    checkResult.addMessage(GenericLogLevel.Warn, "影音檔案超過80MB: " + fileName + ",size:"+ fileSizeInMB, EpubBookStandAloneLocal.class.getName());
                }
                
                if(209715200 < checkFile.length()) {
                    result = 4;
                    checkResult.addMessage(GenericLogLevel.Error, "影音檔案超過200MB: " + fileName + ",size:"+ fileSizeInMB, EpubBookStandAloneLocal.class.getName());
                }
                
                if("mp4".equalsIgnoreCase(extension.substring(1)) ) {
                	MP4Reader aMP4Reader = new MP4Reader (checkFile);
                	String codec = aMP4Reader.getVideoCodecId();
                	if(!codec.contains("avc")) {
                		result = 4;
                		checkResult.addMessage(GenericLogLevel.Error, "MP4檔案編碼不支援: " + fileName + ",codec:"+ codec, EpubBookStandAloneLocal.class.getName());
                	}
                	
                }
            }
        }
        
        if(fileName.contains(".js")) {
            checkResult.addMessage(
                    GenericLogLevel.Warn,
                    "js file found:"+fileName,
                    EpubBookStandAloneLocal.class.getName());
        }
        
        return result;
    }

    private static Boolean checkFileEncoding(File checkFile) throws Exception {
        Boolean result = false;
        InputStream objectData = new FileInputStream(checkFile);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objectData));

        String temp;
        while ((temp = bufferedReader.readLine()) != null) {
            if (temp.contains("<?xml")) {
                String[] split = temp.split("'|\"");
                String utf8 = "UTF-8";
                String encodingType;

                for (int i = 0; i < split.length; i++) {
                    if (split[i].contains("encoding=")) {
                        encodingType = split[i + 1];

                        if (utf8.equals(encodingType.toUpperCase())) {
                            result = true;
                            break;
                        }
                    }
                }
            }else
            {
                // default non-xml file skip utf-8 check
                result = true;
            }
        }
        bufferedReader.close();
        objectData.close();

        return result;
    }

    private static ArrayList<String> checkGeneralInformation(File checkFile) throws Exception {
        // Get extensionList
        ClassLoader classLoader = EpubBookStandAloneLocal.class.getClassLoader();
        InputStream listFile = classLoader.getResourceAsStream("generalInfo.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(listFile));
        ArrayList<String> generalList = new ArrayList<>();
        ArrayList<String> illegalList = new ArrayList<>();

        String buf;
        while ((buf = reader.readLine()) != null) {
            generalList.add(buf);
        }
        reader.close();
        listFile.close();

        InputStream objectData = new FileInputStream(checkFile);
        Document doc = dBuilder.parse(objectData);

        for (int i = 0; i < generalList.size(); i++) {
            NodeList tempList = doc.getElementsByTagNameNS("*", generalList.get(i));

            if (tempList.getLength() == 0) {
                illegalList.add(generalList.get(i));
            }
        }

        objectData.close();

        return illegalList;
    }

    /**
     * anything fail to detect => just return true (other check will report detail error)
     * @param checkOpfFile
     * @return isReflowable
     * @throws Exception
     */
    private static Boolean isReflowable(File checkOpfFile) throws Exception {

        Boolean result = true;
        InputStream objectData = new FileInputStream(checkOpfFile);
        Document doc = dBuilder.parse(objectData);
        NodeList metaList = doc.getElementsByTagNameNS("*", "meta");

        for (int i = 0; i < metaList.getLength(); i++) {
            Node meta = metaList.item(i);
            NamedNodeMap map = meta.getAttributes();
            Node propertyNode = map.getNamedItem("property");

            if (propertyNode != null) {
                String property = propertyNode.getNodeValue();

                if (property.contains("layout")) {
                    Node FC = meta.getFirstChild();

                    if (FC.getNodeValue().contains("pre-paginated")) {
                        result = false;
                    }
                }
            }
        }

        objectData.close();
        return result;
    }

    private String checkEpubVersion(File checkFile) throws Exception {
        String version = "";

        // Parsing full-path to get version
        InputStream objectData = new FileInputStream(checkFile);
        Document doc = dBuilder.parse(objectData);
        Node pac = doc.getElementsByTagNameNS("*", "package").item(0);
        NamedNodeMap map = pac.getAttributes();
        Node ver = map.getNamedItem("version");
        version = ver.getNodeValue();

        objectData.close();

        return version;
    }

    private void epub2ToEpub3() throws Exception {
        // Replace version 2.0 to 3.0 in package
        File rootFile = new File(rootFileDir + rootFileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(rootFile), "UTF-8"));
        ArrayList<String> content = new ArrayList<String>();

        String buf;
        while ((buf = reader.readLine()) != null) {
            if (buf.contains("<package ")) {
                buf = buf.replace("\"2.0\"", "\"3.0\"");
            }
            content.add(buf);
        }

        reader.close();
        rootFile.delete();

        File new_rootFile = new File(rootFileDir + rootFileName);
        BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new_rootFile), "UTF-8"));
        for (int i = 0; i < content.size(); i++) {
            writter.write(content.get(i) + "\r\n");
        }

        writter.close();
    }

    private CheckResult generateEpubTrial() throws Exception {
        String Tag = "[generateEpubTrial]";
        CheckResult checkResult;
        Long StartTime = System.currentTimeMillis();
        convertingLogger.log(Tag, "Start generateEpubTrial()");

        switch (requestData.getPreview_type()) {
        case "1":
            String percentage = requestData.getPreview_content().replace("%", "");
            try {
                cutByPercentage(percentage,requestData.getFormat());
            } catch (Exception e) {
                String errMsg = "";
                if(e instanceof ServerException) {
                    errMsg = ((ServerException)e).getError_message();
                }else {
                    errMsg = e.getMessage();
                }
                checkResult = new CheckResult();
                checkResult.setResult(false);
                checkResult.addMessage(GenericLogLevel.Error, errMsg, EpubBookStandAloneLocal.class.getName());
                consistencyResult.addMessage(GenericLogLevel.Error, errMsg, EpubBookStandAloneLocal.class.getName());
                return checkResult;
            }
            break;
        case "2":
            cutBySpecifyPages(requestData.getPreview_content());
            break;
        }

        checkResult = checkTrialSize();

        convertingLogger.log(GenericLogLevel.Info,
        		"End generateEpubTrial() spend " + 
        				(System.currentTimeMillis() - StartTime) + "ms",
        		EpubBookStandAloneLocal.class.getName());

        return checkResult;
    }

    /* If trial book with no-page or only has cover page, return false,
     * else, return true.
     */
    private CheckResult checkTrialSize() throws Exception {
        CheckResult checkResult = new CheckResult();

        File rootFile = new File(rootFileDir + rootFileName);
        InputStream objectData = new FileInputStream(rootFile);
        Document doc = dBuilder.parse(objectData);
        Node spine = doc.getElementsByTagNameNS("*", "spine").item(0);
        NodeList spineNodeList = spine.getChildNodes();
        int spineCount = 0;
        Boolean hasCover = false;
        String coverId = "";
        Boolean hasEmptyPage = false;
        String emptyPageId = EMPTY_PAGE;

        // Get cover.xhtml id
        Node manifest = doc.getElementsByTagNameNS("*", MANIFEST).item(0);
        NodeList manifestNodeList = manifest.getChildNodes();

        for (int i = 0; i < manifestNodeList.getLength(); i++) {
            Node node = manifestNodeList.item(i);
            NamedNodeMap map = node.getAttributes();

            
            if (map != null) {
                Node id = map.getNamedItem("id");

                if (id != null && id.getNodeValue().compareToIgnoreCase("cover") == 0) {
                    coverId = id.getNodeValue();
                }
            }
        }

        // Count the number of file in spine and check cover is exist.
        for (int i = 0; i < spineNodeList.getLength(); i++) {
            Node node = spineNodeList.item(i);
            if (!node.getNodeName().equals("#text")) {
                spineCount++;
                NamedNodeMap map = node.getAttributes();

                if (map != null) {
                    Node idref = map.getNamedItem("idref");

                    if (idref != null && !coverId.isEmpty() && idref.getNodeValue().equals(coverId)) {
                        hasCover = true;
                    } else if (idref != null && idref.getNodeValue().equals(emptyPageId)) {
                        hasEmptyPage = true;
                    }
                }
            }
        }

        if (spineCount == 0) {
            checkResult.setResult(false);
            checkResult.addMessage(
            		GenericLogLevel.Error,
            				"Generate trial book failed. The trial book without any page.",
            				EpubBookStandAloneLocal.class.getName());
        } else if ((spineCount == 1 && (hasCover || hasEmptyPage)) ||
        		(spineCount == 2 && (hasCover && hasEmptyPage)))
        {
            throw new ServerException("id_err_305", "Generate trial book failed. The trial book only has cover page.");
            // checkResult.setResult(false);
            // checkResult.addMessage(
            // GenericLogLevel.Error,
            // "Generate trial book failed. The trial book only has cover page.",
            // EpubBookStandAlone.class.getName());
        } else {
            checkResult.setResult(true);
            // success does not need to return fine. message
            checkResult.addMessage(
            		GenericLogLevel.Info,
            				"",
            				EpubBookStandAloneLocal.class.getName());

        }

        objectData.close();

        return checkResult;
    }

    private void cutByPercentage(String Ranges, String format) throws Exception {
        long totalSize = 0;
        long TRIAL_MAX_SIZE = 0;

        File rootFile = new File(rootFileDir + rootFileName);
        InputStream objectData = new FileInputStream(rootFile);
        Document doc = dBuilder.parse(objectData);
        Element root = doc.getDocumentElement();
        removeCommentAndEmptyText(root);
        objectData.close();

        Node manifest = doc.getElementsByTagNameNS("*", MANIFEST).item(0);
        NodeList manifestNodeList = manifest.getChildNodes();
        Node spine = doc.getElementsByTagNameNS("*", "spine").item(0);
        NodeList spineNodeList = spine.getChildNodes();

        LinkedHashMap<String, String> manifestMap = new LinkedHashMap<String, String>();

        String coverId = "";
        Node cover_manifest = null;
        Node cover_spine = null;
        String tocId = "";
        
        for (int i = 0; i < manifestNodeList.getLength(); i++) {
            Node child = manifestNodeList.item(i);
            NamedNodeMap map = child.getAttributes();
            Node id = map.getNamedItem("id");
            Node href = map.getNamedItem("href");
            if(null!=map.getNamedItem("properties")) {
                if(StringUtils.containsIgnoreCase("nav", map.getNamedItem("properties").getNodeValue())) {
                    tocId = id.getNodeValue();
                }
            }

            // Remove cover from manifest.
            Boolean isCover = false;
            if (id != null && id.getNodeValue().compareToIgnoreCase("cover") == 0) {
                coverId = id.getNodeValue();
                isCover = true;

                cover_manifest = child.cloneNode(true);
                manifest.removeChild(child);
                i = i - 1; // manifest.removeChild(child) will let manifestNodeList less 1.
            }

            if (!isCover) {
                manifestMap.put(id.getNodeValue(), href.getNodeValue());
            }
        }

        // Remove cover from spine. (for calculate trial book size without cover & emptypage)
        for (int i = 0; i < spineNodeList.getLength(); i++) {
            Node child = spineNodeList.item(i);
            NamedNodeMap map = child.getAttributes();
            Node idref = map.getNamedItem("idref");

            if (idref != null && 
                    !coverId.isEmpty() && idref.getNodeValue().equals(coverId)) {
                cover_spine = child.cloneNode(true);
                spine.removeChild(child);
            }
        }

        
        //判斷是否有截斷文字資料
        boolean reflowableTrailCut = false;
        //判斷該頁面只有圖片
        boolean targetHtmlImgOnly = false;
        //判斷是不是沒有<P> tag
        boolean targetTagPNotFound = false;
        
        boolean allTagPNotFound = false;
        
        boolean usePreviousSpin = false;
        
        boolean isScreenXhtmlWasNav=false;
        
        EPubBookData epubData = new EPubBookData(epubFolder);
        if ("reflowable".equalsIgnoreCase(format)) {

            String screenText = requestData.getScreenText();
            String screenXhtml = requestData.getScreenXhtml();
            String screenXhtmlPrevious = requestData.getScreenXhtmlPrevious();
            SystemLog("[cutByPercentage] " + format,
                    " screenText:" + screenText + ", screenXhtml:" + screenXhtml + ",TotalPage:" + requestData.getTotalPage() + ",TrialPage:" + requestData.getTrialPage());
            convertingLogger.log(GenericLogLevel.Info, "[cutByPercentage] " + format + " screenText:" + screenText + ", screenXhtml:" + screenXhtml + ",TotalPage:" + requestData.getTotalPage()
                    + ",TrialPage:" + requestData.getTrialPage(), EpubBookStandAloneLocal.class.getName());

            // 根據開書結果尋找試閱章節，取得檔案名稱
            String targetScreenXhtmlFileName = "";
            String targetScreenXhtmlPreviousFileName = "";
                for (int i = 0; i < manifestNodeList.getLength(); i++) {
                    Node child = manifestNodeList.item(i);
                    NamedNodeMap map = child.getAttributes();
                    Node id = map.getNamedItem("id");
                    Node href = map.getNamedItem("href");
    
                    if (id != null && id.getNodeValue().equalsIgnoreCase(screenXhtml)) {
                        String targetScreenXhtmlHref = href.getNodeValue();
                        if (targetScreenXhtmlHref.indexOf("/") > 0) {
                            targetScreenXhtmlFileName = targetScreenXhtmlHref.substring(targetScreenXhtmlHref.indexOf("/") + 1, targetScreenXhtmlHref.length());
                            logger.info("試閱目標章節已找到:" + targetScreenXhtmlFileName);
                            convertingLogger.log(GenericLogLevel.Info, "試閱目標章節已找到:" + targetScreenXhtmlFileName, EpubBookStandAloneLocal.class.getName());
                        } else {
                            targetScreenXhtmlFileName = targetScreenXhtmlHref;
                            logger.info("試閱目標章節已找到:" + targetScreenXhtmlFileName);
                            convertingLogger.log(GenericLogLevel.Info, "試閱目標章節已找到:" + targetScreenXhtmlFileName, EpubBookStandAloneLocal.class.getName());
                        }
                    }
                    
                    if(StringUtils.isNotBlank(screenXhtmlPrevious)) {
                        if (id != null && id.getNodeValue().equalsIgnoreCase(screenXhtmlPrevious)) {
                            String targetScreenXhtmlHref = href.getNodeValue();
                            if (targetScreenXhtmlHref.indexOf("/") > 0) {
                                targetScreenXhtmlPreviousFileName = targetScreenXhtmlHref.substring(targetScreenXhtmlHref.indexOf("/") + 1, targetScreenXhtmlHref.length());
                                logger.info("試閱目標章節已找到:" + targetScreenXhtmlFileName);
                                convertingLogger.log(GenericLogLevel.Info, "試閱目標章節已找到:" + targetScreenXhtmlFileName, EpubBookStandAloneLocal.class.getName());
                            } else {
                                targetScreenXhtmlPreviousFileName = targetScreenXhtmlHref;
                                logger.info("試閱目標章節已找到:" + targetScreenXhtmlFileName);
                                convertingLogger.log(GenericLogLevel.Info, "試閱目標章節已找到:" + targetScreenXhtmlFileName, EpubBookStandAloneLocal.class.getName());
                            }
                        }
                    }
                }
                
            // 尋找檔案名稱，截斷文字
            List<File> fileList = Utility.listAllFile(new File(rootFileDir));
            logger.info("試閱目標擷取尋找文字:" + screenText);
            if(tocId.equalsIgnoreCase((screenXhtml))) {
            	isScreenXhtmlWasNav = true;
            	logger.info("試閱目標章節是nav/toc:" + tocId);
            	convertingLogger.log(GenericLogLevel.Info,"試閱目標章節是nav/toc: " + tocId, EpubBookStandAloneLocal.class.getName());
            }
            convertingLogger.log(GenericLogLevel.Info, "試閱目標擷取尋找文字:" + screenText, EpubBookStandAloneLocal.class.getName());
            dBuilder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    if (systemId.contains("dtd")) {
                        return new InputSource(new StringReader(""));
                    } else {
                        return null;
                    }
                }
            });

            List<Node> removeNodeList = new ArrayList<Node>();
            screenText = StringUtils.trimToEmpty(screenText);
            int totalContentLenght = 0;
            int cutContentLenght = 0;
            int allElementPCount = 0;
            for (int i = 0; i < spineNodeList.getLength(); i++) {
                StringBuilder combineText = new StringBuilder();
                Node child = spineNodeList.item(i);
                String path = rootFileDir;
                NamedNodeMap map = child.getAttributes();
                Node idref = map.getNamedItem("idref");
                String spinId = idref.getNodeValue();
                path = path + manifestMap.get(spinId);
                logger.info("尋找章節:" + spinId+",檔案位置:"+path);
                int elementPCount = 0;
                int elementImgCount = 0;
                File file = new File(path);
                InputStream targetObjectData = new FileInputStream(file);
                Document targetdoc = dBuilder.parse(targetObjectData);
                String docPublicId="";
                String docSystemId="";
                if(targetdoc.getDoctype()!=null) {
                	docPublicId =  StringUtils.isBlank(targetdoc.getDoctype().getPublicId()) ? "" : targetdoc.getDoctype().getPublicId();
                	docSystemId = StringUtils.isBlank(targetdoc.getDoctype().getSystemId()) ? "" : targetdoc.getDoctype().getSystemId();
                }
                Element rootEle = doc.getDocumentElement();
                NodeList allNodeList = targetdoc.getElementsByTagName("*");
                NodeList bodyNodeList = targetdoc.getElementsByTagName("body").item(0).getChildNodes();
                NodeList allPNodeList = targetdoc.getElementsByTagName("p");
                boolean findtargetElement = false;
                for (int j = 0; j < allNodeList.getLength(); j++) {
                    Node node = allNodeList.item(j);

                    if ("p".equalsIgnoreCase(node.getNodeName())) {
                        allElementPCount++;
                    }
                    if (!node.getTextContent().isEmpty()) {
                        combineText.append(StringUtils.normalizeSpace(node.getTextContent()));
                    }

                }

                totalContentLenght += combineText.length();

                if (spinId.equalsIgnoreCase(screenXhtml)) {
                    // #11 有可能開書無法擷取到文字或文字小於18，可能該頁只有圖片，則不進行截斷內容處理
                    if (StringUtils.isNotBlank(StringUtils.normalizeSpace(screenText)) && combineText.length() > 0 && !isScreenXhtmlWasNav) {
                        // 取得該檔案所有文字 再進行比對
                        int foundIndex = combineText.indexOf(StringUtils.normalizeSpace(screenText)) + 18;
                        cutContentLenght = (totalContentLenght - combineText.length()) + foundIndex;
                        combineText.setLength(0);
                        NodeList findTextNodeList = allPNodeList.getLength() <= 0 ? bodyNodeList:allPNodeList;
                        
                        for (int k = 0; k < findTextNodeList.getLength(); k++) {
                            Node node = findTextNodeList.item(k);
                            if (!findtargetElement) {
                                    if(StringUtils.isNotBlank(node.getTextContent())) {
                                    	 if ("p".equalsIgnoreCase(node.getNodeName())) {
                                    		 elementPCount++;
                                         }
                                        logger.info("試閱目標擷取文字:" + node.getTextContent());
                                        int beforeIndex = combineText.length();
                                        combineText.append(StringUtils.normalizeSpace(node.getTextContent()));
                                        System.out.println(combineText.length() );
                                        System.out.println("targetLength:"+foundIndex );
                                        if (combineText.length() >= foundIndex) {
                                        	System.out.println("find text in node:"+node.getNodeName());
                                            node.setTextContent(node.getTextContent().substring(0, foundIndex - beforeIndex) + "⋯⋯");
                                            findtargetElement = true;
                                            reflowableTrailCut = true;
                                            logger.info("試閱目標擷取文字已找到:" + node.getTextContent());
                                            convertingLogger.log(GenericLogLevel.Info, "試閱目標擷取文字已找到:" + node.getTextContent(), EpubBookStandAloneLocal.class.getName());
                                        }
                                    }
                                
                                if ("img".equalsIgnoreCase(node.getNodeName()) || "svg".equalsIgnoreCase(node.getNodeName()) || "image".equalsIgnoreCase(node.getNodeName())) {
                                    elementImgCount++;
                                }
                                
                            } else {
                                try {
                                    // 移除非試閱內容
                                	
                                	if(!node.getNodeName().equalsIgnoreCase("body")) {
                                		removeNodeList.add(node);
                                	}
                                    
                                } catch (Exception e) {
                                }
                            }
                        }

                        for (Node node : removeNodeList) {
                            node.getParentNode().removeChild(node);
                        }
                        targetObjectData.close();
                        if(StringUtils.isBlank(docPublicId) && StringUtils.isBlank(docSystemId)) {
                        	writeXmlWithDocType(targetdoc, file.getAbsolutePath() , docPublicId , "about:legacy-compat");
                        }else {
                        	writeXmlWithDocType(targetdoc, file.getAbsolutePath() , docPublicId , docSystemId);
                        }
                    }else {
                        for (int k = 0; k < allNodeList.getLength(); k++) {
                            Node node = allNodeList.item(k);
                            if ("p".equalsIgnoreCase(node.getNodeName())) {
                                if(StringUtils.isNotBlank(node.getTextContent())) {
                                	elementPCount++;
                                }
                            }
                            
                            if ("img".equalsIgnoreCase(node.getNodeName()) || "svg".equalsIgnoreCase(node.getNodeName()) || "image".equalsIgnoreCase(node.getNodeName())) {
                                elementImgCount++;
                            }
                        }
                    }
                    targetTagPNotFound = (elementPCount == 0);
                    targetHtmlImgOnly = (targetTagPNotFound && elementImgCount > 0);
                }
            }
            
            allTagPNotFound = (allElementPCount == 0);
            
            convertingLogger.log(GenericLogLevel.Info, "總文字長度:" + totalContentLenght, EpubBookStandAloneLocal.class.getName());
            convertingLogger.log(GenericLogLevel.Info, "試閱文字長度:" + cutContentLenght, EpubBookStandAloneLocal.class.getName());
            if (!targetHtmlImgOnly && StringUtils.isNotBlank(targetScreenXhtmlPreviousFileName) && StringUtils.isBlank(screenText)) {
                usePreviousSpin = true;
            }
            if (targetHtmlImgOnly) {
                convertingLogger.log(GenericLogLevel.Info, "判斷只有圖片使用整頁", EpubBookStandAloneLocal.class.getName());
            }
            
        } else {
            // #69 試閱轉檔邏輯變更 fixedlayout 改以總html 計數PCT
            logger.info("試閱比例%:" + Integer.valueOf(Ranges));
            totalSize = epubData.getTotalHtmlSize();
            if(totalSize<=0) {
                throw new Exception("偵測總頁數失敗");
            }
            logger.info("有效頁數:" + totalSize);
            BigDecimal totalHtmlBigDecimal = new BigDecimal(totalSize);
            totalHtmlBigDecimal.multiply(new BigDecimal(Integer.valueOf(Ranges)));
            BigDecimal trialsize = totalHtmlBigDecimal.multiply(new BigDecimal(Integer.valueOf(Ranges))).divide(new BigDecimal(100));
            logger.info("試閱比例計算結果:" + trialsize);
            if (trialsize.doubleValue() < 1D) {
                TRIAL_MAX_SIZE = 1;
                logger.info("試閱比例計算結果進位:" + TRIAL_MAX_SIZE);
            } else {
                TRIAL_MAX_SIZE = trialsize.intValue();
                logger.info("試閱比例計算結果整數:" + TRIAL_MAX_SIZE);
            }
            // 沒偵測到封面+1 ,第一頁可能為封面
            if (cover_manifest == null) {
                TRIAL_MAX_SIZE = TRIAL_MAX_SIZE + 1;
                logger.info("沒封面:" + (cover_manifest == null));
            } else {
                logger.info("有封面:" + (cover_manifest == null));
            }

            logger.info("試閱頁數:" + TRIAL_MAX_SIZE);
            SystemLog("[cutByPercentage] " + format, " TRIAL_MAX_SIZE by html count:" + TRIAL_MAX_SIZE + ", TOTAL_SIZE:" + totalSize);
        }

        /* [+] get spineArray & spineRemoveArray */
        int trialSize = 0;
        Boolean isFull = false;
        ArrayList<Node> spineArray = new ArrayList<Node>();
        ArrayList<Node> spineRemoveArray = new ArrayList<Node>();

        boolean reflowableCutAfterSpin = false;
        for (int i = 0; i < spineNodeList.getLength(); i++) {
            Node child = spineNodeList.item(i);
            String path = rootFileDir;
            NamedNodeMap map = child.getAttributes();
            Node idref = map.getNamedItem("idref");
            String spinId = idref.getNodeValue();
            path = path + manifestMap.get(spinId);
            objectData = new FileInputStream(path);

            if ("reflowable".equalsIgnoreCase(format)) {
                
                // #11 TOC 內容不能被移除
                if(StringUtils.isNoneBlank(tocId) && StringUtils.containsIgnoreCase(child.getAttributes().getNamedItem("idref").getNodeValue(), tocId)) {
                    spineArray.add(child);
                    logger.info("加入試閱TOC: " + child.getAttributes().getNamedItem("idref"));
                    convertingLogger.log(GenericLogLevel.Info,"加入試閱TOC: " + child.getAttributes().getNamedItem("idref"), EpubBookStandAloneLocal.class.getName());
                    if(tocId.equalsIgnoreCase(requestData.getScreenXhtml())) {
                    	reflowableCutAfterSpin = true;
                    }
                    if(!usePreviousSpin && spinId.equalsIgnoreCase(requestData.getScreenXhtml())) {
                    	reflowableCutAfterSpin = true;
                    }
                    if(usePreviousSpin && spinId.equalsIgnoreCase(requestData.getScreenXhtmlPrevious())) {
                    	reflowableCutAfterSpin = true;
                    }
                    continue;
                }
                
                if (usePreviousSpin) {
                    if (!reflowableCutAfterSpin) {
                        spineArray.add(child);
                        logger.info("加入試閱內容章節: " + spinId + ":" + requestData.getScreenXhtmlPrevious());
                        if (spinId.equalsIgnoreCase(requestData.getScreenXhtmlPrevious())) {
                            reflowableCutAfterSpin = true;
                            logger.info("加入試閱內容章節: " + child.getAttributes().getNamedItem("idref"));
                            convertingLogger.log(GenericLogLevel.Info, "加入試閱內容章節: " + child.getAttributes().getNamedItem("idref"), EpubBookStandAloneLocal.class.getName());
                        }
                    } else {
                        spineRemoveArray.add(child);
                        logger.info("移除試閱內容章節: " + child.getAttributes().getNamedItem("idref"));
                        convertingLogger.log(GenericLogLevel.Info, "移除試閱內容章節: " + child.getAttributes().getNamedItem("idref"), EpubBookStandAloneLocal.class.getName());
                    }
                } else {
                    if (!reflowableCutAfterSpin) {
                        spineArray.add(child);
                        logger.info("加入試閱內容章節: " + spinId + ":" + requestData.getScreenXhtml());
                        if (spinId.equalsIgnoreCase(requestData.getScreenXhtml())) {
                            reflowableCutAfterSpin = true;
                            logger.info("加入試閱內容章節: " + child.getAttributes().getNamedItem("idref"));
                            convertingLogger.log(GenericLogLevel.Info, "加入試閱內容章節: " + child.getAttributes().getNamedItem("idref"), EpubBookStandAloneLocal.class.getName());
                        }
                    } else {
                        spineRemoveArray.add(child);
                        logger.info("移除試閱內容章節: " + child.getAttributes().getNamedItem("idref"));
                        convertingLogger.log(GenericLogLevel.Info, "移除試閱內容章節: " + child.getAttributes().getNamedItem("idref"), EpubBookStandAloneLocal.class.getName());
                    }
                }

            } else {
                // #69 試閱轉檔邏輯變更 fixedlayout 改以總html 計數PCT
                if (!isFull && i > TRIAL_MAX_SIZE) {
                    isFull = true;
                    spineRemoveArray.add(child);
                } else if (!isFull && i < TRIAL_MAX_SIZE) {

                    NamedNodeMap trialContentChildMap = child.getAttributes();
                    Node idrefTrialContent = trialContentChildMap.getNamedItem("idref");
                    Node linearTrialContent = trialContentChildMap.getNamedItem("linear");
                    File targetFile = epubData.getManifest().getFileById(idrefTrialContent.getNodeValue());
                    org.jsoup.nodes.Document targetFileDoc = Jsoup.parse(targetFile, "UTF-8");
                    //排除linear = no 
                    //排除空白頁
                    boolean isLinearNo = (linearTrialContent != null &&  StringUtils.equalsIgnoreCase(linearTrialContent.getNodeValue(), "no"));
                    boolean isBlank = (targetFileDoc.getElementsByTag("body").text().length() < 1 
                            && targetFileDoc.getElementsByTag("body").get(0).getElementsByTag("img").size() < 1 
                            && targetFileDoc.getElementsByTag("body").get(0).getElementsByTag("image").size() < 1 
                            && targetFileDoc.getElementsByTag("body").get(0).getElementsByTag("svg").size() < 1);
                    if (isLinearNo ||isBlank) {
                        logger.info("加入試閱內容: 無效頁 "+idrefTrialContent.getNodeValue() + ",linear=No:"+isLinearNo +",空白:"+isBlank );
                        TRIAL_MAX_SIZE++;
                    } else {
                        logger.info("加入試閱內容: 有效頁" + child.getAttributes().getNamedItem("idref"));
                    }
                    spineArray.add(child);
                } else {
                    spineRemoveArray.add(child);
                }
            }

            objectData.close();
        }

        if ("reflowable".equalsIgnoreCase(format)) {
            if(allTagPNotFound) {
                throw new Exception("reflowable 檔案內容沒有<P>標籤");
            }
            
            if(!reflowableTrailCut && !targetHtmlImgOnly && spineRemoveArray.isEmpty()) {
                    throw new Exception("reflowable 試閱處理失敗，沒有文字被截斷");
            }
            
            if (spineRemoveArray.isEmpty()&&!reflowableTrailCut) {
                throw new Exception("reflowable 試閱處理失敗，沒有章節被截斷");
            }
        }
        
        cutSpine(spineRemoveArray);
        /* [+] Add cover back */
        if (cover_manifest != null) {
            manifest.appendChild(cover_manifest);
        }
        if (cover_spine != null) {
            logger.info("加入試閱封面");
            spine.appendChild(cover_spine);
        }
        /* [-] Add cover back */
        removeResource(spineRemoveArray);
        updateToc();
    }



	private void cutBySpecifyPages(String Ranges) throws Exception {
        String[] range = Ranges.split(";");
        ArrayList<Integer> pageList = new ArrayList<Integer>();
        
        // Parsing ranges to page list
        for(int i = 0; i < range.length; i++) {
            if(range[i].contains("-")) {
                int start = Integer.valueOf((range[i].split("-"))[0]);
                int end = Integer.valueOf((range[i].split("-"))[1]);
                
                for(int j = start; j < (end + 1); j++) {
                    pageList.add(j - 1);
                }                            
            } else {
                pageList.add(Integer.valueOf(range[i]) - 1);
            }
        }
        
        File rootFile = new File(rootFileDir + rootFileName);
        InputStream objectData = new FileInputStream(rootFile);
        Document doc = dBuilder.parse(objectData);
        Element root = doc.getDocumentElement();
        removeCommentAndEmptyText(root);
        objectData.close();
        
        Node spine = doc.getElementsByTagNameNS("*", "spine").item(0);
        ArrayList<Node> spineRemoveArray = new ArrayList<Node>();
        int count = 0;
        NodeList spineNodeList = spine.getChildNodes();
        for (int i = 0; i < spineNodeList.getLength(); i++) {
            if(pageList.size() > count && pageList.get(count) == i) {
                count++;
            } else {
                spineRemoveArray.add(spineNodeList.item(i));
            }
        }
        
        cutSpine(spineRemoveArray);
        removeResource(spineRemoveArray);
        updateToc();
    }
    
    private void cutSpine(ArrayList<Node> removeArray) throws Exception {
        File rootFile = new File(rootFileDir + rootFileName);
        InputStream objectData = new FileInputStream(rootFile);
        Document doc = dBuilder.parse(objectData);
        Node spine = doc.getElementsByTagNameNS("*", "spine").item(0);
        
        for(int i = 0; i < removeArray.size(); i++) {
            Node remove = removeArray.get(i);
            NodeList childList = spine.getChildNodes();
            
            for(int j = 0; j < childList.getLength(); j++) {
                Node child = childList.item(j);
                NamedNodeMap map = child.getAttributes();
                
                if(map != null) {
                    Node idref = map.getNamedItem("idref");
                    
                    if(remove.getAttributes().getNamedItem("idref").getNodeValue().equals(
                            idref.getNodeValue())) {
                        spine.removeChild(child);
                    }
                }
            }
        }

        // Save OPF file.
        rootFile.delete();
        Utility.writeXml(doc, rootFileDir + rootFileName);

        objectData.close();
    }

    private void removeResource(ArrayList<Node> removeArray) throws Exception {
        File rootFile = new File(rootFileDir + rootFileName);
        InputStream objectData = new FileInputStream(rootFile);
        Document doc = dBuilder.parse(objectData);
        Element root = doc.getDocumentElement();
        removeCommentAndEmptyText(root);
        objectData.close();

        Node manifest = doc.getElementsByTagNameNS("*", MANIFEST).item(0);
        Node spine = doc.getElementsByTagNameNS("*", "spine").item(0);

        LinkedHashMap<String, String> manifestMap = buildManifestMap(manifest);

        // Get dependency file
        ArrayList<String> removeResources = buildRemoveResource(removeArray, manifestMap);

        // Check other xml whether using removeResource.
        NodeList spineNodeList = spine.getChildNodes();
        for (int i = 0; i < spineNodeList.getLength(); i++) {
            Node child = spineNodeList.item(i);
            NamedNodeMap map = child.getAttributes();
            Node idref = map.getNamedItem("idref");

            String folderPath = rootFileDir.substring(0, rootFileDir.lastIndexOf(PATHSEP));
            String srcPath = manifestMap.get(idref.getNodeValue());

            // TODO useless code ?!
            while (srcPath.contains("../")) {
                srcPath.replace("../", "");
                folderPath.substring(0, folderPath.lastIndexOf(PATHSEP));
            }
            srcPath = folderPath + PATHSEP + srcPath;
            objectData = new FileInputStream(srcPath);

            BufferedReader br = new BufferedReader(new InputStreamReader(objectData));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            String content = sb.toString();

            for (int j = 0; j < removeResources.size(); j++) {
                String resourcePath = removeResources.get(j);
                if (content.contains(resourcePath)) {
                    removeResources.remove(j);
                }
            }

            objectData.close();
        }

        // Get manifestRemoveArray and delete file
        ArrayList<Node> manifestRemoveArray = buildManifestRemoveArray(manifest, removeArray, removeResources);

        cutManifest(manifestRemoveArray);
        // deleteFiles(rootFileDir, removeResources);
    }

    /**
     * delete file in directory (according root file path)
     * @param rootFileDir2
     * @param removeResources
     */
    private static void deleteFiles(String rootFileDir, ArrayList<String> removeResources) {
		for (String childPath:removeResources)
		{
            childPath = childPath.replace("../", "");
            File f = new File(rootFileDir + "/" + childPath);
            try {
				if (!f.delete())
				{
					throw new IOException("Fail to delete file." + f.getPath());
				}
			}catch (Exception e)
			{
                logger.warn("Unexcepted item resource delete fail:" + f.getAbsolutePath());
            }
        }

    }

    /**
     * debug purpose
     * @param string
     */
    private static void dumpDir(String string) {
        File f = new File(string);
        logger.debug("dumpDirStruct:" + f.getAbsolutePath());
		if (!f.isDirectory())
		{
            return;
        }

		for (File childF:f.listFiles())
		{
            dumpDir(childF.getAbsolutePath());
        }

    }

	private ArrayList<String> buildRemoveResource(ArrayList<Node> removeArray,
    		LinkedHashMap<String, String> manifestMap) throws IOException {
        ArrayList<String> removeResources = new ArrayList<>();

        for (int i = 0; i < removeArray.size(); i++) {
            Node child = removeArray.get(i);
            NamedNodeMap map = child.getAttributes();
            Node idref = map.getNamedItem("idref");

            String folderPath = rootFileDir.substring(0, rootFileDir.lastIndexOf(PATHSEP));
            String srcPath = manifestMap.get(idref.getNodeValue());
            while (srcPath.contains("../")) {
                srcPath.replace("../", "");
                folderPath.substring(0, folderPath.lastIndexOf(PATHSEP));
            }
            srcPath = folderPath + PATHSEP + srcPath;
            FileInputStream objectData = new FileInputStream(srcPath);

            BufferedReader br = new BufferedReader(new InputStreamReader(objectData));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            String content = sb.toString();

            calcRemoveResource(removeResources, content, "xlink:href");
            calcRemoveResource(removeResources, content, "src");

            objectData.close();
        }
        return removeResources;
    }

	private void calcRemoveResource(ArrayList<String> removeResources,
			String content,String tagName) {

        while (content.contains(tagName)) {
            content = content.substring(content.indexOf(tagName) + tagName.length());
            int start = content.indexOf("\"") + 1;
            int end = content.indexOf("\"", start);
            if (start <= 0 || end <= 0) {
                continue;
            }
            String resourcePath = content.substring(start, end);
            if (!removeResources.contains(resourcePath)) {
                removeResources.add(resourcePath);
            }
        }

    }


	private ArrayList<Node> buildManifestRemoveArray(Node manifest, 
    		ArrayList<Node> removeArray, ArrayList<String> removeResources) {
        ArrayList<Node> manifestRemoveArray = new ArrayList<>();

        NodeList manifestNodeList = manifest.getChildNodes();
        for (int i = 0; i < manifestNodeList.getLength(); i++) {
            Node child = manifestNodeList.item(i);
            NamedNodeMap map = child.getAttributes();
            Node id = map.getNamedItem("id");
            Node href = map.getNamedItem("href");

            for (int j = 0; j < removeArray.size(); j++) {
                Node removeArrayChild = removeArray.get(j);
                NamedNodeMap removeArrayMap = removeArrayChild.getAttributes();
                Node idref = removeArrayMap.getNamedItem("idref");

                if (idref.getNodeValue().equals(id.getNodeValue())) {
                    manifestRemoveArray.add(child);
                    String folderPath = rootFileDir.substring(0, rootFileDir.lastIndexOf(PATHSEP));
                    String srcPath = href.getNodeValue();
                    while (srcPath.contains("../")) {
                        srcPath.replace("../", "");
                        folderPath.substring(0, folderPath.lastIndexOf(PATHSEP));
                    }
                    srcPath = folderPath + PATHSEP + srcPath;
                    File deleteFile = new File(srcPath);
                    deleteFile.delete();
                }
            }

            for (int j = 0; j < removeResources.size(); j++) {
                String removeName = removeResources.get(j).replaceAll("../", "");
                String hrefName = href.getNodeValue().replaceAll("../", "");
                if (removeName.equals(hrefName)) {
                    manifestRemoveArray.add(child);
                    String folderPath = rootFileDir.substring(0, rootFileDir.lastIndexOf(PATHSEP));
                    String srcPath = href.getNodeValue();
                    while (srcPath.contains("../")) {
                        srcPath.replace("../", "");
                        // TODO UselessCode
                        folderPath.substring(0, folderPath.lastIndexOf(PATHSEP));
                    }
                    srcPath = folderPath + PATHSEP + srcPath;
                    File deleteFile = new File(srcPath);
                    deleteFile.delete();
                }
            }
        }
        return manifestRemoveArray;
    }

    private LinkedHashMap<String, String> buildManifestMap(Node manifest) {
        LinkedHashMap<String, String> ret = new LinkedHashMap<String, String>();

        NodeList manifestNodeList = manifest.getChildNodes();
        for (int i = 0; i < manifestNodeList.getLength(); i++) {
            Node child = manifestNodeList.item(i);
            NamedNodeMap map = child.getAttributes();
            Node id = map.getNamedItem("id");
            Node href = map.getNamedItem("href");

            ret.put(id.getNodeValue(), href.getNodeValue());
        }
        return ret;
    }

    private void cutManifest(ArrayList<Node> removeArray) throws Exception {
        File rootFile = new File(rootFileDir + rootFileName);
        InputStream objectData = new FileInputStream(rootFile);
        Document opfDoc = dBuilder.parse(objectData);
        Node manifest = opfDoc.getElementsByTagNameNS("*", MANIFEST).item(0);

        for (int i = 0; i < removeArray.size(); i++) {
            Node remove = removeArray.get(i);
            NodeList childList = manifest.getChildNodes();

            for (int j = 0; j < childList.getLength(); j++) {
                Node child = childList.item(j);
                NamedNodeMap map = child.getAttributes();

                if (map != null) {
                    Node idref = map.getNamedItem("id");

                    if(remove.getAttributes().getNamedItem("id").getNodeValue().equals(
                            idref.getNodeValue())) {
                        manifest.removeChild(child);
                    }
                }
            }
        }

        // Save OPF file.
        rootFile.delete();
        Utility.writeXml(opfDoc, rootFileDir + rootFileName);

        objectData.close();
    }

    private void updateToc() throws Exception {
        // Get all href in manifest
        ArrayList<String> hrefList = new ArrayList<String>();

        File rootFile = new File(rootFileDir + rootFileName);
        InputStream objectData = new FileInputStream(rootFile);
        Document docOpf = dBuilder.parse(objectData);
        Element rootOpf = docOpf.getDocumentElement();
        removeCommentAndEmptyText(rootOpf);
        objectData.close();

        Node manifest = docOpf.getElementsByTagNameNS("*", MANIFEST).item(0);
        NodeList manifestNodeList = manifest.getChildNodes();

        for (int i = 0; i < manifestNodeList.getLength(); i++) {
            Node child = manifestNodeList.item(i);
            NamedNodeMap map = child.getAttributes();
            Node href = map.getNamedItem("href");

            hrefList.add(href.getNodeValue());
        }

        // Get toc path
        String tocPath = getTocPath(manifestNodeList);
        Boolean hasToc = "".equals(tocPath) ? false : true;

        
        // find no toc by properties => nav, try spine.toc
        if (!hasToc) {
            String tocId;

            Node spine = docOpf.getElementsByTagNameNS("*", "spine").item(0);
            NamedNodeMap map = spine.getAttributes();
            Node toc = map.getNamedItem("toc");
            if (toc == null)
                throw new ServerException("id_err_305", "Fail to find Toc, stop generating Trial.");
            tocId = toc.getNodeValue();

            for (int i = 0; i < manifestNodeList.getLength(); i++) {
                Node child = manifestNodeList.item(i);
                NamedNodeMap map_manifest = child.getAttributes();
                Node id = map_manifest.getNamedItem("id");

                if (id.getNodeValue().equals(tocId)) {
                    tocPath = map_manifest.getNamedItem("href").getNodeValue();
                    hasToc = true;
                    break;
                }
            }
        }
        String relativePath = calcRelativePath(tocPath);

        tocPath = rootFileDir + tocPath;
        SystemLog("[updateToc]", " tocPath:" + tocPath);

        // Open toc
        File tocFile = new File(tocPath);
        objectData = new FileInputStream(tocFile);
        Document doc_toc = dBuilder.parse(objectData);
        Element root_toc = doc_toc.getDocumentElement();
        removeCommentAndEmptyText(root_toc);
        objectData.close();

        NodeList body = doc_toc.getElementsByTagName("body");
        NodeList navMap = doc_toc.getElementsByTagName("navMap");
        Node tocRoot;
        String attribute;

        if (body.getLength() > 0) {
            tocRoot = body.item(0);
            attribute = "href";
        } else if (navMap.getLength() > 0) {
            tocRoot = navMap.item(0);
            attribute = "src";
        } else {
            return;
        }

        Boolean hasReplace = replaceAllEmptyPage(tocRoot, attribute, hrefList, relativePath);

        if (hasReplace) {
            // // Copy empty page.
            // ClassLoader classLoader = getClass().getClassLoader();
            // InputStream emptyPageFile =
            // classLoader.getResourceAsStream("samplefinished.html");
            // BufferedReader reader = new BufferedReader(new
            // InputStreamReader(emptyPageFile, "UTF-8"));
            // String copyPath = rootFileDir + "samplefinished.html";
            // File copyFile = new File(copyPath);
            // BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new
            // FileOutputStream(copyFile), "UTF-8"));
            // String buf;
            // while((buf = reader.readLine()) != null) {
            // writter.write(buf + "\r\n");
            // }
            // reader.close();
            // writter.close();
            // emptyPageFile.close();
            //
            // // Add empty page to content.opf
            // // add to manifest
            // Node newChild_item = docOpf.getElementsByTagNameNS("*",
            // "item").item(0).cloneNode(false);
            // NamedNodeMap map_item = newChild_item.getAttributes();
            // Node id = map_item.getNamedItem("id");
            // Node href = map_item.getNamedItem("href");
            // Node media_type = map_item.getNamedItem("media-type");
            // id.setNodeValue(EMPTY_PAGE);
            // href.setNodeValue("samplefinished.html");
            // media_type.setNodeValue(MimeType.fromExtension(".html").getType());
            // if (map_item.getNamedItem("properties") != null) { // avoid "nav" duplication
            // map_item.removeNamedItem("properties");
            // }
            // manifest.appendChild(newChild_item);
            // // add to spine
            // Node spine = docOpf.getElementsByTagNameNS("*", "spine").item(0);
            // NodeList itemrefList = docOpf.getElementsByTagNameNS("*", "itemref");
            // if (itemrefList.getLength() != 0) {
            // Node newChild_itemref = itemrefList.item(0).cloneNode(false);
            // for (int i = 0; i < newChild_itemref.getAttributes().getLength(); i++) {
            // Node att = newChild_itemref.getAttributes().item(i);
            // if("linaer".equalsIgnoreCase(att.getNodeName())) {
            // newChild_itemref.getAttributes().removeNamedItem(att.getNodeName());
            // }
            // }
            // NamedNodeMap map_itemref = newChild_itemref.getAttributes();
            // Node idref = map_itemref.getNamedItem("idref");
            // idref.setNodeValue(EMPTY_PAGE);
            // spine.appendChild(newChild_itemref);
            // }

            // Save OPF file.
            String opfPath = rootFileDir + rootFileName;
            Utility.writeXml(docOpf, opfPath);

            // Save toc file.
            Utility.writeXml(doc_toc, tocPath);
        }
    }

    /**
     * if tocPath has directory, extract the path part. (ex: abcd/toc.xhtml =>
     * if tocPath has no prefix, return "";
     * @param tocPath
     * @return
     */
    private static String calcRelativePath(String tocPath) {
        String ret = "";
        int lastIndex = tocPath.lastIndexOf('/');
		if (lastIndex>=0)
		{
            ret = tocPath.substring(0, lastIndex + 1);
        }

        return ret;
    }

    private static String getTocPath(NodeList manifestNodeList) {
        String tocPath = "";
        for (int i = 0; i < manifestNodeList.getLength(); i++) {
            Node child = manifestNodeList.item(i);
            NamedNodeMap map = child.getAttributes();
            Node properties = map.getNamedItem("properties");

            if (properties != null && properties.getNodeValue().equals("nav")) {
                tocPath = map.getNamedItem("href").getNodeValue();
                return tocPath;
            }
        }
        return tocPath;
    }



    private Boolean replaceAllEmptyPage(Node root, String attribute, ArrayList<String> hrefList, String relativePath) {
        String emptyPage = "samplefinished.html";
        Boolean hasReplace = false;

        NamedNodeMap map = root.getAttributes();
        if (map != null) {
            Node nodeAttribute = map.getNamedItem(attribute);

            if (nodeAttribute != null) {

                String path = nodeAttribute.getNodeValue();

                if (path.contains("../")) {
                    path = StringUtils.remove(path, "../");
                    path = StringUtils.remove(path, relativePath);
                }

                String srcPath = relativePath + path;
                boolean stillExist = false;

                // Proceessing NameSpace
                if (srcPath.contains("#")) {
                    srcPath = srcPath.substring(0, srcPath.lastIndexOf("#"));
                }

                for (int i = 0; i < hrefList.size(); i++) {
                    if (hrefList.get(i).equals(srcPath)) {
                        stillExist = true;
                    }
                }
                Attr attr = nodeAttribute.getOwnerDocument().createAttribute("data-empty");
                if (!stillExist) {
                    nodeAttribute.setNodeValue("");
                    attr.setNodeValue("blank");
                    map.setNamedItem(attr);
                    hasReplace = true;
                }
            }
        }

        NodeList childs = root.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            hasReplace |= replaceAllEmptyPage(childs.item(i), attribute, hrefList, relativePath);
        }

        return hasReplace;
    }

    private static String calcParent(String relativePath) {
        String ret = "";
        for (int i = 0; i < relativePath.length(); i++) {
			if (relativePath.charAt(i)=='/')
			{
                ret += "../";
            }
        }
        return ret;
    }

    private void getEpubCheckSum() throws Exception {
        String Tag = "[getEpubCheckSum]";
        Long StartTime = System.currentTimeMillis();
        convertingLogger.log(Tag, "Start getEpubCheckSum()");

        String opfPath = rootFileDir + rootFileName;
        InputStream objectData = new FileInputStream(opfPath);
        Document doc = dBuilder.parse(objectData);
        Element root = doc.getDocumentElement();
        removeCommentAndEmptyText(root);
        objectData.close();

        Node manifest = doc.getElementsByTagNameNS("*", MANIFEST).item(0);
        Node spine = doc.getElementsByTagNameNS("*", "spine").item(0);

        Map<String, String> manifestMap = new HashMap<String, String>();
        MessageDigest sha = MessageDigest.getInstance("SHA-256");

        for (int i = 0; i < manifest.getChildNodes().getLength(); i++) {
            Node child = manifest.getChildNodes().item(i);
            NamedNodeMap map = child.getAttributes();
            Node id = map.getNamedItem("id");
            Node href = map.getNamedItem("href");

            manifestMap.put(id.getNodeValue(), href.getNodeValue());
        }

        
        for (int i = 0; i < spine.getChildNodes().getLength(); i++) {
            Node child = spine.getChildNodes().item(i);
            String path = rootFileDir;

            NamedNodeMap map = child.getAttributes();
            Node idref = map.getNamedItem("idref");
            path = path + manifestMap.get(idref.getNodeValue());
            objectData = new FileInputStream(path);

            byte[] content = new byte[1024];
            while (objectData.read(content) != -1) {
                sha.update(content);
            }

            objectData.close();
        }

        String checkSum = Utility.byte2hex(sha.digest());

        convertingLogger.log(GenericLogLevel.Info,
        		"CheckSum: " + checkSum + "\r\n" +
                "End getEpubCheckSum() spend " 
        				+ (System.currentTimeMillis() - StartTime) + "ms",
                EpubBookStandAloneLocal.class.getName());
    }

    private HashSet<String> getEpubCheckList(String listName) throws IOException {
        final String listPath = "EpubCheckList/" + listName;
        HashSet<String> list = new HashSet<>();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(listPath);
        if (inputStream == null) {
            return list;
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = br.readLine()) != null) {
            if (!line.isEmpty()) {
                list.add(line);
            }
        }

        br.close();
        inputStream.close();

        return list;
    }

    private File checkFontInfo(File xhtml) throws Exception {
        // get append content
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream headAppend = classLoader.getResourceAsStream("head_append.cfg");
        BufferedReader reader = new BufferedReader(new InputStreamReader(headAppend, "UTF-8"));
        ArrayList<String> appendContent = new ArrayList<String>();
        String buf;
        while ((buf = reader.readLine()) != null) {
            appendContent.add(buf);
        }
        reader.close();
        headAppend.close();

        // If already be written, remove it.
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(xhtml), "UTF-8"));
        ArrayList<String> fileContent = new ArrayList<String>();
        while ((buf = reader.readLine()) != null) {
            if (buf.equals(appendContent.get(0))) {
                ArrayList<String> checkContent = new ArrayList<String>();
                checkContent.add(buf);
                int i = 1;
                while (i < appendContent.size() && (buf = reader.readLine()) != null) {
                    checkContent.add(buf);
                    if (!buf.equals(appendContent.get(i))) {
                        break;
                    }
                    i++;
                }

                if (!(i == appendContent.size())) {
                    fileContent.addAll(checkContent);
                }
            } else {
                fileContent.add(buf);
            }
        }
        reader.close();

        File new_xhtml = new File(xhtml.getPath());
        BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new_xhtml), "UTF-8"));
        for (int i = 0; i < fileContent.size(); i++) {
            writter.write(fileContent.get(i) + "\r\n");
        }
        writter.close();

        return new_xhtml;
    }

    private File addFontInfo(File xhtml) throws IOException {
        // get append content
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream headAppend = classLoader.getResourceAsStream("head_append.cfg");
        BufferedReader reader = new BufferedReader(new InputStreamReader(headAppend, "UTF-8"));
        ArrayList<String> appendContent = new ArrayList<String>();
        String buf;
        while ((buf = reader.readLine()) != null) {
            appendContent.add(buf);
        }
        reader.close();
        headAppend.close();

        // write to new file
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(xhtml), "UTF-8"));
        ArrayList<String> fileContent = new ArrayList<String>();
        Boolean isWritten = false;
        while ((buf = reader.readLine()) != null) {
            if (buf.contains("<head>") && !isWritten) {
                isWritten = true;
                String head = "<head>";
                String before = buf.substring(0, buf.indexOf(head));
                String after = buf.substring(buf.indexOf(head) + head.length());
                fileContent.add(before);
                fileContent.add(head);

                for (int i = 0; i < appendContent.size(); i++) {
                    fileContent.add(appendContent.get(i));
                }

                fileContent.add(after);
            } else {
                fileContent.add(buf);
            }
        }
        reader.close();

        File new_xhtml = new File(xhtml.getPath());
        BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new_xhtml), "UTF-8"));
        for (int i = 0; i < fileContent.size(); i++) {
            writter.write(fileContent.get(i) + "\r\n");
        }
        writter.close();

        return new_xhtml;
    }

    private File addBOM(File xhtml) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(xhtml), "UTF-8"));
        ArrayList<String> fileContent = new ArrayList<String>();
        String buf;
        while ((buf = reader.readLine()) != null) {
            fileContent.add(buf);
        }
        reader.close();

        File new_xhtml = new File(xhtml.getPath());
        BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new_xhtml), "UTF-8"));
        writter.write('\ufeff');
        for (int i = 0; i < fileContent.size(); i++) {
            writter.write(fileContent.get(i) + "\r\n");
        }
        writter.close();

        return new_xhtml;
    }

    private static void removeBom(File file) throws IOException {
        byte[] bs = FileUtils.readFileToByteArray(file);

        if(bs.length > 3 &&
                (bs[0] == -17 && bs[1] == -69 && bs[2] == -65)) {
            byte[] nbs = new byte[bs.length - 3];
            System.arraycopy(bs, 3, nbs, 0, nbs.length);
            FileUtils.writeByteArrayToFile(file, nbs);
        }
    }

    // [+] XML function
    private File writeXml(Document doc, String filePath) throws Exception {
        File xml = new File(filePath);
        if (xml.exists()) {
            xml.delete();
        }
        //for local debug 
//        TransformerFactory transformerFactory = new com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl();
		TransformerFactory transformerFactory = TransformerFactory.newInstance();        
        Transformer transformer = transformerFactory.newTransformer();
//        transformer.setOutputProperty("indent","yes");
        doc.getDoctype();
        DOMSource source = new DOMSource(doc);
        StringWriter stringWriter=new StringWriter();
        StreamResult result = new StreamResult(stringWriter);
        transformer.transform(source, result);
        
        FileOutputStream outputStream = new FileOutputStream(xml);
        byte[] strToBytes = stringWriter.toString().getBytes();
        outputStream.write(strToBytes);
        outputStream.close();

        return xml;
    }
    
    // [+] XML function
    private File writeXmlWithDocType(Document doc, String filePath , String docPublicId , String docSystemId) throws Exception {
        File xml = new File(filePath);
        if (xml.exists()) {
            xml.delete();
        }
        //for local debug 
//        TransformerFactory transformerFactory = new com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl();
		TransformerFactory transformerFactory = TransformerFactory.newInstance();        
        Transformer transformer = transformerFactory.newTransformer();
//        transformer.setOutputProperty("indent","yes");
        doc.getDoctype();
        transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,docPublicId);
        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,docSystemId);
        DOMSource source = new DOMSource(doc);
        StringWriter stringWriter=new StringWriter();
        StreamResult result = new StreamResult(stringWriter);
        transformer.transform(source, result);
        
        FileOutputStream outputStream = new FileOutputStream(xml);
        byte[] strToBytes = stringWriter.toString().getBytes();
        outputStream.write(strToBytes);
        outputStream.close();

        return xml;
    }

    private void removeCommentAndEmptyText(Node root) {
        NodeList Nodes = root.getChildNodes();

        if(Nodes == null) return;

        // remove empty #test
        for (int i = Nodes.getLength() - 1; i >= 0; i--) {
            Node node = Nodes.item(i);
            removeCommentAndEmptyText(node);

            if (node instanceof Text) {
                String value = node.getNodeValue().trim();
                if (value.equals("")) {
                    root.removeChild(node);
                }
            } else if (node instanceof org.w3c.dom.Comment) {
                root.removeChild(node);
            }
        }
    }

    // main is for test
	public static void main(String args[]) {
		String epubPath = "R:/tmp/E050078584.epub";
		String bookFileId = "E050078584";
		bookFileId = bookFileId.replaceAll(".epub", "");

        try {
            EpubBookStandAloneLocal sb = new EpubBookStandAloneLocal();
            LogToS3 converterLogger = new LogToS3();
            RequestData requestData = buildRequestData(bookFileId);

            Properties config = buildConfig("R:/tmp/config.properties");
            sb.setup(epubPath, requestData, config, converterLogger);
            sb.convert();

            converterLogger.addAll(sb.consistencyResult);
            converterLogger.addAll(sb.epubCheckResult);
            logger.info(converterLogger.dumpMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
        }
    }

    private static Properties buildConfig(String configFilePath) throws IOException {
        Properties prop = new Properties();
        File f = new File(configFilePath);
        FileInputStream inStream = new FileInputStream(f);
        prop.load(inStream);
        return prop;
    }

    private static RequestData buildRequestData(String tmpName) {
        RequestData req = new RequestData();
        req.setBook_file_id(tmpName);
		req.setFormat("reflowable");
        req.setItem("E05");
        req.setIsTrial(true);
        req.setPreview_type("1");
        req.setPreview_content("10");
        req.setScreenText("但萬一我們都搞錯了呢");
        req.setScreenXhtml("p-004");
        req.setScreenXhtmlPrevious("p-toc-001");
        req.setTotalPage("855");
        req.setTrialPage("86");
        return req;
    }

	/**
	 * check Margin, warn about negative numbers.	
	 * @param opfPath
	 */
    /**
     * check Margin, warn about negative numbers.
     * @param opfPath
     */
	private static void checkCSS(String opfPath, LogToS3 convertingLogger,CheckResult checkResult,RequestData requestData)
	{

        String fileFormat = requestData.getFormat();

        List<File> fileList = Utility.listAllFile(new File(opfPath));
        // get all CSS
        for (File tmpFile : fileList) {
            CheckHtml.getCss(opfPath, tmpFile, convertingLogger, checkResult, fileFormat);
        }
        // check html
        for (File tmpFile : fileList) {
            CheckHtml.checkCssHtml(opfPath, tmpFile, convertingLogger, checkResult, fileFormat);
        }
    }

    /**
     * get opf unique-identifier info
     * 
     * @param currentFile
     * @return
     */
    private String epubOPFUniqueIdentifierInfoRecu(File currentFile) {
        String uniqueIdentifier = "";

        if (currentFile.isDirectory()) {
            String[] child = currentFile.list();

            for (int i = 0; i < child.length; i++) {
                epubOPFUniqueIdentifierInfoRecu(new File(currentFile, child[i]));
            }
        }

        String fileName = currentFile.getPath();
        String extension = "";

        if (fileName.contains(".")) {
            extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        }

        if (extension.equalsIgnoreCase("opf")) {
            try {
                File opfFile = new File(currentFile.getPath());

                logger.info("opfFile: " + opfFile);
                logger.info("opfFilePath: " + currentFile.getPath());
                logger.info("opfFileLength: " + opfFile.length());

                InputStream xml = new FileInputStream(new File(currentFile.getPath()));
                org.jsoup.nodes.Document doc = Jsoup.parse(xml, "utf-8", "", org.jsoup.parser.Parser.xmlParser());
                xml.close();

                for (org.jsoup.nodes.Element e : doc.select("package")) {
                    logger.info("unique-identifier:" + e.attr("unique-identifier"));

                    for (org.jsoup.nodes.Element ele : e.select("metadata")) {
                        List<org.jsoup.nodes.Node> nodeList = ele.childNodes();

                        for (org.jsoup.nodes.Node n : nodeList) {
                            if (n.nodeName().equals("dc:identifier") && n.attr("id").equals(e.attr("unique-identifier"))) {
                                uniqueIdentifier = ((org.jsoup.nodes.TextNode) n.childNode(0)).text();
                                bookUniqueIdentifier = ((org.jsoup.nodes.TextNode) n.childNode(0)).text();

                                logger.info("dc:identifier:" + ((org.jsoup.nodes.TextNode) n.childNode(0)).text());
                            }
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException ", e);
            } catch (IOException e) {
                logger.error("IOException ", e);
            }
        }

        return uniqueIdentifier;
    }

    /**
     * check local dir if font need decrypt
     * 
     * @param epubFolder
     * @return
     */
    private boolean fontDecryptRecu(File currentFile, String obfuscationKey) {
        boolean ret = true;

        if (currentFile.isDirectory()) {
            String[] child = currentFile.list();

            for (int i = 0; i < child.length; i++) {
                ret &= fontDecryptRecu(new File(currentFile, StringEscapeUtils.escapeHtml4(child[i])), obfuscationKey);
            }
        }

        String fileName = currentFile.getPath();
        String extension = "";

        if (fileName.contains(".")) {
            extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        }

		if (extension.equalsIgnoreCase("ttf") || extension.equalsIgnoreCase("ttc") || extension.equalsIgnoreCase("otf") || extension.equalsIgnoreCase("woff") || extension.equalsIgnoreCase("dfont") || extension.equalsIgnoreCase("suit")) {
            try {
                String fontFilename = FilenameUtils.getName(currentFile.getPath());

                consistencyResult.addMessage(GenericLogLevel.Warn, "Font name: " + fontFilename, EpubBookStandAloneLocal.class.getName());

                if (fontIsEncrypted(currentFile, extension)) {
                    File resultFontFile = null;
                    File obfuscatedDir = new File(currentFile.getParentFile(), "obfuscated");
                    resultFontFile = new File(obfuscatedDir, fontFilename);

                    logger.debug("Obfuscating font " + currentFile + " ...");

                    resultFontFile.getParentFile().mkdirs();
                    InputStream inStream = new FileInputStream(currentFile);
                    FileOutputStream outStream = new FileOutputStream(resultFontFile);

                    // IDPF Algorithm
                    logger.debug("Using IDPF obfuscation key \"" + obfuscationKey + "\"");

                    obfuscateFont(inStream, outStream, obfuscationKey, "");

                    logger.debug("Font obfuscated to " + resultFontFile.getAbsolutePath());

                    inStream.close();
                    outStream.close();

                    if (fontIsEncrypted(new File(resultFontFile.getAbsolutePath()), extension)) {
                        File fail_file = new File(resultFontFile.getAbsolutePath());
                        boolean result = Files.deleteIfExists(fail_file.toPath());

                        // logger.debug("Delete failed file result: " + result);
                        logger.info("Delete failed file result: " + result);

                        if (result) {
                            // Adobe Algorithm
                            InputStream inStreamAdobe = new FileInputStream(currentFile);
                            FileOutputStream outStreamAdobe = new FileOutputStream(resultFontFile);

                            logger.debug("Using Adobe obfuscation key \"" + obfuscationKey + "\"");

                            obfuscateFont(inStreamAdobe, outStreamAdobe, obfuscationKey, "adobe");

                            logger.debug("Font obfuscated to " + resultFontFile.getAbsolutePath());

                            inStreamAdobe.close();
                            outStreamAdobe.close();
                        }
                    }

					if (fontIsEncrypted(new File(resultFontFile.getAbsolutePath()), extension)) {
						logger.debug("Font de-obfuscate failed, file name: " + fontFilename);
						if(!"reflowable".equalsIgnoreCase(requestData.getFormat())) {
						    consistencyResult.addMessage(GenericLogLevel.Fatal, "Font de-obfuscate failed, file name: " + fontFilename, EpubBookStandAloneLocal.class.getName());
						    ret = false;
						}else {
						    consistencyResult.addMessage(GenericLogLevel.Warn, "Font de-obfuscate failed, file name: " + fontFilename, EpubBookStandAloneLocal.class.getName());
						}
					} else {
						logger.debug("Font de-obfuscate successed, file name: " + fontFilename);

						consistencyResult.addMessage(GenericLogLevel.Warn, "Font de-obfuscate successed, file name: " + fontFilename, EpubBookStandAloneLocal.class.getName());
					}

                    Path from = Paths.get(resultFontFile.getAbsolutePath());
                    Path to = Paths.get(currentFile.getPath());
                    Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);

                    FileUtils.deleteDirectory(obfuscatedDir);
                } else {
                    logger.debug("Font files do not need to be confused, file name: " + fontFilename);
                    logger.info("Font files do not need to be confused, file name: " + fontFilename);

                    consistencyResult.addMessage(GenericLogLevel.Warn, "Font files do not need to be confused, file name: " + fontFilename, EpubBookStandAloneLocal.class.getName());
                }
            } catch (Exception e) {
                logger.error("Exception ", e);
                ret = false;
            }
        }

        return ret;
    }
    private static boolean fontIsEncrypted(File currentFile, String extension) {

        if (extension.equalsIgnoreCase("woff")) {
            try {
                FVFont font = FontVerter.readFont(currentFile);

                if (font.isValid()) {
                    logger.debug("FontName:" + currentFile.getName() + " Extension:" + extension);

                    return false;
                } else {
                    return true;
                }
            } catch (IOException e) {
                logger.error("IOException ", e);
            }
        } else {
            // use try catch to determine font encryption.
            InputStream inStream = null;

            try {
                logger.debug("Hit possible font: " + currentFile.getName());

                inStream = new FileInputStream(currentFile);
                Font font = Font.createFont(Font.TRUETYPE_FONT, new BufferedInputStream(inStream));

                logger.debug("FontName:" + font.getFontName() + " NumGlyphs:" + font.getNumGlyphs() + " Extension:" + extension);
            } catch (FileNotFoundException e) {
                logger.error("FileNotFoundException ", e);
            } catch (FontFormatException e) {
                return true;
            } catch (IOException e) {
                logger.error("IOException ", e);
            } finally {
                try {
                    inStream.close();
                } catch (IOException e) {
                    logger.error("IOException ", e);
                }
            }
        }

        return false;
    }

    private static byte[] makeXORMask(String opfUID) {
        if (opfUID == null) {
            return null;
        }

        ByteArrayOutputStream mask = new ByteArrayOutputStream();

        /**
		 * This starts with the "unique-identifier", strips the whitespace, and applies SHA1 hash
		 * giving a 20 byte key that we can apply to the font file.
         * 
         * See: http://www.idpf.org/epub/30/spec/epub30-ocf.html#fobfus-keygen
         **/
        try {
            Security.addProvider(new com.sun.crypto.provider.SunJCE());
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            String temp = opfUID.trim();
            sha.update(temp.getBytes("UTF-8"), 0, temp.length());
            mask.write(sha.digest());
        } catch (NoSuchAlgorithmException e) {
            System.err.println("No such Algorithm (really, did I misspell SHA-1?");
            System.err.println(e.toString());
            return null;
        } catch (IOException e) {
            System.err.println("IO Exception. check out mask.write...");
            System.err.println(e.toString());
            return null;
        }

        if (mask.size() != 20) {
            System.err.println("makeXORMask should give 20 byte mask, but isn't");
            return null;
        }

        return mask.toByteArray();
    }

    private static byte[] makeAdobeXORMask(String uuid) {
        logger.info("uuid: " + uuid);

        String cleanUUID = uuid.replaceAll("urn:uuid", "");
        cleanUUID = cleanUUID.replaceAll("-", "");
        cleanUUID = cleanUUID.replaceAll(":", "");

        logger.info("cleanUUID: " + cleanUUID);

        // return DatatypeConverter.parseHexBinary(cleanUUID);
        return cleanUUID.getBytes();
    }

    private static byte[] makeAdobeXORMaskBak(String opfUID) {
        if (opfUID == null) {
            return null;
        }

        opfUID = opfUID.replaceAll("urn:uuid", "");
        opfUID = opfUID.replaceAll("-", "");
        opfUID = opfUID.replaceAll(":", "");

        ByteArrayOutputStream mask = new ByteArrayOutputStream();
        int acc = 0;
        int len = opfUID.length();

        for (int i = 0; i < len; i++) {
            char c = opfUID.charAt(i);
            int n;

            if ('0' <= c && c <= '9') {
                n = c - '0';
            } else if ('a' <= c && c <= 'f') {
                n = c - ('a' - 10);
            } else if ('A' <= c && c <= 'F') {
                n = c - ('A' - 10);
            } else {
                continue;
            }

            if (acc == 0) {
                acc = 0x100 | (n << 4);
            } else {
                mask.write(acc | n);
                acc = 0;
            }
        }

        if (mask.size() != 16) {
            return null;
        }

        return mask.toByteArray();
    }

	/** Implements the Obfuscation Algorithm from
     * http://www.openebook.org/doc_library/informationaldocs/FontManglingSpec.html
     **/
    public static void obfuscateFont(InputStream in, OutputStream out, String obfuscationKey, String obfuscationMode) throws IOException {
        int headerLen;
        byte[] mask;

        if (obfuscationMode.equalsIgnoreCase("adobe")) {
            headerLen = 1024;
            mask = makeAdobeXORMask(obfuscationKey);
        } else {
            headerLen = 1040;
            mask = makeXORMask(obfuscationKey);
        }

        System.out.printf("The key being used:\n");

        for (int n = 0; n < mask.length; n++) {
            System.out.printf("%2x ", mask[n]);
        }

        try {
            byte[] buffer = new byte[4096];
            int len;
            boolean first = true;

            while ((len = in.read(buffer)) > 0) {
                if (first && mask != null) {
                    first = false;

                    for (int i = 0; i < headerLen; i++) {
                        buffer[i] = (byte) (buffer[i] ^ mask[i % mask.length]);
                    }
                }

                out.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        out.close();
    }

    public static void obfuscateFontNew(InputStream in, OutputStream out, String obfuscationKey, String obfuscationMode) throws IOException {
        int len = 1040;
        byte[] mask;

        if (obfuscationMode.equalsIgnoreCase("adobe")) {
            len = 1024;
            mask = makeAdobeXORMask(obfuscationKey);
        } else {
            len = 1040;
            mask = makeXORMask(obfuscationKey);
        }

        try {
            byte[] bytes = new byte[in.available()];
            int size = in.read(bytes);
            int count = size > len ? len : size;
            int i = 0;

            while (i < count) {
                bytes[i] = (byte) (bytes[i] ^ (mask[i % mask.length]));
                i++;
            }

            out.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        out.close();
    }

    /**
	 * Encrypt the supplied string using a SHA-1 Hash. This could
	 * be stronger by using a salt, etc. but this is lightweight
	 * mangling, so hardly worth it.
	 * 
	 * @param x
	 * @return
	 */
	public static byte[] encrypt(String x) {
		try {
			java.security.MessageDigest d = null;
			d = java.security.MessageDigest.getInstance("SHA-1");
			d.reset();
			d.update(x.getBytes());

			return d.digest();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Generate the IDPF key from the supplied unique ID
	 * 
	 * @param uuid
	 * @return
	 */
	public static byte[] idpfKeyFromIdentifier(String uuid) {
		String cleanUUID = uuid.replaceAll(" ", "");
		cleanUUID = cleanUUID.replaceAll("\t", "");
		cleanUUID = cleanUUID.replaceAll("\n", "");
		cleanUUID = cleanUUID.replaceAll("\r", "");

		return encrypt(cleanUUID);
	}

    public Set<String> getMediaType() {
        return mediaType;
    }

    public void setMediaType(Set<String> mediaType) {
        this.mediaType = mediaType;
    }
}