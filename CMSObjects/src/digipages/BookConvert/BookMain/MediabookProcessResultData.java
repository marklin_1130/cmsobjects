package digipages.BookConvert.BookMain;

import java.util.List;

public class MediabookProcessResultData {
    String item;
    String call_type;
    String convert_id;
    String file_url;
    String file_cover_url;
    boolean convert_result ;
    int return_file_num;
    String error_message;
    List<MediabookReturntData> r_data;
    Long media_size ;
    Long media_lengthtime  ;
    String book_version;
    
    public Long getMedia_size() {
        return media_size;
    }
    public Long getMedia_lengthtime() {
        return media_lengthtime;
    }
    public void setMedia_size(Long media_size) {
        this.media_size = media_size;
    }
    public void setMedia_lengthtime(Long long1) {
        this.media_lengthtime = long1;
    }
    public String getItem() {
        return item;
    }
    public String getCall_type() {
        return call_type;
    }
    public String getConvert_id() {
        return convert_id;
    }
    public String getFile_url() {
        return file_url;
    }
    public String getFile_cover_url() {
        return file_cover_url;
    }
    public boolean isConvert_result() {
        return convert_result;
    }
    public int getReturn_file_num() {
        return return_file_num;
    }
    public String getError_message() {
        return error_message;
    }
    public List<MediabookReturntData> getR_data() {
        return r_data;
    }
    public void setItem(String item) {
        this.item = item;
    }
    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }
    public void setConvert_id(String processing_id) {
        this.convert_id = processing_id;
    }
    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }
    public void setFile_cover_url(String file_cover_url) {
        this.file_cover_url = file_cover_url;
    }
    public void setConvert_result(boolean convert_result) {
        this.convert_result = convert_result;
    }
    public void setReturn_file_num(int return_file_num) {
        this.return_file_num = return_file_num;
    }
    public void setError_message(String error_message) {
        this.error_message = error_message;
    }
    public void setR_data(List<MediabookReturntData> r_data) {
        this.r_data = r_data;
    }
    public String getBook_version() {
        return book_version;
    }
    public void setBook_version(String book_version) {
        this.book_version = book_version;
    }
    
    
    
}
