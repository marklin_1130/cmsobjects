package digipages.BookConvert.BookMain;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashSet;
import java.io.IOException;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.utility.*;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

public class EpubBook extends ConvertingBook {
    private static final String PATHSEP = "/";
    private static final String NO = "no";
    private String epubFolderPath;
    private File epubFolder;
    private String checkResultgPath;
    private static DPLogger logger=DPLogger.getLogger(EpubBook.class.getName());
    
    static DocumentBuilderFactory dbFactory;
    static DocumentBuilder dBuilder;
    private RequestData requestData;
    
    public EpubBook(AmazonS3 awsS3Client, RequestData requestData,Properties config, DataMessage datamessage) throws Exception {
        super(awsS3Client, requestData,config, datamessage);
        
        dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setNamespaceAware(true);
        dBuilder = dbFactory.newDocumentBuilder();
        
        checkResultgPath = getLogFolder() + PATHSEP + "CheckResult.log";
        
        if(NO.equals(properties.getProperty("EpubCheck_is_opened"))) {
        }
        if(NO.equals(properties.getProperty("ConsistencyCheck_is_opened"))) {
        }
        properties.getProperty("fileSizeUpperBound","300000");
        
        epubFolderPath = "/tmp/Epub_" + requestData.getBook_file_id() + PATHSEP;
        epubFolder = new File(epubFolderPath);
        epubFolder.mkdirs();
        this.requestData=requestData;
    }
    
    public void convert() throws Exception {
        Boolean result=false;
        String message="";
        CheckResult consistencyResult = null;
        CheckResult epubCheckResult = null;
        boolean inRetry=false;
        boolean isFixedlayout=false;
    	try {
	        super.convert();
	        if (!checkBookSize()) {
	            String errorMessage = "Maximum file size exceeded.";
	            SystemLog("[EpubBook]", errorMessage);
	            checkResultLogger.log(
	            		GenericLogLevel.Error,errorMessage,
	            		EpubBook.class.getName());
	            checkResultLogger.uploadLog();
	            message=errorMessage;
//	            notifyAPI(false,errorMessage);
	            return;
	        }
	        checkEPubToEc2();
	        getBookSource();
	        EpubBookStandAlone processor=new EpubBookStandAlone();
	        processor.setup(sourcePath, requestData, properties, checkResultLogger);
	        result=processor.convert();
	        mediaType = new HashSet<String>(processor.getMediaType());
//	        message = processor.epubCheckResult.getMessages();
	        consistencyResult=processor.consistencyResult;
	        epubCheckResult=processor.epubCheckResult;
	        uploadEpub(epubFolder);
	        
            // 檢查試閱轉檔檔案大小
            // targetBucket, rootFolder + PATHSEP + fileName
            long trialSize = calculateTotalSize();
            long srcBookSize = getSrcBookSize();
            long trialSizeMB = (trialSize / 1000 / 1000);
            long srcBookSizeMB = (srcBookSize / 1000 / 1000);
            BigDecimal src30pctBookSizeMB = new BigDecimal(srcBookSizeMB);
            src30pctBookSizeMB.setScale(0, BigDecimal.ROUND_UP);
            src30pctBookSizeMB =  src30pctBookSizeMB.multiply(new BigDecimal(0.3));
            
            logger.info("srcBookSize:" + srcBookSizeMB + " ,trialSize:" + trialSizeMB);
            consistencyResult.addMessage(GenericLogLevel.Info, "試閱轉檔資料夾大小:" + trialSizeMB + "MB,來源檔案epub大小:" + srcBookSizeMB+"MB", EpubBook.class.getName());
            if (result && requestData.getIsTrial() && requestData.getPreview_type().equalsIgnoreCase("1")) {
                if ("reflowable".equalsIgnoreCase(requestData.getFormat())) {
//                    if (trialSizeMB >= (srcBookSizeMB / 2)) {
//                        logger.info("trialSizeMB:" + trialSizeMB + " OverSize 50% :" + (srcBookSizeMB / 2));
//                        consistencyResult.addMessage(GenericLogLevel.Warn, "[警告]reflowable試閱轉檔大小超過正式檔案50%,試閱大小:" + trialSizeMB + "MB,來源檔案大小:" + srcBookSizeMB+"MB", EpubBook.class.getName());
//                    }
                } else {
                    if (trialSizeMB >= src30pctBookSizeMB.intValue()) {
                        logger.info("trialSizeMB:" + trialSizeMB + " OverSize 30% :" + src30pctBookSizeMB.intValue());
                        consistencyResult.addMessage(GenericLogLevel.Warn, "[警告]fixedlayout試閱轉檔大小超過正式檔案30%,試閱:" + trialSizeMB + "MB,來源檔案"+srcBookSizeMB+",30%:" + src30pctBookSizeMB.intValue()+"MB", EpubBook.class.getName());
                    }
                }
            }
	        
	        isFixedlayout ="fixedlayout".equals(requestData.getFormat()); 
	        logger.debug("try to add Thumbnail Job, isFixedlayout = " + isFixedlayout +  " result:" + result);
	        
	        boolean isIncludeVedio = false;
			for (String extension : mediaType) {
				if ("mp4".equalsIgnoreCase(extension)) {
					isIncludeVedio = true;
					break;
				}
			}

			if (isIncludeVedio) {
				notifyAPI(result, message);
			}

			if (result && isFixedlayout &&!isIncludeVedio) {
//	        	notifyAPI(result,message);
				addThumbnailJob();
			}

    	}catch (ServerException e)
    	{
    		logger.error("ServerException ",e);
    		result=false;
    		message=e.getError_message();
    		if (consistencyResult==null)
    		{
    			consistencyResult=new CheckResult();
    		}
    		consistencyResult.addMessage(GenericLogLevel.Fatal,message,EpubBook.class.getName());

    	}catch (JobRetryException e)
    	{
    		inRetry=true;
    		throw e;
    	}
    	catch (Exception e)
    	{
    		logger.error("Unexcepted Error:",e);
    		result=false;
    		message="Unexcepted Error "+ e.getMessage();
    		if (consistencyResult==null)
    		{
    			consistencyResult=new CheckResult();
    		}
    		consistencyResult.addMessage(GenericLogLevel.Fatal,message,EpubBook.class.getName());
    	}
        finally{
	        checkResultLogger.addAll(consistencyResult);
	        checkResultLogger.addAll(epubCheckResult);
	        checkResultLogger.uploadLog();
	        Utility.deleteDir(epubFolder);
	        //whend retry, skip notify api
	        if (!inRetry && !(isFixedlayout && result)){
	        	notifyAPI(result,message);
	        }
        }
    }
    
		



	private void addThumbnailJob() throws ServerException, IOException {
        String tag = "[FixedlayoutThumbnail]";
        Long startTime = System.currentTimeMillis();
        
        SystemLog(tag, "add fixedlayout Thumbnail Job");
        
        DataMessage tmpDataMessage = new DataMessage();
        tmpDataMessage.setTargetBucket(targetBucket);
        tmpDataMessage.setFolder(rootFolder);
        tmpDataMessage.setBookFileName(srcFileName);
        tmpDataMessage.setChecksum(checkSum);
        tmpDataMessage.setLogFolder(getLogFolder());
        SystemLog(tag,"dataMessage.getCmsToken()" + tmpDataMessage.getCmsToken());
        tmpDataMessage.setCmsToken(dataMessage.getCmsToken());
        tmpDataMessage.setBookUniId(dataMessage.getBookUniId());
        tmpDataMessage.setMedia(mediaType);
        
        Gson gson = new GsonBuilder().create();
        String data = gson.toJson(requestData);
        tmpDataMessage.setRequestData(data);

        ScheduleJob sj =new ScheduleJob();
        sj.setJobParam(tmpDataMessage);
        sj.setJobGroup(requestData.getBook_file_id()); 
        sj.setJobName("FixedlayoutThumbnail");
        sj.setJobRunner(1);
//        sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
        ScheduleJobManager.addJob(sj);
        
        //delete file without blocker (sourcePath =null => Exception.)
        try {
	        File bookFile = new File(sourcePath);
	        if (bookFile.exists() && !bookFile.delete())
	        {
	        	bookFile.deleteOnExit();
	}
	    } catch (Exception e){
	    	logger.info(logTag + " Delete file fail" + sourcePath + e.getMessage());
        }

        ConvertingLogger.log(GenericLogLevel.Info,
        		"RequestData: " + gson.toJson(requestData) + "\r\n" +
                "DataMessage: " + gson.toJson(tmpDataMessage) + "\r\n" +
                "End triggerLambda() spend " + 
                (System.currentTimeMillis() - startTime) + "ms",
                ConvertingBook.class.getName());
        
        ConvertingLogger.uploadLog();
        SystemLog(tag, "DataMessage: " + gson.toJson(tmpDataMessage));
		
	}

	private void checkEPubToEc2() throws JobRetryException{        
        S3Bucket = srcBucket;
        S3Key = srcFolder + "/" + srcFileName;
        String fileName = srcFileName;
        logger.debug("S3 get file:" + S3Bucket + ":" + S3Key);
        ObjectMetadata meta = s3Client.getObjectMetadata(S3Bucket, S3Key);
        long size = meta.getInstanceLength();
        
        if (bookFormat == BookFormat.Epub && runMode.equals(String.valueOf(RunMode.LambdaMode))) {
        	
            if (size > (lambdaSizeLimitMB*1024*1024)) { // 80 MB
            	String message=fileName + " epub size too big, change to EC2 mode ";
                SystemLog(logTag, message);
                throw new JobRetryException(message);
            }
        }
        
    }
    
    private void uploadEpub(File dir) throws Exception {
    	logger.info("Uploading to S3:" + dir.getName());
        if (dir.isDirectory()) {
            String[] child = dir.list();
            for (int i = 0; i < child.length; i++) {
                uploadEpub(new File(dir, child[i]));
            }
        } else {
            String fileName = dir.getPath().replace(epubFolderPath, "");
            String extension = "";
            if(fileName.contains(".")) {
                extension = fileName.substring(fileName.lastIndexOf('.'));
            }
            MimeType mimeType = (MimeType.fromExtension(extension) == null) 
                    ? MimeType.fromExtension(".txt") : MimeType.fromExtension(extension);
                    
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentType(mimeType.getType());
            PutObjectRequest request = new PutObjectRequest(targetBucket, rootFolder + PATHSEP + fileName, dir)
                    .withCannedAcl(Permission);
            request.setMetadata(meta);
            s3Client.putObject(request);
        }
    }
    
    
    private Long calculateTotalSize() {
        
        Long totalSize = 0L;
        Integer totalItems = 0;
        ObjectListing objects = s3Client.listObjects(targetBucket, rootFolder + "/");
        do {
            for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
                if (objectSummary.getKey().toLowerCase().contains("/thumbnail/")) {
                    continue;
                }
                totalSize += objectSummary.getSize();
                totalItems++;
            }
            objects = s3Client.listNextBatchOfObjects(objects);
        } while (!objects.getObjectSummaries().isEmpty());
        logger.info("check trial size:"  + rootFolder + "/ totalSize:" + totalSize + ", totalItems:" + totalItems);
        return totalSize;
    }
    
 
}
