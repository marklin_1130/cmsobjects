package digipages.BookConvert.BookMain;

import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.PDDestinationNameTreeNode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDNameTreeNode;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.action.PDAction;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDNamedDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.text.PDFTextStripper;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.TocAndOpf.TocAndOpfHandler;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.GenericLogLevel;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.Utility;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

/**
 * PDF book converter.
 * @author Eric.CL.Chou
 */
public class PdfBook extends ConvertingBook {
    private Boolean isReduce; // isTrial && preview type!=3
    static DPLogger logger = DPLogger.getLogger(PdfBook.class.getName());
    private static final String TAG = "[generatePdfTrial]";
    
    public PdfBook(AmazonS3 s3Client, RequestData requestData, Properties config,DataMessage datamessage) throws Exception {
        super(s3Client, requestData,config, datamessage);
        isReduce = requestData.getIsTrial() && !"3".equals(requestData.getPreview_type());
    }
    
    @Override
    /**
     * checkBookSize, 
     * to EPub.
     * do Trial Book
     * calc CheckSum
     * send TocAndOpf
     */
	public void convert() throws Exception {
		super.convert();
		
		CheckResult consistencyResult=new CheckResult();
		boolean inRetry=false;
		boolean success=false;
		try {
			if (!checkBookSize()) {
				String errorMessage = "Maximum file size exceeded.";
				notifyAPI(false, errorMessage);
				return;
			}
			getBookSource();
			checkPDF();
			genEpubDir4pdf();

			if (isReduce) {
				generatePdfTrial();
			} else {
				getPdfCheckSum();
			}

			scheduleTocAndOpf();
		} catch (ServerException e) {
			logger.error("ServerException ", e);
			consistencyResult.setResult(false);
			consistencyResult.addMessage(GenericLogLevel.Fatal, e.getError_message(), PdfBook.class.getName());
			throw e;
		} catch (JobRetryException e) {
			inRetry=true;
			throw e;
		} catch (Exception e) {
			logger.error("Unexcepted Error:", e);
			consistencyResult.setResult(false);
			consistencyResult.addMessage(GenericLogLevel.Fatal, e.getMessage(), PdfBook.class.getName());
		} finally {
			checkResultLogger.addAll(consistencyResult);
			checkResultLogger.uploadLog();
		}
	}
    
    private void checkPDF() throws ServerException, IOException, InterruptedException {
		String cmd = "python dumppdf.py -T " + sourcePath;
		logger.debug("Starting dumpPDF: cmd=" + cmd);
		// Run PDFminer
		Process p = Runtime.getRuntime().exec(cmd);
//		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
		BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream(), "UTF-8"));

		InputStream stdout = p.getInputStream();
        while( stdout.read() >= 0 ) { ; }
        p.waitFor();
        
		String errorStr =errorReader.readLine(); 
		if (null!=errorStr && !errorStr.isEmpty())
		{
			checkResultLogger.warn("Parsing index has Error: 無法轉檔成功" + errorStr);
//			throw new ServerException("id_err_305","Error parsing Index of pdf, message:" + errorStr);
		}
		stdout.close();
		errorReader.close();
		
	}

	private void scheduleTocAndOpf() throws Exception {
        String logTag = "[triggerLambda]";
        Long startTime = System.currentTimeMillis();

        DataMessage tmpDataMessage = new DataMessage();
        tmpDataMessage.setTargetBucket(targetBucket);
        tmpDataMessage.setFolder(rootFolder);
        tmpDataMessage.setBookFileName(srcFileName);
        tmpDataMessage.setChecksum(checkSum);
        tmpDataMessage.setLogFolder(getLogFolder());
        tmpDataMessage.setResult(true);
        tmpDataMessage.setCmsToken(dataMessage.getCmsToken());
        tmpDataMessage.setBookUniId
        (dataMessage.getBookUniId().replaceAll("pdf", "fixedlayout"));
        Gson gson = new GsonBuilder().create();
        String data = gson.toJson(requestData);
        tmpDataMessage.setRequestData(data);
        
    	ScheduleJob sj = new ScheduleJob();
    	sj.setJobParam(tmpDataMessage);
    	sj.setJobName(TocAndOpfHandler.class.getName());
    	sj.setJobGroup(requestData.getBook_file_id()); 
//    	sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
		ScheduleJobManager.addJob(sj);
		
        //delete file without blocker (sourcePath =null => Exception.)
        try {
	        File bookFile = new File(sourcePath);
	        bookFile.delete();
        } catch (Exception e)
        {
        	logger.debug("Fail Delete file:" + sourcePath,e);
        }
        
        ConvertingLogger.log(GenericLogLevel.Info,
        		"RequestData: " + gson.toJson(requestData) + "\r\n" +
                "DataMessage: " + gson.toJson(dataMessage) + "\r\n" +
                "End ScheduleJob-TocAndOpf spend " + (System.currentTimeMillis() - startTime) + "ms",
                PdfBook.class.getName());
        
        ConvertingLogger.uploadLog();
        SystemLog(logTag, "DataMessage: " + gson.toJson(dataMessage));
		
	}


    


    
    private void genEpubDir4pdf() throws Exception {
        String Tag = "[genEpubDir4pdf]";
        Long StartTime = System.currentTimeMillis();
        ConvertingLogger.log(Tag, "Start genEpubDir4pdf()");
        logger.debug(Tag+"targetBucket:"+ targetBucket + "\tPath=" + rootFolder);
        // Create folder into tempBucket
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        PutObjectRequest putObjectRequest = new PutObjectRequest(targetBucket, rootFolder + "/"
                , emptyContent, metadata).withCannedAcl(Permission);
        s3Client.putObject(putObjectRequest);

        putObjectRequest = new PutObjectRequest(targetBucket, rootFolder + "/META-INF/"
                , emptyContent, metadata);
        s3Client.putObject(putObjectRequest);

        putObjectRequest = new PutObjectRequest(targetBucket, rootFolder + "/THUMBNAIL/"
                , emptyContent, metadata);
        s3Client.putObject(putObjectRequest);

        putObjectRequest = new PutObjectRequest(targetBucket, rootFolder + "/PDF/"
                , emptyContent, metadata);
        s3Client.putObject(putObjectRequest);

        putObjectRequest = new PutObjectRequest(targetBucket, rootFolder + "/OEBPS/"
                , emptyContent, metadata);
        s3Client.putObject(putObjectRequest);
        
        emptyContent.close();
        
        String folderPath;
        String fileName;
        
        // Upload PDF file
        if (!isReduce) {
            fileName = srcFileName;
            folderPath = "PDF/";
            
            File bookFile = new File(sourcePath);
            putObjectRequest = new PutObjectRequest(targetBucket, rootFolder + "/" + folderPath + fileName, bookFile)
                    .withCannedAcl(Permission);
            s3Client.putObject(putObjectRequest);
        }
        
        // Create mimetype file
        folderPath = "";
        fileName = "mimetype";
        ClassLoader classLoader = getClass().getClassLoader();
        
        InputStream tmpInputStream =classLoader.getResourceAsStream(fileName);
        logger.warn("mimetype file: " + fileName);
        s3Client.putObject(new PutObjectRequest(targetBucket, rootFolder + "/" + folderPath + fileName, tmpInputStream,null)
                .withCannedAcl(Permission));
        tmpInputStream.close();
        

        // Create container.xml
        folderPath = "META-INF/";
        fileName = "container.xml";
        tmpInputStream =classLoader.getResourceAsStream(fileName);
        s3Client.putObject(new PutObjectRequest(targetBucket, rootFolder + "/" + folderPath + fileName, tmpInputStream,null)
                .withCannedAcl(Permission));
        tmpInputStream.close();
        
        // Create cover.html
        /*
        if(!requestData.getEfile_cover_name().isEmpty()) {
            folderPath = "OEBPS/";
            fileName = "cover.html";
            temp = new File(classLoader.getResource(fileName).getFile());
            s3Client.putObject(targetBucket, rootFolder + "/" + folderPath + fileName, temp);
            generateCoverThumbnail();
            temp.delete();
        }
        */
        
        ConvertingLogger.log(GenericLogLevel.Info,
        		"End genEpubDir4pdf() spend " + 
        (System.currentTimeMillis() - StartTime) + "ms",
        PdfBook.class.getName());
    }

    private void generatePdfTrial() throws Exception {
        Long StartTime = System.currentTimeMillis();
        ConvertingLogger.log(TAG, "Start generatePdfTrial()");
        
        String folderPath = "PDF/";
        String fileName = srcFileName;
        
        File bookFile = new File(sourcePath);
        File targetPDF;
        PDDocument sourceDocument = PDDocument.load(bookFile);
        
        switch(requestData.getPreview_type()) {
            case "1":
                String percentage = requestData.getPreview_content().replace("%", "");
                if(Integer.valueOf(percentage)<=0) {
                    throw new ServerException("id_err_305","試閱本組裝有問題 percentage <=0.");
                }
                
                int totalPage= sourceDocument.getNumberOfPages();
                //#69 試閱轉檔邏輯變更 fixedlayout 
                ConvertingLogger.log(TAG, "試閱比例: percentage:" + percentage);
                ConvertingLogger.log(TAG, "總頁數: totalPage:" + totalPage);
                int trialPage = 0 ;
                Double trialPageMath = Math.floor( new Double(totalPage * Integer.valueOf(percentage)) / 100 );
                ConvertingLogger.log(TAG, "試閱比例計算: trialPage:" + trialPageMath);
                if(trialPageMath<1D) {
                    trialPage = 1;
                }else {
                    trialPage = trialPageMath.intValue();
                }
                //加上封面頁數+1
                trialPage++;
                String range = "1-" + String.valueOf(trialPage);
                ConvertingLogger.log(TAG, "試閱比例: trialPage:" + range);
                targetPDF = cutPdf(sourceDocument, range);
                break;
            case "2":
                targetPDF = cutPdf(sourceDocument, requestData.getPreview_content());
                break;
            default:
                targetPDF = new File("/tmp/targetPDF.pdf");
                sourceDocument.save(targetPDF);
                break;
        }
//        checkTargetPDFPage(targetPDF);
        s3Client.putObject(new PutObjectRequest(targetBucket, rootFolder + "/" + folderPath + fileName, targetPDF)
                .withCannedAcl(Permission));
        
        sourceDocument.close();    
        targetPDF.delete();
        
        ConvertingLogger.log(GenericLogLevel.Info,
        		"End generatePdfTrial() spend " + 
        (System.currentTimeMillis() - StartTime) + "ms",
        PdfBook.class.getName());
    }
    
    private void checkTargetPDFPage(File targetPDF) throws ServerException, InvalidPasswordException, IOException{
    	
		PDDocument doc = PDDocument.load(targetPDF);
		if (doc.getNumberOfPages()<3)
		{
			throw new ServerException("id_err_305","試閱本組裝有問題,NumberOfPages:"+doc.getNumberOfPages());
		}
		doc.close();
	}
    
    private int getImagesFromResources(PDResources resources , int imgCount) throws IOException {
        
        for (COSName xObjectName : resources.getXObjectNames()) {
            PDXObject xObject = resources.getXObject(xObjectName);
            try {
                if (xObject instanceof PDFormXObject) {
                    getImagesFromResources(((PDFormXObject) xObject).getResources(),imgCount);
                } else if (xObject instanceof PDImageXObject) {
                    imgCount++;
                }
            } catch (Exception e) {
                //有些圖片需要第三方程式庫，但由於第三方授權為GPL暫不考慮使用，判斷不能解析就當作圖片
                return imgCount++;
            }
        }
        
        return imgCount;
    }

	/**
     * 
     * @param sourceDocument
     * @param Ranges 1-5;10-20
     * @return
     * @throws Exception
     */
    private File cutPdf(PDDocument sourceDocument, String Ranges) throws Exception {
        PDDocument targetDocument = new PDDocument();
        String[] range = Ranges.split(";");
        
        // Cover should be included into trial book. 
        if (range[0].contains("-")) {
            int start = Integer.valueOf((range[0].split("-"))[0]);
            
            if(start > 1) {
                PDPage tempPage = sourceDocument.getPage(0);
                targetDocument.addPage(tempPage);
            }
        } else {
            if ((Integer.valueOf(range[0]) - 1) != 0) {
                PDPage tempPage = sourceDocument.getPage(0);
                targetDocument.addPage(tempPage);
            }
        }
        
        int invaildPageCount = 0;
        int lastPageCount=0;
        int imgCount = 0;
        for(int i = 0; i < range.length; i++) {
            if(range[i].contains("-")) {
                int start = Integer.valueOf((range[i].split("-"))[0]);
                int end = Integer.valueOf((range[i].split("-"))[1]);
                
                for(int j = start; j < (end + 1); j++) {
                    PDPage tempPage = sourceDocument.getPage(j-1);
                    boolean isHaveImage = false;
                    boolean isHaveText = false;
                    PDResources pdResources = tempPage.getResources();
                    if (pdResources != null) {
                        try {
                            imgCount = getImagesFromResources(pdResources,imgCount);
                        } catch (Exception e) {
                            imgCount++;
                        }
                        if (imgCount > 0) {
                            isHaveImage = true;
                        }
                    }
                    PDFTextStripper reader = new PDFTextStripper();
                    reader.setStartPage(j-1);
                    reader.setEndPage(j-1);
                    String pageText = "";
                    try {
                        pageText = reader.getText(sourceDocument);
                        isHaveText = StringUtils.trimToEmpty(pageText).length() > 0;
                    } catch (Exception e) {
                        ConvertingLogger.log(TAG,"文字提取失敗,第" + (j) + "頁");
                        isHaveText = true;
                    }
                    
                    if (!isHaveImage && !isHaveText) {
                        ConvertingLogger.log(TAG,"無效頁,第" + (j) + "頁，判斷不包含圖片及文字: isHaveImage:" + isHaveImage + ",isHaveText:" + isHaveText);
                        invaildPageCount++;
                    }
                    targetDocument.addPage(tempPage);
                    lastPageCount = j-1;
                }                            
            } else {
                PDPage tempPage = sourceDocument.getPage(Integer.valueOf(range[i]) - 1);
                targetDocument.addPage(tempPage);
                lastPageCount = Integer.valueOf(range[i]) - 1;
            }
        }
        ConvertingLogger.log(TAG, "無效頁總計:"+invaildPageCount);
        ConvertingLogger.log(TAG, "最後一頁為第:"+(lastPageCount+1)+"頁");
        
        // 補上無效頁數差距
        for (int i = 0; i < invaildPageCount; i++) {
            try {
                lastPageCount++;
                ConvertingLogger.log(TAG, "補上第:" + (lastPageCount+1) + "頁");
                PDPage tempPage = sourceDocument.getPage(lastPageCount);
                if (tempPage != null) {
                    targetDocument.addPage(tempPage);
                }
            } catch (Exception e) {
                ConvertingLogger.log(TAG, "補上無效頁數差距失敗:" + e.getMessage());
            }
        }
        
        
        // Add end page (epmty page)
//        ClassLoader classLoader = getClass().getClassLoader();
//        InputStream emptyPageFile = classLoader.getResourceAsStream("samplefinished.pdf");
//        PDDocument emptyPageDocument = PDDocument.load(emptyPageFile);
//        PDPage emptyPage = emptyPageDocument.getPage(0);
//        targetDocument.addPage(emptyPage);
//        emptyPageFile.close();
        
        // Process TOC
        PDDocumentCatalog srcCatalog = sourceDocument.getDocumentCatalog();
        PDDocumentOutline srcOutline = srcCatalog.getDocumentOutline();
        PDDocumentNameDictionary srcNames = sourceDocument.getDocumentCatalog().getNames();
        targetDocument.getDocumentCatalog().setDocumentOutline(srcOutline);
        targetDocument.getDocumentCatalog().setNames(srcNames);
        PDDocumentOutline targetOutline = targetDocument.getDocumentCatalog().getDocumentOutline();
        
        if (targetOutline != null) {
            PDOutlineItem rootItem = targetOutline.getFirstChild();
            
            processingToc(targetDocument, rootItem, range);
        }
        
        File targetPDF = new File("/tmp/targetPDF.pdf");
        targetDocument.save(targetPDF);
        
        targetDocument.close();
//        emptyPageDocument.close();
        
        
        return targetPDF;
    }
    
    private void processingToc(PDDocument targetDocument, PDOutlineItem item, String[] range) {
        while(item != null) {
            PDDestination dest =null;
            PDAction pdAction = null;
            try {                
                dest = getDest(item);
                pdAction = item.getAction();
                
                if (dest != null) {                    
                    if (dest instanceof PDNamedDestination) {
                        String namedDest = ((PDNamedDestination)dest).getNamedDestination();
                        processingNamedDestination(targetDocument, namedDest, range);
                    } else if (dest instanceof PDPageDestination) {
                        int pageNumber = ((PDPageDestination)dest).retrievePageNumber();
                        
                        if (!inRange(pageNumber, range)) {
                            int finalPage = targetDocument.getNumberOfPages() -1;
                            ((PDPageDestination)dest).setPageNumber(finalPage);
                        }
                    }
                } else if (pdAction != null) {                    
                    if (pdAction instanceof PDActionGoTo) {
                        PDDestination dest_pdAction = ((PDActionGoTo) pdAction).getDestination();
                        
                        if (dest_pdAction instanceof PDNamedDestination) {
                            String namedDest = ((PDNamedDestination)dest_pdAction).getNamedDestination();
                            processingNamedDestination(targetDocument, namedDest, range);
                        } else if (dest_pdAction instanceof PDPageDestination) {
                            int pageNumber = ((PDPageDestination)dest_pdAction).retrievePageNumber();
                            
                            if (!inRange(pageNumber, range)) {
                                int finalPage = targetDocument.getNumberOfPages() -1;
                                ((PDPageDestination)dest_pdAction).setPageNumber(finalPage);
                            }
                        }
                    }
                    
                }            
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            PDOutlineItem child = item.getFirstChild();
            processingToc(targetDocument, child, range);
            
            item = item.getNextSibling();
        }
    }
    
    private PDDestination getDest(PDOutlineItem item) {
		try {
			return item.getDestination();
		}catch (Exception e)
		{
			// nothing to worry about. (Destion format error is ignored)
		}
		return null;
	}

	private void processingNamedDestination(PDDocument document, String namedDest, String[] range) throws Exception {
        PDDocumentNameDictionary names = document.getDocumentCatalog().getNames();
        if (names==null){
        	return;
        }
        PDDestinationNameTreeNode dests = names.getDests();
        List<PDNameTreeNode<PDPageDestination>> destList = dests.getKids();
        
        for(int i = 0; i < destList.size(); i++) {
            Map<String, PDPageDestination> destMap = destList.get(i).getNames();
            PDPageDestination pageDestination = destMap.get(namedDest);
            
            if (pageDestination != null) {
                int pageNum = pageDestination.retrievePageNumber();
                
                if(!inRange(pageNum, range)) {
                    int finalPage = document.getNumberOfPages() - 1;
                    pageDestination.setPageNumber(finalPage);
                }
            }
        }
    }
    
    private Boolean inRange(int num, String[] range) {
        Boolean result = false;
        
        for(int i = 0; i < range.length; i++) {
            if(range[i].contains("-")) {
                int start = Integer.valueOf((range[i].split("-"))[0]) - 1;
                int end = Integer.valueOf((range[i].split("-"))[1]) - 1;
                
                if(num >= start &&  num <= end) {
                    result = true;
                }
            } else {
                int page = Integer.valueOf(range[i]) - 1;
                
                if(num == page) {
                    result = true;
                }
            }
        }
        
        return result;
    }
    
    private void getPdfCheckSum() throws Exception {
        String Tag = "[getPdfCheckSum]";
        Long StartTime = System.currentTimeMillis();
        ConvertingLogger.log(Tag, "Start getPdfCheckSum()");
        
        File bookFile = new File(sourcePath);
        InputStream objectData = new FileInputStream(bookFile);
        MessageDigest sha = MessageDigest.getInstance("SHA-256"); 
        
        byte[] content = new byte[1024];

        while (objectData.read(content) != -1) {
            sha.update(content);
        }
        
        objectData.close();
        
        checkSum = Utility.byte2hex(sha.digest());
        
        ConvertingLogger.log(GenericLogLevel.Info,
        		"CheckSum: " + checkSum + "\r\n" +
                "End getPdfCheckSum() spend " + 
        				(System.currentTimeMillis() - StartTime) + "ms",
        		PdfBook.class.getName());
    }
    
	public static int countPageElements(PDPage tempPage) {
		Set<Entry<COSName, COSBase>> entries=tempPage.getCOSObject().entrySet();
		int tmpCnt=0;
		for (Entry<COSName, COSBase> entry:entries)
		{
			tmpCnt+=countChildElements(entry.getValue());
		}
		return tmpCnt;
	}

	private static int countChildElements(COSBase value) {
		int cnt=1; // me = 1
		if (value instanceof COSArray)
		{
			COSArray x = (COSArray) value;
			Iterator <COSBase> iter=x.iterator();
			while (iter.hasNext())
			{
				COSBase tmpItem = iter.next();
//				logger.info(tmpItem);
				cnt += countChildElements(tmpItem);
			}
		}else if (value instanceof COSDictionary){
			COSDictionary di = (COSDictionary) value;
			for (Entry<COSName, COSBase> childEntry:di.entrySet())
			{
				cnt+=countChildElements(childEntry.getValue());
			}
		}else if (value instanceof COSStream)
		{
			COSStream stream = (COSStream) value;
			for (Entry<COSName, COSBase> childEntry:stream.entrySet())
			{
				cnt+=countChildElements(childEntry.getValue());
			}
		}else
		{
			// element must be COSObject, COSFloat, COSInteger
			if (!(value instanceof COSFloat || 
					value instanceof COSInteger || 
					value instanceof COSName ||
					value instanceof COSObject ||
					value instanceof COSString)){
				logger.info("[checkePDFObjCnt]: Warning, Not correct counted Element:" + value);
			}
		}
		return cnt;
	}

}
