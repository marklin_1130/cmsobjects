package digipages.BookConvert.TocAndOpf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.common.html.HtmlEscapers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.NotifyAPI.NotifyAPIHandler;
import digipages.BookConvert.PDF2ePub.PDF2ePubHandler;
import digipages.BookConvert.utility.CommonUtil;
import digipages.BookConvert.utility.DataMessage;
import digipages.BookConvert.utility.LogToS3;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.Utility;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.CommonJobHandler.ScheduleJobManager;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

/**
 * PDF Toc and Opf
 * @author Eric.CL.Chou
 *
 */
public class TocAndOpfHandler implements CommonJobHandlerInterface{

	private AmazonS3 s3Client;

	private DataMessage dataMessage;
	private RequestData requestData;


	private CannedAccessControlList s3Permission;
	private static final String TAG = "[TocAndOpf]";
	private int totalPage;
	private File pdfFile;
	private File tempFile;
	private OutputStream out;
	private Process p;
	private boolean hasToc;
	private Long jobStartTime;
	private static Gson gson = new GsonBuilder().create();
	private Properties config;
	private String rootFolder;
	private String targetBucket;
	private String pdfFileName;

	private LogToS3 checkResultLogger;

	private String originalBucket;

	private String checkResultgPath;

	private Object logFolder;
	private static final DPLogger logger = DPLogger.getLogger(TocAndOpfHandler.class.getName());


	@Override
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {

		jobStartTime = System.currentTimeMillis();

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
		logger.info(TAG + "Start TocAndOpf at " + sdFormat.format(new Date()));

		try {
			CommonUtil.delDirs("/tmp");
			// Get SNS message
			
			
			dataMessage = job.getJobParam(DataMessage.class); 
			requestData = gson.fromJson(dataMessage.getRequestData(), RequestData.class);

			this.config =config;

			logger.info(TAG + "message: " + gson.toJson(dataMessage));

			initEnv();
			pdfMain();
			schedulePDF2ePub(dataMessage,requestData);
			

		} catch (Throwable e) {
			e.printStackTrace();

			// String errorMessage = e.getClass().getName() + ": " +
			// e.getMessage();
			String errorMessage = ExceptionUtils.getStackTrace(e);
			if (e instanceof java.io.IOException 
					&& e.getMessage().contains("Wrong type of referenced length object COSObject")) {
				errorMessage = "Non-parseable PDF format, please re-save by Adobe Reader to fix this problem.";
			}else if (e instanceof ServerException)
			{
				errorMessage = ((ServerException) e).getError_message();
			} else if (e.getMessage().contains("No Space Left on Device")) {
				errorMessage = "No Space Left on Device. Please check book file size. After unzip, it may exceed size limitation."
						+ "\r\n" + errorMessage;
			}else
			{
				errorMessage = "檔案打不開 Unexcepted Exception parsing file" + e.getMessage();
			}

			
			dataMessage.setResult(false);
			dataMessage.appendErrorMessage(errorMessage);
			checkResultLogger.error(errorMessage);
			
			ScheduleJob sj = new ScheduleJob();
			sj.setJobName(NotifyAPIHandler.class.getName());
			sj.setJobRunner(1);
			sj.setJobParam(dataMessage);
			sj.setJobGroup(requestData.getBook_file_id());
//			sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
			try {
				ScheduleJobManager.addJob(sj);
			} catch (Exception e1) {
				e1.printStackTrace();
				logger.error(TAG + " Fail to send job.",e1);
			}
		}
		checkResultLogger.uploadLog();

		logger.info(TAG + "End TocAndOpf spend " + (System.currentTimeMillis() - jobStartTime) + "ms, at "
				+ sdFormat.format(new Date()));

		return "TocAndOpfHandler Finished.";
	}
	


	private void schedulePDF2ePub(DataMessage message, RequestData requestData) throws ServerException,IOException {
		ScheduleJob sj = new ScheduleJob();
		sj.setJobName(PDF2ePubHandler.class.getName());
		sj.setJobRunner(RunMode.ExtEc2Mode.getValue());
		sj.setJobParam(message);
		sj.setJobGroup(requestData.getBook_file_id());
		logger.debug("TocOpfHandler sending Job (PDF2ePubHandler)" + sj.toGsonStr());
//		sj.setShouldFinishTime(CommonUtil.nextNMinutes(30));
		ScheduleJobManager.addJob(sj);
	}



	private void pdfMain() throws Throwable {
		// PDF Info
		rootFolder = dataMessage.getFolder();
		pdfFileName = dataMessage.getBookFileName();
		targetBucket = dataMessage.getTargetBucket();
		logger.info(TAG + "rootFolder: " + rootFolder + ", pdfFileName: " + pdfFileName + ", targetBucket: "
				+ targetBucket);
		String key = rootFolder + "/PDF/" + pdfFileName;
		String localPDFPath = "/tmp/source_TocAndOpf_" + requestData.getBook_file_id() + ".pdf";

		// PDF Download
		pdfFile = Utility.downloadFromS3(s3Client, targetBucket, key, localPDFPath);

//		String cmd = "python dumppdf.py -T " + pdfFile.getAbsolutePath();
//		logger.debug("Starting dumpPDF: cmd=" + cmd);
		// Run PDFminer
//		p = Runtime.getRuntime().exec(cmd);
//		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
		

		ArrayList<ArrayList<String>> navMap=parsePDFTocUseingPDFBOX(pdfFile);
		if (!navMap.isEmpty()){
		    hasToc = true;
//			logger.info(TAG + "toc reader is not null");
			// Parse PDFminer output
//			navMap=parsePDFToc(reader);
			
		}else{
			hasToc = false;
//			logger.info(TAG + "toc reader is null");
			navMap=genDefaultNavMap(pdfFile);
		}
		generateToc(navMap);
		
//		boolean success=p.waitFor(30,TimeUnit.MINUTES);
//		if (!success)
//			throw new ServerException("id_err_305","fail to dump structure of pdf.");
//		p.getErrorStream().close();
//		p.getOutputStream().close();
//		p.getInputStream().close();
		generateOpf();
		
		uploadTemplate();
//		reader.close();
	}

	private ArrayList<ArrayList<String>> genDefaultNavMap(File pdfFile2) throws InvalidPasswordException, IOException {
		PDDocument doc = PDDocument.load(pdfFile2);
		ArrayList<ArrayList<String>> ret = new ArrayList<>();
		for (int i=1;i<= doc.getNumberOfPages();i++)
		{
			ArrayList<String> pageData = new ArrayList<>();
			String pageNo=CommonUtil.paddingLeft(String.valueOf(i), 4, '0');
			pageData.add(pageNo); // text
			pageData.add("page_"+pageNo+".xhtml"); // url
			ret.add(pageData);
		}
		doc.close();
		return ret;
	}



	// copy local resource (FixedLayoutTemplate) to S3 location
	private void uploadTemplate() throws IOException, URISyntaxException {
		
		/**
		 PutObjectRequest putObjectRequest = new PutObjectRequest(targetBucket, rootFolder + "/OEBPS/content.opf",
				tempFile).withCannedAcl(s3Permission);
		s3Client.putObject(putObjectRequest);
		 */
		File sourceDir = new File("/tmp/template/");
		copyResourceToTmpDir("FixedLayoutTemplate",sourceDir);
		String tmpS3Loc = dataMessage.getTargetBucket()+ "/" + dataMessage.getFolder(); 
		CommonUtil.uploadToS3(s3Client, sourceDir, tmpS3Loc, s3Permission, "");
		CommonUtil.delDirs(sourceDir);
	}


	/**
	 * 
	 * @param string
	 * @param sourceDir
	 * @throws IOException 
	 */
	public static void copyResourceToTmpDir(String baseURI, File sourceDir) throws IOException {
		
		if (!sourceDir.exists())
			sourceDir.mkdirs();
		// mimetype
		CommonUtil.copyFromResource(baseURI + "/mimetype", sourceDir);
		// MEAT-INF
		File metaDir=new File(sourceDir + "/META-INF");
		metaDir.mkdirs();
		CommonUtil.copyFromResource(baseURI + "/META-INF/container.xml", metaDir);
		CommonUtil.copyFromResource(baseURI + "/META-INF/com.apple.ibooks.display-options.xml", metaDir);
		
		//OEBPS
		File opsDir=new File(sourceDir + "/OEBPS");
		opsDir.mkdirs();
//		CommonUtil.copyFromResource(baseURI + "/OEBPS/package.opf", opsDir);
		
		//book dir
		File bookDir=new File(sourceDir + "/OEBPS/book");
		bookDir.mkdirs();
		
		// image dir
		File imagesDir=new File(sourceDir + "/OEBPS/images");
		imagesDir.mkdirs();
		
	}

	/**
	 * only for test, copy Resource
	 * @param args
	 */
	public static void main(String []args){
		try {
			copyResourceToTmpDir("FixedLayoutTemplate",new File("/tmp/123334"));
		} catch (IOException e) {
			logger.error("Unexcepted error",e);
		}
	}


	private void generateOpf() throws IOException {
		/* [+] Generate package.opf */
		logger.debug(TAG + "Start generating content.opf");
		
		List<String> contentList = new ArrayList<String>();

		contentList.add("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		contentList.add("<package version=\"3.0\" xmlns=\"http://www.idpf.org/2007/opf\" unique-identifier='bookid'>");
		
		// <metadata>
		contentList.addAll(generateMetaData());
		
		// <manifest>
		contentList.addAll(generateManifest());
		
		
		// <spine>
		// TODO add spine direction: (rtl/ltr/default) 
		String pageProgressionDirection = genDirection(requestData); 
		contentList.add("  <spine page-progression-direction=\""+pageProgressionDirection+"\">");
		contentList.addAll(generateSpine());
		contentList.add("  </spine>");
		contentList.add("</package>");

		tempFile = File.createTempFile("package", ".opf");
		out = new FileOutputStream(tempFile);
		for (String content : contentList) {
			byte[] contentInBytes = (content + "\r\n").getBytes("UTF-8");
			out.write(contentInBytes);
		}

		PutObjectRequest putObjectRequest = new PutObjectRequest(dataMessage.getTargetBucket(), dataMessage.getFolder() + "/OEBPS/package.opf",
				tempFile).withCannedAcl(s3Permission);
		s3Client.putObject(putObjectRequest);

		out.close();
		tempFile.deleteOnExit();
		pdfFile.deleteOnExit();
		/* [-] Generate package.opf */
		
	}

	private String genDirection(RequestData requestData) {
		return requestData.getPageDirection();
	}



	private List<String> generateSpine() throws InvalidPasswordException, IOException {
		List <String> ret = new ArrayList<>();
		PDDocument document = PDDocument.load(pdfFile);
		totalPage = document.getNumberOfPages();
		document.close();
		// htmls
		for (int i = 1; i <= totalPage; i++) {
			String pageNumber=Integer.toString(i);
			pageNumber = CommonUtil.paddingLeft(pageNumber,4,'0');
			ret.add(" <itemref linear=\"yes\" idref=\"book-page"+pageNumber+"\" />");
		}
		return ret;
	}



	private List<String> generateManifest() throws InvalidPasswordException, IOException {
		
		ArrayList<String> contentList=new ArrayList<>();
		contentList.add("  <manifest>");
		
		// DRM in opf
//		if (!requestData.getIsTrial()) {
//			contentList.add("    <item id=\"drm\" href=\"../DRM/drm.xml\" media-type=\"application/xml\"/>");
//		}
		PDDocument pdfDoc = PDDocument.load(pdfFile);
		totalPage = pdfDoc.getNumberOfPages();
		
		contentList.add("<item media-type='application/xhtml+xml' id='book-toc' href='book/table-of-contents.xhtml' properties='nav'/>");

		// htmls
		for (int i = 1; i <= totalPage; i++) {
			String pageNumber=Integer.toString(i);
			pageNumber = CommonUtil.paddingLeft(pageNumber,4,'0');
			contentList.add("    <item media-type='application/xhtml+xml' "
					+ " id='book-page" + pageNumber + "' "
					+ " href='book/page_" + pageNumber + ".xhtml' "
					+ "/>");
		}
		
		// jpgs
		for (int i = 1; i <= totalPage; i++) {
			String pageNumber=Integer.toString(i);
			pageNumber = CommonUtil.paddingLeft(pageNumber,4,'0');
			contentList.add("    <item media-type='image/jpeg' "
					+ " id='page_" + pageNumber + "' "
					+ " href='images/page_" + pageNumber + ".jpg' "
					+ "/>");
		}
		
		// thumbnails
		for (int i = 1; i <= totalPage; i++) {
			String pageNumber=Integer.toString(i);
			pageNumber = CommonUtil.paddingLeft(pageNumber,4,'0');
			
			contentList.add("    <item href=\"../THUMBNAIL/thumbnail" + pageNumber + ".jpg\" id=\"id" + i
					+ "\" media-type=\"image/jpeg\"/>");
		}
		
		
		contentList.add("  </manifest>");
		pdfDoc.close();

		return contentList;
	}


	public static String genMetaItem(String href, String id, String mediaType)
	{
		return 	"    <item id=\""+id+"\" href=\""+href+"\" media-type=\""+mediaType+"\"/>";

	}



	/**
	 * opf meta需要的資訊
	 * 語言dc:language：zh-TW。
	 * 出版品名稱 dc:title：書名，包含副標題。
	 * 創作者 dc:creator：作者姓名。
	 * 出版社dc:publisher：出版社名稱。
	 * 出版日期dc:date：出版日期。
	 * 識別碼dc:identifier：有ISBN或ISSN就帶，若無，則使用隨機生成的唯一用戶ID（UUID）。
	 * 目錄：pdf目錄有編，會有同樣的目錄，並可跳到原本有設定好的頁數。
	 * 翻頁方向 依主檔為主
	 * @return
	 */
	private List<String> generateMetaData() {
		List<String> contentList = new ArrayList<>();

		contentList.add("  <metadata xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:opf=\"http://www.idpf.org/2007/opf\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xml:lang=\"bookid\">");
		contentList.add("    <dc:language>" + "zh-TW" + "</dc:language>");
		contentList.add("    <dc:title>" + requestData.getC_title()  + " " + requestData.getO_title()  + "</dc:title>");
		contentList.add("    <dc:creator opf:role=\"aut\">" + requestData.getAuthor() + "</dc:creator>");
		contentList.add("    <dc:description>" + CommonUtil.stripTextFromPartialHtml(requestData.getIntro())  + "</dc:description>");
		contentList.add("    <dc:publisher>" + requestData.getPublisher_name()  + "</dc:publisher>");
		contentList.add("    <dc:date>" +  CommonUtil.stripTextFromPartialHtml(requestData.getPublish_date()).replaceAll("/", "-")  + "</dc:date>");

		if (requestData.getIsbn() != null && !requestData.getIsbn().isEmpty()) {
			contentList.add("    <dc:identifier id=\"bookid\" opf:scheme=\"ISBN\">urn:isbn:" + requestData.getIsbn() + "</dc:identifier>");
			contentList.add("    <meta refines='#bookid' property='identifier-type' scheme='xsd:string'>isbn</meta>");
		} else if (requestData.getIssn() != null && !requestData.getIssn().isEmpty()) {
			contentList.add("    <dc:identifier id=\"bookid\" opf:scheme=\"ISSN\">urn:issn:" + requestData.getIssn() + "</dc:identifier>");
			contentList.add("    <meta refines='#bookid' property='identifier-type' scheme='xsd:string'>issn</meta>");
		} else {
			String uuid = UUID.randomUUID().toString();
			contentList.add("    <dc:identifier id=\"bookid\" opf:scheme=\"uuid\">urn:uuid:" + uuid + "</dc:identifier>");
			contentList.add("    <meta refines='#bookid' property='identifier-type' scheme='xsd:string'>uuid</meta>");
		}
		// fixedlayout, auto two page
		contentList.add("    <meta property=\"rendition:layout\">pre-paginated</meta>\r\n");
		contentList.add("    <meta property=\"rendition:orientation\">auto</meta>\r\n");
		contentList.add("    <meta property=\"rendition:spread\">auto</meta>\r\n");
		contentList.add("    <meta property=\"dcterms:modified\">"+CommonUtil.fromDateToString(new Date()).substring(0, 19)+"Z"+"</meta>\r\n");
		contentList.add("  </metadata>");

		return contentList;
	}



	// change to toc.xhtml (toc.ncx = epub2 format)
	private void generateToc(ArrayList<ArrayList<String>> navMap) throws IOException {
		logger.info(TAG + "Start generating table-of-contents.xhtml");
		
		// Parse PDFminer output

		tempFile = genTocXhtml(navMap);
		
		logger.debug("uploading  table-of-contents.xhtml to s3");
		String targetS3URI = dataMessage.getTargetBucket()+"/"+ dataMessage.getFolder() + "/OEBPS/book/table-of-contents.xhtml";
		CommonUtil.uploadToS3(s3Client, tempFile, targetS3URI, s3Permission, "");
		
//			PutObjectRequest putObjectRequest = new PutObjectRequest(dataMessage.getTargetBucket(), dataMessage.getFolder() + "/OEBPS/book/table-of-contents.html",
//					tempFile).withCannedAcl(s3Permission);
//			s3Client.putObject(putObjectRequest);

		tempFile.deleteOnExit();
		
	}

	private File genTocXhtml(ArrayList<ArrayList<String>> navMap) throws IOException {
		ArrayList<String> contentList = new ArrayList<String>();

		contentList.add("<?xml version='1.0'?>\r\n" + 
				"<html xmlns='http://www.w3.org/1999/xhtml' xmlns:epub='http://www.idpf.org/2007/ops' xml:lang='en' lang='en'>\r\n" + 
				"	<head>\r\n" + 
				"		<meta charset='utf-8' />\r\n" + 
				"		<title>"+requestData.getC_title()+"</title>\r\n" + 
				"	</head>\r\n" + 
				"	<body>");
		// <navMap>
		contentList.add("  <nav epub:type='toc'>\r\n" + 
				"			<h1>Table of Contents</h1>\r\n"+
		        "<ol>");
		for (ArrayList<String> navPoint : navMap) {
		    
		    String addHrefString = "";
            if (navPoint.size() > 1) {

                if ("".equalsIgnoreCase(navPoint.get(1))) {
                    addHrefString = "href='' data-empty='blank'";
                } else {
                    addHrefString = "href='" + navPoint.get(1) + "'";
                }

            } else {
                addHrefString = "href='' data-empty='blank'";
            }
		    
			contentList.add("" + 
					"                <li>\r\n" + 
					"                    <a "+addHrefString+">" 
										+ HtmlEscapers.htmlEscaper().escape(navPoint.get(0).replaceAll("'", "’")) + "</a>\r\n" + 
					"                </li> \r\n" + 
					"");
		}
		contentList.add("		</ol>\r\n</nav>\r\n" + 
				"	</body>\r\n" + 
				"</html>");
		tempFile = File.createTempFile("table-of-contents", ".xhtml");
		tempFile.deleteOnExit();

		out = new FileOutputStream(tempFile);
		for (String content : contentList) {
			byte[] contentInBytes = (content + "\r\n").getBytes("UTF-8");
			out.write(contentInBytes);
		}
		out.close();

		
		return tempFile;
	}



	/**
	 * parse into navMap
	 * @param reader
	 * @throws IOException
	 */
	private static ArrayList<ArrayList<String>> parsePDFToc(BufferedReader reader) throws IOException {
		String line = "";
		ArrayList<ArrayList<String>> navMap=new ArrayList<>();
		while ((line = reader.readLine()) != null) {
			if (line.contains("<outline level")) {
				ArrayList<String> navPoint = new ArrayList<String>();
				String title;
				Integer pageno;

				String temp = line.substring(line.indexOf("title=\"") + ("title=\"").length());
				title = temp.substring(0, temp.indexOf("\""));
				navPoint.add(title);

				while (!(line = reader.readLine()).contains("</outline>")) {
					if (line.contains("<pageno>")) {
						temp = line.substring(line.indexOf("<pageno>") + ("<pageno>").length());
						pageno = Integer.parseInt(temp.substring(0, temp.indexOf("<")));
						String pageNo=CommonUtil.paddingLeft(String.valueOf(pageno+1), 4, '0');
						String pageHtml = "page_"+pageNo+".xhtml"; 
						navPoint.add(pageHtml);
					}
				}

				// if (navPoint.size() < 2) {
				// throw new IllegalArgumentException("PDF lacks page info
				// in TOC index.");
				// }
				navMap.add(navPoint);
			}
		}
		return navMap;
	}
	
    private static void addToc(PDDocument document, ArrayList<ArrayList<String>> navMap, PDOutlineItem outline) throws IOException {

        while (outline != null) {
            ArrayList<String> navChild2Point = new ArrayList<String>();
            PDPage childPage = outline.findDestinationPage(document);
            int childPageCount = getPdfPage(document, childPage);
            String title = outline.getTitle().replaceAll("\u0007", "").replaceAll("", "").replaceAll("\\x07", "");
            logger.info(" Child:" + title + ",pageNum:" + childPageCount);
            navChild2Point.add(title);
            if (childPageCount > 0) {
                navChild2Point.add("page_" + CommonUtil.paddingLeft(String.valueOf(childPageCount), 4, '0') + ".xhtml");
            } else {
                navChild2Point.add("");
            }
            navMap.add(navChild2Point);
            addToc(document, navMap, outline.getFirstChild());
            outline = outline.getNextSibling();
        }

    }

    private static ArrayList<ArrayList<String>> parsePDFTocUseingPDFBOX(File file) throws Throwable {
        ArrayList<ArrayList<String>> navMap = new ArrayList<>();
        try (PDDocument document = PDDocument.load(file)) {

            PDDocumentCatalog catalog = document.getDocumentCatalog();
            PDDocumentOutline outline = catalog.getDocumentOutline();
//            PDOutlineItem item = outline.getFirstChild();
            
            addToc(document, navMap, outline.getFirstChild());
            
//            if (outline != null) {
//                while (item != null) {
//                    ArrayList<String> navPoint = new ArrayList<String>();
//                    PDOutlineItem child = item.getFirstChild();
//                    PDPage pdPage = item.findDestinationPage(document);
//                    int pageNo = getPdfPage(document, pdPage);
//                    logger.info("Item:" + item.getTitle() + ",pageNum:" + pageNo);
//                    navPoint.add(item.getTitle());
//                    String pageHtml = "page_"+CommonUtil.paddingLeft(String.valueOf(pageNo),4,'0')+".xhtml";
//                    if(pageNo>0) {
//                        navPoint.add(pageHtml);
//                    }else {
//                        navPoint.add("");
//                    }
//                    navMap.add(navPoint);
//                    while (child != null) {
//                        ArrayList<String> navChildPoint = new ArrayList<String>();
//                        PDPage childTilepdPage = child.findDestinationPage(document);
//                        int chilePage = getPdfPage(document, childTilepdPage);
//                        logger.info(" Child:" + child.getTitle() + ",pageNum:" + chilePage);
//                        navChildPoint.add(child.getTitle());
//                        String pageChildHtml = "page_"+CommonUtil.paddingLeft(String.valueOf(chilePage),4,'0')+".xhtml";
//                        if(chilePage>0) {
//                            navChildPoint.add(pageChildHtml);
//                        }else {
//                            navPoint.add("");
//                        }
//                        navMap.add(navChildPoint);
////                        PDOutlineItem child2 = child.getFirstChild();
////                        while (child2 != null) {
////                            ArrayList<String> navChild2Point = new ArrayList<String>();
////                            PDPage child2TilepdPage = child2.findDestinationPage(document);
////                            int child2Page = getPdfPage(document, child2TilepdPage);
////                            logger.info(" Child2:" + child.getTitle() + ",pageNum:" + child2Page);
////                            navChild2Point.add(child2.getTitle());
////                            if(child2Page>0) {
////                                navChild2Point.add("page_"+CommonUtil.paddingLeft(String.valueOf(child2Page),4,'0')+".xhtml");
////                            }else {
////                                navPoint.add("");
////                            }
////                            navMap.add(navChild2Point);
////                            child2 = child2.getNextSibling();
////                        }
//                        child = child.getNextSibling();
//                    }
//                    item = item.getNextSibling();
//                }
//            }

        } catch (Throwable e) {
            // has no toc 
        	logger.warn("pdf book without toc info, use linear parsing.");
        }
        return navMap;

    }

    private static int getPdfPage(PDDocument document, PDPage page) {
        int pageNum = 0;
        for (int i = 0; i < document.getNumberOfPages(); i++) {
            if (document.getPage(i).equals(page)) {
                return i + 1;
            }
        }
        return pageNum;
    }

	private void initEnv() {

		// Create S3 Client 
		this.s3Client = AmazonS3ClientBuilder.defaultClient();
		s3Permission = requestData.getIsTrial() ? CannedAccessControlList.PublicRead
				: CannedAccessControlList.AuthenticatedRead;
        this.originalBucket = config.getProperty("originalBucket");
        checkResultgPath = getLogFolder() + "/" + "CheckResult.log";
        checkResultLogger = new LogToS3(s3Client, originalBucket, checkResultgPath);

	}
	
    protected String getLogFolder() {
        String folderPath;
        this.logFolder = config.getProperty("logFolder");
        String trialFolder = config.getProperty("trialFolder");
        String originalFolder = config.getProperty("originalFolder");
        folderPath = logFolder + "/" + (requestData.getIsTrial() ? trialFolder : originalFolder) +
                "/" + requestData.getItem() + "/" + requestData.getBook_file_id();
        
        return folderPath;
    }


	/**
	 * default retry mode = Ec2.
	 * @param runMode
	 * @return
	 */
	public static RunMode getRetryRunMode(RunMode runMode) {
		return RunMode.ExtEc2Mode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		DataMessage message=job.getJobParam(DataMessage.class);
		String reqData=message.getRequestData();
		RequestData req = gson.fromJson(reqData, RequestData.class);
		String format=req.getFormat();
		logger.debug("Format: " + format);
		if ("pdf".equalsIgnoreCase(format)){
			return RunMode.ExtEc2Mode;
		}
		return RunMode.LambdaMode;
	}


	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// No db access needed.
	}
}
