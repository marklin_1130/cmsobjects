package digipages.BookConvert.TimeoutMonitor;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import digipages.BookConvert.utility.GenericLogRecord;
import digipages.BookConvert.utility.RequestData;
import digipages.BookConvert.utility.ResponseData;
import digipages.BookConvert.utility.Utility;
import digipages.CommonJobHandler.CommonJobHandlerInterface;
import digipages.CommonJobHandler.RunMode;
import digipages.common.DPLogger;
import digipages.exceptions.JobRetryException;
import digipages.exceptions.ServerException;
import model.ScheduleJob;

/**
 * schedule at start of handler (book/magazine/preview), schedule about 30 min. (adjustable)
 * 
 */
public class TimeoutMonitorHandler implements CommonJobHandlerInterface {
	private static DPLogger logger = DPLogger.getLogger(TimeoutMonitorHandler.class.getName());
	private AmazonS3 s3Client;
	private String originalBucket;
	private String trialBucket;
	private String originalFolder;
	private String trialFolder;
	private String logFolder;
	private String failFolder;
	private String urlBookProcessResult;

	private static final String RESPONSECODESUCC = "200";
	private static final String ERRCODETIMEOUT = "id_err_309";
	private static final String TAG = "[TimeoutMonitor]";
	private static final Gson gson = new GsonBuilder().create();
	private static final int BUFFER_SIZE = 1024;
	private boolean isSuccessful = false;
	private boolean isTimeout = true;

			
	@Override
	public String mainHandler(Properties config, ScheduleJob job) throws ServerException, JobRetryException {
		
		try {
			RequestData requestData = job.getJobParam(RequestData.class);
			initEnvironments(config);
			handleFinalMonitor(requestData);
		} catch (Exception e) {
			logger.error("Fail to handle TimeoutMonitor.",e);
		}
		
		return "TimeoutMonitor Finished.";
	}

	@Override
	public void setEntityManagerFactory(EntityManagerFactory emf) {
		// no database access
	}

	
	private void handleFinalMonitor(RequestData requestData) throws Exception {
		logger.info(TAG + " Event source is TimeoutMonitor.");

		// Get folder path
		String hashString = requestData.getItem() + requestData.getRequest_time();
		String hashCode = Utility.encrypt(hashString);

		// hash_code / book_file_id
		String rootFolderPath = (requestData.getIsTrial() ? trialFolder : originalFolder) + "/"
				+ hashCode.substring(hashCode.length() - 6) 
				+ "/" + requestData.getBook_file_id(); 
		
		String logFolderPath = logFolder + "/" + (requestData.getIsTrial() ? trialFolder : originalFolder) + "/"
				+ requestData.getItem() + "/" + requestData.getBook_file_id();

		logger.info(TAG + " RootFolderPath:" + rootFolderPath);
		logger.info(TAG + " LogFolderPath:" + logFolderPath);

		
		// Check timeout and converting result
		checkLog(logFolderPath);

		// Send timeout message
		if (isTimeout) {
			sendTimeout(requestData);
		}

		// If converting fail, move all file to failed.
		if (!isSuccessful || isTimeout) {
			moveFileToFail(requestData, rootFolderPath);
		} else {
			logger.info(TAG + " Convert successful.");
		}
		
	}
	
	private void sendTimeout(RequestData requestData) throws IOException {
		logger.info(TAG + " Send timeout message");
		ResponseData responseData = new ResponseData();
		ResponseData.ResultData resultData = responseData.new ResultData();

		resultData.setResult(false);
		resultData.setError_code(ERRCODETIMEOUT);
		resultData.setError_message("Lambda timeout");
		resultData.setVersion(requestData.getVersion());

		responseData.setItem(requestData.getItem());
		responseData.setCall_type(requestData.getCall_type());
		responseData.setProcessing_id(requestData.getBook_file_id());
		responseData.setR_data(resultData);

		// * [+] Send http request (POST) */
		URL obj = new URL(urlBookProcessResult);
		HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");

		String urlParameters = gson.toJson(responseData);

		// Send post request
		connection.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = connection.getResponseCode();
		logger.info(TAG + "Sending 'POST' request to URL:" + urlBookProcessResult);
		logger.info(TAG + "Response Code:" + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder response = new StringBuilder();

		char[] buffer = new char[BUFFER_SIZE]; // or some other
												// size,
		int charsRead ;
		while ((charsRead = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
			response.append(buffer, 0, charsRead);
		}

		logger.info(TAG + "response:" + response.toString());

		in.close();
		connection.disconnect();
		/* [-] Send http request (POST) */

		
	}
	
	private void checkLog(String logFolderPath) throws Exception {

		String convertedLogPath = logFolderPath + "/converted.log";
		String logPath = "/tmp/S3Log.log";
		File logFile = Utility.downloadFromS3(s3Client, originalBucket, convertedLogPath, logPath);
		InputStream logStream = new FileInputStream(logFile);
		BufferedReader br = new BufferedReader(new InputStreamReader(logStream));
		String line;
		boolean hitNotifyAPI=false;
		while ((line = br.readLine()) != null) {
			if (line.contains("[NotifyAPI]")) {
				hitNotifyAPI=true;
				break;
			}
		}
		
		while (hitNotifyAPI && (line = br.readLine()) != null ) {
			if (line.contains("ResponseData")) {
				String responseDataString = extractResponseStr(line);
				ResponseData responseData = gson.fromJson(responseDataString, 
						ResponseData.class);
				Object rData = responseData.getR_data();
				ResponseData.ResultData resultData = gson.fromJson(gson.toJson(rData),
						ResponseData.ResultData.class);
				isSuccessful = resultData.getResult();

				logger.info(TAG + "[converted.log]ResponseData Result:" + resultData.getResult());
			}

			if (line.contains("ResponseCode")) {
				logger.debug("ResponseCode="+ line);
				String responseCode = line.substring(line.indexOf(':') + 1);
				if (responseCode != null && responseCode.contains(RESPONSECODESUCC)) {
					isTimeout = false;

					logger.info(TAG + "[converted.log]" + line);
				}
			}
		}

		logger.info(TAG + " isTimeout:" + isTimeout + ", isSuccessful:" + isSuccessful);

		br.close();
		logStream.close();
		
		boolean delsucc=logFile.delete();
		if (!delsucc)
		{
			logFile.deleteOnExit();
		}
		
	}
	
	public static String extractResponseStr(String line) {
		String ret="";
		try {
			GenericLogRecord record = GenericLogRecord.fromString(line);
			ret=record.getMessage();
			
			ret=ret.replaceAll("\r", "\n");
			ret=ret.replaceAll("\n\n", "\n");
			String[] x = ret.split("\n");
			for (String tmpLine:x)
			{
				if (tmpLine.startsWith("ResponseData:"))
				{
					ret = tmpLine.substring(tmpLine.indexOf(':')+1).trim();
					break;
				}
			}
			
		}catch (Exception e)
		{
			logger.error("Unexcepted format in parsing GenericLogRcord:" + line,e);
		}
		return ret;
	}

	private void moveFileToFail(RequestData requestData, String rootFolderPath) {
		logger.info(TAG + " Convert fail.");
		logger.info(TAG + " Move to failed.");
		String targetBucket = requestData.getIsTrial() ? trialBucket : originalBucket;

		ObjectListing objectList = s3Client.listObjects(targetBucket, rootFolderPath + "/");
		List<S3ObjectSummary> objectSummaries = objectList.getObjectSummaries();

		while (objectList.isTruncated()) {
			objectList = s3Client.listNextBatchOfObjects(objectList);
			objectSummaries.addAll(objectList.getObjectSummaries());
		}

		logger.info(TAG + " objectSummaries num:" + objectSummaries.size() + ", rootFolderPath+/:"
				+ rootFolderPath + "/");

		for (S3ObjectSummary summary : objectSummaries) {
			String bucket = summary.getBucketName();
			String key = summary.getKey();
			CopyObjectRequest copyObjectRequest = new CopyObjectRequest(bucket, key, originalBucket,
					failFolder + "/" + key);
			copyObjectRequest.setCannedAccessControlList(CannedAccessControlList.AuthenticatedRead);
			s3Client.copyObject(copyObjectRequest);
			s3Client.deleteObject(bucket, key);
		}
	}
	
	private void initEnvironments(Properties config) {
		originalBucket = config.getProperty("originalBucket");
		trialBucket = config.getProperty("trialBucket");
		originalFolder = config.getProperty("originalFolder");
		trialFolder = config.getProperty("trialFolder");
		logFolder = config.getProperty("logFolder");
		failFolder = config.getProperty("failFolder");
		urlBookProcessResult = config.getProperty("URL_BookProcessResult");

		// Create S3 Client
		this.s3Client = AmazonS3ClientBuilder.defaultClient();
	}
	
	public static RunMode getRetryRunMode(RunMode runMode) throws Exception {
		return RunMode.LocalMode;
	}

	public static RunMode getInitRunMode(ScheduleJob job) {
		return RunMode.LocalMode;
	}

}
