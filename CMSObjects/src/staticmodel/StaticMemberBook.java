package staticmodel;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;
import org.eclipse.persistence.annotations.Struct;

import digipages.common.CommonUtil;

/**
 * The persistent class for the member_book database table.
 *
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Struct(name = "device_seq")
@Table(name = "member_book")
public class StaticMemberBook extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String EncryptTypeNone = "none";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@Column(name = "archived")
	private Boolean archived = false;

	@Mutable
	@Column(name = "book_type")
	private String bookType;

	@Mutable
	@Column(name = "passworded")
	private Boolean passworded = false;

	@Mutable
	@Column(name = "encrypt_type")
	private String encryptType = EncryptTypeNone;

	@Mutable
	@Column(name = "encrypt_key")
	private byte[] encryptKey;

	@Mutable
	@Column(name = "max_read_loc")
	private String maxReadLoc;

	@Mutable
	@Column(name = "last_loc")
	private String LastLoc;

	@Mutable
	@Column(name = "lock_status")
	private Integer lockStatus;

	@Mutable
	@Column(name = "operator")
	private String operator;

	@Mutable
	@Column(name = "member_id", insertable = false, updatable = false)
	private Integer memberId;

	@Mutable
	@Column(name = "book_file_id", insertable = false, updatable = false)
	private Integer bookFileId;

	@Mutable
	@Column(name = "readlist_idnames")
	@Array(databaseType = "text")
	private List<String> readlistIdnames = new ArrayList<String>();

	@Mutable
	@Column(name = "device_seq")
	@Array(databaseType = "int4")
	private List<Integer> deviceSeq;

	@Column(name = "item_id", insertable = false, updatable = false)
	private String itemId;

	@Mutable
	@Column(name = "drm_log_ids")
	@Array(databaseType = "int4")
	private List<Long> drmLogIds = new ArrayList<>();

	@Mutable
	@Column(name = "percentage")
	private int percentage;

	@Mutable
	@Column(name = "start_read_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date StartReadTime;

	@Mutable
	@Column(name = "last_read_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date LastReadTime;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@Mutable
	@Column(name = "archived_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date archivedDate;

	@Mutable
	@Column(name = "download_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date downloadTime;

	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime = new Date();

	@Mutable
	@Column(name = "ask_update_version")
	private Boolean askUpdateVersion = false;

	@Mutable
	@Column(name = "bg_update_version")
	private Boolean bgUpdateVersion = false;

	@Mutable
	@Column(name = "version_locked")
	private Boolean versionLocked = false;

	@Mutable
	@Column(name = "book_highlight_status")
	private Boolean bookHighlightStatus = false;

	@Mutable
	@Column(name = "book_bookmark_status")
	private Boolean bookBookmarkStatus = false;

	@Mutable
	@Column(name = "newer_version")
	private String newerVersion;

	@Mutable
	@Column(name = "cur_version")
	private String curVersion;

	@Mutable
	@Column(name = "upgrade_time")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date upgradeTime;

	@Mutable
	@Column(name = "action")
	private String action = "update";

	@Mutable
	@Column(name = "is_hidden")
	private Boolean isHidden = false;

	@Mutable
	@Column(name = "is_book")
	private Boolean isBook = false;

	@Mutable
	@Column(name = "is_magazine")
	private Boolean isMagazine = false;

	@Mutable
	@Column(name = "is_trial")
	private Boolean isTrial = false;

	@Mutable
	@Column(name = "deleted")
	private Boolean deleted = false;

	@Mutable
	@Column(name = "book_uni_id")
	private String bookUniId = "";

	@Mutable
	@Column(name = "create_operator")
	private String createOperator;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public StaticMemberBook() {
		touchUpdateTime();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getArchived() {
		return this.archived;
	}

	public void setArchived(Boolean archived) {
		this.archived = archived;
		touchUpdateTime();
	}

	public String getBookType() {
		return this.bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
		touchUpdateTime();
	}

	public Boolean getPassworded() {
		return this.passworded;
	}

	public void setPassworded(Boolean passworded) {
		this.passworded = passworded;
		touchUpdateTime();
	}

	public String getItem() {
		return itemId;
	}

	public void setItem(String item) {
		this.itemId = item;
		touchUpdateTime();
	}

	public List<Long> getDrmLogIds() {
		return drmLogIds;
	}

	public void addDrmLogIds(Long drmLogId) {
		this.drmLogIds.add(drmLogId);

		if (getDrmLogIds().size() > 0 && this.getAction().equals("del")) {
			this.setAction("update");
			this.setLastUpdated(CommonUtil.zonedTime());
		}
	}

	public List<String> getReadlistIdnames() {
		if (null == readlistIdnames)
			readlistIdnames = new ArrayList<String>();
		return this.readlistIdnames;
	}

	public void setReadlistIdnames(List<String> readlistIdnames) {
		this.readlistIdnames = readlistIdnames;
		touchUpdateTime();
	}

	public List<Integer> getDeviceId() {
		return deviceSeq;
	}

	public void setDeviceId(List<Integer> deviceSeq) {
		this.deviceSeq = deviceSeq;
		touchUpdateTime();
	}

	public void addDeviceId(int deviceSeq) {
		if (null == getDeviceId()) {
			setDeviceId(new ArrayList<Integer>());
		}

		if (!getDeviceId().contains(deviceSeq)) {
			getDeviceId().add(deviceSeq);
			setDeviceId(getDeviceId());
		}

		touchUpdateTime();
	}

	public String getEncryptType() {
		return encryptType;
	}

	public void setEncryptType(String encryptType) {
		this.encryptType = encryptType;
		touchUpdateTime();
	}

	public byte[] getEncryptKey() {
		return encryptKey;
	}

	public void setEncryptKey(byte[] encryptKey) {
		this.encryptKey = encryptKey;
		touchUpdateTime();
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
		touchUpdateTime();
	}

	public Date getStartReadTime() {
		return StartReadTime;
	}

	public void setStartReadTime(Date startReadTime) {
		StartReadTime = startReadTime;
		touchUpdateTime();
	}

	public Date getLastReadTime() {
		return LastReadTime;
	}

	public void setLastReadTime(Date lastReadTime) {
		LastReadTime = lastReadTime;
		touchUpdateTime();
	}

	public Date getArchivedDate() {
		return archivedDate;
	}

	public void setArchivedDate(Date archivedDate) {
		this.archivedDate = archivedDate;
		touchUpdateTime();
	}

	public Date getDownloadTime() {
		return downloadTime;
	}

	public void setDownloadTime(Date downloadTime) {
		this.downloadTime = downloadTime;
		touchUpdateTime();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getMaxReadLoc() {
		return maxReadLoc;
	}

	public void setMaxReadLoc(String maxReadLoc) {
		this.maxReadLoc = maxReadLoc;
		touchUpdateTime();
	}

	public String getLastLoc() {
		return LastLoc;
	}

	public void setLastLoc(String lastLoc) {
		LastLoc = lastLoc;
		touchUpdateTime();
	}

	public Integer getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(Integer lockStatus) {
		this.lockStatus = lockStatus;
		touchUpdateTime();
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
		touchUpdateTime();
	}

	public Boolean getAskUpdateVersion() {
		return askUpdateVersion;
	}

	public void setAskUpdateVersion(Boolean askUpdateVersion) {
		this.askUpdateVersion = askUpdateVersion;
		touchUpdateTime();
	}

	public Boolean getBgUpdateVersion() {
		return bgUpdateVersion;
	}

	public void setBgUpdateVersion(Boolean bgUpdateVersion) {
		this.bgUpdateVersion = bgUpdateVersion;
		touchUpdateTime();
	}

	public Boolean getVersionLocked() {
		return versionLocked;
	}

	public void setVersionLocked(Boolean versionLocked) {
		this.versionLocked = versionLocked;
		touchUpdateTime();
	}

	public Boolean getBookHighlightStatus() {
		return bookHighlightStatus;
	}

	public void setBookHighlightStatus(Boolean bookHighlightStatus) {
		this.bookHighlightStatus = bookHighlightStatus;
		touchUpdateTime();
	}

	public Boolean getBookBookmarkStatus() {
		return bookBookmarkStatus;
	}

	public void setBookBookmarkStatus(Boolean bookBookmarkStatus) {
		this.bookBookmarkStatus = bookBookmarkStatus;
	}

	public String getNewerVersion() {
		return newerVersion;
	}

	public void setNewerVersion(String newerVersion) {
		this.newerVersion = newerVersion;
		touchUpdateTime();
	}

	public String getCurVersion() {
		return curVersion;
	}

	public void setCurVersion(String curVersion) {
		this.curVersion = curVersion;
		touchUpdateTime();
	}

	public Date getUpgradeTime() {
		return upgradeTime;
	}

	public void setUpgradeTime(Date upgradeTime) {
		this.upgradeTime = upgradeTime;
		touchUpdateTime();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		if (action.equals("del")) {
			this.setDeleted(true);
		} else {
			this.setDeleted(false);
		}

		this.action = action;
		touchUpdateTime();
	}

	public String getCreateOperator() {
		return createOperator;
	}

	public void setCreateOperator(String createOperator) {
		this.createOperator = createOperator;
	}

	/**
	 * set my update time to now
	 */
	public void touchUpdateTime() {
		setLastUpdated(new Date());
	}

	public String getBookUniId() {
		return bookUniId;
	}

	public void setBookUniId(String bookUniId) {
		this.bookUniId = bookUniId;
		this.touchUpdateTime();
	}

	public void clearReadlistIdnames() {
		readlistIdnames.clear();
		touchUpdateTime();
	}

	public boolean isDeleted() {
		return getAction().equals("del");
	}

	public Boolean getIsHidden() {
		return isHidden;
	}

	public void setIsHidden(Boolean isHidden) {
		this.isHidden = isHidden;
	}

	public Boolean getIsBook() {
		return isBook;
	}

	public void setIsBook(Boolean isBook) {
		this.isBook = isBook;
	}

	public Boolean getIsMagazine() {
		return isMagazine;
	}

	public void setIsMagazine(Boolean isMagazine) {
		this.isMagazine = isMagazine;
	}

	public Boolean getIsTrial() {
		return isTrial;
	}

	public void setIsTrial(Boolean isTrial) {
		this.isTrial = isTrial;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
}