package staticmodel;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

/**
 * The persistent class for the member_drm_log database table.
 *
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "member_drm_log")
public class StaticMemberDrmLog extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@Column(name = "member_id")
	private Long memberId;

	@Mutable
	@Column(name = "item_id")
	private String itemId;

	@Mutable
	@Column(name = "download_expire_time")
	private String downloadExpireTime = "2099/12/31 23:59:59";

	@Mutable
	@Column(name = "status")
	private int status;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	@Column(name = "read_days")
	private Integer readDays;

	@Column(name = "read_expire_time")
	private String readExpireTime = "2099/12/31 23:59:59";

	@Column(name = "transaction_id")
	private String transactionId;

	@Mutable
	@Column(name = "type")
	private int type;

	public StaticMemberDrmLog() {

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMember() {
		return this.memberId;
	}

	public void setMember(Long member) {
		this.memberId = member;
	}

	public String getItem() {
		return this.itemId;
	}

	public void setItem(String item) {
		this.itemId = item;
	}

	public String getDownloadExpireTime() {
		return this.downloadExpireTime;
	}

	public void setDownloadExpireTime(String downloadExpireTime) {
		this.downloadExpireTime = downloadExpireTime;
	}

	public int getReadDays() {
		return this.readDays;
	}

	public void setReadDays(int readDays) {
		this.readDays = readDays;
	}

	public String getReadExpireTime() {
		return this.readExpireTime;
	}

	public void setReadExpireTime(String readExpireTime) {
		this.readExpireTime = readExpireTime;
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}