#!/bin/sh

mkdir /var/log/convert

cd /usr/local/converter/

#don't use image embedded
rm config.properties
#copy Jars/Environments
aws s3 cp s3://s3private.booksdev.digipage.info/Deployment/Converter/config.properties . 2>> /var/log/convert/error.log
aws s3 cp s3://s3private.booksdev.digipage.info/Deployment/Converter/Converter.jar . 2>> /var/log/convert/error.log

#run Worker
java -jar Converter.jar >> /var/log/convert/debug.log 2>>/var/log/convert/error.log

#copy Logs to S3
aws s3 cp /var/log/convert/debug.log "s3://s3private.booksdev.digipage.info/log-space/WorkerLog/`TZ='Asia/Taipei' date --rfc-3339='seconds'`--`ec2-metadata -i |cut -f 2 -d : - `.log"
aws s3 cp /var/log/convert/error.log "s3://s3private.booksdev.digipage.info/log-space/WorkerLog/`TZ='Asia/Taipei' date --rfc-3339='seconds'`--`ec2-metadata -i |cut -f 2 -d : - `_error.log"

#wait and sutdown
shutdown -h +10