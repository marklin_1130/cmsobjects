﻿#!/bin/sh

/usr/bin/mkdir /var/log/convert

/usr/bin/mkdir /var/log/openbook

/usr/bin/mkdir /usr/local/openbook

cd /usr/local/openbook/

rm -f /var/log/openbook/*.log

ulimit -n 655360
ulimit -u 655360

rm -rf /opt/apache-tomcat-8.0.43/webapps/CMSOpenBook*
aws --region ap-northeast-1 s3 cp s3://s3private-auth.books.com.tw/Deployment/Converter/CMSOpenBook.war /opt/apache-tomcat-8.0.43/webapps/ 2> /var/log/openbook/error.log

/opt/apache-tomcat-8.0.43/bin/startup.sh &

#copy Jars/Environments
aws --region ap-northeast-1 s3 cp s3://s3private-auth.books.com.tw/Deployment/Converter/config.properties . 2> /var/log/openbook/error.log
aws --region ap-northeast-1 s3 cp --recursive s3://s3private-auth.books.com.tw/Deployment/Converter/Converter_lib/ Converter_lib/ 2>> /var/log/openbook/error.log
aws --region ap-northeast-1 s3 cp s3://s3private-auth.books.com.tw/Deployment/Converter/OpenBook.jar . 2>> /var/log/openbook/error.log

#run Worker
java -jar OpenBook.jar

#copy Logs to S3
aws --region ap-northeast-1 s3 cp /var/log/openbook/debug.log "s3://s3private-auth.books.com.tw/log-space/openbookWorkerLog/`TZ='Asia/Taipei' date --rfc-3339='seconds'`--`ec2meta instance_id`.log"
aws --region ap-northeast-1 s3 cp /var/log/openbook/error.log "s3://s3private-auth.books.com.tw/log-space/openbookWorkerLog/`TZ='Asia/Taipei' date --rfc-3339='seconds'`--`ec2meta instance_id`_error.log"

#wait and sutdown
shutdown -h +10
