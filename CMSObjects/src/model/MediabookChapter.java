package model;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import com.google.gson.annotations.Expose;

import digipages.dto.ChapterInfo;
import digipages.dto.PreviewInfo;

@Entity
@Cache(type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
		size = 4000, // Use 4,000 as the initial cache size.
		expiry = 3000, // 3sec
		coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send
																			// invalidation messages.
)
@Table(name = "mediabook_chapter")
@NamedQueries({
@NamedQuery(
        name = "MediabookChapter.findByItemIdAndChapterNo",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.itemId = :itemId"
                + " AND mc.chapterNo = :chapterNo"
                + " AND mc.deleted = false"
                +"  ORDER BY mc.chapterNo"
                ),
@NamedQuery(
        name = "MediabookChapter.findById",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.id = :id "
                + " AND mc.deleted = false"
                ),
@NamedQuery(
        name = "MediabookChapter.findByIdAndItemIdAndChapterNo",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.id = :id"
                + " AND mc.itemId = :itemId "
                + " AND mc.chapterNo = :chapterNo "
                + " AND mc.deleted = false"
                ),
@NamedQuery(
        name = "MediabookChapter.findByConverId",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.convertId = :convertId"
                +"  ORDER BY mc.chapterNo"
                ),
@NamedQuery(
        name = "MediabookChapter.findByBookFileId",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.status = 9 "
                + " AND mc.deleted = false"
                + " AND (mc.bookFileId = :bookFileId OR mc.bookFileTrialId = :bookFileId)"
                +" ORDER BY mc.chapterNo"
                ),
@NamedQuery(
        name = "MediabookChapter.findByBookFileTrialId",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.status = 9 "
                + " AND mc.deleted = false"
                + " AND mc.bookFileTrialId = :bookFileId "
                +" ORDER BY mc.chapterNo"
                ),
@NamedQuery(
        name = "MediabookChapter.findByChapterNoAndItemIdAndStatus",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.status = 9 "
                + " AND mc.deleted = false"
                + " AND mc.itemId = :itemId "
                + " AND mc.chapterNo = :chapterNo "
                +" ORDER BY mc.chapterNo"
                ),
@NamedQuery(
        name = "MediabookChapter.findByItemIdAndStatusAndPreview",
        query = "SELECT mc FROM MediabookChapter mc"
                + " WHERE mc.status = 9 "
                + " AND mc.deleted = false"
                + " AND mc.ispreview = 'Y'"
                + " AND mc.itemId = :itemId "
                +" ORDER BY mc.chapterNo"
                )
})
public class MediabookChapter {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	@Expose
	private Long id;
	
	@Mutable
	@OneToMany(mappedBy = "mediabookChapter",fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@OrderBy("subtitleId")
	@Expose
	private List<MediabookSubtitle> subtitles;

    @Mutable
    @Column(name = "convert_id")
    @Expose
    private Long convertId;
    
	@Mutable
	@Column(name = "item_id")
	@Expose
	private String itemId;

	@Mutable
	@Column(name = "book_file_id")
	@Expose
	private long bookFileId;
	
	@Mutable
	@Column(name = "book_file_trial_id")
	@Expose
	private long bookFileTrialId;
	
	@Mutable
	@Column(name = "version")
	@Expose
	private String version;

	@Mutable
	@Column(name = "cat")
	@Expose
	private String cat;

	@Mutable
	@Column(name = "chapter_no")
	@Expose
	private long chapterNo;

	@Mutable
	@Column(name = "chapter_name")
	@Expose
	private String chapterName;

	@Mutable
	@Column(name = "chapter_file")
	@Expose
	private String chapterFile;

	@Mutable
	@Column(name = "chapter_size")
	@Expose
	private String chapterSize;

	@Mutable
	@Column(name = "chapter_length")
	@Expose
	private String chapterLength;

	@Mutable
	@Column(name = "is_preview")
	@Expose
	private String ispreview;

	@Mutable
	@Column(name = "preview_type")
	@Expose
	private String previewType;

	@Mutable
	@Column(name = "is_trial")
	@Expose
	private boolean isTrial;

	@Mutable
	@Column(name = "preview_content")
	@Expose
	private String previewContent;

	@Mutable
	@Column(name = "is_script")
	@Expose
	private String isScript;

	@Mutable
	@Column(name = "script_filename")
	@Expose
	private String scriptFilename;

	@Mutable
	@Column(name = "script_file")
	@Expose
	private String scriptFile;

	@Mutable
	@Column(name = "is_subtitle")
	@Expose
	private String isSubtitle;

	@Mutable
	@Column(name = "preview_info")
	@Convert(converter = digipages.pgconverter.PGPreviewInfoConverter.class)
	@Expose
	private PreviewInfo previewInfo;

	@Mutable
	@Column(name = "chapter_info")
	@Convert(converter = digipages.pgconverter.PGChapterInfoConverter.class)
	@Expose
	private ChapterInfo chapterInfo;

	@Mutable
	@Column(name = "size")
	@Expose
	private Long size;

	@Mutable
	@Column(name = "file_location")
	@Expose
	private String fileLocation;

	@Mutable
	@Column(name = "src_fname")
	@Expose
	private String srcFname;

	@Mutable
	@Column(name = "format")
	@Expose
	private String format;

	@Mutable
	@Column(name = "status")
	@Expose
	private Integer status;

	@Mutable
	@Column(name = "response_msg")
	@Expose
	private String responseMsg;

	@Mutable
    @Column(name = "result_code")
    @Expose
    private String resultCode;
	
	@Mutable
	@Column(name = "deleted")
	@Expose
	private boolean deleted;

	@Mutable
	@Column(name = "operator")
	@Expose
	private String operator;

	@Mutable
    @Column(name = "preview_length")
    @Expose
	private Integer previewLength;
	
	
	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	private Date createTime = new Date();

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public long getBookFileId() {
		return bookFileId;
	}

	public void setBookFileId(long bookFileId) {
		this.bookFileId = bookFileId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public long getChapterNo() {
		return chapterNo;
	}

	public void setChapterNo(long chapterNo) {
		this.chapterNo = chapterNo;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public String getChapterFile() {
		return chapterFile;
	}

	public void setChapterFile(String chapterFile) {
		this.chapterFile = chapterFile;
	}

	public String getChapterSize() {
		return chapterSize;
	}

	public void setChapterSize(String chapterSize) {
		this.chapterSize = chapterSize;
	}

	public String getChapterLength() {
		return chapterLength;
	}

	public void setChapterLength(String chapterLength) {
		this.chapterLength = chapterLength;
	}

	public String isIspreview() {
		return ispreview;
	}

	public void setIspreview(String ispreview) {
		this.ispreview = ispreview;
	}

	public String getPreviewType() {
		return previewType;
	}

	public void setPreviewType(String previewType) {
		this.previewType = previewType;
	}

	public boolean getIsTrial() {
		return isTrial;
	}

	public void setIsTrial(boolean isTrial) {
		this.isTrial = isTrial;
	}

	public String getPreviewContent() {
		return previewContent;
	}

	public void setPreviewContent(String previewContent) {
		this.previewContent = previewContent;
	}

	public String getIsScript() {
		return isScript;
	}

	public void setIsScript(String isScript) {
		this.isScript = isScript;
	}

	public String getScriptFilename() {
		return scriptFilename;
	}

	public void setScriptFilename(String scriptFilename) {
		this.scriptFilename = scriptFilename;
	}

	public String getScriptFile() {
		return scriptFile;
	}

	public void setScriptFile(String scriptFile) {
		this.scriptFile = scriptFile;
	}

	public String getIsSubtitle() {
		return isSubtitle;
	}

	public void setIsSubtitle(String isSubtitle) {
		this.isSubtitle = isSubtitle;
	}

	public PreviewInfo getPreviewInfo() {
        return previewInfo;
    }

    public void setPreviewInfo(PreviewInfo previewInfo) {
        this.previewInfo = previewInfo;
    }

    public ChapterInfo getChapterInfo() {
		return chapterInfo;
	}

	public void setChapterInfo(ChapterInfo chapterInfo) {
		this.chapterInfo = chapterInfo;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public String getSrcFname() {
		return srcFname;
	}

	public void setSrcFname(String srcFname) {
		this.srcFname = srcFname;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public List<MediabookSubtitle> getSubtitles() {
		return subtitles;
	}

	public void setSubtitles(List<MediabookSubtitle> subtitles) {
		this.subtitles = subtitles;
	}

	public String getIspreview() {
		return ispreview;
	}

	public void setTrial(boolean isTrial) {
		this.isTrial = isTrial;
	}

	public long getBookFileTrialId() {
		return bookFileTrialId;
	}

	public void setBookFileTrialId(long bookFileTrialId) {
		this.bookFileTrialId = bookFileTrialId;
	}

    public Long getConvertId() {
        return convertId;
    }

    public void setConvertId(Long convertId) {
        this.convertId = convertId;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public Integer getPreviewLength() {
        return previewLength;
    }

    public void setPreviewLength(Integer previewLength) {
        this.previewLength = previewLength;
    }
}
