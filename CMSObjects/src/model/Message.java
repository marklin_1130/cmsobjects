package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.*;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

/**
 * The persistent class for the message database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min 
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "message")
@NamedQueries({
	@NamedQuery(
		name = "Message.findAll",
		query = "SELECT m FROM Message m"
	),
	@NamedQuery(
		name = "Message.countAll",
		query = "SELECT COUNT(m) FROM Message m"
	),
	@NamedQuery(
		name = "Message.countByTime",
		query = "SELECT COUNT(m) FROM Message m"
				+ " WHERE m.sendTime BETWEEN :startTime AND :endTime"
	),
	@NamedQuery(
		name = "Message.findBySendTime",
		query = "SELECT m FROM Message m"
				+ " WHERE m.sendTime BETWEEN :startTime AND :endTime"
	),
	@NamedQuery(
		name = "Message.findByMemberId",
		query = "SELECT m FROM Message m"
				+ " WHERE m.member.id = :member_id"
				+ " AND m.sendTime BETWEEN :startTime AND :endTime"
				+ " ORDER BY m.lastUpdated ASC"
	)
})
public class Message extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	// bi-directional many-to-one association to Member
	@Mutable
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "member_id")
	private Member member;

	@Mutable
	@Column(name = "message_type")
	private String messageType;

	@Mutable
	@Column(name = "publisher_name")
	private String publisherName;

	@Mutable
	@Column(name = "title")
	private String title;

	@Mutable
	@Column(name = "content")
	private String content;

	@Mutable
	@Column(name = "link_uri")
	private String linkUri;

	@Mutable
	@Column(name = "send_time")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date sendTime;
	
	@Mutable
	@Column(name = "expire_time")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date expireTime;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@Mutable
	@Column(name = "device_id")
	private String deviceId;

	@Mutable
	@Column(name = "message_id")
	private String messageId;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Message() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLinkUri() {
		return linkUri;
	}

	public void setLinkUri(String linkUri) {
		this.linkUri = linkUri;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
}