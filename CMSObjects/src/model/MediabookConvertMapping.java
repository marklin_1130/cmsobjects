package model;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Mutable;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "mediabook_convert_mapping")
public class MediabookConvertMapping {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	@Expose
	private long id;

	@Mutable
	@Column(name = "convert_status")
	@Expose
	private String convertStatus; 
	
    @Column(name = "convert_type")
    @Expose
    private String convertType; 
	
	@Mutable
	@Column(name = "item_id")
	@Expose
	private String itemId;
	
	@Mutable
	@Column(name = "status")
	@Expose
	private Integer status;

	@Mutable
	@Column(name = "deleted")
	@Expose
	private boolean deleted;

	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	private Date createTime = new Date();

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Long getId() {
		return id;
	}

    public String getConvertStatus() {
        return convertStatus;
    }

    public String getItemId() {
        return itemId;
    }

    public Integer getStatus() {
        return status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setConvertStatus(String convertStatus) {
        this.convertStatus = convertStatus;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getConvertType() {
        return convertType;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setConvertType(String convertType) {
        this.convertType = convertType;
    }

}
