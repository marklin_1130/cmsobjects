package model;

import java.io.Serializable;
import javax.persistence.*;

import digipages.common.CommonUtil;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the worker_status database table.
 * 
 */
@Entity
@Table(name = "worker_status")
@NamedQueries({ 
	@NamedQuery(name = "WorkerStatus.findAll",
		query = "SELECT w FROM WorkerStatus w"),
	@NamedQuery(name = "WorkerStatus.findByInstanceId", 
		query = "SELECT w FROM WorkerStatus w WHERE w.instanceId like CONCAT('%',:instanceId,'%') and w.threadId = :threadId"),
	@NamedQuery(name = "WorkerStatus.countIdleInstances", 
		query = "SELECT count(w) FROM WorkerStatus w WHERE 1=1 and (w.status = 0 OR w.status = 1 ) and w.instanceId not like 'OPENBOOK%'"),
	@NamedQuery(name = "WorkerStatus.countOpenBookIdleInstances", 
    query = "SELECT count(w) FROM WorkerStatus w WHERE 1=1 and (w.status = 0 OR w.status = 1 ) and w.instanceId like 'OPENBOOK%'"),
	@NamedQuery(name = "WorkerStatus.countActiveInstances", 
	query = "SELECT count(w) FROM WorkerStatus w WHERE 1=1 and (w.status = 0 OR w.status = 1 Or w.status=2 ) and w.instanceId not like 'OPENBOOK%'"),
	@NamedQuery(name = "WorkerStatus.countOpenBookActiveInstances", 
    query = "SELECT count(w) FROM WorkerStatus w WHERE 1=1 and (w.status = 0 OR w.status = 1 Or w.status=2 ) and w.instanceId  like 'OPENBOOK%'"),
	@NamedQuery(name = "WorkerStatus.listIdleInstances", 
	query = "SELECT w FROM WorkerStatus w WHERE w.status = 0 OR w.status = 1"),
	@NamedQuery(name = "WorkerStatus.cleanOldRecords", 
	query = "DELETE FROM WorkerStatus w WHERE w.udt < :last3months"),
	@NamedQuery(name = "WorkerStatus.findWorkerStatusByInstanceId", 
	query = "SELECT w FROM WorkerStatus w WHERE w.instanceId = :instanceId"),
})
public class WorkerStatus extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(name = "instance_id", length = 2147483647)
	private String instanceId;

	@Column(name = "job_finish_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date jobFinishTime;

	@Column(name = "job_info")
	@Convert(converter = digipages.pgconverter.PGGenericJsonContentConverter.class)
	private Object jobInfo;

	@Column(name = "job_start_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date jobStartTime;

	private Integer status = 0;
	
	@Column(name = "thread_id")
	private Integer threadId = 0;

	@Temporal(TemporalType.TIMESTAMP)
	private Date udt = new Date();

	@Column(name = "worker_start_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date workerStartTime;

	public WorkerStatus() {

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInstanceId() {
		return this.instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Date getJobFinishTime() {
		return this.jobFinishTime;
	}

	public void setJobFinishTime(Date jobFinishTime) {
		this.jobFinishTime = jobFinishTime;
	}

	public Object getJobInfo() {
		return this.jobInfo;
	}

	public void setJobInfo(Object jobInfo) {
		this.jobInfo = jobInfo;
	}

	public Date getJobStartTime() {
		return this.jobStartTime;
	}

	public void setJobStartTime(Date jobStartTime) {
		this.jobStartTime = jobStartTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getUdt() {
		return this.udt;
	}

	public void setUdt(Date udt) {
		this.udt = udt;
	}

	public Date getWorkerStartTime() {
		return this.workerStartTime;
	}

	public void setWorkerStartTime(Date workerStartTime) {
		this.workerStartTime = workerStartTime;
	}

	/**
	 * @param postgres
	 * @param instanceId
	 * @return null if not found.
	 */
	public static WorkerStatus findByInstanceId(EntityManager postgres, String instanceId, Integer threadId) {
		TypedQuery<WorkerStatus> query = postgres.createNamedQuery("WorkerStatus.findByInstanceId", WorkerStatus.class);
		query.setParameter("instanceId", instanceId);
		query.setParameter("threadId", threadId);
		List<WorkerStatus> list = query.getResultList();
		if (list.isEmpty())
			return null;
		return list.get(0);
	}
	
	/**
	 * @param postgres
	 * @param instanceId
	 * @return null if not found.
	 */
	public static WorkerStatus findWorkerStatusByInstanceId(EntityManager postgres, String instanceId) {
		TypedQuery<WorkerStatus> query = postgres.createNamedQuery("WorkerStatus.findWorkerStatusByInstanceId", WorkerStatus.class);
		query.setParameter("instanceId", instanceId);
		List<WorkerStatus> list = query.getResultList();
		if (list.isEmpty())
			return null;
		return list.get(0);
	}
	
	public static List<WorkerStatus> listByInstanceId(EntityManager postgres, String instanceId, Integer threadId) {
		TypedQuery<WorkerStatus> query = postgres.createNamedQuery("WorkerStatus.findByInstanceId", WorkerStatus.class);
		query.setParameter("instanceId", instanceId);
		query.setParameter("threadId", threadId);
		List<WorkerStatus> list = query.getResultList();
		return list;
	}
	

	/**
	 * update status and udt by that
	 * 
	 * @param that
	 */
	public void updateStatusBy(WorkerStatus that) {
		this.setStatus(that.getStatus());
		this.setUdt(new Date());
		if (null!=that.getJobInfo())
			this.setJobInfo(that.getJobInfo());
		if (null!=that.getJobStartTime())
			this.setJobStartTime(that.getJobStartTime());
		if (null!=that.getWorkerStartTime())
			this.setWorkerStartTime(that.getWorkerStartTime());
		if (null!=that.getJobFinishTime())
			this.setJobFinishTime(that.getJobFinishTime());
		

	}

	public static int countAvailInstances(EntityManager postgres) {
		int ret = 0;
		Object o = postgres.createNamedQuery(
				"WorkerStatus.countIdleInstances", WorkerStatus.class)
				.getSingleResult();

		if (o instanceof Integer) {
			ret = (Integer) o;
		}

		if (o instanceof Long) {
			ret = ((Long) o).intValue();
		}

		return ret;
	}
	
	   public static int countOpenBookAvailInstances(EntityManager postgres) {
	        int ret = 0;
	        Object o = postgres.createNamedQuery(
	                "WorkerStatus.countOpenBookIdleInstances", WorkerStatus.class)
	                .getSingleResult();

	        if (o instanceof Integer) {
	            ret = (Integer) o;
	        }

	        if (o instanceof Long) {
	            ret = ((Long) o).intValue();
	        }

	        return ret;
	    }
	
	public static int countAllInstances(EntityManager postgres) {
		int ret = 0;
		Object o = postgres.createNamedQuery("WorkerStatus.countActiveInstances", WorkerStatus.class).getSingleResult();

		if (o instanceof Integer) {
			ret = (Integer) o;
		}

		if (o instanceof Long) {
			ret = ((Long) o).intValue();
		}

		return ret;
	}

    public static int countOpenBookAllInstances(EntityManager postgres) {
        int ret = 0;
        Object o = postgres.createNamedQuery("WorkerStatus.countOpenBookActiveInstances", WorkerStatus.class).getSingleResult();

        if (o instanceof Integer) {
            ret = (Integer) o;
        }

        if (o instanceof Long) {
            ret = ((Long) o).intValue();
        }

        return ret;
    }

	public Integer getThreadId() {
		return threadId;
	}

	public void setThreadId(Integer threadId) {
		this.threadId = threadId;
	}

	public static List<WorkerStatus> listAvailWorkers(EntityManager em) {
		return em.createNamedQuery(
				"WorkerStatus.listIdleInstances", WorkerStatus.class)
				.getResultList();
	}

	public static void cleanOldRecords(EntityManager postgres) {
		try {
			postgres.getTransaction().begin();
			postgres.createNamedQuery("WorkerStatus.cleanOldRecords").
			setParameter("last3months", CommonUtil.nextNMinutes(-60*24*90)).executeUpdate();
			postgres.getTransaction().commit();
		} finally
		{
			if (postgres.getTransaction().isActive())
				postgres.getTransaction().rollback();
		}
	}


}