package model;

import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import com.google.gson.annotations.Expose;


@Entity
@Cache(
		type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
		size = 4000, // Use 4,000 as the initial cache size.
		expiry = 3000, // 3sec
		coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
	)
@Table(name="mediabook_subtitle")
public class MediabookSubtitle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	@Expose
	private Long id;
	
	@Mutable
	@JoinColumn(name = "chapter_id", referencedColumnName = "id") 
	@ManyToOne
	@JsonIgnore
	private MediabookChapter mediabookChapter;
	
	@Mutable
	@Column(name = "book_file_id")
	@Expose
	private Long bookFileId;
	
	@Mutable
	@Column(name = "item_id")
	@Expose
	private String item;
	
	@Mutable
	@Column(name = "chapter_id", insertable = false, updatable = false)
	@Expose
	private Long chapterId;
	
	@Mutable
	@Column(name = "chapter_no")
	@Expose
	private Long chapterNo;
	
	@Mutable
	@Column(name = "version")
	@Expose
	private String version;
	
	@Mutable
	@Column(name = "subtitle_id")
	@Expose
	private String subtitleId;
	
	@Mutable
	@Column(name = "subtitle_file")
	@Expose
	private String subtitleFile;
	
	@Mutable
	@Column(name = "src_fname")
	@Expose
	private String srcFname;
	
	@Mutable
	@Column(name = "file_location")
	@Expose
	private String fileLocation;
	
	@Mutable
	@Column(name = "status")
	@Expose
	private int status;
	
	@Mutable
	@Column(name = "deleted")
	@Expose
	private Boolean deleted;
	
	@Mutable
	@Column(name = "size")
	@Expose
	private int size;
	
	@Mutable
	@Column(name = "operator")
	@Expose
	private String operator;
	
	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	private Date create_time;
	
	
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	protected Date lastUpdated;
	
	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Long getChapterId() {
		return chapterId;
	}

	public void setChapterId(Long chapterId) {
		this.chapterId = chapterId;
	}

	public Long getChapterNo() {
		return chapterNo;
	}

	public void setChapterNo(Long chapterNo) {
		this.chapterNo = chapterNo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSubtitleId() {
		return subtitleId;
	}

	public void setSubtitleId(String subtitleId) {
		this.subtitleId = subtitleId;
	}

	public String getSubtitleFile() {
		return subtitleFile;
	}

	public void setSubtitleFile(String subtitleFile) {
		this.subtitleFile = subtitleFile;
	}

	public String getSrcFname() {
		return srcFname;
	}

	public void setSrcFname(String srcFname) {
		this.srcFname = srcFname;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public MediabookChapter getMediabookChapter() {
		return mediabookChapter;
	}

	public void setMediabookChapter(MediabookChapter mediabookChapter) {
		this.mediabookChapter = mediabookChapter;
	}

	public Long getBookFileId() {
		return bookFileId;
	}

	public void setBookFileId(Long bookFileId) {
		this.bookFileId = bookFileId;
	}
	
	
}
