package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;


@Entity
@Cache(
		  type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
		  size=4000,  // Use 4,000 as the initial cache size.
		  expiry=300000,  // 5min 
		  coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
		)
@Table(name="like_hotbook_feedback")
@NamedQueries({
	@NamedQuery(name="LikeHotbookFeedback.findAll",query="SELECT m FROM LikeHotbookFeedback m"),
	@NamedQuery(name="LikeHotbookFeedback.findByMemberIdAndMBNId",
	query="SELECT hbf FROM LikeHotbookFeedback hbf "
			+ " WHERE hbf.memberId=:memberId AND hbf.memberBookNoteId=:memberBookNoteId"),
	@NamedQuery(name="LikeHotbookFeedback.findByMemberId",
	query="SELECT hbf FROM LikeHotbookFeedback hbf "
			+ " WHERE hbf.memberId=:memberId")
})

public class LikeHotbookFeedback extends digipages.common.BasePojo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;
	
	@Mutable
	@Column(name="action")
	private String action;
	
	@Mutable
	@Column(name="member_id")
	private Long memberId;
	
	@Mutable
	@Column(name="member_book_note_id")
	private Long memberBookNoteId;
	
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;
	
	@PrePersist
	@PreUpdate
	public void onPrePersist() {

		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long long1) {
		this.memberId = long1;
	}

	public Long getMemberBookNoteId() {
		return memberBookNoteId;
	}

	public void setMemberBookNoteId(Long long1) {
		this.memberBookNoteId = long1;
	}
	
	
}
