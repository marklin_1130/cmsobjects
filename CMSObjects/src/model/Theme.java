package model;

import java.io.Serializable;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import java.sql.Timestamp;


/**
 * The persistent class for the theme database table.
 * 
 */
@Entity
@Cache(
		  type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
		  size=4000,  // Use 4,000 as the initial cache size.
		  expiry=300000,  // 5min 
		  coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
		)
@NamedQuery(name="Theme.findAll", query="SELECT t FROM Theme t")
public class Theme implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String author;

	private String description;

	@Mutable
	@Column(name="device_type")
	private String deviceType;

	@Mutable
	@Column(name="file_uri")
	private String fileUri;

	@Mutable
	@Column(name="icon_uri")
	private String iconUri;

	private String name;

	@Mutable
	@Column(name="thumbnail01_uri")
	private String thumbnail01Uri;

	@Mutable
	@Column(name="thumbnail02_uri")
	private String thumbnail02Uri;

	@Mutable
	@Column(name="thumbnail03_uri")
	private String thumbnail03Uri;

	private Timestamp udt;

	public Theme() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeviceType() {
		return this.deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getFileUri() {
		return this.fileUri;
	}

	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}

	public String getIconUri() {
		return this.iconUri;
	}

	public void setIconUri(String iconUri) {
		this.iconUri = iconUri;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getThumbnail01Uri() {
		return this.thumbnail01Uri;
	}

	public void setThumbnail01Uri(String thumbnail01Uri) {
		this.thumbnail01Uri = thumbnail01Uri;
	}

	public String getThumbnail02Uri() {
		return this.thumbnail02Uri;
	}

	public void setThumbnail02Uri(String thumbnail02Uri) {
		this.thumbnail02Uri = thumbnail02Uri;
	}

	public String getThumbnail03Uri() {
		return this.thumbnail03Uri;
	}

	public void setThumbnail03Uri(String thumbnail03Uri) {
		this.thumbnail03Uri = thumbnail03Uri;
	}

	public Timestamp getUdt() {
		return this.udt;
	}

	public void setUdt(Timestamp udt) {
		this.udt = udt;
	}

}