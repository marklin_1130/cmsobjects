package model;

public class MagInfo extends BaseBookInfo {
	private String issn;
	private String mag_id;
	private String issue;
	private String issue_year;
	private String publish_type;
	private String cover_man;
	private String cover_story;
	private String display_file_size="";

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public String getMag_id() {
		return mag_id;
	}

	public void setMag_id(String mag_id) {
		this.mag_id = mag_id;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getIssue_year() {
		return issue_year;
	}

	public void setIssue_year(String issue_year) {
		this.issue_year = issue_year;
	}

	public String getPublish_type() {
		return publish_type;
	}

	public void setPublish_type(String publish_type) {
		this.publish_type = publish_type;
	}

	public String getCover_man() {
		return cover_man;
	}

	public void setCover_man(String cover_man) {
		this.cover_man = cover_man;
	}

	public String getCover_story() {
		return cover_story;
	}

	public void setCover_story(String cover_story) {
		this.cover_story = cover_story;
	}
	
	public String getDisplay_file_size() {
		return display_file_size;
	}

	public void setDisplay_file_size(String display_file_size) {
		this.display_file_size = display_file_size;
	}
}