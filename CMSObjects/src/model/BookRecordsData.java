package model;

import java.util.List;

public class BookRecordsData {
	List<BookRecords> book_records;

	public List<BookRecords> getRecords() {
		return book_records;
	}

	public void setRecords(List<BookRecords> book_records) {
		this.book_records = book_records;
	}
}