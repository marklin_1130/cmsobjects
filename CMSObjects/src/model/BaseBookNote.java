package model;

public class BaseBookNote {
	private String book_format;
	private String type; // 類別：閱讀心得、劃線筆記、書籤
	private String version; // 書籍版號
	private String color; // 顏色
	private Integer spine; // 閱讀順序

	public String getBook_format() {
		return book_format;
	}

	public void setBook_format(String book_format) {
		this.book_format = book_format;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getSpine() {
		return spine;
	}

	public void setSpine(Integer spine) {
		this.spine = spine;
	}
}