package model;

import com.google.gson.annotations.Expose;

public class MediaBookInfo extends BaseBookInfo {
    @Expose
    private String isbn;
    @Expose
    private String author;
    @Expose
    private String o_author; // 原文作者
    @Expose
    private String translator;
    @Expose
    private String editor;
    @Expose
    private String illustrator;
    @Expose
    private String edition;
    @Expose
    private String foreword; // 序
    @Expose
    private String intro;
    @Expose
    private String reader="";
    @Expose
    private String author_reading="";
    @Expose
    private String p_convertstatus="";
    @Expose
    private String info_package="";
    @Expose
    private String e_convertstatus="";
    @Expose
    private String speaker="";
    @Expose
    private Long total_play_time;
    @Expose
    private Long total_preview_play_time;
    @Expose
    private Long total_size;
    @Expose
    private Long total_preview_size;
    
    private String display_file_size="";

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getO_author() {
		return o_author;
	}

	public void setO_author(String o_author) {
		this.o_author = o_author;
	}

	public String getTranslator() {
		return translator;
	}

	public void setTranslator(String translator) {
		this.translator = translator;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getIllustrator() {
		return illustrator;
	}

	public void setIllustrator(String illustrator) {
		this.illustrator = illustrator;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getForeword() {
		return foreword;
	}

	public void setForeword(String foreword) {
		this.foreword = foreword;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}
	
	public String getReader() {
		return reader;
	}

	public void setReader(String reader) {
		this.reader = reader;
	}

	public String getAuthor_reading() {
		return author_reading;
	}

	public void setAuthor_reading(String author_reading) {
		this.author_reading = author_reading;
	}

	public String getP_convertstatus() {
		return p_convertstatus;
	}

	public void setP_convertstatus(String p_convertstatus) {
		this.p_convertstatus = p_convertstatus;
	}

	public String getInfo_package() {
		return info_package;
	}

	public void setInfo_package(String info_package) {
		this.info_package = info_package;
	}

	public String getE_convertstatus() {
		return e_convertstatus;
	}

	public void setE_convertstatus(String e_convertstatus) {
		this.e_convertstatus = e_convertstatus;
	}

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public Long getTotal_play_time() {
        return total_play_time;
    }

    public void setTotal_play_time(Long total_play_time) {
        this.total_play_time = total_play_time;
    }

    public Long getTotal_preview_play_time() {
        return total_preview_play_time;
    }

    public void setTotal_preview_play_time(Long total_preview_play_time) {
        this.total_preview_play_time = total_preview_play_time;
    }

    public Long getTotal_size() {
        return total_size;
    }

    public Long getTotal_preview_size() {
        return total_preview_size;
    }

    public String getDisplay_file_size() {
        return display_file_size;
    }

    public void setTotal_size(Long total_size) {
        this.total_size = total_size;
    }

    public void setTotal_preview_size(Long total_preview_size) {
        this.total_preview_size = total_preview_size;
    }

    public void setDisplay_file_size(String display_file_size) {
        this.display_file_size = display_file_size;
    }
	
}