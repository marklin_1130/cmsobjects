package model;

import java.io.Serializable;
import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the gps database table.
 * 
 */
@Entity
@Cache(
		  type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
		  size=4000,  // Use 4,000 as the initial cache size.
		  expiry=300000,  // 5min 
		  coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
		)
@NamedQuery(name="Gps.findAll", query="SELECT g FROM Gps g")
public class Gps implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Mutable
	@Column(name = "coordinate")
	private String coordinate;

	@Mutable
	@Column(name="member_device_id")
	private Long memberDeviceId;

	@Mutable
	@Column(name="session_id")
	private String sessionId;

	@Mutable
	@Column(name="udt")
	@Temporal(TemporalType.TIMESTAMP)
	private Date udt;

	public Gps() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCoordinate() {
		return this.coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public Long getMemberDeviceId() {
		return this.memberDeviceId;
	}

	public void setMemberDeviceId(Long memberDeviceId) {
		this.memberDeviceId = memberDeviceId;
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Date getUdt() {
		return this.udt;
	}

	public void setUdt(Date udt) {
		this.udt = udt;
	}

}