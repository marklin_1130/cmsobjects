package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.dev.utility.BookUpgrader;
import digipages.exceptions.ServerException;

/**
 * The persistent class for the member database table.
 * 
 */
@Entity
@Cache(
    type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
    size = 4000, // Use 4,000 as the initial cache size.
    expiry = 3000, // 3 sec
    coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
)
@Table(name="member")
@NamedQueries({
    @NamedQuery(name="Member.findAll", query="SELECT m FROM Member m"),
    @NamedQuery(name="Member.findByBmemberIdnType", 
        query="SELECT m FROM Member m where m.bmemberId = :bmemberid and m.memberType = :memberType "
                + "Order by m.id asc"),
    @NamedQuery(name="Member.findByBmemberId", 
    query="SELECT m FROM Member m where m.bmemberId = :bmemberid"),
    @NamedQuery(name="Member.findByMemberId", 
        query="SELECT m FROM Member m where m.id = :memberId"),
    @NamedQuery(name="Member.findFirstGuestByDeviceId",
        query="SELECT distinct m from Member m JOIN m.memberDevices md  "
                + "where m.memberType='Guest' and "
                + "md.deviceId = :deviceId"),
    @NamedQuery(name="Member.findAllNormalReader", 
        query ="SELECT m from Member m "
                + "where m.memberType = 'NormalReader' AND "
                + " m.bmemberId is not null AND"
                + " m.bmemberId not like 'bench-sample%'"),
                
    @NamedQuery(name="Member.findThirdPartyReader", 
    query ="SELECT m from Member m where m.memberType != 'NormalReader' AND "
            + " m.bmemberId is not null"),
    @NamedQuery(name="Member.findThumbGenerator", 
    query ="SELECT m from Member m where m.memberType = 'SuperReader' AND "
            + " m.bmemberId = 'nologin-ThumbGenerator'")
})
@NamedNativeQuery(name="Member.findRandomMember", 
    query="select * from member where "
            + "member_type='NormalReader' "
            + "and bmember_id is not null "
            + "order by random() limit 1", resultClass=Member.class)
public class Member extends digipages.common.BasePojo implements Serializable {
    
    private static DPLogger logger =DPLogger.getLogger(Member.class.getName());
    
    private static final long serialVersionUID = 1L;

    public static final String MEMBERTYPE_GUEST = "Guest";
    public static final String MEMBERTYPE_NORMALREADER = "NormalReader";
    public static final String MEMBERTYPE_VNDORREADER = "VendorReader";
    public static final String MEMBERTYPE_PUBLISHERREADER = "PublisherReader";
    public static final String MEMBERTYPE_SUPERREADER = "SuperReader";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false)
    private Long id;
    
    @Mutable
    @Column(name="bmember_id")
    private String bmemberId;
    
    @Mutable
    @Column(name="member_type")
    private String memberType;

    @Mutable
    @Column(name="name")
    private String name;
    
    @Mutable
    @Column(name="nick")
    private String nick="";
    
    @Mutable
    @Column(name="device_cnt_limit")
    private int deviceCntLimit=5;

    @Mutable
    @Column(name="operator")
    private String operator;

    //bi-directional many-to-one association to Publisher
    @Mutable
    @JoinColumn(name="publisher_id")
    @ManyToOne
    @JsonIgnore
    private Publisher publisher;

    //bi-directional many-to-one association to Vendor
    @Mutable
    @JoinColumn(name="vendor_id")
    @ManyToOne
    @JsonIgnore
    private Vendor vendor;

    //bi-directional many-to-one association to MemberBook
    @Mutable
    @OneToMany(mappedBy="member", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<MemberBook> memberBooks=new ArrayList<>();

    //bi-directional many-to-one association to MemberDevice
    @Mutable
    @OneToMany(mappedBy="member", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<MemberDevice> memberDevices= new ArrayList<>();

    //bi-directional many-to-one association to MemberReadList
    @Mutable
    @OneToMany(mappedBy="member", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<MemberReadList> memberReadLists = new ArrayList<>();
    
    //bi-directional many-to-one association to Message
    @Mutable
    @OneToMany(mappedBy="member", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<Message> message = new ArrayList<>();

    @Mutable
    @OneToMany(mappedBy="member", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<MemberDrmLog> memberDrmLogs=new ArrayList<>();

    @Mutable
    @Column(name = "udt")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;

    @PrePersist
    @PreUpdate
    public void onPrePersist() {

        if (lastUpdated == null) {
            lastUpdated = new Date();
            setLastUpdated(lastUpdated);
        }
    }

    public Date getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(Date lastUpdateTime) {
        this.lastUpdated = lastUpdateTime;
    }

    public void setLastUpdated(String lastUpdateTime) {
        ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
        setLastUpdated(Date.from(zdt.toInstant()));
    }

    public Member() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBmemberId() {
        return this.bmemberId;
    }

    public void setBmemberId(String bmemberId) {
        this.bmemberId = bmemberId;
    }

    public String getMemberType() {
        return this.memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (null!=bmemberId && null !=name  && !bmemberId.startsWith("bench_") && 
                !name.equals(bmemberId))
        {
//            logger.error("Eric Debug: no matchd name/bmemberID, " + name + "/" + bmemberId);
            Exception e = new Exception();
            logger.error(e);;
        }
        this.name = name;
    }

    public String getNick() {
        return this.nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Publisher getPublisher() {
        return this.publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Vendor getVendor() {
        return this.vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public List<MemberBook> getMemberBooks() {
        return this.memberBooks;
    }

    public void setMemberBooks(List<MemberBook> memberBooks) {
        this.memberBooks = memberBooks;
    }

    public MemberBook addMemberBook(MemberBook memberBook) {
        // getMemberBooks().add(memberBook); // 效能問題
        memberBook.setMember(this);
        
        return memberBook;
    }

    public MemberBook addBenchMemberBook(MemberBook memberBook) {
        getMemberBooks().add(memberBook);
        memberBook.setMember(this);
        
        return memberBook;
    }

    public MemberBook removeMemberBook(MemberBook memberBook) {
        getMemberBooks().remove(memberBook);
        memberBook.setMember(null);

        return memberBook;
    }

    public List<MemberDevice> getMemberDevices() {
        return this.memberDevices;
    }

    public void setMemberDevices(List<MemberDevice> memberDevices) {
        this.memberDevices = memberDevices;
    }

    public MemberDevice addMemberDevice(MemberDevice memberDevice) throws ServerException{
        if (memberDevice==null)
        {
            throw new ServerException("id_err_999","Check RD for bug");
        }
        // do nothing if already have it.
        if (hasDeviceByDeviceId(memberDevice.getDeviceId())){
            memberDevice.setStatus(MemberDevice.STATUSLOGIN);
            return memberDevice;
        }
        
        // real add
        // check device name duplication.
        fixMemberDeviceName(memberDevice);
        getMemberDevices().add(memberDevice);
        memberDevice.setMember(this);
        if (!memberDevice.isWeb()){
            checkDeviceCnt(); 
        }

        return memberDevice;
    }
    
    public void checkDeviceCnt() throws ServerException {
        int deviceCnt=getActiveMemberDeviceCount();
        
        if (deviceCnt>getDeviceCntLimit())
        {
            throw new ServerException("id_err_201"," Device Count " +  deviceCnt+ " exceed limit " + getDeviceCntLimit());
        }
    }

    /**
     * if memberDevice has name same as other device, change this name to #2, #3....
     * @param memberDevice
     */
    private void fixMemberDeviceName(MemberDevice memberDevice) {
    
        List <String> deviceNames = getDeviceNames();
        String curName = memberDevice.getDeviceName();
        if (CommonUtil.hasDuplicate(deviceNames, curName)){
            String suggestDeviceName = CommonUtil.genNewName(deviceNames
                    ,curName);
            memberDevice.setDeviceName(suggestDeviceName);
        }
    }

    private List<String> getDeviceNames() {
        List <String> ret =new ArrayList<String>();
        for (MemberDevice md:getMemberDevices())
        {
            ret.add(md.getDeviceName());
        }
        return ret;
    }

    /**
     * for first time device name generation.
     * @param curName
     * @return
     */


    /**
     * get Active MemberDevice, not include Web
     * @return
     */
    public int getActiveMemberDeviceCount() {
        int cnt=0;
        for (MemberDevice md:getMemberDevices())
        {
            if (md==null){
                logger.error("Warning MemberDevice Null: " + this.getId());
                continue;
            }
            if (md.getOsType()==null)
            {
                logger.error("Warning Device Without OSType ?!" + md.getDeviceId());
                continue;
            }
            if (md.getOsType().toLowerCase().contains("web"))
                continue;
            if (md.getStatus()==MemberDevice.STATUSLOGIN)
                cnt++;
        }
        return cnt;
    }

    private boolean hasDeviceByDeviceId(String deviceId) {
        for (MemberDevice tmpDevice:getMemberDevices())
        {
            if (tmpDevice==null)
                continue;
            if (deviceId.equals(tmpDevice.getDeviceId()))
                return true;
        }
        return false;
        
    }

    public int getDeviceCntLimit() {
        return deviceCntLimit;
    }

    public void setDeviceCntLimit(int cnt)
    {
        this.deviceCntLimit=cnt;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public MemberDevice removeMemberDevice(MemberDevice memberDevice) {
        memberDevice.setStatus(MemberDevice.STATUSLOGOUT);
        getMemberDevices().remove(memberDevice);

        return memberDevice;
    }

    public List<MemberReadList> getMemberReadLists() {
        return this.memberReadLists;
    }

    public void setMemberReadLists(List<MemberReadList> memberReadLists) {
        this.memberReadLists = memberReadLists;
    }

    public MemberReadList addMemberReadList(MemberReadList memberReadList) {
        getMemberReadLists().add(memberReadList);
        memberReadList.setMember(this);

        return memberReadList;
    }

    public MemberReadList removeMemberReadList(MemberReadList memberReadList) {
        getMemberReadLists().remove(memberReadList);
        memberReadList.setMember(null);

        return memberReadList;
    }

    public List<Message> getMessage() {
        return this.message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Message addMessage(Message message) {
        getMessage().add(message);
        message.setMember(this);

        return message;
    }

    public Message removeMessage(Message message) {
        getMessage().remove(message);
        message.setMember(null);

        return message;
    }
    
    public List<MemberDrmLog> getMemberDrmLogs() {
        return this.memberDrmLogs;
    }

    public void setMemberDrmLog(List<MemberDrmLog> MemberDrmLog) {
        this.memberDrmLogs = MemberDrmLog;
    }

    public MemberDrmLog addMemberDrmLog(MemberDrmLog memberDrmLog) {
//      getMemberDrmLogs().add(memberDrmLog);//TODO 效能問題註解
        memberDrmLog.setMember(this);

        return memberDrmLog;
    }

    public MemberDrmLog removeTrialMemberDrmLog(MemberDrmLog memberDrmLog) {
        getMemberDrmLogs().remove(memberDrmLog);
        memberDrmLog.setMember(null);

        return memberDrmLog;
    }



    public MemberDevice getMemberDeviceByID(Long deviceId) {
        for (MemberDevice tmpDevice:getMemberDevices())
        {
            if (tmpDevice.getId().longValue()==deviceId.longValue())
                return tmpDevice;
        }
        return null;
    }

    // TODO performance improve by using query.
    public MemberDevice getMemberDeviceByDeviceID(String deviceID) {
        MemberDevice myDevice=null;
        for (MemberDevice device:getMemberDevices()){
            if (device==null)
                continue;
            if (deviceID.equals(device.getDeviceId()))
            {
                myDevice=device;
            }
        }
        return myDevice;
    }

    // generate 
    public void genDefaultReadLists(EntityManager connection)
    {
        if (!getMemberReadLists().isEmpty())
        {

            boolean isMediaBookExist = false;
            boolean isAudioBookExist = false;
            for (MemberReadList memberReadList : getMemberReadLists()) {
                
                if("mediabook".equalsIgnoreCase(memberReadList.getIdname())) {
                    isMediaBookExist = true;
                }
                
                if("audiobook".equalsIgnoreCase(memberReadList.getIdname())) {
                    isAudioBookExist = true;
                }
            }
            
            if(!isMediaBookExist) {
                MemberReadList listMediabook = new MemberReadList("mediabook","影音書",this);
                connection.persist(listMediabook);
                getMemberReadLists().add(listMediabook);
            }
            if(!isAudioBookExist) {
                MemberReadList listAudiobook = new MemberReadList("audiobook","有聲書",this);
                connection.persist(listAudiobook);
                getMemberReadLists().add(listAudiobook);
            }
            return;
        }
        
        MemberReadList listAll = new MemberReadList("all","已購買",this);
        connection.persist(listAll);
        getMemberReadLists().add(listAll);
        
        MemberReadList listBook = new MemberReadList("book","書籍",this);
        connection.persist(listBook);
        getMemberReadLists().add(listBook);
        
        MemberReadList listMag = new MemberReadList("magazine","雜誌",this);
        connection.persist(listMag);
        getMemberReadLists().add(listMag);

        MemberReadList listMediabook = new MemberReadList("mediabook","影音書",this);
        connection.persist(listMediabook);
        getMemberReadLists().add(listMediabook);
        
        MemberReadList listAudiobook = new MemberReadList("audiobook","有聲書",this);
        connection.persist(listAudiobook);
        getMemberReadLists().add(listAudiobook);
        
        MemberReadList listTrial = new MemberReadList("trial","試閱",this);
        listTrial.setReadonly(false);
        connection.persist(listTrial);
        getMemberReadLists().add(listTrial);

        MemberReadList listPrivate = new MemberReadList("private","密碼書單",this);
        listPrivate.setReadonly(false);
        connection.persist(listPrivate);
        getMemberReadLists().add(listPrivate);

        MemberReadList listArchive = new MemberReadList("archive","封存書單",this);
        listArchive.setReadonly(false);
        connection.persist(listArchive);
        getMemberReadLists().add(listArchive);
        

    }
    
    // generate 
    public void genDefaultReadLists()
    {
        if (!getMemberReadLists().isEmpty())
        {
            return;
        }
        
        MemberReadList listAll = new MemberReadList("all","已購買",this);
        getMemberReadLists().add(listAll);
        
        MemberReadList listBook = new MemberReadList("book","書籍",this);
        getMemberReadLists().add(listBook);
        
        MemberReadList listMag = new MemberReadList("magazine","雜誌",this);
        getMemberReadLists().add(listMag);

        MemberReadList listMediabook = new MemberReadList("mediabook","影音書",this);
        getMemberReadLists().add(listMediabook);
        
        MemberReadList listAudiobook = new MemberReadList("audiobook","有聲書",this);
        getMemberReadLists().add(listAudiobook);
        
        MemberReadList listTrial = new MemberReadList("trial","試閱",this);
        listTrial.setReadonly(false);
        getMemberReadLists().add(listTrial);

        MemberReadList listPrivate = new MemberReadList("private","密碼書單",this);
        listPrivate.setReadonly(false);
        getMemberReadLists().add(listPrivate);

        MemberReadList listArchive = new MemberReadList("archive","封存書單",this);
        listArchive.setReadonly(false);
        getMemberReadLists().add(listArchive);

    }


    public MemberDevice getDeviceByDeviceId(String deviceId)
    {
        for (MemberDevice md:getMemberDevices())
        {
            if (deviceId.equals(md.getDeviceId()))
                return md;
        }
        return null;
    }

    /**
     * @param curBookFileId
     * @return
     */
    public MemberBook getMemberBookByBookFileId(EntityManager em,int curBookFileId) {
        
        MemberBook memberBook=null;
        try {
            memberBook=MemberBook.findByBookFileId(em, this.getId(), curBookFileId);
        }catch (Exception ex)
        {
            
        }

        if(memberBook!=null &&  curBookFileId == memberBook.getBookFile().getId() ){

            // this book has no readlist. (happened when book is first time trial)
            if( memberBook.getReadlistIdnames().isEmpty() ){
                memberBook.addToReadList("trial");
                memberBook.setLastUpdated(new Date());
                memberBook.setMemberBookNoteBack(CommonUtil.zonedTime());
            }
            return memberBook;
        }
        
        return null;
    }
    
    

    public static Member getRandomMember(EntityManager em) {
        Member m=(Member) em.createNamedQuery("Member.findRandomMember").getSingleResult();
        return m;
    }
    
    public static Member getMemberByBMemberId(EntityManager em, String bMemberId) throws ServerException {
        try {
            return em.createNamedQuery("Member.findByBmemberId", Member.class)
            .setParameter("bmemberid", bMemberId)
            .setMaxResults(1)
            .getResultList().get(0);
        } catch (java.lang.ArrayIndexOutOfBoundsException e)
        {
            // find nothing = null => do new.
            return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static Member getMemberByBMemberId(EntityManager em, String bMemberId, String memberType) throws ServerException {
        try {
            return em.createNamedQuery("Member.findByBmemberIdnType", Member.class)
            .setParameter("bmemberid", bMemberId)
            .setParameter("memberType", memberType)
            .setMaxResults(1)
            .getResultList().get(0);
        } catch (java.lang.ArrayIndexOutOfBoundsException e)
        {
            // find nothing = null => do new.
            return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Member getMemberByMemberId(EntityManager em, Integer memberId) throws ServerException {
        try {
            return em.createNamedQuery("Member.findByMemberId", Member.class).setParameter("memberId", memberId).getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
//          throw new ServerException("id_err_204", " Account not exists.");
        }
    }

    public static Member getGuestByDeviceId(EntityManager em,String parameter) {
        
        try {
            return em.createNamedQuery("Member.findFirstGuestByDeviceId", Member.class).setParameter("deviceId", parameter).getSingleResult();
        } catch (Exception ex)
        {
            
        }
        return null;
    }

    public static List<Member> listNormalReaderMembers(EntityManager em) {
        List <Member> memberList=
                em.createNamedQuery("Member.findAllNormalReader", Member.class)
                .getResultList();
        return memberList;
    }
    
    public static List<Member> listThirdPartyReaderMembers(EntityManager em) {
        List <Member> memberList=
                em.createNamedQuery("Member.findThirdPartyReader", Member.class)
                .getResultList();
        return memberList;
    }

    /**
     * 
     * @param itemId
     * @return
     */
    public MemberBook getMemberTrialBookByItemId(EntityManager em,String itemId) {
        MemberBook mb=MemberBook.findTrialByItemId(em,this.getId(),itemId);
        return mb;
    }
    
    /**
     * 
     * @param itemId
     * @return
     */
    public MemberBook getMemberNormalBookByItemId(EntityManager em,String itemId) {
        MemberBook mb=MemberBook.findNormalByItemId(em,this.getId(),itemId);
        return mb;
    }

    public void mergeFrom(EntityManager em,Member guestMember) {
        mergeMessageFrom(guestMember);
        mergeReadListFrom(guestMember);
        mergeMemberBooksFrom(em,guestMember);
        mergeDrmLogsFrom(guestMember);
        mergeDevicesFrom(em,guestMember);
        
    }
    
    private void mergeDevicesFrom(EntityManager em,Member guestMember) {
        List <MemberDevice> removeList = new ArrayList<>();
        List <MemberDevice> addList = new ArrayList<>();
        for (MemberDevice tmpDevice:guestMember.getMemberDevices())
        {
            removeList.add(tmpDevice);
            if (!this.hasDeviceByDeviceId(tmpDevice.getDeviceId()))
            {
                addList.add(tmpDevice);
            }
        }
        
        for (MemberDevice tmpDevice:removeList)
        {
            guestMember.removeMemberDevice(tmpDevice);
            em.remove(tmpDevice);
        }
        
        for (MemberDevice tmpDevice:addList)
        {
            try {
                addMemberDevice(tmpDevice);
            }catch (Exception ex)
            {
                // exceed limit => just delete it.
                guestMember.removeMemberDevice(tmpDevice);
            }
        }
    }


    private void mergeMessageFrom(Member guestMember) {
        List <Message> removeList = new ArrayList<>();
        List <Message> addList = new ArrayList<>();
        for (Message tmpMemberMessage:guestMember.getMessage())
        {
            // remove duplication
            removeList.add(tmpMemberMessage);
            if (!hasMessage(tmpMemberMessage))
            {
                addList.add(tmpMemberMessage);
            }
        }
        for (Message tmpMemberMessage:removeList)
        {
            guestMember.removeMessage(tmpMemberMessage);
        }
        for (Message tmpMemberMessage:addList)
        {
            this.addMessage(tmpMemberMessage);
        }
    }

    private boolean hasMessage(Message tmpMemberMessage) {
        for (Message tmpMyMsg:this.getMessage())
        {
            if (tmpMyMsg.getContent().equals(tmpMemberMessage.getContent()))
                return true;
        }
        return false;
    }

    public void mergeReadListFrom(Member guestMember) {
        
        List <MemberReadList> removeList = new ArrayList<>();
        List <MemberReadList> addList = new ArrayList<>();
        for (MemberReadList tmpList:guestMember.getMemberReadLists())
        {
            removeList.add(tmpList);
            if (!hasReadList(tmpList))
            {
                addList.add(tmpList);
            }
        }
        
        for (MemberReadList tmpList:removeList)
        {
            guestMember.removeMemberReadList(tmpList);
        }
        for (MemberReadList tmpList:addList)
        {
            this.addMemberReadList(guestMember.removeMemberReadList(tmpList));
        }
    }

    private boolean hasReadList(MemberReadList tmpList) {
        for (MemberReadList readList:getMemberReadLists())
        {
            if (readList.getIdname().equals(tmpList))
                return true;
        }
        return false;
    }

    private void mergeDrmLogsFrom(Member guestMember) {
        List<MemberDrmLog> removeList=new ArrayList<>();
        List<MemberDrmLog> addList = new ArrayList<>();
        for (MemberDrmLog tmplog:guestMember.getMemberDrmLogs())
        {
            removeList.add(tmplog);
            if (!hasDrmLog(tmplog))
            {
                addList.add(tmplog);
            }
        }
        
        for (MemberDrmLog tmplog:removeList){
            guestMember.removeTrialMemberDrmLog(tmplog);
        }
        
        for (MemberDrmLog tmplog:addList)
        {
            this.addMemberDrmLog(tmplog);
        }
    }

    
    /**
     * if this member has similar drmLog, used by merge. 
     * @param drmLog
     * @return
     */
    public boolean hasDrmLog(MemberDrmLog drmLog) {
        // SomeDay  since merge guest won't hit me, this is empty by default.
        return false;
    }

    public void mergeMemberBooksFrom(EntityManager em,Member guestMember) {
        List<MemberBook> removeList=new ArrayList<>();
        List<MemberBook> addList=new ArrayList<>();
        for (MemberBook tmpBook:guestMember.getMemberBooks())
        {
            removeList.add(tmpBook);
            MemberBook mb =null;
            if(this.id!=null) {
            	mb = getMemberBookByBUID(em,tmpBook.getBookUniId());
            }
            if (mb==null)
            {
                addList.add(tmpBook);
                continue;
            }
            
            //skip too old ones.
            if (tmpBook.getLastUpdated().before(mb.getLastUpdated())) {
                continue;
            }
            
            // matched, try to merge book notes
            mb.mergeNotes(tmpBook);
            // undelete ( the if statement will reduce write (performance))
            if (mb.isDeleted())
            {
                mb.unDelete();
            }
        }

        // remove from guest (set null)
        for (MemberBook tmpBook:removeList)
        {
            if(tmpBook.getIsTrial()) {
                guestMember.removeMemberBook(tmpBook);
            }else {
                logger.error("member ="+guestMember.getBmemberId() +",memberId="+guestMember.getId()+",memberType="+guestMember.getMemberType()+",MemberBookId:"+ tmpBook.getId()+" prevent remove normal book error, only trial book can be removed.");
            }
        }
        
        // add to read member.
        for (MemberBook tmpBook:addList)
        {
            tmpBook = renewMemberBook(em,tmpBook);

            addMemberBook(tmpBook);
            tmpBook.mergeNotes(tmpBook);
            
            // reset readlist
            try {
                tmpBook.updateByBookFile(tmpBook.getBookFile());
                tmpBook.setDefaultReadList(tmpBook);
            } catch (ServerException e) {
                e.printStackTrace();
            }
            
        }
        
        // real delete guest null items.
        for (MemberBook tmpbook:removeList)
        {
            if (null==tmpbook.getMember()) {
                if(tmpbook.getIsTrial()) {
                    em.remove(tmpbook);
                }else {
                    logger.error("member ="+guestMember.getBmemberId() +",memberId="+guestMember.getId()+",memberType="+guestMember.getMemberType()+",MemberBookId:"+ tmpbook.getId()+" prevent remove normal book error, only trial book can be removed.");
                }
            }
        }
    }
    
    public MemberBook addTrialBook(EntityManager em,BookFile b) throws ServerException
    {
        
        if (null==b)
            return null;
        

        MemberBook mb=this.getMemberBookByBUID(em, b.getBookUniId());
        if (null!=mb)
        {
          mb.setAction("update");
//          mb.setDefaultReadList();
            this.renewMemberBook(em, mb);
            mb.setLastUpdated(new Date());
            mb.setCreateTime(new Date());
            return mb;
        }
        
        mb = new MemberBook();
        
        mb.updateByBookFile(b);
        
        this.addMemberBook(mb);
        em.persist(mb);
        return mb;
    }


    public static Member createNewMember(String bmemberId,String name,String memberType) {
        Member m = new Member();
        m.setBmemberId(bmemberId);
        m.setName(name);
        m.genDefaultReadLists();
        //default
        if (null==memberType || memberType.length()<1)
            m.setMemberType(MEMBERTYPE_NORMALREADER);
        else
            m.setMemberType(memberType);
        return m;
    }

    /**
     * 
     * @param postgres
     * @param booksMemberId
     * @return
     */
    public static Member getOrCreateNormalReader(EntityManager postgres, String booksMemberId) {
        
        return getOrCreateReaderByType(postgres,booksMemberId,"NormalReader");
    }

    public static Member getOrCreateReaderByType(EntityManager postgres, String booksMemberId, String memberType) {
        Member m;

        try {
            m = getMemberByBMemberId(postgres, booksMemberId, memberType);

            if (m == null) {
                throw new ServerException("id_err_204", "Member not exists:" + booksMemberId);
            }
        } catch (ServerException e) {
            m = createNewMember(booksMemberId, booksMemberId, MEMBERTYPE_NORMALREADER);
            postgres.persist(m);
        }

        return m;
    }

    public void logoutWebDevice() {
        List <MemberDevice> memberDevices = getMemberDevices();
        for (MemberDevice md:memberDevices)
        {
            if (md.isWeb()){
                md.setStatus(MemberDevice.STATUSLOGOUT);
            }
        }
    }

    // todo faster version ?!
    public MemberBook getMemberBookByMBId(Long member_book_id) {
        if (null==member_book_id)
            return null;
        for (MemberBook mb:memberBooks)
        {
            if (member_book_id.longValue()==mb.getId().longValue())
                return mb;
        }
        return null;
    }

    // 
    public MemberBook getMemberBookByBUID(EntityManager em, String bookUniId) {
        
        try {
            MemberBook mb=em.createNamedQuery("MemberBook.findByMemberBookUniId",MemberBook.class)
            .setParameter("bookUniId",bookUniId)
            .setParameter("memberId", this.getId())
            .getSingleResult();
            
            return mb;
        } catch (Exception ex)
        {
//          ex.printStackTrace();
            // not found => return null
//          logger.info("Warning Search return no result,  Member.getMemberBookByBUID: " 
//                  + "member:" + getId() + " bookUniId:" + bookUniId);
//          logger.error(ex);;
        }
        return null;
    }
    
    // 
    public MemberBook renewMemberBook(EntityManager em, MemberBook mb) {
        
        try {
            
            // eric: for update fail books, try do find a usable version.
            if (mb.getBookFile().getStatus()!=9)
            {
                BookFile bf =BookFile.findLastVersionByBookUniId(em,mb.getBookUniId());
                mb=BookUpgrader.upgreadMemberBook(em, mb, bf);
            }

            // update author/other info according to last update info.
            if (mb.getLastUpdated().before(mb.getItem().getLastUpdated()))
            {
                mb.setLastUpdated(mb.getItem().getLastUpdated());
                mb.setAction("update");
                for (MemberBookNote bn:mb.getMemberBookNotes())
                {
                    if (bn.getAction().equals("hide"))
                        bn.setAction("update");
                }
            }

            return mb;
        } catch (Exception ex)
        {
            logger.error("fail renew MemberBook",ex);
            // not found => return null
            logger.info("Warning Search return no result,  Member.getMemberBookByBUID: " 
                    + "member:" + getId() + " bookUniId:" + mb.getBookUniId());
            logger.error(ex);;
        }
        return null;
    }

    // for super/thirdpartys
    public boolean hasItemByThirdPartyId(Item item) {
        if (getMemberType().equals("SuperReader"))
            return true;
        if (getVendor()!=null && item.getVendor().getBId().equals(getVendor().getBId()))
            return true;
        if (getPublisher()!=null && item.getPublisher().getBId().equals(getPublisher().getBId()))
            return true;
        return false;
    }

    
    /**
     * 
     * @param item
     * @return
     */
    public boolean hasNormalItem(EntityManager em,Item item) {
        
        if (hasItemByThirdPartyId(item))
            return true;
        
        try {
            MemberBook mb = getMemberNormalBookByItemId(em,item.getId());
            if (mb!=null)
                return true;
        }catch (Exception ex)
        {
            return false;
        }
        
        return false;
    }

    /**
     * TODO performance impact: change to query method
     * @param childs
     * @return
     */
    public List<MemberBook> listChildMemberBooksByItemId(EntityManager emy,List<String> childs) {
        List <MemberBook> ret = new ArrayList<>();
        
        for (MemberBook tmpBook:getMemberBooks())
        {
            String itemId = tmpBook.getItem().getId();
            if (childs.contains(itemId))
                ret.add(tmpBook);
        }
        return ret;
    }

    //TODO performance improve by query.
    public MemberDevice getMemberWebDevice() {
        for (MemberDevice tmpDevice:getMemberDevices())
        {
            if (tmpDevice.isWeb())
                return tmpDevice;
        }
        return null;
    }


    /**
     * notice: this function is mainly for authorization judge. include 3rd party reader.
     * Not suit for other purpose (ex: list book for 3rd party, check for BookDownloadURL...)
     * @param item
     * @return
     */
    public boolean hasTrialItem(EntityManager em,Item item) {
        
        try {
            MemberBook mb = getMemberTrialBookByItemId(em,item.getId());
            if (mb!=null)
                return true;
        }catch (Exception ex)
        {
            return false;
        }
        
        return false;
    }

    
    
    public boolean hasItemByBookUniId(EntityManager em,String bookUniId) throws ServerException {
        if (isSuperReader())
            return true;

        MemberBookFinder finder = new MemberBookFinder(bookUniId);
        finder.find(em, this);
        
        if (isNormalReader() || isGuest())
        {
            MemberBook mb = finder.getMemberBook(); 
            if (mb!=null)
                return true;
        }else  
        {
            Item item = finder.getBookFile().getItem();
            if (isVendor()){
                return getVendor().getBId().equals(item.getVendor().getBId());
            }else if (isPublisher())
            {
                return getPublisher().getBId().equals(item.getPublisher().getBId());
            }else if (isSuperReader())
                return true;
            
        }
        
        // trial book is world accessable. 
        if (finder.getBookFile().getIsTrial())
            return true;
        
        return false;
    }

    public boolean isVendor() {
        if (getMemberType().equals(MEMBERTYPE_VNDORREADER) && getVendor()!=null){
            return true;
        }
        return false;
    }
    
    public boolean isPublisher() {
        if (getMemberType().equals(MEMBERTYPE_PUBLISHERREADER) && getPublisher()!=null){
            return true;
        }
        return false;
    }

    public boolean isSuperReader() {
        if (getMemberType().equals(MEMBERTYPE_SUPERREADER))
        {
            return true;
        }
        return false;
    }

    public boolean isGuest() {
        if (getMemberType().equals(MEMBERTYPE_GUEST))
        {
            return true;
        }
        return false;
    }

    public boolean isNormalReader() {
        if (getMemberType().equals(MEMBERTYPE_NORMALREADER))
        {
            return true;
        }
        
        // error handle isVendor and no vendor id
        if (isVendor() && getVendor()==null)
            return true;
        // error handle isPublisher and no Publisher id
        if (isPublisher() && getPublisher()==null)
            return true;
        
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<MemberDrmLog> listAliveMemberDrms(EntityManager postgres) {
        String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
        String nowDateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

        return postgres.createNativeQuery(
                "SELECT * FROM member_drm_log "
                + "WHERE member_id = " + this.getId() 
                + " AND status = 1 AND read_expire_time >= '" + nowDateTimeString + "'", 
                MemberDrmLog.class).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<MemberDrmLog> listAliveMemberItemDrms(EntityManager postgres, Item item) {
        String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
        String nowDateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

        List<MemberDrmLog> memberDrmLogs = postgres.createNativeQuery("SELECT * FROM member_drm_log WHERE member_id = " + this.getId() + " AND item_id = '" + item.getId() + "' AND read_expire_time >= '" + nowDateTimeString + "' AND status = 1", MemberDrmLog.class).getResultList();

        return memberDrmLogs;
    }

    @SuppressWarnings("unchecked")
    public List<MemberDrmLog> listAliveMemberSerialItemDrms(EntityManager postgres, Item item) {
        String curLocalDateTime = CommonUtil.getCurLocalDateTimeStr("UTC+8");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(curLocalDateTime.substring(0, 19).replace('T', ' '), formatter);
        String nowDateTimeString = String.valueOf(dateTime.getYear()) + "/" + String.format("%02d", dateTime.getMonthValue()) + "/" + String.format("%02d", dateTime.getDayOfMonth()) + " " + String.format("%02d", dateTime.getHour()) + ":" + String.format("%02d", dateTime.getMinute()) + ":" + String.format("%02d", dateTime.getSecond());

        List<MemberDrmLog> memberDrmLogs = postgres.createNativeQuery("SELECT * FROM member_drm_log WHERE member_id = " + this.getId() + " AND item_id IN (SELECT id FROM item WHERE item_type = 'serial' AND childs && '{\"" + item.getId() + "\"}') AND read_expire_time >= '" + nowDateTimeString + "' AND status = 1", MemberDrmLog.class).getResultList();

        return memberDrmLogs;
    }

    public boolean isThirdParty() {
        return isVendor() || isPublisher() || isSuperReader();
    }

    public boolean matchListPassword(String password) {
        for (MemberReadList tmpList:this.getMemberReadLists())
        {
            if (tmpList.getIdname().equals("private"))
            {
                if (password.equals(tmpList.getPassword()))
                    return true;
            }
        }
        return false;
    }
    
    /*
     * for thumbnail generator, if not exists, generate one.  
     */
	public static Member getThumbGenerator(EntityManager postgres) {
		List <Member> memberList=postgres
				.createNamedQuery("Member.findThumbGenerator", Member.class)
				.getResultList();
		if (memberList.size()==0)
		{
			postgres.getTransaction().begin();
			// create no-login member
			postgres.createNativeQuery("insert into member (bmember_id,member_type,operator,name, nick) "
					+ 	"values ('nologin-ThumbGenerator','"+MEMBERTYPE_SUPERREADER+"',1,'nologin-ThumbGenerator','nologin-ThumbGenerator');").executeUpdate();
			postgres.getTransaction().commit();
			
			memberList=postgres
					.createNamedQuery("Member.findThumbGenerator", Member.class)
					.getResultList();

			postgres.getTransaction().begin();
			// create corrosponding device
			postgres.createNativeQuery("insert into member_device (member_id,device_id,language,os_type,os_version,screen_resolution,screen_dpi,device_vendor,device_model,device_name,status)"
					+ "values (?1,'no-login-device','zh-TW','ios','11.3.1','750x1334','326','apple','iPhone','Apple iPhone',2)")
			.setParameter(1, memberList.get(0).getId()).executeUpdate();
			postgres.createNativeQuery("insert into member_device (member_id,device_id,language,os_type,os_version,screen_resolution,screen_dpi,device_vendor,device_model,device_name,status)"
					+ "values (?1,'no-login-device','zh-TW','web','11.3.1','750x1334','326','apple','iPhone','Apple iPhone',2)")
			.setParameter(1, memberList.get(0).getId()).executeUpdate();

			postgres.getTransaction().commit();
			
		}
		return memberList.get(0);
	}
}