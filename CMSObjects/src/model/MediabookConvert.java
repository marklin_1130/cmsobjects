package model;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Mutable;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "mediabook_convert")
public class MediabookConvert {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	@Expose
	private long id;

	@Mutable
    @Column(name = "convert_id")
    @Expose
	private long convertId;
	
	@Mutable
	@Column(name = "convert_status")
	@Expose
	private String convertStatus; 
	
	@Mutable
	@Column(name = "convert_type")
	@Expose
	private String convertType;
	
	@Mutable
	@Column(name = "item_id")
	@Expose
	private String itemId;

	@Mutable
	@Column(name = "book_file_id")
	@Expose
	private long bookFileId;
	
	@Mutable
	@Column(name = "version")
	@Expose
	private String version;
	
	@Mutable
	@Column(name = "chapter_id")
	@Expose
	private long chapterId;

	@Mutable
	@Column(name = "chapter_no")
	@Expose
	private long chapterNo;

	@Mutable
	@Column(name = "chapter_name")
	@Expose
	private String chapterName;

	@Mutable
	@Column(name = "chapter_file")
	@Expose
	private String chapterFile;

	@Mutable
	@Column(name = "is_preview")
	@Expose
	private String ispreview;

	@Mutable
	@Column(name = "preview_type")
	@Expose
	private String previewType;

	@Mutable
	@Column(name = "is_trial")
	@Expose
	private boolean isTrial;

	@Mutable
	@Column(name = "preview_content")
	@Expose
	private String previewContent;

	@Mutable
	@Column(name = "is_script")
	@Expose
	private String isScript;

	@Mutable
	@Column(name = "script_filename")
	@Expose
	private String scriptFilename;

	@Mutable
	@Column(name = "script_file")
	@Expose
	private String scriptFile;

	@Mutable
	@Column(name = "is_subtitle")
	@Expose
	private String isSubtitle;

	@Mutable
	@Column(name = "subtitle_data")
	@Convert(converter = digipages.pgconverter.PGSubtitleConvertInfoConverter.class)
	@Expose
	private SubtitleConvertData subtitleData;
	
	@Mutable
    @Column(name = "attach_id")
    @Expose
    private long attachId;

    @Mutable
    @Column(name = "attach_no")
    @Expose
    private long attachNo;

    @Mutable
    @Column(name = "attach_name")
    @Expose
    private String attachName;

    @Mutable
    @Column(name = "attach_file")
    @Expose
    private String attachFile;
	
	@Mutable
	@Column(name = "status")
	@Expose
	private Integer status;

	@Mutable
	@Column(name = "response_msg")
	@Expose
	private String responseMsg;

	@Mutable
	@Column(name = "deleted")
	@Expose
	private boolean deleted;

	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	private Date createTime = new Date();

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	@Expose
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public long getId() {
		return id;
	}

	public String getConvertStatus() {
		return convertStatus;
	}

	public String getConvertType() {
		return convertType;
	}

	public String getItemId() {
		return itemId;
	}

	public long getBookFileId() {
		return bookFileId;
	}

	public String getVersion() {
		return version;
	}

	public long getChapterId() {
		return chapterId;
	}

	public long getChapterNo() {
		return chapterNo;
	}

	public String getChapterName() {
		return chapterName;
	}

	public String getChapterFile() {
		return chapterFile;
	}

	public String getIspreview() {
		return ispreview;
	}

	public String getPreviewType() {
		return previewType;
	}

	public boolean isTrial() {
		return isTrial;
	}

	public String getPreviewContent() {
		return previewContent;
	}

	public String getIsScript() {
		return isScript;
	}

	public String getScriptFilename() {
		return scriptFilename;
	}

	public String getScriptFile() {
		return scriptFile;
	}

	public String getIsSubtitle() {
		return isSubtitle;
	}

	public SubtitleConvertData getSubtitleData() {
		return subtitleData;
	}

	public Integer getStatus() {
		return status;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setConvertStatus(String convertStatus) {
		this.convertStatus = convertStatus;
	}

	public void setConvertType(String convertType) {
		this.convertType = convertType;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public void setBookFileId(long bookFileId) {
		this.bookFileId = bookFileId;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setChapterId(long chapterId) {
		this.chapterId = chapterId;
	}

	public void setChapterNo(long chapterNo) {
		this.chapterNo = chapterNo;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public void setChapterFile(String chapterFile) {
		this.chapterFile = chapterFile;
	}

	public void setIspreview(String ispreview) {
		this.ispreview = ispreview;
	}

	public void setPreviewType(String previewType) {
		this.previewType = previewType;
	}

	public void setTrial(boolean isTrial) {
		this.isTrial = isTrial;
	}

	public void setPreviewContent(String previewContent) {
		this.previewContent = previewContent;
	}

	public void setIsScript(String isScript) {
		this.isScript = isScript;
	}

	public void setScriptFilename(String scriptFilename) {
		this.scriptFilename = scriptFilename;
	}

	public void setScriptFile(String scriptFile) {
		this.scriptFile = scriptFile;
	}

	public void setIsSubtitle(String isSubtitle) {
		this.isSubtitle = isSubtitle;
	}

	public void setSubtitleData(SubtitleConvertData subtitleData) {
		this.subtitleData = subtitleData;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    public long getAttachId() {
        return attachId;
    }

    public long getAttachNo() {
        return attachNo;
    }

    public String getAttachName() {
        return attachName;
    }

    public String getAttachFile() {
        return attachFile;
    }

    public void setAttachId(long attachId) {
        this.attachId = attachId;
    }

    public void setAttachNo(long attachNo) {
        this.attachNo = attachNo;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public void setAttachFile(String attachFile) {
        this.attachFile = attachFile;
    }

    public long getConvertId() {
        return convertId;
    }

    public void setConvertId(long convertId) {
        this.convertId = convertId;
    }

}
