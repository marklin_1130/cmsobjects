package model;

import java.io.Serializable;
import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import java.sql.Timestamp;
import java.util.Date;

/**
 * The persistent class for the setting database table.
 * 
 */
@Entity
@Cache(
		type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
		size = 4000, // Use 4,000 as the initial cache size.
		expiry = 300000, // 5min
		coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
)
@Table(name="setting")
@NamedQueries({
	@NamedQuery(name="Setting.findAll", query="SELECT s FROM Setting s"),
	@NamedQuery(name="Setting.findByMemberId",
		query="SELECT s FROM Setting s WHERE s.memberId = :memberId"),
})
public class Setting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="member_id")
	private Long memberId;

	@Column(name="setting_data")
	@Convert(converter = digipages.pgconverter.PGGenericJsonContentConverter.class)
	@Mutable
	private Object settingData;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	private Date udt;


	public Setting() {
	}

	public Long getMemberId() {
		return this.memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Object getSettingData() {
		return this.settingData;
	}

	public void setSettingData(Object settingData) {
		this.settingData = settingData;
	}

	public Date getUdt() {
		return this.udt;
	}

	public void setUdt(Date udt) {
		this.udt = udt;
	}

}