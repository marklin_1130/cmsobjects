package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

/**
 * The persistent class for the message_queue database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "message_queue")
@NamedQueries({
	@NamedQuery(
		name = "MessageQueue.findAll",
		query = "SELECT mq FROM MessageQueue mq"
	),
	@NamedQuery(
		name = "MessageQueue.findAllByStatus",
		query = "SELECT mq FROM MessageQueue mq"
				+ " WHERE mq.status = :status"
				+ " ORDER BY mq.lastUpdated ASC"
	),
	@NamedQuery(
		name = "MessageQueue.findAllByMessageId",
		query = "SELECT mq FROM MessageQueue mq"
				+ " WHERE mq.messageId = :messageId"
	),
	@NamedQuery(
		name = "MessageQueue.findAllByTime",
		query = "SELECT mq FROM MessageQueue mq"
				+ " WHERE mq.sendTime = :sendTime"
	),
	@NamedQuery(
		name = "MessageQueue.findAllPromptly",
		query = "SELECT mq FROM MessageQueue mq"
				+ " WHERE mq.messageId = :messageId"
				+ " AND(mq.startDate IS NULL OR mq.expireDate IS NULL OR mq.sendTime IS NULL)"
				+ " AND mq.status = :status"
				+ " ORDER BY mq.lastUpdated DESC"
	),
	@NamedQuery(
		name = "MessageQueue.findAllScheduled",
		query = "SELECT mq FROM MessageQueue mq"
				+ " WHERE (mq.startDate IS NOT NULL OR mq.expireDate IS NOT NULL OR mq.sendTime IS NOT NULL)"
				+ " AND mq.status = :status"
				+ " ORDER BY mq.lastUpdated DESC"
	),
	@NamedQuery(
		name = "MessageQueue.findAllJobs",
		query = "SELECT mq FROM MessageQueue mq"
				+ " WHERE mq.startDate <= :startDate"
				+ " AND mq.sendTime <= :sendTime"
				+ " AND (mq.ownerId IS NULL or mq.ownerId = '' )"
				+ " AND (mq.osType = 'android' OR mq.osType = 'ios')"
				+ " AND mq.status = 0"
				+ " ORDER BY mq.lastUpdated DESC"
	)
})
public class MessageQueue extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@Column(name = "message_id")
	private String messageId;

	@Mutable
	@Column(name = "content")
	private String content;

	@Mutable
	@Column(name = "start_date")
	private String startDate;

	@Mutable
	@Column(name = "expire_date")
	private String expireDate;

	@Mutable
	@Column(name = "send_time")
	private String sendTime;
	
	@Mutable
	@Column(name = "expire_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expireTime;

	@Mutable
	@Column(name = "target_id")
	private String targetId;

	@Mutable
	@Column(name = "file_uri")
	private String fileUri;

	@Mutable
	@Column(name = "status")
	private Integer status;

	@Mutable
	@Column(name = "owner_id")
	private String ownerId;

	@Mutable
	@Column(name = "msg_start_time")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date msgStartTime;

	@Mutable
	@Column(name = "msg_end_time")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date msgEndTime;

	@Mutable
	@Column(name = "os_type")
	private String osType;

	@Mutable
	@Column(name = "msg_message_id")
	private String msgMessageId;

	@Mutable
	@Column(name = "expand_status")
	private Integer expandStatus;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public MessageQueue() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getFileUri() {
		return fileUri;
	}

	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public Date getMsgStartTime() {
		return this.msgStartTime;
	}

	public void setMsgStartTime(Date msgStartDateTime) {
		this.msgStartTime = msgStartDateTime;
	}

	public void setMsgStartTime(String msgStartDateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(msgStartDateTime);
		setMsgStartTime(Date.from(zdt.toInstant()));
	}

	public Date getMsgEndTime() {
		return this.msgEndTime;
	}

	public void setMsgEndTime(Date msgEndDateTime) {
		this.msgEndTime = msgEndDateTime;
	}

	public void setMsgEndTime(String msgEndDateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(msgEndDateTime);
		setMsgEndTime(Date.from(zdt.toInstant()));
	}

	public String getOsType() {
		return this.osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public String getMsgMessageId() {
		return msgMessageId;
	}

	public void setMsgMessageId(String msgMessageId) {
		this.msgMessageId = msgMessageId;
	}

	public Integer getExpandStatus() {
		return expandStatus;
	}

	public void setExpandStatus(Integer expandStatus) {
		this.expandStatus = expandStatus;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

}