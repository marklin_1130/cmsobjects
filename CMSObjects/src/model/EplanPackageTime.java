package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "eplan_package_time")
@NamedQueries({
    @NamedQuery(
            name = "EplanPackageTime.findByEplanidAndItemId",
            query = "SELECT ept FROM EplanPackageTime ept"
            		+ " WHERE ept.eplanid = :eplanid"
            		+ " AND ept.itemId = :itemId"
        )
})
public class EplanPackageTime extends digipages.common.BasePojo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@Mutable
    @Column(name = "eplanid")
	private String eplanid = "";
	
	@Mutable
    @Column(name = "item_id")
	private String itemId;
	
	@Mutable
    @Column(name = "is_eplan")
	private String isEplan;
	
	@Mutable
    @Column(name = "is_always_eplan")
	private String isAlwaysEplan;
	
	@Mutable
    @Column(name = "eplan_start_date")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date eplanStartDate;
	
	@Mutable
    @Column(name = "eplan_expire_date")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date eplanExpireDate;
	
	@Mutable
    @Column(name = "udt")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEplanid() {
		return eplanid;
	}

	public void setEplanid(String eplanid) {
		this.eplanid = eplanid;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getIsEplan() {
		return isEplan;
	}

	public void setIsEplan(String isEplan) {
		this.isEplan = isEplan;
	}

	public String getIsAlwaysEplan() {
		return isAlwaysEplan;
	}

	public void setIsAlwaysEplan(String isAlwaysEplan) {
		this.isAlwaysEplan = isAlwaysEplan;
	}

	public Date getEplanStartDate() {
		return eplanStartDate;
	}

	public void setEplanStartDate(Date eplanStartDate) {
		this.eplanStartDate = eplanStartDate;
	}
	
	public void setEplanStartDate(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setEplanStartDate(Date.from(zdt.toInstant()));
	}

	public Date getEplanExpireDate() {
		return eplanExpireDate;
	}

	public void setEplanExpireDate(Date eplanExpireDate) {
		this.eplanExpireDate = eplanExpireDate;
	}
	
	public void setEplanExpireDate(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setEplanExpireDate(Date.from(zdt.toInstant()));
	}

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

}
	