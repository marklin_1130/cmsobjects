package model;

public class DRMInfo {
    
    String read_end_time = "";
    String drm_type= "";
    String book_uni_id= "";
    String drm_info= "";
    String is_buyout="";
    
    public String getBook_uni_id() {
		return book_uni_id;
	}
	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}
	public String getDrm_info() {
		return drm_info;
	}
	public void setDrm_info(String drm_info) {
		this.drm_info = drm_info;
	}
    public String getRead_end_time() {
        return read_end_time;
    }
    public void setRead_end_time(String read_expire_time) {
        this.read_end_time = read_expire_time;
    }
    public String getDrm_type() {
        return drm_type;
    }
    public void setDrm_type(String drm_type) {
        this.drm_type = drm_type;
    }
	public String getIs_buyout() {
		return is_buyout;
	}
	public void setIs_buyout(String is_buyout) {
		this.is_buyout = is_buyout;
	}
    
}