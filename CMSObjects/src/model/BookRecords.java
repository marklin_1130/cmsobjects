package model;

public class BookRecords {
	String category;
	String session_id;
	String ip;
	String book_uni_id;
	Long member_book_id;
	Integer item_type;
	String version;
	String current_page;
	String duration;
	String start_time;
	String end_time;
	String updated_time;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getBook_uni_id() {
		return book_uni_id;
	}

	public void setBook_uni_id(String book_uni_id) {
		this.book_uni_id = book_uni_id;
	}

	public Long getMember_book_id() {
		return member_book_id;
	}

	public void setMember_book_id(Long member_book_id) {
		this.member_book_id = member_book_id;
	}

	public Integer getItem_type() {
		return item_type;
	}

	public void setItem_type(Integer item_type) {
		this.item_type = item_type;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCurrent_page() {
		return current_page;
	}

	public void setCurrent_page(String current_page) {
		this.current_page = current_page;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}
}