package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

/**
 * The persistent class for the faq database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "faq")
@NamedQueries({
	@NamedQuery(name = "Faq.findAll", query = "SELECT f FROM Faq f"),
	@NamedQuery(name = "Faq.findByFaqId", query = "SELECT f FROM Faq f WHERE f.faqId = :faqId"),
	@NamedQuery(name = "Faq.findAllBySequenceASC", query = "SELECT f FROM Faq f ORDER BY f.sequence,f.lastUpdated ASC"),
	@NamedQuery(name = "Faq.findBySequenceASC", query = "SELECT f FROM Faq f WHERE f.lastUpdated > :lastUpdated ORDER BY f.sequence,f.lastUpdated ASC")
})
public class Faq extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@Column(name = "faq_id")
	private Integer faqId;

	@Mutable
	@Column(name = "title")
	private String title;

	@Mutable
	@Column(name = "content")
	private String content;

	@Mutable
	@Column(name = "sequence")
	private Integer sequence;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Faq() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getFaqId() {
		return faqId;
	}

	public void setFaqId(Integer faqId) {
		this.faqId = faqId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
}