package model;

import com.google.gson.annotations.Expose;

import digipages.common.DPLogger;

public class BaseBookInfo extends ItemInfo {
	
	private static DPLogger logger =DPLogger.getLogger(BaseBookInfo.class.getName());
	
	public static final String BookFormatPDF="pdf";
	public static final String BookFormatFixedLayout="fixedlayout";
	public static final String BookFormatReflowable="reflowable";
	public static final String BookFormatMedia="media";
	public static final String BookFormatAudio="audio";
	
	public String getSrc_cover_url() {
		return src_cover_url;
	}

	public void setSrc_cover_url(String src_cover_url) {
		this.src_cover_url = src_cover_url;
	}

	// Books definition
	@Expose
	private String item; // item = product id from books
	@Expose
	private String c_title; // 商品名稱
	@Expose
	private String o_title; // 次要品名
	@Expose
	private String publisher_id;
	@Expose
	private String publisher_name; // 出版社名稱, 備註: 出版社改名時須全面改名
	@Expose
	private String category; // 分類代碼
	@Expose
	private String rank; // 分級
	@Expose
	private String publish_date; // 出版日期
	@Expose
	private String toc;
	@Expose
	private String efile_nofixed_name;
	@Expose
	private String efile_fixed_pad_name;
	@Expose
	private String efile_fixed_phone_name;
	@Expose
	private String efile_url;
	@Expose
	private String efile_cover_url;
	@Expose
	private String src_cover_url;
	@Expose
	private String status;
	@Expose
	private int return_file_num;
	@Expose
	private Long bookGroup;
	@Expose
	private String book_format;
	@Expose
	private Integer page_direction;
	@Expose
	private String language;
	@Expose
	private String version_type;
	@Expose
	private String buffet_flag = "N";
	@Expose
	private String buffet_type;
	@Expose
	private String like_flag = "Y";
	@Expose
	private String annotation_flag = "Y";
	@Expose
	private String bookmark_flag = "Y";
	@Expose
	private String note_flag = "Y";
	@Expose
	private String public_flag = "Y";
	@Expose
	private String share_flag = "Y";
	@Expose
	private String share_link; // not used.
	@Expose
	private String tts_flag ="N";

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getC_title() {
		return c_title;
	}

	public void setC_title(String c_title) {
		this.c_title = c_title;
	}

	public String getO_title() {
		return o_title;
	}

	public void setO_title(String o_title) {
		this.o_title = o_title;
	}

	public String getPublisher_id() {
		return publisher_id;
	}

	public void setPublisher_id(String publisher_id) {
		this.publisher_id = publisher_id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getPublish_date() {
		return publish_date;
	}

	public void setPublish_date(String publish_date) {
		this.publish_date = publish_date;
	}

	public String getToc() {
		return toc;
	}

	public void setToc(String toc) {
		this.toc = toc;
	}

	public String getEfile_nofixed_name() {
		return efile_nofixed_name;
	}

	public void setEfile_nofixed_name(String efile_nofixed_name) {
		this.efile_nofixed_name = efile_nofixed_name;
	}

	public String getEfile_fixed_pad_name() {
		return efile_fixed_pad_name;
	}

	public void setEfile_fixed_pad_name(String efile_fixed_pad_name) {
		this.efile_fixed_pad_name = efile_fixed_pad_name;
	}

	public String getEfile_fixed_phone_name() {
		return efile_fixed_phone_name;
	}

	public void setEfile_fixed_phone_name(String efile_fixed_phone_name) {
		this.efile_fixed_phone_name = efile_fixed_phone_name;
	}

	public String getEfile_url() {
		return efile_url;
	}

	public void setEfile_url(String efile_url) {
		this.efile_url = efile_url;
	}

	public String getEfile_cover_url() {
		return efile_cover_url;
	}

	public void setEfile_cover_url(String efile_cover_url) {
		if (null==efile_cover_url || efile_cover_url.length()<1)
		{
			RuntimeException ex = new RuntimeException("Debug: efile_cover_url is set to empty !!");
			logger.error(ex);;
		}
		this.efile_cover_url = efile_cover_url;
	}

	public String getShare_flag() {
		return share_flag;
	}

	public void setShare_flag(String share_flag) {
		this.share_flag = share_flag;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getReturn_file_num() {
		return return_file_num;
	}

	public void setReturn_file_num(int return_file_num) {
		this.return_file_num = return_file_num;
	}

	public Long getBookGroup() {
		return bookGroup;
	}
	
   public void setBookGroup(int bookGroup) {
        this.bookGroup = Long.valueOf( bookGroup);
    }

	public void setBookGroup(Long bookGroup) {
		this.bookGroup = bookGroup;
	}

	public String getBook_format() {
		return book_format;
	}

	public void setBook_format(String book_format) {
		this.book_format = book_format;
	}

	public Integer getPage_direction() {
		return page_direction;
	}

	public void setPage_direction(Integer page_direction) {
		this.page_direction = page_direction;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getVersion_type() {
		return version_type;
	}

	public void setVersion_type(String version_type) {
		this.version_type = version_type;
	}

	public String getBuffet_flag() {
		return buffet_flag;
	}

	public void setBuffet_flag(String buffet_flag) {
		this.buffet_flag = buffet_flag;
	}

	public String getBuffet_type() {
		return buffet_type;
	}

	public void setBuffet_type(String buffet_type) {
		this.buffet_type = buffet_type;
	}

	public String getPublisher_name() {
		return publisher_name;
	}

	public void setPublisher_name(String publisher_name) {
		this.publisher_name = publisher_name;
	}

	public String getLike_flag() {
		return like_flag;
	}

	public void setLike_flag(String like_flag) {
		this.like_flag = like_flag;
	}

	public String getAnnotation_flag() {
		return annotation_flag;
	}

	public void setAnnotation_flag(String annotation_flag) {
		this.annotation_flag = annotation_flag;
	}

	public String getBookmark_flag() {
		return bookmark_flag;
	}

	public void setBookmark_flag(String bookmark_flag) {
		this.bookmark_flag = bookmark_flag;
	}

	public String getNote_flag() {
		return note_flag;
	}

	public void setNote_flag(String note_flag) {
		this.note_flag = note_flag;
	}

	public String getPublic_flag() {
		return public_flag;
	}

	public void setPublic_flag(String public_flag) {
		this.public_flag = public_flag;
	}

	public String getShare_link() {
		return share_link;
	}

	public void setShare_link(String share_link) {
		this.share_link = share_link;
	}

	public String getTts_flag() {
		return tts_flag;
	}

	public void setTts_flag(String tts_flag) {
		this.tts_flag = tts_flag;
	}
	
	
}