package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import digipages.exceptions.ServerException;

/**
 * The persistent class for the vendor database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 3000, // 3 sec
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "vendor")
@NamedQueries({
	@NamedQuery(name = "Vendor.findAll", query = "SELECT v FROM Vendor v"),
	@NamedQuery(name = "Vendor.findById", query = "SELECT v FROM Vendor v WHERE v.id=:id"),
	@NamedQuery(name = "Vendor.findBybId", query = "SELECT v FROM Vendor v WHERE v.bId = :bId")
})
public class Vendor extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;
	private static List<Vendor> vendor = null;
	private static java.util.Random rand = new java.util.Random();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer id;

	@Mutable
	@Column(name = "b_id")
	private String bId;

	@Mutable
	@Column(name = "name")
	private String name;

	@Mutable
	@JsonIgnore
	// bi-directional many-to-one association to Item
	@OneToMany(mappedBy = "vendor", cascade = CascadeType.ALL)
	private List<Item> items;

	@Mutable
	@JsonIgnore
	// bi-directional many-to-one association to Member
	@OneToMany(mappedBy = "vendor", cascade = CascadeType.ALL)
	private List<Member> members;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Vendor() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBId() {
		return this.bId;
	}

	public void setBId(String bId) {
		this.bId = bId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Item addItem(Item item) {
		getItems().add(item);
		item.setVendor(this);
		return item;
	}

	public Item removeItem(Item item) {
		getItems().remove(item);
		item.setVendor(null);
		return item;
	}

	public List<Member> getMembers() {
		return this.members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public Member addMember(Member member) {
		getMembers().add(member);
		member.setVendor(this);
		return member;
	}

	public Member removeMember(Member member) {
		getMembers().remove(member);
		member.setVendor(null);
		return member;
	}

	/**
	 * vendor is a slow query, use local cache instead.
	 * 
	 * @param em
	 * @return
	 */
	public static Vendor getRandomVendor(EntityManager em) {
		if (null == vendor) {
			vendor = em.createNamedQuery("Vendor.findAll", Vendor.class).getResultList();
		}

		return vendor.get(rand.nextInt(vendor.size()));
	}

	/**
	 * slower version when in writing transaction.
	 * 
	 * @param em
	 * @param b_id
	 * @return
	 */
	public static Vendor findVendorBybid(EntityManager em, String b_id) throws ServerException {
		try {
			return em.createNamedQuery("Vendor.findBybId", Vendor.class).setParameter("bId", b_id).getSingleResult();
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 
	 * @param postgres
	 * @param partyId
	 * @return
	 * @throws ServerException
	 */
	public static Vendor getVendorBybid(EntityManager postgres, String partyId) throws ServerException {
		Vendor p;

		p = findVendorBybid(postgres, partyId);

		if (p == null) {
			throw new ServerException("id_err_212", "Vendor not exists.");
		}

		return p;
	}
}