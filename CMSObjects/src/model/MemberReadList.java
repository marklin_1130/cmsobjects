package model;

import java.io.Serializable;

import javax.persistence.*;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.Date;
import java.sql.Array;


/**
 * The persistent class for the member_read_list database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
)
@Table(name="member_read_list")
@NamedQueries({
	@NamedQuery(name="MemberReadList.findAll", query="SELECT m FROM MemberReadList m"),
	@NamedQuery(name="MemberReadList.findAllCustom", query="SELECT m FROM MemberReadList m WHERE  m.idname like 'custom%' "),
	}
)
@NamedNativeQueries ({
	@NamedNativeQuery(
			name="MemberReadList.deleteAllbyCustomTypeId",
			query="UPDATE member_read_list "
					+ "SET idname=replace(idname, 'custom', 'del'), udt=now() "
					+ "WHERE member_id=? AND idname =?"),
	@NamedNativeQuery(
            name="MemberReadList.findAllCustomAndMember",
            query="SELECT * from member_read_list "
                    + "WHERE member_id=? AND idname like 'custom%'",
                    resultClass = MemberReadList.class),
	}
)

public class MemberReadList extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;
	@Mutable
	@Column(name="idname")
	private String idname;
	@Mutable
	@Column(name="password")
	private String password;
	@Mutable
	@Column(name="readonly")
	private Boolean readonly;
	@Mutable
	@Column(name="title")
	private String title;
	//bi-directional many-to-one association to Member
	@Mutable
	@JoinColumn(name="member_id")
	@ManyToOne
	@JsonIgnore
	private Member member;
	
	@Mutable
	@Column(name = "operator")
	private String operator ;
	
	@Mutable
	@Column(name = "create_operator")
	private String createOperator ;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;
	
	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime=new Date();

	@Mutable
	@Column(name = "seq")
	private Integer seq ;
	
	@PrePersist
	@PreUpdate
	public void onPrePersist() {

		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}
	
	
	public MemberReadList() {
	}
	
	public MemberReadList(String idname,String title, Member m) {
		setMember(m);
		setIdname(idname);
		setTitle(title);
		setReadonly(true);

	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getIdname() {
		return this.idname;
	}

	public void setIdname(String idname) {
		this.idname = idname;
	}


	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Boolean getReadonly() {
		return this.readonly;
	}

	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}


	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
		touchUpdateTime();
	}
	
	public String getCreateOperator() {
		return createOperator;
	}

	public void setCreateOperator(String createOperator) {
		this.createOperator = createOperator;
	}
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public void touchUpdateTime() {
		setLastUpdated(new Date());
		
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}