package model;

import java.util.ArrayList;
import java.util.List;

public class CFILocation implements Comparable<CFILocation> {

	private String cfiStr;
	public CFILocation(String cfiStr) {
		this.cfiStr=cfiStr;
	}
	
	/**
	 * if src is bigger than target 
	 * @param srcCfi
	 * @param targetCfi
	 * @return
	 */
	public static int isBigger(String srcCfi, String targetCfi)
	{

		if (null==srcCfi || "epubcfi()".equals(srcCfi) || "".equals(srcCfi) 
				|| "undefined".equals(targetCfi) )
			return -1;
		if (null==targetCfi || "epubcfi()".equals(targetCfi) || "".equals(srcCfi) 
				|| "undefined".equals(targetCfi) )
			return 1;
		
		if( ("epubcfi()".equals(srcCfi) && "epubcfi()".equals(targetCfi)) 
				|| ( null==srcCfi && null==targetCfi) ){
			return 0;
		}

		CFILocation srcLoc = new CFILocation(srcCfi);
		CFILocation tgtLoc = new CFILocation(targetCfi);

		return srcLoc.compareTo(tgtLoc);
	}
	
	@Override
	public int compareTo(CFILocation that) {
//		Boolean MaxReadLoc = false;
		int status = 0 ;
		int MaxReadLoc = 0;
		List<Integer> cfiPosition = new ArrayList<>();
		List<Integer> maxReadLocPosition = new ArrayList<>();

		String[] tmpCFI = this.cfiStr.split("!");
		String[] tmpmaxReadLoc = that.cfiStr.split("!");

		// 比較是否是同一個檔案
		if (tmpCFI.length > 0 && tmpmaxReadLoc.length > 0) {
			if (!tmpCFI[0].equals(tmpmaxReadLoc[0])) {
				cfiPosition = findNodesPostion(tmpCFI[0]);
				maxReadLocPosition = findNodesPostion(tmpmaxReadLoc[0]);
				MaxReadLoc = compareNodePosition(cfiPosition, maxReadLocPosition);
				if (-1==MaxReadLoc) {
					return 1;
				}else if( 1==MaxReadLoc ){
					return -1;
				}
			}
		}
		// 若是同一個檔案
		if (tmpCFI.length > 1 && tmpmaxReadLoc.length > 1) {
			cfiPosition = findNodesPostion(tmpCFI[1].split(",")[0]);
			maxReadLocPosition = findNodesPostion(tmpmaxReadLoc[1].split(",")[0]);
			MaxReadLoc = compareNodePosition(cfiPosition, maxReadLocPosition);
			if ( -1==MaxReadLoc ) {
				return 1;
			}else if( 1==MaxReadLoc ){
				return -1;
			}
		}
		return status ;
	}
	
	private static List<Integer> findNodesPostion(String step) {

		List<Integer> nodePosition = new ArrayList<>();
		String[] items = step.split("/");
		if (items.length > 1) {
			String node = "";

			for (String item : items) {
				if (item.contains("[")) {
					node = item.split("\\[")[0];
				} else if (item.contains(":")) {
					node = item.split(":")[0];
				} else {
					node = item;
				}

				try {
					if( !"epubcfi(".equals(node)){
						nodePosition.add(Integer.valueOf(node));
					}
				} catch (NumberFormatException ex) {
//					logger.info("BookReadProgress : " + step);
//					logger.info("BookReadProgress @ NumberFormatException/ " + node);
				}
			}
		}
		return nodePosition;
	}
	
	
	private static int compareNodePosition(List<Integer> cfiNodesPosition, List<Integer> maxReadLocNodesPosition) {
		int status = 0;

		Integer compareLength = cfiNodesPosition.size();
		if (cfiNodesPosition.size() > maxReadLocNodesPosition.size()) {
			compareLength = maxReadLocNodesPosition.size();
		}

		for (Integer compareIndx = 0; compareIndx < compareLength; compareIndx++) {

			Integer cfiNodePosition = cfiNodesPosition.get(compareIndx);
			Integer maxReadLocNodePosition = maxReadLocNodesPosition.get(compareIndx);
			if (cfiNodePosition > maxReadLocNodePosition) {				
				return -1;
			}else if( cfiNodePosition < maxReadLocNodePosition ){
				return 1;
			}
		}
		return status;
	}

}
