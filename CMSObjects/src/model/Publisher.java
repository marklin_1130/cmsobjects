package model;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;
import org.eclipse.persistence.config.QueryHints;

import digipages.exceptions.ServerException;

/**
 * The persistent class for the publisher database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 3000, // 3 sec
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "publisher")
@NamedQueries({
	@NamedQuery(name = "Publisher.findAll", query = "SELECT p FROM Publisher p"),
	@NamedQuery(name = "Publisher.findById", query = "SELECT p FROM Publisher p WHERE p.id=:id"),
	@NamedQuery(
		name = "Publisher.findBybId",
		query = "SELECT p FROM Publisher p WHERE p.bId = :bId",
		hints = {
			@QueryHint(name = QueryHints.QUERY_RESULTS_CACHE, value = "TRUE"),
			@QueryHint(name = QueryHints.QUERY_RESULTS_CACHE_SIZE, value = "500")
		}
	)
})
public class Publisher extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Map<String, Publisher> myMap = null;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer id;

	@Mutable
	@Column(name = "b_id")
	private String bId;

	@Mutable
	@Column(name = "name")
	private String name;

	@Mutable
	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Item> items;

	@Mutable
	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Member> members;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Publisher() {

	}

	// @Id
	// @GeneratedValue(strategy=GenerationType.IDENTITY)
	// @Column(unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	// @Column(name="b_id")
	public String getBId() {
		return this.bId;
	}

	public void setBId(String bId) {
		this.bId = bId;
	}

	// @Column(name="name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// bi-directional many-to-one association to Item
	// @OneToMany(mappedBy="publisher")
	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Item addItem(Item item) {
		getItems().add(item);
		item.setPublisher(this);
		return item;
	}

	public Item removeItem(Item item) {
		getItems().remove(item);
		item.setPublisher(null);
		return item;
	}

	// bi-directional many-to-one association to Member
	// @OneToMany(mappedBy="publisher")
	public List<Member> getMembers() {
		return this.members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public Member addMember(Member member) {
		getMembers().add(member);
		member.setPublisher(this);
		return member;
	}

	public Member removeMember(Member member) {
		getMembers().remove(member);
		member.setPublisher(null);
		return member;
	}

	/**
	 * Found that create item is extreme slow, due to find the publisher. make it local static info (since size is small)
	 * 
	 * @param em
	 * @param b_id
	 * @return
	 */
	public static Publisher findPublisherBybidMapped(EntityManager em, String b_id) {
		if (null == myMap) {
			myMap = new HashMap<>();

			Query q = em.createNamedQuery("Publisher.findAll", Publisher.class);
			@SuppressWarnings("unchecked")
			List<Publisher> publishers = (List<Publisher>) q.getResultList();

			for (Publisher p : publishers) {
				myMap.put(p.getBId(), p);
			}
		}

		return myMap.get(b_id);
	}

	/**
	 * slower version when in writing transaction.
	 * 
	 * @param em
	 * @param b_id
	 * @return
	 */
	public static Publisher findPublisherBybid(EntityManager em, String b_id) throws ServerException {
		try {
			return em.createNamedQuery("Publisher.findBybId", Publisher.class).setParameter("bId", b_id).getSingleResult();
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 
	 * @param postgres
	 * @param partyId
	 * @return
	 * @throws ServerException
	 */
	public static Publisher getPublisherBybid(EntityManager postgres, String partyId) throws ServerException {
		Publisher p;

		p = findPublisherBybid(postgres, partyId);

		if (p == null) {
			throw new ServerException("id_err_211", "Publisher not exists.");
		}

		return p;
	}

	/**
	 * just test sample
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	public static Publisher fromResultSet(ResultSet rs) throws SQLException {
		Publisher ret = new Publisher();
		ret.setBId(rs.getString("b_id"));
		ret.setId(rs.getInt("id"));
		// not used
		// ret.setItems(items);
		// ret.setMembers(members);
		ret.setLastUpdated(rs.getDate("udt"));
		ret.setName(rs.getString("name"));
		return ret;
	}
}