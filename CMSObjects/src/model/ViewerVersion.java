package model;

import java.io.Serializable;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import java.sql.Timestamp;


/**
 * The persistent class for the viewer_version database table.
 * 
 */
@Entity
@Cache(
		  type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
		  size=4000,  // Use 4,000 as the initial cache size.
		  expiry=300000,  // 5min 
		  coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
		)
@Table(name="viewer_version")
@NamedQueries({
	@NamedQuery(name="ViewerVersion.findAll", query="SELECT v FROM ViewerVersion v"),
	@NamedQuery(name="ViewerVersion.findAllOrderByDate", query="SELECT v FROM ViewerVersion v ORDER BY v.udt DESC")
})

public class ViewerVersion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;
	
	@Mutable
	@Column(name="os_type")
	private String osType;

	@Mutable
	@Column(name="os_version")
	private String osVersion;

	private Timestamp udt;

	@Mutable
	@Column(name="v_version")
	private String vVersion;

	public ViewerVersion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOsType() {
		return this.osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public String getOsVersion() {
		return this.osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public Timestamp getUdt() {
		return this.udt;
	}

	public void setUdt(Timestamp udt) {
		this.udt = udt;
	}

	public String getVVersion() {
		return this.vVersion;
	}

	public void setVVersion(String vVersion) {
		this.vVersion = vVersion;
	}

}