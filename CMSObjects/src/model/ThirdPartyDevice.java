package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

/**
 * The persistent class for the message database table.
 *
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
)
@Table(name = "third_party_device")
@NamedQueries({
	@NamedQuery(
		name = "ThirdPartyDevice.findAll",
		query = "SELECT tpd FROM ThirdPartyDevice tpd"
	),
	@NamedQuery(
		name = "ThirdPartyDevice.findAllByMemberIdAndDeviceId",
		query = "SELECT tpd FROM ThirdPartyDevice tpd"
				+ " WHERE tpd.memberId = :memberId"
				+ " AND tpd.deviceId = :deviceId"
	),
	@NamedQuery(
			name = "ThirdPartyDevice.findAllByDeviceId",
			query = "SELECT tpd FROM ThirdPartyDevice tpd"
					+ " WHERE tpd.deviceId = :deviceId"
		),
	@NamedQuery(
		name = "ThirdPartyDevice.findAllByMemberId",
		query = "SELECT tpd FROM ThirdPartyDevice tpd"
				+ " WHERE tpd.memberId = :memberId"
	)
})
public class ThirdPartyDevice extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer id;

	@Mutable
	@Column(name = "member_id")
	private Integer memberId;

	@Mutable
	@Column(name = "device_id")
	private String deviceId;
	
	@Mutable
	@Column(name = "party_type")
	private String partyType;
	
	@Mutable
	@Column(name = "bparty_id")
	private String bpartyId;
	
	@Mutable
	@Column(name = "operator")
	private String operator;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public String getPartyType() {
		return partyType;
	}

	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	public String getBpartyId() {
		return bpartyId;
	}

	public void setBpartyId(String bpartyId) {
		this.bpartyId = bpartyId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public static boolean containsDeviceId(EntityManager em,String deviceId) {
		List <ThirdPartyDevice> partyDevices=
				em.createNamedQuery("ThirdPartyDevice.findAllByDeviceId",
						ThirdPartyDevice.class)
				.setParameter("deviceId",deviceId).getResultList();
		if (partyDevices.size()>0)
			return true;
		return false;
	}

	public static String getTypeByDeviceId(EntityManager em, String deviceId) {
		List <ThirdPartyDevice> partyDevices=
				em.createNamedQuery("ThirdPartyDevice.findAllByDeviceId",
						ThirdPartyDevice.class)
				.setParameter("deviceId",deviceId).getResultList();
		
		if (partyDevices.size()>0){
			return String.valueOf(partyDevices.get(0).getPartyType());
		}
		
		return "NormalReader";
	}
}