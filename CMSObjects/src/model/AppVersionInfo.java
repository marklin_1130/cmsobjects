package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

/**
 * The persistent class for the faq database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "app_version_info")
@NamedQueries({
	@NamedQuery(name = "AppVersionInfo.findAll", query = "SELECT a FROM AppVersionInfo a"),
	@NamedQuery(name = "AppVersionInfo.findByVersionId", query = "SELECT a FROM AppVersionInfo a WHERE a.versionId = :versionId and a.deleted = false"),
	@NamedQuery(name = "AppVersionInfo.findByVersionDESC", query = "SELECT a FROM AppVersionInfo a WHERE a.os = :os and a.version > :version and a.deleted = false and a.release = 'Y' ORDER BY a.version DESC"),
	@NamedQuery(name = "AppVersionInfo.findAllByVersionDESC", query = "SELECT a FROM AppVersionInfo a ORDER BY a.versionId DESC"),
	@NamedQuery(name = "AppVersionInfo.findByUdtDESC", query = "SELECT a FROM AppVersionInfo a WHERE a.lastUpdated > :lastUpdated ORDER BY a.version,a.lastUpdated DESC")
})
public class AppVersionInfo extends digipages.common.BasePojo implements Serializable {

    private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@Column(name="version_id")
	private Integer versionId;
	
	@Mutable
	@Column(name = "version")
	private String version;
	
	@Mutable
    @Column(name = "os")
    private String os;

	@Mutable
	@Column(name = "title")
	private String title;

	@Mutable
	@Column(name = "content")
	private String content;

	@Mutable
	@Column(name = "update_type")
	private String updateType;
	
	@Mutable
    @Column(name = "release")
    private String release;
	
	@Mutable
    @Column(name = "deleted")
    private boolean deleted;
	
	@Mutable
    @Column(name = "create_user")
    private String createUser;
	
	@Mutable
    @Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
    private Date createTime;
	
	@Mutable
    @Column(name = "modify_user")
    private String modifyUser;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setCreateTime(String createTime) {
        ZonedDateTime zdt = ZonedDateTime.parse(createTime);
        setLastUpdated(Date.from(zdt.toInstant()));
    }
    
    public String getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }
}