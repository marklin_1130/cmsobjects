package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Mutable;

@Entity
@Table(name = "classification")
public class Classification {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id",unique = true, nullable = false)
	private Integer id;
	
	@Mutable
    @Column(name = "cat")
	private String cat;
	
	@Mutable
    @Column(name = "sub")
	private String sub;
	
	@Mutable
    @Column(name = "sub_name")
	private String subName;
	
	@Mutable
    @Column(name = "sequence")
	private Integer sequence;
	
	@Mutable
    @Column(name = "delmark")
	private String delmark;
	
	@Mutable
    @Column(name = "cl_level")
	private Integer clLevel;
	
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getDelmark() {
		return delmark;
	}

	public void setDelmark(String delmark) {
		this.delmark = delmark;
	}

	public Integer getClLevel() {
		return clLevel;
	}

	public void setClLevel(Integer clLevel) {
		this.clLevel = clLevel;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	

}
