package model;

import java.io.Serializable;
import java.util.List;

public class NoteContent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2829504502580954608L;

	String cfi;
	String cfieven;
	String chapter; // 章節目錄
	String page; // 頁數
	String highlight_text; // 劃線筆記
	String text; // 劃記文字
	String color; // 顏色
	String note; // 閱讀心得
	String bookformat; // 檔案格式 EPUB Reflow or fixed or PDF
	String author_nick; // 暱稱
	List<Grafinfo> graffiti; // PDF的劃線(塗鴉)
	String origAction;

	public String getCfi() {
		return cfi;
	}

	public void setCfi(String cfi) {
		this.cfi = cfi;
	}

	public String getCfieven() {
		return cfieven;
	}

	public void setCfieven(String cfieven) {
		this.cfieven = cfieven;
	}

	public String getChapter() {
		return chapter;
	}

	public void setChapter(String chapter) {
		this.chapter = chapter;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getHighlight_text() {
		return highlight_text;
	}

	public void setHighlight_text(String highlight_text) {
		this.highlight_text = highlight_text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

//	public Double getAlpha() {
//		return alpha;
//	}
//
//	public void setAlpha(Double alpha) {
//		this.alpha = alpha;
//	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getBookformat() {
		return bookformat;
	}

	public void setBookformat(String bookformat) {
		this.bookformat = bookformat;
	}

	public String getAuthor_nick() {
		return author_nick;
	}

	public void setAuthor_nick(String author_nick) {
		this.author_nick = author_nick;
	}

	public List<Grafinfo> getGraffiti() {
		return graffiti;
	}

	public void addGraffiti(Grafinfo grafinfo) {
		graffiti.add(grafinfo);
	}

	public void setGraffiti(List<Grafinfo> graffiti) {
		this.graffiti = graffiti;
	}

	public String getOrigAction() {
		return origAction;
	}

	public void setOrigAction(String origAction) {
		this.origAction = origAction;
	}
}