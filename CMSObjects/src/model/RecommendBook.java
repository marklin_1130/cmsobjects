package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.*;

import org.boon.json.annotations.JsonIgnore;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

/**
 * The persistent class for the recommand_books database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "recommend_books")
@NamedQueries({
	@NamedQuery(
		name = "RecommendBook.findAll",
		query = "SELECT rb FROM RecommendBook rb"
				+ " WHERE rb.startTime <= :nowTime"
				+ " AND rb.endTime > :nowTime"
				+ " AND rb.bookFile in (select bf from BookFile bf where bf.status=9)"
				+ " ORDER BY rb.startTime,rb.lastUpdated DESC"
	),
	@NamedQuery(
		name = "RecommendBook.findByItemId",
		query = "SELECT rb FROM RecommendBook rb"
				+ " WHERE rb.itemId = :itemId"
	),
	@NamedQuery(
		name = "RecommendBook.findByItemIdAndSequence",
		query = "SELECT rb FROM RecommendBook rb"
				+ " WHERE rb.itemId = :itemId"
				+ " AND rb.sequence = :sequence"
	),
	@NamedQuery(
		name = "RecommendBook.findAllByItemIds",
		query = "SELECT rb FROM RecommendBook rb"
				+ " WHERE rb.itemId IN :itemIds"
				+ " AND rb.startTime <= :nowTime"
				+ " AND rb.endTime > :nowTime"
				+ " ORDER BY rb.startTime,rb.lastUpdated DESC"
	),
	@NamedQuery(
		name = "RecommendBook.findAllByTime",
		query = "SELECT rb FROM RecommendBook rb"
				+ " WHERE rb.startTime <= :nowTime"
				+ " AND rb.endTime > :nowTime"
				+ " ORDER BY rb.startTime,rb.lastUpdated DESC"
	)
})
public class RecommendBook extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Mutable
	@Column(name = "end_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endTime;

	@Mutable
	@Column(name = "item_id")
	private String itemId;

	@Mutable
	@Column(name = "start_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;

	@Mutable
	@Column(name = "sequence")
	private Integer sequence;

	// bi-directional many-to-one association to BookFile
	@Mutable
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "book_uni_id")
	private BookFile bookFile;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public RecommendBook() {

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getItemId() {
		return this.itemId;
	}

	public void setItemId(String item) {
		this.itemId = item;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public BookFile getBookFile() {
		return this.bookFile;
	}

	public void setBookFile(BookFile bookFile) {
		this.bookFile = bookFile;
	}
}