package model;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;
import org.eclipse.persistence.annotations.Struct;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;
import digipages.dev.utility.Enc01;
import digipages.exceptions.ServerException;

/**
 * The persistent class for the member_book database table.
 *
 */
@Entity
@Cache(
    type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
    size = 4000, // Use 4,000 as the initial cache size.
    expiry = 300000, // 5min
    coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Struct(name = "device_seq")
@Table(name = "member_book")

@NamedNativeQueries({
    @NamedNativeQuery(
            name = "MemberBook.removeFromReadList",
            query = "update member_book "
                    + " set readlist_idnames = array_remove(readlist_idnames, ?) , udt=now()"
                    + " where ? = any(readlist_idnames) "
                    + " and member_id= ?"
    )
})

@NamedQueries({
    @NamedQuery(
        name = "MemberBook.findAll",
        query = "SELECT mb FROM MemberBook mb"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByBookFileIds",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.bookFileId IN :bookFileIds"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findByMemberBookUniId",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.bookUniId = :bookUniId"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByItemId",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.item = :item"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByItemAndVersionLocked",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.item = :item"
                + " AND mb.versionLocked = :versionLocked"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByBookFileIdAndVersionLocked",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.bookUniId = :bookUniId"
                + " AND mb.versionLocked = :versionLocked"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByItemAndBookFileIdAndVersionLocked",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.item = :item"
                + " AND mb.bookFileId = :bookFileId"
                + " AND mb.versionLocked = :versionLocked"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByMemberIdAndItemAndVersionLocked",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.item = :item"
                + " AND mb.versionLocked = :versionLocked"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findByMemberIdAndBookFileId",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.bookFileId = :bookFileId"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByMemberIdAndItemIds",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.item IN :itemIds"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findNormalByMemberIdAndItemId",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.itemId = :itemId"
                + " AND NOT mb.encryptType = '" + MemberBook.EncryptTypeNone + "'"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
            name = "MemberBook.findAllTrialByMemberIdAndItemId",
            query = "SELECT mb FROM MemberBook mb"
                    + " WHERE mb.memberId = :memberId"
                    + " AND mb.itemId = :itemId"
                    + " AND (mb.encryptType = '" + MemberBook.EncryptTypeNone + "' "
                    + " OR  mb.encryptType is null )"
                    + " ORDER BY mb.lastUpdated DESC"
        ),
    
    @NamedQuery(
        name = "MemberBook.findAllByMemberIdAndItemIdsAndUDT",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.item IN :items"
                + " AND mb.lastUpdated > :lastUpdated"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByMemberId",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.findAllByMemberIdAndItem",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.item = :item"
    ),
    @NamedQuery(
        name = "MemberBook.findAllUnreadBookByBooksMemberId",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.StartReadTime IS NULL"
                + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "MemberBook.countAllUnreadBookByBooksMemberId",
        query = "SELECT COUNT(mb) FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.StartReadTime IS NULL"
                + " AND (mb.deleted=false OR mb.deleted is null)"
                + " AND mb.action<>'del'"
                + " AND mb.action<>'hide'"
    ),
    @NamedQuery(
        name = "MemberBook.findAllMemberBookByMemberidAndUdt",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND NOT mb.encryptType = '" + MemberBook.EncryptTypeNone + "'"
                + " AND mb.lastUpdated > :updateTime"
    ),
    @NamedQuery(
            name = "MemberBook.findAllByMemberIdAndUDT",
            query = "SELECT mb FROM MemberBook mb"
                    + " WHERE mb.memberId = :memberId"
                    + " AND mb.lastUpdated > :lastUpdated"
                    + " ORDER BY mb.lastUpdated DESC"
    ),
    @NamedQuery(
            name = "MemberBook.countByfindAllByMemberIdAndUDT",
            query = "SELECT COUNT(mb) FROM MemberBook mb"
                    + " WHERE mb.memberId = :memberId"
                    + " AND mb.lastUpdated > :lastUpdated"
    ),
    @NamedQuery(
        name = "MemberBook.findByMemberIdAndItemIdAndIsTrial",
        query = "SELECT mb FROM MemberBook mb"
                + " WHERE mb.memberId = :memberId"
                + " AND mb.itemId =:itemId"
                + " AND mb.isTrial =:isTrial"
    )
})
public class MemberBook extends digipages.common.BasePojo implements Serializable {
    
    @Override
    public String toString() {
        return "MemberBook [id=" + id + ", archived=" + archived + ", bookType=" + bookType + ", passworded=" + passworded + ", encryptType=" + encryptType + ", encryptKey="
                + Arrays.toString(encryptKey) + ", maxReadLoc=" + maxReadLoc + ", LastLoc=" + LastLoc + ", lockStatus=" + lockStatus + ", operator=" + operator + ", memberId=" + memberId
                + ", bookFileId=" + bookFileId + ", readlistIdnames=" + readlistIdnames + ", deviceSeq=" + deviceSeq + ", bookFile=" + bookFile + ", item=" + item + ", itemId=" + itemId + ", member="
                + member + ", memberBookNotes=" + memberBookNotes + ", percentage=" + percentage + ", StartReadTime=" + StartReadTime + ", LastReadTime=" + LastReadTime + ", lastUpdated="
                + lastUpdated + ", archivedDate=" + archivedDate + ", downloadTime=" + downloadTime + ", createTime=" + createTime + ", askUpdateVersion=" + askUpdateVersion + ", bgUpdateVersion="
                + bgUpdateVersion + ", versionLocked=" + versionLocked + ", bookHighlightStatus=" + bookHighlightStatus + ", bookBookmarkStatus=" + bookBookmarkStatus + ", newerVersion="
                + newerVersion + ", curVersion=" + curVersion + ", upgradeTime=" + upgradeTime + ", action=" + action + ", isHidden=" + isHidden + ", isBook=" + isBook + ", isMagazine=" + isMagazine
                + ", isTrial=" + isTrial + ", deleted=" + deleted + ", bookUniId=" + bookUniId + ", createOperator=" + createOperator + ", memberDrmLogs=" + memberDrmLogs + ", memberBookDrmMappings="
                + memberBookDrmMappings + "]";
    }

    private static DPLogger logger =DPLogger.getLogger(MemberBook.class.getName());
    private static final long serialVersionUID = 1L;
    public static final String EncryptTypeNone = "none";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Mutable
    @Column(name = "archived")
    private Boolean archived = false;

    @Mutable
    @Column(name = "book_type")
    private String bookType;

    @Mutable
    @Column(name = "passworded")
    private Boolean passworded = false;

    @Mutable
    @Column(name = "encrypt_type")
    private String encryptType = EncryptTypeNone;

    @Mutable
    @Column(name = "encrypt_key")
    private byte[] encryptKey;

    @Mutable
    @Column(name = "max_read_loc")
    private String maxReadLoc;

    @Mutable
    @Column(name = "last_loc")
    private String LastLoc;

    @Mutable
    @Column(name = "lock_status")
    private Integer lockStatus;

    @Mutable
    @Column(name = "operator")
    private String operator;

    @Mutable
    @Column(name = "member_id", insertable = false, updatable = false)
    private Integer memberId;

    @Mutable
    @Column(name = "book_file_id", insertable = false, updatable = false)
    private Integer bookFileId;

    @Mutable
    @Column(name = "readlist_idnames")
    @Array(databaseType = "text")
    // @Convert(converter = digipages.pgconverter.PGArrayConverter.class)
    private List<String> readlistIdnames = new ArrayList<String>();

    @Mutable
    @Column(name = "device_seq")
    @Array(databaseType = "int4")
    // @Convert(converter = digipages.pgconverter.PGIntArrayConverter.class)
    private List<Integer> deviceSeq;

    // bi-directional many-to-one association to BookFile
    @Mutable
    @JoinColumn(name = "book_file_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private BookFile bookFile;

    @Mutable
    @JoinColumn(name = "item_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Item item;

    @Column(name = "item_id", insertable = false, updatable = false)
    private String itemId;

    // bi-directional many-to-one association to Member
    @Mutable
    @JoinColumn(name = "member_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Member member;

    // bi-directional many-to-one association to MemberBookNote
    @Mutable
    @OneToMany(mappedBy = "memberBook", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MemberBookNote> memberBookNotes = new ArrayList<>();

    @Mutable
    @Column(name = "percentage")
    private int percentage;

    @Mutable
    @Column(name = "start_read_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date StartReadTime;

    @Mutable
    @Column(name = "last_read_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date LastReadTime;

    @Mutable
    @Column(name = "udt")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;

    @Mutable
    @Column(name = "archived_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date archivedDate;

    @Mutable
    @Column(name = "download_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date downloadTime;

    @Mutable
    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime = new Date();

    @Mutable
    @Column(name = "ask_update_version")
    private Boolean askUpdateVersion = false;

    @Mutable
    @Column(name = "bg_update_version")
    private Boolean bgUpdateVersion = false;

    @Mutable
    @Column(name = "version_locked")
    private Boolean versionLocked = false;

    @Mutable
    @Column(name = "book_highlight_status")
    private Boolean bookHighlightStatus = false;

    @Mutable
    @Column(name = "book_bookmark_status")
    private Boolean bookBookmarkStatus = false;

    @Mutable
    @Column(name = "newer_version")
    private String newerVersion;

    @Mutable
    @Column(name = "cur_version")
    private String curVersion;

    @Mutable
    @Column(name = "upgrade_time")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date upgradeTime;

    @Mutable
    @Column(name = "action")
    private String action = "update";

    @Mutable
    @Column(name = "is_hidden")
    private Boolean isHidden = false;

    @Mutable
    @Column(name = "is_book")
    private Boolean isBook = false;

    @Mutable
    @Column(name = "is_magazine")
    private Boolean isMagazine = false;

    @Mutable
    @Column(name = "is_trial")
    private Boolean isTrial = false;

    @Mutable
    @Column(name = "deleted")
    private Boolean deleted = false;

    @Mutable
    @Column(name = "book_uni_id")
    private String bookUniId = "";

    @Mutable
    @Column(name = "create_operator")
    private String createOperator;
    
    @Mutable
    @Column(name = "finish_flag")
    private String finishFlag;
    
    @Mutable
    @Column(name = "finish_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishTime;
    

    @PrePersist
    @PreUpdate
    public void onPrePersist() {
        if (lastUpdated == null) {
            lastUpdated = new Date();
            setLastUpdated(lastUpdated);
        }
    }
    
    @PreRemove
    public void onPreRemove() {

        try {
            logger.error("issue delete book:" + this.toString());
            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                System.out.println(ste);
            }
        } catch (Exception e) {

        }
    }

    public Date getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(Date lastUpdateTime) {
        this.lastUpdated = lastUpdateTime;
    }

    public void setLastUpdated(String lastUpdateTime) {
        ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
        setLastUpdated(Date.from(zdt.toInstant()));
    }

    //bi-directional many-to-one association to MemberDrmLog
    @ManyToMany
      @JoinTable(
          name="member_book_drm_mapping",
          joinColumns=@JoinColumn(name="member_book_id", referencedColumnName="id"),
          inverseJoinColumns=@JoinColumn(name="member_book_drm_log_id", referencedColumnName="id"))
    private List<MemberDrmLog> memberDrmLogs;

    //bi-directional many-to-one association to MemberBookDrmMapping
    @OneToMany(mappedBy="memberBook")
    private List<MemberBookDrmMapping> memberBookDrmMappings;

    public MemberBook() {
        touchUpdateTime();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getArchived() {
        return this.archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
        touchUpdateTime();
    }

    public String getBookType() {
        return this.bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
        touchUpdateTime();
    }

    public Boolean getPassworded() {
        return this.passworded;
    }

    public void setPassworded(Boolean passworded) {
        this.passworded = passworded;
        touchUpdateTime();
    }

    public BookFile getBookFile() {
        return this.bookFile;
    }

    public void setBookFile(BookFile bookFile) {
        this.bookFile = bookFile;
        this.setItem(bookFile.getItem());
        this.setCurVersion(bookFile.getVersion());
        touchUpdateTime();
    }

    public Member getMember() {
        return this.member;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
        touchUpdateTime();
    }

    public void setMember(Member member) {
        this.member = member;
        touchUpdateTime();
    }

    public List<MemberBookNote> getMemberBookNotes() {
        return this.memberBookNotes;
    }

    public void setMemberBookNotes(List<MemberBookNote> memberBookNotes) {
        this.memberBookNotes = memberBookNotes;
        touchUpdateTime();
    }

    public MemberBookNote addMemberBookNote(MemberBookNote memberBookNote) {
        getMemberBookNotes().add(memberBookNote);
        memberBookNote.setMemberBook(this);
        touchUpdateTime();
        return memberBookNote;
    }

       public void markAsDelete() {
            // Delete member book.
            // this.clearReadlistIdnames();
            //不需要刪除筆記
//        this.setMemberBookNoteDel(CommonUtil.zonedTime());
            this.setLastUpdated(CommonUtil.zonedTime());
            this.setAction("del");
            this.setDeleted(true);
        }

    public MemberBookNote removeMemberBookNote(MemberBookNote memberBookNote) {
        getMemberBookNotes().remove(memberBookNote);
        memberBookNote.setMemberBook(null);
        touchUpdateTime();
        return memberBookNote;
    }

    public void setMemberBookNoteBack(String memberBookNoteudt) {
        for (MemberBookNote mbn : getMemberBookNotes()) {
            if (!"del".equals(mbn.getAction())) {
                mbn.setLastUpdated(memberBookNoteudt);
                mbn.setAction("add");
            }
        }

        touchUpdateTime();
    }

    public void setMemberBookNoteDel(String memberBookNoteudt) {
        for (MemberBookNote mbn : getMemberBookNotes()) {
            if (null != mbn.getNoteContent()) {
                if ("del".equals(mbn.getAction())) {
                    mbn.getNoteContent().setOrigAction("del");
                } else {
                    mbn.getNoteContent().setOrigAction("add");
                }
            } else {
                NoteContent nc = new NoteContent();

                if ("del".equals(mbn.getAction())) {
                    nc.setOrigAction("del");
                } else {
                    nc.setOrigAction("add");
                }

                mbn.setNoteContent(nc);
            }

            mbn.setAction("del");
            // mbn.setLastUpdated(CommonUtil.zonedTime());
            mbn.setLastUpdated(memberBookNoteudt);
        }
    }

    public List<String> getReadlistIdnames(MemberBook mb) {
        List<String> readlist_idnames = new ArrayList<String>();

        if (mb.getAction().equals("del") || (null != mb.getIsHidden() && mb.getIsHidden() == true)) {
            if ((null != mb.getIsTrial() && mb.getIsTrial() == true) || mb.getBookFile().getIsTrial() == true) {
                readlist_idnames.add("trial");
            }

            return readlist_idnames;
        } else {
            if ((null != mb.getIsTrial() && mb.getIsTrial() == true) || mb.getBookFile().getIsTrial() == true) {
                readlist_idnames.add("trial");
            } else if ((null != mb.getArchived() && mb.getArchived() == true) || (null == mb.getReadlistIdnames() && mb.getArchived() == true)) {
                readlist_idnames.add("archive");
            } else if ((null != mb.getPassworded() && mb.getPassworded() == true) || (null == mb.getReadlistIdnames() && mb.getPassworded() == true)) {
                readlist_idnames.add("private");
            } else {
                
                if("mediabook".equalsIgnoreCase(mb.getBookType())) {
                    readlist_idnames.add("all");
                    readlist_idnames.add("mediabook");
                    if (mb.getReadlistIdnames().size() > 0) {
                        for (String readlistName : mb.getReadlistIdnames()) {
                            if (readlistName.contains("custom")) {
                                readlist_idnames.add(readlistName);
                            }
                        }
                    }
                }
                
                if("audiobook".equalsIgnoreCase(mb.getBookType())) {
                    readlist_idnames.add("all");
                    readlist_idnames.add("audiobook");
                    if (mb.getReadlistIdnames().size() > 0) {
                        for (String readlistName : mb.getReadlistIdnames()) {
                            if (readlistName.contains("custom")) {
                                readlist_idnames.add(readlistName);
                            }
                        }
                    }
                }
                
                if ((null != mb.getIsBook() && mb.getIsBook() == true) || "book".equals(mb.getItem().getItemType().toLowerCase())) {
                    readlist_idnames.add("all");
                    readlist_idnames.add("book");

                    if (mb.getReadlistIdnames().size() > 0) {
                        for (String readlistName : mb.getReadlistIdnames()) {
                            if (readlistName.contains("custom")) {
                                readlist_idnames.add(readlistName);
                            }
                        }
                    }
                } else if ((null != mb.getIsMagazine() && mb.getIsMagazine() == true) || "magazine".equals(mb.getItem().getItemType().toLowerCase())) {
                    readlist_idnames.add("all");
                    readlist_idnames.add("magazine");

                    if (mb.getReadlistIdnames().size() > 0) {
                        for (String readlistName : mb.getReadlistIdnames()) {
                            if (readlistName.contains("custom")) {
                                readlist_idnames.add(readlistName);
                            }
                        }
                    }
                }
            }
        }

        return readlist_idnames;
    }

    public List<String> getReadlistIdnames() {
        if (null == readlistIdnames)
            readlistIdnames = new ArrayList<String>();
        return this.readlistIdnames;
    }

    public void setReadlistIdnames(List<String> readlistIdnames) {
        this.readlistIdnames = readlistIdnames;
        touchUpdateTime();
    }

    public List<Integer> getDeviceId() {
        return deviceSeq;
    }

    public void setDeviceId(List<Integer> deviceSeq) {
        this.deviceSeq = deviceSeq;
        touchUpdateTime();
    }

    public void addDeviceId(int deviceSeq) {
        if (null == getDeviceId()) {
            setDeviceId(new ArrayList<Integer>());
        }

        if (!getDeviceId().contains(deviceSeq)) {
            getDeviceId().add(deviceSeq);
            setDeviceId(getDeviceId());
        }

        touchUpdateTime();
    }

    public String getEncryptType() {
        return encryptType;
    }

    public void setEncryptType(String encryptType) {
        this.encryptType = encryptType;
        touchUpdateTime();
    }

    public byte[] getEncryptKey() {
        return encryptKey;
    }

    public void setEncryptKey(byte[] encryptKey) {
        this.encryptKey = encryptKey;
        touchUpdateTime();
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
        touchUpdateTime();
    }

    public Date getStartReadTime() {
        return StartReadTime;
    }

    public void setStartReadTime(Date startReadTime) {
        StartReadTime = startReadTime;
        touchUpdateTime();
    }

    public Date getLastReadTime() {
        return LastReadTime;
    }

    public void setLastReadTime(Date lastReadTime) {
        LastReadTime = lastReadTime;
        touchUpdateTime();
    }

    public Date getArchivedDate() {
        return archivedDate;
    }

    public void setArchivedDate(Date archivedDate) {
        this.archivedDate = archivedDate;
        touchUpdateTime();
    }

    public Date getDownloadTime() {
        return downloadTime;
    }

    public void setDownloadTime(Date downloadTime) {
        this.downloadTime = downloadTime;
        touchUpdateTime();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getMaxReadLoc() {
        return maxReadLoc;
    }

    public void setMaxReadLoc(String maxReadLoc) {
        this.maxReadLoc = maxReadLoc;
        touchUpdateTime();
    }

    public String getLastLoc() {
        return LastLoc;
    }

    public void setLastLoc(String lastLoc) {
        LastLoc = lastLoc;
        touchUpdateTime();
    }

    public Integer getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Integer lockStatus) {
        this.lockStatus = lockStatus;
        touchUpdateTime();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
        touchUpdateTime();
    }

    public Boolean getAskUpdateVersion() {
        return askUpdateVersion;
    }

    public void setAskUpdateVersion(Boolean askUpdateVersion) {
        this.askUpdateVersion = askUpdateVersion;
        touchUpdateTime();
    }

    public Boolean getBgUpdateVersion() {
        return bgUpdateVersion;
    }

    public void setBgUpdateVersion(Boolean bgUpdateVersion) {
        this.bgUpdateVersion = bgUpdateVersion;
        touchUpdateTime();
    }

    public Boolean getVersionLocked() {
        return versionLocked;
    }

    public void setVersionLocked(Boolean versionLocked) {
        this.versionLocked = versionLocked;
        touchUpdateTime();
    }

    public Boolean getBookHighlightStatus() {
        return bookHighlightStatus;
    }

    public void setBookHighlightStatus(Boolean bookHighlightStatus) {
        this.bookHighlightStatus = bookHighlightStatus;
        touchUpdateTime();
    }

    public Boolean getBookBookmarkStatus() {
        return bookBookmarkStatus;
    }

    public void setBookBookmarkStatus(Boolean bookBookmarkStatus) {
        this.bookBookmarkStatus = bookBookmarkStatus;
    }

    public String getNewerVersion() {
        return newerVersion;
    }

    public void setNewerVersion(String newerVersion) {
        this.newerVersion = newerVersion;
        touchUpdateTime();
    }

    public String getCurVersion() {
        return curVersion;
    }

    public void setCurVersion(String curVersion) {
        this.curVersion = curVersion;
        touchUpdateTime();
    }

    public Date getUpgradeTime() {
        return upgradeTime;
    }

    public void setUpgradeTime(Date upgradeTime) {
        this.upgradeTime = upgradeTime;
        touchUpdateTime();
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        if (action.equals("del")) {
            this.setDeleted(true);
        } else {
            this.setDeleted(false);
        }

        this.action = action;
        touchUpdateTime();
    }

    public String getCreateOperator() {
        return createOperator;
    }

    public void setCreateOperator(String createOperator) {
        this.createOperator = createOperator;
    }

    public void removeFromReadList(String readListIdName) {
        if (null == readlistIdnames)
            readlistIdnames = new ArrayList<String>();

        if (readListIdName.equals("all") ||
                readListIdName.equals("magazine") ||
                readListIdName.equals("book")) // remove from all is meaning less
            return;
        
        readlistIdnames.remove(readListIdName);

        if (readListIdName.equals("trial")) {
            this.setAction("del");
        } else if (readListIdName.equals("private") ||
                readListIdName.equals("archive") ||
                readListIdName.equals("password")) {
            readlistIdnames.clear();
            setDefaultReadList();
            
            if ("archive".equals(readListIdName)) {
                setArchived(false);
            } else if ("password".equals(readListIdName) || "private".equals(readListIdName)) {
                setPassworded(false);
            }

            touchMemberBookNotesUpdateTime();
        }

        this.touchUpdateTime();
    }

    /**
     * set my update time to now
     */
    public void touchUpdateTime() {
        setLastUpdated(new Date());
    }

    public void touchMemberBookNotesUpdateTime() {
        for (MemberBookNote mb : getMemberBookNotes()) {
            if (!"del".equals(mb.getAction())) {
                int min = 1;
                int max = 10;
                int randomNum = new Random().nextInt((max - min) + 1) + min;
                int seconds = randomNum;

                java.sql.Timestamp ts = java.sql.Timestamp.valueOf(LocalDateTime.now().plusSeconds(seconds));

                mb.setLastUpdated(CommonUtil.fromDateToString(ts));
            }
        }
    }

    public void addToReadList(String readListIdName) {
    	
    	boolean isCustom =  readListIdName.contains("custom");
    	
        if (null == readlistIdnames)
            readlistIdnames = new ArrayList<String>();
        // to private/archived/trial = remove other attribute.
        if (readListIdName.equals("trial") ||
                readListIdName.equals("private") ||
                readListIdName.equals("archive") ||
                readListIdName.equals("password")) {
            readlistIdnames.clear();

            if ("archive".equals(readListIdName)) {
                setArchived(true);
                setArchivedDate(new Date());
            } else if ("password".equals(readListIdName) || "private".equals(readListIdName)) {
                setPassworded(true);
            }

            touchMemberBookNotesUpdateTime();
        } else {
            // all/customx can be book/magazine
            if (!readlistIdnames.contains("all"))
                readlistIdnames.add("all");
        }

        this.setAction("update");
        if(!isCustom) {
        	clearCustomIdNames();
        }
        
        if (!readlistIdnames.contains(readListIdName))
            readlistIdnames.add(readListIdName);
        this.touchUpdateTime();
    }

    private void clearCustomIdNames() {
        List <String> removeList = new ArrayList<>();
        for (String tmpIdName:readlistIdnames)
        {
            if (tmpIdName.startsWith("custom"))
            {
                removeList.add(tmpIdName);
            }
        }
        for (String tmpIdName:removeList)
        {
            readlistIdnames.remove(tmpIdName);
        }
        
    }

    public static MemberBook findByMemberBookUniId(EntityManager em, Long memberId, String bookUniId) throws ServerException {
        try {
            return em.createNamedQuery("MemberBook.findByMemberBookUniId", MemberBook.class)
                    .setParameter("memberId", memberId)
                    .setParameter("bookUniId", bookUniId)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<MemberBookNote> getHighLights(EntityManager em, MemberBook mb) {
        Query q = em.createNamedQuery("MemberBookNote.findHighLightByMmberBookIDandMemberId");
        q.setParameter("type", "highlight");
        q.setParameter("memberId", mb.getMember().getId());
        q.setParameter("bookFileId", mb.getBookFile().getId());
        return (List<MemberBookNote>) q.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<MemberBookNote> getBookMarks(EntityManager em, MemberBook mb) {
        Query q = em.createNamedQuery("MemberBookNote.findBookMarkByMmberBookIDandMemberId");
        q.setParameter("type", "bookmark");
        q.setParameter("memberId", mb.getMember().getId());
        q.setParameter("bookFileId", mb.getBookFile().getId());
        return (List<MemberBookNote>) q.getResultList();
    }

    public boolean matchsUniId(String bookUniId) throws ServerException {
        BookUniId b = new BookUniId(bookUniId);

        if (getItem().getId().equals(b.getItemId()) &&
                b.isTrial() == getBookFile().getIsTrial() &&
                b.getFormat().equals(getBookFile().getFormat()))
            return true;
        return false;
    }

    // set ReadList according info (Mag/Book)
    public void setDefaultReadList() {
        /*
         * rules:
         * from Magazine/Book
         * to
         * all,book,magazine,trial,private,archive,custom
         */

        // default archive, password not handled
        if (getReadlistIdnames().contains("archive") ||
                getReadlistIdnames().contains("password") ||
                getReadlistIdnames().contains("private")) {
            return;
        }

        // default trial book is not in any list, unless assigned.
        try {
            if (getBookFile().getIsTrial()) {
                addToReadList("trial");
                return;
            }
        } catch (Exception ex) {
            logger.error(ex);;
        }

        // normal case, mag/book, all
        String tmpType = getBookType().toLowerCase();

        if (tmpType.contains("magazine") || tmpType.contains("book")) {
            addToReadList(tmpType);
            addToReadList("all");
            return;
        }

        this.touchUpdateTime();
    }

    // set ReadList according info (Mag/Book)
    public MemberBook setDefaultReadList(MemberBook mb) {
        /*
         * rules:
         * from Magazine/Book
         * to
         * all,book,magazine,trial,private,archive,custom
         */

        if ("book".equals(mb.getBookFile().getItem().getItemType().toLowerCase())) {
            mb.setIsBook(true);
            mb.setIsMagazine(false);
        } else if ("magazine".equals(mb.getBookFile().getItem().getItemType().toLowerCase())) {
            mb.setIsBook(false);
            mb.setIsMagazine(true);
        } else if ("serial".equals(mb.getBookFile().getItem().getItemType().toLowerCase())) {
            mb.setIsBook(false);
            mb.setIsMagazine(false);
        }

        if (mb.getBookFile().getIsTrial()) {
            mb.setIsTrial(true);
        } else {
            mb.setIsTrial(false);
        }

        mb.setArchived(false);
        mb.setPassworded(false);
        mb.setIsHidden(false);
        mb.setDeleted(false);

        // default archive, password not handled
        if (getReadlistIdnames().contains("archive") ||
                getReadlistIdnames().contains("password") ||
                getReadlistIdnames().contains("private")) {
            return mb;
        }

        // default trial book is not in any list, unless assigned.
        try {
            if (getBookFile().getIsTrial()) {
                addToReadList("trial");
                return mb;
            }
        } catch (Exception ex) {
            logger.error(ex);;
        }

        // normal case, mag/book, all
        String tmpType = getBookType().toLowerCase();

        if (tmpType.contains("magazine") || tmpType.contains("book")) {
            addToReadList(tmpType);
            addToReadList("all");
            return mb;
        }

        this.touchUpdateTime();

        return mb;
    }

    public String getBookUniId() {
        return bookUniId;
    }

    public void setBookUniId(String bookUniId) {
        this.bookUniId = bookUniId;
        this.touchUpdateTime();
    }

    /**
     * for all notes (include bookmark/annotation/feedback)
     * but if already have feedback, don't replace it.
     * @param tmpBook
     */
    public void mergeNotes(MemberBook tmpBook) {
        boolean hasFeedback = hasFeedback();
        List<MemberBookNote> tmpTarget = new ArrayList<>();
        tmpTarget.addAll(tmpBook.getMemberBookNotes());

        for (MemberBookNote mbn : tmpTarget) {
            // don't merge feed back if exists
            if (hasFeedback && "feedback".equals(mbn.getType())) {
                continue;
            }

            this.addMemberBookNote(mbn);
            //電子書改版不要update
//            mbn.setLastUpdated(CommonUtil.zonedTime());
            mbn.setAction("update");
        }
    }

    private boolean hasFeedback() {
        for (MemberBookNote myNote : getMemberBookNotes()) {
            if ("feedback".equals(myNote.getType()))
                return true;
        }

        return false;
    }

    public void clearReadlistIdnames() {
        readlistIdnames.clear();
        touchUpdateTime();
    }

    public boolean isDeleted() {
        return getAction().equals("del");
    }

    public void unDelete() {
        this.setDefaultReadList();
        this.setMemberBookNoteDel(CommonUtil.zonedTime());
        this.setLastUpdated(CommonUtil.zonedTime());
        this.setAction("update");
    }

    public Boolean getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(Boolean isHidden) {
        this.isHidden = isHidden;
    }

    public Boolean getIsBook() {
        return isBook;
    }

    public void setIsBook(Boolean isBook) {
        this.isBook = isBook;
    }

    public Boolean getIsMagazine() {
        return isMagazine;
    }

    public void setIsMagazine(Boolean isMagazine) {
        this.isMagazine = isMagazine;
    }

    public Boolean getIsTrial() {
        return isTrial;
    }

    public void setIsTrial(Boolean isTrial) {
        this.isTrial = isTrial;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void updateByBookFile(BookFile bookFile) throws ServerException {
        setBookUniId(bookFile.getBookUniId());
        setBookFile(bookFile);
        setItem(bookFile.getItem());
        setBookType(bookFile.getItem().getInfo().getType());

        if (bookFile.getIsTrial()) {
            setEncryptType(EncryptTypeNone);
        } else {
            setEncryptType(Enc01.EncryptType);

            try {
                byte[] encryptionKey;
                encryptionKey = CommonUtil.generateAES256Key();
                setEncryptKey(encryptionKey);
            } catch (NoSuchAlgorithmException e) {
                throw new ServerException("id_err_999", "Internal Error");
            }
        }
    }

    public static MemberBook findByBookFileId(EntityManager em, long memberId, long bookFileId) {
        try {
            return em.createNamedQuery("MemberBook.findByMemberIdAndBookFileId", MemberBook.class)
                    .setParameter("memberId", memberId)
                    .setParameter("bookFileId", bookFileId)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    public static MemberBook findTrialByItemId(EntityManager em, long memberId, String itemId) {
        try {
            List<MemberBook> myBooks = em.createNamedQuery("MemberBook.findAllTrialByMemberIdAndItemId", MemberBook.class)
                    .setParameter("memberId", memberId)
                    .setParameter("itemId", itemId)
                    .getResultList();

            for (MemberBook tmpBook : myBooks) {
                if (tmpBook.getBookFile().getIsTrial())
                    return tmpBook;
            }

        } catch (Exception ex) {
            return null;
        }
        return null;
    }

    public static MemberBook findNormalByItemId(EntityManager em, Long memberId, String itemId) {
        try {
            List<MemberBook> myBooks = em.createNamedQuery("MemberBook.findNormalByMemberIdAndItemId", MemberBook.class)
                    .setParameter("memberId", memberId)
                    .setParameter("itemId", itemId)
                    .getResultList();

            for (MemberBook tmpBook : myBooks) {
                if (!tmpBook.getBookFile().getIsTrial())
                    return tmpBook;
            }

            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    public static List<MemberBook> listAllMemberBook(EntityManager em) {
        @SuppressWarnings("unchecked")
        List<MemberBook> myBooks = em.createNativeQuery("SELECT * FROM member_book", MemberBook.class).getResultList();

        return myBooks;
    }

    public static String findMemberBookLastUpdateTime(EntityManager em, Long memberId) {
        try {
            List<MemberBook> myBooks = em.createNamedQuery("MemberBook.findAllByMemberId", MemberBook.class)
                    .setParameter("memberId", memberId)
                    .setMaxResults(1)
                    .getResultList();

            if (myBooks.size() > 0) {
                for (MemberBook tmpBook : myBooks) {
                    return CommonUtil.fromDateToString(tmpBook.lastUpdated);
                }
            } else {
                return CommonUtil.zonedTime();
            }
        } catch (Exception ex) {
            return CommonUtil.zonedTime();
        }

        return CommonUtil.zonedTime();
    }

    public static MemberBook findMemberBookByBookFile(EntityManager postgres, Member member, BookFile bookFile) {
        MemberBook tmpMemberBook = null;

        try {
            tmpMemberBook = MemberBook.findByMemberBookUniId(postgres, member.getId(), bookFile.getBookUniId());

            if (null == tmpMemberBook || null == tmpMemberBook.getBookFile()) {
                tmpMemberBook = new MemberBook();

                String bookType = "";

                List<String> readlist_idnames = new ArrayList<String>();

                if (bookFile.getIsTrial()) {
                    readlist_idnames.add("trial");

                    if (bookFile.getItem().getItemType().toLowerCase().equals("book") || bookFile.getItem().getItemType().toLowerCase().equals("serial")) {
                        bookType = "book";
                    } else if (bookFile.getItem().getItemType().toLowerCase().equals("magazine")) {
                        bookType = "magazine";
                    }else if (bookFile.getItem().getItemType().toLowerCase().equals("mediabook")) {
                        bookType = "mediabook";
                    }else if (bookFile.getItem().getItemType().toLowerCase().equals("audiobook")) {
                        bookType = "audiobook";
                    }
                } else {
                    if (bookFile.getItem().getItemType().toLowerCase().equals("book") || bookFile.getItem().getItemType().toLowerCase().equals("serial")) {
                        readlist_idnames.add("all");
                        readlist_idnames.add("book");

                        bookType = "book";
                    } else if (bookFile.getItem().getItemType().toLowerCase().equals("magazine")) {
                        readlist_idnames.add("all");
                        readlist_idnames.add("magazine");

                        bookType = "magazine";
                    }else if (bookFile.getItem().getItemType().toLowerCase().equals("mediabook")) {
                        bookType = "mediabook";
                    }else if (bookFile.getItem().getItemType().toLowerCase().equals("audiobook")) {
                        bookType = "audiobook";
                    }
                }

                // fake response, don't write to db (add to member)
                tmpMemberBook.setItem(bookFile.getItem());
                tmpMemberBook.setBookFile(bookFile);
                tmpMemberBook.setStartReadTime(Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant()));
                tmpMemberBook.setLastReadTime(Date.from(CommonUtil.parseDateTime("1900-01-01T00:00:00+0800").toInstant()));
                tmpMemberBook.setMember(member);
                tmpMemberBook.setBookType(bookType);
                tmpMemberBook.setBookUniId(bookFile.getBookUniId());
                tmpMemberBook.setReadlistIdnames(readlist_idnames);
                tmpMemberBook.setDefaultReadList();
                tmpMemberBook.setPercentage(0);
                tmpMemberBook.setLastLoc("");
                tmpMemberBook.setDownloadTime(bookFile.getLastUpdated());
                tmpMemberBook.setLastUpdated(CommonUtil.fromDateToString(bookFile.getLastUpdated()));
                tmpMemberBook.setAskUpdateVersion(false);
                tmpMemberBook.setBgUpdateVersion(false);
                tmpMemberBook.setVersionLocked(false);
                tmpMemberBook.setBookHighlightStatus(false);
                tmpMemberBook.setCurVersion(bookFile.getVersion());
                tmpMemberBook.setCreateTime(bookFile.getCreateTime());
                tmpMemberBook.setItem(bookFile.getItem());
            }
        } catch (ServerException e) {
            logger.error(e);;
        }

        return tmpMemberBook;
    }

    public static void removeReadList(EntityManager postgres, Long memberId, String customTypeId) {
        postgres.createNamedQuery("MemberBook.removeFromReadList",MemberBook.class)
        .setParameter(1, customTypeId)
        .setParameter(2,customTypeId)
        .setParameter(3, memberId)
        .executeUpdate();
        postgres.flush();
        
    }

    public List<MemberBookDrmMapping> getMemberBookDrmMappings() {
        return this.memberBookDrmMappings;
    }

    public void setMemberBookDrmMappings(List<MemberBookDrmMapping> memberBookDrmMappings) {
        this.memberBookDrmMappings = memberBookDrmMappings;
    }

    public MemberBookDrmMapping addMemberBookDrmMapping(MemberBookDrmMapping memberBookDrmMapping) {
        getMemberBookDrmMappings().add(memberBookDrmMapping);
        memberBookDrmMapping.setMemberBook(this);

        return memberBookDrmMapping;
    }

    public MemberBookDrmMapping removeMemberBookDrmMapping(MemberBookDrmMapping memberBookDrmMapping) {
        getMemberBookDrmMappings().remove(memberBookDrmMapping);
        memberBookDrmMapping.setMemberBook(null);

        return memberBookDrmMapping;
    }
    
    public List<MemberDrmLog> getMemberDrmLogs() {
        return memberDrmLogs;
    }

    public void setMemberDrmLogs(List<MemberDrmLog> memberDrmLogs) {
        this.memberDrmLogs = memberDrmLogs;
    }

	public String getFinishFlag() {
		return finishFlag;
	}

	public void setFinishFlag(String finishFlag) {
		this.finishFlag = finishFlag;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

}