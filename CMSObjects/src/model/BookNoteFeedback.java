package model;

public class BookNoteFeedback extends BaseBookNote {
	private String note;

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}