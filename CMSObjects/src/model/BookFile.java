package model;

import java.io.Serializable;

import javax.persistence.*;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import digipages.common.DPLogger;
import digipages.exceptions.ServerException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the book_file database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 3000, // 3sec
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name="book_file")
@NamedQueries({
	@NamedQuery(name="BookFile.findAll", query="SELECT b FROM BookFile b"),
	@NamedQuery(name="BookFile.findByItemId" , 
	query="SELECT b FROM BookFile b WHERE b.item.id=:item_id order by b.id ASC"),
	@NamedQuery(
			name = "BookFile.findProcessedBookFileByItemId",
			query = "SELECT b FROM BookFile b"
					+ " WHERE b.item.id = :item_id"
					+ " AND b.status >= 8"
					+ " ORDER BY b.id ASC"),
	@NamedQuery(
			name = "BookFile.findByItemIdForThirdPartyAccount",
			query = "SELECT b FROM BookFile b"
					+ " WHERE b.item.id = :item_id"
					+ " AND b.status >= 8"
					+ " ORDER BY b.id ASC"),
	@NamedQuery(
			name = "BookFile.findByItemIdForThumbGenerator",
			query = "SELECT b FROM BookFile b"
					+ " WHERE b.item.id = :item_id"
					+ " ORDER BY b.id ASC"),
	@NamedQuery(
			name = "BookFile.findByItemIdForTrialBook",
			query = "SELECT b FROM BookFile b"
					+ " WHERE b.item.id = :item_id"
					+ " AND b.isTrial = :isTrial"
					+ " AND b.status = 9"
					+ " ORDER BY b.id ASC"),
	@NamedQuery(
			name = "BookFile.findByItemIdForNormalReader",
			query = "SELECT b FROM BookFile b"
					+ " WHERE b.item.id = :item_id"
					+ " AND b.status = 9"
					+ " ORDER BY b.id ASC"),
	@NamedQuery(
			name = "BookFile.findByItemIdAndTrialPrivilaged",
			query = "SELECT b FROM BookFile b"
					+ " WHERE b.item.id = :item_id"
					+ " AND b.isTrial = :isTrial"
					+ " AND b.status >= 8"
					+ " ORDER BY b.lastUpdated DESC"),
	@NamedQuery(
			name = "BookFile.findByItemIdAndTrial",
			query = "SELECT b FROM BookFile b"
					+ " WHERE b.item.id = :item_id"
					+ " AND b.isTrial = :isTrial"
					+ " AND b.status = 9"
					+ " ORDER BY b.lastUpdated DESC"),
	@NamedQuery(name="BookFile.findById" , query="SELECT b FROM BookFile b WHERE b.id=:id"),
	@NamedQuery(name="BookFile.findByBookUniId" , 
	query="SELECT b FROM BookFile b WHERE "
			+ "b.item.id=:itemId "
			+ "and b.isTrial=:isTrial "
			+ "and b.format=:format "
			+ "AND b.status=9 "
			+ "order by b.lastUpdated Desc"),
	@NamedQuery(name="BookFile.findByBookUniIdAndVersion", 
	query="SELECT b FROM BookFile b WHERE "
			+ "b.item.id=:itemId "
			+ "and b.version=:version "
			+ "AND b.status=9"),
	@NamedQuery(name="BookFile.findByItemIdAndVersion", 
	query="SELECT b FROM BookFile b"
			+ " WHERE b.itemId = :itemId"
			+ " AND b.isTrial = :isTrial"
			+ " AND b.version = :version"),
	@NamedQuery(name="BookFile.findLastVersionByBookUniIdPrivilaged" , 
	query="SELECT b FROM BookFile b WHERE "
			+ "b.item.id=:itemId "
			+ "and b.isTrial=:isTrial "
			+ "and b.format=:format "
			+ "AND b.status>=8 "
			+ "order by b.version Desc"),
	@NamedQuery(name="BookFile.findLastVersionByBookUniIdThumbnail" , 
	query="SELECT b FROM BookFile b WHERE "
			+ "b.item.id=:itemId "
			+ "and b.isTrial=:isTrial "
			+ "and b.format=:format "
			+ "AND b.status>=0 "
			+ "order by b.version Desc"),
	@NamedQuery(name="BookFile.findLastVersionByBookUniId" , 
	query="SELECT b FROM BookFile b WHERE "
			+ "b.item.id=:itemId "
			+ "and b.isTrial=:isTrial "
			+ "and b.format=:format "
			+ "AND b.status=9 "
			+ "order by b.version Desc"),
	@NamedQuery(name="BookFile.findLastVersionByItemId" , 
	query="SELECT b FROM BookFile b WHERE "
			+ "b.item.id=:itemId "
			+ "and b.isTrial=:isTrial "
			+ "AND b.status=9 "
			+ "order by b.version Desc"),
	@NamedQuery(name="BookFile.findByItemIdandVersionForBook" 
	, query="SELECT b FROM BookFile b "
			+ "WHERE b.version=:version AND b.itemId=:itemId "
			+ "AND b.isTrial=:isTrial ")
})

@NamedNativeQueries({
	@NamedNativeQuery(name="BookFile.getRandomBookFile", 
		query="select * from book_file order by random() limit 1", resultClass=BookFile.class),
	@NamedNativeQuery(name="BookFile.getRandomTrialBookFile", 
		query="select * from book_file where format = ? and is_trial=true and status=9 order by random() limit 1", resultClass=BookFile.class),
	@NamedNativeQuery(name="BookFile.getRandomRealBookFile", 
		query="select * from book_file where format = ? and is_trial=false and status=9 order by random() limit 1", resultClass=BookFile.class),

	@NamedNativeQuery(name="BookFile.getNewRandomRealBookFileForMember", 
		query="select * from book_file where id not in "
				+ " ( select book_file_id from member_book where member_id = ? )"
				+ " and random() < 0.5 and item_id not like 'bench-sample%' order by random() ", resultClass=BookFile.class),
	
	@NamedNativeQuery(name="BookFile.getNewRandomBenchBookFileForMember", 
	query="select * from book_file where id not in "
			+ " ( select book_file_id from member_book where member_id = ? )"
			+ " and random() < 0.5 and item_id like 'bench-sample%' order by random() ", resultClass=BookFile.class),
	
	@NamedNativeQuery(name="BookFile.listRandomTrialReflowableBookFiles", 
	query="select * from book_file where is_trial=true and format='reflowable' and "
			+ " random() < 0.8 order by random() ", resultClass=BookFile.class),

	@NamedNativeQuery(name="BookFile.listRandomTrialPDFBookFiles", 
	query="select * from book_file where is_trial=true and format='pdf' and "
			+ " random() < 0.8 order by random() ", resultClass=BookFile.class),

	@NamedNativeQuery(name="BookFile.getLastestBookByItemId", 
	query="select * from book_file where item_id = ? and is_trial=false order by version desc limit 1;", resultClass=BookFile.class),

	@NamedNativeQuery(name="BookFile.getLastestAuditedBookByItemId", 
	query="select * from book_file where item_id = ? and is_trial=false and status=9 order by version desc limit 1;", resultClass=BookFile.class),
	
	@NamedNativeQuery(name="BookFile.getNewTrialBookByItemId", 
		query="select * from book_file where item_id = ? and is_trial=true order by version desc limit 1;", resultClass=BookFile.class),
})
public class BookFile extends digipages.common.BasePojo implements Serializable,Cloneable {
	private static final DPLogger logger = DPLogger.getLogger(BookFile.class.getName());
	private static final long serialVersionUID = 1L;

	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
		}  
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer id;

	@Mutable
	@Column(name = "file_location")
	private String fileLocation;

	@Mutable
	@Column(name = "orig_file_location")
	private String origFileLocation;

	@Mutable
	@Column(name = "format")
	private String format;

	@Mutable
	@Column(name = "orig_format")
	private String origFormat;
	
	@Mutable
	@Column(name = "is_trial")
	private Boolean isTrial;

	@Mutable
	@Column(name = "version")
	private String version;

	@Mutable
	@Column(name = "size")
	private long size;

	@Mutable
	@Column(name = "item_id", insertable = false, updatable = false)
	private String itemId;

	@Mutable
	@Column(name = "checksum")
	private String checksum;

	@Mutable
	@Column(name = "src_fname")
	private String src_fname;

	@Mutable
	@Column(name = "status")
	private Integer status=0;// default is 

	@Mutable
	@Column(name = "response_msg")
	private String responseMsg;

//	@Mutable
//	@Column(name = "chapter_meter")
//	@Convert(converter = digipages.pgconverter.PGGenericJsonContentConverter.class)
//	private Object chapterMeter;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	// bi-directional many-to-one association to Item
	@Mutable
	@JoinColumn(name = "item_id")
	@ManyToOne
	@JsonIgnore
	private Item item;

	// bi-directional many-to-one association to MemberBook
	@OneToMany(mappedBy = "bookFile", cascade = CascadeType.ALL)
	private List<MemberBook> memberBooks;

	// bi-directional many-to-one association to RecommendBook
	@OneToMany(mappedBy = "bookFile", cascade = CascadeType.ALL)
	private List<RecommendBook> RecommendBooks;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime = new Date();

	@Mutable
	@Column(name = "audit_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date auditTime;

	@Mutable
	@Column(name = "operator")
	private String operator;

	public Date getLastUpdated() {
		return this.lastUpdated;
	}
	
	public static List<BookFile> findByBookUniId(EntityManager em, String bookUniId) throws ServerException{
		BookUniId buid = new BookUniId(bookUniId);
		List <BookFile> ret = em.createNamedQuery("BookFile.findByBookUniId",BookFile.class)
				.setParameter("itemId", buid.getItemId())
				.setParameter("isTrial", buid.isTrial())
				.setParameter("format", buid.getFormat()).getResultList();
		return ret;
	}

	public static BookFile findByItemIdAndVersion(EntityManager em, String itemId, String version) {
		BookFile ret = null;

		try {
			ret = em.createNamedQuery("BookFile.findByItemIdAndVersion", BookFile.class)
					.setParameter("itemId", itemId)
					.setParameter("isTrial", false)
					.setParameter("version", version)
					.setMaxResults(1)
					.getSingleResult();
		} catch (Exception ex) {
			logger.warn("\nWarn Book not found, BookFile.findByItemIdAndVersion:" + itemId + 
					"\nversion: " + version + "\n"+ ex.getMessage());
		}

		return ret;
	}

	public static BookFile findByBookUniIdAndVersion(EntityManager em, String bookUniId, String version) throws ServerException {
		BookUniId buid = new BookUniId(bookUniId);
		BookFile ret = null;

		try {
			ret = em.createNamedQuery("BookFile.findByBookUniIdAndVersion",BookFile.class)
					.setParameter("itemId", buid.getItemId())
					.setParameter("version", version)
					.setMaxResults(1)
					.getSingleResult();
		} catch (Exception ex) {
			logger.warn("\nWarn Book not found, BookFile.findByBookUniIdAndVersion:" +bookUniId+
					"\nversion: " + version+"\n"+ex.getMessage());
		}

		return ret;
	}
	
	
	
	public static BookFile findLastVersionByBookUniIdThumbnail(EntityManager em, String bookUniId) throws ServerException{
		BookUniId buid = new BookUniId(bookUniId);
		BookFile ret = null;

		try {
			ret = em.createNamedQuery("BookFile.findLastVersionByBookUniIdThumbnail",BookFile.class)
					.setParameter("itemId", buid.getItemId())
					.setParameter("format", buid.getFormat())
					.setParameter("isTrial", buid.isTrial())
					.setMaxResults(1)
					.getSingleResult();
		} catch (Exception ex) {
			logger.warn("\nWarn Book not found, BookFile.FindLastVersionByBookUniIdPrivileged:" +bookUniId+
					"\nItemId: " +buid.getItemId() + 
					"\nformat: "+buid.getFormat()  + 
					"\nisTrial: " + buid.isTrial() + 
					"\nException: " + ex.getMessage());
		}

		return ret;
	}
	
	public static BookFile findLastVersionByBookUniIdPrivilaged(EntityManager em, String bookUniId) throws ServerException{
		BookUniId buid = new BookUniId(bookUniId);
		BookFile ret = null;

		try {
			ret = em.createNamedQuery("BookFile.findLastVersionByBookUniIdPrivilaged",BookFile.class)
					.setParameter("itemId", buid.getItemId())
					.setParameter("format", buid.getFormat())
					.setParameter("isTrial", buid.isTrial())
					.setMaxResults(1)
					.getSingleResult();
		} catch (Exception ex) {
			logger.warn("\nWarn Book not found, BookFile.FindLastVersionByBookUniIdPrivileged:" +bookUniId+
					"\nItemId: " +buid.getItemId() + 
					"\nformat: "+buid.getFormat()  + 
					"\nisTrial: " + buid.isTrial() + 
					"\nException: " + ex.getMessage());
		}

		return ret;
	}

	public static BookFile findLastVersionByItemId(EntityManager em, String itemId) throws ServerException {
		BookFile ret = null;

		try {
			ret = em.createNamedQuery("BookFile.findLastVersionByItemId", BookFile.class)
					.setParameter("itemId", itemId)
					.setParameter("isTrial", false)
					.setMaxResults(1)
					.getSingleResult();
		} catch (Exception ex) {
			logger.warn("\nWarn Book not found, BookFile.findLastVersionByItemId:ItemId: " + itemId);
		}

		return ret;
	}

	public static BookFile findLastVersionByItemIdAndTrial(EntityManager em, String itemId) throws ServerException {
		BookFile ret = null;

		try {
			ret = em.createNamedQuery("BookFile.findLastVersionByItemId", BookFile.class)
					.setParameter("itemId", itemId)
					.setParameter("isTrial", true)
					.setMaxResults(1)
					.getSingleResult();
		} catch (Exception ex) {
			logger.warn("\nWarn Trial Book not found, BookFile.findLastVersionByItemId:ItemId: " + itemId);
		}

		return ret;
	}

	public static BookFile findLastVersionByBookUniId(EntityManager em, String bookUniId) throws ServerException {
		BookUniId buid = new BookUniId(bookUniId);
		BookFile ret = null;

		try {
			ret = em.createNamedQuery("BookFile.findLastVersionByBookUniId",BookFile.class)
					.setParameter("itemId", buid.getItemId())
					.setParameter("format", buid.getFormat())
					.setParameter("isTrial", buid.isTrial())
					.setMaxResults(1)
					.getSingleResult();
		} catch (Exception ex) {
			logger.warn("\nWarn Book not found, BookFile.FindLastVersionByBookUniId:" +bookUniId+
					"\nItemId: " +buid.getItemId() + 
					"\nformat: "+buid.getFormat()  + 
					"\nisTrial: " + buid.isTrial() );
		}

		return ret;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public BookFile() {

	}

	public Integer getId() {
		return this.id;
	}

	public String getUniId() {
		return BookFile.bookUniIdfromBookFile(this);
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileLocation() {
		return this.fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	
	public String getOrigFileLocation() {
		return this.origFileLocation;
	}

	public void setOrigFileLocation(String origFileLocation) {
		this.origFileLocation = origFileLocation;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getOrigFormat() {
		return this.origFormat;
	}

	public void setOrigFormat(String origFormat) {
		this.origFormat = origFormat;
	}

	
	public Boolean getIsTrial() {
		return this.isTrial;
	}

	public void setIsTrial(Boolean isTrial) {
		this.isTrial = isTrial;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long fileSize) {
		this.size = fileSize;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getResponseMsg() {
		return this.responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public List<MemberBook> getMemberBooks() {
		return this.memberBooks;
	}

	public void setMemberBooks(List<MemberBook> memberBooks) {
		this.memberBooks = memberBooks;
	}

	public MemberBook addMemberBook(MemberBook memberBook) {
		getMemberBooks().add(memberBook);
		memberBook.setBookFile(this);

		return memberBook;
	}

	public MemberBook removeMemberBook(MemberBook memberBook) {
		getMemberBooks().remove(memberBook);
		memberBook.setBookFile(null);

		return memberBook;
	}

	public List<RecommendBook> getRecommendBooks() {
		return this.RecommendBooks;
	}

	public void setRecommendBooks(List<RecommendBook> RecommendBooks) {
		this.RecommendBooks = RecommendBooks;
	}

	public RecommendBook addRecommendBook(RecommendBook RecommendBook) {
		getRecommendBooks().add(RecommendBook);
		RecommendBook.setBookFile(this);

		return RecommendBook;
	}

	public RecommendBook removeRecommendBook(RecommendBook RecommendBook) {
		getRecommendBooks().remove(RecommendBook);
		RecommendBook.setBookFile(null);

		return RecommendBook;
	}

	/**
	 * warning, performance bottleneck.
	 * 
	 * @param em
	 * @return
	 */
	public static BookFile getRandomBookFile(EntityManager em) {
		Query q1 = em.createNativeQuery("select * from book_file order by random() limit 1", BookFile.class);
		q1.setHint("javax.persistence.cache.storeMode", "REFRESH");
		BookFile bf = (BookFile) q1.getSingleResult();
		// logger.info("Got bookfile id" + bf);
		return bf;
	}

	public static BookFile findNewTrialByItemId(EntityManager em, String itemId) {
		try {
			BookFile bf = em.createNamedQuery("BookFile.getNewTrialBookByItemId", BookFile.class).setParameter(1, itemId).getSingleResult();
			return bf;
		} catch (Exception ex) {
			logger.warn("Find new Trial found Exception "+ex.getMessage());
			return null;
		}
	}
	
	public static BookFile findLastestByItemId(EntityManager em, String itemId) {
		try {
			
			BookFile bf = em.createNamedQuery("BookFile.getLastestBookByItemId", 
					BookFile.class)
					.setParameter(1, itemId)
					.getSingleResult();
			return bf;
		} catch (Exception ex) {
			logger.warn("Find new Trial found Exception "+ex.getMessage());
			return null;
		}
	}

	public static BookFile findLastestReadyByItemId(EntityManager em, String itemId ) {
		try {
			
			BookFile bf = em.createNamedQuery("BookFile.getLastestAuditedBookByItemId", 
					BookFile.class)
					.setParameter(1, itemId)
					.getSingleResult();
			return bf;
		} catch (Exception ex) {
			logger.warn("Find new Trial found Exception "+ex.getMessage());
			return null;
		}
	}
	

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getSrc_fname() {
		return src_fname;
	}

	public void setSrc_fname(String src_fname) {
		this.src_fname = src_fname;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setCreateTime(Date createDateTime) {
		this.createTime = createDateTime;
	}

	public void setCreateTime(String createDateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(createDateTime);
		setCreateTime(Date.from(zdt.toInstant()));
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setAuditTime(Date auditDateTime) {
		this.auditTime = auditDateTime;
	}

	public void setAuditTime(String auditDateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(auditDateTime);
		setAuditTime(Date.from(zdt.toInstant()));
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

//	public Object getChapterMeter() {
//		return chapterMeter;
//	}
//
//	public void setChapterMeter(Object chapterMeter) {
//		this.chapterMeter = chapterMeter;
//	}

	public static String bookUniIdfromBookFile(BookFile bf) {
		String bookUniId = bf.getItem().getId() 
				+"_"+ bf.getFormat()
				+ "_" +bf.getIsTrial();
		return bookUniId;
	}

	/**
	 * from jdbc resultset.
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	public static BookFile fromResultSet(ResultSet rs) throws SQLException {
		BookFile bf = new BookFile();
		bf.setChecksum(rs.getString("checksum"));
		bf.setFileLocation(rs.getString("file_location"));
		bf.setFormat(rs.getString("format"));
		bf.setId(rs.getInt("id"));
		bf.setIsTrial(rs.getBoolean("is_trial"));
		// itemid not used.
		// bf.setItem(item);
		bf.setLastUpdated(rs.getDate("udt"));
		// memberBooks not used yet.
		// bf.setMemberBooks(memberBooks);
		// bf.setRecommendBooks(RecommendBooks);;
		bf.setSize(rs.getInt("size"));
		bf.setVersion(rs.getString("version"));

		return bf;
	}

	/**
	 * 
	 * @param em
	 * @param m
	 * @param size2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<BookFile> listNewRandomRealBookFilesForMember(EntityManager em, Member m, int size) {
		Query q = em.createNamedQuery("BookFile.getNewRandomRealBookFileForMember");
		if (null == m.getId())
			q.setParameter(1, -1L);
		else
			q.setParameter(1, m.getId());
		q.setMaxResults(size);

		return (List<BookFile>) q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public static List<BookFile> listNewRandomBenchBookFilesForMember(EntityManager em, Member m, int size) {
		Query q = em.createNamedQuery("BookFile.getNewRandomBenchBookFileForMember");
		if (null == m.getId())
			q.setParameter(1, -1L);
		else
			q.setParameter(1, m.getId());
		q.setMaxResults(size);

		return (List<BookFile>) q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public static List<BookFile> listRandomTrialReflowableBookFiles(EntityManager em, int size) {
		Query q = em.createNamedQuery("BookFile.listRandomTrialReflowableBookFiles");
		q.setMaxResults(size);
		return (List<BookFile>) q.getResultList();
	}

	public static List<BookFile> findByItem(EntityManager em, Item item2) {
		return item2.getBookFiles();
	}

	@SuppressWarnings("unchecked")
	public static List<BookFile> listRandomTrialPDFBookFiles(EntityManager em, int size) {
		Query q = em.createNamedQuery("BookFile.listRandomTrialPDFBookFiles");
		q.setMaxResults(size);
		return (List<BookFile>) q.getResultList();
	}

	public static BookFile getRandomTrialBookFile(EntityManager em, String format) {
		BookFile bf = em.createNamedQuery("BookFile.getRandomTrialBookFile",BookFile.class)
				.setParameter(1, format)
				.getSingleResult();
		
		return bf;
	}

	public static BookFile getRandomRealBookFile(EntityManager em, String format) {
		BookFile bf = em.createNamedQuery("BookFile.getRandomRealBookFile",BookFile.class)
				.setParameter(1, format)
				.getSingleResult();
		return bf;
	}

	public String getBookUniId() {
		BookUniId bid = new BookUniId();
		bid.setItemId(getItem().getId());
		bid.setFormat(getFormat());
		bid.setTrial(isTrial);
		return bid.toString();
	}

	public boolean newerThan(BookFile that) {
		// null==> no version
		if (null == that || that.getVersion() == null)
			return true;
		BookVersion thisVersion = new BookVersion(this.getVersion());
		BookVersion thatVersion = new BookVersion(that.getVersion());

		return thisVersion.newer(thatVersion);
	}

	public static List<BookFile> findByItemIdAndTrial(EntityManager em, Member member, String item_id, boolean isTrial) {
		List<BookFile> bookFiles;

		if (member.isSuperReader() || member.isPublisher() || member.isVendor()) {
			bookFiles = em.createNamedQuery("BookFile.findByItemIdAndTrialPrivilaged", BookFile.class)
					.setParameter("item_id", item_id)
					.setParameter("isTrial", isTrial)
					.setMaxResults(1)
					.getResultList();			
		} else {
			bookFiles = em.createNamedQuery("BookFile.findByItemIdAndTrial", BookFile.class)
					.setParameter("item_id", item_id)
					.setParameter("isTrial", isTrial)
					.setMaxResults(1)
					.getResultList();
		}

		return bookFiles;
	}
	
	public static List<BookFile> findByItemIdAndTrialPrivilaged(EntityManager em,  String item_id, boolean isTrial) {
        List<BookFile> bookFiles;

            bookFiles = em.createNamedQuery("BookFile.findByItemIdAndTrialPrivilaged", BookFile.class)
                    .setParameter("item_id", item_id)
                    .setParameter("isTrial", isTrial)
                    .setMaxResults(1)
                    .getResultList();           
        return bookFiles;
    }

	/**
	 * according to my version, find the max ready version this item has.
	 * if that version is newer than me, return true;
	 * @return
	 */
	public boolean hasNewerReadyVersion(boolean isTrial) {

		BookVersion thatVersion=new BookVersion(this.getItem().getNewestReadyVersion(isTrial));
		BookVersion thisVersion = new BookVersion(this.getVersion());
		
		if (thatVersion.newer(thisVersion))
			return true;
		return false;
	}

	public static void deleteCascade(EntityManager postgres, BookFile bf) {
		postgres.remove(bf);
		//
	}

	public static BookFile getBookFile(EntityManager em, Integer bookFileId) {
		try {
			return em.createNamedQuery("BookFile.findById", BookFile.class)
					.setParameter("id", bookFileId).getSingleResult();
		} catch (Exception e) 
		{
			return null;
		}
	}


}