package model;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import digipages.exceptions.ServerException;

// find memberbook or book file, input BookUniId, memberBookId
public class MemberBookFinder {
	private static Logger logger = LoggerFactory.getLogger(MemberBookFinder.class);
	private MemberBook mb;
	private BookFile bf;

	private String bookUniId;

	public MemberBookFinder(String uniIdOrMemberBookId) {
		bookUniId = uniIdOrMemberBookId;
	}

	public void find(EntityManager postgres, Member member) throws ServerException {
		/**
		 * special case: only book_uni_id ==> trial/recommend book,
		 * or PartyReader (vnedor),
		 * need to loop inside member book, performance impact.
		 */
//		logger.debug("Eric Debug: bookUniId=" + bookUniId);

		if (member != null) {
//			logger.debug("Eric Debug: member=" + member.getId());
			// mb = member.getMemberBookByBUID(postgres, bookUniId);
			try {
				mb = (MemberBook) postgres.createNativeQuery
						("SELECT * FROM member_book WHERE book_uni_id = '" 
				+ bookUniId + "' AND member_id = " + member.getId() + 
				" order by udt::varchar::timestamp desc limit 1", MemberBook.class)
						.setHint("javax.persistence.cache.storeMode", "REFRESH")
						.getSingleResult();
			} catch (Exception ex)
			{
				// no result = exception, don't need to handle.
			}
		}

//		logger.debug("Eric Debug: memberBook=" + mb );
		if (mb == null) {
			if ("nologin-ThumbGenerator".equals(member.getBmemberId()))
				bf = BookFile.findLastVersionByBookUniIdThumbnail(postgres, bookUniId);
			else if (member.isPublisher() || member.isSuperReader() || member.isVendor())
				bf = BookFile.findLastVersionByBookUniIdPrivilaged(postgres, bookUniId);
			else
				bf = BookFile.findLastVersionByBookUniId(postgres, bookUniId);
//			logger.debug("Eric Debug: bookFile=" + bf );
		} else {
			bf = mb.getBookFile();
//			logger.debug("Eric Debug: memberBook=" + mb.getId() + " bookFile=" + bf);
		}
	}

	public MemberBook getMemberBook() {
		return mb;
	}

	public BookFile getBookFile() {
		return bf;
	}
}