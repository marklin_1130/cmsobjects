package model;

import java.util.List;

import com.google.gson.annotations.Expose;

public class ItemInfo {
    @Expose
    private String name;
    @Expose
    private String type;
    
    private DRMInfo drm_info;
    
    private List<String> eplanids;
    
    private String isbuyout;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

	public DRMInfo getDrm_info() {
		return drm_info;
	}

	public void setDrm_info(DRMInfo drm_info) {
		this.drm_info = drm_info;
	}

	public List<String> getEplanids() {
		return eplanids;
	}

	public void setEplanids(List<String> eplanids) {
		this.eplanids = eplanids;
	}

	public String getIsbuyout() {
		return isbuyout;
	}

	public void setIsbuyout(String isbuyout) {
		this.isbuyout = isbuyout;
	}

}
