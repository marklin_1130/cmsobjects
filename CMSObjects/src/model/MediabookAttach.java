package model;

import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import com.google.gson.annotations.Expose;

@Entity
@Cache(type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size = 4000, // Use 4,000 as the initial cache size.
        expiry = 3000, // 3sec
        coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send
                                                                            // invalidation messages.
)
@Table(name = "mediabook_attach")
@NamedQueries({
        @NamedQuery(name = "MediabookAttach.findByItemIdAndAttachfileNo", 
                    query = "SELECT ma FROM MediabookAttach ma" + " WHERE ma.itemId = :itemId" + " AND ma.attachfileNo = :attachfileNo"+ " AND ma.status = 9"),
        @NamedQuery(name = "MediabookAttach.findById", 
                    query = "SELECT ma FROM MediabookAttach ma" + " WHERE ma.id = :id"),
        @NamedQuery(name = "MediabookAttach.findByItemId", 
                    query = "SELECT ma FROM MediabookAttach ma" + " WHERE ma.itemId = :itemId AND ma.status = 9" ) })
public class MediabookAttach {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    @Expose
    private long id;

    @Mutable
    @Column(name = "item_id")
    @Expose
    private String itemId;

    @Mutable
    @Column(name = "cat")
    @Expose
    private String cat;

    @Mutable
    @Column(name = "version")
    @Expose
    private String version;

    @Mutable
    @Column(name = "attachfile_no")
    @Expose
    private Integer attachfileNo;

    @Mutable
    @Column(name = "attachfile_type")
    @Expose
    private String attachfileType;

    @Mutable
    @Column(name = "attachfile_name")
    @Expose
    private String attachfileName;

    @Mutable
    @Column(name = "attachfile_title")
    @Expose
    private String attachfileTitle;

    @Mutable
    @Column(name = "attach_info")
    @Expose
    @Convert(converter = digipages.pgconverter.PGAttachInfoConverter.class)
    private AttachInfo attachInfo;

    @Mutable
    @Column(name = "src_fname")
    @Expose
    private String srcFname;

    @Mutable
    @Column(name = "file_location")
    @Expose
    private String fileLocation;

    @Mutable
    @Column(name = "status")
    @Expose
    private int status;

    @Mutable
    @Column(name = "size")
    @Expose
    private int size;

    @Mutable
    @Column(name = "deleted")
    @Expose
    private boolean deleted;

    @Mutable
    @Column(name = "operator")
    @Expose
    private String operator;

    @Mutable
    @Column(name = "create_time")
    @Temporal(TemporalType.TIMESTAMP)
    @Expose
    private Date create_time;

    @Mutable
    @Column(name = "udt")
    @Temporal(TemporalType.TIMESTAMP)
    @Expose
    protected Date lastUpdated;

    @PrePersist
    @PreUpdate
    public void onPrePersist() {
        if (lastUpdated == null) {
            lastUpdated = new Date();
            setLastUpdated(lastUpdated);
        }
    }

    public Date getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(Date lastUpdateTime) {
        this.lastUpdated = lastUpdateTime;
    }

    public void setLastUpdated(String lastUpdateTime) {
        ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
        setLastUpdated(Date.from(zdt.toInstant()));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getAttachfileNo() {
        return attachfileNo;
    }

    public void setAttachfileNo(Integer attachfileNo) {
        this.attachfileNo = attachfileNo;
    }

    public String getAttachfileType() {
        return attachfileType;
    }

    public void setAttachfileType(String attachfileType) {
        this.attachfileType = attachfileType;
    }

    public String getAttachfileName() {
        return attachfileName;
    }

    public void setAttachfileName(String attachfileName) {
        this.attachfileName = attachfileName;
    }

    public String getAttachfileTitle() {
        return attachfileTitle;
    }

    public void setAttachfileTitle(String attachfileTitle) {
        this.attachfileTitle = attachfileTitle;
    }

    public AttachInfo getAttachInfo() {
        return attachInfo;
    }

    public void setAttachInfo(AttachInfo attachInfo) {
        this.attachInfo = attachInfo;
    }

    public String getSrcFname() {
        return srcFname;
    }

    public void setSrcFname(String srcFname) {
        this.srcFname = srcFname;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

}
