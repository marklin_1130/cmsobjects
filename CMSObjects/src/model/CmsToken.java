package model;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.boon.json.JsonFactory;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import digipages.common.DPLogger;
import digipages.exceptions.ServerException;
/**
 * The persistent class for the cms_token database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "cms_token")
@NamedQuery(name = "CmsToken.findAll", query = "SELECT c FROM CmsToken c")
public class CmsToken extends digipages.common.BasePojo implements Serializable {
	
	private static DPLogger logger = DPLogger.getLogger(CmsToken.class.getName());
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@Column(name = "client_ip")
	@Convert(converter = digipages.pgconverter.PGcidrConverter.class)
	private String clientIp;

	@Mutable
	@Column(name = "device_id")
	private Long deviceId;

	@Mutable
	@Column(name = "member_id")
	private Long memberId;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@Transient
	private String deviceStr; // devie.device_id

	public String getDeviceStr() {
		return deviceStr;
	}

	public void setDeviceStr(String deviceStr) {
		this.deviceStr = deviceStr;
	}

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public CmsToken() {

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClientIp() {
		return this.clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public Long getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Long getMemberId() {
		return this.memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	/**
	 * remove all cmsToken which is web based. (not in member_device)
	 * @param memberSession
	 * @throws ServerException 
	 */
	public static void clearWebTokens(EntityManager em, Member m) throws ServerException {
		// skip new member
		if (null == m || null == m.getId()) {
			return;
		}
		/**
		 * 
		 * select * from cms_token where member_id=? and device_id not in
		 * (select id from member_device where (os_type<>'web' and os_type
		 * <>'mobile_web') and member_id=?)
		 */
		@SuppressWarnings("unchecked")
		List<CmsToken> tokens = em.createNativeQuery(
				"select * from cms_token where member_id=? and device_id not in " + 
				"  (select id from member_device where (os_type<>'web' and os_type " + 
				"  <>'mobile_web' and os_type <>'mobile' ) and member_id=?)", CmsToken.class)
			.setParameter(1, m.getId())
			.setParameter(2, m.getId())
			.getResultList();

		for (CmsToken tmpToken : tokens) {
			em.remove(tmpToken);
		}
	}

	@SuppressWarnings("unchecked")
	public static List<CmsToken> listTokenByMember(EntityManager em, Long memberId) throws ServerException {
		List<CmsToken> ret = null;

		try {
			ret = (List<CmsToken>) 
				em.createNativeQuery(
					"select * from cms_token where member_id = ?", CmsToken.class)
					.setParameter(1, memberId)
					.getResultList();

		} catch (Exception ex) {
			throw new ServerException("id_err_999", ex.getMessage());
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}

		return ret;
	}
	
	static public CmsToken fromString(String string) throws ServerException
	{
		CmsToken ret = null;
		try {
				ret = decryptCmsToken(string);
		} catch (Exception ex)
		{
			logger.error("CmsToken Decrypt Fail:"+string,ex);;
			ServerException sex=new ServerException("id_err_203","CmsToken Decrypt Fail:" + ex.getMessage() + "String=" + string);
			throw sex;
		}
		
		return ret;
	}

	private static CmsToken decryptCmsToken(String srcStr) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		
		String salt = srcStr.substring(0,6);
		String Key = "*3:9.MumWx" + salt; //Key Size=56
		
		byte[] KeyData = Key.getBytes();
		SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");
		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(Cipher.DECRYPT_MODE, KS);
		String data = URLDecoder.decode(srcStr,"UTF-8").substring(6);
		byte [] jsonary= cipher.doFinal(Base64.getDecoder().decode(data));
		String jsonStr = new String(jsonary, "UTF-8");
		return JsonFactory.fromJson(jsonStr, CmsToken.class);
	}
	
	public String toEncStr() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
	{
		String jsonStr=this.toJsonString();
		
		String salt = UUID.randomUUID().toString().substring(0, 6);
		String Key = "*3:9.MumWx" + salt; //Key Size=56 (448)
		byte[] KeyData = Key.getBytes();
		SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");
		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(Cipher.ENCRYPT_MODE, KS);
		byte []enced=cipher.doFinal(jsonStr.getBytes("UTF-8"));
		String base64 = Base64.getEncoder().encodeToString(enced);
		return URLEncoder.encode(salt + base64, "UTF-8");
	}

}