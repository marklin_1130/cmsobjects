package model;

public class BookPreviewInfo extends BaseBookInfo {
	private String preview_type;
	private String preview_content;
	private String file_url;

	public String getPreview_type() {
		return preview_type;
	}

	public void setPreview_type(String preview_type) {
		this.preview_type = preview_type;
	}

	public String getPreview_content() {
		return preview_content;
	}

	public void setPreview_content(String preview_content) {
		this.preview_content = preview_content;
	}

	public String getFile_url() {
		return file_url;
	}

	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}
}