package model;

import java.util.List;

public class Grafinfo {
	String page;
	String color;
	String alpha;
	String pensize;

	List<GrafContent> contents;

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getAlpha() {
		return alpha;
	}

	public void setAlpha(String alpha) {
		this.alpha = alpha;
	}

	public String getPensize() {
		return pensize;
	}

	public void setPensize(String pensize) {
		this.pensize = pensize;
	}

	public List<GrafContent> getContents() {
		return contents;
	}

	public void setContents(List<GrafContent> contents) {
		this.contents = contents;
	}

	public void addContents(GrafContent content) {
		contents.add(content);
	}
}