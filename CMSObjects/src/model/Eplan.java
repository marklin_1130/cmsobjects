package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "eplan")
@NamedQueries({
    @NamedQuery(
            name = "Eplan.findByEplanid",
            query = "SELECT e FROM Eplan e"
            		+ " WHERE e.eplanid = :eplanid"
        )
})
public class Eplan {
	@Id
	@Column(name="eplanid")
	private String eplanid;
	
	@Mutable
    @Column(name = "c_title")
	private String c_title;
	
	@Mutable
    @Column(name = "seq_no")
	private Integer seq_no;
	
	@Mutable
    @Column(name = "delmark")
	private String delmark;
	
	@Mutable
    @Column(name = "eplan_info")
	private String eplan_info;

	public String getEplanid() {
		return eplanid;
	}

	public void setEplanid(String eplanid) {
		this.eplanid = eplanid;
	}

	public String getC_title() {
		return c_title;
	}

	public void setC_title(String c_title) {
		this.c_title = c_title;
	}

	public Integer getSeq_no() {
		return seq_no;
	}

	public void setSeq_no(Integer seq_no) {
		this.seq_no = seq_no;
	}

	public String getDelmark() {
		return delmark;
	}

	public void setDelmark(String delmark) {
		this.delmark = delmark;
	}

	public String getEplan_info() {
		return eplan_info;
	}

	public void setEplan_info(String eplan_info) {
		this.eplan_info = eplan_info;
	}


}
	