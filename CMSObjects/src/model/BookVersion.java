package model;

public class BookVersion {
	private String versionStr;
	int majorVersion = 1;
	int minorVersion = 1;

	// V001.0001
	public BookVersion(String versionStr) {
		this.versionStr = versionStr;

		if (versionStr.toLowerCase().startsWith("v")) {
			this.versionStr = versionStr.substring(1);
		}

		String split[] = this.versionStr.split("\\.");
		majorVersion = Integer.parseInt(split[0]);
		minorVersion = Integer.parseInt(split[1]);
	}

	public boolean newer(BookVersion that) {
		double thisVer = Double.parseDouble(versionStr);
		double thatVer = Double.parseDouble(that.versionStr);

		return thisVer > thatVer;
	}

	public boolean sameMajorVersion(BookVersion that) {
		return that.majorVersion == this.majorVersion;
	}

	public boolean isMarjorChange(BookVersion that) {
		return that.majorVersion != this.majorVersion;
	}

	public boolean isMinorChange(BookVersion that) {
		return that.minorVersion != this.minorVersion;
	}

	@Override
	public String toString() {
		String majorStr = String.valueOf(majorVersion);
		String minorStr = String.valueOf(minorVersion);
		return "V" + padding(3, "0", majorStr) + "." + padding(4, "0", minorStr);
	}

	public static String padding(int size, String padChr, String origStr) {
		String ret = origStr;

		while (ret.length() < size) {
			ret = padChr + ret;
		}

		return ret;
	}

	public BookVersion upgradeVersion(boolean majorVersionUpgrade) {
		if (majorVersionUpgrade) {
			majorVersion += 1;
			minorVersion = 1;
		} else {
			minorVersion += 1;
		}

		return this;
	}
}