package model;

import digipages.exceptions.ServerException;

public class BookUniId {
	private String itemId;
	private String format;
	private boolean isTrial;

	public BookUniId(String unidStr) throws ServerException{
		String x[] = unidStr.split("_");

		if (x.length != 3) {
			throw new ServerException("id_err_301","Wrong BookUniId format, expect \"E05123001_pdf_trial\", found " + unidStr);
		}

		itemId = x[0];
		format = x[1];
		isTrial = x[2].equals("trial");
	}

	public BookUniId() {

	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(itemId);
		sb.append("_");
		sb.append(format);
		sb.append("_");
		sb.append(isTrial ? "trial" : "normal");
		return sb.toString();
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public boolean isTrial() {
		return isTrial;
	}

	public void setTrial(boolean isTrial) {
		this.isTrial = isTrial;
	}
}