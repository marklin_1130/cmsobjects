package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;


@Entity
@Cache(
		  type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
		  size=4000,  // Use 4,000 as the initial cache size.
		  expiry=300000,  // 5min 
		  coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
		)
@Table(name="hothighlights")
@NamedQueries({
	@NamedQuery(name="Hothighlights.findBookFileIdAndCfihead",
	query="SELECT h FROM Hothighlights h WHERE h.cfiHead=:cfiHead AND h.bookFileId=:bookFileId"),

	@NamedQuery(name="Hothighlights.countByBookFileId",
	query="SELECT COUNT(h) FROM Hothighlights h WHERE h.bookFileId IN "
            + "(SELECT bf.id FROM BookFile bf WHERE bf.itemId=:itemId AND bf.format=:format AND bf.isTrial=:isTrial AND bf.status=9)"),
	
	@NamedQuery(name="Hothighlights.findByBookFileId",
	query="SELECT h FROM Hothighlights h WHERE h.bookFileId=:bookFileId ORDER BY h.count DESC "),
	
	@NamedQuery(name="Hothighlights.findByBookUniId",
	query="SELECT h FROM Hothighlights h WHERE h.bookFileId IN "
            + "(SELECT bf.id FROM BookFile bf WHERE bf.itemId=:itemId AND bf.format=:format AND bf.isTrial=:isTrial AND bf.status=9) "
            + "ORDER BY h.count DESC")
	
})

public class Hothighlights extends digipages.common.BasePojo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true,nullable=false)
	private long id;
	
	@Mutable
	@Column(name="cfi_head")
	private String cfiHead ;
	
	@Mutable
	@Column(name="count")
	private int count;
	
	@Mutable
	@Column(name="text")
	private String text ;
	
	@Mutable
	@Column(name="book_file_id")
	private int bookFileId ;
	
	@Mutable
	@Column(name="item_id")
	private String itemId ;
	
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;
	
	@PrePersist
	@PreUpdate
	public void onPrePersist(){
		if( lastUpdated==null ){
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}
	
	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCfiHead() {
		return cfiHead;
	}

	public void setCfiHead(String cfiHead) {
		this.cfiHead = cfiHead;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getBookFileId() {
		return bookFileId;
	}

	public void setBookFileId(int bookFileId) {
		this.bookFileId = bookFileId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
		
}
