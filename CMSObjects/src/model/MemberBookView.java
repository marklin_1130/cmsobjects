package model;

import java.io.Serializable;
import javax.persistence.*;

import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Struct;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the member_book_view database table.
 *
 */
@Entity
@Struct(name = "device_seq")
@Table(name = "member_book_view")
@NamedQueries({
	@NamedQuery(
		name = "MemberBookView.findAll",
		query = "SELECT mb FROM MemberBookView mb"
	)
})
public class MemberBookView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "action")
	private String action;

	@Column(name = "archived")
	private Boolean archived;

	@Column(name = "archived_date")
	private Timestamp archivedDate;

	@Column(name = "ask_update_version")
	private Boolean askUpdateVersion;

	@Column(name = "author")
	private String author;

	@Column(name = "bg_update_version")
	private Boolean bgUpdateVersion;

	@Column(name = "book_bookmark_status")
	private Boolean bookBookmarkStatus;

	@Column(name = "book_file_create_time")
	private Timestamp bookFileCreateTime;

	@Column(name = "book_file_id")
	private Integer bookFileId;

	@Column(name = "book_highlight_status")
	private Boolean bookHighlightStatus;

	@Column(name = "book_type")
	private String bookType;

	@Column(name = "book_uni_id")
	private String bookUniId;

	@Column(name = "c_title")
	private String cTitle;

	@Column(name = "childs")
	@Array(databaseType = "text")
	private List<String> childs = new ArrayList<String>();

	@Column(name = "create_operator")
	private String createOperator;

	@Column(name = "create_time")
	private Timestamp createTime;

	@Column(name = "cur_version")
	private String curVersion;

	@Column(name = "device_seq")
	@Array(databaseType = "int4")
	private List<Integer> deviceSeq = new ArrayList<Integer>();

	@Column(name = "download_time")
	private Timestamp downloadTime;

	@Column(name = "drm_log_id")
	private Long drmLogId;

	@Column(name = "drm_log_ids")
	@Array(databaseType = "int4")
	private List<Long> drmLogIds = new ArrayList<Long>();

	@Column(name = "encrypt_key")
	private byte[] encryptKey;

	@Column(name = "encrypt_type")
	private String encryptType;

	@Id
	private Long id;

	@Column(name = "info")
	private ItemInfo info;

	@Column(name = "is_hidden")
	private Boolean isHidden;

	@Column(name = "item_id")
	private String itemId;

	@Column(name = "item_type")
	private String itemType;

	@Column(name = "item_udt")
	private Timestamp itemUdt;

	@Column(name = "last_loc")
	private String lastLoc;

	@Column(name = "last_read_time")
	private Timestamp lastReadTime;

	@Column(name = "lock_status")
	private Integer lockStatus;

	@Column(name = "max_read_loc")
	private String maxReadLoc;

	@Column(name = "member_id")
	private Long memberId;

	@Column(name = "newer_version")
	private String newerVersion;

	@Column(name = "operator")
	private String operator;

	@Column(name = "passworded")
	private Boolean passworded;

	@Column(name = "percentage")
	private Integer percentage;

	@Column(name = "publisher_name")
	private String publisherName;

	@Column(name = "rank")
	private String rank;

	@Column(name = "read_time")
	private Timestamp readTime;

	@Column(name = "readlist_idnames")
	@Array(databaseType = "text")
	private List<String> readlistIdnames = new ArrayList<String>();

	@Column(name = "start_read_time")
	private Timestamp startReadTime;

	@Column(name = "udt")
	private Timestamp udt;

	@Column(name = "upgrade_time")
	private Timestamp upgradeTime;

	@Column(name = "vendor_name")
	private String vendorName;

	@Column(name = "version")
	private String version;

	@Column(name = "version_locked")
	private Boolean versionLocked;

	@Column(name = "is_book")
	private Boolean isBook;

	@Column(name = "is_magazine")
	private Boolean isMagazine;

	@Column(name = "is_trial")
	private Boolean isTrial;

	@Column(name = "deleted")
	private Boolean deleted;

	public MemberBookView() {

	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getArchived() {
		return this.archived;
	}

	public void setArchived(Boolean archived) {
		this.archived = archived;
	}

	public Timestamp getArchivedDate() {
		return this.archivedDate;
	}

	public void setArchivedDate(Timestamp archivedDate) {
		this.archivedDate = archivedDate;
	}

	public Boolean getAskUpdateVersion() {
		return this.askUpdateVersion;
	}

	public void setAskUpdateVersion(Boolean askUpdateVersion) {
		this.askUpdateVersion = askUpdateVersion;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Boolean getBgUpdateVersion() {
		return this.bgUpdateVersion;
	}

	public void setBgUpdateVersion(Boolean bgUpdateVersion) {
		this.bgUpdateVersion = bgUpdateVersion;
	}

	public Boolean getBookBookmarkStatus() {
		return this.bookBookmarkStatus;
	}

	public void setBookBookmarkStatus(Boolean bookBookmarkStatus) {
		this.bookBookmarkStatus = bookBookmarkStatus;
	}

	public Timestamp getBookFileCreateTime() {
		return this.bookFileCreateTime;
	}

	public void setBookFileCreateTime(Timestamp bookFileCreateTime) {
		this.bookFileCreateTime = bookFileCreateTime;
	}

	public Integer getBookFileId() {
		return this.bookFileId;
	}

	public void setBookFileId(Integer bookFileId) {
		this.bookFileId = bookFileId;
	}

	public Boolean getBookHighlightStatus() {
		return this.bookHighlightStatus;
	}

	public void setBookHighlightStatus(Boolean bookHighlightStatus) {
		this.bookHighlightStatus = bookHighlightStatus;
	}

	public String getBookType() {
		return this.bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public String getBookUniId() {
		return this.bookUniId;
	}

	public void setBookUniId(String bookUniId) {
		this.bookUniId = bookUniId;
	}

	public String getCTitle() {
		return this.cTitle;
	}

	public void setCTitle(String cTitle) {
		this.cTitle = cTitle;
	}

	public List<String> getChilds() {
		return this.childs;
	}

	public void setChilds(List<String> childs) {
		this.childs = childs;
	}

	public String getCreateOperator() {
		return this.createOperator;
	}

	public void setCreateOperator(String createOperator) {
		this.createOperator = createOperator;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getCurVersion() {
		return this.curVersion;
	}

	public void setCurVersion(String curVersion) {
		this.curVersion = curVersion;
	}

	public List<Integer> getDeviceSeq() {
		return this.deviceSeq;
	}

	public void setDeviceSeq(List<Integer> deviceSeq) {
		this.deviceSeq = deviceSeq;
	}

	public Timestamp getDownloadTime() {
		return this.downloadTime;
	}

	public void setDownloadTime(Timestamp downloadTime) {
		this.downloadTime = downloadTime;
	}

	public Long getDrmLogId() {
		return this.drmLogId;
	}

	public void setDrmLogId(Long drmLogId) {
		this.drmLogId = drmLogId;
	}

	public List<Long> getDrmLogIds() {
		return this.drmLogIds;
	}

	public void setDrmLogIds(List<Long> drmLogIds) {
		this.drmLogIds = drmLogIds;
	}

	public byte[] getEncryptKey() {
		return this.encryptKey;
	}

	public void setEncryptKey(byte[] encryptKey) {
		this.encryptKey = encryptKey;
	}

	public String getEncryptType() {
		return this.encryptType;
	}

	public void setEncryptType(String encryptType) {
		this.encryptType = encryptType;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ItemInfo getInfo() {
		return this.info;
	}

	public void setInfo(ItemInfo info) {
		this.info = info;
	}

	public Boolean getIsHidden() {
		return this.isHidden;
	}

	public void setIsHidden(Boolean isHidden) {
		this.isHidden = isHidden;
	}

	public String getItemId() {
		return this.itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemType() {
		return this.itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public Timestamp getItemUdt() {
		return this.itemUdt;
	}

	public void setItemUdt(Timestamp itemUdt) {
		this.itemUdt = itemUdt;
	}

	public String getLastLoc() {
		return this.lastLoc;
	}

	public void setLastLoc(String lastLoc) {
		this.lastLoc = lastLoc;
	}

	public Timestamp getLastReadTime() {
		return this.lastReadTime;
	}

	public void setLastReadTime(Timestamp lastReadTime) {
		this.lastReadTime = lastReadTime;
	}

	public Integer getLockStatus() {
		return this.lockStatus;
	}

	public void setLockStatus(Integer lockStatus) {
		this.lockStatus = lockStatus;
	}

	public String getMaxReadLoc() {
		return this.maxReadLoc;
	}

	public void setMaxReadLoc(String maxReadLoc) {
		this.maxReadLoc = maxReadLoc;
	}

	public Long getMemberId() {
		return this.memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getNewerVersion() {
		return this.newerVersion;
	}

	public void setNewerVersion(String newerVersion) {
		this.newerVersion = newerVersion;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Boolean getPassworded() {
		return this.passworded;
	}

	public void setPassworded(Boolean passworded) {
		this.passworded = passworded;
	}

	public Integer getPercentage() {
		return this.percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public String getPublisherName() {
		return this.publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getRank() {
		return this.rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public Timestamp getReadTime() {
		return this.readTime;
	}

	public void setReadTime(Timestamp readTime) {
		this.readTime = readTime;
	}

	public List<String> getReadlistIdnames(MemberBookView mb) {
		List<String> readlist_idnames = new ArrayList<String>();

		if (mb.getAction().equals("del") || (null != mb.getIsHidden() && mb.getIsHidden() == true)) {
			return readlist_idnames;
		} else {
			if ((null != mb.getIsTrial() && mb.getIsTrial() == true) ) {
				readlist_idnames.add("trial");
			} else if (null != mb.getArchived() && mb.getArchived() == true) {
				readlist_idnames.add("archive");
			} else if (null != mb.getPassworded() && mb.getPassworded() == true) {
				readlist_idnames.add("private");
			} else {
				if ((null != mb.getIsBook() && mb.getIsBook() == true) || "book".equals(mb.getItemType().toLowerCase())) {
					readlist_idnames.add("all");
					readlist_idnames.add("book");

					if (mb.getReadlistIdnames().size() > 0) {
						for (String readlistName : mb.getReadlistIdnames()) {
							if (readlistName.contains("custom")) {
								readlist_idnames.add(readlistName);
							}
						}
					}
				} else if ((null != mb.getIsMagazine() && mb.getIsMagazine() == true) || "magazine".equals(mb.getItemType().toLowerCase())) {
					readlist_idnames.add("all");
					readlist_idnames.add("magazine");

					if (mb.getReadlistIdnames().size() > 0) {
						for (String readlistName : mb.getReadlistIdnames()) {
							if (readlistName.contains("custom")) {
								readlist_idnames.add(readlistName);
							}
						}
					}
				}
			}
		}

		return readlist_idnames;
	}

	public List<String> getReadlistIdnames() {
		if (null == readlistIdnames)
			readlistIdnames = new ArrayList<String>();
		return this.readlistIdnames;
	}

	public void setReadlistIdnames(List<String> readlistIdnames) {
		this.readlistIdnames = readlistIdnames;
	}

	public Timestamp getStartReadTime() {
		return this.startReadTime;
	}

	public void setStartReadTime(Timestamp startReadTime) {
		this.startReadTime = startReadTime;
	}

	public Timestamp getUdt() {
		return this.udt;
	}

	public void setUdt(Timestamp udt) {
		this.udt = udt;
	}

	public Timestamp getUpgradeTime() {
		return this.upgradeTime;
	}

	public void setUpgradeTime(Timestamp upgradeTime) {
		this.upgradeTime = upgradeTime;
	}

	public String getVendorName() {
		return this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getVersionLocked() {
		return this.versionLocked;
	}

	public void setVersionLocked(Boolean versionLocked) {
		this.versionLocked = versionLocked;
	}

	public Boolean getIsBook() {
		return isBook;
	}

	public void setIsBook(Boolean isBook) {
		this.isBook = isBook;
	}

	public Boolean getIsMagazine() {
		return isMagazine;
	}

	public void setIsMagazine(Boolean isMagazine) {
		this.isMagazine = isMagazine;
	}

	public Boolean getIsTrial() {
		return isTrial;
	}

	public void setIsTrial(Boolean isTrial) {
		this.isTrial = isTrial;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
}