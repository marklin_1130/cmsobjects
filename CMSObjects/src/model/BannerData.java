package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Mutable;

@Entity
@Table(name = "banner_data")
public class BannerData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id",unique = true, nullable = false)
	private Integer id;
	
	@Mutable
    @Column(name = "pic_url")
	private String picUrl;
	
	@Mutable
    @Column(name = "start_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Mutable
    @Column(name = "expire_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expireDate;
	
	@Mutable
    @Column(name = "delmark")
	private String delmark;
	
	@Mutable
    @Column(name = "type")
	private String type;
	
	@Mutable
    @Column(name = "source_id")
	private String sourceId;
	
	@Mutable
    @Column(name = "seq_no")
	private Integer seqNo;
	
	@Mutable
	@Column(name = "banner_info")
	@Convert(converter = digipages.pgconverter.PGGenericJsonContentConverter.class)
	private Object bannerInfo;
	
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getDelmark() {
		return delmark;
	}

	public void setDelmark(String delmark) {
		this.delmark = delmark;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public Object getBannerInfo() {
		return bannerInfo;
	}

	public void setBannerInfo(Object bannerInfo) {
		this.bannerInfo = bannerInfo;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}
