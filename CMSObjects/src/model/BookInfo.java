package model;

public class BookInfo extends BaseBookInfo {
	private String isbn;
	private String author;
	private String o_author; // 原文作者
	private String translator;
	private String editor;
	private String illustrator;
	private String edition;
	private String foreword; // 序
	private String intro;
	private String display_file_size="";

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getO_author() {
		return o_author;
	}

	public void setO_author(String o_author) {
		this.o_author = o_author;
	}

	public String getTranslator() {
		return translator;
	}

	public void setTranslator(String translator) {
		this.translator = translator;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getIllustrator() {
		return illustrator;
	}

	public void setIllustrator(String illustrator) {
		this.illustrator = illustrator;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getForeword() {
		return foreword;
	}

	public void setForeword(String foreword) {
		this.foreword = foreword;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}
	

	public String getDisplay_file_size() {
		return display_file_size;
	}

	public void setDisplay_file_size(String display_file_size) {
		this.display_file_size = display_file_size;
	}
	
}