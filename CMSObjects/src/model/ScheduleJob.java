package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import com.google.gson.Gson;

import digipages.common.CommonUtil;
import digipages.common.DPLogger;

@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "schedule_job")
@NamedQueries({
	@NamedQuery(name = "ScheduleJob.findAll", query = "SELECT sj FROM ScheduleJob sj"),
	@NamedQuery(name = "ScheduleJob.listTimeoutRetryJobs", 
			query = "SELECT sj FROM ScheduleJob sj where " 
			+ " (sj.status=1 or  sj.status=2) " 
			+ " and  sj.shouldFinishTime < :now " 
			+ " and  sj.retryCnt <  :retryCnt "),
	@NamedQuery(name = "ScheduleJob.cleanOldJobs", 
			query = "DELETE FROM ScheduleJob sj where sj.lastUpdated < :monthsago "),
	@NamedQuery(name = "ScheduleJob.findByUUID", query = "SELECT sj FROM ScheduleJob sj WHERE sj.uuid = :uuid"),
	@NamedQuery(name = "ScheduleJob.listByJobNameAndStatusAndRetry", 
			query = "SELECT sj FROM ScheduleJob sj where " 
			+ " sj.jobName = :jobName " 
			+ " and  sj.status = :status " 
			+ " and  sj.retryCnt = :retryCnt ")

})

public class ScheduleJob extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;
	static Gson gson = new Gson();
	private static final int MAXRETRYCNT=3;
	private static DPLogger logger = DPLogger.getLogger(ScheduleJob.class.getName());

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@Column(name = "job_key")
	@Convert(converter = digipages.pgconverter.PGuuidConverter.class)
	private UUID uuid;

	@Mutable
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime = new Date();

	@JsonIgnore
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated=new Date();

	@Mutable
	@Column(name = "status")
	private Integer status=0; // default is waiting. (init)

	@Mutable
	@Column(name = "retry_cnt")
	private Integer retryCnt=0;

	@Mutable
	@Column(name = "job_name")
	private String jobName;

	@Mutable
	@Column(name = "job_param")
	@Convert(converter = digipages.pgconverter.PGGenericJsonContentConverter.class)
	private Object jobParam;

	@Mutable
	@Column(name = "result_str")
	private String resultStr;

	@Mutable
	@Column(name = "err_message")
	private String errMessage;

	@Mutable
	@Column(name = "priority")
	private Integer priority=500; // 0= high, 9999 = low

	/**
	 * real job start time
	 */
	@Mutable
	@Column(name = "start_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;

	@Mutable
	@Column(name = "should_start_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date shouldStartTime=new Date();

	@Mutable
	@Column(name = "job_runner")
	private Integer jobRunner;

	@Mutable
	@Column(name = "job_group")
	private String jobGroup;

	@Mutable
	@Column(name = "should_finish_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date shouldFinishTime;
	
	@Mutable
	@Column(name = "finish_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date finishTime;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setCreateTime(String createTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(createTime);
		setCreateTime(Date.from(zdt.toInstant()));
	}

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (createTime == null) {
			createTime = new Date();
			setCreateTime(createTime);
		}

		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getRetryCnt() {
		return retryCnt;
	}

	public void setRetryCnt(Integer retryCnt) {
		this.retryCnt = retryCnt;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * notice, if fail to from json, got 
	 * @param target class
	 * @return object of that class
	 */
	public <T> T getJobParam(Class<T> classOf) {
		String tmpJson=gson.toJson(jobParam);
		
		return gson.fromJson(tmpJson, classOf);
	}

	public void setJobParam(Object jobParam) {
		this.jobParam = jobParam;
	}

	public String getResultStr() {
		return resultStr;
	}

	public void setResultStr(String resultStr) {
		this.resultStr = resultStr;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setStartTime(String startTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(startTime);
		setCreateTime(Date.from(zdt.toInstant()));
	}

	public Integer getJobRunner() {
		return jobRunner;
	}

	public void setJobRunner(Integer jobRunner) {
		this.jobRunner = jobRunner;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	/**
	 * merge LastUpdate, ErrMessage, ResultStr, RetryCnt, Status
	 * @param tmpJob
	 * @return
	 */
	public ScheduleJob mergeScheduleJob(ScheduleJob tmpJob) {
		
		// for each Parameter, do replace if it is not null
		if (null!=tmpJob.getLastUpdated() && null!=this.getLastUpdated()){
			// tmpJob's update time is older ?! skip it. (with torlance 1000 ms)
			long timediff = this.getLastUpdated().getTime()-tmpJob.getLastUpdated().getTime();
			if (timediff>1000){
				logger.warn("skip merge, diff="+ timediff +"ms new: " + tmpJob.toGsonStr() + "\n indb:" + this.toGsonStr() );
				return this;
			}
			this.setLastUpdated(tmpJob.getLastUpdated());
		}else
		{
			// default is now.
			this.setLastUpdated(new Date());
		}
		
		// replace if only that has value.
		if (null!=tmpJob.getErrMessage() && tmpJob.getErrMessage().length()>0)
			this.setErrMessage(tmpJob.getErrMessage());
		if (null!=tmpJob.getResultStr() && tmpJob.getResultStr().length()>0)
			this.setResultStr(tmpJob.getResultStr());
		if (null!=tmpJob.getRetryCnt() && tmpJob.getRetryCnt()>0)
			this.setRetryCnt(tmpJob.getRetryCnt());
		if (null!=tmpJob.getStatus())
			this.setStatus(tmpJob.getStatus());
		if (null!=tmpJob.getFinishTime())
			this.setFinishTime(tmpJob.getFinishTime());
		if (null!=tmpJob.getStartTime())
			this.setStartTime(tmpJob.getStartTime());
		if (null!=tmpJob.getShouldFinishTime())
			this.setShouldFinishTime(tmpJob.getShouldFinishTime());
		if (null!=tmpJob.getShouldStartTime())
			this.setShouldStartTime(tmpJob.getShouldStartTime());
		if (null!=tmpJob.getJobRunner())
			this.setJobRunner(tmpJob.getJobRunner());
		if (null!=tmpJob.getJobGroup())
			this.setJobGroup(tmpJob.getJobGroup());
		if (null!=tmpJob.getUuid() && null==this.getUuid())
			this.setUuid(tmpJob.getUuid());
		
		
		return this;
	}

	public Date getShouldFinishTime() {
		return shouldFinishTime;
	}

	public void setShouldFinishTime(Date shouldEndTime) {
		this.shouldFinishTime = shouldEndTime;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date endTime) {
		this.finishTime = endTime;
	}

	public Date getShouldStartTime() {
		return shouldStartTime;
	}

	public void setShouldStartTime(Date shouldStartTime) {
		this.shouldStartTime = shouldStartTime;
	}

	public static List<ScheduleJob> listTimeoutRetryJobs(EntityManager postgres) {
		
		return postgres.createNamedQuery("ScheduleJob.listTimeoutRetryJobs", 
				ScheduleJob.class)
				.setParameter("now", new Date())
				.setParameter("retryCnt", MAXRETRYCNT).getResultList();
	}

	public static void cleanOldJobs(EntityManager postgres) {
		
		try {
			
			postgres.getTransaction().begin();
			postgres.createNamedQuery("ScheduleJob.cleanOldJobs")
			.setParameter("monthsago", CommonUtil.nextNMinutes(-(60*24*30))) // 30 days
			.executeUpdate();
			postgres.getTransaction().commit();
		}finally{
			if (postgres.getTransaction().isActive())
				postgres.getTransaction().rollback();
		}
	}

	
}