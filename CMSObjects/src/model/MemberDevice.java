package model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.sessions.CopyGroup;


/**
 * The persistent class for the member_device database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
)
@Table(name="member_device")
@NamedQueries({
	@NamedQuery(name = "MemberDevice.findAll", query = "SELECT m FROM MemberDevice m"),
	@NamedQuery(name = "MemberDevice.findByToken", query = "SELECT m FROM MemberDevice m WHERE m.deviceToken IS NOT NULL"),
	@NamedQuery(name = "MemberDevice.findByAndroidAndToken", query = "SELECT m FROM MemberDevice m WHERE m.osType = 'android' AND m.deviceToken IS NOT NULL"),
	@NamedQuery(name = "MemberDevice.findByIosAndToken", query = "SELECT m FROM MemberDevice m WHERE m.osType = 'ios' AND m.deviceToken IS NOT NULL"),
	@NamedQuery(name = "MemberDevice.findByDeviceToken", query = "SELECT m FROM MemberDevice m WHERE m.deviceToken = :deviceToken"),
	@NamedQuery(name = "MemberDevice.findByDeviceId", query = "SELECT m FROM MemberDevice m WHERE m.deviceId = :deviceId"),
	@NamedQuery(name = "MemberDevice.findByMemberId", query = "SELECT m FROM MemberDevice m WHERE m.member.id = :member_id and m.status>0"),
	@NamedQuery(name = "MemberDevice.findThirdPartyDevice", query = 
			"SELECT m FROM MemberDevice m "
			+ "WHERE m.member.id = :memberId "
			+ "and m.deviceId = :deviceId "),
})
public class MemberDevice extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int STATUSDELETED = -9;
	public static final int STATUSBANNED = -1;
	public static final int STATUSLOGOUT = 0;
	public static final int STATUSGUEST = 1;
	public static final int STATUSLOGIN = 2;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Mutable
	@Column(name="device_id")
	private String deviceId;

	@Mutable
	@Column(name="device_model")
	private String deviceModel;

	@Mutable
	@Column(name="device_vendor")
	private String deviceVendor;

	@Mutable
	@Column(name="language")
	private String language;

	@Mutable
	@Column(name="last_online_ip")
	@Convert(converter = digipages.pgconverter.PGcidrConverter.class)
	private String lastOnlineIp;

	@Mutable
	@Column(name="last_online_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastOnlineTime;

	@Mutable
	@Column(name="os_type")
	private String osType;

	@Mutable
	@Column(name="os_version")
	private String osVersion;

	@Mutable
	@Column(name="screen_dpi")
	private String screenDpi;

	@Mutable
	@Column(name="screen_resolution")
	private String screenResolution;

	@Mutable
	@Column(name="device_token")
	private String deviceToken;

	@Mutable
	@Column(name="register_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerTime=new Date();
	
	@Mutable
	@Column(name="device_name")
	private String deviceName;

	@Mutable
	@Column(name="status")
	private int status;
	
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;
	
	
	// target: detect by dpi/resolution.
	@Transient
	private String deviceType="pad";

	//bi-directional many-to-one association to Member
	@Mutable
	@JsonIgnore
	@JoinColumn(name="member_id")
	@ManyToOne
	private Member member;

	
	

	@PrePersist
	@PreUpdate
	public void onPrePersist() {

		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}
	
	

	public MemberDevice() {
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	public String getDeviceModel() {
		return this.deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}


	public String getDeviceVendor() {
		return this.deviceVendor;
	}

	public void setDeviceVendor(String deviceVendor) {
		this.deviceVendor = deviceVendor;
	}


	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}


	public String getLastOnlineIp() {
		return this.lastOnlineIp;
	}

	public void setLastOnlineIp(String lastOnlineIp) {
		if (null==lastOnlineIp)
			return;
		if (!lastOnlineIp.equals(this.lastOnlineIp))
			setLastUpdateTime(LocalDateTime.now());
		this.lastOnlineIp = lastOnlineIp;
	}


	public Date getLastOnlineTime() {
		return this.lastOnlineTime;
	}

	public void setLastOnlineTime(Date lastOnlineTime) {
		this.lastOnlineTime = lastOnlineTime;
	}


	public String getOsType() {
		return this.osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}


	public String getOsVersion() {
		return this.osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}


	public String getScreenDpi() {
		return this.screenDpi;
	}

	public void setScreenDpi(String screenDpi) {
		this.screenDpi = screenDpi;
	}


	public String getScreenResolution() {
		return this.screenResolution;
	}

	public void setScreenResolution(String screenResolution) {
		this.screenResolution = screenResolution;
	}



	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	// converter
	public void setLastOnlineTime(LocalDateTime now) {
		java.sql.Timestamp ts=java.sql.Timestamp.valueOf(now);
		setLastOnlineTime(ts);
	}


	public void setLastUpdateTime(LocalDateTime now) {
		java.sql.Timestamp ts=java.sql.Timestamp.valueOf(now);
		setLastUpdated(ts);
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		if (deviceToken!=null)
		if (!deviceToken.equals(this.deviceToken)){
			setLastUpdateTime(LocalDateTime.now());
		}
		this.deviceToken = deviceToken;
	}
	
	public MemberDevice clone(EntityManager em)
	{
		CopyGroup group = new CopyGroup();
		group.setShouldResetPrimaryKey( true );
		MemberDevice copy = (MemberDevice)em.unwrap( JpaEntityManager.class ).copy( this, group );
		return copy;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(LocalDateTime now) {
		java.sql.Timestamp ts=java.sql.Timestamp.valueOf(now);
		setRegisterTime(ts);
	}
	
	public void setRegisterTime(Date time)
	{
		this.registerTime=time;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		
		if (!deviceName.equals(this.getDeviceName()))
			setLastUpdateTime(LocalDateTime.now());
		this.deviceName = deviceName;
	}

	public boolean isWeb() {
		if (getOsType().toLowerCase().endsWith("web"))
			return true;
		return false;
	}

	// similar with cmsToken
	public static void clearWebDevices(EntityManager em, Member member) {
		List<MemberDevice> delList = new ArrayList<>();
		for (MemberDevice tmpDev:member.getMemberDevices())
		{
			if (tmpDev.getDeviceModel().toLowerCase().contains("web")||
					tmpDev.getOsType().toLowerCase().contains("web"))
			{
				delList.add(tmpDev);
			}
		}
		
		for (MemberDevice tmpDevice:delList)
		{
			member.removeMemberDevice(tmpDevice);
			em.remove(tmpDevice);
		}
		
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


}