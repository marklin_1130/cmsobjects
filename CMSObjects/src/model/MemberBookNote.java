package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import digipages.common.DPLogger;

/**
 * The persistent class for the member_book_note database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
)
@Table(name="member_book_note")

@NamedQueries({
	@NamedQuery(name="MemberBookNote.findAll", query="SELECT m FROM MemberBookNote m"),
	
	@NamedQuery(name="MemberBookNote.countAllFromMemberId",
	query="SELECT COUNT(m) FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			) ,
			
	@NamedQuery(name="MemberBookNote.countExcludeActionFromMemberId",
	query="SELECT COUNT(m) FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type "
			+" AND m.action!='del' "
			+ "AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			) ,
			
	@NamedQuery(name="MemberBookNote.findAllFromMemberIdASC",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			+ " ORDER BY m.lastUpdated ASC ") ,
			
	@NamedQuery(name="MemberBookNote.findExcludeActionFromMemberIdASC",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type "
			+ " AND m.action!='del' "
			+ " AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			+ " ORDER BY m.lastUpdated ASC ") ,
			
	@NamedQuery(name="MemberBookNote.findAllFromMemberIdDESC",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			+ " ORDER BY m.lastUpdated DESC ") ,	
			
	@NamedQuery(name="MemberBookNote.findExcludeActionFromMemberIdDESC",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type "
			+ " AND m.action!='del' "
			+ " AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			+ " ORDER BY m.lastUpdated DESC ") ,
	
	@NamedQuery(name="MemberBookNote.countByMmberBookIDandMemberId",
	query="SELECT COUNT(m) FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated "
			+ "AND m.type=:type AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId)"
			) ,
			
	@NamedQuery(name="MemberBookNote.countByMmberBookIDandMemberIdExcludeAction",
	query="SELECT COUNT(m) FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated "
			+ "AND m.type=:type "
			+ " AND m.action!='del' "
			+ " AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId)"
			) ,		
			
	@NamedQuery(name="MemberBookNote.findByMmberBookIDandMemberIdASC",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated "
			+ "AND m.type=:type AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId)"
			+ " ORDER BY m.lastUpdated ASC"),
	
	@NamedQuery(name="MemberBookNote.findByMmberBookIDandMemberIdDESC",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated "
			+ "AND m.type=:type AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId)"
			+ " ORDER BY m.lastUpdated DESC"),

	@NamedQuery(name="MemberBookNote.findHighLightByMmberBookIDandMemberId",
	query="SELECT m FROM MemberBookNote m WHERE m.type=:type "
			+ "AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId)"),

	@NamedQuery(name="MemberBookNote.findBookMarkByMmberBookIDandMemberId",
	query="SELECT m FROM MemberBookNote m WHERE m.type=:type "
			+ "AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId)"),

	@NamedQuery(name="MemberBookNote.findbyUUIDTypeBookFileId" ,
	query="SELECT m FROM MemberBookNote m "
			+ " WHERE m.uuid=:uuid  AND m.type=:type "),

	@NamedQuery(name="MemberBookNote.findbyUUID" ,
	query="SELECT m FROM MemberBookNote m "
			+ " WHERE m.uuid=:uuid"),
	
	@NamedQuery(name="MemberBookNote.findHotBookFeedBackByLikeCnt" ,
	query="SELECT m FROM MemberBookNote m "
			+ " WHERE m.type='feedback' AND m.memberBookId "
			+ " IN (SELECT mb.id FROM MemberBook mb WHERE mb.bookFileId=:bookFileId )"),
			
	@NamedQuery(name="MemberBookNote.sortByHotBookFeedBackLikeCnt" ,
	query="SELECT COUNT(m) FROM MemberBookNote m "
			+ " WHERE m.type='feedback' AND m.memberBookId "
			+ " IN (SELECT mb.id FROM MemberBook mb WHERE mb.bookFileId=:bookFileId )"),
	
	@NamedQuery(name="MemberBookNote.findByBookFile" ,
	query="SELECT m FROM MemberBookNote m "
			+ " WHERE m.type='highlight' AND m.memberBookId "
			+ " IN (SELECT mb.id FROM MemberBook mb WHERE mb.bookFileId=:bookFileId )") ,
			
	@NamedQuery(name="MemberBookNote.findByMemberBookId" ,
	query="SELECT m FROM MemberBookNote m "
			+ " WHERE m.type=:type AND m.memberBookId=:memberBookId ") ,
			
	@NamedQuery(name="MemberBookNote.OrderByNamefindAllFromMemberId",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			+ " ORDER BY m.name") ,
			
	@NamedQuery(name="MemberBookNote.OrderByNameExcludeActionfindFromMemberId",
	query="SELECT m FROM MemberBookNote m WHERE m.lastUpdated>:lastUpdated"
			+ " AND m.type=:type "
			+ " AND m.action!='del' "
			+ " AND m.memberBookId IN "
			+ "(SELECT mb.id FROM MemberBook mb WHERE mb.memberId =:memberId)"
			+ " ORDER BY m.name") ,		
			
	@NamedQuery(name="MemberBookNote.findAllExcludeActionDelByBookFile" ,
	query="SELECT m FROM MemberBookNote m "
			+ " WHERE m.action!='del' AND m.memberBookId "
			+ " IN (SELECT mb.id FROM MemberBook mb "
			+ "WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId )") ,

	@NamedQuery(name="MemberBookNote.countfindAllExcludeActionDelByBookFile" ,
	query="SELECT count(m) FROM MemberBookNote m "
			+ " WHERE m.action!='del' AND m.memberBookId "
			+ " IN (SELECT mb.id FROM MemberBook mb "
			+ "WHERE mb.memberId =:memberId AND mb.bookFileId=:bookFileId )") ,
	
	@NamedQuery(name="MemberBookNote.findMemberBookNotebyBookUniId" ,
	query="SELECT m FROM MemberBookNote m WHERE m.memberBookId IN "
					+"(SELECT mb.bookFileId FROM MemberBook mb WHERE mb.bookUniId =:bookuniid ) "
					+ " AND m.type=:type "
					+ " AND lower(m.name) LIKE :searchKeyword "
					+ " AND m.action != 'del'"
					+ " AND m.color =:color"
					+ " ORDER BY :sortOrder") ,
	
	@NamedQuery(name="MemberBookNote.findMemberBookNotebyAll" ,
	query="SELECT m FROM MemberBookNote m WHERE m.memberBookId =:bookid "
					+ " AND m.type=:type "
					+ " AND lower(m.name) LIKE :searchKeyword "
					+ " AND m.action != 'del'"
					+ " ORDER BY :sortOrder")
			
})

public class MemberBookNote extends digipages.common.BasePojo implements Serializable {
	
	private static DPLogger logger =DPLogger.getLogger(MemberBookNote.class.getName());
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Mutable
	@Column(name="is_public")
	private Boolean isPublic=true;

	@Mutable
	@Column(name="like_cnt")
	private Long likeCnt=0L;
	
	@Mutable
	@Column(name="type")
	private String type;
	
	@Mutable
	@Column(name="version")
	private String version;

	@Mutable
	@Column(name="note_content")
	@Convert(converter = digipages.pgconverter.PGNoteContentConverter.class)
	private NoteContent noteContent;

	@Mutable
	@Column (name="uuid")
	@Convert(converter = digipages.pgconverter.PGuuidConverter.class)
	private UUID uuid;
	
	@Mutable
	@Column(name="color")
	private String color;
	
	@Mutable
	@Column(name="spine")
	private Double spine=0.0D;
	
	@Mutable
	@Column(name="name")
	private String name;
	
	@Mutable
	@Column(name="note")
	private String note;
	
	@Mutable
	@Column(name="highlight_text")
	private String highlightText;
	
	@Mutable
	@Column(name="text")
	private String text;
	
	@Mutable
	@Column(name="action")
	private String action="add";
	
	@Mutable
	@Column(name="spine_order")
	private Double spineOrder=0.0D;
	
	@Mutable
	@Column(name="member_book_id", insertable=false, updatable=false)
	private Integer memberBookId;
	
	//bi-directional many-to-one association to MemberBook
	@Mutable
	@JoinColumn(name="member_book_id")
	@ManyToOne
	@JsonIgnore
	private MemberBook memberBook;
	
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {

		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}
	
	public MemberBookNote() {
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Boolean getIsPublic() {
		return this.isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}


	public Long getLikeCnt() {
		return this.likeCnt;
	}

	public void setLikeCnt(Long likeCnt) {
		this.likeCnt = likeCnt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public NoteContent getNoteContent() {
		return this.noteContent;
	}

	public void setNoteContent(NoteContent noteContent) {
		this.noteContent = noteContent;
	}


	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Double getSpine() {
		return spine;
	}

	public void setSpine(Double spine) {
		this.spine = spine;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getHighlightText() {
		return highlightText;
	}

	public void setHighlightText(String highlightText) {
		this.highlightText = highlightText;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	

//
//	public Integer getMemberBookId() {
//		return memberBookId;
//	}
//
//	public void setMemberBookId(Integer memberBookId) {
//		this.memberBookId = memberBookId;
//	}

	public MemberBook getMemberBook() {
		return this.memberBook;
	}

	public Double getSpineOrder() {
		return spineOrder;
	}

	public void setSpineOrder(Double spineOrder) {
		this.spineOrder = spineOrder;
	}

	public void setMemberBook(MemberBook memberBook) {
		this.memberBook = memberBook;
	}
	
	public static MemberBookNote getNoteByUUid(EntityManager em, String uuid)
	{
		MemberBookNote memberBookNotes= 
				em.createNamedQuery("MemberBookNote.findbyUUID"
				,MemberBookNote.class)
				.setParameter("uuid", java.util.UUID.fromString(uuid))
				.getSingleResult();
		return memberBookNotes;
	}
	
	public static List<MemberBookNote> sort(List<MemberBookNote> src)
	{
		List <MemberBookNote> x2 = new ArrayList<>();
		x2.addAll(src);
		try{
			Collections.sort(x2,new Comparator<MemberBookNote>() {
	            public int compare(MemberBookNote o1, MemberBookNote o2) {
	            	
	            	if (o1==null && o1==o2)
	            	{
	            		return 0;
	            	}
	            	if (o1==null)
	            		return -1;
	            	if (o2==null)
	            		return 1;
	            	
	            	String thisCfi=o1.getNoteContent().getCfi();
	            	String thatCfi=o2.getNoteContent().getCfi();

//	            	return (CFILocation.isBigger(thisCfi, thatCfi) ? 1 :-1);
	            	if( ("epubcfi()".equals(thisCfi)  && "epubcfi()".equals(thatCfi)) ||
	            			("".equals(thisCfi) && "".equals(thatCfi)) ||
	            			( null==thatCfi &&  null==thisCfi )  || 
	            			( "undefined".equals(thatCfi) && "undefined".equals(thisCfi))    ){
	            		return 0 ;
	            	}
	            	
	            	
	            	return CFILocation.isBigger(thisCfi, thatCfi);
	            }
	        });
		}catch( Exception ex){
			logger.error(ex);;
		}
		
		return x2;
	}
	
	public static double findCFINum(String cfiStr) {
		if (cfiStr.contains("cfi")) {
			return findCFINumOld(cfiStr);
		}

		double total = 0;

		try {
			// new cfi ( chapter/curPage/CurChapterTotalPages)
			String[] tmpCFI = cfiStr.split("/");

			if (tmpCFI.length > 1) {
				total = Double.valueOf(tmpCFI[0]) + (Double.valueOf(tmpCFI[1]) / Double.valueOf(tmpCFI[2]));
			}

		} catch (Exception ex) {
			logger.error(ex);
		}

		return total;
	}

	public static double findCFINumOld(String cfiStr) {

        if (cfiStr.contains("undefined") || "epubcfi()".equals(cfiStr) || null == cfiStr || "".equals(cfiStr) || !cfiStr.contains("epubcfi(")) {
            return 0;
        }
        cfiStr = cfiStr.replace("epubcfi(", "");
        cfiStr = cfiStr.replace(")", "");
        String[] tmpCFI = cfiStr.split("!");
        double total = 0;

        if (tmpCFI.length > 1) {
        	
        	//新格式轉為舊格式處理
        	String[] tmpCFI1 = tmpCFI[1].split(",");
        	if (tmpCFI1.length > 2) {
        		String[] tmpCFI1_n = new String[2];
        		tmpCFI1_n[0] = new StringBuilder(tmpCFI1[0].trim()).append(tmpCFI1[1].trim()).toString();
        		tmpCFI1_n[1] = new StringBuilder(tmpCFI1[0].trim()).append(tmpCFI1[2].trim()).toString();
        		tmpCFI[1] = String.join(",", tmpCFI1_n);
        	}
        	
            String[] cfihead = tmpCFI[0].split("/");

            for (int i = 0; i < cfihead.length; i++) {

                if ("".equals(cfihead[i])) {
                    continue;
                }
                String[] nodes = cfihead[i].split("\\[");
                if (nodes.length > 1) {
                    cfihead[i] = nodes[0];
                }

                double cfiLong = Long.valueOf(cfihead[i]);

                cfiLong = Math.pow(cfiLong * 1000, (double) 1 / 3);
                cfiLong = (cfiLong * 1000000) / Math.pow(10, i);
                total += cfiLong;
            }

            String[] tmpCfiTail = tmpCFI[1].split(":");
            String[] cfitail = tmpCfiTail[0].split("/");

            for (int j = 0; j < cfitail.length; j++) {
                if ("".equals(cfitail[j])) {
                    continue;
                }

                String[] nodes = cfitail[j].split("\\[");
                if (nodes.length > 1) {
                    cfitail[j] = nodes[0];
                }

                double cfiLong = Long.valueOf(cfitail[j]);

                cfiLong = Math.pow(cfiLong, (double) 1 / 3);
                cfiLong = (cfiLong * 10000) / Math.pow(10, j);
                total += cfiLong;
            }

			if (tmpCfiTail.length > 1) {
				String tmpCfiTailByte = tmpCfiTail[1].split(",")[0];

				if (!"".equals(tmpCfiTailByte)) {
					double cfiLong = Long.valueOf(tmpCfiTailByte);

					cfiLong = Math.pow(cfiLong / 1000, (double) 1 / 3);
					cfiLong = (cfiLong * 10);
					total += cfiLong;
				}
			}
        }

        return total;
    }

	/**
	 * 
	 * @param newNote (not inserted yet)
	 * @param origNotes (same member, same book)
	 * @return spine location of new Note
	 */
	public static double calcSpineOrder(MemberBookNote newNote, List<MemberBookNote> origNotes)
	{
		double ret =-9999;
		if (origNotes.size()==0)
		{
			return 0.5;
		}

		List <MemberBookNote> x2 = new ArrayList<>();
		x2.addAll(origNotes);
		Collections.sort(x2,new Comparator<MemberBookNote>() {
            public int compare(MemberBookNote o1, MemberBookNote o2) {
            	
            	String thisCfi=o1.getNoteContent().getCfi();
            	String thatCfi=o2.getNoteContent().getCfi();

            	return ( CFILocation.isBigger(thisCfi, thatCfi) );
            }
        });
		
		int loc = findMaxIndex(newNote, x2);

		ret = calcSpineOrder(loc, x2);
		
		return ret;
	}
	
	public static List <MemberBookNote> reIndexSpineOrder(List<MemberBookNote> noteList)
	{
	
		List <MemberBookNote> tmpList = sort(noteList);

		double interVal = 0.99/tmpList.size();
		double curVal = 0;
		String tmpCFI = "";
		for (MemberBookNote tmpNote:tmpList)
		{

			if( null==tmpNote.getNoteContent().getCfi() || 
					"epubcfi()".equals( tmpNote.getNoteContent().getCfi() )  ){
				continue ;
			}

			if( !tmpCFI.equals(tmpNote.getNoteContent().getCfi())  ){
				curVal +=interVal;	
			}
			tmpNote.setSpineOrder(curVal);
			tmpCFI = tmpNote.getNoteContent().getCfi() ;
		}
		
		return tmpList ;
	}

	private static Double calcSpineOrder(int loc, List<MemberBookNote> origNotes) {
		double below =0;
		double above =1;
		double ret=0;

		try {
			if (origNotes.size()==1)
			{
				if (loc==0)
				{
					above = origNotes.get(0).getSpineOrder();
				}else
				{
					below = origNotes.get(0).getSpineOrder();
				}

			}else
			{

				if( (loc-1)==origNotes.size() ){
					below = origNotes.get(origNotes.size()-1).getSpineOrder() ;
				}else if( loc==0 ){
					above = origNotes.get(loc+1).getSpineOrder() ;
				}else{
					below = origNotes.get(loc-1).getSpineOrder();
					above = origNotes.get(loc).getSpineOrder();
				}
			}
		}
		catch (Exception ex)
		{
		}

		ret = (below + above)/2;
		
		return ret;
	}

	/**
	 * 
	 * @param newNote
	 * @param origNotes
	 * @return location in array (), smallest =0, biggest = n+1
	 */
	private static int findMaxIndex(MemberBookNote newNote, List<MemberBookNote> origNotes) {
		
		String newNoteCfi=newNote.getNoteContent().getCfi();
		int i=0;
		boolean found=false;
		// can be improved by binary search.
		for (; i< origNotes.size();i++)
		{
			String tmpCfiLoc= origNotes.get(i).getNoteContent().getCfi();

//			if (!CFILocation.isBigger(newNoteCfi, tmpCfiLoc)){
			if (CFILocation.isBigger(newNoteCfi, tmpCfiLoc)<0){
				found=true;
				break;
			}
		}
		if (!found)
			i++;
		return i;
	}

}
