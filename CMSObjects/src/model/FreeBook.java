package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

@Entity
@Cache(
    type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
    size = 4000, // Use 4,000 as the initial cache size.
    expiry = 300000, // 5min
    coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "free_books")
@NamedQueries({
    @NamedQuery(
        name = "FreeBook.findAll",
        query = "SELECT fb FROM FreeBook fb"
                + " WHERE fb.startTime <= :nowTime"
                + " AND fb.endTime > :nowTime"
                + " AND fb.free_type = 'NORMAL' "
                + " AND fb.bookFile in (select bf from BookFile bf where bf.status=9)"
                + " ORDER BY fb.startTime,fb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "FreeBook.findByItemId",
        query = "SELECT fb FROM FreeBook fb"
                + " WHERE fb.itemId = :itemId"
                + " AND fb.free_type = 'NORMAL' "
    ),
    @NamedQuery(
            name = "FreeBook.findAvailableByItemId",
            query = "SELECT fb FROM FreeBook fb"
                    + " WHERE fb.itemId = :itemId"
                    + " AND fb.status = 1"
                    + " AND fb.startTime <= :nowTime"
                    + " AND fb.endTime >= :nowTime"
                    + " AND fb.free_type = 'NORMAL' "
                    + " ORDER BY fb.sequence ,fb.lastUpdated DESC"
        ),
    @NamedQuery(
        name = "FreeBook.findByItemIdAndSequence",
        query = "SELECT fb FROM FreeBook fb"
                + " WHERE fb.itemId = :itemId"
                + " AND fb.sequence = :sequence"
                + " AND fb.free_type = 'NORMAL' "
    ),
    @NamedQuery(
            name = "FreeBook.findByStatusOrderUpdateTime",
            query = "SELECT fb FROM FreeBook fb"
                    + " WHERE fb.status = 1"
                    + " AND fb.startTime <= :nowTime"
                    + " AND fb.endTime >= :nowTime"
                    + " AND fb.free_type = 'NORMAL' "
                    + " ORDER BY fb.sequence ,fb.lastUpdated DESC , substring(fb.itemId,0,4) ,fb.itemId DESC"
        ),
    @NamedQuery(
            name = "FreeBook.findByStatusOrderUpdateTimeByCat",
            query = "SELECT fb FROM FreeBook fb"
                    + " WHERE fb.status = 1"
                    + " AND fb.startTime <= :nowTime"
                    + " AND fb.endTime >= :nowTime"
                    + " AND fb.free_type = 'NORMAL' "
                    + " AND fb.itemId like :cat "
                    + " ORDER BY fb.sequence ,fb.lastUpdated DESC,fb.itemId DESC"
        ),
    @NamedQuery(
            name = "FreeBook.findByStatusOrderUpdateTimePage",
            query = "SELECT fb FROM FreeBook fb"
                    + " WHERE fb.status = 1"
                    + " AND fb.startTime <= :nowTime"
                    + " AND fb.endTime >= :nowTime"
                    + " AND fb.free_type = 'NORMAL' "
                    + " ORDER BY fb.sequence ,fb.lastUpdated DESC,substring(fb.itemId,0,4) ,fb.itemId DESC  "
        ),
    @NamedQuery(
            name = "FreeBook.findByStatusOrderUpdateTimePageByCat",
            query = "SELECT fb FROM FreeBook fb"
                    + " WHERE fb.status = 1"
                    + " AND fb.startTime <= :nowTime"
                    + " AND fb.endTime >= :nowTime"
                    + " AND fb.free_type = 'NORMAL' "
                    + " AND fb.itemId like :cat "
                    + " ORDER BY fb.sequence ,fb.lastUpdated DESC,fb.itemId DESC"
        ),
    @NamedQuery(
        name = "FreeBook.findAllByItemIds",
        query = "SELECT fb FROM FreeBook fb"
                + " WHERE fb.itemId IN :itemIds"
                + " AND fb.startTime <= :nowTime"
                + " AND fb.endTime > :nowTime"
                + " AND fb.free_type = 'NORMAL' "
                + " ORDER BY fb.startTime,fb.lastUpdated DESC"
    ),
    @NamedQuery(
        name = "FreeBook.findAllByTime",
        query = "SELECT fb FROM FreeBook fb"
                + " WHERE fb.startTime <= :nowTime"
                + " AND fb.endTime > :nowTime"
                + " AND fb.free_type = 'NORMAL' "
                + " ORDER BY fb.startTime,fb.lastUpdated DESC"
    ),
    @NamedQuery(
            name = "FreeBook.findByItemIdAndCorrelationId",
            query = "SELECT fb FROM FreeBook fb"
                    + " WHERE fb.itemId = :itemId"
                    + " AND fb.correlation_id = :correlation_id"
                    + " AND fb.free_type = 'NORMAL' "
        )
})
public class FreeBook extends digipages.common.BasePojo implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Mutable
    @Column(name = "item_id")
    private String itemId;
    
    // bi-directional many-to-one association to BookFile
    @Mutable
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "book_uni_id")
    private BookFile bookFile;
    
    @Mutable
    @Column(name = "correlation_id")
    private String correlation_id;
    
    @Mutable
    @Column(name = "free_type")
    private String free_type;
    
    @Mutable
    @Column(name = "read_expire_time")
    private String read_expire_time;
    
    @Mutable
    @Column(name = "read_start_time")
    private String read_start_time;
    
    @Mutable
    @Column(name = "read_days")
    private Integer read_days;
    
    @Mutable
    @Column(name = "status")
    private Integer status;

    @Mutable
    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    
    @Mutable
    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @Mutable
    @Column(name = "create_time")
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_time;
    
    @Mutable
    @Column(name = "sequence")
    private Integer sequence;
    
    @Mutable
    @Column(name = "order_priority")
    private Integer order_priority;

    @Mutable
    @Column(name = "udt")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;
    
    @Mutable
    @Column(name = "purchase_area_limit")
    private String purchase_area_limit;
    
    @Mutable
    @Column(name = "purchase_area_carea")
    private String purchase_area_carea;
    
    @Transient
    private String receive_status ;

    @PrePersist
    @PreUpdate
    public void onPrePersist() {
        if (lastUpdated == null) {
            lastUpdated = new Date();
            setLastUpdated(lastUpdated);
        }
    }

    public Date getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(Date lastUpdateTime) {
        this.lastUpdated = lastUpdateTime;
    }

    public void setLastUpdated(String lastUpdateTime) {
        ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
        setLastUpdated(Date.from(zdt.toInstant()));
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getItemId() {
        return this.itemId;
    }

    public void setItemId(String item) {
        this.itemId = item;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public BookFile getBookFile() {
        return this.bookFile;
    }

    public void setBookFile(BookFile bookFile) {
        this.bookFile = bookFile;
    }

    public String getCorrelation_id() {
        return correlation_id;
    }

    public void setCorrelation_id(String correlation_id) {
        this.correlation_id = correlation_id;
    }

    public String getFree_type() {
        return free_type;
    }

    public void setFree_type(String free_type) {
        this.free_type = free_type;
    }

    public String getRead_expire_time() {
        return read_expire_time;
    }

    public void setRead_expire_time(String read_expire_time) {
        this.read_expire_time = read_expire_time;
    }

    public String getRead_start_time() {
        return read_start_time;
    }

    public void setRead_start_time(String read_start_time) {
        this.read_start_time = read_start_time;
    }

    public Integer getRead_days() {
        return read_days;
    }

    public void setRead_days(Integer read_days) {
        this.read_days = read_days;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Integer getOrder_priority() {
        return order_priority;
    }

    public void setOrder_priority(Integer order_priority) {
        this.order_priority = order_priority;
    }

	public String getPurchase_area_limit() {
		return purchase_area_limit;
	}

	public void setPurchase_area_limit(String purchase_area_limit) {
		this.purchase_area_limit = purchase_area_limit;
	}

	public String getPurchase_area_carea() {
		return purchase_area_carea;
	}

	public void setPurchase_area_carea(String purchase_area_carea) {
		this.purchase_area_carea = purchase_area_carea;
	}

	public String getReceive_status() {
		return receive_status;
	}

	public void setReceive_status(String receive_status) {
		this.receive_status = receive_status;
	}
    
}
