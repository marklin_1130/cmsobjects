package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

import digipages.exceptions.ServerException;

/**
 * The persistent class for the member_drm_log database table.
 *
 */
@Entity
@Cache(
	type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 300000, // 5min
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Table(name = "member_eplan_drm_log")
@NamedQueries({
    @NamedQuery(
            name = "MemberEplanDrmLog.findByAllMemberAndEplanAndTransactionIdAndItemId",
            query = "SELECT medl FROM MemberEplanDrmLog medl"
                    + " WHERE medl.member = :member"
                + " AND medl.eplan = :eplan"
                + " AND medl.transactionId = :transactionId"
                + " AND medl.itemId = :itemId"
        ),
    @NamedQuery(
            name = "MemberEplanDrmLog.findByEplanIdDesc",
            query = "SELECT medl FROM MemberEplanDrmLog medl"
                    + " WHERE medl.eplan = :eplan"
                    + " ORDER BY medl.readExpireTime DESC"
        ),
    @NamedQuery(
    		name = "MemberEplanDrmLog.findByMemberAndEplanAndTransactionIdAndItemIdAndType",
    		query = "SELECT medl FROM MemberEplanDrmLog medl"
    			+ " WHERE medl.member = :member"
    			+ " AND medl.eplan = :eplan"
    			+ " AND medl.transactionId = :transactionId"
                + " AND medl.itemId = :itemId"
    			+ " AND medl.type = :type"
    	),
    @NamedQuery(
    		name = "MemberEplanDrmLog.findByMemberAndEplanDesc",
    		query = "SELECT medl FROM MemberEplanDrmLog medl"
    			+ " WHERE medl.member = :member"
    			+ " AND medl.eplan = :eplan"
    			+ " AND medl.status = 1"
    			+ " ORDER BY medl.readExpireTime DESC"
    	),
    @NamedQuery(
    		name = "MemberEplanDrmLog.findByMemberAndEplanAndTransactionIdDesc",
    		query = "SELECT medl FROM MemberEplanDrmLog medl"
    			+ " WHERE medl.member = :member"
    			+ " AND medl.eplan = :eplan"
    			+ " AND medl.transactionId = :transactionId"
    			+ " AND medl.status = 1"
    			+ " ORDER BY medl.readExpireTime DESC"
    	)
	
})
public class MemberEplanDrmLog extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Mutable
	@JoinColumn(name = "member_id")
	@ManyToOne
	@JsonIgnore
	private Member member;

	// bi-directional many-to-one association to Item
	@Mutable
	@JoinColumn(name = "eplan_id")
	@ManyToOne
	@JsonIgnore
	private Eplan eplan;
	
	@Mutable
	@Column(name = "item_id")
	private String itemId;
	
	@Mutable
	@Column(name = "download_expire_time")
	private String downloadExpireTime = "2099/12/31 23:59:59";

	@Mutable
	@Column(name = "status")
	private int status;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	@Column(name = "read_days")
	private Integer readDays;
	
	@Column(name = "read_start_time")
    private String readStartTime = "2099/12/31 23:59:59";

	@Column(name = "read_expire_time")
	private String readExpireTime = "2099/12/31 23:59:59";

    @Column(name = "transaction_id")
	private String transactionId;

	@Mutable
	@Column(name = "type")
	private int type;

//    //bi-directional many-to-one association to MemberBook
//	@ManyToMany(mappedBy="memberDrmLogs")
//    private List<MemberBook> memberBooks;
//
//    //bi-directional many-to-one association to MemberBookDrmMapping
//    @OneToMany(mappedBy="memberDrmLog")
//    private List<MemberBookDrmMapping> memberBookDrmMappings;
    

	public MemberEplanDrmLog() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Eplan getEplan() {
		return eplan;
	}

	public void setEplan(Eplan eplan) {
		this.eplan = eplan;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getDownloadExpireTime() {
		return this.downloadExpireTime;
	}

	public void setDownloadExpireTime(String downloadExpireTime) {
		this.downloadExpireTime = downloadExpireTime;
	}

	public int getReadDays() {
		return this.readDays;
	}

	public void setReadDays(int readDays) {
		this.readDays = readDays;
	}

	public String getReadStartTime() {
		return readStartTime;
	}

	public void setReadStartTime(String readStartTime) {
		this.readStartTime = readStartTime;
	}

	public String getReadExpireTime() {
		return readExpireTime;
	}

	public void setReadExpireTime(String readExpireTime) {
		this.readExpireTime = readExpireTime;
	}

	public void setReadDays(Integer readDays) {
		this.readDays = readDays;
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public static MemberEplanDrmLog getMemberDrmLogByTransactionIdWithTypeAndSubmitId(EntityManager em,
			Member member, Item item, String transactionId, int status,int type,String submitId) throws ServerException {
		try {
			return em.createNamedQuery("MemberDrmLog.findByMemberAndItemAndTransactionIdWithTypeAndSubmitId", MemberEplanDrmLog.class)
				.setParameter("member", member)
				.setParameter("item", item)
				.setParameter("transactionId", transactionId)
				.setParameter("status", status)
				.setParameter("type", type)
				.setParameter("submitId", submitId)
				.getSingleResult();
		} catch (Exception ex) {
			throw new ServerException("id_err_501", "DRM not exists.");
		}
	}

	public static MemberEplanDrmLog getMemberDrmLogById(EntityManager postgres, Long l) throws ServerException {
		try {
			return postgres.createNamedQuery("MemberDrmLog.findById", MemberEplanDrmLog.class)
				.setParameter("id", l)
				.getSingleResult();
		} catch (Exception ex) {
			throw new ServerException("id_err_501", "DRM not exists.");
		}
	}

	public boolean isStillActive() {
		if (status!=1)
			return false;

		return true;
	}
	
//	public List<MemberBook> getMemberBooks() {
//		return this.memberBooks;
//	}
//
//	public void setMemberBooks(List<MemberBook> memberBooks) {
//		this.memberBooks = memberBooks;
//	}
//
//	public List<MemberBookDrmMapping> getMemberBookDrmMappings() {
//		return this.memberBookDrmMappings;
//	}
//
//	public void setMemberBookDrmMappings(List<MemberBookDrmMapping> memberBookDrmMappings) {
//		this.memberBookDrmMappings = memberBookDrmMappings;
//	}
//
//	public MemberBookDrmMapping addMemberBookDrmMapping(MemberBookDrmMapping memberBookDrmMapping) {
//		getMemberBookDrmMappings().add(memberBookDrmMapping);
//		memberBookDrmMapping.setMemberDrmLog(this);
//
//		return memberBookDrmMapping;
//	}
//
//	public MemberBookDrmMapping removeMemberBookDrmMapping(MemberBookDrmMapping memberBookDrmMapping) {
//		getMemberBookDrmMappings().remove(memberBookDrmMapping);
//		memberBookDrmMapping.setMemberDrmLog(null);
//
//		return memberBookDrmMapping;
//	}
}