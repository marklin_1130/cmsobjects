package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.persistence.*;

import org.boon.json.annotations.JsonIgnore;

import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;
import org.eclipse.persistence.annotations.Struct;

import digipages.common.DPLogger;
import digipages.exceptions.ServerException;

/**
 * The persistent class for the item database table.
 * 
 */
@Entity
@Cache(
	type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
	size = 4000, // Use 4,000 as the initial cache size.
	expiry = 3000, // 3sec
	coordinationType = CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS // if cache coordination is used, only send invalidation messages.
)
@Struct(name= "childs")
@Table(name = "item")
@NamedQueries({
	@NamedQuery(
		name = "Item.findAll",
		query = "SELECT i FROM Item i"
	),
	@NamedQuery(
		name = "Item.findById",
		query = "SELECT i FROM Item i"
				+ " WHERE i.id=:id"
	),
	@NamedQuery(
		name = "Item.findByItemIds",
		query = "SELECT i FROM Item i"
				+ " WHERE i.id IN :itemIds"
	),
	@NamedQuery(
		name = "Item.findByItemIdsAndKeyword",
		query = "SELECT i FROM Item i"
				+ " WHERE i.id IN :itemIds"
				+ " AND i.cTitle LIKE :cTitle"
				+ " OR i.author LIKE :author"
				+ " OR i.publisherName LIKE :publisherName"
	),
	}
)
@NamedNativeQueries({
	@NamedNativeQuery(
			name = "Item.findChildItems", 
			query = "select * from item i where i.id in (select unnest(it.childs) from item it where it.Id= ?);",
			resultClass = Item.class
			
	)
}
)

public class Item extends digipages.common.BasePojo implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final DPLogger logger=DPLogger.getLogger(Item.class.getName());
	@Id
	@Column(unique = true, nullable = false)
	private String id;

	@Mutable
	@Column(name = "info")
	@Convert(converter = digipages.pgconverter.PGItemInfoConverter.class)
	private ItemInfo info;

	@Mutable
	@Column(name = "item_type")
	private String itemType;

	@Mutable
	@Column(name = "operator")
	private String operator;

	// bi-directional many-to-one association to BookFile
	@Mutable
	@OneToMany(mappedBy = "item",fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JsonIgnore
	private List<BookFile> bookFiles;

	// bi-directional many-to-one association to Item
	@Mutable
	@JoinColumn(name = "parent_item_id")
	@ManyToOne (fetch = FetchType.LAZY)
	@JsonIgnore
	private Item parentItem;

	// bi-directional many-to-one association to Item
	@Mutable
	@OneToMany(mappedBy = "parentItem", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Item> childItems;

	// bi-directional many-to-one association to Publisher
	@Mutable
	@JoinColumn(name = "publisher_id")
	@ManyToOne (fetch = FetchType.LAZY)
	@JsonIgnore
	private Publisher publisher;

	// bi-directional many-to-one association to Vendor
	@Mutable
	@JoinColumn(name = "vendor_id")
	@ManyToOne (fetch = FetchType.LAZY)
	@JsonIgnore
	private Vendor vendor;

	@Mutable
	@Column(name = "c_title")
	private String cTitle;

	@Mutable
	@Column(name = "author")
	private String author;

	@Mutable
	@Column(name = "publisher_name")
	private String publisherName;

	@Mutable
	@Column(name = "rank")
	private String rank;

	@Mutable
	@Column(name = "childs")
	@Array(databaseType = "text")
	private List<String> childs=new ArrayList<>();

	@Mutable
	@Column(name = "status")
	private int status=0;

	// bi-directional many-to-one association to MemberDrmLog
	@Mutable
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "item",fetch = FetchType.LAZY)
	@JsonIgnore
	private List<MemberDrmLog> memberDrmLogs;

	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Item() {

	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ItemInfo getInfo() {
		return this.info;
	}

	public void setInfo(ItemInfo info) {
		this.info = info;
	}

	public String getItemType() {
		return this.itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public List<BookFile> getBookFiles() {
		return this.bookFiles;
	}

	public void setBookFiles(List<BookFile> bookFiles) {
		this.bookFiles = bookFiles;
	}

	public BookFile addBookFile(BookFile bookFile) {
		getBookFiles().add(bookFile);
		bookFile.setItem(this);

		return bookFile;
	}

	public BookFile removeBookFile(BookFile bookFile) {
		getBookFiles().remove(bookFile);
		bookFile.setItem(null);

		return bookFile;
	}

	public Item getParentItem() {
		return this.parentItem;
	}

	public void setParentItem(Item item) {
		this.parentItem = item;
	}

	public List<Item> getChildItems() {
		return this.childItems;
	}

	public void setChildItems(List<Item> items) {
		this.childItems = items;
	}

	public Item addItem(Item item) {
		getChildItems().add(item);
		item.setParentItem(this);

		return item;
	}

	public Item removeItem(Item item) {
		getChildItems().remove(item);
		item.setParentItem(null);

		return item;
	}

	public Publisher getPublisher() {
		return this.publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Vendor getVendor() {
		return this.vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getcTitle() {
		return cTitle;
	}

	public void setcTitle(String cTitle) {
		this.cTitle = cTitle;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public List<String> getChilds() {
		return childs;
	}

	public void setChilds(List<String> childs) {
		this.childs = childs;
	}
	
	public void addChildItemId(String child)
	{
		childs.add(child);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<MemberDrmLog> getMemberDrmLogs() {
		return this.memberDrmLogs;
	}

	public void setMemberDrmLogs(List<MemberDrmLog> memberDrmLogs) {
		this.memberDrmLogs = memberDrmLogs;
	}

	public MemberDrmLog addMemberDrmLog(MemberDrmLog memberDrmLog) {
		getMemberDrmLogs().add(memberDrmLog);
		memberDrmLog.setItem(this);

		return memberDrmLog;
	}

	public static Item getItemByItemId(EntityManager em, String itemId) throws ServerException {
		try {
			return em.createNamedQuery("Item.findById", Item.class).setParameter("id", itemId).getSingleResult();
		} catch (Exception ex) {
			throw new ServerException("id_err_303", " Book not found, itemId" + itemId);
		}
	}


	public String getNewestReadyVersion(boolean isTrial) {
		BookVersion maxVersion = new BookVersion("V000.0000");

		for (BookFile bf : getBookFiles()) {
			// skip not match tria/normal version
			if (bf.getIsTrial()!=isTrial)
				continue;
			// skip not ready items.
			if (bf.getStatus()==null || bf.getStatus()<9)
				continue;
			BookVersion curVersion = new BookVersion(bf.getVersion());

			if (curVersion.newer(maxVersion)) {
				maxVersion = curVersion;
			}
		}

		return maxVersion.toString();
	}

	
	public BookFile getNewestReadyNormalBookFile(EntityManager em) {
		
		return BookFile.findLastestReadyByItemId(em, id);
	}
	public BookFile getNewestNormalBookFile(EntityManager em) {
		return BookFile.findLastestByItemId(em, id);
	}
	
	public BookFile getNewestReadyTrialBookFile(EntityManager em) {
		return BookFile.findNewTrialByItemId(em, id);
	}
	public BookFile getNewestTrialBookFile(EntityManager em) {
		return BookFile.findNewTrialByItemId(em, id);
	}
	
	public String genNewTrialVersion(EntityManager em, boolean isMajor)
	{
		String version="V001.0001";
		try {
			BookFile bf=this.getNewestTrialBookFile(em);
			version = bf.getVersion();
			BookVersion v = new BookVersion(version);
			v.upgradeVersion(isMajor);
			version=v.toString();
		} catch (Exception e)
		{
			logger.info("Notice, fail to gen version, use default (V001.0001):" + e.getMessage());
		}
		return version;
	}
	
	public String genNewNormalVersion(EntityManager em, boolean isMajor)
	{
		String version="V001.0001";
		try {
			BookFile bf=this.getNewestNormalBookFile(em);
			version = bf.getVersion();
			BookVersion v = new BookVersion(version);
			v.upgradeVersion(isMajor);
			version=v.toString();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return version;
	}

	
	/**
	 * query example: select * from item where id in (select unnest(childs) from item where id='0001');
	 * @param postgres
	 * @return
	 */
	public Vector<Item> getChildItems(EntityManager postgres) {
		
		Vector<Item> ret = new Vector<>();
		
		TypedQuery<Item> query = postgres.createNamedQuery("Item.findChildItems", Item.class);
		query.setParameter(1, this.getId());
		ret = (Vector<Item>) query.getResultList();
		return ret;
	}

	public String getBookUniId(boolean isTrial) {
		String format = getBookFiles().get(0).getFormat();
		String uniId = getId() + "_" + format;
		uniId += (isTrial ? "_trial" : "_normal");
		return uniId;
	}

	/**
	 * search all my book_file, if any bookFile is success in convert, return true;
	 * @return
	 */
	public boolean hasSuccessFiles() {
		for (BookFile bf:this.getBookFiles())
		{
			// 0 = in progress/just inserted, 8=convert done, 9=ready to ship.
			if (bf.getStatus()>=0)
				return true;
		}
		return false;
	}

	public static void deleteCascade(EntityManager postgres, Item item) {
		List<BookFile> removeList=new ArrayList<>();
		for (BookFile bf:item.bookFiles)
		{
			removeList.add(bf);
		}
		
		for (BookFile bf:removeList)
		{
			BookFile.deleteCascade(postgres,bf);
			item.removeBookFile(bf);
		}
		postgres.remove(item);
		
	}

	/**
	 * if there are too many version in single item. (ex too many version)
	 * consider use query method. 
	 * @param postgres
	 * @return
	 */
	public int getAvailTrialBookCnt(EntityManager postgres) {
		int trialCnt=0;
		for (BookFile bf:getBookFiles())
		{
			if (!bf.getIsTrial())
				continue;
			
			if (null!=bf.getStatus() && bf.getStatus()>=8)
				trialCnt++;
		}
		return trialCnt;
	}




}