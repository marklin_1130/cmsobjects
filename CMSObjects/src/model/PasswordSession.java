package model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.boon.json.annotations.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.Mutable;

@Entity
@Cache(
		type = CacheType.NONE, // Cache everything until the JVM decides memory is low.
		size = 4000, // Use 4,000 as the initial cache size.
		expiry = 300000, // 5min
		coordinationType=CacheCoordinationType.INVALIDATE_CHANGED_OBJECTS  // if cache coordination is used, only send invalidation messages.
)
@Table(name = "password_session")

@NamedQueries({
	@NamedQuery(name = "PasswordSession.findAll", query = "SELECT p FROM PasswordSession p") ,
	@NamedQuery(name = "PasswordSession.findBySessionToken", 
			query = "SELECT p FROM PasswordSession p WHERE p.memberId=:memberId"
					+ " AND p.sessionToken=:sessionToken"
					+ " AND p.expiresAfter>:expiresAfter  ") ,
	@NamedQuery(name = "PasswordSession.newNewPSMemberId",
			query = "SELECT p FROM PasswordSession p WHERE p.memberId=:memberId "
					+ " AND p.status!=2 "
					+ " AND p.expiresAfter>:expiresAfter ORDER BY p.lastUpdated DESC ") 	
})


public class PasswordSession extends digipages.common.BasePojo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static final int STATUSERROR = -9;
	public static final int STATUSSEND = 0;
	public static final int STATUSCLICKED = 1;
	public static final int STATUSSUCCESS = 2;


	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	@Mutable
	@Column(name = "member_id")
	private Long memberId;
	
	@Mutable
	@Column(name = "session_token")
	private String sessionToken;

	@Mutable
	@Column(name = "expires_after")
	private String expiresAfter;
	
	@Mutable
	@Column(name = "msg")
	private String msg;

	@Mutable
	@JsonIgnore
	@Column(name = "status")
	private int status;

	@JsonIgnore
	@Mutable
	@Column(name = "udt")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdated;

	@PrePersist
	@PreUpdate
	public void onPrePersist() {
		if (lastUpdated == null) {
			lastUpdated = new Date();
			setLastUpdated(lastUpdated);
		}
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdateTime) {
		this.lastUpdated = lastUpdateTime;
	}

	public void setLastUpdated(String lastUpdateTime) {
		ZonedDateTime zdt = ZonedDateTime.parse(lastUpdateTime);
		setLastUpdated(Date.from(zdt.toInstant()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getExpiresAfter() {
		return expiresAfter;
	}

	public void setExpiresAfter(String expiresAfter) {
		this.expiresAfter = expiresAfter;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
